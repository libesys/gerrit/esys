/*!
 * \file esys/version.h
 * \brief Version info for esys_wx
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WX_VERSION_H__
#define __ESYS_WX_VERSION_H__

// Bump-up with each new version
#define ESYS_WX_MAJOR_VERSION    0
#define ESYS_WX_MINOR_VERSION    0
#define ESYS_WX_RELEASE_NUMBER   1
#define ESYS_WX_VERSION_STRING   "ESys wxWdigets 0.0.1"
#define ESYS_WX_VERSION_WSTRING  L"ESys wxWdigets 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYS_WX_VERSION_NUM_DOT_STRING   "0.0.1"
#define ESYS_WX_VERSION_NUM_DOT_WSTRING  L"0.0.1"
#define ESYS_WX_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define ESYS_WX_VERSION_NUMBER (ESYS_WX_MAJOR_VERSION * 1000) + (ESYS_WX_MINOR_VERSION * 100) + ESYS_WX_RELEASE_NUMBER
#define ESYS_WX_BETA_NUMBER      1
#define ESYS_WX_VERSION_FLOAT ESYS_WX_MAJOR_VERSION + (ESYS_WX_MINOR_VERSION/10.0) + (ESYS_WX_RELEASE_NUMBER/100.0) + (ESYS_WX_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYS_WX_CHECK_VERSION(major,minor,release) \
    (ESYS_WX_MAJOR_VERSION > (major) || \
    (ESYS_WX_MAJOR_VERSION == (major) && ESYS_WX_MINOR_VERSION > (minor)) || \
    (ESYS_WX_MAJOR_VERSION == (major) && ESYS_WX_MINOR_VERSION == (minor) && ESYS_WX_RELEASE_NUMBER >= (release)))

#endif

