/*!
 * \file esys/esys_wx_defs.h
 * \brief Definitions needed for esys_wx
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WX_DEFS_H__
#define __ESYS_WX_DEFS_H__

#ifdef ESYS_WX_EXPORTS
#define ESYS_WX_API __declspec(dllexport)
#elif ESYS_WX_USE
#define ESYS_WX_API __declspec(dllimport)
#else
#define ESYS_WX_API
#endif

#endif

