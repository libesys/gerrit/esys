/*!
 * \file esys/version.h
 * \brief Version info for esys_sysc
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_VERSION_H__
#define __ESYS_SYSC_VERSION_H__

// Bump-up with each new version
#define ESYS_SYSC_MAJOR_VERSION    0
#define ESYS_SYSC_MINOR_VERSION    0
#define ESYS_SYSC_RELEASE_NUMBER   1
#define ESYS_SYSC_VERSION_STRING   "ESys SystemC 0.0.1"
#define ESYS_SYSC_VERSION_WSTRING  L"ESys SystemC 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYS_SYSC_VERSION_NUM_DOT_STRING   "0.0.1"
#define ESYS_SYSC_VERSION_NUM_DOT_WSTRING  L"0.0.1"
#define ESYS_SYSC_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define ESYS_SYSC_VERSION_NUMBER (ESYS_SYSC_MAJOR_VERSION * 1000) + (ESYS_SYSC_MINOR_VERSION * 100) + ESYS_SYSC_RELEASE_NUMBER
#define ESYS_SYSC_BETA_NUMBER      1
#define ESYS_SYSC_VERSION_FLOAT ESYS_SYSC_MAJOR_VERSION + (ESYS_SYSC_MINOR_VERSION/10.0) + (ESYS_SYSC_RELEASE_NUMBER/100.0) + (ESYS_SYSC_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYS_SYSC_CHECK_VERSION(major,minor,release) \
    (ESYS_SYSC_MAJOR_VERSION > (major) || \
    (ESYS_SYSC_MAJOR_VERSION == (major) && ESYS_SYSC_MINOR_VERSION > (minor)) || \
    (ESYS_SYSC_MAJOR_VERSION == (major) && ESYS_SYSC_MINOR_VERSION == (minor) && ESYS_SYSC_RELEASE_NUMBER >= (release)))

#endif

