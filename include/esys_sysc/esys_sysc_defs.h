/*!
 * \file esys/esys_sysc_defs.h
 * \brief Definitions needed for esys_sysc
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_DEFS_H__
#define __ESYS_SYSC_DEFS_H__

#ifdef ESYS_SYSC_LOCAL

#include "esys/esys_defs.h"
#define ESYS_SYSC_API ESYS_API

#else

#ifdef ESYS_SYSC_EXPORTS
#define ESYS_SYSC_API __declspec(dllexport)
#elif ESYS_SYSC_USE
#define ESYS_SYSC_API __declspec(dllimport)
#else
#define ESYS_SYSC_API
#endif

#endif

#endif

