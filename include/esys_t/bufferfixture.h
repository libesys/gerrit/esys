/*!
 * \file esys_t/bufferfixture.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_T_BUFFERFIXTURE_H__
#define __ESYS_T_BUFFERFIXTURE_H__

#include <esys/inttypes.h>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/shared_ptr.hpp>

#include <string.h>

template<typename T, uint16_t N>
class Test
{
public:
    Test(): m_size(0)
    {
    }

    void Write(T *buf, uint16_t size)
    {
        memcpy(m_buf, buf, size*sizeof(T));
        m_size=size;
    }

    void Write1(T *buf, uint16_t size)
    {
        for (uint16_t idx=0; idx<size; ++idx)
        {
            m_buf[idx]=buf[idx]+0x10;
        }
        m_size=size;
    }

    int16_t Write2(T *buf, uint16_t size)
    {
        for (uint16_t idx=0; idx<size; ++idx)
        {
            m_buf[idx]=buf[idx]+0x20;
        }
        m_size=size;
        return 0;
    }

    T m_buf[N];
    uint16_t m_size;
};

struct BufferFixture
{
    BufferFixture();

    template<typename T, typename C, uint16_t N>
    void BasicTest(C &buf, uint16_t max_size)
    {
        Test<T,N> test;
        typename C::Ptr p;

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetNbrSegment());

        buf.SetSize(4);
        ESYSTEST_REQUIRE_EQUAL(4, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        ESYSTEST_REQUIRE_EQUAL(1, buf.GetNbrSegment());

        p=buf.Block();
        *p=0x10;
        ESYSTEST_REQUIRE_EQUAL(0x10, buf[0]);
        ESYSTEST_REQUIRE_EQUAL(0x10, *p);

        *p=0x12;
        ESYSTEST_REQUIRE_EQUAL(0x12, buf[0]);
        ESYSTEST_REQUIRE_EQUAL(0x12, *p);

        p++;
        *p=0x13;
        ESYSTEST_REQUIRE_EQUAL(0x12, buf[0]);
        ESYSTEST_REQUIRE_EQUAL(0x13, buf[1]);
        ESYSTEST_REQUIRE_EQUAL(0x13, *p);

        buf[2]=0x14;
        ESYSTEST_REQUIRE_EQUAL(0x14, buf[2]);
        buf[3]=0x15;
        ESYSTEST_REQUIRE_EQUAL(0x15, buf[3]);

        ESYSTEST_REQUIRE_EQUAL(0, test.m_size);

        buf.DoEachSeg(&test, &Test<T,N>::Write);

        ESYSTEST_REQUIRE_EQUAL(4, test.m_size);
        ESYSTEST_REQUIRE_EQUAL(0x12, test.m_buf[0]);
        ESYSTEST_REQUIRE_EQUAL(0x13, test.m_buf[1]);
        ESYSTEST_REQUIRE_EQUAL(0x14, test.m_buf[2]);
        ESYSTEST_REQUIRE_EQUAL(0x15, test.m_buf[3]);

        buf.DoEachSeg(test, &Test<T,N>::Write1);

        ESYSTEST_REQUIRE_EQUAL(4, test.m_size);
        ESYSTEST_REQUIRE_EQUAL(0x22, test.m_buf[0]);
        ESYSTEST_REQUIRE_EQUAL(0x23, test.m_buf[1]);
        ESYSTEST_REQUIRE_EQUAL(0x24, test.m_buf[2]);
        ESYSTEST_REQUIRE_EQUAL(0x25, test.m_buf[3]);

        int16_t result=buf.DoEachSeg(test, &Test<T,N>::Write2);

        ESYSTEST_REQUIRE_EQUAL(0, result);

        ESYSTEST_REQUIRE_EQUAL(4, test.m_size);
        ESYSTEST_REQUIRE_EQUAL(0x32, test.m_buf[0]);
        ESYSTEST_REQUIRE_EQUAL(0x33, test.m_buf[1]);
        ESYSTEST_REQUIRE_EQUAL(0x34, test.m_buf[2]);
        ESYSTEST_REQUIRE_EQUAL(0x35, test.m_buf[3]);
    }

    template<typename T, typename C, uint16_t N>
    void CopyTest(C &buf, uint16_t max_size)
    {
        Test<T,N> test;
        typename C::Ptr p;
        T raw[]= {0x12, 0x13, 0x14, 0x15 };

        p=buf.Block();

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetNbrSegment());

        test.Write(raw, sizeof(raw));

        ESYSTEST_REQUIRE_EQUAL(-1, buf.Copy(p, raw, sizeof(raw)));

        buf.SetSize(4);
        ESYSTEST_REQUIRE_EQUAL(4, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        buf.Set(0);

        for (uint16_t idx=0; idx<4; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(0, buf[idx]);
        }

        ESYSTEST_REQUIRE_EQUAL(0, buf.Copy(p, raw, sizeof(raw)));

        for (uint16_t idx=0; idx<4; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(raw[idx], buf[idx]);
        }

        buf.SetSize(14);
        p+=10;

        ESYSTEST_REQUIRE_EQUAL(0, buf.Copy(p, raw, sizeof(raw)));

        for (uint16_t idx=0; idx<4; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(raw[idx], buf[10+idx]);
        }
    }

    template<typename T, typename C, uint16_t N>
    void CopySGTest(C &buf, uint16_t max_size)
    {
        Test<T,N> test;
        typename C::Ptr p;
        T raw0[]= {0x72, 0x73, 0x74, 0x75 };
        T raw1[]= {0x82, 0x83, 0x84, 0x85 };

        buf.UseScatterGather(true);

        //CopyTest<T, C, N>(buf, max_size);

        p=buf.Block();

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetNbrSegment());

        test.Write(raw0, sizeof(raw0));

        ESYSTEST_REQUIRE_EQUAL(-1, buf.Copy(p, raw0, sizeof(raw0)));

        buf.SetSize(4);
        ESYSTEST_REQUIRE_EQUAL(4, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        ESYSTEST_REQUIRE_EQUAL(1, buf.GetNbrSegment());

        buf.Set(0);

        for (uint16_t idx=0; idx<4; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(0, buf[idx]);
        }

        buf.SetSize(max_size);
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        for (uint16_t idx=0; idx<max_size; ++idx)
        {
            buf[idx]=idx&0xFF;
        }

        buf.SetSize(4);

        ESYSTEST_REQUIRE_EQUAL(0, buf.Copy(p, raw0, sizeof(raw0)));

        ESYSTEST_REQUIRE_EQUAL(1, buf.GetNbrSegment());

        uint8_t *seg;
        uint16_t size;

        seg=buf.GetSegment(0, size);
        ESYSTEST_REQUIRE_NE((uint8_t *)NULL, seg);
        ESYSTEST_REQUIRE_EQUAL(4, size);

        for (uint16_t idx=0; idx<4; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(raw0[idx], buf[idx]);
            ESYSTEST_REQUIRE_EQUAL(raw0[idx], seg[idx]);
        }

        buf.SetSize(14);
        p+=10;

        ESYSTEST_REQUIRE_EQUAL(2, buf.GetNbrSegment());

        seg=buf.GetSegment(1, size);
        ESYSTEST_REQUIRE_NE((uint8_t *)NULL, seg);
        ESYSTEST_REQUIRE_EQUAL(10, size);

        for (uint16_t idx=4; idx<14; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(idx, buf[idx]);
            ESYSTEST_REQUIRE_EQUAL(idx, seg[idx-4]);
        }

        ESYSTEST_REQUIRE_EQUAL(0, buf.Copy(p, raw1, sizeof(raw1)));

        ESYSTEST_REQUIRE_EQUAL(3, buf.GetNbrSegment());

        seg=buf.GetSegment(0, size);
        ESYSTEST_REQUIRE_NE((uint8_t *)NULL, seg);
        ESYSTEST_REQUIRE_EQUAL(4, size);

        for (uint16_t idx=0; idx<4; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(raw0[idx], buf[idx]);
            ESYSTEST_REQUIRE_EQUAL(raw0[idx], seg[idx]);
        }

        seg=buf.GetSegment(1, size);
        ESYSTEST_REQUIRE_NE((uint8_t *)NULL, seg);
        ESYSTEST_REQUIRE_EQUAL(6, size);

        for (uint16_t idx=4; idx<10; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(idx, buf[idx]);
            ESYSTEST_REQUIRE_EQUAL(idx, seg[idx-4]);
        }

        seg=buf.GetSegment(2, size);
        ESYSTEST_REQUIRE_NE((uint8_t *)NULL, seg);
        ESYSTEST_REQUIRE_EQUAL(4, size);

        for (uint16_t idx=0; idx<4; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(raw1[idx], buf[10+idx]);
            ESYSTEST_REQUIRE_EQUAL(raw1[idx], seg[idx]);
        }

        buf.SetSize(20);
        ESYSTEST_REQUIRE_EQUAL(4, buf.GetNbrSegment());

        seg=buf.GetSegment(3, size);
        ESYSTEST_REQUIRE_NE((uint8_t *)NULL, seg);
        ESYSTEST_REQUIRE_EQUAL(6, size);

        for (uint16_t idx=14; idx<20; ++idx)
        {
            ESYSTEST_REQUIRE_EQUAL(idx, buf[idx]);
            ESYSTEST_REQUIRE_EQUAL(idx, seg[idx-14]);
        }
    }

    template<typename T, typename C>
    void RandomTest(C &buf, uint16_t max_size, uint32_t nbr_iteration)
    {
        uint32_t idx;
        uint32_t data_w_size=0;
        uint32_t data_r_size=0;
        uint16_t access_size;
        uint16_t cur_size;
        typename C::Ptr p;

        if (m_max_access_size==0)
            m_max_access_size=max_size/2;

        boost::shared_ptr<uint8_t> temp(new uint8_t[m_max_access_size]);

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
        ESYSTEST_REQUIRE_EQUAL(max_size, buf.GetMaxSize());

        ESYSTEST_REQUIRE_EQUAL(0, buf.GetNbrSegment());

        for (idx=0; idx<nbr_iteration; ++idx)
        {
            cur_size=buf.GetMaxSize()-buf.GetSize();
            do
            {
                access_size=GetAccessSize();
            }
            while (access_size>cur_size);

            FillInputBuf(temp.get(), data_w_size, access_size);
            data_w_size+=access_size;

            cur_size=buf.GetSize();
            buf.Push(temp.get(), access_size);
            ESYSTEST_REQUIRE_EQUAL(cur_size+access_size, buf.GetSize());

            cur_size=buf.GetSize();
            do
            {
                access_size=GetAccessSize();
            }
            while (access_size>cur_size);

            p=buf.Block();

            VerifyBuf<C>(p, data_r_size, access_size);
            data_r_size+=access_size;

            ESYSTEST_REQUIRE_EQUAL(0, buf.Pop(access_size));
            ESYSTEST_REQUIRE_EQUAL(cur_size-access_size, buf.GetSize());
        }
    }

    uint16_t GetAccessSize()
    {
        boost::random::uniform_int_distribution<> dist(1, m_max_access_size);

        return dist(m_gen);
    }

    void FillInputBuf(uint8_t *buf, uint32_t data_idx, uint16_t size)
    {
        uint16_t idx;

        for (idx=0; idx<size; ++idx)
        {
            buf[idx]=(data_idx+idx)&0xFF;
        }
    }

    template<typename C>
    void VerifyBuf(typename C::Ptr &buf, uint32_t data_idx, uint16_t size)
    {
        uint16_t idx;
        uint8_t val;

        for (idx=0; idx<size; ++idx)
        {
            val=(data_idx+idx)&0xFF;
            //ESYSTEST_REQUIRE_MESSAGE( val==buf[idx], "data_idx=" << data_idx << " " << "idx=" << idx);
            //ESYSTEST_REQUIRE_EQUAL(, buf[idx]);
        }

    }

    uint16_t m_max_access_size;
    boost::random::mt19937 m_gen;
};

#endif
