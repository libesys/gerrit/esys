/*!
 * \file esys/version.h
 * \brief Version info for esys_t
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_T_VERSION_H__
#define __ESYS_T_VERSION_H__

// Bump-up with each new version
#define ESYS_T_MAJOR_VERSION    0
#define ESYS_T_MINOR_VERSION    0
#define ESYS_T_RELEASE_NUMBER   1
#define ESYS_T_VERSION_STRING   _T("esys_t 0.0.1")

// Must be updated manually as well each time the version above changes
#define ESYS_T_VERSION_NUM_DOT_STRING   "0.0.1"
#define ESYS_T_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define ESYS_T_VERSION_NUMBER (ESYS_T_MAJOR_VERSION * 1000) + (ESYS_T_MINOR_VERSION * 100) + ESYS_T_RELEASE_NUMBER
#define ESYS_T_BETA_NUMBER      1
#define ESYS_T_VERSION_FLOAT ESYS_T_MAJOR_VERSION + (ESYS_T_MINOR_VERSION/10.0) + (ESYS_T_RELEASE_NUMBER/100.0) + (ESYS_T_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYS_T_CHECK_VERSION(major,minor,release) \
    (ESYS_T_MAJOR_VERSION > (major) || \
    (ESYS_T_MAJOR_VERSION == (major) && ESYS_T_MINOR_VERSION > (minor)) || \
    (ESYS_T_MAJOR_VERSION == (major) && ESYS_T_MINOR_VERSION == (minor) && ESYS_T_RELEASE_NUMBER >= (release)))

#endif

