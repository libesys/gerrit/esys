/*!
 * \file esys_t/unitestexe.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_T_UNITTESTEXE__
#define __ESYS_T_UNITTESTEXE__

#include "esys_t_fix/unittestexebase.h"

#include <esystest/testcasectrl.h>
#include <esystest/stdlogger.h>

/*namespace esys_t
{ */

class UnitTestExe: public esys_t_fix::UnitTestExeBase
{
public:
    UnitTestExe();
    virtual ~UnitTestExe();

    static UnitTestExe &Get();
protected:
    static UnitTestExe *s_unit_test_exe;
    esystest::StdLogger m_logger;
    //esystest::TestCaseCtrl m_test_ctrl;
};

//}

#endif

