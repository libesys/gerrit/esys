/*!
 * \file esys_t_fix/setup.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_T_FIX_SETUP_H__
#define __ESYS_T_FIX_SETUP_H__

#include <esys/setup.h>

#ifdef ESYS_T_FIX_HAS_CFG
#include "esys_t_fix/config.h"
#endif

#endif


