/*!
 * \file esys/esys_t_fix_defs.h
 * \brief Definitions needed for esys_t_fix
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_T_FIX_DEFS_H__
#define __ESYS_T_FIX_DEFS_H__

#ifdef ESYS_T_FIX_EXPORTS
#define ESYS_T_FIX_API __declspec(dllexport)
#elif ESYS_T_FIX_USE
#define ESYS_T_FIX_API __declspec(dllimport)
#else
#define ESYS_T_FIX_API
#endif

#include <esys/esys_setup.h>

#include "esys_t_fix/config.h"

#endif

