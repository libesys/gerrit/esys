/*!
 * \file esys_t_fix/argsfixturebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_t_fix/esys_t_fix_defs.h"
//#include <esys/esys_setup.h>

#ifdef _MSC_VER
//#pragma warning (disable : 4996)
#endif

#include <boost/filesystem/path.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

#include <vector>
#include <string>

namespace esys_t_fix
{

/*! \class ArgsFixtureBase esys_t_fix/argsfixturebase.h "esys_t_fix/argsfixturebase.h"
 *  \brief The base class of all fixture handling command line arguments
 *
 */
class ESYS_T_FIX_API ArgsFixtureBase
{
public:
    //! Constructor
    ArgsFixtureBase();

    //! Destructor
    virtual ~ArgsFixtureBase();

    //! Set the folder where the test files are located
    void SetTestFilesFolder(const std::wstring &test_files_folder);

    //! Set the folder where the test files are located
    void SetTestFilesFolder(const std::string &test_files_folder);

    //! Search for the folder containing the test files
    /*
     * The following steps are taken by searching path:
     * - composed of 1 environment variable and a relative search path
     * - composed of current working directory and a relative search path
     * - absolute search path
     * - environment variable as absolute path
     */
    virtual int32_t FindFolders();

    //! Search for test files folder by using environment variable as root folder
    virtual int32_t FindFoldersEnvVarAsRoot();

    //! Search for test files folder by using current working directory as root folder
    virtual int32_t FindFoldersCWDAsRoot();

    void AddSearchPath(const std::wstring &search_path);
    void AddSearchPathEnvVar(const std::wstring &env_var);
    int32_t SearchRelativePathFromRoot(const boost::filesystem::path &root_path);

    virtual void AddOptions(boost::program_options::options_description *desc);
    virtual int32_t Parse();
    bool IsParsed();

    void LoadEnvVar();

    void SetArgs(int argc, char **argv);
    void SetArgs(int argc, wchar_t **argv);
protected:
    void AddDefaultOptions();

    int m_argc = 0;
    char **m_argv = nullptr;
    wchar_t **m_wargv = nullptr;
    std::map<std::wstring, std::wstring> m_map_env_vars;
    std::vector<std::wstring> m_vec_env_vars;
    std::vector<std::wstring> m_search_paths;
    std::vector<std::wstring> m_search_path_env_vars;
    std::wstring m_test_file_path;
    std::string m_test_file_path_s;
    std::string m_log_trace_path;
    std::wstring m_test_files_folder;
    std::string m_dft_test_file_path;
    boost::filesystem::path m_abs_test_file_path;
    boost::filesystem::path m_abs_temp_path;
    boost::program_options::variables_map m_vm;
    boost::program_options::options_description m_desc{ "Allowed options" };
    bool m_is_parsed = false;
    int m_verbose = 0;
    bool m_log_trace = false;
};

}





