/*!
 * \file ral_test_fix/crctest.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_t_fix/esys_t_fix_defs.h"

#include <esys/crcbase.h>

namespace esys_t_fix
{

class ESYS_T_FIX_API CRCCCITT16Test
{
public:
    CRCCCITT16Test();
    virtual ~CRCCCITT16Test();

    void Test();

    void SetCRC(esys::CRCBase *crc);
    esys::uint32_t GetChecksumCount();
    esys::uint16_t GetChecksum(esys::uint32_t idx);
protected:
    esys::CRCBase *m_crc = nullptr;
    esys::uint16_t m_checksums[2];
};

}



