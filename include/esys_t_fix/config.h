/*!
 * \file esys_t_fix/config.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYS_LIB_NAME
#undef ESYS_LIB_NAME
#endif
#define ESYS_LIB_NAME esys_t_fix

#ifdef ESYS_LIB_VERSION
#undef ESYS_LIB_VERSION
#endif
#define ESYS_LIB_VERSION "0.1.0"

#ifndef ESYS_T_FIX_EXPORTS
#include <esys/autolink_msvc.h>
#endif
