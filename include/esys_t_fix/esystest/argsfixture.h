/*!
 * \file esys_t_fix/esystest/argsfixture.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_T_FIX_ESYSTEST_ARGSFIXTURE_H__
#define __ESYS_T_FIX_ESYSTEST_ARGSFIXTURE_H__

#include "esys_t_fix/esys_t_fix_defs.h"
#include <esys/esys_setup.h>
#include <esystest/testcasectrl.h>

#include <string>

#ifdef _MSC_VER
#pragma warning (disable : 4996)
#endif

#include <boost/filesystem.hpp>

#ifdef _MSC_VER
#pragma warning (default : 4996)
#endif

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace esys_t_fix
{

namespace esystest
{

class ESYS_T_FIX_API ArgsFixture: public ::esystest::TestCaseCtrl
{
public:
    ArgsFixture();
    virtual ~ArgsFixture();

    virtual void AddOptions(::boost::program_options::options_description &desc) override;
    //virtual int32_t Parse();
    //bool IsParsed();
    virtual int32_t HandleSwitches() override;
    void SetTestFilesFolder(const std::string &test_files_folder);

    //virtual void FindFolders(); // override;

protected:
    void AddDefaultOptions();

    //int m_argc;
    //char **m_argv;
    std::string m_test_file_path;
    std::string m_log_trace_path;
    std::string m_test_files_folder;
    std::string m_dft_test_file_path;
    ::boost::filesystem::path m_abs_test_file_path;
    ::boost::filesystem::path m_abs_temp_path;
    /*::boost::program_options::variables_map m_vm; */
    ::boost::program_options::options_description m_desc;
    /*bool m_is_parsed;
    int m_verbose;
    bool m_log_trace; */
};

}

#ifdef ESYS_T_FIX_ESYSTEST
using namespace esystest;
#endif

}

#ifdef _MSC_VER
#pragma warning(pop)
#pragma warning (default : 4251)
#endif

#endif


