/*!
 * \file esys_t_fix/unitestexebase.h
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_T_FIX_UNITTESTEXEBASE__
#define __ESYS_T_FIX_UNITTESTEXEBASE__

#include "esys_t_fix/esys_t_fix_defs.h"

#ifdef ESYS_VHW
#include "esys_t_fix/argsfixture.h"
#endif

namespace esys_t_fix
{

class ESYS_T_FIX_API UnitTestExeBase
#ifdef ESYS_VHW
    : public ArgsFixture
#endif
{
public:
    UnitTestExeBase();
    virtual ~UnitTestExeBase();
protected:
    //::boost::program_options::options_description m_desc;
};

}

#endif
