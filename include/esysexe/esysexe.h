/*!
 * \file esysexe/esysexe.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esys/usbtreeitembase.h>

#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/options_description.hpp>

#include <string>
#include <ostream>

namespace po = boost::program_options;

class ESysExe
{
public:
    ESysExe();
    ~ESysExe();

    void SetArgs(int argc, char **argv);
    void SetArgs(int argc, wchar_t **argv);
    int ParseArgs();
    int Do();

    bool GetDebug();

    void SetOs(std::ostream &os);
    std::ostream *GetOs();

    int CmdListPlugings();
    int CmdListUSB();
    int CmdTreeUSB();
    int CmdListCOM();

    void Print(esys::USBTreeItemBase *tree_item, uint32_t level);
    void Print(unsigned int idx, esys::USBDevInfo *dev_info, uint32_t level);
    void PrintHelp(std::ostream &os);

    void SetErrorMsg(const std::string &error_msg);
    const std::string &GetErrorMsg();

protected:
    std::ostream *m_os = nullptr;
    int m_argc = 0;
    char **m_argv = nullptr;
    std::vector<std::string> m_args;
    po::variables_map m_vm;
    po::options_description *m_desc = nullptr;
    std::vector<std::string> m_to_parse_further;
    bool m_parse_error = false;
    std::string m_error_msg;
};
