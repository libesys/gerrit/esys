/*!
 * \file esys/version.h
 * \brief Version info for esysexe
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSEXE_MAJOR_VERSION 0
#define ESYSEXE_MINOR_VERSION 1
#define ESYSEXE_RELEASE_NUMBER 0
#define ESYSEXE_VERSION_STRING "ESysEXE 0.1.0"

// Must be updated manually as well each time the version above changes
#define ESYSEXE_VERSION_NUM_DOT_STRING "0.1.0"
#define ESYSEXE_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSEXE_VERSION_NUMBER (ESYSEXE_MAJOR_VERSION * 1000) + (ESYSEXE_MINOR_VERSION * 100) + ESYSEXE_RELEASE_NUMBER
#define ESYSEXE_BETA_NUMBER 1
#define ESYSEXE_VERSION_FLOAT                                                                 \
    ESYSEXE_MAJOR_VERSION + (ESYSEXE_MINOR_VERSION / 10.0) + (ESYSEXE_RELEASE_NUMBER / 100.0) \
        + (ESYSEXE_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSEXE_CHECK_VERSION(major, minor, release)                                                          \
    (ESYSEXE_MAJOR_VERSION > (major) || (ESYSEXE_MAJOR_VERSION == (major) && ESYSEXE_MINOR_VERSION > (minor)) \
     || (ESYSEXE_MAJOR_VERSION == (major) && ESYSEXE_MINOR_VERSION == (minor) && ESYSEXE_RELEASE_NUMBER >= (release)))
