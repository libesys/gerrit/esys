/*!
 * \file esys/usbdevserialportinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_USBDEVINFOSERIALPORT_H__
#define __ESYS_USBDEVINFOSERIALPORT_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/usbdevserialportinfobase.h>
#include <esys/usbdevinfo.h>

#include <string>

namespace esys
{

/* class ESYS_API USBDevSerialPortInfo: public USBDevSerialPortInfoBase, public USBDevInfo
{
public:
    friend class ESYSPROG_API USBEnum;

    USBDevSerialPortInfo();
    virtual ~USBDevSerialPortInfo();

    //virtual void GetPortName(char *name,uint8_t &size);
    //virtual const std::string &GetPortName() const;

    //virtual bool operator==(const USBDevInfoBase& obj) const;

    //void SetPortName(const char *name);
protected:


    std::string m_port_name;
}; */

}

#endif