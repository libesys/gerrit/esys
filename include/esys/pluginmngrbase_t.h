/*!
 * \file esys/pluginmngrbase_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/pluginmngrbase.h"
#include "esys/pluginbase.h"

#include <boost/locale.hpp>

namespace esys
{

template<typename T>
class PluginMngrBase_t : public PluginMngrBase
{
public:
    PluginMngrBase_t();
    virtual ~PluginMngrBase_t();

    virtual int Load() override;

    virtual PluginBase *GetBase(const std::wstring &short_name) override;
    virtual PluginBase *GetBase(const std::string &short_name) override;

    virtual T *Get(std::size_t index);
    virtual T *Get(const std::wstring &short_name);
    virtual T *Get(const std::string &short_name);
protected:
    std::vector<T *> m_plugins;
    std::map<std::wstring, T *> m_map_plugin_short_name;
};

template<typename T>
PluginMngrBase_t<T>::PluginMngrBase_t() : PluginMngrBase()
{
}

template<typename T>
PluginMngrBase_t<T>::~PluginMngrBase_t()
{
}

template<typename T>
int PluginMngrBase_t<T>::Load()
{
    int result;
    std::size_t idx;
    PluginBase *plugin_base;
    T *plugin;

    result = PluginMngrBase::Load();

    if (result < 0)
        return result;

    for (idx = 0; idx < GetNbr(); ++idx)
    {
        plugin_base = PluginMngrBase::GetBase(idx);

        assert(plugin_base != nullptr);

        plugin = dynamic_cast<T *>(plugin_base);

        m_plugins.push_back(plugin);
        m_map_plugin_short_name[plugin_base->GetShortName()] = plugin;
    }
    return result;
}

template<typename T>
PluginBase *PluginMngrBase_t<T>::GetBase(const std::wstring &short_name)
{
    return Get(short_name);
}

template<typename T>
PluginBase *PluginMngrBase_t<T>::GetBase(const std::string &short_name)
{
    return Get(short_name);
}

template<typename T>
T *PluginMngrBase_t<T>::Get(std::size_t index)
{
    if (index >= m_plugins.size())
        return nullptr;
    return m_plugins[index];
}

template<typename T>
T *PluginMngrBase_t<T>::Get(const std::wstring &short_name)
{
    typename std::map<std::wstring, T *>::iterator it;

    it = m_map_plugin_short_name.find(short_name);
    if (it == m_map_plugin_short_name.end())
        return nullptr;
    return it->second;
}

template<typename T>
T *PluginMngrBase_t<T>::Get(const std::string &short_name)
{
    std::wstring wshort_name = ::boost::locale::conv::utf_to_utf<wchar_t>(short_name);
    T *result;

    result = Get(wshort_name);
    return result;
}

}




