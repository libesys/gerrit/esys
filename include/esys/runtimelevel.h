/*!
 * \file esys/runtimelevel.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

/*! \class RuntimeLevel esys/runtimelevel.h "esys/runtimelevel.h"
 *  \brief Definition of runtime level
 */
class ESYS_API RuntimeLevel
{
public:
    //! Constructor
    RuntimeLevel();

    //! Converting Constructor
    /*!
     *  \param[in] id The name of the Object
     */
    RuntimeLevel(const int32_t &id);

    enum : int32_t
    {
        NOT_SET=-1,  //!< Runtime level not set
        ALL=-2,      //!< Runtime level for all levels
        ROOT=0,     //!< Root runtime level used only by the SystemTask
        ESYS,     //!< Runtime level of Object in ESys library
        ESYSIO,   //!< Runtime level of Object in ESysIO library
        APP,      //!< Runtime level of Object in application
        LAST
    };

    operator int32_t() const;           //!< Implicit conversion to unsigned int

    RuntimeLevel &operator=(const RuntimeLevel &lvl);   //!< Assignment operator
    RuntimeLevel &operator++();                         //!< Prefix increment operator
    RuntimeLevel operator++(int increment);             //!< Postfix increment operator
    RuntimeLevel &operator--();                         //!< Prefix decrement operator
    RuntimeLevel operator--(int decrement);             //!< Postfix decrement operator

    //! Return the number of runtime levels
    /*!
     *  \return the number of runtime levels
     */
    static uint32_t GetCount();
//!< \cond DOXY_IMPL
protected:
    static uint32_t g_count;    //!< The number of runtime levels
    int32_t m_id;               //!< The value/id of the runtime level
//!< \endcond
};

}


