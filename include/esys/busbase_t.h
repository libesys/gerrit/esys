/*!
 * \file esys/busbase_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BUSBASE_T_H__
#define __ESYS_BUSBASE_T_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

template<class T>
class BusBase_t
{
public:
    BusBase_t();
    virtual ~BusBase_t();

    void SetNext(BusBase_t<T> *bus);
    BusBase_t<T> *GetNext();
protected:
    BusBase_t<T> *m_next;
};

template<class T>
BusBase_t<T>::BusBase_t(): m_next(NULL)
{
}

template<class T>
BusBase_t<T>::~BusBase_t()
{
}

template<class T>
void BusBase_t<T>::SetNext(BusBase_t<T> *bus)
{
    m_next=bus;
}

template<class T>
BusBase_t<T> *BusBase_t<T>::GetNext()
{
    return m_next;
}

}

#endif

