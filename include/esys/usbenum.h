/*!
 * \file esys/usbenum.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYSPROG_USBENUM_H__
#define __ESYSPROG_USBENUM_H__

#ifdef ARDUINO

#elif defined WIN32
#include <esys/win32/usbenum.h>
#elif defined __GNUC__ || defined __GNUG__
#include <esys/unix/usbenum.h>
#endif

#endif

