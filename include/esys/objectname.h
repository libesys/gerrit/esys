/*!
 * \file esys/objectname.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_OBJECTNAME_H__
#define __ESYS_OBJECTNAME_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/listitem.h"

#ifdef GetObject
#undef GetObject
#endif

namespace esys
{

class ESYS_API Object;

/*! \class ObjectName esys/objectname.h "esys/objectname.h"
 *  \brief The name of an Object or ObjectMngr and all their derivate classes
 *
 * This class is the essential piece to create automatically the tree of Object, which is then followed
 * by the static initialization and the runtime initialization. Automatically means in this context that
 * the only steps needed by the programmer is to define member variables of type Object or any derivate in a
 * class of type Object, ObjectMngr or any derivate to provide all the information or behavior needed to define
 * the Object tree.
 *
 * The magic is based on the lifetime of the ObjectName when passed to the constructor of an Object or any derivate.
 * All derivate class of Object must have the first parameter of all their constructor of type ObjectName. When a new
 * Object or derivate is instantiated, a name must be given to it and it must be a const char*. ObjectName has a
 * converting constructor which will instantiate a temporary ObjectName on the stack to name the Object or derivate, which
 * is about to be instantiated. The critical point here is to realize that the lifetime of the ObjectName is longer
 * than the execution of the construction of the Object or derivate. In other words, when the constructor of the Object
 * returns, its related temporary ObjectName on the stack is still alive, and its destructor hasn't been called yet.
 * In the destructor of ObjectName, any steps required to build the tree of Object can be performed on the just
 * instantiated Object or derivate, since we know by design that the Object is fully initialized.
 *
 * The next step is to build a recursion over those basic behaviors. When the destructor of an ObjectName anywhere
 * in the Object hierarchy is called, the previous Object, the parent of the current Object, was fully instantiated
 * by its constructor by design, but the current Object as well. It's then safe to access both parent and child Object
 * to create effectively this relation between both Objects, by calling AddChild on the parent and SetParent on
 * the child.
 *
 * With this procedure, when the constructor of any ObjectMngr or derivate returns, the complete Object tree has been
 * build using a temporary list of ObjectName created on the stack. Of course, it means that this list of ObjectName
 * doesn't use any memory after the constructor returns, since the stack has been unwound.
 */
class ESYS_API ObjectName: public ListItem<ObjectName>
{
public:
    //! Converting constructor
    /*!
     *  \param[in] name The name of the Object
     */
    ObjectName(const char *name);

    //! Default constructor
    /*!
     *  \param[in] name The name of the Object
     */
    ObjectName(const ObjectName &name);

    //! Destructor
    ~ObjectName();

    //! Return the actual name of the Object as a string
    /*!
     *  \return the actual name of the Object
     */
    const char *GetName() const;

    //! Conversion operator to return the actual name of the Object as a string
    /*!
     *  \return the actual name of the Object
     */
    operator const char *() const;

    //! Set the Object from which this ObjectName hold the name
    /*!
     *  \param[in] object the Object
     */
    void SetObject(Object *object);

    //! Return the Object from which this ObjectName hold the name
    /*!
     *  \return the Object which has its name given by this ObjectName
     */
    Object *GetObject() const;

    //! Set the current ObjectName
    /*!
     *  \param[in] current the current ObjectName
     */
    static void SetCurrent(ObjectName *current);

    //! Get the current ObjectName
    /*!
     *  \return the current ObjectName
     */
    static ObjectName *GetCurrent();
protected:
//!< \cond DOXY_IMPL
    static ObjectName *g_current;   //!< The current ObjectName
    const char *m_name;             //!< The actual name of the Object
    Object *m_object;               //!< The Object
//!< \endcond
};

}

#endif

