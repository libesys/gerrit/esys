/*!
 * \file esys/semaphorebase.h
 * \brief The Semaphore base class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2014-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/objectleaf.h"

namespace esys
{

/*! \class SemaphoreBase esys/semaphorebase.h "esys/semaphorebase.h"
 *  \brief Base class of all Semaphore implementation
 */
class ESYS_API SemaphoreBase : public ObjectLeaf
{
public:
    SemaphoreBase(const ObjectName &name);
    virtual ~SemaphoreBase();

    //! Increases the semaphore counter
    /*! \return 0 if successful, <0 otherwise
     */
    virtual int32_t Post(bool from_isr = false) = 0;

    //! Wait on the semaphore
    /*! \return 0 if successful, <0 otherwise
     */
    virtual int32_t Wait(bool from_isr = false) = 0;

    //! Wait on the semaphore for a given amount of time
    /*!
     * param[in] ms the time to wait in ms
     * \return 0 if successful, <0 otherwise
     */
    virtual int32_t WaitFor(uint32_t ms, bool from_isr = false) = 0;

    //! Same functionality as Wait on the semaphore, but never actually wait
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t TryWait(bool from_isr = false) = 0;

    //! Returns the semaphore count
    /*! \return the semaphore count
    */
    virtual uint32_t Count() = 0;
};

}


