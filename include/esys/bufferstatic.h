/*!
 * \file esys/bufferstatic.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BUFFERSTATIC_H__
#define __ESYS_BUFFERSTATIC_H__

#include "esys/esys_defs.h"
#include "esys/bufferwrap.h"

namespace esys
{

template<typename T, uint16_t N, uint8_t N_EXT_SEG=1>
class BufferStatic: public BufferWrapBase<T, N_EXT_SEG>
{
public:
    BufferStatic(): BufferWrapBase<T, N_EXT_SEG>(N, 0)
    {
    }

    BufferStatic(uint16_t size): BufferWrapBase<T>(N, size)
    {
    }

    virtual ~BufferStatic()
    {
    }

    virtual void SetData(T *p)
    {
        //m_p=p;
    }

    virtual void SetData(T *p, uint16_t size)
    {
        //m_p=p;

        //SetSize(size);
    }

protected:
    inline virtual T *GetData()
    {
        return m_p;
    }

    T m_p[N];
};

}

#endif



