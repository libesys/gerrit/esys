/*!
 * \file esys/pluginmngrcore_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/pluginmngrcore.h"
#include "esys/pluginbase.h"

#include <boost/locale.hpp>

namespace esys
{

template<typename T>
class PluginMngrCore_t : public PluginMngrCore
{
public:
    PluginMngrCore_t();
    virtual ~PluginMngrCore_t();

    virtual int Load() override;

    virtual PluginBase *GetBase(const std::string &short_name) override;

    virtual T *Get(std::size_t index);
    virtual T *Get(const std::string &short_name);
protected:
    std::vector<T *> m_plugins;
    std::map<std::string, T *> m_map_plugin_short_name;
};

template<typename T>
PluginMngrCore_t<T>::PluginMngrCore_t() : PluginMngrCore()
{
}

template<typename T>
PluginMngrCore_t<T>::~PluginMngrCore_t()
{
}

template<typename T>
int PluginMngrCore_t<T>::Load()
{
    int result;
    std::size_t idx;
    PluginBase *plugin_base;
    T *plugin;

    result = PluginMngrCore::Load();

    if (result < 0)
        return result;

    for (idx = 0; idx < GetNbr(); ++idx)
    {
        plugin_base = PluginMngrCore::GetBase(idx);

        assert(plugin_base != nullptr);

        plugin = dynamic_cast<T *>(plugin_base);

        m_plugins.push_back(plugin);
        m_map_plugin_short_name[plugin_base->GetShortName()] = plugin;
    }
    return result;
}

template<typename T>
PluginBase *PluginMngrCore_t<T>::GetBase(const std::string &short_name)
{
    return Get(short_name);
}

template<typename T>
T *PluginMngrCore_t<T>::Get(std::size_t index)
{
    if (index >= m_plugins.size())
        return nullptr;
    return m_plugins[index];
}

template<typename T>
T *PluginMngrCore_t<T>::Get(const std::string &short_name)
{
    typename std::map<std::string, T *>::iterator it;

    it = m_map_plugin_short_name.find(short_name);
    if (it == m_map_plugin_short_name.end())
        return nullptr;
    return it->second;
}

}




