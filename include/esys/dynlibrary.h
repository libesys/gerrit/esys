/*!
 * \file esys/dynlibrary.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_setup.h"

#if !defined(ESYS_DYNLIB_USE_WX) || defined(ESYS_DYNLIB_USE_BOOST)
#ifndef ESYS_DYNLIB_USE_BOOST
#define ESYS_DYNLIB_USE_BOOST
#endif
#include "esys/boost/dynlibrary.h"
#else
#ifndef ESYS_DYNLIB_USE_WX
#define ESYS_DYNLIB_USE_WX
#endif
#include "esys/wx/dynlibrary.h"
#endif



