/*!
 * \file esys/i2cbusbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_I2CBUSBASE_H__
#define __ESYS_I2CBUSBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/busbase_t.h"
#include "esys/i2ctrans_if.h"

namespace esys
{

class ESYS_API I2CBusBase: public BusBase_t<I2CBusBase>, public I2CTrans_if
{
public:
    I2CBusBase();
    virtual ~I2CBusBase();

protected:
};

}

#endif

