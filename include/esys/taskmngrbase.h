/*!
 * \file esys/taskmngrbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASKMNGRBASE_H__
#define __ESYS_TASKMNGRBASE_H__

#include "esys/esys_defs.h"
#include "esys/objectmngr.h"
#include "esys/taskbase.h"
#include "esys/tasklist.h"
#include "esys/taskmngrplatif.h"
#include "esys/mutex.h"
#include "esys/systemtask.h"

namespace esys
{

/*! \class TaskMngrBase esys/taskmngrbase.h "esys/taskmngrbase.h"
 *  \brief Base class of all TaskMngr
 *
 */
class ESYS_API TaskMngrBase : public ObjectMngr, public virtual TaskMngrPlatIf
{
public:
    friend class ESYS_API SystemTask;

    //! Constructor
    /*!
     *  \param[in] name The name of the TaskMngr
     */
    TaskMngrBase(const ObjectName &name);

    //! Destructor
    virtual ~TaskMngrBase();

    //! Function called to start the TaskMngr and all its Tasks
    /*! This function returns only when all application Tasks have terminated
     *
     *  \return <0 if an error occured, 0 otherwise
     */
    virtual int32_t Run();

    virtual void TaskDone(TaskBase *task);
    virtual void TaskStarted(TaskBase *task);

    virtual uint16_t GetNbrTasks(TaskType type = TaskType::APPLICATION);
    virtual uint16_t GetNbrActiveTasks(TaskType type = TaskType::APPLICATION);
    virtual TaskBase *GetTask(uint16_t idx, TaskType type = TaskType::APPLICATION);
    virtual TaskBase *GetFirstTask(TaskType type = TaskType::APPLICATION);

    //virtual void Done(TaskBase *task);

    virtual void Started(TaskBase *task);

    virtual int32_t Stopping(TaskBase *task);

    virtual void AllAppTasksDone();

    virtual int32_t Init();
    virtual int32_t Release();

    //virtual int32_t StartScheduler();
    //virtual void ExitScheduler();
    void SetSystemTask(SystemTask *system_task);
    SystemTask *GetSystemTask();
protected:
//!< \cond DOXY_IMPL
    enum class Tasks
    {
        ALL,
        ALL_NOT_SYSTEM,
        ONLY_SYSTEM
    };

    class FunctorBase
    {
    public:
        FunctorBase(bool do_set_task_mngr = false) : m_do_set_task_mngr(do_set_task_mngr)
        {
        }

        virtual int32_t CallFct(/*TaskPlatIf *task, */TaskMngrBase *taskmngr_base, Tasks tasks=Tasks::ALL) = 0;

        virtual void SetTask(TaskBase *task) = 0;

        void SetDoSetTaskMngr(bool do_set_task_mngr)
        {
            m_do_set_task_mngr = do_set_task_mngr;
        }
        bool GetDoSetTaskMngr()
        {
            return m_do_set_task_mngr;
        }
    protected:
        bool m_do_set_task_mngr;
    };

    template<typename T>
    class FunctorTaskBaseFct: public FunctorBase
    {
    public:
        typedef int32_t(T::*TaskFct)();
        FunctorTaskBaseFct(TaskFct fct=nullptr, bool do_set_task_mngr = false) : FunctorBase(do_set_task_mngr), m_fct(fct)
        {
        }

        virtual void SetTask(TaskBase *task) override
        {
            m_task = task;
        }
        virtual int32_t CallFct(/*TaskPlatIf *task, */TaskMngrBase *taskmngr_base, Tasks tasks = Tasks::ALL) override
        {
            /*bool call_it = false;

            if (tasks == Tasks::ALL)
                call_it = true;
            else
            {
                switch (tasks)
                {
                    case Tasks::ALL_NOT_SYSTEM:
                        if (task->GetRuntimeLevel() != RuntimeLevel::ROOT)
                            call_it = true;
                        break;
                    case Tasks::ONLY_SYSTEM:
                        if (task->GetRuntimeLevel() == RuntimeLevel::ROOT)
                            call_it = true;
                        break;
                    default:
                        call_it = true;
                }
            }
            if (call_it == true)
            {
                if (GetDoSetTaskMngr() == true)
                    task->SetMngrBase(taskmngr_base);
                return (task->*m_fct)();
            }
            return 0;*/
            /*if (GetDoSetTaskMngr() == true)
                m_task->GetTaskBase()->SetMngrBase(taskmngr_base); */
            return (m_task->*m_fct)();
        }
    protected:
        TaskFct m_fct;
        T *m_task;
    };

    virtual int32_t CreateAll();
    virtual int32_t StartAll();
    virtual int32_t StopAll(Tasks task);

    int32_t DoOnAll(FunctorBase &task_fctor, Tasks tasks = Tasks::ALL);
    int32_t DoOnAll(FunctorBase &task_fctor, TaskType type, Tasks tasks = Tasks::ALL);
    TaskList &GetTaskList();

    TaskList *m_task_list;
    FunctorTaskBaseFct<TaskPlatIf> m_functor_create_all;
    FunctorTaskBaseFct<TaskPlatIf> m_functor_start_all;
    FunctorTaskBaseFct<TaskBase> m_functor_stop_all;
    Mutex m_mutex;
    SystemTask *m_system_task = nullptr;
//!< \endcond
};

}

#endif
