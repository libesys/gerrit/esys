/*!
 * \file esys/objectnode.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_OBJECTNODE_H__
#define __ESYS_OBJECTNODE_H__

#include "esys/object.h"
#include "esys/treeitem.h"

namespace esys
{

/*! \class ObjectNode esys/objectnode.h "esys/objectnode.h"
 *  \brief Object which can have children Object
 *
 *
 */
class ESYS_API ObjectNode: public Object, public TreeItem<Object>
{
public:
    ObjectNode(const ObjectName &name);
    virtual ~ObjectNode();
};

}

#endif


