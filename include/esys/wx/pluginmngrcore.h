/*!
 * \file esys/wx/pluginmngrcore.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/pluginbase.h"
#include "esys/pluginmngrbase.h"
#include "esys/dynlibrary.h"

namespace esys
{

namespace wx
{

class PluginMngrImplHelper
{
public:
    PluginMngrImplHelper();
    virtual ~PluginMngrImplHelper();

    void SetDynLib(DynLibrary *dyn_lib);
    DynLibrary *GetDynLib();

    void SetEntryPoint(void *entry);
    void *GetEntryPoint();

    void SetPlugin(PluginBase *plugin);
    PluginBase *GetPlugin();
protected:
    DynLibrary *m_dyn_lib = nullptr;      //!< Pointer to the object implementing the dynamic loading
    void *m_entry_fct = nullptr;                //!< The entry point giving access to the plugin
    PluginBase *m_plugin = nullptr;             //!< Pointer to the plugin
};

class ESYS_API PluginMngrCore: public PluginMngrBase
{
public:
    PluginMngrCore();
    virtual ~PluginMngrCore();

    virtual int Load() override;                                 //!< Load the plugins
    virtual int Release() override;                              //!< Release the plugins
    virtual std::size_t GetNbr() override;                       //!< Return the number of loaded plugins
    virtual PluginBase *GetBase(std::size_t index) override;         //!< Return the plugin with a given index

protected:
    std::vector<PluginMngrImplHelper *> m_plugins;
};

}

#ifdef ESYS_PLUGIN_USE_WX
using namespace wx;
#endif

}




