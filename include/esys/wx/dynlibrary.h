/*!
 * \file esys/wx/dynlibrary.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/dynlibrarybase.h"

namespace esys
{

namespace wx
{

class ESYS_API DynLibraryImpl;

class ESYS_API DynLibrary: public DynLibraryBase
{
public:
    DynLibrary();
    virtual ~DynLibrary();

    virtual int Load(const std::wstring &filename) override;
    virtual int UnLoad() override;
    virtual bool IsLoaded() override;
    virtual bool HasSymbol(const std::wstring& name) override;
    virtual void *GetSymbol(const std::wstring& name) override;
protected:
    DynLibraryImpl *m_impl = nullptr;
};

}

#ifdef ESYS_DYNLIB_USE_WX
using namespace wx;
#endif

}
