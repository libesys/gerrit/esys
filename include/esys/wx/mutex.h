/*!
 * \file esys/wx/mutex.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WX_MUTEX_H__
#define __ESYS_WX_MUTEX_H__

#include "esys/esys_defs.h"
#include "esys/mutexbase.h"

class wxMutex;

namespace esys
{

namespace wx
{

class ESYS_API Mutex: public esys::MutexBase
{
public:
    Mutex(const ObjectName &name, Type type=DEFAULT);
    virtual ~Mutex();

    virtual int16_t Lock();
    virtual int16_t UnLock();
    virtual int16_t TryLock();

protected:
    wxMutex *m_mutex;
};

}

#ifdef ESYS_USING_PLAT_NS
using namespace wx;
#endif

}

#endif
