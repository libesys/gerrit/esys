/*!
 * \file esys/module.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MODULE_H__
#define __ESYS_MODULE_H__

#include "esys/esys_defs.h"
#include "esys/objectmngr.h"
#include "esys/listitem.h"

namespace esys
{

class ESYS_API SystemBase;
#ifdef ESYS_MULTI_PLAT
namespace mp
{
#else
namespace em
{
#endif
class ESYS_API ModuleList;
#ifdef ESYS_MULTI_PLAT
}
using namespace mp;
#else
}
using namespace em;
#endif

/*! \class Object esys/module.h "esys/module.h"
 *  \brief Module class
 */
class ESYS_API Module : public ListItem<Module>, public ObjectMngr
{
public:
    //! Constructor
    /*!
     *  \param[in] name The name of the Module
     *  \param[in] order_id the order id of the module
     */
    Module(const ObjectName &name, int32_t order_id = -1);

    //! Destructor
    virtual ~Module();

    //! Return the order id of the module
    /*!
     *  \return the order id of the module
     */
    int32_t GetOrderId();

    //! Set the order id of the module
    /*!
     *  \param[in] order_id the order id of the module
     */
    void SetOrderId(int32_t order_id);

    //! Return SystemBase
    /*!
     *  \return the SystemBase
     */
    SystemBase *GetSystemBase();

    //! Set the SystemBase
    /*!
     *  \param[in] system_base set the SystemBase
     */
    void SetSystemBase(SystemBase *system_base);

    //! Set the next module in the list
    /*!
     *  \param[in] next the next module
     */
    void SetNext(Module *next);

    //! Set the previous module in the list
    /*!
     *  \param[in] prev the previous module
     */
    void SetPrev(Module *prev);

    //! Return the next Module
    /*!
     *  \return the next Module
     */
    Module *GetNext();

    //! Return the previous Module
    /*!
     *  \return the previous Module
     */
    Module *GetPrev();
protected:
//!< \cond DOXY_IMPL
    int32_t m_order_id;
    SystemBase *m_system_base;
    static ModuleList &s_list_module;
//!< \endcond
};

}

#endif

