/*!
 * \file esys/nfc/desfireapp_tt.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIREAPP_TT_H__
#define __ESYS_NFC_DESFIREAPP_TT_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/nfc/desfireappbase.h"
#include "esys/nfc/desfirefile.h"

#include "esys/assert.h"

namespace esys
{

template <int N_SUP, int N=1>
class DesFireApp_tt: public virtual esys::DesFireAppBase
{
public:
    DesFireApp_tt();
    virtual ~DesFireApp_tt();

    virtual uint16_t GetNbrFiles();
    virtual DesFireFileBase *GetFileIdx(uint16_t idx);
    virtual DesFireFileBase *GetFile(uint8_t id);

    virtual int AddSupportedFile(DesFireFileBase *file);
    virtual int32_t GetNbrSupportedFiles();
    virtual int32_t GetMaxNbrSupportedFiles();
    virtual bool IsSupportedFile(uint32_t file_id, DesFireFileBase **file=NULL);

    virtual void Clear();

    virtual void SetDesFire(esys::DesFireBase *desfire);
protected:
    virtual void AddFile(uint8_t file_id);

    DesFireFile m_files[N];
    uint16_t m_nbr_files;
    pDesFireFileBase m_supported_files[N_SUP];
    uint16_t m_nbr_supported_files;
};

template <int N_SUP, int N>
DesFireApp_tt<N_SUP, N>::DesFireApp_tt(): DesFireAppBase(), m_nbr_files(0), m_nbr_supported_files(0)
{
    if (this->GetDesFire()!=NULL)
        SetDesFire(this->GetDesFire());
}

template <int N_SUP, int N>
DesFireApp_tt<N_SUP, N>::~DesFireApp_tt()
{
}

template <int N_SUP, int N>
void DesFireApp_tt<N_SUP, N>::SetDesFire(esys::DesFireBase *desfire)
{
    uint16_t idx;

    DesFireAppBase::SetDesFire(desfire);

    for (idx=0; idx<N; ++idx)
    {
        m_files[idx].SetDesFire(desfire);
    }

    for (idx=0; idx<m_nbr_supported_files; ++idx)
    {
        m_supported_files[idx]->SetDesFire(desfire);
    }
}

template <int N_SUP, int N>
uint16_t DesFireApp_tt<N_SUP, N>::GetNbrFiles()
{
    return m_nbr_files+m_nbr_supported_files;
}

template <int N_SUP, int N>
DesFireFileBase *DesFireApp_tt<N_SUP, N>::GetFileIdx(uint16_t idx)
{
    if (idx>=GetNbrFiles())
        return NULL;
    if (idx<m_nbr_files)
        return &m_files[idx];
    else if ((idx-m_nbr_files)<m_nbr_supported_files)
        return m_supported_files[idx-m_nbr_files];
    else
        return NULL;
}

template <int N_SUP, int N>
DesFireFileBase *DesFireApp_tt<N_SUP, N>::GetFile(uint8_t id)
{
    uint16_t idx;
    DesFireFileBase *file;

    for (idx=0; idx<GetNbrFiles(); ++idx)
    {
        file=GetFileIdx(idx);
        assert(file!=NULL);
        if (file->GetID()==id)
            return file;
    }
    return NULL;
}

template <int N_SUP, int N>
int DesFireApp_tt<N_SUP, N>::AddSupportedFile(DesFireFileBase *file)
{
    if (m_nbr_supported_files>=N_SUP)
        return -1;
    m_supported_files[m_nbr_supported_files++]=file;
    file->SetDesFire(this->GetDesFire());
    return 0;
}

template <int N_SUP, int N>
int32_t DesFireApp_tt<N_SUP, N>::GetNbrSupportedFiles()
{
    return m_nbr_supported_files;
}

template <int N_SUP, int N>
int32_t DesFireApp_tt<N_SUP, N>::GetMaxNbrSupportedFiles()
{
    return N_SUP;
}

template <int N_SUP, int N>
bool DesFireApp_tt<N_SUP, N>::IsSupportedFile(uint32_t file_id, DesFireFileBase **file)
{
    uint16_t idx;

    for (idx=0; idx<GetNbrSupportedFiles(); ++idx)
    {
        if (m_supported_files[idx]->GetID()==file_id)
        {
            if (file!=NULL)
                *file=m_supported_files[idx];
            return true;
        }
    }
    if (file!=NULL)
        *file=NULL;
    return false;
}

template <int N_SUP, int N>
void DesFireApp_tt<N_SUP, N>::Clear()
{
    uint16_t idx;

    m_nbr_files=0;

    for (idx=0; idx<N; ++idx)
    {
        m_files[idx].SetPresent(false);
    }

    for (idx=0; idx<m_nbr_supported_files; ++idx)
    {
        m_supported_files[idx]->SetPresent(false);
    }
}

template <int N_SUP, int N>
void DesFireApp_tt<N_SUP, N>::AddFile(uint8_t file_id)
{
    if (m_nbr_files>=N)
        return;
    m_files[m_nbr_files].SetID(file_id);
    m_files[m_nbr_files++].SetPresent(true);
}


}

#endif

