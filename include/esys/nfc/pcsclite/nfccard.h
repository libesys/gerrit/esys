/*!
 * \file esys/nfc/pcsclite/nfccard.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_PCSCLITE_NFCCARD_H__
#define __ESYS_NFC_PCSCLITE_NFCCARD_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <winscard.h>

#include <string>

namespace esys
{

class ESYS_API NFCCard
{
public:
    friend class ESYS_API NFCReader;

    NFCCard(const std::wstring &name=L"");
    NFCCard(NFCCard *card);
    virtual ~NFCCard();

    void SetName(const std::wstring &name);
    std::wstring &GetName();

    int Disconnect();

    int GetSerialNumber(uint8_t *buf, uint32_t &buf_size);
    int GetATS(uint8_t *buf, uint32_t &buf_size);
    int GetATQA(uint16_t &atqa);
    int Transmit(uint8_t *tx_buf, uint32_t tx_size,uint8_t *rx_buf, uint32_t &rx_size);
    int Discover();
protected:
    void SetHandle(SCARDHANDLE handle);
    bool GetHandle(SCARDHANDLE &handle);
    void InvalidateHandle();
    int GetAttrib(uint32_t attrib, uint8_t *buf, uint32_t &buf_size);


    std::wstring m_name;
    bool m_valid_handle;
    SCARDHANDLE m_handle;
    NFCCard *m_card;
};

}

#endif
