/*!
 * \file esys/nfc/hslcardapp.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_HSLCARDAPP_H__
#define __ESYS_NFC_HSLCARDAPP_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/desfireapp_t.h"
#include "esys/nfc/hslcardfileid.h"

namespace esys
{

class ESYS_API DesFireApp;

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4250 )
#endif

class ESYS_API HSLCardApp: public DesFireApp_t<1>
{
public:
    HSLCardApp();
    virtual ~HSLCardApp();

protected:
    HSLCardFileId m_hsl_card_file_id;
};

#ifdef _MSC_VER
#pragma warning( pop )
#endif

}

#endif
