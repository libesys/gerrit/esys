/*!
 * \file esys/nfc/desfirebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIREBASE_H__
#define __ESYS_NFC_DESFIREBASE_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/nfc/nfccard.h>
#include <esys/constnamed.h>

//#include <string>

namespace esys
{

class ESYS_API DesFireAppBase;

class ESYS_API DesFireBase: public NFCCard
{
public:
    typedef uint8_t Error;

    static const Error OPERATION_OK             = 0x00;
    static const Error NO_CHANGES               = 0x0C;
    static const Error OUT_OF_EEPROM_ERROR      = 0x0E;
    static const Error ILLEGAL_COMMAND_CODE     = 0x1C;
    static const Error INTEGRITY_ERROR          = 0x1E;
    static const Error NO_SUCH_KEY              = 0x40;
    static const Error LENGTH_ERROR             = 0x7E;
    static const Error PERMISSION_DENIED        = 0x9D;
    static const Error PARAMETER_ERROR          = 0x9E;
    static const Error APPLICATION_NOT_FOUND    = 0xA0;
    static const Error APPL_INTEGRITY_ERROR     = 0xA1;
    static const Error AUTHENTICATION_ERROR     = 0xAE;
    static const Error ADDITIONAL_FRAME         = 0xAF;
    static const Error BOUNDARY_ERROR           = 0xBE;
    static const Error PICC_INTEGRITY_ERROR     = 0xC1;
    static const Error COMMAND_ABORTED          = 0xCA;
    static const Error PICC_DISABLED_ERROR      = 0xCD;
    static const Error COUNT_ERROR              = 0xCE;
    static const Error DUPLICATE_ERROR          = 0xDE;
    static const Error EEPROM_ERROR             = 0xEE;
    static const Error FILE_NOT_FOUND           = 0xF0;
    static const Error FILE_INTEGRITY_ERROR     = 0xF1;

    DesFireBase(NFCCard *card=NULL);
    virtual ~DesFireBase();

    virtual void SetNFCCard(NFCCard *card);

    virtual uint16_t GetNbrAppIDs()=0;
    virtual int32_t GetAppID(uint16_t idx)=0;
    virtual DesFireAppBase *GetAppIdx(uint16_t idx)=0;
    virtual DesFireAppBase *GetApp(uint32_t idx)=0;

    virtual int AddSupportedApp(DesFireAppBase *app)=0;
    virtual int32_t GetNbrSupportedApp()=0;
    virtual int32_t GetMaxNbrSupportedApp()=0;
    virtual bool IsSupportedApp(uint32_t app_id, DesFireAppBase **app=NULL)=0;

    int SelectApp(uint16_t idx);

    virtual void Clear()=0;

    // Desfire commands, their implementation is platform dependent
    virtual int GetAppIDs()=0;
    virtual int SelectAppID(uint32_t app_id)=0;

protected:
    virtual void AddAppIDs(uint8_t *buf, uint16_t size);
    virtual void AddAppID(uint32_t app_id)=0;
    NFCCard *m_card;
    //std::vector<DesFireApp *> m_apps;
};

typedef DesFireBase *pDesFireBase;

}

#endif
