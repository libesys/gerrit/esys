/*!
 * \file esys/nfc/hslcard.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_HSLCARD_H__
#define __ESYS_NFC_HSLCARD_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/desfire_t.h"
#include "esys/nfc/hslcardapp.h"
#include "esys/constnamed.h"

namespace esys
{

class ESYS_API DesFireApp;

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4250 )
#endif

class ESYS_API HSLCard: public DesFire_t<1>
{
public:
    HSLCard(NFCCard *card=NULL);
    virtual ~HSLCard();

protected:
    HSLCardApp m_hsl_card_app;
};

#ifdef _MSC_VER
#pragma warning( pop )
#endif

}

#endif
