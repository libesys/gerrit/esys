/*!
 * \file esys/nfc/desfirefilebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIREFILEBASE_H__
#define __ESYS_NFC_DESFIREFILEBASE_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

namespace esys
{

class ESYS_API DesFireBase;

class ESYS_API DesFireFileBase
{
public:
    //friend class ESYS_API DesFireApp;
    //friend class ESYS_API DesFireAppBase;

    DesFireFileBase();
    virtual ~DesFireFileBase();

    void SetID(uint8_t id);
    int16_t GetID();

    virtual int GetData(uint32_t offset, uint8_t *buf, uint16_t &buf_size)=0;

    void SetPresent(bool present=true);
    bool IsPresent();
public:
    void SetDesFire(esys::DesFireBase *desfire);
    esys::DesFireBase *GetDesFire();
protected:
    int16_t m_id;
    esys::DesFireBase *m_desfire;
    bool m_present;
};

typedef DesFireFileBase *pDesFireFileBase;

}

#endif
