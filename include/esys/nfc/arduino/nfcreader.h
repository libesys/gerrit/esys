/*!
 * \file esys/nfc/arduino/nfcreader.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_ARDUINO_NFCREADER_H__
#define __ESYS_NFC_ARDUINO_NFCREADER_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/nfcreaderbase.h"
#include "esys/nfc/arduino/nfccard.h"

#include <PN532.h>

#define PN532_CS 10

namespace esys
{

class ESYS_API NFCMngr;
class ESYS_API NFCCard;

class ESYS_API NFCReader
{
public:
    //NFCReader(const std::wstring &name=L"");
    NFCReader(const char *name="");
    virtual ~NFCReader();

    //void SetName(const std::wstring &name);
    //std::wstring &GetName();

    void SetMngr(NFCMngr *mngr);
    NFCMngr *GetMngr();

    int Init();

    int Connect();
    bool IsConnected();

    int Transmit(uint8_t *tx_buf, uint32_t tx_size,uint8_t *rx_buf, uint32_t &rx_size);

    NFCCard *ConnectCard();

    unsigned int GetNbrCards();
    NFCCard *GetCard(unsigned int idx);
    int RefreshCards();

    int WaitNotification(int32_t timeout, int32_t &size, uint8_t *atr);
    bool HasCard();

    PN532 &GetPN532();
protected:
    void Clear();

    //std::wstring m_name;
    bool m_connected;
    NFCMngr *m_mngr;
    //SCARDHANDLE m_card_hnd;
    unsigned long m_act_protocol;

    PN532 m_nfc;
    esys::NFCCard m_card;
};

}

#endif
