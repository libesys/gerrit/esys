/*!
 * \file esys/nfc/arduino/desfire.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_ARDUINO_DESFIRE_H__
#define __ESYS_NFC_ARDUINO_DESFIRE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/desfire_t.h"
#include "esys/nfc/arduino/desfirebase.h"

namespace esys
{

namespace ard
{

//#pragma warning( push )
//#pragma warning( disable : 4250 )

template <int N_SUP, int N=1, int APP_N_SUP=1, int APP_N=1>
class DesFire: public DesFireBase, public esys::DesFire_t<N_SUP, N, APP_N_SUP, APP_N>
{
public:
    DesFire(NFCCard *card=NULL);
    virtual ~DesFire();
};

//#pragma warning( pop )
template <int N_SUP, int N, int APP_N_SUP, int APP_N>
DesFire<N_SUP, N, APP_N_SUP, APP_N>::DesFire(NFCCard *card): esys::DesFire_t<N_SUP, N, APP_N_SUP, APP_N>()
{
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
DesFire<N_SUP, N, APP_N_SUP, APP_N>::~DesFire()
{
}

}

}

#endif
