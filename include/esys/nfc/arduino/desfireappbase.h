/*!
 * \file esys/nfc/arduino/desfireappbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_ARDUINO_DESFIREAPPBASE_H__
#define __ESYS_NFC_ARDUINO_DESFIREAPPBASE_H__

#include "esys/esys_defs.h"
#include "esys/nfc/desfireappbase.h"

namespace esys
{

namespace ard
{

class ESYS_API DesFireAppBase: public virtual esys::DesFireAppBase
{
public:
    DesFireAppBase();
    virtual ~DesFireAppBase();

    // DesFire commands
    virtual int GetFiles();
protected:
    uint8_t m_buf[64];
};

}

}

#endif
