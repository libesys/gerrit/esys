/*!
 * \file esys/nfc/arduino/desfirebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_ARDUINO_DESFIREBASE_H__
#define __ESYS_NFC_ARDUINO_DESFIREBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/desfirebase.h"
#include "esys/constnamed.h"

//#include <string>

namespace esys
{

namespace ard
{

class ESYS_API DesFireBase: public virtual esys::DesFireBase //public NFCCard
{
public:
    DesFireBase(NFCCard *card=NULL);
    virtual ~DesFireBase();

    virtual void SetNFCCard(NFCCard *card);

    virtual int GetAppIDs();
    virtual int SelectAppID(uint32_t app_id);
protected:
    uint8_t m_buf[64];
};

}

}

#endif
