/*!
 * \file esys/nfc/arduino/desfireapp.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_ARDUINO_DESFIREAPP_H__
#define __ESYS_NFC_ARDUINO_DESFIREAPP_H__

#include "esys/esys_defs.h"
#include "esys/nfc/desfireapp_t.h"
#include "esys/nfc/arduino/desfireappbase.h"

namespace esys
{

namespace ard
{

//#pragma warning( push )
//#pragma warning( disable : 4250 )
template <int N_SUP, int N=1>
class DesFireApp: public esys::DesFireApp_t<N_SUP, N>, public DesFireAppBase
{
public:
    DesFireApp();
    virtual ~DesFireApp();

};

//#pragma warning( pop )

template <int N_SUP, int N=1>
DesFireApp<N_SUP, N>::DesFireApp(): esys::DesFireApp_t<N_SUP, N>(), DesFireAppBase()
{

}

template <int N_SUP, int N=1>
DesFireApp<N_SUP, N>::~DesFireApp()
{
}

}

}

#endif
