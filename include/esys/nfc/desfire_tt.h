/*!
 * \file esys/nfc/desfire_tt.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIRE_TT_H__
#define __ESYS_NFC_DESFIRE_TT_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/desfirebase.h"
#include "esys/nfc/desfireapp_t.h"
#include "esys/constnamed.h"

#include "esys/assert.h"

namespace esys
{

template <int N_SUP, int N=1, int APP_N_SUP=1, int APP_N=1>
class DesFire_tt: public virtual esys::DesFireBase //public NFCCard
{
public:
    DesFire_tt(NFCCard *card=NULL);
    virtual ~DesFire_tt();

    //int GetAppIDs();
    virtual uint16_t GetNbrAppIDs();
    virtual int32_t GetAppID(uint16_t idx);
    virtual esys::DesFireAppBase *GetAppIdx(uint16_t idx);
    virtual esys::DesFireAppBase *GetApp(uint32_t idx);

    //int SelectApp(uint16_t idx);
    //int SelectAppID(uint32_t app_id);

    virtual int AddSupportedApp(esys::DesFireAppBase *app);
    virtual int32_t GetNbrSupportedApp();
    virtual int32_t GetMaxNbrSupportedApp();
    virtual bool IsSupportedApp(uint32_t app_id, esys::DesFireAppBase **app=NULL);

    virtual void Clear();
protected:
    virtual void AddAppID(uint32_t app_id);

    esys::DesFireApp_t<APP_N_SUP, APP_N> m_apps[N];
    int m_nbr_apps;
    pDesFireAppBase m_supported_apps[N_SUP];
    int m_nbr_sup_apps;
};

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::DesFire_tt(esys::NFCCard *card): DesFireBase(card), m_nbr_apps(0), m_nbr_sup_apps(0)
{
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::~DesFire_tt()
{
    Clear();
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
uint16_t DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::GetNbrAppIDs()
{
    return m_nbr_apps+m_nbr_sup_apps;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
int32_t DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::GetAppID(uint16_t idx)
{
    esys::DesFireAppBase *app;

    app=GetAppIdx(idx);

    if (app==NULL)
        return -1;
    else
        return app->GetID();
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
esys::DesFireAppBase *DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::GetAppIdx(uint16_t idx)
{
    if (idx>=GetNbrAppIDs())
        return NULL;
    if (idx<m_nbr_apps)
        return &m_apps[idx];
    else if ((idx-m_nbr_apps)<m_nbr_sup_apps)
        return m_supported_apps[idx-m_nbr_apps];
    else
        return NULL;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
esys::DesFireAppBase *DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::GetApp(uint32_t app_id)
{
    uint16_t idx;
    esys::DesFireAppBase *app;
    uint16_t nbr_apps=GetNbrAppIDs();

    for (idx=0; idx<nbr_apps; ++idx)
    {
        app=GetAppIdx(idx);
        assert(app!=NULL);

        if (app->GetID()==app_id)
            return app;
    }
    return NULL;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
int DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::AddSupportedApp(esys::DesFireAppBase *app)
{
    if (m_nbr_sup_apps>=N_SUP)
        return -1;

    app->SetDesFire(this);
    m_supported_apps[m_nbr_sup_apps++]=app;
    return 0;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
int32_t DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::GetNbrSupportedApp()
{
    return m_nbr_sup_apps;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
int32_t DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::GetMaxNbrSupportedApp()
{
    return N_SUP;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
bool DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::IsSupportedApp(uint32_t app_id, esys::DesFireAppBase **app)
{
    uint16_t idx;
    esys::DesFireAppBase *app_;

    for (idx=0; idx<m_nbr_sup_apps; ++idx)
    {
        app_=m_supported_apps[idx];
        assert(app!=NULL);

        if (app_id==app_->GetID())
        {
            if (app!=NULL)
                *app=app_;
            return true;
        }
    }
    if (app!=NULL)
        *app=NULL;
    return false;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
void DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::Clear()
{
    uint16_t idx;

    for (idx=0; idx<m_nbr_apps; ++idx)
    {
        m_apps[idx].SetPresent(false);
    }
    m_nbr_apps=0;
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
void DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>::AddAppID(uint32_t app_id)
{
    if (m_nbr_apps<N)
    {
        m_apps[m_nbr_apps].SetID(app_id);
        m_apps[m_nbr_apps++].SetPresent();
    }
}

}

#endif
