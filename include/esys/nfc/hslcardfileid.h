/*!
 * \file esys/nfc/hslcardfileid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_HSLCARDFILEID_H__
#define __ESYS_NFC_HSLCARDFILEID_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/desfirefile.h"

namespace esys
{

class ESYS_API HSLCardFileId: public DesFireFile
{
public:
    HSLCardFileId();
    virtual ~HSLCardFileId();

protected:
    char m_id[20];
    //HSLCardFileId m_hsl_card_file_id;
};

}

#endif
