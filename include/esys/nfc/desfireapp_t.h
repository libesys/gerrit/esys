/*!
 * \file esys/nfc/desfireapp_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIREAPP_T_H__
#define __ESYS_NFC_DESFIREAPP_T_H__

#if defined WIN32
#include "esys/nfc/win32/desfireapp_t.h"
namespace esys
{
using namespace win32;
}
#elif defined ARDUINO
#include "esys/nfc/arduino/desfireapp_t.h"
namespace esys
{
using namespace ard;
}
#endif

#endif
