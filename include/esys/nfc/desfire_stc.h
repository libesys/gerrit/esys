/*!
 * \file esys/nfc/desfire.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIRE_STC_H__
#define __ESYS_NFC_DESFIRE_STC_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

//#include <esys/nfc/nfccard.h>
#include "esys/nfc/desfirebase.h"
#include "esys/constnamed.h"

//#include <string>

namespace esys
{

class ESYS_API DesFire_stc: public virtual DesFireBase //public NFCCard
{
public:
    DesFire_stc(NFCCard *card=NULL);
    virtual ~DesFire_stc();

    virtual uint16_t GetNbrAppIDs();
    virtual int32_t GetAppID(uint16_t idx);
    virtual esys::DesFireAppBase *GetAppIdx(uint16_t idx);
    virtual esys::DesFireAppBase *GetApp(uint32_t idx);

    virtual int AddSupportedApp(esys::DesFireAppBase *app);
    virtual int32_t GetNbrSupportedApp();
    virtual int32_t GetMaxNbrSupportedApp();
    virtual bool IsSupportedApp(uint32_t app_id, esys::DesFireAppBase **app=NULL);

    virtual void Clear();
protected:
    virtual void AddAppID(uint32_t app_id);

    std::vector<esys::DesFireAppBase *> m_apps;
    std::vector<esys::DesFireAppBase *> m_supported_apps;
};

}

#endif
