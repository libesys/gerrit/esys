/*!
 * \file esys/nfc/desfireappbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIREAPPBASE_H__
#define __ESYS_NFC_DESFIREAPPBASE_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

class ESYS_API DesFireBase;
class ESYS_API DesFireFileBase;

class ESYS_API DesFireAppBase
{
public:
    //friend class ESYS_API esys::DesFireBase;
    //friend class ESYS_API DesFire;
    //template <int N_SUP, int N, int APP_N_SUP, int APP_N> friend class DesFire_t;

    DesFireAppBase();
    virtual ~DesFireAppBase();

    void SetID(uint32_t id);
    int32_t GetID();

    // DesFire commands
    virtual int GetFiles()=0;


    virtual int Select();

    // Internal data access functions
    virtual uint16_t GetNbrFiles()=0;
    virtual DesFireFileBase *GetFileIdx(uint16_t idx)=0;
    virtual DesFireFileBase *GetFile(uint8_t id)=0;

    virtual int AddSupportedFile(DesFireFileBase *file)=0;
    virtual int32_t GetNbrSupportedFiles()=0;
    virtual int32_t GetMaxNbrSupportedFiles()=0;
    virtual bool IsSupportedFile(uint32_t file_id, DesFireFileBase **file=NULL)=0;

    virtual void Clear()=0;
    bool IsPresent();
    void SetPresent(bool present=true);
public:
    virtual void SetDesFire(esys::DesFireBase *desfire);
    esys::DesFireBase *GetDesFire();
protected:
    virtual void AddFiles(uint8_t *buf, uint16_t size);
    virtual void AddFile(uint8_t file_id)=0;

    int32_t m_id;
    bool m_present;
    //std::vector<DesFireFile *> m_files;
    esys::DesFireBase *m_desfire;
};

typedef DesFireAppBase *pDesFireAppBase;

}

#endif
