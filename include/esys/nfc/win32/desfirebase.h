/*!
 * \file esys/nfc/win32/desfirebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_DESFIREBASE_H__
#define __ESYS_NFC_WIN32_DESFIREBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

//#include <esys/nfc/nfccard.h>
#include "esys/nfc/desfirebase.h"
#include "esys/constnamed.h"

//#include <string>

namespace esys
{

//class ESYS_API DesFireApp;

namespace win32
{

class ESYS_API DesFireBase: public virtual esys::DesFireBase //public NFCCard
{
public:
    DesFireBase(NFCCard *card=NULL);
    virtual ~DesFireBase();

    virtual int GetAppIDs();
    virtual int SelectAppID(uint32_t app_id);
};

}

}

#endif
