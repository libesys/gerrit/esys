/*!
 * \file esys/nfc/win32/desfireapp.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_DESFIREAPP_H__
#define __ESYS_NFC_WIN32_DESFIREAPP_H__

#include "esys/esys_defs.h"
#include "esys/nfc/desfireapp_stc.h"
#include "esys/nfc/win32/desfireappbase.h"

namespace esys
{

namespace win32
{

#pragma warning( push )
#pragma warning( disable : 4250 )

class ESYS_API DesFireApp: public esys::DesFireApp_stc, public DesFireAppBase
{
public:
    DesFireApp();
    virtual ~DesFireApp();

};

#pragma warning( pop )

}

}

#endif
