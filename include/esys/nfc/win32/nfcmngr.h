/*!
 * \file esys/nfc/win32/nfcmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_NFCMNGR_H__
#define __ESYS_NFC_WIN32_NFCMNGR_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>
#include <esys/nfc/nfcmngrbase.h>

#include <vector>

#include "winscard.h"

namespace esys
{

class ESYS_API NFCReader;

class ESYS_API NFCMngr
{
public:
    friend class ESYS_API NFCReader;

    NFCMngr();
    virtual ~NFCMngr();

    virtual int Init();

    virtual unsigned int GetNbrReaders();
    virtual NFCReader *GetReader(unsigned int idx);
protected:
    void Clear();
    SCARDCONTEXT &GetContext();

    bool m_init;
    bool m_released;
    SCARDCONTEXT m_context;
    std::vector<NFCReader *> m_readers;
};

}

#endif
