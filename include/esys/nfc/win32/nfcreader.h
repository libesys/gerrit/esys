/*!
 * \file esys/nfc/win32/nfcreader.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/nfc/nfcreaderbase.h>

#include <string>
#include <vector>

#include <winscard.h>

namespace esys
{

class ESYS_API NFCMngr;
class ESYS_API NFCCard;

class ESYS_API NFCReader
{
public:
    NFCReader(const std::string &name = "");
    virtual ~NFCReader();

    void SetName(const std::string &name);
    std::string &GetName();

    void SetMngr(NFCMngr *mngr);
    NFCMngr *GetMngr();

    int Connect();
    bool IsConnected();

    int Transmit(uint8_t *tx_buf, uint32_t tx_size, uint8_t *rx_buf, uint32_t &rx_size);

    NFCCard *ConnectCard();

    unsigned int GetNbrCards();
    NFCCard *GetCard(unsigned int idx);
    int RefreshCards();

    int WaitNotification(int32_t timeout, int32_t &size, uint8_t *atr);
    bool HasCard();

protected:
    void Clear();

    std::string m_name;
    bool m_connected;
    NFCMngr *m_mngr;
    SCARDHANDLE m_card_hnd;
    unsigned long m_act_protocol;
    std::vector<NFCCard *> m_cards;
    SCARD_READERSTATE m_reader_states[1];
};

} // namespace esys
