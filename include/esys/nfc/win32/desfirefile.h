/*!
 * \file esys/nfc/win32/desfirefile.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_DESFIREFILE_H__
#define __ESYS_NFC_WIN32_DESFIREFILE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/nfc/desfirefilebase.h"

namespace esys
{

namespace win32
{

class ESYS_API DesFireFile: public DesFireFileBase
{
public:
    DesFireFile();
    virtual ~DesFireFile();

    virtual int GetData(uint32_t offset, uint8_t *buf, uint16_t &buf_size);

};

}

}

#endif
