/*!
 * \file esys/nfc/win32/desfire.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_DESFIRE_H__
#define __ESYS_NFC_WIN32_DESFIRE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/nfc/desfire_stc.h"
#include "esys/nfc/win32/desfirebase.h"

namespace esys
{

namespace win32
{

#pragma warning( push )
#pragma warning( disable : 4250 )

class ESYS_API DesFire: public DesFireBase, public esys::DesFire_stc
{
public:
    DesFire(NFCCard *card=NULL);
    virtual ~DesFire();
};

#pragma warning( pop )

}

}

#endif
