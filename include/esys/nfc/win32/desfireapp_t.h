/*!
 * \file esys/nfc/win32/desfireapp_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_DESFIREAPP_T_H__
#define __ESYS_NFC_WIN32_DESFIREAPP_T_H__

#include "esys/esys_defs.h"
#include "esys/nfc/desfireapp_tt.h"
#include "esys/nfc/win32/desfireappbase.h"

#include "esys/assert.h"

namespace esys
{

namespace win32
{

#pragma warning( push )
#pragma warning( disable : 4250 )

template <int N_SUP, int N=1>
class DesFireApp_t: public esys::DesFireApp_tt<N_SUP, N>, public esys::win32::DesFireAppBase
{
public:
    DesFireApp_t();
    virtual ~DesFireApp_t();

};

#pragma warning( pop )

template <int N_SUP, int N>
DesFireApp_t<N_SUP, N>::DesFireApp_t(): esys::DesFireApp_tt<N_SUP, N>(), esys::win32::DesFireAppBase()
{
}

template <int N_SUP, int N>
DesFireApp_t<N_SUP, N>::~DesFireApp_t()
{
}

}

}

#endif
