/*!
 * \file esys/nfc/win32/desfireappbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_DESFIREAPPBASE_H__
#define __ESYS_NFC_WIN32_DESFIREAPPBASE_H__

#include "esys/esys_defs.h"
#include "esys/nfc/desfireappbase.h"

namespace esys
{

namespace win32
{

class ESYS_API DesFireAppBase: public virtual esys::DesFireAppBase
{
public:
    /*friend class ESYS_API DesFireBase;
    friend class ESYS_API DesFire;
    template <int N_SUP, int N, int APP_N_SUP, int APP_N> friend class DesFire_t; */

    DesFireAppBase();
    virtual ~DesFireAppBase();

    // DesFire commands
    virtual int GetFiles();

};

}

}

#endif
