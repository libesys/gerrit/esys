/*!
 * \file esys/nfc/win32/desfire_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_WIN32_DESFIRE_T_H__
#define __ESYS_NFC_WIN32_DESFIRE_T_H__

#include "esys/esys_defs.h"
#include "esys/nfc/desfire_tt.h"
#include "esys/nfc/win32/desfire.h"

#include "esys/assert.h"

namespace esys
{

namespace win32
{

#pragma warning( push )
#pragma warning( disable : 4250 )

template <int N_SUP, int N=1, int APP_N_SUP=1, int APP_N=1>
class DesFire_t: public esys::DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>, public esys::win32::DesFireBase
{
public:
    DesFire_t(NFCCard *card=NULL);
    virtual ~DesFire_t();

};

#pragma warning( pop )

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
DesFire_t<N_SUP, N, APP_N_SUP, APP_N>::DesFire_t(esys::NFCCard *card): esys::DesFire_tt<N_SUP, N, APP_N_SUP, APP_N>(card), DesFireBase(card)
{
}

template <int N_SUP, int N, int APP_N_SUP, int APP_N>
DesFire_t<N_SUP, N, APP_N_SUP, APP_N>::~DesFire_t()
{
}

}

}

#endif
