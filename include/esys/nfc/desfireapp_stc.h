/*!
 * \file esys/nfc/desfireapp_stc.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_DESFIREAPP_STC_H__
#define __ESYS_NFC_DESFIREAPP_STC_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/nfc/desfireappbase.h"

#include <vector>

namespace esys
{

//class ESYS_API DesFire;
class ESYS_API DesFireFileBase;

class ESYS_API DesFireApp_stc: public virtual esys::DesFireAppBase
{
public:
//    friend class ESYS_API DesFire;

    DesFireApp_stc();
    virtual ~DesFireApp_stc();

    virtual uint16_t GetNbrFiles();
    virtual DesFireFileBase *GetFileIdx(uint16_t idx);
    virtual DesFireFileBase *GetFile(uint8_t id);

    virtual int AddSupportedFile(DesFireFileBase *file);
    virtual int32_t GetNbrSupportedFiles();
    virtual int32_t GetMaxNbrSupportedFiles();
    virtual bool IsSupportedFile(uint32_t file_id, DesFireFileBase **file=NULL);

    virtual void Clear();

protected:
    //void SetDesFire(DesFire *desfire);
    //DesFire *GetDesFire();
    //virtual void AddFiles(uint8_t *buf, uint16_t size);
    virtual void AddFile(uint8_t file_id);
    //int32_t m_id;
    std::vector<DesFireFileBase *> m_files;
    std::vector<DesFireFileBase *> m_supported_files;
    //DesFire *m_desfire;
};

}

#endif
