/*!
 * \file esys/nfc/nfcreader.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_NFC_NFCREADER_H__
#define __ESYS_NFC_NFCREADER_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#ifdef ARDUINO

#elif defined WIN32
#include <esys/nfc/win32/nfcreader.h>
#elif defined __GNUC__ || defined __GNUG__
#include <esys/nfc/pcsclite/nfcreader.h>
#endif

#endif
