/*!
 * \file esys/buffer.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BUFFER_H__
#define __ESYS_BUFFER_H__

#include "esys/esys_defs.h"
#include "esys/assert.h"
#include "esys/inttypes.h"
#include "esys/bufferbase.h"
#include "esys/pointer.h"

namespace esys
{

template<typename T, uint8_t N_EXT_SEG=1>
class Buffer: public BufferBase
{
public:
    Buffer(uint16_t max_size=0, uint16_t size=0)
        : BufferBase(max_size, size), m_use_scatter_gather(false), m_ext_segs_nbr(0)
    {
    }

    virtual ~Buffer()
    {
    }

    void UseScatterGather(bool use_scatter_gather)
    {
        m_use_scatter_gather=use_scatter_gather;
    }

    inline bool UseScatterGather()
    {
        return m_use_scatter_gather;
    }

    inline void ResetScatterGather()
    {
        m_ext_segs_nbr=0;
    }

    virtual uint16_t GetNbrSegment(bool data=true)=0;
    virtual T *GetSegment(uint16_t idx, uint16_t &size, bool data=true)=0;
    //virtual void WriteSegment(uint16_t idx, T *, uint16_t size, bool data=true)=0;

    //virtual Pointer<T> &operator++(int)=0;
    virtual T &operator[](uint16_t idx)=0;
    virtual T &operator()(uint16_t idx)=0;
    //virtual T &operator*()=0;

    virtual int16_t Push(T *data, uint16_t size)=0;
    virtual int16_t Pop(uint16_t size)=0;
    virtual void Set(const T &val)=0;
    virtual int16_t Set(const T &val, uint16_t nbr)=0;
    /*friend Pointer<T> &operator+(Pointer<T> &lhs,       // passing first arg by value helps optimize chained a+b+c
                                    const int16_t& rhs) // alternatively, both parameters may be const references.
    {
        uint16_t offset=lhs.GetOffset();

        offset+=rhs;
        lhs.SetOffset(offset);
        return lhs; // reuse compound assignment and return the result by value
    } */

    template<typename OBJ>
    inline void DoEachSeg(OBJ *obj, void (OBJ::*func)(T *buf, uint16_t size), bool data=true)
    {
        T *p;
        uint16_t size;
        uint16_t nbr_segs;

        nbr_segs=GetNbrSegment(data);

        for (uint16_t idx=0; idx<nbr_segs; ++idx)
        {
            p=GetSegment(idx, size, data);
            (*obj.*func)(p, size);
        }
    }

    template<typename OBJ>
    inline void DoEachSeg(OBJ &obj, void (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        DoEachSeg(&obj, func, data);
    }

    template<typename OBJ>
    inline int16_t DoEachSeg(OBJ *obj, int16_t (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        T *p;
        uint16_t size;
        int16_t result = 0;
        uint16_t nbr_segs;

        nbr_segs=GetNbrSegment(data);

        for (uint16_t idx=0; idx<nbr_segs; ++idx)
        {
            p=GetSegment(idx, size, data);
            result=(*obj.*func)(p, size);
            if (result<0)
                return result;
        }
        return result;
    }

    template<typename OBJ>
    inline int16_t DoEachSeg(OBJ &obj, int16_t (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        return DoEachSeg(&obj, func, data);
    }
    //virtual operator T()=0;
    //virtual Pointer<T> &operator=(const T &val)=0;
protected:
    struct ExtSegment
    {
        ExtSegment(): m_index(0), m_buf(nullptr), m_size(0)
        {
        }

        uint16_t m_index;
        T *m_buf;
        uint16_t m_size;
    };

    bool m_use_scatter_gather;
    ExtSegment m_ext_segs[N_EXT_SEG];
    static const uint8_t m_ext_segs_max_count=N_EXT_SEG;
    uint8_t m_ext_segs_nbr;
};

template<typename T>
Buffer<T> &memset(Buffer<T> &dst, const T &value, uint16_t size)
{
    uint16_t idx;

    assert(dst.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=value;
    return dst;
}

template<typename T>
Buffer<T> &memcpy(Buffer<T> &dst, Buffer<T> &src, uint16_t size)
{
    uint16_t idx;

    assert(dst.GetSize()>=size);
    assert(src.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=src[idx];
    return dst;
}

template<typename T>
Buffer<T> &memcpy(Buffer<T> &dst, T *src, uint16_t size)
{
    uint16_t idx;

    assert(dst.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=src[idx];
    return dst;
}

/*template<int N>
uint8_t *memcpy(uint8_t(&ar)[N], Buffer<uint8_t> &src, uint16_t size)
{
    return &ar[0];
} */

template<typename T>
T *memcpy(T *dst, Buffer<T> &src, uint16_t size)
{
    uint16_t idx;

    assert(src.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=src[idx];
    return dst;
}


/*template<typename T>
PointerStatic<T> &memset(PointerStatic<T> &dst, const T &value, uint16_t size) */

}

#endif
