/*!
 * \file esys/timercallbackbase.h
 * \brief The base class of all Timer Callbacks
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

namespace esys
{

class ESYS_API TimerBase;

/*! \class TimerCallbackBase esys/timercallbackbase.h "esys/timercallbackbase.h"
 *  \brief Base class of all Timer Callbacks
 */
class ESYS_API TimerCallbackBase
{
public:
    TimerCallbackBase();
    virtual ~TimerCallbackBase();

    virtual void SetTimer(TimerBase *timer);
    virtual TimerBase *GetTimer();

    virtual void Callback() = 0;
protected:
//!< \cond DOXY_IMPL
    TimerBase *m_timer;
//!< \endcond
};

}




