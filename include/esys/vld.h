/*!
 * \file esys/vld.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#if defined(WIN32) && !defined(ESYS_NO_VLD)
#include <vld.h>
#ifndef ESYS_USE_VLD
#define ESYS_USE_VLD
#endif
#endif

