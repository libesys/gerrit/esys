/*!
 * \file esys/serialportenumbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include <string>

namespace esys
{

class ESYS_API SerialPortEnumBase
{
public:
    SerialPortEnumBase();
    virtual ~SerialPortEnumBase();

    virtual int32_t Refresh() = 0;

    virtual uint32_t GetCount() = 0;

    virtual std::string GetPortName(uint32_t idx) = 0;
    
protected:

};

}

