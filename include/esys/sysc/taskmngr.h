/*!
 * \file esys/sysc/taskmngr.h
 * \brief Declaration of the SystemC TaskMngr class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_TASKMNGR_H__
#define __ESYS_SYSC_TASKMNGR_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/taskmngrbase.h"
#include "esys/sysc/taskmngrplat.h"

namespace esys
{

namespace sysc
{

/*! \class TaskMngr esys/sysc/taskmngr.h "esys/sysc/taskmngr.h"
 *  \brief SystemC TaskMngr class
 */
class ESYS_SYSC_API TaskMngr: public TaskMngrBase, public virtual TaskMngrPlat
{
public:
    TaskMngr(const ObjectName &name);
    virtual ~TaskMngr();
};

}

#ifdef ESYS_USING_PLAT_NS
using namespace sysc;
#endif

}

#endif








