/*!
 * \file esys/sysc/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_TASKPLAT_H__
#define __ESYS_SYSC_TASKPLAT_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/taskplatif.h"

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API Task;
class ESYS_SYSC_API TaskPlatImpl;
class ESYS_SYSC_API TaskMngrPlat;
class ESYS_SYSC_API TaskMngrPlatImpl;

/*! \class TaskPlat esys/sysc/taskplat.h "esys/sysc/taskplat.h"
 *  \brief
 */
class ESYS_SYSC_API TaskPlat: public esys::TaskPlatIf
{
public:
    TaskPlat(TaskBase *task_base);
    virtual ~TaskPlat();
    // Platform functions
    virtual int32_t Create() override;
    virtual int32_t Start() override;
    virtual int32_t Stop() override;
    virtual int32_t Kill() override;
    virtual int32_t Destroy() override;
    //virtual int32_t Done() override;

    virtual millis_t Millis() override;
    virtual void Sleep(millis_t ms) override;
    virtual void SleepU(micros_t us) override;

    virtual void SetTaskBase(TaskBase *task_base) override;

    //virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base)=0;
    //virtual void NotifyExit()=0;
    TaskPlatImpl *GetImpl();
    void SetTaskMngrBase(TaskMngrBase *taskmngr_base);

    //static TaskPlat &GetCurrent();
protected:
    Task *m_task = nullptr;
    TaskBase *m_task_base = nullptr;
    TaskPlatImpl *m_impl = nullptr;
    TaskMngrPlat *m_mngr = nullptr;
    TaskMngrPlatImpl *m_mngr_impl = nullptr;
};

}

}

#endif




