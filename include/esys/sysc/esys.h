/*!
 * \file esys/sysc/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/mp/esysbase.h"

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API ESys: public mp::ESysBase
{
public:
    ESys();
    virtual ~ESys();

    static ESys &Get();
    static void Select();

    virtual SemaphoreBase *NewSemaphoreBase(const ObjectName &name, uint32_t count) override;
    virtual MutexBase *NewMutexBase(const ObjectName &name, MutexBase::Type type = MutexBase::DEFAULT) override;
    virtual TaskPlatIf *NewTaskPlat(TaskBase *task_base) override;
    virtual TaskMngrPlatIf *NewTaskMngrPlat(TaskMngrBase *taskmngr_base) override;
    virtual SystemPlatIf *NewSystemPlat(SystemBase *system_base) override;
    virtual TimerBase *NewTimerBase(const ObjectName &name) override;

    virtual TaskPlatIf *GetCurTaskPlatIf() override;
protected:
    static ESys s_esys;
};

}

}


