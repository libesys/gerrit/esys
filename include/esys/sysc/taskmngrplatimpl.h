/*!
 * \file esys/esys/taskmngrplatimpl.h
 * \brief Declaration of the PIMPL class of the SystemC TaskMngrPlat
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_TASKMNGRPLATIMPL_H__
#define __ESYS_SYSC_TASKMNGRPLATIMPL_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/taskmngrbase.h"
#include "esys/semaphore.h"

#include <systemc.h>
#include <sysc/sc_simulation.h>

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API TaskMngr;

/*! \class TaskMngrPlatImpl esys/sysc/taskmngrplatimpl.h "esys/sysc/taskmngrplatimpl.h"
 *  \brief SystemC TaskMngr class
 */
class ESYS_SYSC_API TaskMngrPlatImpl: public sc_simulation
{
public:
    TaskMngrPlatImpl(TaskMngrBase *taskmngr_base);
    virtual ~TaskMngrPlatImpl();

    virtual int main() override;

    int32_t StartScheduler();
    void ExitScheduler();
    void Done(TaskBase *task_base);

    int32_t Init();

protected:
    TaskMngrBase *m_taskmngr_base;
};

}

}

#endif









