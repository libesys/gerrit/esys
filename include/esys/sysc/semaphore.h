/*!
 * \file esys/sysc/semaphore.h
 * \brief Declaration of the SystemC Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_SEMAPHORE_H__
#define __ESYS_SYSC_SEMAPHORE_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/semaphorebase.h"

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API SemaphoreImpl;

/*! \class Semaphore esys/sysc/semaphore.h "esys/sysc/semaphore.h"
 *  \brief SystemC Semaphore class
 */
class ESYS_SYSC_API Semaphore: public SemaphoreBase
{
public:
    Semaphore(const ObjectName &name, uint32_t count);
    virtual ~Semaphore();

    virtual int32_t Post(bool from_isr = false) override;
    virtual int32_t Wait(bool from_isr = false) override;
    virtual int32_t WaitFor(uint32_t ms, bool from_isr = false) override;
    virtual int32_t TryWait(bool from_isr = false) override;
    virtual uint32_t Count() override;

protected:
    SemaphoreImpl *m_impl;
};

}

#ifdef ESYS_USE_SYSC
using namespace sysc;
#endif

}

#endif


