/*!
 * \file esys/sysc/task.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_TASK_H__
#define __ESYS_SYSC_TASK_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/taskbase.h"
#include "esys/sysc/taskplat.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4250)
#endif

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API TaskMngr;

/*! \class Task esys/sysc/task.h "esys/sysc/task.h"
 *  \brief SystemC implementation of a scheduled Task
 */
class ESYS_SYSC_API Task: public esys::TaskBase, public virtual TaskPlat
{
public:
    Task(const esys::ObjectName &name, TaskType type);
    Task(const Task &) = delete;

    virtual ~Task();

    virtual void SetMngrBase(TaskMngrBase *taskmngr_base);
protected:

};

}

#ifdef ESYS_USE_SYSC
using namespace sysc;
#endif
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif




