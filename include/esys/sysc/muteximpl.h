/*!
 * \file esys/sysc/muteximpl.h
 * \brief Declaration of the SystemC Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_MUTEXIMPL_H__
#define __ESYS_SYSC_MUTEXIMPL_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/mutexbase.h"

#include <systemc.h>

namespace esys
{

namespace sysc
{

/*! \class MutexImpl esys/sysc/muteximpl.h "esys/sysc/muteximpl.h"
 *  \brief Boost Mutex class
 */
class ESYS_SYSC_API MutexImpl
{
public:
    MutexImpl(const char *name, esys::MutexBase::Type type = esys::MutexBase::DEFAULT);
    ~MutexImpl();
    int32_t Lock(bool from_isr = false);
    int32_t UnLock(bool from_isr = false);
    int32_t TryLock(bool from_isr = false);

protected:
    sc_mutex m_mutex;
};

}

}


#endif




