/*!
 * \file esys/sysc/system.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_SYSTEM_H__
#define __ESYS_SYSC_SYSTEM_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/systembase.h"
#include "esys/sysc/systemplat.h"

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API System : public SystemBase, public SystemPlat
{
public:
    System(const ObjectName &name);
    virtual ~System();
};

}

}

#endif



