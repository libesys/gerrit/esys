/*!
 * \file esys/sysc/mutex.h
 * \brief Declaration of the SystemC Mutex class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_MUTEX_H__
#define __ESYS_SYSC_MUTEX_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/mutexbase.h"

#ifndef ESYS_VHW
#include "esys/sysc/muteximpl.h"
#endif

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API MutexImpl;

/*! \class Mutex esys/sysc/mutex.h "esys/sysc/mutex.h"
 *  \brief SysC Mutex class
 */
class ESYS_SYSC_API Mutex: public esys::MutexBase
{
public:
    Mutex(const esys::ObjectName &name, Type type = DEFAULT);
    virtual ~Mutex();

    virtual int32_t Lock(bool from_isr=false) override;
    virtual int32_t UnLock(bool from_isr = false) override;
    virtual int32_t TryLock(bool from_isr = false) override;

protected:
    MutexImpl *m_impl;      //!< The PIMPL object
};

}

#ifdef ESYS_USE_SYSC
using namespace sysc;
#endif

}

#endif




