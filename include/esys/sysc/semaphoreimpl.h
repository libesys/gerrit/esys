/*!
 * \file esys/sysc/semaphoreimpl.h
 * \brief Declaration of the SystemC Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_SEMAPHOREIMPL_H__
#define __ESYS_SYSC_SEMAPHOREIMPL_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/semaphorebase.h"

#include <systemc.h>

namespace esys
{

namespace sysc
{

/*! \class SemaphoreImpl esys/sysc/semaphoreimpl.h "esys/sysc/semaphoreimpl.h"
 *  \brief SystemC Semaphore PIMPL class
 */
class ESYS_SYSC_API SemaphoreImpl
{
public:
    SemaphoreImpl(const char *name, uint32_t  count = 0);
    ~SemaphoreImpl();

    int32_t Post(bool from_isr = false);
    int32_t Wait(bool from_isr = false);
    int32_t WaitFor(uint32_t ms, bool from_isr = false);
    int32_t TryWait(bool from_isr = false);
    uint32_t Count();

    int32_t StaticInit(SemaphoreBase::Order order);
    int32_t StaticRelease(SemaphoreBase::Order order);
protected:
    sc_semaphore m_sem;
};

}

}

#endif




