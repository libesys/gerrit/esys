/*!
 * \file esys/sysc/taskmngrplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_TASKMNGRPLAT_H__
#define __ESYS_SYSC_TASKMNGRPLAT_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/taskmngrplatif.h"

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API TaskMngrPlatImpl;

/*! \class TaskMngrPlat esys/sysc/taskmngrplat.h "esys/sysc/taskmngrplat.h"
 *  \brief Defines the platform dependent SystemC TaskMngr API when there is a scheduler
 */
class ESYS_SYSC_API TaskMngrPlat: public virtual TaskMngrPlatIf
{
public:
    TaskMngrPlat(TaskMngrBase *taskmngr_base);
    virtual ~TaskMngrPlat();

    virtual int32_t StartScheduler() override;
    virtual void ExitScheduler() override;
    virtual void Done(TaskBase *task) override;

    TaskMngrPlatImpl *GetImpl();
protected:
    TaskMngrPlatImpl *m_impl;
};

}

}

#endif






