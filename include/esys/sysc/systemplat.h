/*!
 * \file esys/sysc/systemplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_SYSTEMPLAT_H__
#define __ESYS_SYSC_SYSTEMPLAT_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/inttypes.h"
#include "esys/systemplatif.h"
#include "esys/systembase.h"

namespace esys
{

namespace sysc
{

/*! \class SystemPlat esys/sysc/systemplat.h "esys/sysc/systemplat.h"
 *  \brief Defines the SystemC SystemPlat when there is a scheduler
 */
class ESYS_SYSC_API SystemPlat: public virtual SystemPlatIf
{
public:
    SystemPlat(SystemBase *system_base);
    virtual ~SystemPlat();

    virtual int32_t PlatInit() override;
    virtual int32_t PlatRelease() override;
    SystemBase *GetSystemBase();
    void SetSystemBase(SystemBase *system_base);
protected:
    SystemBase *m_system_base;
};

}

}

#endif





