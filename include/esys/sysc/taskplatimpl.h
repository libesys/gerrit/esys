/*!
 * \file esys/sysc/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSC_TASKPLATIMPL_H__
#define __ESYS_SYSC_TASKPLATIMPL_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/taskplatif.h"
#include "esys/taskbase.h"
#include "esys/sysc/semaphore.h"

#include <systemc.h>

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API TaskMngr;
class ESYS_SYSC_API TaskMngrImpl;

/*! \class TaskPlat esys/sysc/taskplatimpl.h "esys/sysc/taskplatimpl.h"
 *  \brief
 */
class ESYS_SYSC_API TaskPlatImpl: public sc_module
{
public:
    SC_HAS_PROCESS(TaskPlatImpl);

    TaskPlatImpl(const sc_module_name &name, TaskBase *task_base);
    virtual ~TaskPlatImpl();

    int32_t Create();
    int32_t Start();
    int32_t Stop();
    int32_t Kill();
    int32_t Destroy();
    int32_t Done();

    millis_t Millis();
    void Sleep(millis_t ms);
    void SleepU(micros_t us);

    void CallEntry();

    void SetTaskBase(TaskBase *task_base);
protected:
    TaskBase *m_task_base = nullptr;
    const char *m_name = nullptr;
};

}

}

#endif





