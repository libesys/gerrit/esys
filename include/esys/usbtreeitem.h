/*!
 * \file esys/usbtreeitem.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_USBTREEITEM_H__
#define __ESYS_USBTREEITEM_H__

#ifdef WIN32
#include "esys/win32/usbtreeitem.h"
#else
#include "esys/unix/usbtreeitem.h"
#endif

#endif


