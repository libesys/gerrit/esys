/*!
 * \file esys/taskmngrplatif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASKMNGRPLATIF_H__
#define __ESYS_TASKMNGRPLATIF_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/types.h"

namespace esys
{

class ESYS_API TaskBase;
class ESYS_API TaskMngrBase;

/*! \class TaskMngrPlatIf esys/taskmngrplatif.h "esys/taskmngrplatif.h"
 *  \brief Defines the platform dependent TaskMngr API when there is a scheduler
 */
class ESYS_API TaskMngrPlatIf
{
public:
    virtual ~TaskMngrPlatIf();

    virtual int32_t PlatInit();
    virtual int32_t PlatRelease();
    virtual int32_t StartScheduler() = 0;
    virtual void Done(TaskBase *task) = 0;
    virtual void ExitScheduler();
};

}

#endif


