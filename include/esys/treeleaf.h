/*!
 * \file esys/treeleaf.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TREELEAF_H__
#define __ESYS_TREELEAF_H__

#include "esys/esys_defs.h"
#include "esys/listitem.h"
#include "esys/treeitem_if.h"
#include "esys/assert.h"

namespace esys
{

/*! \class TreeLeaf esys/treeleaf.h "esys/treeleaf.h"
 *  \brief A leaf of a tree
 *  \tparam T the type of leaf of the tree
 *
 */
template<typename T>
class TreeLeaf: public ListItem<T>, public virtual TreeItem_if<T>
{
public:

    TreeLeaf(): ListItem<T>()
    {
    }

    virtual ~TreeLeaf()
    {
    }

    //! Set the parent of this item in the tree
    /*!
     *  \param[in] parent the parent item
     */
    virtual void SetParent(T *parent) override
    {
        m_parent = parent;
    }

    //! Return the parent of this item in the tree
    /*!
     *  \return the parent if any, nullptr otherwise
     */
    virtual T *GetParent() override
    {
        return m_parent;
    }

    virtual void SetFirstChild(T *last) override
    {
        assert(false);
    }

    //! Return the first child of this item in the tree
    /*!
     *  \return the first child is any, nullptr otherwise
     */
    virtual T *GetFirstChild() override
    {
        return nullptr;
    }

    //! Return the last child of this item in the tree
    /*!
     *  \return the last child is any, nullptr otherwise
     */
    virtual T *GetLastChild() override
    {
        return nullptr;
    }

    virtual void SetLastChild(T *last) override
    {
        assert(false);
    }

    virtual void SetNext(T *next) override
    {
        ListItem<T>::SetNext(next);
    }

    virtual T *GetNext() override
    {
        return ListItem<T>::GetNext();
    }

    virtual void SetPrev(T *prev) override
    {
        ListItem<T>::SetPrev(prev);
    }

    virtual T *GetPrev() override
    {
        return ListItem<T>::GetPrev();
    }

    virtual void AddChild(T *child) override
    {
        assert(false);
    }

    //! Function to walk the complete tree
    /*!
     *  \param[in] top the item at the top of the tree or sub-tree to traverse
     *  \param[in] function the functor to call on each visited item in the tree
     *  \param[out] err_count the number of error found when walking the tree
     *  \param[in] reverse true if traversing starting from the first child of the top item, false if starting from the last
     */
    static int32_t WalkTreeDepthFirst(T *top, typename TreeItem_if<T>::FunctorType &function, uint32_t *err_count, bool reverse = false);

    //! Function to walk the complete tree
    /*!
    *  \param[in] top the item at the top of the tree or sub-tree to traverse
    *  \param[in] function the functor to call on each visited item in the tree
    *  \param[in] reverse true if traversing starting from the first child of the top item, false if starting from the last
    */
    static int32_t WalkTreeDepthFirst(T *top, typename TreeItem_if<T>::FunctorType &function, bool reverse = false)
    {
        return WalkTreeDepthFirst(top, function, nullptr, reverse);
    }

protected:
//!< \cond DOXY_IMPL
    T *m_parent = nullptr;  //<! The parent of this item
//!< \endcond
};

template<typename T>
int32_t TreeLeaf<T>::WalkTreeDepthFirst(T *child, typename TreeItem_if<T>::FunctorType &function, uint32_t *err_count, bool reverse)
{
    T *cur_obj = child;
    T *cur_child;
    T *next;
    int32_t result = 0;
    int32_t tmp_result = 0;

    if (err_count != nullptr)
        *err_count = 0;

    while (cur_obj != nullptr)
    {
        tmp_result = function.CallFct(cur_obj, TreeItem_if<T>::BEFORE_CHILDREN);
        if ((result == 0) && (tmp_result < 0))
        {
            result = tmp_result;
            if (err_count != nullptr)
                (*err_count) += 1;
        }

        if (reverse == false)
            cur_child = cur_obj->GetFirstChild();
        else
            cur_child = cur_obj->GetLastChild();
        if (cur_child != nullptr)
        {
            cur_obj = cur_child;
        }
        else
        {
            tmp_result = function.CallFct(cur_obj, TreeItem_if<T>::AFTER_CHILDREN);
            if ((result == 0) && (tmp_result < 0))
            {
                result = tmp_result;
                if (err_count != nullptr)
                    (*err_count) += 1;
            }

            if (reverse == false)
                next = cur_obj->GetNext();
            else
                next = cur_obj->GetPrev();
            if (next != nullptr)
            {
                cur_obj = next;
            }
            else
            {
                while (next == nullptr)
                {
                    cur_obj = cur_obj->GetParent();
                    if (cur_obj == nullptr)
                        break;

                    result = function.CallFct(cur_obj, TreeItem_if<T>::AFTER_CHILDREN);
                    if ((result == 0) && (tmp_result < 0))
                    {
                        result = tmp_result;
                        if (err_count != nullptr)
                            (*err_count) += 1;
                    }

                    if (reverse == false)
                        next = cur_obj->GetNext();
                    else
                        next = cur_obj->GetPrev();
                }
                cur_obj = next;
            }
        }
    }
    return result;
}

}

#endif


