/*!
 * \file esys/listitemdata.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_LISTITEMDATA_H__
#define __ESYS_LISTITEMDATA_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{


/*! \class ListItemData esys/listitemdata.h "esys/listitemdata.h"
 *  \brief Data of an item in a list
 *  \tparam T the type of item in the list
 *
 */
template<typename T>
class ListItemData
{
public:
    ListItemData();
    ~ListItemData();

    //! Set the object preceding the current one in the list
    /*!
     *  \param[in] prev the previous object
     */
    void SetPrev(T *prev);

    //! Return the object preceding the current one in the list
    /*!
     *  \return the previous object if any, nullptr otherwise
     */
    T *GetPrev();

    //! Set the object following the current one in the list
    /*!
     *  *  \param[in] next the next object
     */
    void SetNext(T *next);

    //! Return the object following the current one in the list
    /*!
     *  \return the following object if any, nullptr otherwise
     */
    T *GetNext();
protected:
//!< \cond DOXY_IMPL
    T *m_prev = nullptr;    //<! The previous item
    T *m_next = nullptr;    //<! The next item
//!< \endcond
};

template<typename T>
ListItemData<T>::ListItemData()
{
}

template<typename T>
ListItemData<T>::~ListItemData()
{
}

template<typename T>
void ListItemData<T>::SetPrev(T *prev)
{
    m_prev = prev;
}

template<typename T>
T *ListItemData<T>::GetPrev()
{
    return m_prev;
}

template<typename T>
void ListItemData<T>::SetNext(T *next)
{
    m_next = next;
}

template<typename T>
T *ListItemData<T>::GetNext()
{
    return m_next;
}

}

#endif
