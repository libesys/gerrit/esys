/*!
 * \file esys/semaphore.h
 * \brief The Semaphore class
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SEMAPHORE_H__
#define __ESYS_SEMAPHORE_H__

#include "esys/esys_defs.h"
#include "esys/object.h"

#ifndef ESYS_MULTI_PLAT

#ifdef ESYS_USE_SYSC

#elif defined(ESYS_USE_BOOST)
#include "esys/boost/semaphore.h"
#elif defined(ESYS_USE_FREERTOS)
#include "esys/freertos/semaphore.h"
#else

#endif

#else

#include "esys/mp/semaphore.h"

#endif

#endif


