/*!
 * \file esys/gen/criticalsection.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"
#include "esys/criticalsectionbase.h"
#include "esys/mutex.h"
#include "esys/objectleaf.h"

namespace esys
{

namespace gen
{

/*!
 * \brief Critical section emulation on VHW.
 */
class CriticalSection : public ObjectLeaf, public CriticalSectionBase
{
public:
    CriticalSection(const ObjectName &name)
        : ObjectLeaf(name)
        , CriticalSectionBase()
    {
    }

    virtual ~CriticalSection()
    {
    }

    virtual void Enter() final override
    {
        m_mutex.Lock();
    }

    virtual void Exit() final override
    {
        m_mutex.UnLock();
    }

    virtual void ISREnter() final override
    {
        // ISRs do not really exist on VHW so the mutex mechanism used.
        Enter();
    }

    virtual void ISRExit() final override
    {
        // ISRs do not really exist on VHW so the mutex mechanism used.
        Exit();
    }

private:
    esys::Mutex m_mutex {"CrSecMutex"};
};

}

#ifdef ESYS_USE_GEN_CRITICAL_SECTION
using namespace gen;
#endif

}



