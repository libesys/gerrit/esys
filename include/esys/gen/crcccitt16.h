/*!
 * \file esys/gen/crcccitt16.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/crcbase.h"

namespace esys
{

namespace gen
{

class ESYS_API CRCCCITT16: public CRCBase
{
public:
    CRCCCITT16();
    virtual ~CRCCCITT16();

    virtual void Reset() override;
    virtual void AddData(esys::uint8_t *data, esys::uint32_t size) override;
    esys::uint32_t GetChecksum() const override;

    static esys::uint32_t Checksum(esys::uint8_t *data, esys::uint32_t size);
protected:
    void AddByte(esys::uint8_t data);
    static esys::uint16_t AddByte(esys::uint16_t crc, esys::uint8_t data);

    esys::uint16_t m_crc;
};

}

#ifdef ESYS_GEN_CRCCCITT16
using namespace gen;
#endif

}



