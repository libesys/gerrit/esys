/*!
 * \file esys/dynlibrarybase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

#include <string>

namespace esys
{

class ESYS_API DynLibraryBase
{
public:
    DynLibraryBase();
    virtual ~DynLibraryBase();

    virtual int Load(const std::string &filename) = 0;
    virtual int UnLoad() = 0;
    virtual bool IsLoaded() = 0;
    virtual bool HasSymbol(const std::string& name) = 0;
    virtual void *GetSymbol(const std::string& name) = 0;

};

}
