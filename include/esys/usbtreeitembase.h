/*!
 * \file esys/usbtreeitembase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

#include <string>
#include <vector>

namespace esys
{

class ESYS_API USBDevInfo;

class ESYS_API USBTreeItemBase
{
public:
    USBTreeItemBase();
    virtual ~USBTreeItemBase();

    const std::string &GetName();
    void SetName(const std::string &name);
    void SetParent(USBTreeItemBase *parent);
    USBTreeItemBase *GetParent();
    int32_t NbrChildren();
    USBTreeItemBase *GetChild(int32_t idx);
    void AddChild(USBTreeItemBase *child);
    void SetHubPort(int32_t port);
    int32_t GetHubPort();
    void SetUSBDevInfo(USBDevInfo *usb_dev_info);
    USBDevInfo *GetUSBDevInfo();

protected:
    USBTreeItemBase *m_parent;
    std::string m_name;
    std::vector<USBTreeItemBase *> m_children;
    int32_t m_hub_port = {-1};
    USBDevInfo *m_usb_dev_info = {nullptr};
};

} // namespace esys
