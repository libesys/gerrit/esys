/*!
 * \file esys/i2ctrans_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_I2CTRANS_IF_H__
#define __ESYS_I2CTRANS_IF_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/i2chw_if.h"

namespace esys
{

class ESYS_API I2CTrans_if: public I2CHW_if
{
public:

};

}

#endif
