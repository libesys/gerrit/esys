/*!
 * \file esys/pointer.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
*/

#ifndef __ESYS_POINTER_H__
#define __ESYS_POINTER_H__

#include "esys/esys_defs.h"
#include "esys/assert.h"
#include "esys/inttypes.h"

#include <stddef.h>     //Needed for NULL under GCC
#include <string.h>		//for memcpy

namespace esys
{

class ESYS_API PointerBase
{
public:
    PointerBase(uint16_t max_size=0, uint16_t size=0);
    virtual ~PointerBase();

    void SetSize(uint16_t size);
    virtual uint16_t GetSize()=0;
    void SetMaxSize(uint16_t size);
    uint16_t GetMaxSize();
    void SetIndex(uint16_t idx=0);
    uint16_t &GetIndex();
    uint16_t GetIndex() const;
    virtual void Reset();
    bool IsValid();
    void SetValid(bool valid);
    void SetOffset(uint16_t offset);
    uint16_t GetOffset() const;
protected:
    uint16_t m_size;
    uint16_t m_max_size;
    uint16_t m_index;
    uint16_t m_offset;
    bool m_valid;
};

template<typename T>
class Pointer: public PointerBase
{
public:
    Pointer(): PointerBase()
    {
    }

    virtual ~Pointer()
    {
    }

    //virtual uint16_t GetNbrSegment(bool data=true)=0;
    //virtual T *GetSegment(uint16_t idx, uint16_t &size, bool data=true)=0;
    //virtual void WriteSegment(uint16_t idx, T *, uint16_t size, bool data=true)=0;

    virtual Pointer<T> &operator+=(int value)=0;
    virtual Pointer<T> &operator++(int)=0;
    virtual T &operator[](uint16_t idx)=0;
    virtual T &operator()(uint16_t idx)=0;
    virtual T &operator*()=0;

    virtual int16_t Push(T *data, uint16_t size)=0;
    virtual int16_t CopyFrom(T *data, uint16_t size)=0;

    friend Pointer<T> &operator+(Pointer<T> &lhs,       // passing first arg by value helps optimize chained a+b+c
                                 const int16_t& rhs) // alternatively, both parameters may be const references.
    {
        uint16_t offset=lhs.GetOffset();

        offset+=rhs;
        lhs.SetOffset(offset);
        return lhs; // reuse compound assignment and return the result by value
    }

    /*template<typename OBJ>
    void DoEachSeg(OBJ *obj, void (OBJ::*func)(T *buf, uint16_t size), bool data=true)
    {
        T *p;
        uint16_t size;

        for (uint16_t idx=0;idx<GetNbrSegment(data);++idx)
        {
            p=GetSegment(idx, size, data);
            (*obj.*func)(p, size);
        }
    }

    template<typename OBJ>
    inline void DoEachSeg(OBJ &obj, void (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        DoEachSeg(&obj, func, data);
    }

    template<typename OBJ>
    int16_t DoEachSeg(OBJ *obj, int16_t (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        T *p;
        uint16_t size;
        int16_t result;

        for (uint16_t idx=0;idx<this->GetNbrSegment(data);++idx)
        {
            p=GetSegment(idx, size, data);
            result=(*obj.*func)(p, size);
            if (result<0)
                return result;
        }
        return result;
    }

    template<typename OBJ>
    inline int16_t DoEachSeg(OBJ &obj, int16_t (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        return DoEachSeg(&obj, func, data);
    } */
    //virtual operator T()=0;
    //virtual Pointer<T> &operator=(const T &val)=0;
protected:

};


template<typename T, typename BLOCK>
class PointerBlock: public Pointer<T>
{
public:
    PointerBlock(BLOCK *block=NULL): Pointer<T>(), m_mem_block(block)
    {
    }

    virtual ~PointerBlock()
    {
    }

    void SetBlock(BLOCK *block)
    {
        m_mem_block=block;
    }

    BLOCK *GetBlock()
    {
        return m_mem_block;
    }

    virtual uint16_t GetSize()
    {
        assert(m_mem_block!=NULL);

        return m_mem_block->GetSize()-this->m_offset-this->m_index;
    }

    virtual int16_t Push(T *data, uint16_t size)
    {
        assert(m_mem_block!=NULL);

        return m_mem_block->Push(data,size);
    }

    virtual int16_t CopyFrom(T *data, uint16_t size)
    {
        assert(m_mem_block!=NULL);

        return m_mem_block->Copy(*this, data, size);
    }

    virtual Pointer<T> &operator+=(int value)
    {
        assert(m_mem_block!=NULL);
        //assert(this->m_offset+this->m_idx<m_mem_block->GetSize());
        this->m_index+=value;
        if (this->m_offset+this->m_index>=m_mem_block->GetSize())
            this->SetValid(false);
        else
            this->SetValid(true);
        return *this;
    }

    virtual Pointer<T> &operator++(int)
    {
        assert(m_mem_block!=NULL);
        //assert(this->m_offset+this->m_idx<m_mem_block->GetSize());
        this->m_index++;
        if ((this->m_offset + this->m_index)>=m_mem_block->GetSize())
            this->SetValid(false);
        else
            this->SetValid(true);
        return *this;
    }

    virtual T &operator[](uint16_t idx)
    {
        assert(this->m_mem_block!=NULL);

        return (*this->m_mem_block)[this->m_offset+idx];
    }

    virtual T &operator()(uint16_t idx)
    {
        assert(this->m_mem_block!=NULL);

        return (*this->m_mem_block)(idx);
    }
    virtual T &operator*()
    {
        assert(this->m_mem_block!=NULL);

        uint16_t idx=this->m_offset+this->m_index;

        return (*this->m_mem_block)[idx];
    }

    template<typename OBJ>
    void DoEachSeg(OBJ *obj, void (OBJ::*func)(T *buf, uint16_t size), bool data=true)
    {
        assert(this->m_mem_block!=NULL);

        this->m_mem_block->DoEachSeg(obj, func, data);
    }

    template<typename OBJ>
    inline void DoEachSeg(OBJ &obj, void (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        DoEachSeg(&obj, func, data);
    }

    template<typename OBJ>
    int16_t DoEachSeg(OBJ *obj, int16_t (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        assert(this->m_mem_block!=NULL);

        return this->m_mem_block->DoEachSeg(obj, func, data);
    }

    template<typename OBJ>
    inline int16_t DoEachSeg(OBJ &obj, int16_t (OBJ::*func)(T *buf, uint16_t size), bool data=true )
    {
        return DoEachSeg(&obj, func, data);
    }

    friend PointerBlock<T, BLOCK> operator+(PointerBlock<T, BLOCK> &lhs,       // passing first arg by value helps optimize chained a+b+c
                                            const int16_t& rhs) // alternatively, both parameters may be const references.
    {
        PointerBlock<T, BLOCK> temp=lhs;
        uint16_t index=lhs.GetIndex()+lhs.GetOffset();

        index+=rhs;
        temp.SetOffset(index);
        temp.SetIndex(0);
        return temp; // reuse compound assignment and return the result by value
    }
protected:
    BLOCK *m_mem_block;
};

template<typename T>
class PointerWrapBase: public Pointer<T>
{
public:
    PointerWrapBase(uint16_t max_size=0)
        : Pointer<T>(max_size, 0)
    {
        //SetSize(0);
    }

    PointerWrapBase(uint16_t max_size, uint16_t size=0): Pointer<T>(max_size, size)
    {
        //SetSize(size);
    }

    virtual ~PointerWrapBase()
    {
    }

    virtual int16_t Push(T *data, uint16_t size)
    {
        uint16_t av_size=this->GetMaxSize()-this->GetSize();
        int16_t result=0;

        if (size>av_size)
        {
            size=av_size;
            result=-2;
        }
        ::memcpy(GetData()+this->GetSize(), data, size);
        this->SetSize(size+this->GetSize());
        return result;
    }

    virtual uint16_t GetNbrSegment(bool data=true)
    {
        if (this->GetSize()==0)
        {
            if (data==true)
                return 0; // There is no data
            else
                return 1;
        }
        else
        {
            if (this->GetSize()!=this->GetMaxSize())
                return 1;
            else
                return 0;
        }
    }

    virtual T *GetSegment(uint16_t idx, uint16_t &size, bool data=true)
    {
        if (idx!=0)
        {
            size=0;
            return NULL;
        }

        if (this->GetSize()==0)
        {
            //There is no data in the buffer
            if (data==true)
            {
                size=0;
                return NULL; // There is no data
            }
            else
            {
                size=this->GetMaxSize();  //Free space in the buffer
                return GetData();
            }
        }
        else
        {
            //There is data in the buffer
            if (data==true)
            {
                size=this->GetSize();
                return GetData();
            }
            else
            {
                size=this->GetMaxSize()-this->GetSize();
                return GetData()+this->GetSize();
            }
        }
    }

    virtual void WriteSegment(uint16_t idx, T *, uint16_t size, bool data=true)
    {

    }

    /*virtual Pointer<T> &operator++(int)
    {
        assert(this->m_offset+this->m_idx<this->m_size);

        this->m_idx++;
        return *this;
    } */

    virtual T &operator[](uint16_t idx)
    {
        assert((this->m_offset+idx)<this->m_size);

        return GetData()[this->m_offset+idx];
    }

    virtual T &operator()(uint16_t idx)
    {
        assert(idx<this->m_size);

        return GetData()[idx];
    }

    virtual T &operator*()
    {
        assert(this->m_offset+this->m_idx<this->m_size);

        return GetData()[this->m_offset+this->m_idx];
    }
protected:
    virtual T *GetData()=0;

};

template<typename T>
class PointerWrap: public PointerWrapBase<T>
{
public:
    PointerWrap(T *p=NULL, uint16_t max_size=0)
        : PointerWrapBase<T>(max_size, 0), m_p(p)
    {
        this->SetSize(0);
    }

    PointerWrap(T *p, uint16_t max_size, uint16_t size): PointerWrapBase<T>(max_size, size), m_p(p)
    {
        this->SetSize(size);
    }

    virtual ~PointerWrap()
    {
    }

    virtual void SetData(T *p, uint16_t size)
    {
        m_p=p;
        this->SetSize(size);
        this->m_idx=0;
    }

protected:
    inline virtual T *GetData()
    {
        return m_p;
    }

    T *m_p;
};

template<typename T, uint16_t N>
class PointerStatic: public PointerWrapBase<T>
{
public:
    PointerStatic(): PointerWrapBase<T>(N, 0)
    {
    }

    PointerStatic(uint16_t size): PointerWrapBase<T>(N, size)
    {
    }

    virtual ~PointerStatic()
    {
    }

    virtual void SetData(T *p)
    {
        //m_p=p;
    }

    virtual void SetData(T *p, uint16_t size)
    {
        //m_p=p;

        //SetSize(size);
    }

protected:
    inline virtual T *GetData()
    {
        return m_p;
    }

    T m_p[N];
};

template<typename T>
Pointer<T> &memset(Pointer<T> &dst, const T &value, uint16_t size)
{
    uint16_t idx;

    assert(dst.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=value;
    return dst;
}

template<typename T>
Pointer<T> &memcpy(Pointer<T> &dst, Pointer<T> &src, uint16_t size)
{
    uint16_t idx;

    assert(dst.GetSize()>=size);
    assert(src.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=src[idx];
    return dst;
}

template<typename T>
Pointer<T> &memcpy(Pointer<T> &dst, T *src, uint16_t size)
{
    uint16_t idx;

    assert(dst.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=src[idx];
    return dst;
}

template<typename T>
T *memcpy(T *dst, Pointer<T> &src, uint16_t size)
{
    uint16_t idx;

    assert(src.GetSize()>=size);

    for (idx=0; idx<size; ++idx)
        dst[idx]=src[idx];
    return dst;
}

/*template<typename T>
PointerStatic<T> &memset(PointerStatic<T> &dst, const T &value, uint16_t size) */

}

#endif
