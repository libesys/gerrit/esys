/*!
 * \file esys/connection.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_CONNECTION_H__
#define __ESYS_CONNECTION_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

namespace esys
{

class ESYS_API Connection
{
public:
    Connection();
    virtual ~Connection();

};

}

#endif

