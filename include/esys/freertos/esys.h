/*!
 * \file esys/freertos/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_ESYS_H__
#define __ESYS_FREERTOS_ESYS_H__

#include "esys_freertos/esys_freertos_defs.h"
#ifdef ESYS_MULTI_PLAT
#include "esys/mp/esysbase.h"
#else
#include "esys/esysbase.h"
#endif

#include "esys/timebase.h"

namespace esys
{

namespace freertos
{

class ESYS_FREERTOS_API ESys:
#ifdef ESYS_MULTI_PLAT
    public mp::ESysBase
#else
    public ESysBase
#endif
{
public:
#ifdef ESYS_MULTI_PLAT
    typedef mp::ESysBase BaseType;
#else
    typedef ESysBase BaseType;
#endif

    ESys();
    virtual ~ESys();

    static ESys &Get();

#ifdef ESYS_MULTI_PLAT
    static void Select();

    virtual SemaphoreBase *NewSemaphoreBase(const ObjectName &name, uint32_t count) override;
    virtual MutexBase *NewMutexBase(const ObjectName &name, MutexBase::Type type = MutexBase::DEFAULT) override;
    virtual TaskPlatIf *NewTaskPlat(TaskBase *task_base) override;
    virtual TaskMngrPlatIf *NewTaskMngrPlat(TaskMngrBase *taskmngr_base) override;
    virtual SystemPlatIf *NewSystemPlat(SystemBase *system_base) override;
    virtual TimerBase *NewTimerBase(const ObjectName &name) override;

    virtual uint32_t implMillis(TimeBase::Source source = TimeBase::DEFAULT_CLK) override;
    virtual uint64_t implMicros(TimeBase::Source source = TimeBase::DEFAULT_CLK) override;

    virtual TaskPlatIf *GetCurTaskPlatIf() override;
#endif



protected:
    static ESys s_esys;
};

}

#ifdef ESYS_USE_FREERTOS
using namespace freertos;
#endif

}

#endif

