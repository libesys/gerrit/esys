/*!
 * \file esys/freertos/semaphoreimpl.h
 * \brief Declaration of the FreeRTOS Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/semaphorebase.h"

#include <FreeRTOS.h>
#include <FreeRTOS/semphr.h>

namespace esys
{

namespace freertos
{

/*! \class Semaphore esys/freertos/semaphore.h "esys/freertos/semaphore.h"
 *  \brief FreeRTOS Semaphore PIMPL class
 */
class ESYS_FREERTOS_API SemaphoreImpl
#ifndef ESYS_VHW
    : public SemaphoreBase
#endif
{
public:
#ifndef ESYS_VHW
    SemaphoreImpl(const ObjectName &name, uint32_t  count=0);

    virtual ~SemaphoreImpl();

    virtual int32_t Post(bool from_isr = false) override;
    virtual int32_t Wait(bool from_isr = false) override;
    virtual int32_t WaitFor(uint32_t ms, bool from_isr = false) override;
    virtual int32_t TryWait(bool from_isr = false) override;
    virtual uint32_t Count() override;

    virtual int32_t StaticInit(Order order) override;
    virtual int32_t StaticRelease(Order order) override;
#else
    SemaphoreImpl(uint32_t  count = 0);
    ~SemaphoreImpl();

    int32_t Post(bool from_isr = false);
    int32_t Wait(bool from_isr = false);
    int32_t WaitFor(uint32_t ms, bool from_isr = false);
    int32_t TryWait(bool from_isr = false);
    uint32_t Count();

    int32_t StaticInit(SemaphoreBase::Order order);
    int32_t StaticRelease(SemaphoreBase::Order order);
#endif
protected:
    SemaphoreHandle_t m_sem;
    uint32_t m_count;
};

}

}



