/*!
 * \file esys/freeros/task.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_TASK_H__
#define __ESYS_FREERTOS_TASK_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/taskbase.h"
#include "esys/freertos/taskplat.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4250)
#endif

namespace esys
{

namespace freertos
{

class ESYS_FREERTOS_API TaskMngr;

/*! \class Task esys/boost/scheduler/task.h "esys/boost/scheduler/task.h"
 *  \brief Boost implementation of a scheduled Task
 */
class ESYS_FREERTOS_API Task: public esys::TaskBase, public TaskPlat
{
public:
    Task(const esys::ObjectName &name, TaskType type = TaskType::APPLICATION);
    Task(const Task &) = delete;

    virtual ~Task();

    virtual void SetMngrBase(TaskMngrBase *taskmngr_base);
protected:

};

}

#ifdef ESYS_USE_FREERTOS
using namespace freertos;
#endif
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif



