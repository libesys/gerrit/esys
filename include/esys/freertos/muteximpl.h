/*!
 * \file esys/freertos/muteximpl.h
 * \brief Declaration of the FreeRTOS Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_MUTEXIMPL_H__
#define __ESYS_FREERTOS_MUTEXIMPL_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/mutexbase.h"

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>
#include <FreeRTOS/semphr.h>

namespace esys
{

namespace freertos
{

/*! \class MutexImpl esys/freertos/muteximpl.h "esys/freertos/muteximpl.h"
 *  \brief Boost Mutex class
 */
class ESYS_FREERTOS_API MutexImpl
#ifndef ESYS_VHW
    : public esys::MutexBase
#endif
{
public:

#ifndef ESYS_VHW
    MutexImpl(const esys::ObjectName &name, Type type = DEFAULT);
    virtual ~MutexImpl();
    virtual int32_t Lock(bool from_isr = false) override;
    virtual int32_t UnLock(bool from_isr = false) override;
    virtual int32_t TryLock(bool from_isr = false) override;

    virtual int32_t StaticInit(Order order) override;
    virtual int32_t StaticRelease(Order order) override;
#else
    MutexImpl(MutexBase::Type type = MutexBase::DEFAULT);
    ~MutexImpl();
    int32_t Lock(bool from_isr = false);
    int32_t UnLock(bool from_isr = false);
    int32_t TryLock(bool from_isr = false);

    int32_t StaticInit(MutexBase::Order order);
    int32_t StaticRelease(MutexBase::Order order);
#endif

protected:
    SemaphoreHandle_t m_mutex;
    MutexBase::Type m_type;
};

}

}


#endif



