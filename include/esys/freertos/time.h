/*!
 * \file esys/freertos/time.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/inttypes.h"
#include "esys/timebase.h"

namespace esys
{

namespace freertos
{

/*! \class Time esys/freertos/time.h "esys/freertos/time.h"
 *  \brief
 */
class ESYS_FREERTOS_API Time: public TimeBase
{
public:
    Time();

    static uint32_t Millis(Source source = DEFAULT_CLK);
    static uint64_t Micros(Source source = DEFAULT_CLK);
    static uint32_t MicrosLow();

    static void Sleep(uint32_t ms);
    static void USleep(uint32_t us);

    static int32_t SetTime(TimeStruct &time);
    static int32_t GetTime(TimeStruct &time);
    static int32_t SetDate(DateStruct &date);
    static int32_t GetDate(DateStruct &date);
    static int32_t SetDateTime(DateTimeStruct &date_time);
    static int32_t GetDateTime(DateTimeStruct &date_time);

    static int32_t StartTimestamp();
    static int32_t StopTimestamp();
private:

};

}

#ifdef ESYS_USE_FREERTOS
using namespace freertos;
#endif

}



