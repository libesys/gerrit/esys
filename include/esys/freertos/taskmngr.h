/*!
 * \file esys/freertos/taskmngr.h
 * \brief Declaration of the Boost TaskMngr class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_TASKMNGR_H__
#define __ESYS_FREERTOS_TASKMNGR_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/taskmngrbase.h"
#include "esys/freertos/taskmngrplat.h"

namespace esys
{

namespace freertos
{

/*! \class TaskMngr esys/freertos/taskmngr.h "esys/freertos/taskmngr.h"
 *  \brief FreeRTOS TaskMngr class
 */
class ESYS_FREERTOS_API TaskMngr: public TaskMngrBase, public TaskMngrPlat
{
public:
    TaskMngr(const ObjectName &name);
    virtual ~TaskMngr();
};

}

#ifdef ESYS_USING_PLAT_NS
using namespace freertos;
#endif

}

#endif







