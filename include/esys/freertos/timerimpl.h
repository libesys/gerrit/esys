/*!
 * \file esys/freertos/timerimpl.h
 * \brief The FreeRTOS declaration of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/timerbase.h"

#include <FreeRTOS.h>
#include <FreeRTOS/timers.h>

namespace esys
{

namespace freertos
{

/*! \class TimeImpl esys/freertos/timerimpl.h "esys/freertos/timerimpl.h"
 *  \brief FreeRTOS PIMPL class for the Timer
 */
class ESYS_FREERTOS_API TimerImpl : public TimerBase
{
public:
    TimerImpl(const char *name);
    virtual ~TimerImpl();

    //! Start the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Start(uint32_t period_ms, bool one_shot = false, bool from_isr = false) override;

    //! Stop the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Stop(bool from_isr = false) override;

    //! Return the Timer period in ms
    /*! \return the period in ms
    */
    virtual int32_t GetPeriod() override;

    //! Return the state of the Timer
    /*! \return True if the Timer is running, false otherwise
    */
    virtual bool IsRunning() override;
    virtual bool IsOneShot() override;

    static void Callback(TimerHandle_t timer_handle);
protected:
    xTimerHandle m_timer_handle;
    const char *m_name;
    uint32_t m_period_ms;
    bool m_one_shot;
};

}

}


