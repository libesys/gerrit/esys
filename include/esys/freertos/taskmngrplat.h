/*!
 * \file esys/freertos/taskmngrplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_TASKMNGRPLATIF_H__
#define __ESYS_FREERTOS_TASKMNGRPLATIF_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/taskmngrplatif.h"

#ifndef ESYS_MULTI_PLAT

#include "esys/freertos/taskmngrplatimpl.h"

namespace esys
{

namespace freertos
{

typedef TaskMngrPlatImpl TaskMngrPlat;

}

#ifdef ESYS_USE_FREERTOS
using namespace freertos;
#endif

}

#else

namespace esys
{

namespace freertos
{

class ESYS_FREERTOS_API TaskMngrPlatImpl;

/*! \class TaskMngrPlat esys/freertos/taskmngrplat.h "esys/freertos/taskmngrplat.h"
 *  \brief Defines the platform dependent FreeRTOS TaskMngr API when there is a scheduler
 */
class ESYS_FREERTOS_API TaskMngrPlat: public virtual TaskMngrPlatIf
{
public:
    TaskMngrPlat(TaskMngrBase *taskmngr_base);
    virtual ~TaskMngrPlat();

    virtual int32_t StartScheduler() override;
    virtual void ExitScheduler() override;
    virtual void Done(TaskBase *task) override;

    TaskMngrPlatImpl *GetImpl();
protected:
    TaskMngrPlatImpl *m_impl;
};

}

}

#endif

#endif





