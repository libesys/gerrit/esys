/*!
 * \file esys/freertos/system.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_SYSTEM_H__
#define __ESYS_FREERTOS_SYSTEM_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/systembase.h"
#include "esys/freertos/systemplat.h"

namespace esys
{

namespace freertos
{

class ESYS_FREERTOS_API System : public SystemBase, public SystemPlat
{
public:
    System(const ObjectName &name);
    virtual ~System();
};

}

#ifdef ESYS_USE_FREERTOS
using namespace freertos;
#endif

}

#endif



