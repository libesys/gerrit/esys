/*!
 * \file esys/freertos/mutex.h
 * \brief Declaration of the FreeRTOS Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_MUTEX_H__
#define __ESYS_FREERTOS_MUTEX_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/mutexbase.h"

#ifndef ESYS_VHW
#include "esys/freertos/muteximpl.h"
#endif

namespace esys
{

namespace freertos
{

#ifndef ESYS_VHW
typedef MutexImpl Mutex;
#else

class ESYS_FREERTOS_API MutexImpl;

/*! \class Mutex esys/freertos/mutex.h "esys/freertos/mutex.h"
 *  \brief FreeRTOS Mutex class
 */
class ESYS_FREERTOS_API Mutex: public esys::MutexBase
{
public:
    Mutex(const esys::ObjectName &name, Type type = DEFAULT);
    virtual ~Mutex();

    virtual int32_t Lock(bool from_isr=false) override;
    virtual int32_t UnLock(bool from_isr = false) override;
    virtual int32_t TryLock(bool from_isr = false) override;

    virtual int32_t StaticInit(Order order) override;
    virtual int32_t StaticRelease(Order order) override;
protected:
    MutexImpl *m_impl;      //!< The PIMPL object
};

#endif

}

#ifdef ESYS_USE_FREERTOS
using namespace freertos;
#endif

}

#endif



