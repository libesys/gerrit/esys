/*!
 * \file esys/freertos/criticalsection.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"
#include "esys/criticalsectionbase.h"
#include <FreeRTOS/FreeRTOS.h>

namespace esys
{

namespace freertos
{

class CriticalSection : public CriticalSectionBase
{
public:
    CriticalSection(const char* name)
    {
    }

    virtual ~CriticalSection()
    {
    }

    virtual void Enter() final override
    {
        taskENTER_CRITICAL();
    }

    virtual void Exit() final override
    {
        taskEXIT_CRITICAL();
    }

    virtual void ISREnter() final override
    {
#ifdef taskENTER_CRITICAL_FROM_ISR
        m_saved_interrupt_status = taskENTER_CRITICAL_FROM_ISR();
#endif
    }

    virtual void ISRExit() final override
    {
#ifdef taskENTER_CRITICAL_FROM_ISR
        taskEXIT_CRITICAL_FROM_ISR(m_saved_interrupt_status);
#endif
    }
private:
    UBaseType_t m_saved_interrupt_status;
};

}

}


