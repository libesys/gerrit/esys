/*!
 * \file esys/freertos/taskmngrimpl.h
 * \brief Declaration of the PIMPL class of the Boost TaskMngr
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_TASKMNGRIMPL_H__
#define __ESYS_FREERTOS_TASKMNGRIMPL_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/taskmngrbase.h"
#include "esys/semaphore.h"

#ifndef ESYS_VHW
#include "esys/taskmngrplatif.h"
#endif

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>

namespace esys
{

namespace freertos
{

class ESYS_FREERTOS_API TaskMngr;

/*! \class TaskMngrPlatImpl esys/freertos/taskmngrplatimpl.h "esys/freertos/taskmngrplatimpl.h"
 *  \brief Boost TaskMngr class
 */
class ESYS_FREERTOS_API TaskMngrPlatImpl
#ifndef ESYS_VHW
    : public virtual TaskMngrPlatIf
#endif
{
public:
    TaskMngrPlatImpl(TaskMngrBase *taskmngr_base);
#ifndef ESYS_VHW
    virtual ~TaskMngrPlatImpl();

    virtual int32_t StartScheduler() override;
    virtual void ExitScheduler() override;
    virtual void Done(TaskBase *task) override;
    virtual int32_t PlatInit() override;
#else
    ~TaskMngrPlatImpl();

    int32_t StartScheduler();
    void ExitScheduler();
    void Done(TaskBase *task_base);
    int32_t PlatInit();
#endif

    static void ApplicationIdleCallback();
    static void PostSleepProcessing(unsigned long xExpectedIdleTime);
    static void ApplicationMallocFailedHook(void);
    static void ApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName);
    static void ApplicationTickHook(void);
    static void AssertCalled(void);
protected:
    TaskMngrBase *m_taskmngr_base;
};

}

}

#endif








