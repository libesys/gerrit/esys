/*!
 * \file esys/freertos/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_TASKPLATIMPL_H__
#define __ESYS_FREERTOS_TASKPLATIMPL_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/taskplatif.h"
#include "esys/taskbase.h"
//#include "esys/freertos/task.h"
#include "esys/freertos/semaphore.h"

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>

namespace esys
{

namespace freertos
{

class ESYS_FREERTOS_API TaskMngr;
class ESYS_FREERTOS_API TaskMngrImpl;

/*! \class TaskPlat esys/freertos/scheduler/taskplatimpl.h "esys/freertos/scheduler/taskplatimpl.h"
 *  \brief
 */
#ifndef ESYS_VHW
class ESYS_FREERTOS_API TaskPlatImpl : public virtual esys::TaskPlatIf
#else
class ESYS_FREERTOS_API TaskPlatImpl
#endif
{
public:
    TaskPlatImpl();
    TaskPlatImpl(TaskBase *task_base, const char *name=nullptr);
    virtual ~TaskPlatImpl();

#ifndef ESYS_VHW
    // Platform functions
    virtual int32_t Create() override;
    virtual int32_t Start() override;
    virtual int32_t Stop() override;
    virtual int32_t Kill() override;
    virtual int32_t Destroy() override;
    //virtual int32_t Done() override;

    virtual millis_t Millis() override;
    virtual void Sleep(millis_t ms) override;
    virtual void SleepU(micros_t us) override;
#else
    int32_t Create();
    int32_t Start();
    int32_t Stop();
    int32_t Kill();
    int32_t Destroy();
    int32_t Done();

    millis_t Millis();
    void Sleep(millis_t ms);
    void SleepU(micros_t us);

#endif
    void SetTaskBase(TaskBase *task_base);
    TaskBase *GetTaskBase();
    //virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base)=0;
    //virtual void NotifyExit()=0;
    static void sCallEntry(void *param);
protected:
    esys::TaskBase *m_task_base = nullptr;
    TaskHandle_t m_task = nullptr;
    const char *m_name = nullptr;
};

}

}

#endif




