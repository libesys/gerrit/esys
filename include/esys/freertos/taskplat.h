/*!
 * \file esys/freertos/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_TASKPLAT_H__
#define __ESYS_FREERTOS_TASKPLAT_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/taskplatif.h"

#ifndef ESYS_MULTI_PLAT

#include "esys/freertos/taskplatimpl.h"

namespace esys
{

namespace freertos
{

typedef TaskPlatImpl TaskPlat;

}

#ifdef ESYS_USE_FREERTOS
using namespace freertos;
#endif

}

#else

namespace esys
{

namespace freertos
{

class ESYS_FREERTOS_API Task;
class ESYS_FREERTOS_API TaskPlatImpl;
class ESYS_FREERTOS_API TaskMngrPlat;
class ESYS_FREERTOS_API TaskMngrPlatImpl;

/*! \class TaskPlat esys/freertos/taskplat.h "esys/freertos/taskplat.h"
 *  \brief
 */
class ESYS_FREERTOS_API TaskPlat: public esys::TaskPlatIf
{
public:
    TaskPlat(TaskBase *task_base);
    virtual ~TaskPlat();
    // Platform functions
    virtual int32_t Create() override;
    virtual int32_t Start() override;
    virtual int32_t Stop() override;
    virtual int32_t Kill() override;
    virtual int32_t Destroy() override;
    //virtual int32_t Done() override;

    virtual millis_t Millis() override;
    virtual void Sleep(millis_t ms) override;
    virtual void SleepU(micros_t us) override;

    virtual void SetTaskBase(TaskBase *task_base) override;

    //virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base)=0;
    //virtual void NotifyExit()=0;
    TaskPlatImpl *GetImpl();
    void SetTaskMngrBase(TaskMngrBase *taskmngr_base);

    //static TaskPlat &GetCurrent();
protected:
    Task *m_task;
    TaskBase *m_task_base;
    TaskPlatImpl *m_impl;
    TaskMngrPlat *m_mngr;
    TaskMngrPlatImpl *m_mngr_impl;
};

}

}

#endif

#endif



