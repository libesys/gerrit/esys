/*!
 * \file esys/freertos/timer.h
 * \brief The FreeRTOS declaration of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/timerbase.h"

#ifndef ESYS_FREERTOS_PIMPL
#include "esys/freertos/timerimpl.h"
#endif

namespace esys
{

namespace freertos
{

#ifndef ESYS_FREERTOS_PIMPL

typedef TimerImpl Timer;

#else

class ESYS_FREERTOS_API TimerImpl;

/*! \class Timer esys/freertos/timer.h "esys/freertos/timer.h"
 *  \brief FreeRTOS implementation of a Timer
 */
class ESYS_FREERTOS_API Timer : public TimerBase
{
public:
    Timer(const ObjectName &name);
    virtual ~Timer();

    //! Start the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Start(uint32_t ms, bool one_shot = false, bool from_isr = false) override;

    //! Stop the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Stop(bool from_isr = false) override;

    //! Return the Timer period in ms
    /*! \return the period in ms
    */
    virtual int32_t GetPeriod() override;

    //! Return the state of the Timer
    /*! \return True if the Timer is running, false otherwise
    */
    virtual bool IsRunning() override;
    virtual bool IsOneShot() override;
protected:
    TimerImpl *m_impl;
    bool m_one_shot;
};

#endif

}

}





