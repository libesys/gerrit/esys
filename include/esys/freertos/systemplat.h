/*!
 * \file esys/freertos/systemplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_SYSTEMPLAT_H__
#define __ESYS_FREERTOS_SYSTEMPLAT_H__

#include "esys_freertos/esys_freertos_defs.h"
#include "esys/inttypes.h"
#include "esys/systemplatif.h"
#include "esys/systembase.h"

namespace esys
{

namespace freertos
{

/*! \class SystemPlat esys/freertos/systemplat.h "esys/freertos/systemplat.h"
 *  \brief Defines the FreeRTOS SystemPlat when there is a scheduler
 */
class ESYS_FREERTOS_API SystemPlat: public virtual SystemPlatIf
{
public:
    SystemPlat(SystemBase *system_base);
    virtual ~SystemPlat();

    virtual int32_t PlatInit() override;
    virtual int32_t PlatRelease() override;
    SystemBase *GetSystemBase();
    void SetSystemBase(SystemBase *system_base);
protected:
    SystemBase *m_system_base;
    bool m_heap5_initialized;
};

}

}

#endif




