/*!
 * \file esys/tasklistbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASKLISTBASE_H__
#define __ESYS_TASKLISTBASE_H__

#include "esys/esys_defs.h"
//#include "esys/taskbase.h"
#include "esys/tasktype.h"

namespace esys
{

class ESYS_API TaskBase;

/*! \class TaskListBase esys/tasklistbase.h "esys/tasklistbase.h"
 *  \brief Base class for the list of Task
 *
 */
class ESYS_API TaskListBase
{
public:
    friend class ESYS_API TaskBase;

    //! Constructor
    TaskListBase();

    //! Destructor
    virtual ~TaskListBase();

    //! Return the number of Task of a given type
    /*!
     *  \return the number of Task of a given type
     */
    uint32_t GetNbrTask(TaskType typ);

    //! Return the number of active Task of a given type
    /*!
     *  \return the number of Task of a given type
     */
    uint32_t GetActiveCount(TaskType typ);

    //! Increase the count of active Task of a given type
    /*!
     *  \param[in] typ the type of Task to increase the count
     */
    void IncActiveCount(TaskType typ);

    //! Decrease the count of active Task of a given type
    /*!
     *  \param[in] typ the type of Task to increase the count
     */
    void DecActiveCount(TaskType typ);

    //! Return the first Task of the given type
    /*!
     *  \return the first Task of the given type
     */
    TaskBase *GetFirst(TaskType typ);

    //! Return the last Task of the given type
    /*!
     *  \return the last Task of the given type
     */
    TaskBase *GetLast(TaskType typ);

    //! Reset the Task list
    void Reset();
protected:
//!< \cond DOXY_IMPL
    virtual void Add(TaskBase *task_base);

    struct ListHelper
    {
        ListHelper() : m_first(nullptr), m_last(nullptr), m_count(0), m_active_count(0)
        {
        }

        TaskBase *m_first;
        TaskBase *m_last;
        uint32_t m_count;
        uint32_t m_active_count;
    };
    ListHelper m_list_info[2];
//!< \endcond
};


}

#endif

