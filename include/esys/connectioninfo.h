/*!
 * \file esys/connectioninfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_CONNECTIONINFO_H__
#define __ESYS_CONNECTIONINFO_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <string>

namespace esys
{

class ESYS_API ConnectionInfo
{
public:
    typedef uint16_t Kind;
    static const Kind USB=1;

    ConnectionInfo(Kind kind);
    virtual ~ConnectionInfo();

    Kind GetKind();

    virtual const std::string &GetAddr()=0;
protected:
    Kind m_kind;
};

}

#endif
