/*!
 * \file esys/em/tasklist.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EM_TASKLIST_H__
#define __ESYS_EM_TASKLIST_H__

#include "esys/esys_defs.h"
#include "esys/tasklistbase.h"

namespace esys
{

namespace em
{

class ESYS_API TaskList: public TaskListBase
{
public:
    TaskList();
    virtual ~TaskList();

    virtual void Add(TaskBase *task_base);

    static TaskList &GetCurrent();
protected:
    static TaskList *s_task_list;
};

}

#ifdef ESYSEM
using namespace em;
#endif

}
#endif



