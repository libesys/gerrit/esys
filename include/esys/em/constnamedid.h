/*!
 * \file esys/em/constnamedid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EM_CONSTNAMEDID_H__
#define __ESYS_EM_CONSTNAMEDID_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

template<typename T, int N=0>
class ConstNamedId
{
public:
    ConstNamedId(uint16_t id, const char *name=NULL): m_const(id), m_next(m_first), m_name(name)
    {
        m_first=this;
        m_count++;
    }

    static uint16_t GetNbr()
    {
        return m_count;
    }

    static const ConstNamedId *Get(uint16_t idx)
    {
        const ConstNamedId *p;

        if (idx>=GetNbr())
            return NULL;

        p=m_first;
        while (p && (idx>0))
        {
            p=p->m_next;
            idx--;
        }
        if (idx==0)
            return p;
        return NULL;
    }

    const uint32_t GetValue() const
    {
        return m_const;
    }
    operator const uint32_t()
    {
        return m_const;
    }
    bool operator==(const ConstNamedId& c) const
    {
        return (c.m_const==m_const);
    }
    bool operator!=(const ConstNamedId& c) const
    {
        return (c.m_const!=m_const);
    }
    const char *GetName() const
    {
        return m_name;
    }
protected:
    static uint16_t m_count;
    static ConstNamedId *m_first;
    ConstNamedId *m_next;
    const uint32_t m_const;
    const char *m_name;
};

template<typename T,int N> uint16_t ConstNamedId<T,N>::m_count=0;
template<typename T,int N> ConstNamedId<T,N> *ConstNamedId<T,N>::m_first=NULL;
}

#endif
