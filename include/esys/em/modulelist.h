/*!
 * \file esys/em/modulelist.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EM_MODULELIST_H__
#define __ESYS_EM_MODULELIST_H__

#include "esys/esys_defs.h"
#include "esys/modulelistbase.h"

namespace esys
{

namespace em
{

/*! \class ModuleList esys/em/modulelist.h "esys/em/modulelist.h"
 * \brief Module list for embedded build
 *
 */
class ESYS_API ModuleList : public ModuleListBase
{
public:
    //! Constructor
    ModuleList();

    //! Destructor
    ~ModuleList();

    static ModuleList &GetCurrent();
protected:
//!< \cond DOXY_IMPL
    static ModuleList *m_dft_list_mod;
//!< \endcond
};

}

#ifdef ESYSEM
using namespace em;
#endif

}

#endif




