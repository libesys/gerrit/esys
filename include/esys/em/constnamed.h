/*!
 * \file esys/em/constnamed.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EM_CONSTNAMED_H__
#define __ESYS_EM_CONSTNAMED_H__

#include "esys/em/constnamedid.h"

namespace esys
{

template<typename T, int N=0>
class ConstNamed: public ConstNamedId<T, N>
{
public:
    ConstNamed(const char *name=NULL): ConstNamedId<T, N>(m_next_id++)
    {
    }

protected:
    static uint32_t m_next_id;
};

template<typename T,int N> uint32_t ConstNamed<T,N>::m_next_id=0;
}

#endif
