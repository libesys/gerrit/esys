/*!
 * \file esys/criticalsectionbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"

namespace esys
{

/*!
 * \brief Base class for critical sections.
 *
 * This is an abstract representation of critical sections to support actual critical sections
 * (aka CLI/STI) on real HW and mutex based emulation on virtual HW, so that the code can be
 * tested on VHW reasonably realistically.
 *
 * \note Consider this a temporary solution. This was required for fixing the current
 *       esysio::IntPinMngr, but there are better solutions (requiring refactoring ESysIO).
*/
class ESYS_API CriticalSectionBase
{
public:
    CriticalSectionBase();

    virtual ~CriticalSectionBase();

    /*!
     * \brief Enter section.
     *
     * This method is used to enter the section from normal code (anything other than interrupt
     * context).
    */
    virtual void Enter() = 0;

    /*!
     * \brief Enter section.
     *
     * This method is used to exit the section from normal code (anything other than interrupt
     * context).
     */
    virtual void Exit() = 0;

    /*!
     * \brief Enter section from ISR.
     *
     * This method is used to enter the section from an interrupt service routine (ISR).
     * \note On RTOS platforms this typically is an empty implementation.
     */
    virtual void ISREnter() = 0;

    /*!
     * \brief Enter section from ISR.
     *
     * This method is used to enter the section from an interrupt service routine (ISR).
     * \note On RTOS platforms this typically is an empty implementation.
     */
    virtual void ISRExit() = 0;
};

}
