/*!
 * \file esys/usbdevinfobase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_USBDEVINFOBASE_H__
#define __ESYS_USBDEVINFOBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/connection.h"
#include "esys/usbaddr.h"

namespace esys
{

class ESYS_API USBEnumBase;
namespace multios
{
class ESYS_API USBEnumBase;
}

/*! \class USBDevInfoBase esys/usbdevinfobase.h "esys/usbdevinfobase.h"
 *  \brief The base class for storing information about a USB Device.
 */
class ESYS_API USBDevInfoBase: public virtual Connection
{
public:
    friend class ESYS_API multios::USBEnumBase;

    typedef uint8_t Kind;
    static const Kind BASIC=0;
    static const Kind SERIALPORT=1;

    USBDevInfoBase(Kind kind=BASIC);
    virtual ~USBDevInfoBase();

    //! Return the kind of USB Device information
    /*! \return SERIALPORT if the connection to the USB Device is a serial port, BASIC otherwise.
     */
    Kind GetKind();

    //! Return the USB Address of the USB Device
    /* Note that the USBAddr is platform dependent.
     */
    USBAddr &GetUSBAddr();

    virtual bool operator==(const USBDevInfoBase& obj) const=0;
    inline bool operator!=(const USBDevInfoBase& obj) const
    {
        return !operator==(obj);
    }

    uint32_t GetId();

    //! Return true if the USB Device information is valid.
    /* The information about a USB Device may become invalid if the USB Device is
     * unplugged, not supported or in an unoperational state.
     * \return true if valid, false otherwise
     */
    bool IsValid();

    //! Return reference count of this USB Device Information instance.
    /* As long as the reference count is >0, this instance will be kept, otherwise
     * garbage collecting may occur and delete this instance.
     * \return the reference count.
     */
    int32_t GetRefCount();

    //! States that this instance is in use, which increases the reference count
    void Use();

    //! States that this instance is not in use anymore, which decreases the reference count
    /* If by calling this function, the reference count reaches 0, this instance may be deleted
     * immediatly or shortly. It is thus unsafe to access this instance after calling this function.
     */
    void Release();
protected:
    //! Sets the validity of this instance.
    void SetValid(bool valid=true);
    void SetUSBEnum(USBEnumBase *usb_enum);

    USBAddr m_usb_addr;         //!< The USB Address
    Kind m_kind;                //!< The kind of information
    uint32_t m_id;              //!< The unique id
    static uint32_t m_next_id;  //!< The next unique id
    bool m_valid;               //!< True if the information is valid, false otherwise
    int32_t m_ref_count;        //!< The reference count
    USBEnumBase *m_usb_enum;    //!< The manager class of USBDevInfoBase instances.
};

}

#endif