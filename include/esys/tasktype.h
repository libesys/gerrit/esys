/*!
 * \file esys/tasktype.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASKTYPE_H__
#define __ESYS_TASKTYPE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

class ESYS_API TaskType
{
public:

    static const int32_t NOT_SET=-1;
    static const int32_t INTERNAL=0;
    static const int32_t APPLICATION=1;

    TaskType();
    TaskType(const int32_t &value);

    operator int32_t();
    int32_t GetValue() const;
protected:
    int32_t m_value;
};

}

/*inline bool operator==(const esys::TaskType& lhs, const esys::TaskType& rhs)
{
    return (lhs.GetValue() == rhs.GetValue());
}

inline bool operator!=(const esys::TaskType& lhs, const esys::TaskType& rhs)
{
    return !(lhs == rhs);
} */

#endif
