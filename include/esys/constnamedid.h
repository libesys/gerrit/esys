/*!
 * \file esys/constnamedid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_CONSTNAMEDID_H__
#define __ESYS_CONSTNAMEDID_H__

#if defined ARDUINO || defined ESYSEM
#include <esys/em/constnamedid.h>
#elif defined WIN32 || defined MULTIOS
#include <esys/multios/constnamedid.h>
#else

#endif

#endif
