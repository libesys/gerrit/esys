/*!
 * \file esys/systemtask.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSTEMTASK_H__
#define __ESYS_SYSTEMTASK_H__

#include "esys/esys_defs.h"
#include "esys/task.h"

namespace esys
{

/*! \class SystemTask esys/systemtask.h "esys/systetask.h"
 *  \brief Implements the platform independent SystemTask API
 */
class ESYS_API SystemTask: public Task
{
public:
    SystemTask(const ObjectName &name="");
    virtual ~SystemTask();

    virtual int32_t Entry() override;
    virtual int32_t ExecLoop();

    void AllAppTasksDone();

    static SystemTask &GetCurrent();
protected:
    static void SetCurrent(SystemTask *current);
#ifndef ESYS_MULTI_SYS
    static SystemTask *m_current;
#endif
};

}

#endif


