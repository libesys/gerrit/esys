/*!
 * \file esys/logchannel.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

namespace esys
{

/*! \class LogChannel esys/logchannel.h "esys/logchannel.h"
 *  \brief Base class of all Logging Channels
 */
class ESYS_API LogChannel
{
public:
    LogChannel();
    virtual ~LogChannel();

    //! Log a null terminated string
    /*! \param buf the null terminated string
    */
    virtual void PutStr(const char *buf) = 0;


    virtual void PutStr(char *buf, uint16_t size) = 0;

    //! Log binary data
    /*!
    * \param buf the buffer of data
    * \param size the size in bytes of the data buffer
    */
    virtual void Put(uint8_t *buf, uint16_t size)=0;

    //! Force a flush of all logging data
    /*!
    */
    virtual void Flush()=0;

    //! Set channel as active or not
    /*! Only an active channel can log data.
    *
    * \param active true if the channel is active, false if non active
    */
    void SetActive(bool active);

    //! Returns the activity of the Channel
    /*! \return true if the Channel is active, false otherwise
    */
    bool GetActive();

    //! Enable or disable a Channel
    /*! Only an active channel can be enabled or disabled. This is used to temporarily
    * stops the logging on a Channel
    * \param enable true to enable the Channel, false to disable it
    */
    void SetEnable(bool enable);

    //! Tells if the Channel is enabled or disabled
    /*! \return true if the Channel is enabled, false if disabled
    */
    bool GetEnable();
protected:
//!< \cond DOXY_IMPL
    bool m_active;      //!< True if the Channel is active, false if inactive
    bool m_enable;      //!< True if the Channel is enabled, false if disabled
//!< \endcond
};

}




