/*!
 * \file esys/criticalsection.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"

#ifdef ESYS_USE_FREERTOS
#include "esys/freertos/criticalsection.h"
#else
#define ESYS_USE_GEN_CRITICAL_SECTION
#include "esys/gen/criticalsection.h"
#endif


