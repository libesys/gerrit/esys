/*!
 * \file esys/serialportimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SERIALPORTIMPL_H__
#define __ESYS_SERIALPORTIMPL_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

namespace esys
{

//class ESYS_API USBDevSerialPortInfo;
class ESYS_API USBDevInfo;

class ESYS_API SerialPortImpl
{
public:
    //SerialPortImpl(USBDevSerialPortInfo *com_info);
    SerialPortImpl(USBDevInfo *com_info);
    virtual ~SerialPortImpl();

    virtual int16_t Open()=0;
    virtual int16_t Close()=0;

    virtual int16_t Write(uint8_t val)=0;
    virtual int16_t Write(uint8_t *buf,uint16_t size)=0;
    virtual int16_t Write(const char *cmd,bool enldn=true)=0;

    virtual int16_t Read(uint8_t *buf,uint16_t size)=0;
    virtual int16_t Read(uint8_t &val)=0;
    virtual int16_t Available()=0;

    virtual int16_t ClearRXBuffer()=0;

    virtual void Removed();
protected:
    //USBDevSerialPortInfo *m_com_info;
    USBDevInfo *m_com_info;
    bool m_is_open;
};

}

#endif
