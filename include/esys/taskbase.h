/*!
 * \file esys/taskbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASKBASE_H__
#define __ESYS_TASKBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/objectname.h"
#include "esys/objectnode.h"
#include "esys/runtimelevel.h"
#include "esys/semaphore.h"
#include "esys/tasklist.h"
#include "esys/tasktype.h"
//#include "esys/scheduler/taskbaseif.h"

#include "esys/taskplatif.h"

namespace esys
{

class ESYS_API TaskMngrBase;
//class ESYS_API TaskPlatIf;

/*! \class TaskBase esys/taskbase.h "esys/taskbase.h"
 *  \brief Implements the platform independent Task API
 */
class ESYS_API TaskBase: public ObjectNode, /*public virtual TaskBaseIf,*/ public virtual TaskPlatIf
{
public:

    enum class State
    {
        INSTANCIATED = 0,
        INITILIAZED,
        STARTED,
        STOPPING,
        STOPPED
    };

    enum Priority
    {
        LOWEST0=0,
        LOWEST1,
        LOWEST2,
        LOWEST3,
        LOW0,
        LOW1,
        LOW2,
        LOW3,
        HIGH0,
        HIGH1,
        HIGH2,
        HIGH3,
        HIGHEST0,
        HIGHEST1,
        HIGHEST2,
        HIGHEST3,
        CRITICAL0,
        CRITICAL1,
        CRITICAL2,
        CRITICAL3,
    };

    static const uint32_t DEFAULT_STACK_SIZE = 512;

    //! Constructor
    /*!
     *  \param[in] name The name of the Task
     *  \param[in] type the type of the Task
     */
    TaskBase(const ObjectName &name, TaskType type = TaskType::APPLICATION);

    //! Destructor
    virtual ~TaskBase();

    virtual int32_t StaticInit(Order order) override;
    virtual int32_t RuntimeInit(RuntimeLevel runtime_lvl, Order order) override;

    //! The entry function of the Task
    /*!
     *  \return <0 if an error occured
     */
    virtual int32_t Entry() = 0;


    virtual int32_t Started();
    virtual int32_t Done();

    virtual int32_t StopTask();

    //! Set the type of the Task
    /*!
     *  \param[in] typ the type of Task
     */
    void SetType(TaskType typ);

    //! Return the type of the Task
    /*!
     *  \return the type of Task
     */
    TaskType GetType();

    //! Set the state of the Task
    /*!
     *  \param[in] state the state of Task
     */
    void SetState(State state);

    //! Get the state of the Task
    /*!
     *  \return the state of Task
     */
    State GetState();

    void SetNextTask(TaskBase *next_task);
    TaskBase *GetNextTask();

    void WaitRuntimeInitDone();

    //! Set the exit value of the Task
    /*!
     *  \param[in] exit_value the exit value of the Task
     */
    void SetExitValue(int32_t exit_value);

    //! Get the exit value of the Task
    /*!
     *  return the exit value of the Task
     */
    int32_t GetExitValue();

    //! Set the TaskMngr owning this Task
    /*!
     *  \param[in] task_mngr_base the TaskMngr owning the Task
     */
    virtual void SetMngrBase(TaskMngrBase *task_mngr_base);

    //! Return the TaskMngr owning this Task
    /*!
     *  \return the TaskMngr owning the Task
     */
    TaskMngrBase *GetMngrBase();

    //! Set the runtime level of the Task
    /*!
     *  \param[in] runtime_lvl the RuntimeLevel of the Task
     */
    void SetRuntimeLevel(RuntimeLevel runtime_lvl);

    //! Return the runtime level of the Task
    /*!
     *  \return the RuntimeLevel of the Task
     */
    RuntimeLevel GetRuntimeLevel();

    //! Get the stack size of the Task
    /*! This function gives the size of the allocated stack, not the size of the stack during runtime.
     *
     *  \return the size of the stack allocated for this Task
     */
    uint32_t GetStackSize();

    //! Set stack size of the Task
    /*! This must be called before the static initialization to make certain it will
     *  work properly in all cases. Depending of the underlying RTOS, the allocation of the stack
     *  for this Task maybe be done either during the static or runtime initialization.
     *
     *  \param[in] stack_size the stack size of the Task
     */
    void SetStackSize(uint32_t stack_size);

    Priority GetPriority();
    void SetPriority(Priority priority);

protected:
//!< \cond DOXY_IMPL
    static TaskList s_task_list;
    TaskMngrBase *m_task_mngr_base;
    int32_t m_exit_value;
    RuntimeLevel m_runtime_lvl;
    Semaphore m_sem;
    TaskType m_type;
    TaskBase *m_next_task;
    State m_state;
    uint32_t m_stack_size;
    Priority m_priority;
//!< \endcond
};

}

#endif

