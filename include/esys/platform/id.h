/*!
 * \file esys/platform/id.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_PLATFORM_ID_H__
#define __ESYS_PLATFORM_ID_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#define ESYS_PLAT_MULTIOS_WX    0
#define ESYS_PLAT_BOOST         1
#define ESYS_PLAT_SYSC          2
#define ESYS_PLAT_ARDUINO       3
#define ESYS_PLAT_MULTI_PLAT    4
#define ESYS_PLAT_FREERTOS		5

#ifdef ESYS_MULTI_PLAT

#include <string>
#include <map>

namespace esys
{

namespace platform
{

class ESYS_API Id
{
public:
    Id(uint_t id, const std::wstring &name);
    ~Id();

    operator uint_t() const;
    const std::wstring &GetName();
    const uint_t GetId() const;

    static Id *Get(uint_t id);
    static Id *Find(const std::wstring &name);
protected:
    const uint_t m_id;

    static void Populate();
    static Id *m_last;
    static uint_t m_count;
    static std::map<uint_t, Id*> m_map_ids;
    static std::map<std::wstring, Id*> m_map_name_ids;

    Id *m_prev;

    const std::wstring m_name;
};

template<uint_t N>
class Id_t
{
public:
    static const uint_t ID=N;

    Id_t(const Id &id);

    const Id &GetId() const;
protected:
    const Id &m_id;
};

template<uint_t N>
Id_t<N>::Id_t(const Id &id): m_id(id)
{
}

template<uint_t N>
const Id &Id_t<N>::GetId() const
{
    return m_id;
}

ESYS_API extern const Id MULTIOS_WX;
ESYS_API extern const Id BOOST;
ESYS_API extern const Id SYSC;
ESYS_API extern const Id MULTI_PLAT;
ESYS_API extern const Id FREERTOS;
//static const Id ARD;

/*class MultiOSwx_tt: public Id_t<PLATFORM_MULTIOS_WX>
{
public:
    static const Id &m_idid=MULTIOS_WX;
}; */

typedef Id_t<ESYS_PLAT_MULTIOS_WX> MultiOSwx_t;
typedef Id_t<ESYS_PLAT_BOOST> Boost_t;
typedef Id_t<ESYS_PLAT_SYSC> SysC_t;
typedef Id_t<ESYS_PLAT_MULTI_PLAT> MultiPlat_t;
typedef Id_t<ESYS_PLAT_MULTI_PLAT> FreeRTOS_t;

ESYS_API extern const MultiOSwx_t MultiOSwx;
ESYS_API extern const Boost_t Boost;
ESYS_API extern const SysC_t SysC;
ESYS_API extern const MultiPlat_t MultiPlat;
ESYS_API extern const FreeRTOS_t FreeRTOS;

}

}

#endif

#endif
