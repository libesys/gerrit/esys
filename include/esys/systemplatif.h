/*!
 * \file esys/systemplatif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSTEMPLATIF_H__
#define __ESYS_SYSTEMPLATIF_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/types.h"

namespace esys
{

/*! \class SystemPlatIf esys/systemplatif.h "esys/systemplatif.h"
 *  \brief Defines the platform dependent System API when there is a scheduler
 */
class ESYS_API SystemPlatIf
{
public:
    virtual ~SystemPlatIf();

    virtual int32_t PlatInit() = 0;
    virtual int32_t PlatRelease() = 0;
};

}

#endif



