/*!
 * \file esys/timebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

class ESYS_API TimeBase
{
public:
    typedef uint64_t Stamp;
    enum Source
    {
        DEFAULT_CLK,
        MCU_CLK_BASED,
        PERMANENT_EXT_CLK_BASED
    };

    struct TimeStruct
    {
        TimeStruct() : m_hour(0), m_minutes(0), m_seconds(0)
        {
        }

        uint8_t seconds()
        {
            return m_seconds;
        }

        uint8_t minutes()
        {
            return m_minutes;
        }

        uint8_t hours()
        {
            return m_hour;
        }

        uint8_t m_hour;
        uint8_t m_minutes;
        uint8_t m_seconds;
    };

    struct DateStruct
    {
        DateStruct() : m_year(0), m_month(0), m_day(0)
        {
        }

        uint8_t day()
        {
            return m_day;
        }

        uint8_t month()
        {
            return m_month;
        }

        uint16_t year()
        {
            return m_year;
        }

        uint16_t m_year;
        uint8_t m_month;
        uint8_t m_day;
    };

    struct DateTimeStruct
    {
        DateTimeStruct() : m_date(), m_time()
        {
        }
        // create DateTimeStruct from EPOCH time (ms from 1.1.1970)
        DateTimeStruct(esys::uint64_t ms);
        void SetEpochMS(esys::uint64_t ms);

        DateStruct m_date;
        TimeStruct m_time;

        DateStruct &date()
        {
            return m_date;
        }

        TimeStruct &time()
        {
            return m_time;
        }
    };

    static void SetDftTimeSrc(Source dft_time_source);
    static Source GetDftTimeSrc();

    //convert EPOC time (days from 1.1.1970) to civil calendar time UTC.
    static void CivilFromDays(esys::uint64_t z, esys::int32_t *py, esys::uint32_t *pm, esys::uint32_t *pd);
protected:
    static Source m_dft_time_source;
};

}


