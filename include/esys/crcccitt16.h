/*!
 * \file esys/crcccitt16.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

#ifndef ESYS_GEN_CRCCCITT16
#define ESYS_GEN_CRCCCITT16
#endif

#include "esys/gen/crcccitt16.h"
