/*!
 * \file esys/config.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_CONFIG_H__
#define __ESYS_CONFIG_H__

#ifdef ESYS_USE_ARD
#elif defined(ESYSEM) || defined(ESYS_HW)
#else
#define ESYS_MULTI_PLAT 1
#define ESYS_OBJECT_META 1
#endif

#endif

