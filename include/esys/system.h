/*!
 * \file esys/system.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSTEM_H__
#define __ESYS_SYSTEM_H__

#include "esys/esys_setup.h"

#ifdef ESYS_VHW
#ifdef ESYS_MULTI_PLAT
#include "esys/mp/system.h"
#else
#endif
#elif defined ESYS_USE_FREERTOS
#include "esys/freertos/system.h"
#else
#error One of those must be defined: ESYS_VHW, ESYS_FREERTOS
#endif

#endif


