/*!
 * \file esys/types.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TYPES_H__
#define __ESYS_TYPES_H__

#include "esys/inttypes.h"

namespace esys
{

#ifdef ARDUINO
typedef uint32_t millis_t;
typedef uint32_t micros_t;
#elif defined(ESYSEM)
typedef uint32_t millis_t;
typedef uint32_t micros_t;
#elif defined WIN32 || defined MULTIOS || defined __GNUC__ || defined __GNUG__
typedef uint64_t millis_t;
typedef uint64_t micros_t;
#endif

}

#endif
