/*!
 * \file esys/esys_defs.h
 * \brief Definitions needed for esys
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYS_EXPORTS
#define ESYS_API __declspec(dllexport)
#elif ESYS_USE
#define ESYS_API __declspec(dllimport)
#else
#define ESYS_API
#endif

#ifdef _MSC_VER
#pragma warning( disable : 4250)    //At least for Visual c++ 2008
#pragma warning (disable : 4251)
#endif

#include "esys/autolink.h"

#include "esys/inttypes.h"
#include "esys/types.h"

