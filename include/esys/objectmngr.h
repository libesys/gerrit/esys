/*!
 * \file esys/objectmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_OBJECTMNGR_H__
#define __ESYS_OBJECTMNGR_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/objectnode.h"

namespace esys
{

class ESYS_API ObjectName;

/*! \class ObjectMngr esys/objectmngr.h "esys/objectmngr.h"
 *  \brief Manage a tree of Objects
 *
 */
class ESYS_API ObjectMngr: public ObjectNode
{
public:
    friend class ESYS_API Object;
    friend class ESYS_API ObjectName;

    static const Order ONLY_CHILDREN = 2;
    static const Order EVERYTHING = 3;

    //! Constructor
    /*!
     *  \param[in] name The name of the ObjectMngr
     *  \param[in] top true if this ObjectMngr is at the top, the root of an Object tree
     */
    ObjectMngr(const ObjectName &name, bool top=false);

    //! Destructor
    virtual ~ObjectMngr();

    //! Static initialization of all children handled by this ObjectMngr
    /*! Compared to the class Object, the ObjectMngr defines 2 additional value for the parameter order:
     *  the default is EVERTHING, which will be called only on the top ObjectMngr in a tree of Objects.
     *  This is important because an ObjectMngr is also an Object and thus an ObjectMngr can be a child
     *  of an Object or of another ObjectMngr. All other children ObjectMngr, so all others than the top one,
     *  will be initialized just as any other Object.
     *
     *  The new ONLY_CHIlDREN value is provided for completeness and if given as parameter, only the children
     *  will be initialized. Effectively, StaticInitBeforeChildren and StaticInitAfterChildren of the ObjectMngr
     *  won't be called.
     *
     *  \param[in] order define if the initialization is done after, before or is everything
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t StaticInit(Order order = EVERYTHING) override;

    //! Static release of all children handled by this ObjectMngr
    /*! The same said about EVERYTHING and ONLY_CHILDREN for StaticInit applies here as well.
     *
     *  \param[in] order define if the release is done after, before or is everything
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t StaticRelease(Order order = EVERYTHING) override;

    //! Runtime initialization run before starting the initialization of its children
    /*! The same said about EVERYTHING and ONLY_CHILDREN for StaticInit applies here as well.
     *
     *  \param[in] runtime_lvl the current Level of the runtime initialization
     *  \param[in] order define if the release is done after, before or is everything
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeInit(RuntimeLevel runtime_lvl = RuntimeLevel::ROOT, Order order = EVERYTHING) override;

    //! Runtime release run before starting the release of its children
    /*! The same said about EVERYTHING and ONLY_CHILDREN for StaticInit applies here as well.
     *
     *  \param[in] runtime_lvl the current Level of the runtime release
     *  \param[in] order define if the release is done after, before or is everything
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeRelease(RuntimeLevel runtime_lvl = RuntimeLevel::ROOT, Order order = EVERYTHING) override;

    //! Define if this ObjectMngr is the top Object and ObjectMngr in the tree of Objects
    /*!
     *  \param[in] top true of root of the Object tree, false otherwise
     */
    void SetTop(bool top = true);

    //! Tells if the ObjectMgr is the root, at the top of the Object tree
    /*!
     *  \return true if this ObjectMngr is the root of the Object tree, false otherwise
     */
    bool GetTop();
protected:
//!< \cond DOXY_IMPL
    //! Set the current ObjectMngr
    /*!
     *  \param[in] cur_mngr the current ObjectMngr
     */
    static void SetCurrent(ObjectMngr *cur_mngr);

    //! Return the current ObjectMngr
    /*!
     *  \return the current ObjectMngr
     */
    static ObjectMngr *GetCurrent();

    static ObjectMngr *g_cur_mngr;      //!< the current ObjectMngr

    bool m_top;                         //!< true if this ObjectMngr is the root of the Object tree, false otherwise
//!< \endcond
};

}

#endif

