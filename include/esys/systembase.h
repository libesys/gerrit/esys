/*!
 * \file esys/systembase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_SYSTEMBASE_H__
#define __ESYS_SYSTEMBASE_H__

#include "esys/esys_defs.h"
#include "esys/objectmngr.h"
#include "esys/systemplatif.h"

namespace esys
{

class ESYS_API Module;

class ESYS_API SystemBase : public ObjectMngr, public virtual SystemPlatIf
{
public:
    SystemBase(const ObjectName &name);
    virtual ~SystemBase();

    virtual int32_t Init();
    virtual int32_t Release();

    void StartInit(Module *module);
protected:
    uint32_t m_expected_module_id;
};

}

#endif


