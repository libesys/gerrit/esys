/*!
 * \file esys/setup.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/config.h"

#ifdef ESYS_USE_ARD
#elif defined(ESYSEM) || defined(ESYS_HW)
#else
#define ESYS_MULTI_PLAT 1
#define ESYS_OBJECT_META 1
#endif

#if defined(WIN32) || defined(LINUX)

#ifndef MULTIOS
#define MULTIOS
#endif
#ifndef ESYS_VHW
#define ESYS_VHW
#endif

#endif

#ifdef ESYS_DEV_CMAKE_BUILD

#elif ARDUINO
#include "esys/arduino/setup.h"
#elif defined(ESYSEM)
#include "esys/em/setup.h"
#elif defined WIN32
#include "esys/win32/setup.h"
#elif defined __GNUC__ || defined __GNUG__
#include "esys/unix/setup.h"
#endif

