/*!
 * \file esys/usbenumbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/usbenumfiltermngr.h"

namespace esys
{

class ESYS_API USBDevInfo;
class ESYS_API USBDevInfoBase;
// class ESYS_API USBDevSerialPortInfo;

/*! \class USBEnumBase esys/usbenumbase.h "usbenumbase.h"
 *  \brief The base class for all platform USB Enumeration
 */
class ESYS_API USBEnumBase
{
public:
    friend class ESYS_API USBDevInfoBase;

    USBEnumBase();
    virtual ~USBEnumBase();

    //! Refresh and rebuild the list of USB Devices.
    /*! \return 0 if the operation succeeded, <0 otherwise.
     */
    virtual int Refresh() = 0;

    //! Returns the number of USB Device Information found in the last Refresh
    /*! \return the number of USB Device Information
     */
    virtual std::size_t GetNbrDev() = 0;

    //! Returns a USB Device Information given by its index in the list of them
    /*! \return the USB Device Information
     */
    virtual USBDevInfo *GetDev(std::size_t idx) = 0;

    //! Returns the number of serial USB Device Information found in the last Refresh
    /*! \return the number of serial USB Device Information
     */
    virtual std::size_t GetNbrSerialPort() = 0;

    //! Returns a serial USB Device Information given by its index in the list of them
    /*! \return the USB Device Information
     */
    virtual USBDevInfo *GetSerialPort(std::size_t idx) = 0;

    virtual void SetFilterMngr(USBEnumFilterMngr *filter_mngr);
    USBEnumFilterMngr *GetFilterMngr();

    virtual std::size_t GetNbrFiltered();
    virtual USBDevInfo *GetFiltered(std::size_t idx);

    void ClearFiltered();

    virtual void SetVerbose(bool verbose = true);

protected:
    //! Called when a USBDevInfoBase is not in used anymore
    virtual void Released(USBDevInfoBase *obj) = 0;

    USBEnumFilterMngr *m_filter_mngr;

    bool m_verbose;
};

} // namespace esys
