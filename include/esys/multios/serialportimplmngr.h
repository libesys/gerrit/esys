/*!
 * \file esys/multios/serialportimplmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_SERIALPORTIMPLMNGR_H__
#define __ESYS_MULTIOS_SERIALPORTIMPLMNGR_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/serialport.h>

#include <vector>

namespace esys
{

//class ESYS_API USBDevSerialPortInfo;
class ESYS_API USBDevInfo;
class ESYS_API SerialPortImpl;

class ESYS_API SerialPortImplMngr
{
public:
    SerialPortImplMngr();
    virtual ~SerialPortImplMngr();

    //SerialPort::Handle Get(USBDevSerialPortInfo *com_info,int type);
    SerialPort::Handle Get(USBDevInfo *com_info,int type);

    static SerialPortImplMngr &Get();
protected:
    static SerialPortImplMngr s_mngr;

    std::vector<SerialPortImpl *> m_impls;
    std::vector<SerialPort *> m_ports;
};

}

#endif
