/*!
 * \file esys/wx/taskwx.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WX_TASK_H__
#define __ESYS_WX_TASK_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbaseif.h"
#include "esys/evtloop/taskbase.h"

namespace esys
{

namespace wx
{

class ESYS_API TaskImpl;

/*! \class Task esys/wx/taskwx.h "esys/wx/taskwx.h"
 *  \brief wxWdiget implementation of the Multios Task
 */
class ESYS_API Task: public evtloop::TaskBaseIf
{
public:
    friend class ESYS_API ESys;

    Task(const char *name);
    virtual ~Task();

    virtual millis_t Millis();
    virtual void Wait();
    virtual void Signal();

    virtual void DoLoopIterationEnds();

    static Task *Current();
    static evtloop::TaskBase *CurrentBase();

    virtual void Sleep(millis_t ms);
    virtual void SleepU(micros_t us);
protected:
    TaskImpl *m_impl;

    //wxLongLong m_started_time;
};

}

}

#endif