/*!
 * \file esys/multios/taskboost.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_TASKBOOST_H__
#define __ESYS_MULTIOS_TASKBOOST_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/evtloop/task.h>

//#include <boost/date_time/posix_time/posix_time.hpp>

namespace esys
{

namespace boost
{

class ESYS_API TaskImpl;

class ESYS_API Task: public evtloop::TaskBase
{
public:
    friend class ESYS_API ESys;

    Task(const char *name);
    virtual ~Task();

    virtual millis_t Millis();
    virtual void Wait();
    virtual void Signal();

    static Task *Current();
    static TaskBase *CurrentBase();

    virtual void Sleep(uint64_t val);
protected:
    TaskImpl *m_impl;
};

}

}

#endif