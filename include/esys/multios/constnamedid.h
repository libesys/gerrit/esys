/*!
 * \file esys/multios/constnamedid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_CONSTNAMEDID_H__
#define __ESYS_MULTIOS_CONSTNAMEDID_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <map>
#include <vector>
#include <string>
#include <ostream>

namespace esys
{

template<typename T, int N=0>
class ConstNamedId
{
public:
    ConstNamedId(uint32_t id, const char *name = nullptr) : m_const(id), m_name(name)
    {
        if (m_first == nullptr)
        {
            m_first = this;
            m_last = this;
        }
        else
        {
            m_last->SetNext(this);
            m_last = this;
        }
        m_count++;
    }

    static uint16_t GetNbr()
    {
        Populate();
        return (uint16_t)m_consts.size();
    }

    static const ConstNamedId *Get(uint16_t idx)
    {
        if (idx>=GetNbr())
            return NULL;
        return m_consts[idx];
    }

    static void Populate()
    {
        const ConstNamedId *p;

        if (m_map_name.size()==m_count)
            return;
        m_map_name.clear();
        m_map_id.clear();
        m_consts.clear();

        p=m_first;
        while (p)
        {
            m_map_name[p->m_name]=p;
            m_map_id[p->m_const]=p;
            m_consts.push_back(p);
            p=p->m_next;
        }
    }

    static const ConstNamedId *Find(const char *name)
    {
        return Find(std::string(name));
    }

    static const ConstNamedId *Find(const std::string &name)
    {
        typename std::map<std::string,const ConstNamedId *>::iterator it;

        Populate();

        it=m_map_name.find(name);
        if (it==m_map_name.end())
            return NULL;
        return it->second;
    }

    static const ConstNamedId *Find(uint32_t id)
    {
        typename std::map<uint32_t,const ConstNamedId *>::iterator it;

        Populate();

        it=m_map_id.find(id);
        if (it==m_map_id.end())
            return NULL;
        return it->second;
    }

    const uint32_t GetValue() const
    {
        return m_const;
    }

    operator const uint32_t() const
    {
        return m_const;
    }

    bool operator==(const ConstNamedId& c) const
    {
        return (c.m_const==m_const);
    }

    bool operator!=(const ConstNamedId& c) const
    {
        return (c.m_const!=m_const);
    }

    const char *GetName() const
    {
        return m_name;
    }
protected:
    void SetNext(ConstNamedId *next)
    {
        m_next = next;
    }

    static ConstNamedId *m_first;
    static ConstNamedId *m_last;
    static std::map<std::string,const ConstNamedId *> m_map_name;
    static std::map<uint32_t, const ConstNamedId *> m_map_id;
    static std::vector<const ConstNamedId *> m_consts;
    static uint32_t m_count;
    ConstNamedId *m_next;
    const uint32_t m_const;
    const char *m_name;
};

template<typename T, int N>
std::ostream &operator<< (std::ostream &os, const ConstNamedId<T,N> &p)
{
    os << std::string(p.GetName());
    return os;
}

template<typename T, int N> ConstNamedId<T, N> *ConstNamedId<T, N>::m_first = nullptr;
template<typename T, int N> ConstNamedId<T, N> *ConstNamedId<T, N>::m_last = nullptr;
template<typename T,int N> std::map<std::string,const ConstNamedId<T,N> *> ConstNamedId<T,N>::m_map_name;
template<typename T,int N> std::map<uint32_t,const ConstNamedId<T,N> *> ConstNamedId<T,N>::m_map_id;
template<typename T,int N> std::vector<const ConstNamedId<T,N> *> ConstNamedId<T,N>::m_consts;
template<typename T,int N> uint32_t ConstNamedId<T,N>::m_count=0;
}

#endif
