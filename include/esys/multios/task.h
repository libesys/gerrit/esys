/*!
 * \file esys/multios/task.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_TASK_H__
#define __ESYS_MULTIOS_TASK_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include <esys/evtloop/task.h>

#include <esys/multios/taskwx.h>
#include <esys/multios/taskboost.h>

namespace esys
{

#ifdef ESYS_USE_WX
//#define Task TaskWx
typedef wx::Task Task;
#elif defined ESYS_USE_BOOST
typedef boost::Task Task;
//typedef TaskBoost Task;
#endif

}

#endif

