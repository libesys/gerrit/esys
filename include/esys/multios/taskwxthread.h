/*!
 * \file esys/multios/taskwxthread.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_TASKWXTHREAD_H__
#define __ESYS_MULTIOS_TASKWXTHREAD_H__

#include "esys/esys_defs.h"

#include "esys/inttypes.h"
#include "esys/taskbase.h"

namespace esys
{

namespace wx
{

class ESYS_API TaskThreadBaseImpl;

class ESYS_API TaskThreadBase
{
public:
    TaskThreadBase(bool detached=true);
    virtual ~TaskThreadBase();

    void SetTask(esys::TaskBase *task);
    void YieldThread();
protected:
    TaskThreadBaseImpl *m_impl;
    bool m_exit;
};

/*template<typename T>
class TaskThread: public T, public TaskThreadBase
{
public:
    TaskThread(bool detached=true): T(), TaskThreadBase(detached)
    {
    }

    virtual ~TaskThread()
    {
    }

    virtual void Do()
    {
        while (!m_exit)
        {
            DoLoopIteration(true);
            YieldThread();
        }
    }
}; */

}

}

#endif
