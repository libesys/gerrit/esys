/*!
 * \file esys/multios/taskwximpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_TASKWXIMPL_H__
#define __ESYS_MULTIOS_TASKWXIMPL_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/taskbase.h>

#include <wx/longlong.h>
#include <wx/thread.h>

namespace esys
{

namespace wx
{

class ESYS_API Task;

class ESYS_API TaskImpl: public wxThread
{
public:
    friend class ESYS_API Task;

    TaskImpl(wxThreadKind kind = wxTHREAD_DETACHED);
    virtual ~TaskImpl();

    virtual millis_t Millis();
    virtual void Wait();
    virtual void Signal();

    virtual void DoLoopIterationEnds();

    virtual ExitCode Entry();

    static TaskImpl *GetCurrent();

    void SetTask(Task *task);
    Task *GetTask();
    virtual void Sleep(uint64_t val);
protected:
    wxLongLong m_started_time;
    Task *m_task;
    wxSemaphore m_sem;
    bool m_started;
};

}

}

#endif