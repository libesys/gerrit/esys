/*!
 * \file esys/multios/taskboostimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_TASKBOOSTIMPL_H__
#define __ESYS_MULTIOS_TASKBOOSTIMPL_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/task.h>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace esys
{

namespace boost
{

class ESYS_API TaskImpl
{
public:
    friend class ESYS_API ESys;
    friend class ESYS_API Task;

    TaskImpl();
    virtual ~TaskImpl();

    virtual millis_t Millis();
    virtual void Wait();
    virtual void Signal();

    static TaskImpl *Current();
    void SetTask(Task *task);
    Task *GetTask();

    virtual void Sleep(uint64_t val);
protected:
    Task *m_task;
    ::boost::posix_time::ptime m_started_time;
};

}

}

#endif