/*!
 * \file esys/multios/usbenumbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/usbenumbase.h"
#include "esys/usbdevinfo.h"

#include <vector>
#include <map>

namespace esys
{

namespace multios
{

/*! \class USBEnumBase esys/multios/usbenumbase.h "esys/multios/usbenumbase.h"
 *  \brief The base class for storing information about a USB Device using c++ std
 */
class ESYS_API USBEnumBase : public esys::USBEnumBase
{
public:
    USBEnumBase();
    virtual ~USBEnumBase();

    virtual std::size_t GetNbrDev() override;
    virtual USBDevInfo *GetDev(std::size_t idx) override;

    virtual std::size_t GetNbrSerialPort() override;
    virtual USBDevInfo *GetSerialPort(std::size_t idx) override;

    USBDevInfo *Find(const std::string &location, std::map<std::string, USBDevInfo *>::iterator *it_ = NULL);

protected:
    void Add(USBDevInfo *obj);
    void AddSerial(USBDevInfo *obj);
    USBDevInfo *FindParent(USBDevInfo *child);
    void FindParentsOfPendingChildren();

    virtual void Released(USBDevInfoBase *obj);
    void Remove(USBDevInfo *obj);
    void Remove(USBDevInfo *obj, std::vector<USBDevInfo *> &list);
    void Remove(USBDevInfo *obj, std::map<std::string, USBDevInfo *> &map_dev);
    void RefreshFound(std::map<std::string, USBDevInfo *> &found);

    std::vector<USBDevInfo *> m_dev_infos;             //!< All valid USBDevInfo
    std::vector<USBDevInfo *> m_child_dev_infos;       //!< Temporary list of child without parents
    std::vector<USBDevInfo *> m_all_dev_infos;         //!< All USBDevInfo valid or invalid
    std::vector<USBDevInfo *> m_dev_serial_port_infos; //!< All valid serial USBDevInfo
    std::map<std::string, USBDevInfo *> m_map_dev_infos;
    std::map<std::string, USBDevInfo *> m_map_loc_path_dev_infos;
};

} // namespace multios

} // namespace esys
