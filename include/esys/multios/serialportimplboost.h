/*!
 * \file esys/serialportimplboost.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_SERIALPORTIMPLBOOST_H__
#define __ESYS_MULTIOS_SERIALPORTIMPLBOOST_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/serialportimpl.h>

#ifdef ESYS_BOOST

namespace esys
{

class ESYS_API USBDevSerialPortInfo;

class ESYS_API SerialPortImplBoost:public SerialPortImpl
{
public:
    SerialPortImplBoost(USBDevSerialPortInfo *com_info);
    virtual ~SerialPortImplBoost();

    virtual int16_t Open();
    virtual int16_t Close();

    virtual int16_t Write(uint8_t val);
    virtual int16_t Write(uint8_t *buf,uint16_t size);
    virtual int16_t Write(const char *cmd,bool enldn=true);

    virtual int16_t Read(uint8_t *buf,uint16_t size);
    virtual int16_t Read(uint8_t &val);
    virtual int16_t Available();

    virtual int16_t ClearRXBuffer();
protected:

};

}

#endif

#endif

