/*!
 * \file esys/multios/serialportimplwxctb.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MULTIOS_SERIALPORTIMPLWXCTB_H__
#define __ESYS_MULTIOS_SERIALPORTIMPLWXCTB_H__

#ifdef ESYS_USE_WX

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/serialportimpl.h>

#include <wx/ctb/serport.h>

namespace esys
{

//class ESYS_API USBDevSerialPortInfo;
class ESYS_API USBDevInfo;

class ESYS_API SerialPortImplWxCTB:public SerialPortImpl
{
public:
    //SerialPortImplWxCTB(USBDevSerialPortInfo *com_info);
    SerialPortImplWxCTB(USBDevInfo *com_info);
    virtual ~SerialPortImplWxCTB();

    virtual int16_t Open();
    virtual int16_t Close();

    virtual int16_t Write(uint8_t val);
    virtual int16_t Write(uint8_t *buf,uint16_t size);
    virtual int16_t Write(const char *cmd,bool enldn=true);

    virtual int16_t Read(uint8_t *buf,uint16_t size);
    virtual int16_t Read(uint8_t &val);
    virtual int16_t Available();

    virtual int16_t ClearRXBuffer();

    virtual void Removed();
protected:
    wxSerialPort *m_com;
    wxSerialPort_DCS m_dft_dcs;
    std::string m_rx_buf;

};

}

#endif

#endif

