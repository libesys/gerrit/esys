/*!
 * \file esys/usbaddr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_USBADDR_H__
#define __ESYS_USBADDR_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <string>

namespace esys
{

class ESYS_API USBAddr
{
public:
    USBAddr();
    USBAddr(uint16_t vid, uint16_t pid);
    USBAddr(uint16_t vid, uint16_t pid, uint16_t rev);
    USBAddr(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi);
    virtual ~USBAddr();

    void Set(uint16_t vid, uint16_t pid);
    void Set(uint16_t vid, uint16_t pid, uint16_t rev);
    void Set(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi);
    void SetVID(uint16_t vid);
    void SetPID(uint16_t pid);
    void SetREV(uint16_t rev,bool present=true);
    void SetMI(uint16_t mi,bool present=true);
    uint16_t GetVID() const;
    uint16_t GetPID() const;
    uint16_t GetREV() const;
    bool IsREVSet();
    uint16_t GetMI() const;
    bool IsMISet();

    virtual const std::string &GetAddr(bool full = true);

    virtual bool operator==(const USBAddr &addr) const;
protected:
    uint16_t m_vid;
    uint16_t m_pid;
    uint16_t m_rev;
    bool m_is_rev_set;
    uint16_t m_mi;
    bool m_is_mi_set;
    std::string m_addr;
};

}

#endif
