/*!
 * \file esys/timerbase.h
 * \brief The base class of all Timer implementations
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/objectleaf.h"

namespace esys
{

class ESYS_API TimerCallbackBase;
class ESYS_API TimerMngrBase;

/*! \class TimerBase esys/timerbase.h "esys/timerbase.h"
 *  \brief Base class of all Timer implementations
 */
class ESYS_API TimerBase: public ObjectLeaf
{
public:
    TimerBase(const ObjectName &name);
    virtual ~TimerBase();

    //! Start the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Start(uint32_t ms, bool one_shot=false, bool from_isr = false) = 0;

    //! Stop the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Stop(bool from_isr = false) = 0;

    //! Return the Timer period in ms
    /*! \return the period in ms
    */
    virtual int32_t GetPeriod() = 0;

    //! Return the state of the Timer
    /*! \return True if the Timer is running, false otherwise
    */
    virtual bool IsRunning() = 0;

    virtual bool IsOneShot() = 0;

    virtual void Notify();

    virtual void SetCallback(TimerCallbackBase *callback);
    virtual TimerCallbackBase *GetCallback();

    int32_t StartOnce(uint32_t ms)
    {
        return Start(ms, true);
    }

    virtual void SetNext(TimerBase *next);
    virtual TimerBase *GetNext();

    virtual void SetMngr(TimerMngrBase *mngr);
    virtual TimerMngrBase &GetMngr();
protected:
//!< \cond DOXY_IMPL
    TimerCallbackBase *m_callback;
    TimerBase *m_next;
    TimerMngrBase *m_mngr;
//!< \endcond
};

}




