/*!
 * \file esys/pluginmngrcore.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_setup.h"

#if !defined(ESYS_PLUGIN_USE_WX) || defined(ESYS_PLUGIN_USE_BOOST)
#ifndef ESYS_PLUGIN_USE_BOOST
#define ESYS_PLUGIN_USE_BOOST
#endif
#include "esys/boost/pluginmngrcore.h"
#else
#ifndef ESYS_PLUGIN_USE_WX
#define ESYS_PLUGIN_USE_WX
#endif
#include "esys/wx/pluginmngrcore.h"
#endif
