/*!
 * \file esys/pluginbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

#include <cstddef>
#include <string>
#include <vector>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem/path.hpp>

namespace po = boost::program_options;

namespace esys
{

class ESYS_API PluginBase
{
public:
    friend class ESYS_API PluginMngrBase;

    PluginBase();
    virtual ~PluginBase();

    std::string &GetName();
    std::string &GetShortName();
    std::string &GetVersion();
    std::string &GetFileName();

    bool IsDebug();

    virtual int Init() = 0;
    virtual int Release() = 0;

    virtual int Run(const std::vector<std::string> &cmd_line, const po::variables_map &vm);

    virtual std::size_t GetNbrOptionsDesc();
    virtual po::options_description *GetOptionsDesc(std::size_t idx = 0);
    virtual void AddOptionsDesc(po::options_description *options_desc);

    const ::boost::filesystem::path &GetPath();
protected:
    void SetDebug(bool debug);
    void SetName(const std::string &name);
    void SetShortName(const std::string &short_name);
    void SetVersion(const std::string &version);
    void SetPath(const ::boost::filesystem::path &path);
    void SetFileName(const std::string &filename);

    bool m_is_debug = false;
    bool m_init = false;
    std::string m_name;
    std::string m_short_name;
    std::string m_version;
    std::vector<po::options_description *> m_options_desc;
    ::boost::filesystem::path m_path;
    std::string m_filename;
};

}
