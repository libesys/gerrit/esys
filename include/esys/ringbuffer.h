/*!
 * \file esys/ringbuffer.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_RINGBUFFER_H__
#define __ESYS_RINGBUFFER_H__

#include "esys/esys_defs.h"
#include "esys/assert.h"
#include "esys/inttypes.h"
#include "esys/buffer.h"

#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

class ESYS_API RingBufferBase
{
public:
    RingBufferBase();
    virtual ~RingBufferBase();

    //uint16_t GetReadIdx();
    //uint16_t GetWriteIdx();
protected:
    uint16_t m_nbr_el;
    uint16_t m_read_idx;
    uint16_t m_write_idx;
    uint16_t m_cur_idx;
};

/*template<typename T> class RingBuffer;
template<typename T, uint16_t nbr_el> class RingBufferStatic;

template<typename T>
class RingBufferStaticPtr: public Pointer<T>
{
public:
    template<typename T> friend class RingBuffer;
    template<typename T, uint16_t nbr_el> friend class RingBufferStatic;

    RingBufferStaticPtr(RingBuffer<T> *buf=NULL): m_buffer(buf)
    {
    }
    virtual ~RingBufferStaticPtr()
    {
    }

    virtual uint16_t GetNgrSegment(bool data=true)
    {

        return 0;
    }

    virtual T *GetSegment(uint16_t idx, uint16_t &size, bool data=true)
    {
        return NULL;
    }

    void SetBuffer(RingBuffer<T> *buf)
    {
        m_buffer=buf;
    }

    RingBuffer<T> *SetBuffer()
    {
        return m_buffer;
    }

    //virtual int16_t Peek(Pointer<T> **data)
    //{
    //
    //}

    virtual Pointer<T> &operator++(int)
    {
        assert(m_cur_idx<m_size);
        m_cur_idx++;
        return *this;
    }

    virtual T &operator[](uint16_t idx)
    {
        assert(idx<m_size);
        assert(m_buffer!=NULL);

        //return m_buf[(m_read_idx+idx) % MAX_NBR_EL];
        return m_buffer->GetAbsolute(m_read_idx+idx);
    }
    virtual T &operator*()
    {
        assert(m_buffer!=NULL);

        return m_buffer->GetAbsolute(m_read_idx+m_cur_idx);
        //return m_buf[m_read_idx % MAX_NBR_EL];
    }
protected:
    RingBuffer<T> *m_buffer;
    uint16_t m_read_idx;
    uint16_t m_cur_idx;
    uint16_t m_max_read_idx;
}; */

template <typename T>
class RingBuffer: public /*RingBufferBase,*/ Buffer<T>
{
public:
    //template<typename T> friend class RingBufferStaticPtr;

    //typedef RingBufferStaticPtr<T> Ptr;
    //typedef BufferWrapBasePtr<T, RingBuffer<T> > Ptr;
    typedef PointerBlock<T, RingBuffer<T> > Ptr;

    RingBuffer(): Buffer<T>(), /*RingBufferBase(),*/ m_read_idx(0), m_write_idx(0), m_cur_idx(0)
    {
        this->SetSize(0);
    }

    RingBuffer(uint16_t max_size): Buffer<T>(max_size), /*RingBufferBase(),*/ m_read_idx(0), m_write_idx(0), m_cur_idx(0)
    {
        this->SetSize(0);
    }

    virtual ~RingBuffer()
    {
    }

    virtual void Clear()
    {
        Buffer<T>::Clear();

        m_read_idx=0;
        m_write_idx=0;
        m_cur_idx=0;
    }

    Ptr Begin()
    {
        return Ptr(this);
    }

    Ptr Block(uint16_t idx=0)
    {
        return Ptr(this);
    }

    uint16_t GetReadIdx()
    {
        return m_read_idx;
    }

    uint16_t GetWriteIdx()
    {
        return m_write_idx;
    }

    virtual void Set(const T &val)
    {
        //assert(false); //Not implemented
        if ((val==0) || (sizeof(T)==1))
        {
            ::memset(GetData(), val, this->GetMaxSize()*sizeof(T));
        }
        else
        {
            for (uint16_t idx=0; idx<this->GetMaxSize(); ++idx)
            {
                GetData()[idx]=val;
            }
        }
    }

    virtual int16_t Set(const T &val, uint16_t nbr)
    {
        assert(false); //Not implemented
        /*if (nbr>GetSize())
            return -1;

        if (val==0)
        {
            ::memset(GetData(), 0, GetSize());
        }
        else
        {
            for (uint16_t idx; idx<GetSize(); ++idx)
            {
                GetData()[idx]=val;
            }
        }
        return 0; */
        return -1;
    }

    // Pointer functions
    int16_t Push(T *data, uint16_t size)
    {
        uint16_t nbr_seg=GetNbrSegment(false);
        uint16_t seg_size;
        uint16_t data_size=size;
        T *seg;

        if (nbr_seg==0)
            return -1;

        for (uint16_t idx=0; (idx<nbr_seg) && (size!=0) ; ++idx)
        {
            seg=GetSegment(idx, seg_size, false);
            assert(seg!=NULL);
            assert(seg_size!=0);

            if (seg_size>=size)
            {
                ::memcpy(seg, data, size);
                this->SetSize(this->GetSize()+size);
                this->m_write_idx=(this->m_write_idx+data_size) % this->GetMaxSize();
                return 0;
            }
            ::memcpy(seg, data, seg_size);
            this->SetSize(this->GetSize()+seg_size);
            data+=seg_size;
            size-=seg_size;
        }
        if (size!=0)
            return -1;
        this->m_write_idx=(this->m_write_idx+data_size) % this->GetMaxSize();
        return 0;
    }

    virtual int16_t Copy(const Ptr &dst, T *data, uint16_t size)
    {
        uint16_t nbr_seg=GetNbrSegment(true);
        uint16_t seg_size;
        uint16_t data_size=size;
        uint16_t dst_index;
        uint16_t data_index=0;
        T *seg;

        assert(data!=NULL);

        if (nbr_seg==0)
            return -1;
        if (size==0)
            return 0;

        dst_index=dst.GetIndex()+dst.GetOffset();

        for (uint16_t idx=0; (idx<nbr_seg) && (size!=0); ++idx)
        {
            seg=GetSegment(idx, seg_size, true);
            assert(seg!=NULL);
            assert(seg_size!=0);

            if (dst_index<seg_size)
            {
                //The first byte to be copied in located in this segment
                if (seg_size-dst_index>=data_size)
                {
                    //The current segment is large enough for the amount of data to be copied
                    ::memcpy(seg, data+data_index, data_size);
                    return 0;
                }
                else
                {
                    //The current segment doesn't have enough space for the whole data to be copied
                    ::memcpy(seg, data+data_index, seg_size-dst_index);
                    //Set the amount of data to be still copied and where is its first byte
                    data_size-=seg_size-dst_index;
                    data_index+=seg_size-dst_index;
                    //The data should be copied at the first byte from the following segment
                    dst_index=0;
                }
            }
            else
            {
                dst_index-=seg_size;
            }
        }
        return -1;
    }

    virtual int16_t Pop(uint16_t size)
    {
        assert(size<=this->GetSize());

        this->m_read_idx=(this->m_read_idx+size) % this->GetMaxSize();
        this->SetSize((this->m_write_idx-this->m_read_idx+this->GetMaxSize()) % this->GetMaxSize());
        if (this->m_read_idx==this->m_write_idx)
        {
            this->m_read_idx=0;
            this->m_write_idx=0;
        }
        return 0;
    }

    virtual uint16_t GetNbrSegment(bool data=true)
    {
        uint16_t r_idx;
        uint16_t w_idx;

        r_idx=m_read_idx % this->GetMaxSize();
        w_idx=m_write_idx % this->GetMaxSize();

        if (w_idx==r_idx)       // If the buffer is empty,
        {
            if (data)
                return 0;       // there is no data segment available;
            else
                return 1;       // but one free segment. Note this assumes that when the buffer is just emptied, read and write index shall be put back to 0.
        }
        else if (w_idx>r_idx)   // If the write index is higher in the buffer than the read index,
        {
            if (data)
                return 1;       // there is only 1 data segment;
            else
            {
                if (r_idx==0)
                    return 1;
                else
                    return 2;   // but there are then 2 free segments
            }
        }
        else if (w_idx<r_idx)   // If the read index is higher in the buffer than the write index, the data rolled over the end of the buffer,
        {
            if (data)
            {
                if (w_idx==0)
                    return 1;
                else
                    return 2;       // there are then 2 data segments;
            }
            else
                return 1;       // but only 1 free segments.
        }
        assert(false);
        return 0;
    }

    virtual T *GetSegment(uint16_t idx, uint16_t &size, bool data=true)
    {
        uint16_t nbr_seg=GetNbrSegment(data);
        uint16_t r_idx;
        uint16_t w_idx;

        if (idx>=nbr_seg)
        {
            size=0;
            return NULL;
        }

        r_idx=m_read_idx % this->GetMaxSize();
        w_idx=m_write_idx % this->GetMaxSize();

        if (w_idx==r_idx)
        {
            if (data)
            {
                size=0;
                return NULL;
            }
            else
            {
                size=this->GetMaxSize();
                return GetData();
            }
        }
        else if (w_idx>r_idx)
        {
            if (data)
            {
                assert(idx==0);

                size=w_idx-r_idx;
                return GetData()+r_idx;
            }
            else
            {
                assert(idx<2);

                if (r_idx==0)
                {
                    size=this->GetMaxSize()-w_idx;
                    return GetData()+w_idx;
                }
                else
                {
                    if (idx==0)
                    {
                        size=this->GetMaxSize()-w_idx;
                        return GetData()+w_idx;
                    }
                    else
                    {
                        size=r_idx;
                        return GetData();
                    }
                }
            }
        }
        else if (w_idx<r_idx)
        {
            if (data)
            {
                assert(idx<2);

                if (w_idx==0)
                {
                    size=this->GetMaxSize()-r_idx;
                    return GetData()+r_idx;
                }
                else
                {
                    if (idx==0)
                    {
                        size=w_idx;
                        return GetData();
                    }
                    else
                    {
                        size=this->GetMaxSize()-r_idx;
                        return GetData()+r_idx;
                    }
                }
            }
            else
            {
                assert(idx==0);

                size=r_idx-w_idx;
                return GetData()+w_idx;
            }
        }

        assert(false);
        return NULL;
    }

    /*virtual Pointer<T> &operator++(int)
    {
        assert((m_cur_idx % this->GetMaxSize())>= m_read_idx);
        assert((m_cur_idx % this->GetMaxSize())< m_write_idx);

        m_cur_idx++;
        return *this;
    } */

    virtual T &operator[](uint16_t idx)
    {
        assert(this->GetSize()!=0);
        //assert((this->m_offset+idx)<this->GetSize());
        assert(idx<this->GetSize());

        //return GetData()[(m_read_idx+this->m_offset+idx) % this->GetMaxSize()];
        return GetData()[(m_read_idx+idx) % this->GetMaxSize()];
    }

    virtual T &operator()(uint16_t idx)
    {
        assert(this->GetSize()!=0);
        assert(idx<this->GetSize());

        return GetData()[(m_read_idx+idx) % this->GetMaxSize()];
    }

    virtual T &operator*()
    {
        assert(this->GetSize()!=0);

        return GetData()[m_read_idx];
    }

    //Own interface
    //virtual int16_t Peek(Ptr &data)=0;
    //virtual operator T*()=0;
    //virtual int16_t Write(T *buf, uint16_t nbr_el)=0;

    virtual RingBuffer<T> &operator<<(const T& val)
    {
        GetData()[this->m_write_idx]=val;
        this->m_write_idx=(this->m_write_idx+1) % this->GetMaxSize();
        this->m_size++;
        return *this;
    }

    //RingBufferStatic<T, nbr_el> &operator>>(T& val)
    virtual RingBuffer<T> &operator>>(T& val)
    {
        assert(this->m_size>0);

        val=GetData()[this->m_read_idx];
        this->m_size--;
        this->m_read_idx=(this->m_read_idx+1) % this->GetMaxSize();
        return *this;
    }

protected:
    virtual T *GetData()=0;
    inline virtual T &GetAbsolute(uint16_t idx)=0;

    int16_t m_read_idx;
    int16_t m_write_idx;
    uint16_t m_cur_idx;
};




template <typename T, uint16_t nbr_el>
class RingBufferStatic: public RingBuffer<T>
{
public:
    //template<typename T> friend class RingBufferStaticPtr;

    static const uint16_t MAX_NBR_EL=nbr_el;

    RingBufferStatic():RingBuffer<T>(nbr_el)
    {
//        m_write_idx=0;
    }

    virtual ~RingBufferStatic()
    {
    }

    /*virtual int16_t Peek(Ptr &data)
    {
        data.m_read_idx=m_read_idx;
        data.m_cur_idx=0;
        data.SetSize(m_nbr_el);
        data.SetBuffer(this);
        return 0;
    }*/

    /*virtual uint16_t GetMaxNbrEl()
    {
        return MAX_NBR_EL;
    }*/

    /*virtual uint16_t GetMaxSize()
    {
        return MAX_NBR_EL; // *sizeof(T);
    }*/

    /*virtual uint16_t GetSize()
    {
        return m_nbr_el; // *sizeof(T);
    } */

    /*virtual int16_t Write(T *buf, uint16_t nbr_el)
    {
        if (GetNbrEl()>=GetMaxNbrEl())
            return -1;
        for (uint16_t idx=0;idx<nbr_el;++idx)
        {
            m_buf[m_write_idx++]=buf[idx];
            m_write_idx%=MAX_NBR_EL;
            m_nbr_el++;
        }
        return nbr_el;
    } */

    /*RingBufferStatic<T, nbr_el> &operator= (const T &val)
    {
        m_buf[m_write_idx]=val;
        return *this;
    } */

    /*RingBufferStatic<T, nbr_el> &operator++(int)
    {
        m_write_idx++;
        m_nbr_el++;
        if (m_read_idx==-1)
            m_read_idx=0;
        return *this;
    }

    virtual T& operator[](uint16_t idx)
    {
        assert(idx<MAX_NBR_EL);
        return m_buf[(m_read_idx+idx) % MAX_NBR_EL];
    }

    //RingBufferStatic<T, nbr_el> &operator<<(const T& val)
    virtual RingBuffer<T> &operator<<(const T& val)
    {
        m_buf[m_write_idx]=val;
        m_write_idx=(m_write_idx+1) % MAX_NBR_EL;
        m_nbr_el++;
        if (m_read_idx==-1)
            m_read_idx=0;
        return *this;
    }

    //RingBufferStatic<T, nbr_el> &operator>>(T& val)
    virtual RingBuffer<T> &operator>>(T& val)
    {
        if (m_nbr_el<=0)
            return *this;
        val=m_buf[m_read_idx];
        m_nbr_el--;
        m_read_idx=(m_read_idx+1) % MAX_NBR_EL;
        return *this;
    } */

    /*const T& operator[](uint16_t idx) const
    {
        const_cast<T&>(*this)[idx];
    }*/

    virtual operator T*()
    {
        if (this->m_read_idx==-1)
            return NULL;
        return m_buf+this->m_read_idx;
    }
protected:
    inline virtual T &GetAbsolute(uint16_t idx)
    {
        return m_buf[idx % MAX_NBR_EL];
    }

    inline virtual T *GetData()
    {
        return m_buf;
    }

    T m_buf[MAX_NBR_EL+1];
};

}

#endif
