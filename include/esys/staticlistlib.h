/*!
 * \file esys/staticlist.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_STATICLIST_H__
#define __ESYS_STATICLIST_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

template<typename T, typename LIB>
class StaticList
{
public:
    template<typename T, typename LIB> class StaticItem;

    StaticList();
    virtual ~StaticList();

    static T *GetFirst();
    static T *GetLast();
    static uint32_t GetCount();
protected:
    static T *m_first;
    static T *m_last;
    static uint32_t m_count;
};

template<typename T, typename LIB>
StaticList<T, LIB>::StaticList()
{
}

template<typename T, typename LIB>
StaticList<T, LIB>::~StaticList()
{
}

template<typename T, typename LIB>
T *StaticList<T, LIB>::GetFirst()
{
    return StaticList<T, LIB>::m_first;
}

template<typename T, typename LIB>
T *StaticList<T, LIB>::GetLast()
{
    return StaticList<T, LIB>::m_last;
}

template<typename T, typename LIB>
uint32_t StaticList<T, LIB>::GetCount()
{
    return StaticList<T, LIB>::m_count;
}

template<typename T, typename LIB> T *StaticList<T, LIB>::m_first=NULL;
template<typename T, typename LIB> T *StaticList<T, LIB>::m_last=NULL;
template<typename T, typename LIB> uint32_t StaticList<T, LIB>::m_count=0;

}

#endif

