/*!
 * \file esys/treeitem.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TREEITEM_H__
#define __ESYS_TREEITEM_H__

#include "esys/esys_defs.h"
#include "esys/treeleaf.h"
#include "esys/assert.h"

namespace esys
{

/*! \class TreeItem esys/treeitem.h "esys/treeitem.h"
 *  \brief The item of the of tree
 *  \tparam T the type of item in the tree
 *
 * This class is used to build static trees of objects, which means that every TreeItem has all the
 * member variables to point to its children or parent.
 */
template<typename T>
class TreeItem: public TreeLeaf<T>
{
public:
    //! Constructor
    TreeItem() : TreeLeaf<T>()
    {
    }

    //! Destructor
    virtual ~TreeItem()
    {
    }

    //! Set the first child of this item in the tree
    /*!
     *  \param[in] first the first child
     */
    virtual void SetFirstChild(T *first) override
    {
        m_first = first;
    }

    //! Return the first child of this item in the tree
    /*!
     *  \return the first child is any, nullptr otherwise
     */
    virtual T *GetFirstChild() override
    {
        return m_first;
    }

    //! Set the last child of this item in the tree
    /*!
     *  \param[in] last the last child
     */
    virtual void SetLastChild(T *last) override
    {
        m_last = last;
    }

    //! Return the last child of this item in the tree
    /*!
     *  \return the last child is any, nullptr otherwise
     */
    virtual T *GetLastChild() override
    {
        return m_last;
    }

    //! Add a child to this item in the tree
    /*!
     *  \param[in] child the child to add
     */
    virtual void AddChild(T *child) override;

protected:
//!< \cond DOXY_IMPL
    T *m_first = nullptr;   //<! The first child of this item
    T *m_last = nullptr;    //<! The last child of this item
//!< \endcond
};

template<typename T>
void TreeItem<T>::AddChild(T *child)
{
    if (GetFirstChild() == nullptr)
    {
        // This is the first child for this object
        SetFirstChild(child);
        SetLastChild(child);
    }
    else
    {
        // Simply add the new child at the end of the linked list of children
        child->SetPrev(GetLastChild());
        GetLastChild()->SetNext(child);
        SetLastChild(child);
    }
}

}

#endif

