/*!
 * \file esys/uuid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_UUID_H__
#define __ESYS_UUID_H__

#include <esys/esys_defs.h>

#ifdef ESYS_VHW
#include "esys/boost/uuid.h"
#else
#error UUID not supported for this platform
#endif

#endif


