/*!
 * \file esys/staticitem.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_STATICITEM_H__
#define __ESYS_STATICITEM_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

template<typename T, typename LIB>
class StaticItem
{
public:
    StaticItem();
    virtual ~StaticItem();

    T *GetNext();
    T *GetPrev();
protected:
    T *m_next;
    T *m_prev;
};

template<typename T, typename LIB>
StaticItem<T, LIB>::StaticItem(): m_next(NULL), m_prev(NULL)
{
    if (LIB::m_first==NULL)
    {
        LIB::m_first=this;
        LIB::m_next=this;
    }
    else
    {
        LIB::m_last->m_next=this;
        m_prev=LIB::m_last;
        LIB::m_last=this;
    }
    LIB::m_count++;

}

template<typename T, typename LIB>
StaticItem<T, LIB>::~StaticItem()
{
}

template<typename T, typename LIB>
T *StaticItem<T, LIB>::GetNext()
{
    return m_next;
}

template<typename T, typename LIB>
T *StaticItem<T, LIB>::GetPrev()
{
    return m_prev;
}

}

#endif

