/*!
 * \file esys/modulelistbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MODULELISTBASE_H__
#define __ESYS_MODULELISTBASE_H__

#include "esys/esys_defs.h"
#include "esys/list.h"
#include "esys/module.h"

namespace esys
{

/*! \class ModuleListBase esys/modulelistbase.h "esys/modulelistbase.h"
 *  \brief Base class of module list
 */
class ESYS_API ModuleListBase : public List<Module>
{
public:
    //! Constructor
    ModuleListBase();

    //! Destructor
    ~ModuleListBase();

};

}

#endif


