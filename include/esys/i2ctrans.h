/*!
 * \file esys/i2ctrans.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_I2CTRANS_H__
#define __ESYS_I2CTRANS_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/i2cdevicebase.h"

namespace esys
{

class ESYS_API I2CTrans
{
public:
    I2CTrans(I2CDeviceBase *dev=NULL);
    virtual ~I2CTrans();

    void SetTxBuf(uint8_t *buf, uint16_t buf_len);
    void SetRxBuf(uint8_t *buf, uint16_t buf_len);
    uint8_t *GetTxBuf(uint16_t &buf_len);
    uint8_t *GetRxBuf(uint16_t &buf_len);
    bool IsWriteFirst();
    void SetWriteFirst(bool write_first=true);

protected:
    I2CDeviceBase *m_dev;
    bool m_write_first;
    uint8_t *m_tx_buf;
    uint16_t m_tx_buf_len;
    uint8_t *m_rx_buf;
    uint16_t m_rx_buf_len;
}

}

#endif

