/*!
 * \file esys/timer.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"

#ifdef ESYS_USE_MP
#include "esys/mp/timer.h"
#elif defined ESYS_USE_BOOST
#include "esys/boost/timer.h"
#elif defined ESYS_USE_SYSC
#include "esys/sysc/timer.h"
#elif defined ESYS_USE_FREERTOS
#include "esys/freertos/timer.h"
#else
#error One of the following must be defined ESYS_USE_MP, ESYS_USE_BOOST, ESYS_USE_SYSC or ESYS_USE_FREERTOS
#endif





