/*!
 * \file esys/hwuid.h
 * \brief Header file of the HW UID class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include <cstring>

namespace esys
{

class ESYS_API HWUID
{
public:
    typedef esys::uint8_t Type;

    static const Type RAW = 0;
    static const Type MICROCHIP_E2PROM = 1;     //!< Microchip E2PROM UID
    static const Type EUI_48 = 2;               //!< EUI-48
    static const Type EUI_64 = 3;               //!< EUI-64
    static const Type ATMEL_E2PROM_SN = 4;      //!< Atmel E2PROM 128 bits serial number

    HWUID();
    virtual ~HWUID();

    Type GetType();
    virtual esys::uint8_t GetSize() = 0;
    virtual esys::uint8_t GetRawSize() = 0;
    virtual esys::uint8_t *GetData() = 0;
    virtual esys::uint8_t *GetRawData() = 0;
    virtual void SetRawData(esys::uint8_t *data, esys::uint8_t size) = 0;

    void SetData(esys::uint8_t *data, esys::uint8_t size);

protected:
    void SetType(Type type);
};

template<int N>
class HWUID_t : public HWUID
{
public:
    HWUID_t(Type type = RAW);
    virtual ~HWUID_t();

    virtual esys::uint8_t GetSize() override;
    virtual esys::uint8_t GetRawSize() override;
    virtual esys::uint8_t *GetData() override;
    virtual esys::uint8_t *GetRawData() override;
    virtual void SetRawData(esys::uint8_t *raw, esys::uint8_t size) override;
protected:
    esys::uint8_t m_raw_data[N + 2];
};

template<int N>
HWUID_t<N>::HWUID_t(Type type): HWUID()
{
    m_raw_data[0] = type;
    m_raw_data[1] = N;
    memset(m_raw_data + 2, 0x00, N);
}

template<int N>
HWUID_t<N>::~HWUID_t()
{
}

template<int N>
esys::uint8_t HWUID_t<N>::GetRawSize()
{
    return N + 2;
}

template<int N>
esys::uint8_t HWUID_t<N>::GetSize()
{
    return N;
}

template<int N>
esys::uint8_t *HWUID_t<N>::GetRawData()
{
    return m_raw_data;
}

template<int N>
esys::uint8_t *HWUID_t<N>::GetData()
{
    return m_raw_data + 2;
}

template<int N>
void HWUID_t<N>::SetRawData(esys::uint8_t *raw, esys::uint8_t size)
{
    assert(size == GetRawSize());

    memcpy(m_raw_data, raw, size);
}

}


