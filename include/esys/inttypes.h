/*!
 * \file esys/inttypes.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_INTTYPES_H__
#define __ESYS_INTTYPES_H__

/*namespace esys
{ */

#include "esys/esys_setup.h"

namespace esys
{

typedef int int_t;
typedef unsigned int uint_t;
}

#ifdef _MSC_VER
#include <limits.h>
#if _MSC_VER < 1600
namespace esys
{
typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;

typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
}
#define ESYS_INTTYPES_DEF
#define UINT32_T_MAX ULONG_MAX
#else
#include <stdint.h>
#endif

#elif defined(__GNUC__) || defined(__GNUG__) || defined(ESYSEM)
#ifdef ESYS_HAVE_STDINT_H
#include <stdint.h>
#elif defined(ESYS_HAVE_INTTYPES_H)
#include <inttypes.h>
#else
#include <stdint.h>
#endif
#include <limits.h>
#define UINT32_T_MAX ULONG_MAX

#elif defined ARDUINO
#include <Arduino.h>
#include <inttypes.h>

#elif defined __GNUC__ || defined __GNUG__


#endif //_MSC_VER

#ifndef ESYS_INTTYPES_DEF
namespace esys
{
typedef ::int8_t int8_t;
typedef ::int16_t int16_t;
typedef ::int32_t int32_t;
typedef ::int64_t int64_t;

typedef ::uint8_t uint8_t;
typedef ::uint16_t uint16_t;
typedef ::uint32_t uint32_t;
typedef ::uint64_t uint64_t;
}
#endif
//}

#endif
