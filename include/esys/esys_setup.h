/*!
 * \file esys/esys_setup.h
 * \brief The public version of the setup.h file
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_ESYS_SETUP_H__
#define __ESYS_ESYS_SETUP_H__

#include "esys/config.h"

#if defined(WIN32)
#ifndef ESYS_MULTI_PLAT
#define ESYS_MULTI_PLAT 1
#endif
#ifndef ESYS_USE_MP
#define ESYS_USE_MP 1
#endif
#ifndef ESYS_VHW
#define ESYS_VHW 1
#endif

#ifndef ESYS_OBJECT_META
#define ESYS_OBJECT_META 1
#endif

#ifndef  ESYS_FREERTOS_HEAP_5
#define  ESYS_FREERTOS_HEAP_5
#endif
#ifndef MULTIOS
#define MULTIOS
#endif
#endif

#ifdef ESYS_DEV_CMAKE_BUILD
#elif defined WIN32
#elif defined ESYS_USE_ARD

#elif defined ESYSEM

#elif defined __GNUC__ || defined __GNUG__
#include "esys/unix/esys_setup.h"
#endif

#if ESYS_MULTIOS
#ifndef MULTIOS
#define MULTIOS
#endif
#endif

#endif
