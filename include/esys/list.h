/*!
 * \file esys/list.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_LIST_H__
#define __ESYS_LIST_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/listitem.h"

namespace esys
{

/*! \class List esys/list.h "esys/list.h"
 *  \brief List of items
 *  \tparam T the type of item in the list
 *
 */
template<typename T>
class List
{
public:
    typedef int32_t(T::*SortFct)();     //!< Sorting function type

    //! Default constructor
    List();

    //! Destructor
    ~List();

    void Clear();               //!< Clear the list

    //! Add an item to the list
    /*!
     *  \param[in] item the item to add at the back of the list
     */
    void Add(T *item);

    //! Set the first item in the list
    /*!
     *  \param[in] first the first item of the list
     */
    void SetFirst(T *first);

    //! Get the first item in the list
    /*!
     *  \return the first item of the list
     */
    T *GetFirst();

    //! Set the last item in the list
    /*!
     *  \param[in] last the last item of the list
     */
    void SetLast(T *last);

    //! Get the last item in the list
    /*!
     *  \return the last item of the list
     */
    T *GetLast();

    //! Get the last item in the list
    /*!
     *  \return the last item of the list
     */
    uint32_t GetCount();

    //! Swap 2 items in the list
    /*!
     *  \param[in] item1 the first item to swap
     *  \param[in] item2 the other item to swap
     */
    void Swap(T *item1, T *item2);

    //! Sort the list using the given function
    /*!
     *  \param[in] sort_fct the function used to sort the list
     */
    void Sort(SortFct sort_fct);
protected:
//!< \cond DOXY_IMPL
    T *m_first = nullptr;
    T *m_last = nullptr;
    uint32_t m_count = 0;
//!< \endcond
};

template<typename T>
List<T>::List()
{
}

template<typename T>
List<T>::~List()
{
}

template<typename T>
void List<T>::Clear()
{
    m_first = nullptr;
    m_last = nullptr;
    m_count = 0;
}

template<typename T>
void List<T>::Add(T *item)
{
    if (m_first == nullptr)
    {
        m_count = 1;
        m_first = item;
        m_last = item;
    }
    else
    {
        ++m_count;
        m_last->SetNext(item);
        item->SetPrev(m_last);
        m_last = item;
    }
}

template<typename T>
void List<T>::SetFirst(T *first)
{
    m_first = first;
}

template<typename T>
T *List<T>::GetFirst()
{
    return m_first;
}

template<typename T>
void List<T>::SetLast(T *last)
{
    m_last = last;
}

template<typename T>
T *List<T>::GetLast()
{
    return m_last;
}

template<typename T>
uint32_t List<T>::GetCount()
{
    return m_count;
}

template<typename T>
void List<T>::Swap(T *item1, T *item2)
{
    if (item1->GetPrev() != nullptr)
        item1->GetPrev()->SetNext(item2);
    if (item2->GetNext() != nullptr)
        item2->GetNext()->SetPrev(item1);
    item2->SetPrev(item1->GetPrev());
    item1->SetNext(item2->GetNext());
    item2->SetNext(item1);
    item1->SetPrev(item2);

    if (m_first == item1)
        m_first = item2;
    if (m_last == item2)
        m_last = item1;
}

template<typename T>
void List<T>::Sort(SortFct sort_fct)
{
    T *item1;
    T *item2;

    bool swapped = true;
    uint32_t idx;
    uint32_t count;

    if (m_count < 2)
        return;

    // Very simply bubble sort, mostly because there is no need for auxiliary memory
    count = m_count - 1;

    while (swapped == true)
    {
        swapped = false;
        item1 = GetFirst();
        item2 = item1->GetNext();

        for (idx = 0; idx < count; ++idx)
        {
            if ((item1->*sort_fct)() > (item2->*sort_fct)())
            {
                Swap(item1, item2);
                swapped = true;
                item2 = item1->GetNext();
            }
            else
            {
                item1 = item2;
                item2 = item1->GetNext();
            }
        }
        --count;
    }
}

}

#endif

