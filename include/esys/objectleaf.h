/*!
 * \file esys/objectleaf.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_OBJECTLEAF_H__
#define __ESYS_OBJECTLEAF_H__

#include "esys/object.h"
#include "esys/treeleaf.h"

namespace esys
{

/*! \class ObjectLeaf esys/objectleaf.h "esys/objectleaf.h"
 *  \brief Object which can't have children Objects
 *
 *
 */
class ESYS_API ObjectLeaf: public Object, public TreeLeaf<Object>
{
public:
    //! Constructor
    /*!
     *  \param[in] name The name of the Object
     */
    ObjectLeaf(const ObjectName &name);

    //! Destructor
    virtual ~ObjectLeaf();
};

}

#endif

