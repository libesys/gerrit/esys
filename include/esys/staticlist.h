/*!
 * \file esys/staticlist.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_STATICLIST_H__
#define __ESYS_STATICLIST_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include <stddef.h>     //Needed for NULL under GCC

#ifdef MULTIOS
#include <vector>
#endif

namespace esys
{

template<typename T, int N=0>
class StaticList
{
public:
    typedef StaticList<T, N> Item;

    StaticList(T *element=NULL);
    ~StaticList();

    static T *GetByIdx(uint16_t idx);
    static uint16_t GetCount();
    static Item *GetLast();

    Item *GetPrev();
    void SetElement(T *element);
    T *GetElement();
protected:
#ifdef MULTIOS
    static void Populate();
#endif

    static Item *m_last;
    static uint16_t m_count;
    Item *m_prev;
    T *m_element;

#ifdef MULTIOS
    static std::vector<T *> m_vector;
#endif
};

template<typename T, int N>
StaticList<T,N>::StaticList(T *element): m_element(element)
{
    m_prev=m_last;
    m_last=this;
    m_count++;
}

template<typename T, int N>
StaticList<T,N>::~StaticList()
{
}

template<typename T, int N>
void StaticList<T,N>::SetElement(T *element)
{
    m_element=element;
}

template<typename T, int N>
T *StaticList<T,N>::GetElement()
{
    return m_element;
}

template<typename T, int N>
T *StaticList<T,N>::GetByIdx(uint16_t idx)
{
#ifdef MULTIOS
    if (m_vector.size()!=m_count)
        Populate();
    if (idx>=m_vector.size())
        return NULL;
    return m_vector[idx]->GetElement();
#else
    Item *p=m_last;
    uint16_t item_idx=0;

    if (idx>=m_count)
        return NULL;

    while (idx!=item_idx)
    {
        p=p->m_prev;
        item_idx++;
    }
    return p->GetElement();
#endif
}

template<typename T, int N>
uint16_t StaticList<T,N>::GetCount()
{
    return m_count;
}

template<typename T, int N>
StaticList<T,N> *StaticList<T,N>::GetLast()
{
    return m_last;
}

template<typename T, int N>
StaticList<T,N> *StaticList<T,N>::GetPrev()
{
    return m_prev;
}

#ifdef MULTIOS
template<typename T, int N>
void StaticList<T,N>::Populate()
{
    Item *p=m_last;

    m_vector.clear();

    while (p!=NULL)
    {
        m_vector.push_back(p->GetElement());
        p=p->m_prev;
    }
}
#endif

template<typename T, int N> StaticList<T,N> *StaticList<T,N>::m_last=NULL;
template<typename T, int N> uint16_t StaticList<T,N>::m_count=0;
#ifdef MULTIOS
template<typename T, int N> std::vector<T *> StaticList<T,N>::m_vector;
#endif

}

#endif
