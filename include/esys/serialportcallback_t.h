/*!
 * \file esys/serialportcallback_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/serialportcallbackbase.h"

namespace esys
{

/*! \class SerialPortCallback_t esys/serialportcallback_t.h "esys/serialportcallback_t.h"
 *  \brief
 */
template<typename OBJ>
class SerialPortCallback_t: public SerialPortCallbackBase
{
public:
    typedef int (OBJ::*CallbackType)(RingBuffer<uint8_t> &, SerialPortBase *);

    //! Constructor
    SerialPortCallback_t();
    SerialPortCallback_t(OBJ *callee, CallbackType callback);

    //! Destructor
    virtual ~SerialPortCallback_t();

    void Set(OBJ *callee, CallbackType callback);

    //! Used to trigger the callback
    /*!
     */
    virtual int Reveived(RingBuffer<uint8_t> &buffer) override;
protected:
    OBJ *m_callee = nullptr;            //!< The object, which should be called through the provided callback
    CallbackType m_callback = nullptr;  //!< The callback member function
};

template<typename OBJ>
SerialPortCallback_t<OBJ>::SerialPortCallback_t()
{
}

template<typename OBJ>
SerialPortCallback_t<OBJ>::SerialPortCallback_t(OBJ *callee, CallbackType callback)
    : m_callee(callee), m_callback(callback)
{
}

template<typename OBJ>
SerialPortCallback_t<OBJ>::~SerialPortCallback_t()
{
}

template<typename OBJ>
void SerialPortCallback_t<OBJ>::Set(OBJ *callee, CallbackType callback)
{
    m_callee = callee;
    m_callback = callback;
}

template<typename OBJ>
int SerialPortCallback_t<OBJ>::Reveived(RingBuffer<uint8_t> &buffer)
{
    if ((m_callee != nullptr) && (m_callback != nullptr))
        return (m_callee->*m_callback)(buffer, GetSerialPortBase());
    else
        return -1;
}

}


