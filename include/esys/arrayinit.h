/*!
* \file esys/arrayinit.h
* \brief
*
* \cond
* __legal_b__
*
* Copyright (c) 2014 Michel Gillet
* Distributed under the wxWindows Library Licence, Version 3.1.
* (See accompanying file LICENSE_3_1.txt or
* copy at http://www.wxwidgets.org/about/licence)
*
* __legal_e__
* \endcond
*
*/

#ifndef __ESYS_ARRAYINIT_H__
#define __ESYS_ARRAYINIT_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

namespace esys
{

template<typename T, uint32_t N>
class ArrayInit
{
public:
    ArrayInit();
    virtual ~ArrayInit();


protected:
    T m_array[N];
};

template<typename T, uint32_t N>
ArrayInit<T,N>::ArrayInit()
{
    for (uint32_t idx=0; idx<N; ++idx)
        m_array[idx].SetIndex(idx);
}

template<typename T, uint32_t N>
ArrayInit<T,N>::~ArrayInit()
{
}


}

#endif
