/*!
 * \file esys/bufferwrapbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BUFFERWRAPBASE_H__
#define __ESYS_BUFFERWRAPBASE_H__

#include "esys/esys_defs.h"
#include "esys/buffer.h"

namespace esys
{

template<typename T, uint8_t N_EXT_SEG=1>
class BufferWrapBase: public Buffer<T, N_EXT_SEG>
{
public:
    typedef PointerBlock<T, BufferWrapBase<T, N_EXT_SEG> > Ptr;
    //typedef BufferWrapBasePtr<T, BufferWrapBase<T> > Ptr;

    BufferWrapBase(uint16_t max_size=0)
        : Buffer<T, N_EXT_SEG>(max_size, 0)
    {
        //SetSize(0);
    }

    BufferWrapBase(uint16_t max_size, uint16_t size=0): Buffer<T, N_EXT_SEG>(max_size, size)
    {
        //SetSize(size);
    }

    virtual ~BufferWrapBase()
    {
    }

    Ptr Begin()
    {
        return Ptr(this);
    }

    Ptr Block(uint16_t idx=0)
    {
        return Ptr(this);
    }

    virtual int16_t Push(T *data, uint16_t size)
    {
        uint16_t av_size=this->GetMaxSize()-this->GetSize();
        int16_t result=0;

        if (this->m_ext_segs_nbr==0)
        {
            if (size>av_size)
            {
                size=av_size;
                result=-2;
            }
            ::memcpy(GetData()+this->GetSize(), data, size);
            this->SetSize(size+this->GetSize());
            return result;
        }
        assert(false);
        return -1;
    }

    virtual int16_t Copy(const Ptr &dst, T *data, uint16_t size)
    {
        bool scatter_gather=this->UseScatterGather();
        uint16_t index=dst.GetIndex()+dst.GetOffset();

        if ((index+size)>this->GetSize())
            return -1;

        if (scatter_gather)
        {
            assert(this->m_ext_segs_nbr<this->m_ext_segs_max_count);

            this->m_ext_segs[this->m_ext_segs_nbr].m_index=index;
            this->m_ext_segs[this->m_ext_segs_nbr].m_buf=data;
            this->m_ext_segs[this->m_ext_segs_nbr].m_size=size;
            this->m_ext_segs_nbr++;
        }
        else
            ::memcpy(this->GetData()+index, data, size);
        return 0;
    }

    virtual int16_t Pop(uint16_t size)
    {
        assert(size<=this->GetSize());

        if (this->m_ext_segs_nbr==0)
        {
            if (this->GetSize()==0)
                return 0;
            ::memcpy(GetData(), GetData()+size, this->GetSize()-size);
            this->SetSize(this->GetSize()-size);
            return 0;
        }
        assert(false);
        return -1;
    }

    virtual void Set(const T &val)
    {
        if (this->m_ext_segs_nbr==0)
        {
            if (val==0)
            {
                ::memset(GetData(), 0, this->GetMaxSize());
            }
            else
            {
                for (uint16_t idx=0; idx<this->GetMaxSize(); ++idx)
                {
                    GetData()[idx]=val;
                }
            }
            return;
        }
        assert(false);
    }

    virtual int16_t Set(const T &val, uint16_t nbr)
    {
        if (nbr>this->GetSize())
            return -1;

        if (this->m_ext_segs_nbr==0)
        {
            if (val==0)
            {
                ::memset(GetData(), 0, this->GetSize());
            }
            else
            {
                for (uint16_t idx=0; idx<this->GetSize(); ++idx)
                {
                    GetData()[idx]=val;
                }
            }
            return 0;
        }
        assert(false);
        return -1;
    }

    virtual uint16_t GetNbrSegment(bool data=true)
    {
        if (this->m_ext_segs_nbr==0)
        {
            if (this->GetSize()==0)
            {
                if (data==true)
                    return 0; // There is no data
                else
                    return 1;
            }
            else
            {
                if (data==true)
                    return 1;
                else
                {
                    if (this->GetSize()<this->GetMaxSize())
                        return 1;
                    else
                        return 0;
                }
            }
        }

        uint8_t ext_sg_idx=0;
        uint16_t nbr_segs=0;
        uint16_t index=0;

        while (ext_sg_idx<this->m_ext_segs_nbr)
        {
            if (this->m_ext_segs[ext_sg_idx].m_index>index)
            {
                //Some data in stored in the buffer before the curren ext seg
                nbr_segs+=2;
                index=this->m_ext_segs[ext_sg_idx].m_index+this->m_ext_segs[ext_sg_idx].m_size;
                ext_sg_idx++;
            }
            else
            {
                nbr_segs+=1;
                index=this->m_ext_segs[ext_sg_idx].m_index+this->m_ext_segs[ext_sg_idx].m_size;
                ext_sg_idx++;
            }
        }
        if (index<this->GetSize())
            //If data is after the last ext segment
            nbr_segs++;
        return nbr_segs;
    }

    virtual T *GetSegment(uint16_t idx, uint16_t &size, bool data=true)
    {
        if ((this->m_ext_segs_nbr==0) || (!data))
        {
            if (idx!=0)
            {
                size=0;
                return nullptr;
            }

            if (this->GetSize()==0)
            {
                //There is no data in the buffer
                if (data==true)
                {
                    size=0;
                    return nullptr; // There is no data
                }
                else
                {
                    size=this->GetMaxSize();  //Free space in the buffer
                    return GetData();
                }
            }
            else
            {
                //There is data in the buffer
                if (data==true)
                {
                    size=this->GetSize();
                    return GetData();
                }
                else
                {
                    size=this->GetMaxSize()-this->GetSize();
                    return GetData()+this->GetSize();
                }
            }
        }

        uint8_t ext_sg_idx=0;
        uint16_t nbr_segs=0;
        uint16_t index=0;
        T *seg = nullptr;

        while ((nbr_segs<=idx)&&(ext_sg_idx<this->m_ext_segs_nbr))
        {
            if (this->m_ext_segs[ext_sg_idx].m_index>index)
            {
                //Some data in stored in the buffer before the current ext seg
                seg=GetData()+index;
                size=this->m_ext_segs[ext_sg_idx].m_index-index;
                nbr_segs+=1;
                index=this->m_ext_segs[ext_sg_idx].m_index;
            }
            else
            {
                nbr_segs+=1;
                seg=this->m_ext_segs[ext_sg_idx].m_buf;
                size=this->m_ext_segs[ext_sg_idx].m_size;
                index=this->m_ext_segs[ext_sg_idx].m_index+this->m_ext_segs[ext_sg_idx].m_size;
                ext_sg_idx++;
            }
        }
        if (nbr_segs==idx)
        {
            if (index<this->GetSize())
            {
                // Data is after the last ext segment
                seg=GetData()+index;
                size=this->GetSize()-index;
            }
            else
            {
                size=0;
                seg=nullptr;
            }
        }
        return seg;
    }

    virtual void WriteSegment(uint16_t idx, T *, uint16_t size, bool data=true)
    {

    }

    /*virtual Pointer<T> &operator++(int)
    {
        assert(this->m_offset+this->m_idx<this->m_size);

        this->m_idx++;
        return *this;
    } */

    virtual T &operator[](uint16_t idx)
    {
        //assert((this->m_offset+idx)<this->m_size);

        //return GetData()[this->m_offset+idx];

        assert(idx<this->m_size);

        if (this->m_ext_segs_nbr==0)
            return GetData()[idx];

        uint8_t ext_sg_idx=0;

        while (ext_sg_idx<this->m_ext_segs_nbr)
        {
            if (this->m_ext_segs[ext_sg_idx].m_index>idx)
            {
                //Some data in stored in the buffer before the current ext seg
                return GetData()[idx];
            }
            else
            {
                if (idx<this->m_ext_segs[ext_sg_idx].m_index+this->m_ext_segs[ext_sg_idx].m_size)
                    return this->m_ext_segs[ext_sg_idx].m_buf[idx-this->m_ext_segs[ext_sg_idx].m_index];
                ext_sg_idx++;
            }
        }
        //Here if data is after the last ext segment
        return GetData()[idx];
    }

    virtual T &operator()(uint16_t idx)
    {
        assert(idx<this->m_size);

        if (this->m_ext_segs_nbr==0)
            return GetData()[idx];
        assert(false);
        return GetData()[idx];
    }

    /*virtual T &operator*()
    {
        //assert(this->m_offset+this->m_idx<this->m_size);

        //return GetData()[this->m_offset+this->m_idx];
        assert(this->m_idx<this->m_size);

        return GetData()[this->m_idx];
    } */
protected:
    virtual T *GetData()=0;

};

}

#endif

