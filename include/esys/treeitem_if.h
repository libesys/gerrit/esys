/*!
 * \file esys/treeitem_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TREEITEM_IF_H__
#define __ESYS_TREEITEM_IF_H__

#include "esys/esys_defs.h"
//#include "esys/listitem_if.h"
#include "esys/assert.h"

namespace esys
{

/*! \class TreeItem_if esys/treeitem_if.h "esys/treeitem_if.h"
 *  \brief
 *  \tparam T the type of item of the tree
 *
 */
template<typename T>
class TreeItem_if
{
public:
    typedef int32_t Order;

    static const Order BEFORE_CHILDREN = 0;
    static const Order AFTER_CHILDREN = 1;

    template<typename U>
    class FunctorBase
    {
    public:
        virtual int32_t CallFct(U *obj, Order order) = 0;
    };

    typedef FunctorBase<T> FunctorType;

    virtual void SetNext(T *next) = 0;
    virtual T *GetNext() = 0;
    virtual void SetPrev(T *prev) = 0;
    virtual T *GetPrev() = 0;

    //! Set the parent of this item in the tree
    /*!
     *  \param[in] parent the parent item
     */
    virtual void SetParent(T *parent) = 0;

    //! Return the parent of this item in the tree
    /*!
     *  \return the parent if any, nullptr otherwise
     */
    virtual T *GetParent() = 0;

    virtual void SetFirstChild(T *first) = 0;


    //! Return the first child of this item in the tree
    /*!
     *  \return the first child is any, nullptr otherwise
     */
    virtual T *GetFirstChild() = 0;


    virtual void SetLastChild(T *last) = 0;

    //! Return the last child of this item in the tree
    /*!
     *  \return the last child is any, nullptr otherwise
     */
    virtual T *GetLastChild() = 0;

    virtual void AddChild(T *child) = 0;

protected:

};


}

#endif



