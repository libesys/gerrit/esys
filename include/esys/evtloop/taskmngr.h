/*!
 * \file esys/evtloop/taskmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKMNGR_H__
#define __ESYS_EVTLOOP_TASKMNGR_H__

#if !defined(ESYS_MULTI_PLAT) && defined(ESYS_USE_SYSC)
#include "esys/evtloop/sysc/taskmngr.h"
#elif defined(ESYS_MULTI_PLAT)
#include "esys/evtloop/mp/taskmngr.h"
#else

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/evtloop/taskmngrbase.h"

namespace esys
{

namespace evtloop
{

/*! \class TaskMngr esys/evtloop/taskmngr.h "esys/evtloop/taskmngr.h"
 *  \brief Multi Platform TaskMngr implementation, which can use any platform as backend
 */
class ESYS_API TaskMngr: public TaskMngrBase
{
public:
    TaskMngr();
    virtual ~TaskMngr();

protected:
};

}

}

#endif

#endif



