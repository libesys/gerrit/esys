/*!
 * \file esys/evtloop/sysc/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_SYSC_TASKPLATIMPL_H__
#define __ESYS_EVTLOOP_SYSC_TASKPLATIMPL_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbase.h"

#ifdef SYSTEMC_201
#include <systemc/kernel/sc_module.h>
#include <systemc/kernel/sc_event.h>
#else
#include <systemc.h>
#endif

namespace esys
{

namespace evtloop
{

namespace sysc
{

class ESYS_SYSC_API TaskPlat;

/*! \class TaskPlat esys/sysc/taskplat.h "esys/sysc/taskplat.h"
 *  \brief The PIMPL class of SystemC TaskPlat implementation
 */
class ESYS_SYSC_API TaskPlatImpl: public sc_module
{
public:
    friend class ESYS_SYSC_API TaskPlat;

    SC_HAS_PROCESS(TaskPlatImpl);

    TaskPlatImpl(const sc_module_name &name);
    virtual ~TaskPlatImpl();

    millis_t Millis();
    void Sleep(uint64_t val);
    void SleepU(micros_t us);

    void DoLoopIterationEnds();
    void Wait();
    void Signal();

    void NotifyExit();
    //virtual ExitCode Entry();
    void Main();

    static TaskPlatImpl *GetCurrent();

    void SetTaskBase(TaskBase *task_base);
    TaskBase *GetTaskBase();

    void SetTaskPlat(TaskPlat *task_plat);
    TaskPlat *GetTaskPlat();

    void StartTask();
    void WaitTaskDone();
protected:
    TaskPlat *m_task_plat;
    TaskBase *m_task_base;
    bool m_started;
    sc_event m_start_task;
    sc_event m_task_done;
};

}

}

}

#endif