/*!
 * \file esys/evtloop/sysc/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/inttypes.h"
#include "esys/evtloop/mp/esysbase.h"

#include <string>

#ifndef ESYS_USE_SYSC
#define ESYS_USE_SYSC 1
#endif

#if !defined(SYSTEMC_201) && !defined(SYSTEMC_231)
#define SYSTEMC_231 1
#endif

#ifdef ESYS_USE_WX
#undef ESYS_USE_WX
#endif
#ifdef ESYS_USE_BOOST
#undef ESYS_USE_BOOST
#endif

/*! \defgroup esys ESys core library
 *  The ESys core library
 *  @{
 */

namespace esys
{

namespace evtloop
{

namespace sysc
{

class ESYS_SYSC_API ESysImpl;

/*! \class ESys esys/sysc/esys.h "esys/sysc/esys.h"
 *  \brief
 */
class ESYS_SYSC_API ESys : public esys::evtloop::mp::ESysBase
{
public:
    static void Sleep(millis_t msec);
    static millis_t Millis();
    static void Init();
    static void Release();

    static ESys &Get();
    void SetAppName(const char *app_name);
    int32_t CreateLog();

    static int32_t GetExeDir(std::string &exe_dir);

protected:
    ESys();
    virtual ~ESys();

    static ESys g_esys;
    static uint64_t m_started_time;

private:
    ESysImpl &m_impl;

#ifdef ESYS_MULTI_PLAT
protected:
    virtual void impSleep(millis_t msec);
    virtual millis_t impMillis();
    virtual ESys &impGet();
    virtual void impInit();
    virtual void impRelease();
    virtual TaskPlatIf *impNewTaskPlat(const char *name);
    virtual TaskMngrBase *impNewTaskMngrBase();
    virtual MutexBase *impNewMutexBase(MutexBase::Type type);
#endif
};

} // namespace sysc

#ifdef ESYS_USING_PLAT_NS
using namespace sysc;
#endif

} // namespace evtloop

} // namespace esys

/*! @}*/
