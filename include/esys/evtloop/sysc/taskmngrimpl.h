/*!
 * \file esys/evtloop/sysc/taskmngrimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_SYSC_TASKMNGRIMPL_H__
#define __ESYS_EVTLOOP_SYSC_TASKMNGRIMPL_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbase.h"

#if 0 //#ifdef SYSTEMC_201
#include <systemc/kernel/sc_time.h>
#include <systemc/kernel/sc_event.h>
#include <systemc/kernel/sc_module.h>
#elif defined SYSTEMC_231
#include <systemc.h>
//#include <sysc/kernel/sc_time.h>
//#include <sysc/kernel/sc_event.h>
//#include <sysc/kernel/sc_module.h>
#endif

namespace esys
{

namespace evtloop
{

namespace sysc
{

class ESYS_SYSC_API TaskMngr;

/*! \class TaskMngrImpl esys/sysc/taskmngrimpl.h "esys/sysc/taskmngrimpl.h"
 *  \brief The PIMPL class for the SystemC TaskMngr implementation
 */
class ESYS_SYSC_API TaskMngrImpl: public sc_module
{
public:
    friend class ESYS_API TaskMngr;

    SC_HAS_PROCESS(TaskMngrImpl);

    TaskMngrImpl(sc_module_name name);
    virtual ~TaskMngrImpl();

    void SetTaskMngr(TaskMngrBase *task);
    void SetTaskMngr(TaskMngr *task);
    TaskMngr *GetTaskMngr();

    void WaitInitSetupDone();
    void SignalTaskDone();

    void Main();

protected:
    TaskMngr *m_task_mngr;
    TaskMngrBase *m_task_mngr_base;
    sc_event m_init_setup_done;
    sc_event m_task_done;
    sc_time m_boot_time;
    bool m_started;
};

}

}

}

#endif