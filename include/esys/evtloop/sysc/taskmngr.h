/*!
 * \file esys/evtloop/sysc/taskmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_SYSC_TASKMNGR_H__
#define __ESYS_EVTLOOP_SYSC_TASKMNGR_H__

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/evtloop/taskmngrbase_t.h"
#include "esys/inttypes.h"
#include "esys/evtloop/sysc/taskmngrimpl.h"
#include "esys/evtloop/sysc/taskplat.h"

#include <vector>

namespace esys
{

namespace evtloop
{

namespace sysc
{

//class ESYS_API TaskMngrImpl;
//class ESYS_API Task;
//class ESYS_API TaskPlat;

/*! \class TaskMngr esys/sysc/taskmngr.h "esys/sysc/taskmngr.h"
 *  \brief SystemC TaskMngr implementation
 */
class ESYS_SYSC_API TaskMngr: public TaskMngrBaseImp_t<TaskPlat, TaskMngrImpl>
{
public:
    typedef TaskMngrBaseImp_t<TaskPlat, TaskMngrImpl> BaseType;

    TaskMngr();
    virtual ~TaskMngr();

    virtual void Run();
};

/*class ESYS_API TaskMngr: public TaskMngrBase_t<TaskPlat>
{
public:
    typedef TaskMngrBase_t<TaskPlat> BaseType;

    TaskMngr();
    virtual ~TaskMngr();

    virtual void Run();

    TaskMngrImpl *GetImpl();
protected:
    TaskMngrImpl *m_impl;
};*/

/*class ESYS_API TaskMngr: public TaskMngrBase
{
public:
    TaskMngr();
    virtual ~TaskMngr();

    virtual void Run();
    virtual void AddTask(TaskBase *task);

    TaskMngrImpl *GetImpl();
    bool AllTasksDone();
protected:
    struct TaskData
    {
        TaskBase *m_task_base;
        TaskPlat *m_task_plat;
    };

    TaskMngrImpl *m_impl;
    std::vector<TaskData *> m_tasks;
}; */

}

#ifdef ESYS_USING_PLAT_NS
using namespace sysc;
#endif

}

}

#endif
