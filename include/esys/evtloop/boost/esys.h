/*!
 * \file esys/evtloop/boost/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/evtloop/mp/esysbase.h"

#include <string>

/*! \defgroup esys ESys core library
 *  The ESys core library
 *  @{
 */

namespace esys
{

namespace evtloop
{

namespace boost
{

class ESYS_API ESysImpl;

/*! \class ESys esys/esys.h "esys/event.h"
 *  \brief The base class of all logging or reporting events
 */
class ESYS_API ESys : public mp::ESysBase
{
public:
    static void Sleep(uint16_t msec);
    static millis_t Millis();
    static void Init();
    static void Release();

    static ESys &Get();
    void SetAppName(const char *app_name);
    int32_t CreateLog();

    static int32_t GetExeDir(std::string &exe_dir);

protected:
    ESys();
    virtual ~ESys();

    static ESys g_esys;

    static uint64_t m_started_time;

private:
    ESysImpl &m_impl;

#ifdef ESYS_MULTI_PLAT
protected:
    virtual void impSleep(millis_t msec) override;
    virtual millis_t impMillis();
    virtual ESys &impGet();
    virtual void impInit();
    virtual void impRelease();

    virtual esys::evtloop::TaskPlatIf *impNewTaskPlat(const char *name) override;
    virtual esys::evtloop::TaskMngrBase *impNewTaskMngrBase() override;
    virtual MutexBase *impNewMutexBase(MutexBase::Type type);
#endif
};

} // namespace boost

#ifdef ESYS_USING_PLAT_NS
using namespace boost;
#endif

} // namespace evtloop

} // namespace esys
/*! @}*/
