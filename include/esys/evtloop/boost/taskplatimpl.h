/*!
 * \file esys/evtloop/boost/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbase.h"
#include "esys/boost/semaphore.h"

#include <boost/thread.hpp>

namespace esys
{

namespace evtloop
{

namespace boost
{

class ESYS_API TaskPlat;

class ESYS_API TaskPlatImpl
{
public:
    friend class ESYS_API Task;

    TaskPlatImpl();
    virtual ~TaskPlatImpl();

    millis_t Millis();
    void Sleep(millis_t ms);
    void SleepU(micros_t us);

    void DoLoopIterationEnds();
    void Wait();
    void Signal();

    static TaskPlatImpl *GetCurrent();

    void SetTaskBase(TaskBase *task_base);
    TaskBase *GetTaskBase();

    void SetTaskPlat(TaskPlat *task_plat);
    TaskPlat *GetTaskPlat();

protected:
    TaskPlat *m_task_plat;
    TaskBase *m_task_base;
    ::boost::thread m_thread;       //!< Boost thread used
    esys::boost::Semaphore m_sem;
    bool m_started;
};

}

}

}


