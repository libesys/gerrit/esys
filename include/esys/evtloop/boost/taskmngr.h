/*!
 * \file esys/evtloop/boost/taskmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/evtloop/taskmngrbase_t.h"
#include "esys/inttypes.h"

namespace esys
{

namespace evtloop
{

namespace boost
{

class ESYS_API TaskMngrImpl;
class ESYS_API Task;
class ESYS_API TaskPlat;

class ESYS_API TaskMngr: public TaskMngrBaseImp_t<TaskPlat, TaskMngrImpl>
{
public:
    typedef TaskMngrBaseImp_t<TaskPlat, TaskMngrImpl> BaseType;

    TaskMngr();
    virtual ~TaskMngr();

    virtual void Run();
};

}

#ifdef ESYS_USING_PLAT_NS
using namespace boost;
#endif

}

}



