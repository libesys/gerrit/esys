/*!
 * \file esys/evtloop/wx/taskmngrimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_WX_TASKMNGRIMPL_H__
#define __ESYS_EVTLOOP_WX_TASKMNGRIMPL_H__

#include "esys_wx/esys_wx_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbase.h"

namespace esys
{

namespace evtloop
{

namespace wx
{

class ESYS_WX_API TaskMngr;

class ESYS_WX_API TaskMngrImpl
{
public:
    friend class ESYS_WX_API TaskMngr;

    TaskMngrImpl(const char *name);
    virtual ~TaskMngrImpl();

    void SetTaskMngr(TaskMngrBase *task);
    void SetTaskMngr(TaskMngr *task);
    TaskMngr *GetTaskMngr();

    void WaitInitSetupDone();
    void SignalTaskDone();

    void Main();

protected:
    TaskMngr *m_task_mngr;
    bool m_started;
};

}

}

}

#endif