/*!
 * \file esys/evtloop/wx/taskmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_WX_TASKMNGR_H__
#define __ESYS_EVTLOOP_WX_TASKMNGR_H__

#include "esys_wx/esys_wx_defs.h"
#include "esys/evtloop/taskmngrbase_t.h"
#include "esys/inttypes.h"

#include <vector>

namespace esys
{

namespace evtloop
{

namespace wx
{

class ESYS_WX_API TaskMngrImpl;
class ESYS_WX_API Task;
class ESYS_WX_API TaskPlat;

class ESYS_WX_API TaskMngr: public TaskMngrBaseImp_t<TaskPlat, TaskMngrImpl>
{
public:
    typedef TaskMngrBaseImp_t<TaskPlat, TaskMngrImpl> BaseType;

    TaskMngr();
    virtual ~TaskMngr();

    virtual void Run();
};

}

#ifdef ESYS_USING_PLAT_NS
using namespace wx;
#endif

}

}

#endif
