/*!
 * \file esys/evtloop/wx/taskimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WX_TASKIMPL_H__
#define __ESYS_WX_TASKIMPL_H__

#include "esys_wx/esys_wx_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbase.h"

#include <wx/longlong.h>
#include <wx/thread.h>

namespace esys
{

namespace evtloop
{

namespace wx
{

class ESYS_WX_API TaskPlat;

class ESYS_WX_API TaskPlatImpl: public wxThread
{
public:
    friend class ESYS_WX_API Task;

    TaskPlatImpl(wxThreadKind kind = wxTHREAD_DETACHED);
    virtual ~TaskPlatImpl();

    millis_t Millis();
    void Sleep(millis_t ms);
    void SleepU(micros_t us);

    void DoLoopIterationEnds();
    void Wait();
    void Signal();

    virtual ExitCode Entry();

    static TaskPlatImpl *GetCurrent();

    void SetTaskBase(TaskBase *task_base);
    TaskBase *GetTaskBase();

    void SetTaskPlat(TaskPlat *task_plat);
    TaskPlat *GetTaskPlat();

protected:
    wxLongLong m_started_time;
    TaskPlat *m_task_plat;
    TaskBase *m_task_base;
    wxSemaphore m_sem;
    bool m_started;
};

}

}

}

#endif