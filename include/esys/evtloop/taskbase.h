/*!
 * \file esys/evtloop/taskbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKBASE_H__
#define __ESYS_EVTLOOP_TASKBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/evtloop/tasklet.h"
#include "esys/evtloop/taskbaseif.h"
#include "esys/evtloop/taskplatif.h"

#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

namespace evtloop
{

class ESYS_API TaskMngrBase;

struct ESYS_API TaskletInfo
{
    Tasklet *m_tasklet;
    bool m_is_core;
};

// Forward declaration, to make GCC happy
template<bool add_it, typename T> class AddIfTaskletHelper;

template<typename T>
class AddIfTaskletHelper<false, T>
{
public:
    AddIfTaskletHelper(TaskBase *task_base, T *tasklet, bool core = false)
    {
    }
};

/*! \class TaskBase esys/evtloop/taskbase.h "esys/evtloop/taskbase.h"
 *  \brief Implements the platform independent Task API
 */
class ESYS_API TaskBase: public virtual TaskBaseIf, public virtual TaskPlatIf
{
public:
    //friend class ESYS_API ESys;

    typedef TaskBase *(*CurrentFct)();

    TaskBase(const char *name=NULL);
    virtual ~TaskBase();

    // TaskBaseIf
    //virtual void Init();
    //virtual void Setup();

    virtual void Exit(int16_t ret);
    virtual bool IsExit();
    virtual int16_t GetExit();
    virtual void Done();

    virtual void DoLoopIteration(bool all=false);
    //virtual void DoLoopIterationEnds();   //For platform
    virtual void Inits();
    virtual void Setups();

    virtual void SetInfoArray(TaskletInfo *info, uint8_t size);
    virtual void AddTasklet(Tasklet *tasklet, bool core=false);

    template<typename T>
    void AddIfTasklet(T *tasklet, bool core = false)
    {
        AddIfTaskletHelper<T::IS_TASKLET, T> do_it(this, tasklet, core);
    }

    // TaskBase functions
    virtual void SetTaskMngr(TaskMngrBase *task_mngr);
    TaskMngrBase *GetTaskMngr();
protected:
    virtual void InternalInit();
public:

    // TaskBase specific
    virtual void Do();
    void DoTasklets(bool all=false);

    static TaskBase *Current();
    void SetPrevTask(TaskBase *task);
    TaskBase *GetPrevTask();

    virtual void SetTaskPlat(TaskPlatIf *task_plat);
    virtual TaskPlatIf *GetTaskPlat();
    static void AddCurrentFct(CurrentFct fct);
protected:

    //virtual void InternalCoreLoop();
#if defined WIN32 || defined MULTIOS
    //virtual void _Sleep(uint64_t val)=0;
#endif
    TaskletInfo *m_info;
    TaskMngrBase *m_task_mngr;
    TaskPlatIf *m_task_plat;
    uint8_t m_nbr_info;
    uint8_t m_max_info;
    bool m_exit;
    int16_t m_ret;
    TaskBase *m_prev_task;
    uint16_t m_done_count;
    bool m_running;
};

template<bool add_it, typename T>
class AddIfTaskletHelper
{
public:
    AddIfTaskletHelper(TaskBase *task_base, T *tasklet, bool core = false)
    {
        task_base->AddTasklet(tasklet, core);
    }
};

}

}

#endif
