/*!
 * \file esys/evtloop/taskmngrbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKMNGRBASE_H__
#define __ESYS_EVTLOOP_TASKMNGRBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

namespace evtloop
{

class ESYS_API TaskBase;

class ESYS_API TaskMngrBase
{
public:
    TaskMngrBase();
    virtual ~TaskMngrBase();

    virtual void Init();
    virtual void Release();
    virtual void Setup();
    virtual void Run();

    virtual void AddTask(TaskBase *task);
protected:
    TaskBase *m_last_task;
    uint16_t m_task_count;
};

}

}

#endif
