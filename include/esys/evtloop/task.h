/*!
 * \file esys/evtloop/task.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbase.h"

#ifndef ESYS_MULTI_PLAT

#if defined ESYS_EXTPORT
// This can't be found in ESys, but this is the include file which shall be defined by an external port
#include "esys/extport/evtloop/task.h"
#elif defined ESYS_USE_ARD
#include "esys/evtloop/ard/task.h"
#elif defined ESYS_USE_STM32
#include "esys/evtloop/stm32/task.h"
#elif defined ESYS_USE_SYSC
#include "esys/evtloop/sysc/task.h"
#elif defined WIN32 || defined MULTIOS
#include "esys/evtloop/multios/task.h"
#else
#error When ESYS_MULTI_PLAT is not difined one of those must be: ESYS_EXTPORT, ESYS_USE_ARD, ESYS_USE_SYSC or WIN32
#endif

#else

namespace esys
{

namespace evtloop
{
/*! \class Task esys/evtloop/task.h "esys/evtloop/task.h"
 *  \brief Multi Platform Task implementation, which can use any platform as backend
 */
class ESYS_API Task: public TaskBase
{
public:
    Task(const char *name);
    virtual ~Task();

    // Application Task and Tasklet specific
    virtual void Init();
    virtual void Setup();
    //virtual void Loop()=0;

    // Platform and Application independent present both in Tasks and Tasklets
    //virtual void Exit(int16_t ret)=0;
    //virtual bool IsExit()=0;
    //virtual int16_t GetExit()=0;
    //virtual void Done()=0;

    // Platform functions for both Task and Tasklet
    virtual millis_t Millis();
    virtual void Sleep(millis_t ms);
    virtual void SleepU(micros_t us);

    // Platform and Application independent present in Tasks only
    //virtual void DoLoopIteration(bool all=false)=0;
    //virtual void Inits()=0;

    //virtual void SetInfoArray(TaskletInfo *info, uint8_t size)=0;
    //virtual void AddTasklet(Tasklet *tasklet, bool core=false)=0;

    // Platform and Application independent present in Tasklets only
    //virtual bool WaitCond()=0;

    // Platform functions for Task only
    virtual void DoLoopIterationEnds();
    virtual void Wait();
    virtual void Signal();

    virtual TaskPlatIf *GetTaskPlat();

    virtual void SetTaskBase(TaskBase *task_base);
    virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base);
    virtual void NotifyExit();
protected:
    virtual void InternalInit();
    virtual void InternalCoreLoop();

    //TaskPlatIf *m_task_plat;
    const char *m_name;
};

}

}

#endif

