/*!
 * \file esys/evtloop/mp/task.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_MP_TASK_H__
#define __ESYS_EVTLOOP_MP_TASK_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/evtloop/taskbase.h"
#include "esys/evtloop/mp/taskplat.h"

namespace esys
{

namespace evtloop
{

namespace mp
{

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4250 )
#endif

/*! \class Task esys/mp/task.h "esys/mp/task.h"
 *  \brief SystemC Task implementation
 */
class ESYS_API Task: public TaskBase, public TaskPlat
{
public:
    Task(const char *name);
    virtual ~Task();

    // TaskBaseIf
    virtual void Init();
    virtual void Setup();

    virtual TaskPlatIf *GetTaskPlat();
protected:
    // TaskBaseIf
    virtual void InternalInit();
    virtual void InternalCoreLoop();

    //TaskPlatIf *m_task_plat_if;
    const char *m_name;
};

#ifdef _MSC_VER
#pragma warning( pop )
#endif

}

}

}

#endif
