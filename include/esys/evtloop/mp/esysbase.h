/*!
 * \file esys/evtloop/mp/esysbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"
#include "esys/inttypes.h"

#ifndef ESYS_MULTI_PLAT

namespace esys
{

namespace evtloop
{

namespace mp
{

class ESYS_API ESysBase
{
public:
    static const bool MULTI_PLAT = false;

    ESysBase(int_t platform_id = -1, ESysBase *element = NULL);

    void SetElement(ESysBase *element);
};

} // namespace mp

} // namespace evtloop

} // namespace esys

#define ESYS_USING_PLAT_NS

#ifdef ESYS_USE_SYSC
#include "esys/evtloop/sysc/esys.h"
#elif defined WIN32 || defined MULTIOS
#include "esys/evtloop/multios/esys.h"
#elif defined ARDUINO
#include "esys/evtloop/arduino/esys.h"
#endif

#else

#if !defined(WIN32) && !defined(MULTIOS) && !defined(ESYS_MULTIOS)
#error ESYS_MULTI_PLAT requires to have WIN32 or MULTIOS be defined
#endif

#ifdef ESYS_USING_PLAT_NS
#undef ESYS_USING_PLAT_NS
#endif

#include "esys/platform/id.h"
#include "esys/staticlistid.h"
#include "esys/evtloop/taskplatif.h"
#include "esys/mutexbase.h"
#include "esys/semaphorebase.h"

#include <map>

namespace esys
{

namespace evtloop
{

class ESYS_API TaskMngrBase;

namespace mp
{

class ESYS_API ESysBase : public StaticListId<ESysBase, int_t>
{
public:
    typedef StaticListId<ESysBase, int_t> BaseType;

    static const bool MULTI_PLAT = true;

    // static void Sleep(millis_t msec);
    // static millis_t Millis();
    // static void Init();
    // static void Release();

    // int32_t CreateLog();
    // void FlushLog();

    /*static void SetPlatform(int platform_id);
    static void SetPlatform(platform::Id &platform_id); */
    //    static ESysBase &Get();
    // static int32_t GetExeDir(std::string &exe_dir);

    // void SetAppName(const char *app_name);
    platform::Id *GetPlatformId();
    const std::string &GetName();
    /*static TaskPlatIf *NewTaskPlat(const char *name);
    static TaskMngrBase *NewTaskMngrBase();
    static MutexBase *NewMutexBase(MutexBase::Type type); */
protected:
    ESysBase(int_t platform_id, const std::string &name);
    virtual ~ESysBase();

public:
    virtual void impSleep(millis_t msec) = 0;
    virtual millis_t impMillis() = 0;
    virtual ESysBase &impGet() = 0;
    virtual void impInit() = 0;
    virtual void impRelease() = 0;
    virtual esys::evtloop::TaskPlatIf *impNewTaskPlat(const char *name) = 0;
    virtual esys::evtloop::TaskMngrBase *impNewTaskMngrBase() = 0;
    virtual MutexBase *impNewMutexBase(MutexBase::Type type) = 0;
    // virtual int_t CreateLog();
    // virtual SemaphoreBase *impNewSemaphoreBase(const ObjectName &name, uint32_t count) = 0;
private:
    // int_t m_platform_id;
    // static ESysBase *m_esys;
    platform::Id *m_platform = nullptr;
    const std::string m_name;
};

} // namespace mp

} // namespace evtloop

} // namespace esys

#endif


