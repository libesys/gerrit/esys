/*!
 * \file esys/evtloop/mp/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"
#include "esys/inttypes.h"

#include <stddef.h>     //Needed for NULL under GCC

#if !defined(WIN32) && !defined(MULTIOS) && !defined(ESYS_MULTIOS)
#error ESYS_MULTI_PLAT requires to have WIN32 or MULTIOS be defined
#endif

#ifdef ESYS_USING_PLAT_NS
#undef ESYS_USING_PLAT_NS
#endif

#include "esys/platform/id.h"
#include "esys/staticlistid.h"
#include "esys/evtloop/taskplatif.h"
#include "esys/mutexbase.h"
#include "esys/evtloop/mp/esysbase.h"

#include <map>

namespace esys
{

namespace evtloop
{

class ESYS_API TaskMngrBase;

namespace mp
{

class ESYS_API ESys: public ESysBase
{
public:
    static void Sleep(millis_t msec);
    static millis_t Millis();
    static void Init();
    static void Release();

    int32_t CreateLog();
    void FlushLog();

    static void SetPlatform(int platform_id);
    static void SetPlatform(platform::Id &platform_id);
    static ESys &Get();
    static ESysBase &GetESysBase();
    static ESysBase *GetBase(std::size_t idx);
    static int32_t GetExeDir(std::string &exe_dir);

    void SetAppName(const char *app_name);
    platform::Id *GetPlatformId();
    static esys::evtloop::TaskPlatIf *NewTaskPlat(const char *name);
    static esys::evtloop::TaskMngrBase *NewTaskMngrBase();
    static MutexBase *NewMutexBase(MutexBase::Type type);

    static SemaphoreBase *NewSemaphoreBase(const ObjectName &name, uint32_t count);
protected:
    ESys();
    virtual ~ESys();

    /*void SetPluginsLoaded(bool plugins_loaded = true);
    int LoadPlugins();
    int ReleasePlugins(); */

    static ESys g_esys;
private:
    virtual void impSleep(millis_t msec);
    virtual millis_t impMillis();
    virtual ESys &impGet();
    virtual void impInit();
    virtual void impRelease();
    virtual esys::evtloop::TaskPlatIf *impNewTaskPlat(const char *name);
    virtual esys::evtloop::TaskMngrBase *impNewTaskMngrBase();
    virtual MutexBase *impNewMutexBase(MutexBase::Type type);

    static int_t m_platform_id;
    static ESysBase *m_esys;
    static platform::Id *m_platform;
};

}

//#ifdef ESYS_USING_PLAT_NS
using namespace mp;     // If compiled, it means it's a multi platform build and thus the default ESys
//#endif
}

}

