/*!
 * \file esys/evtloop/mp/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_MP_TASKPLAT_H__
#define __ESYS_EVTLOOP_MP_TASKPLAT_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskplatif.h"

namespace esys
{

namespace evtloop
{

namespace mp
{

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable : 4250 )
#endif

//class ESYS_API TaskPlatImpl;
class ESYS_API TaskMngr;

/*! \class TaskPlat esys/mp/taskplat.h "esys/mp/taskplat.h"
 *  \brief Multi Platform TaskPlat implementation
 */
class ESYS_API TaskPlat: public virtual TaskPlatIf
{
public:
    friend class ESYS_API ESys;

    TaskPlat(const char *name);
    virtual ~TaskPlat();

    // TaskPlatIf
    virtual millis_t Millis();
    virtual void Sleep(millis_t ms);
    virtual void SleepU(micros_t us);

    virtual void DoLoopIterationEnds();
    virtual void Wait();
    virtual void Signal();

    virtual void SetTaskBase(TaskBase *task_base);
    virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base);

    virtual void NotifyExit();

    // TaskPlat functions
    //void SetTaskMngr(TaskMngr *task_mngr);
    TaskMngrBase *GetTaskMngrBase();
    void SetTaskPlatIf(TaskPlatIf *task_plat_if);

    void StartTask();
    void WaitTaskDone();

    //static TaskPlat *Current();
    //static TaskBase *CurrentBase();
protected:
    //TaskPlatImpl *m_impl;
    TaskPlatIf *m_task_plat_if;
    TaskMngrBase *m_task_mngr;

};

#ifdef _MSC_VER
#pragma warning( pop )
#endif

}

using namespace mp;

}

}

#endif