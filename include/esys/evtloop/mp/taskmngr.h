/*!
 * \file esys/evtloop/mp/taskmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_MP_TASKMNGR_H__
#define __ESYS_EVTLOOP_MP_TASKMNGR_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/evtloop/taskmngrbase.h"

namespace esys
{

namespace evtloop
{

namespace mp
{

/*! \class TaskMngr esys/evtloop/mp/taskmngr.h "esys/evtloop/mp/taskmngr.h"
 *  \brief Multi Platform TaskMngr implementation, which can use any platform as backend
 */
class ESYS_API TaskMngr: public esys::evtloop::TaskMngrBase
{
public:
    TaskMngr();
    virtual ~TaskMngr();

    virtual void Init();
    virtual void Setup();
    virtual void Run();

    virtual void AddTask(TaskBase *task);
protected:
    TaskMngrBase *Get();
    TaskMngrBase *m_task_mngr;
};

}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif

}

}

#endif



