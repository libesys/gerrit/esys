/*!
 * \file esys/evtloop/tasklet.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKLET_H__
#define __ESYS_EVTLOOP_TASKLET_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskletif.h"

namespace esys
{

namespace evtloop
{

class ESYS_API TaskBase;
class ESYS_API TaskPlatIf;

/*! \class Tasklet esys/evtloop/tasklet.h "esys/evtloop/tasklet.h"
 *  \brief Piece of a Task
 */
class ESYS_API Tasklet: public virtual TaskletIf
{
public:
    static const bool IS_TASKLET = true;

    Tasklet();
    virtual ~Tasklet();

    // TaskletIf
    // Application Task and Tasklet specific
    virtual void Init() override;
    virtual void Setup() override;
    virtual void Loop() override;

    // Platform and Application independent present both in Tasks and Tasklets
    virtual void Exit(int16_t ret) override;
    virtual bool IsExit() override;
    virtual int16_t GetExit() override;
    virtual void TaskletDone() override;

    // Platform functions for both Task and Tasklet
    virtual millis_t Millis() override;
    virtual void Sleep(millis_t ms) override;
    virtual void SleepU(micros_t us) override;

    // Platform and Application independent present in Tasklets only
    virtual bool WaitCond() override;
    virtual int16_t Wait(StateType state, TimeOutType time_out_ms = WAIT_FOREVER) override;

    // Tasklet functions
    void SetTask(TaskBase *task);
    TaskBase *GetTask();

    TaskPlatIf *GetTaskPlat();

    void SetState(StateType state);
    StateType GetState();
    void SetPrevState(StateType prev_state);
    StateType GetPrevState();
protected:
    TaskBase *m_task = nullptr;
    TaskPlatIf *m_task_plat = nullptr;
    int16_t m_exit_val = 0;
    bool m_exit = false;
    StateType m_state = IDLE;
    StateType m_prev_state = IDLE;
};

}

}

#endif
