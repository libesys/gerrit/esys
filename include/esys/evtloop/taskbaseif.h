/*!
 * \file esys/evtloop/taskbaseif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKBASEIF_H__
#define __ESYS_EVTLOOP_TASKBASEIF_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/types.h"

namespace esys
{

namespace evtloop
{

struct ESYS_API TaskletInfo;
class ESYS_API Tasklet;

/*! \class TaskBaseIf esys/evtloop/taskbaseif.h "esys/evtloop/taskbaseif.h"
 *  \brief Defines the platform independent Task API
 */
class ESYS_API TaskBaseIf
{
public:

    // Application Task and Tasklet specific
    virtual void Init()=0;              //!< Initialize the Task
    virtual void Setup()=0;             //!< Setup the Task
    virtual void Loop()=0;              //!< The core of the task executed within its execition loop

    // Platform and Application independent present both in Tasks and Tasklets
    virtual void Exit(int16_t ret)=0;   //!< Called by a Task to terminate and exit
    virtual bool IsExit()=0;            //!< True if Exit was called, false otherwise
    virtual int16_t GetExit()=0;        //!< Return the code passed to Exit when it was called
    virtual void Done()=0;              //!< Called by a Task to inform that it has nothing left to do

    // Platform functions for both Task and Tasklet
    //virtual millis_t Millis()=0;
    //virtual void Sleep(millis_t ms)=0;
    //virtual void SleepU(micros_t us)=0;

    // Platform and Application independent present in Tasks only
    //virtual void DoLoopIteration(bool all=false)=0;
    virtual void Inits()=0;
    virtual void Setups()=0;

    virtual void SetInfoArray(TaskletInfo *info, uint8_t size)=0;
    virtual void AddTasklet(Tasklet *tasklet, bool core=false)=0;

    // Platform and Application independent present in Tasklets only
    //virtual bool WaitCond()=0;

    // Platform functions for Task only
    //virtual void DoLoopIterationEnds()=0;
    //virtual void Wait()=0;
    //virtual void Signal()=0;

protected:
    virtual void InternalInit()=0;
    virtual void InternalCoreLoop()=0;
};

}

}

#endif
