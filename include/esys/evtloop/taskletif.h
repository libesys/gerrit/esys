/*!
 * \file esys/evtloop/taskletif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKLETIF_H__
#define __ESYS_EVTLOOP_TASKLETIF_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

namespace esys
{

namespace evtloop
{

/*! \class TaskletIf esys/evtloop/taskletif.h "esys/evtloop/taskletif.h"
 *  \brief Defines the Tasklet API
 */
class ESYS_API TaskletIf
{
public:
    typedef uint16_t StateType;
    typedef int16_t TimeOutType;

    static const TimeOutType WAIT_FOREVER = -1;
    static const TimeOutType IDLE = 0;

    static const int16_t SUCCESS = 0;
    static const int16_t TIMED_OUT = -1;

    // Application Task and Tasklet specific
    virtual void Init() = 0;
    virtual void Setup() = 0;
    virtual void Loop() = 0;

    // Platform and Application independent present both in Tasks and Tasklets
    virtual void Exit(int16_t ret) = 0;
    virtual bool IsExit() = 0;
    virtual int16_t GetExit() = 0;
    virtual void TaskletDone() = 0;

    // Platform functions for both Task and Tasklet
    virtual millis_t Millis() = 0;
    virtual void Sleep(millis_t ms) = 0;
    virtual void SleepU(micros_t us) = 0;

    // Platform and Application independent present in Tasklets only
    virtual bool WaitCond() = 0;
    virtual int16_t Wait(StateType state, TimeOutType time_out_ms = WAIT_FOREVER) = 0;
};

}

}

#endif
