/*!
 * \file esys/evtloop/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKPLAT_H__
#define __ESYS_EVTLOOP_TASKPLAT_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#ifndef ESYS_MULTI_PLAT

#ifdef ESYS_USE_SYSC
#include "esys/evtlopp/sysc/taskplat.h"
#elif defined WIN32 || defined MULTIOS
#include "esys/evtloop/sysc/taskplat.h"
#elif defined ARDUINO
#include "esys/evtloop/arduino/taskplat.h"
#endif

#else

#include "esys/evtloop/mp/taskplat.h"

#endif

#endif
