/*!
 * \file esys/evtloop/taskmngrbase_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_EVTLOOP_TASKMNGRBASE_T_H__
#define __ESYS_EVTLOOP_TASKMNGRBASE_T_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/evtloop/taskmngrbase.h"
#include "esys/evtloop/taskbase.h"

#include <stddef.h>     //Needed for NULL under GCC

#include <vector>

namespace esys
{

namespace evtloop
{

template<typename T>
class TaskMngrBase_t: public TaskMngrBase
{
public:
    TaskMngrBase_t();
    virtual ~TaskMngrBase_t();

    virtual void Run();
    virtual void AddTask(TaskBase *task);
    bool AllTasksDone();

protected:
    struct TaskData
    {
        TaskBase *m_task_base;
        T *m_task_plat;
    };

    std::vector<TaskData *> m_tasks;
};

template<typename T>
TaskMngrBase_t<T>::TaskMngrBase_t(): TaskMngrBase()
{
}

template<typename T>
TaskMngrBase_t<T>::~TaskMngrBase_t()
{
    uint_t idx;

    for (idx=0; idx<m_tasks.size(); ++idx)
        delete m_tasks[idx];
}

template<typename T>
void TaskMngrBase_t<T>::Run()
{
    uint_t idx;

    TaskMngrBase::Run();

    for (idx=0; idx<m_tasks.size(); ++idx)
        m_tasks[idx]->m_task_plat->SetTaskMngrBase(this);
}

template<typename T>
void TaskMngrBase_t<T>::AddTask(TaskBase *task)
{
    TaskData *task_data;

    TaskMngrBase::AddTask(task);

    task->SetTaskMngr(this);

    task_data=new TaskData();
    task_data->m_task_base=task;
    task_data->m_task_plat=dynamic_cast<T *>(task->GetTaskPlat());
    //task_data->m_task_plat=dynamic_cast<T *>(task);
    task->SetTaskPlat(task_data->m_task_plat);

    m_tasks.push_back(task_data);
}

template<typename T>
bool TaskMngrBase_t<T>::AllTasksDone()
{
    uint16_t task_idx;

    for (task_idx=0; task_idx<m_tasks.size(); ++task_idx)
    {
        if (m_tasks[task_idx]->m_task_base->IsExit()==false)
            return false;
    }
    return true;
}

template<typename T, typename IMP>
class TaskMngrBaseImp_t: public TaskMngrBase_t<T>
{
public:
    TaskMngrBaseImp_t(const char *name);
    virtual ~TaskMngrBaseImp_t();

    IMP *GetImpl();
protected:
    IMP *m_impl;
};

template<typename T, typename IMP>
TaskMngrBaseImp_t<T,IMP>::TaskMngrBaseImp_t(const char *name): TaskMngrBase_t<T>()
{
    m_impl=new IMP(name);
    m_impl->SetTaskMngr(this);
}

template<typename T, typename IMP>
TaskMngrBaseImp_t<T,IMP>::~TaskMngrBaseImp_t()
{
    if (m_impl!=NULL)
        delete m_impl;
}

template<typename T, typename IMP>
IMP *TaskMngrBaseImp_t<T,IMP>::GetImpl()
{
    return m_impl;
}

}

}

#endif
