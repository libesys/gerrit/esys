/*!
 * \file esys/evtloop/stm32/task.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/evtloop/taskbase.h>
#include <esys/evtloop/stm32/taskplat.h>

#define IMPLEMENT_ESYS_TASK(task) \
    task g_task; \
 \
    int main(void) \
    { \
        g_task.Inits(); \
 \
        g_task.Setup(); \
 \
        g_task.Do(); \
        return 0; \
    }

namespace esys
{

namespace evtloop
{

namespace stm32
{

class ESYS_API Task: public TaskBase, public TaskPlat
{
public:
    Task(const char *name=NULL);
    virtual ~Task();

    virtual void Init();

    virtual void SerialEventRun();

protected:
    virtual void InternalCoreLoop();
};

}

#ifdef ESYS_USE_STM32
using namespace stm32;
#endif

}

}

