/*!
 * \file esys/arduino/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#ifndef ESYS_USE_ARD
#define ESYS_USE_ARD
#endif

#ifdef ESYS_USE_SYSC
#undef ESYS_USE_SYSC
#endif
#ifdef ESYS_USE_WX
#undef ESYS_USE_WX
#endif
#ifdef ESYS_USE_BOOST
#undef ESYS_USE_BOOST
#endif

#if defined WIN32 || defined MULTIOS
#include <string>
#endif

#include "esys/esys.h"

/*! \defgroup esys ESys core library
 *  The ESys core library
 *  @{
 */

namespace esys
{

namespace ard
{

class ESYS_API ESysImpl;

/*! \class ESys esys/esys.h "esys/event.h"
 *  \brief The base class of all logging or reporting events
 */
class ESYS_API ESys: public esys::ESysBase
{
public:

    static void Sleep(uint16_t msec);
    static millis_t Millis();

#if defined WIN32 || defined MULTIOS
    static ESys &Get();
    void SetAppName(const char *app_name);
    int32_t CreateLog();

    static int32_t GetExeDir(std::string &exe_dir);
#endif

protected:
    ESys();
    virtual ~ESys();

#if defined WIN32 || defined MULTIOS
    static ESys g_esys;

    static uint64_t m_started_time;


private:
    ESysImpl &m_impl;
#endif
};

}

#ifdef ESYS_USING_PLAT_NS
using namespace ard;
#endif

}
