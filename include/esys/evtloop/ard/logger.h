/*!
 * \file esys/arduino/logger.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_ARDUINO_LOGGER_H__
#define __ESYS_ARDUINO_LOGGER_H__

#include <esys/loggerbase.h>

#include "Stream.h"

namespace esys
{

class ESYS_API Logger: public LoggerBase
{
public:
    Logger();
    virtual ~Logger();

    void SetStream(::Stream *stream);

    virtual LoggerBase& operator<< (uint8_t val);
    virtual LoggerBase& operator<< (int8_t val);
    virtual LoggerBase& operator<< (uint16_t val);
    virtual LoggerBase& operator<< (int16_t val);
    virtual LoggerBase& operator<< (int8_t *string);
    virtual LoggerBase& operator<< (const char *string);

protected:
    virtual int16_t Write(uint8_t *buf, int16_t size);
    ::Stream *m_stream;
};

}

#endif
