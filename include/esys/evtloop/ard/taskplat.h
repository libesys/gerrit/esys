/*!
 * \file esys/evtloop/ard/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskplatif.h"

namespace esys
{

namespace evtloop
{

namespace ard
{

class ESYS_API TaskPlatImpl;
class ESYS_API TaskMngr;

/*! \class TaskPlat esys/evtloop/ard/taskplat.h "esys/evtloop/ard/taskplat.h"
 *  \brief Arduino TaskPlat implementation
 */
class ESYS_API TaskPlat: public virtual TaskPlatIf
{
public:
    friend class ESYS_API ESys;

    TaskPlat(const char *name);
    virtual ~TaskPlat();

    // TaskPlatIf
    virtual millis_t Millis() override;
    virtual void Sleep(millis_t ms) override;
    virtual void SleepU(micros_t us) override;

    virtual void DoLoopIterationEnds() override;
    virtual void Wait() override;
    virtual void Signal() override;

    virtual void SetTaskBase(TaskBase *task_base) override;
    virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base) override;

    virtual void NotifyExit() override;

    // TaskPlat functions
    //void SetTaskMngr(TaskMngr *task_mngr);
    TaskMngrBase *GetTaskMngrBase();

    void StartTask();
    void WaitTaskDone();

    static TaskPlat *Current();
    static TaskBase *CurrentBase();
protected:
    TaskMngrBase *m_task_mngr;

};

}

}

}


