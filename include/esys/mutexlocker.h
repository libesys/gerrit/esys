/*!
 * \file esys/mutexlocker.h
 * \brief
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2015-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MUTEXLOCKER_H__
#define __ESYS_MUTEXLOCKER_H__

#include "esys/esys_defs.h"
#include "esys/mutex.h"

namespace esys
{

/*! \class MutexLocker esys/mutexlocker.h "esys/mutexlocker.h"
 *  \brief Class to lock and unlock automatically a mutex following object lifecycle
 */
class ESYS_API MutexLocker
{
public:
    //! Constructor
    /*!
     *  \param[in] mutex the controlled Mutex
     *  \param[in] from_isr tells if the MutexLocker is instantiated from a HW interrupt context
     *
     */
    MutexLocker(Mutex& mutex, bool from_isr = false);

    //! Destructor
    ~MutexLocker();

    //! Tells if the Mutex was successfully locked
    /*!
     *  \return true if the Mutex was locked, false otherwise
     */
    bool IsOk() const;
private:
//!< \cond DOXY_IMPL
    MutexLocker(const MutexLocker&) = delete;
    MutexLocker& operator=(const MutexLocker&) = delete;

    bool m_is_ok;       //!< Return true of the Mutex was successfully locked
    Mutex& m_mutex;     //!< The controlled Mutex
    bool m_from_isr;    //!< Tells if the object instantiation was done from a HW interrupt context
//!< \endcond
};

}

#endif

