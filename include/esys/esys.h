/*!
 * \file esys/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_ESYS_H__
#define __ESYS_ESYS_H__

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"
#include "esys/inttypes.h"

#ifndef ESYS_MULTI_PLAT

#define ESYS_USING_PLAT_NS

#ifdef ESYS_USE_SYSC
#include "esys/sysc/esys.h"
#elif defined ESYS_USE_FREERTOS
#include "esys/freertos/esys.h"
#elif defined ARDUINO
#include "esys/arduino/esys.h"
#elif defined ESYS_EXTPORT
// This can't be found in ESys, but this is the include file which shall be defined by an external port
#include "esys/extport/esys.h"
#elif defined WIN32 || defined MULTIOS
#include "esys/multios/esys.h"
#else
#endif

#else

#if !defined(WIN32) && !defined(MULTIOS) && !defined(ESYS_MULTIOS)
#error ESYS_MULTI_PLAT requires to have WIN32 or MULTIOS or ESYS_MULTIOS be defined
#endif

#ifdef ESYS_USING_PLAT_NS
#undef ESYS_USING_PLAT_NS
#endif

#include "esys/mp/esys.h"

#endif

#endif
