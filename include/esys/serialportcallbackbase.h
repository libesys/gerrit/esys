/*!
 * \file esys/serialportcallbackbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/ringbuffer.h"

namespace esys
{

class ESYS_API SerialPortBase;

/*! \class SerialPortCallbackBase esys/serialportcallbackbase.h "esys/serialportcallbackbase.h"
 *  \brief Base class of all SerialPort Callbacks
 */
class ESYS_API SerialPortCallbackBase
{
public:
    //! Constructor
    SerialPortCallbackBase();

    //! Destructor
    virtual ~SerialPortCallbackBase();

    void SetSerialPortBase(SerialPortBase *serial_port);
    SerialPortBase *GetSerialPortBase();

    //! Used to trigger the callback
    /*!
     */
    virtual int Reveived(RingBuffer<uint8_t> &buffer) = 0;
protected:
    SerialPortBase *m_serial_port = nullptr;
};

}




