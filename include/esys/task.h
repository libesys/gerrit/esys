/*!
 * \file esys/task.h
 * \brief The Task class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASK_H__
#define __ESYS_TASK_H__

#include "esys/esys_defs.h"

#ifndef ESYS_MULTI_PLAT

#ifdef ESYS_USE_SYSC

#elif defined(ESYS_USE_BOOST)
#include "esys/boost/task.h"
#elif defined(ESYS_USE_FREERTOS)
#include "esys/freertos/task.h"
#else

#endif

#else

#include "esys/mp/task.h"

#endif

#endif



