/*!
 * \file esys/bufferwrap.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BUFFERWRAP_H__
#define __ESYS_BUFFERWRAP_H__

#include "esys/esys_defs.h"
#include "esys/bufferwrapbase.h"

namespace esys
{

template<typename T, uint8_t N_EXT_SEG=1>
class BufferWrap: public BufferWrapBase<T, N_EXT_SEG>
{
public:
    BufferWrap(T *p=nullptr, uint16_t max_size=0)
        : BufferWrapBase<T>(max_size, 0), m_p(p)
    {
        this->SetSize(0);
    }

    BufferWrap(T *p, uint16_t max_size, uint16_t size): BufferWrapBase<T>(max_size, size), m_p(p)
    {
        this->SetSize(size);
    }

    virtual ~BufferWrap()
    {
    }

    virtual void SetData(T *p, uint16_t size)
    {
        m_p=p;
        this->SetSize(size);
        //this->m_idx=0;
    }

protected:
    inline virtual T *GetData()
    {
        return m_p;
    }

    T *m_p;
};

}

#endif


