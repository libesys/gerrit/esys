/*!
 * \file esys/log.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_setup.h"

#ifdef ESYS_LOG_USE

#include "esys/logger.h"

#define ESYS_LOG( x ) esys::Logger::Get() << x
#define ESYS_LOG_TRANS( x ) esys::Logger::Get() << esys::Begin << x << esys::End
#define ESYS_LOG_TRANS_CH( ch, x ) esys::Logger::Get() << esys::Begin << esys::SetChannel(ch) << x << esys::End
#define ESYS_LOG_FLUSH esys::Logger::Get().Flush()

#define ESYS_LOG_DEBUG( x ) esys::Logger::Get() << esys::Begin << esys::Debug << x << esys::End
#define ESYS_LOG_INFO( x ) esys::Logger::Get() << esys::Begin << esys::Info << x << esys::End
#define ESYS_LOG_WARN( x ) esys::Logger::Get() << esys::Begin << esys::Warn << x << esys::End
#define ESYS_LOG_CRITICAL( x ) esys::Logger::Get() << esys::Begin << esys::Critical << x << esys::End
#define ESYS_LOG_FATAL( x ) esys::Logger::Get() << esys::Begin << esys::Fatal << x << esys::End

#define ESYS_LOG_TH_DEBUG esys::Logger::Get().SetThresholdLevel(esys::Logger::LOG_DEBUG)
#define ESYS_LOG_TH_INFO esys::Logger::Get().SetThresholdLevel(esys::Logger::LOG_INFO)
#define ESYS_LOG_TH_WARN esys::Logger::Get().SetThresholdLevel(esys::Logger::LOG_WARN)
#define ESYS_LOG_TH_CRITICAL esys::Logger::Get().SetThresholdLevel(esys::Logger::LOG_CRITICAL)
#define ESYS_LOG_TH_FATAL esys::Logger::Get().SetThresholdLevel(esys::Logger::LOG_FATAL)


#ifdef ESYS_VHW
#define ESYS_LOG_VHW( x )
#else
#endif

#else

#define ESYS_LOG( x )
#define ESYS_LOG_TRANS( x )
#define ESYS_LOG_TRANS_CH( ch, x )
#define ESYS_LOG_FLUSH

#define ESYS_LOG_DEBUG( x )
#define ESYS_LOG_INFO( x )
#define ESYS_LOG_WARN( x )
#define ESYS_LOG_CRITICAL( x )
#define ESYS_LOG_FATAL( x )

#define ESYS_LOG_TH_DEBUG
#define ESYS_LOG_TH_INFO
#define ESYS_LOG_TH_WARN
#define ESYS_LOG_TH_CRITICAL
#define ESYS_LOG_TH_FATAL

#endif

#include "esys/log/segger.h"


