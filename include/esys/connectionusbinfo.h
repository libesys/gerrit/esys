/*!
 * \file esys/connectionusbinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_CONNECTIONUSBINFO_H__
#define __ESYS_CONNECTIONUSBINFO_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/connectioninfo.h>
#include <esys/usbaddr.h>

#include <ostream>

namespace esys
{

class ESYS_API ConnectionUSBInfo: public ConnectionInfo
{
public:
    class ESYS_API Baudrate
    {
    public:
        Baudrate(const char *name=NULL, uint32_t value=0): m_name(name), m_value(value)
        {
            m_baudrate=m_next++;
        }
        /*Baudrate& Baudrate::operator= (const int &i)
        {
            m_baudrate=i;

            return *this;
        } */
        const uint32_t GetValue() const
        {
            return m_value;
        }
        operator uint32_t()
        {
            return m_value;
        }
        bool operator==(const Baudrate& rhs) const
        {
            return (rhs.m_baudrate==m_baudrate);
        }
        const char *GetName() const
        {
            return m_name;
        }
    protected:
        static uint32_t m_next;
        const char *m_name;
        uint32_t m_value;
        uint32_t m_baudrate;
    };

    static const Baudrate BPS_9600;
    static const Baudrate BPS_19200;
    static const Baudrate BPS_38400;
    static const Baudrate BPS_57600;
    static const Baudrate BPS_115200;
    static const Baudrate BPS_230400;
    static const Baudrate BPS_460800;
    static const Baudrate BPS_921600;
    static const Baudrate BPS_1M5;
    static const Baudrate BPS_1M8432;
    static const Baudrate BPS_3M;

    ConnectionUSBInfo();
    ConnectionUSBInfo(uint16_t vid, uint16_t pid,const Baudrate *b);
    ConnectionUSBInfo(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi, const Baudrate *b);
    virtual ~ConnectionUSBInfo();

    /*uint16_t GetVID();
    uint16_t GetPID();
    uint16_t GetREV();
    uint16_t GetMI(); */
    void SetUSBAddr(uint16_t vid, uint16_t pid);
    void SetUSBAddr(uint16_t vid, uint16_t pid, uint16_t rev);
    void SetUSBAddr(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi);

    const USBAddr &GetUSBAddr() const;
    USBAddr &GetUSBAddr();
    void SetBaudrate(const Baudrate *b);
    const Baudrate *GetBaudrate() const;
    bool UseCTSRTS() const;
    void SetUseCTSRTS(bool use);
    uint16_t GetStopBits() const;
    void SetStopBits(uint16_t stop_bits);
    const std::string &GetName();

    virtual const std::string &GetAddr();
protected:
    USBAddr m_usb_addr;
    /*uint16_t m_vid;
    uint16_t m_pid;
    uint16_t m_rev;
    uint16_t m_mi;
    bool m_is_rev_set;
    bool m_is_mi_set; */
    const Baudrate *m_baudrate;
    std::string m_addr;
    std::string m_name;
    bool m_use_ctsrts;
    uint16_t m_stop_bits;
};

}

ESYS_API std::ostream& operator<< (std::ostream& stream, const esys::ConnectionUSBInfo::Baudrate& b);

#endif
