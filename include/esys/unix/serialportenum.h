/*!
 * \file esys/unix/serialportenum.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/serialportenumbase.h"

namespace esys
{

class ESYS_API SerialPortEnum: public SerialPortEnumBase
{
public:
    SerialPortEnum();
    virtual ~SerialPortEnum();

    virtual int32_t Refresh() override;

    virtual uint32_t GetCount() override;

    virtual std::wstring GetPortName(uint32_t idx) override;

    static SerialPortEnum &Get();
protected:
    static SerialPortEnum s_com_port_enum;
};

}


