/*!
 * \file esys/unix/udevenum.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_UNIX_UDEVENUM_H__
#define __ESYS_UNIX_UDEVENUM_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/usbdevinfo.h"

#include <string>
#include <vector>
#include <map>

namespace esys
{

class ESYS_API UDevEnum
{
public:
    UDevEnum();
    ~UDevEnum();

    int FindCOMPort();
    std::wstring GetCOMPort(unsigned int idx);
    unsigned int GetCOMPortCount();

    int32_t FindDevName(USBDevInfo *dev_info, const std::wstring &port_name=L"");

    static UDevEnum &Get();

    void SetVerbose(bool verbose=true);
protected:
    static UDevEnum s_udev_enum;
    std::vector<std::wstring> m_com_port_vec;
    bool m_verbose;
};

}

#endif
