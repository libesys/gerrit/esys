/*!
 * \file esys/unix/usbtreeitem.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_UNIX_USBTREEITEM_H__
#define __ESYS_UNIX_USBTREEITEM_H__

#include "esys/usbtreeitembase.h"

namespace esys
{

class ESYS_API USBTreeItem: public USBTreeItemBase
{
public:
    USBTreeItem();
    virtual ~USBTreeItem();

};

}


#endif


