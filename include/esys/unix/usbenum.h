/*!
 * \file esys/unix/usbenum.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_UNIX_USBENUM_H__
#define __ESYS_UNIX_USBENUM_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/usbenumbase.h"
#include "esys/usbdevinfo.h"
#include "esys/usbtree.h"
#include "esys/usbdevserialportinfo.h"

#include <libusb-1.0/libusb.h>

#include <vector>
#include <map>

namespace esys
{

class USBEnumImpl;

class ESYS_API USBEnum: public USBEnumBase
{
public:
    USBEnum();
    virtual ~USBEnum();

    virtual int16_t Refresh();

    virtual uint16_t GetNbrDev();
    virtual USBDevInfo *GetDev(uint16_t idx);

    virtual uint16_t GetNbrSerialPort();
    virtual USBDevInfo *GetSerialPort(uint16_t idx);

    libusb_device_handle *GetLibUSBDevHandle(USBDevInfo *dev_info);

    void SetDebug(bool debug=true);

    USBTree &GetTree();
    static USBEnum &Get();

protected:
    virtual void Released(USBDevInfoBase *obj);
    void Remove(USBDevInfo *obj);

    static USBEnum s_usb_enum;
    bool m_debug;
    std::vector<USBDevInfo *> m_dev_infos;
    std::vector<USBDevInfo *> m_dev_serial_port_infos;
    std::map<std::wstring, USBDevInfo *> m_map_dev_infos;
    USBEnumImpl *m_impl;
    int m_init;
    USBTree m_root_tree;
};

}

#endif
