/*!
 * \file esys/usbdevserialportinfobase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_USBDEVINFOSERIALPORTBASE_H__
#define __ESYS_USBDEVINFOSERIALPORTBASE_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/usbdevinfobase.h>

namespace esys
{

class ESYS_API SerialPortImpl;

class ESYS_API USBDevSerialPortInfoBase: public virtual USBDevInfoBase
{
public:

    USBDevSerialPortInfoBase();
    virtual ~USBDevSerialPortInfoBase();

//    virtual void GetPortName(char *name,uint8_t &size)=0;
    virtual void Removed();
    virtual void SetSerialPortImpl(esys::SerialPortImpl *impl);
protected:
    esys::SerialPortImpl *m_serial_impl;
};

}

#endif