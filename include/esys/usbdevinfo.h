/*!
 * \file esys/usbdevinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <esys/usbdevinfobase.h>

#include <string>
#include <vector>
#include <map>

namespace esys
{

class ESYS_API SerialPortImpl;
class ESYS_API DevNode;
class ESYS_API USBTreeItem;

/*! \class USBDevInfo esys/usbdevinfo.h "esys/usbdevinfo.h"
 *  \brief The class storing information about a USB Device.
 */
class ESYS_API USBDevInfo : public virtual USBDevInfoBase
{
public:
    friend class ESYS_API USBEnum;

    USBDevInfo(Kind kind = BASIC);
    virtual ~USBDevInfo();

    uint8_t GetClass();
    uint8_t GetSubClass();
    uint16_t GetIdVendor();
    uint16_t GetIdProduct();
    uint8_t GetBusNumber();
    uint8_t GetDeviceAddress();

    const std::string &GetDescription() const;
    const std::string &GetManufacturer() const;
    const std::string &GetFriendlyName() const;
    const std::string &GetPhyDevObjName() const;
    const std::string &GetBusNumberWStr() const;
    const std::string &GetLocationPath() const;
    const std::string &GetSerialNumber() const;

    virtual bool operator==(const USBDevInfoBase &obj) const;
    bool operator==(const USBDevInfo &obj) const;

    virtual const std::string GetPortName_s() const;
    virtual const std::string &GetPortName() const;
    virtual int GetPortNameInt();
    void SetPortName(const char *name);
    void SetPortName(const std::string &name);
    virtual void SetSerialPortImpl(esys::SerialPortImpl *impl);
    uint32_t GetDeviceInstance();
    DevNode *GetDevNode();
    void SetSerialNumber(const std::string &number);
    void SetSerialNumber(const char *number);

    //! Set an internal Unique String
    void SetUID(const std::string &uid);

    //! Get the internal Unique String
    const std::string &GetUID();

    void SetParent(USBDevInfo *parent);
    USBDevInfo *GetParent();

    void AddChild(USBDevInfo *child);
    uint32_t GetChildrenCount() const;
    USBDevInfo *GetChild(uint32_t idx) const;
    // USBDevInfo *GetChild(const std::string &target);
    int32_t FindPortName();

    void SetUSBTreeItem(USBTreeItem *usb_tree_item);
    USBTreeItem *GetUSBTreeItem();
    const std::vector<std::string> &GetLocationPaths();

protected:
    void SetDescription(const std::string &desc);
    void SetManufacturer(const std::string &manufacturer);
    void SetFriendlyName(const std::string &name);
    void SetPhyDevObjName(const std::string &name);
    void SetBusNumber(const std::string &name);
    void SetDeviceAddress(uint8_t addr);
    void SetLocationPath(const std::string &name);
    void SetClass(uint8_t class_);
    void SetSubClass(uint8_t subclass);
    void SetIdVendor(uint16_t id_vendor);
    void SetIdProduct(uint16_t id_product);
    void SetBusNumber(uint8_t busnnum);
    void SetPorts(uint8_t *ports);
    void SetDeviceInstance(uint32_t dev_inst);
    void SetDevNode(DevNode *dev_node);
    void SetLocationPaths(const std::vector<std::string> &location_paths);

    std::string m_description;
    std::string m_manufacturer;
    std::string m_friendly_name;
    std::string m_phy_dev_obj_name;
    std::string m_bus_number;
    std::string m_location_path;
    std::string m_serial_port_name;
    std::string m_serial_number;
    std::string m_uid;
    uint32_t
        m_dev_inst; // Under windows, an opaque handle to the device instance (also known as a handle to the devnode).
    uint8_t m_class;
    uint8_t m_subclass;
    uint16_t m_id_vendor;
    uint16_t m_id_product;
    uint8_t m_bus_num;
    uint8_t m_dev_addr;
    uint8_t m_ports[16];
    esys::SerialPortImpl *m_serial_impl;
    DevNode *m_dev_node;
    USBDevInfo *m_parent;
    std::vector<USBDevInfo *> m_children;
    std::map<USBDevInfo *, bool> m_children_map;
    USBTreeItem *m_usb_tree_item = {nullptr};
    std::vector<std::string> m_location_paths;
};

} // namespace esys
