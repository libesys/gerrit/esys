/*!
 * \file esys/constnamed.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_CONSTNAMED_H__
#define __ESYS_CONSTNAMED_H__

#include "esys/esys_setup.h"

#if defined WIN32 || defined MULTIOS
#include <esys/multios/constnamed.h>
#else
#include <esys/em/constnamed.h>
#endif

#endif
