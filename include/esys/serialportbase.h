/*!
 * \file esys/serialportbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/ringbuffer.h"
#include "esys/serialportcallbackbase.h"

#include <string>
#include <memory>

namespace esys
{

/*! \class SerialPortBase esys/boost/serialportbase.h "esys/boost/serialportbase.h"
 *  \brief
 */
class ESYS_API SerialPortBase
{
public:
    SerialPortBase();               //!< Default constructor
    virtual ~SerialPortBase();

    virtual int Open(const std::string& devname) = 0;
    virtual int Open(const std::wstring& devname) = 0;
    virtual bool IsOpen() const = 0;
    virtual int Close() = 0;

    virtual const std::wstring &GetName();
    const std::string GetName_s();
    virtual void UseCTSRTS(bool fc) = 0;
    virtual void SetBaudrate(uint32_t baud) = 0;

    virtual int Write(uint8_t *buf, uint16_t size) = 0;
    virtual int Write(const std::string &data) = 0;
    virtual int Read(std::string &data, int timeout_ms = -1) = 0;

    virtual void SetCallback(SerialPortCallbackBase *call_back);
    SerialPortCallbackBase *GetCallback();
protected:
    std::wstring m_name;
    SerialPortCallbackBase *m_call_back = nullptr;
};

}



