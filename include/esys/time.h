/*!
 * \file esys/time.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_defs.h"
#include "esys/esys_setup.h"
#include "esys/inttypes.h"

#ifdef ESYS_MULTI_PLAT
#include "esys/mp/time.h"
#elif defined ESYS_USE_BOOST
#include "esys/boost/time.h"
#elif defined ESYS_USE_SYSC
#include "esys/sysc/time.h"
#elif defined ESYS_USE_FREERTOS
#include "esys/freertos/time.h"
#else
#error One of the following must be defined ESYS_USE_MP, ESYS_USE_BOOST, ESYS_USE_FREERTOS or ESYS_USE_SYSC
#endif


