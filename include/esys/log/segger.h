/*!
 * \file esys/log/segger.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/logchannel.h"

namespace esys
{

namespace log
{

class SeggerBufferBase
{
public:
    SeggerBufferBase(const char *name) : m_name(name)
    {
    }

    virtual const esys::uint32_t GetSizeUp() const = 0;
    virtual const esys::uint32_t GetSizeDown() const = 0;

    virtual esys::uint8_t *GetUp() = 0;
    virtual esys::uint8_t *GetDown() = 0;

    const char *GetName() const
    {
        return m_name;
    }
protected:
    const char *m_name;
};

template<unsigned BUF_SIZE_UP=1024, unsigned BUF_SIZE_DOWN=16>
class SeggerBuffer: public SeggerBufferBase
{
public:
    static const esys::uint32_t SIZE_UP = BUF_SIZE_UP;
    static const esys::uint32_t SIZE_DOWN = BUF_SIZE_DOWN;

    SeggerBuffer(const char *name): SeggerBufferBase(name)
    {
    }

    virtual const esys::uint32_t GetSizeUp() const override
    {
        return BUF_SIZE_UP;
    }

    virtual const esys::uint32_t GetSizeDown() const override
    {
        return BUF_SIZE_DOWN;
    }

    virtual esys::uint8_t *GetUp() override
    {
        return m_up_buf;
    }

    virtual esys::uint8_t *GetDown() override
    {
        return m_down_buf;
    }
protected:
    esys::uint8_t m_up_buf[BUF_SIZE_UP];
    esys::uint8_t m_down_buf[BUF_SIZE_DOWN];
};

class ESYS_API Segger : public LogChannel
{
public:
    Segger(unsigned buffer_index = 0);
    virtual ~Segger();

    virtual void PutStr(const char *buf) override;
    virtual void PutStr(char *buf, uint16_t size) override;
    virtual void Put(uint8_t *buf, uint16_t size) override;
    virtual void Flush() override;

    void SetBuffer(SeggerBufferBase *buf, int buffer_index = -1);
#ifdef ESYS_VHW
    static void SetWriteBufInfo(bool write_buf_info);
#endif
protected:
    unsigned m_buffer_index;
#ifdef ESYS_VHW
    static bool m_write_buf_info;
#endif
};

}

}




