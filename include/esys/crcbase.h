/*!
 * \file esys/crcbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

/*! \class CRCBase esys/crcbase.h "esys/crcbase.h"
 *  \brief Base class of all CRC implementations
 */
class ESYS_API CRCBase
{
public:
    //! Constructor
    CRCBase();

    //! Destructor
    virtual ~CRCBase();

    //! Reset the CRC, takes initial value
    virtual void Reset() = 0;

    //! Add data to continue computing the CRC
    /*!
     * \param[in] data buffer with data to add to the CRC
     * \param[in] size size of the data
     */
    virtual void AddData(esys::uint8_t *data, esys::uint32_t size) = 0;

    //! Return the calculated CRC
    /*!
     * \return the CRC
     */
    virtual esys::uint32_t GetChecksum() const = 0;

private:
};

}



