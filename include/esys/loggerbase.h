/*!
 * \file esys/loggerbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_LOGGERBASE_H__
#define __ESYS_LOGGERBASE_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

namespace esys
{
/*! \class LoggerBase esys/loggerbase.h "esys/loggerbase.h"
 *  \brief The base class of all Logger implementation
 */
class ESYS_API LoggerBase
{
public:
    //! Constructor
    LoggerBase();

    //! Destructor
    virtual ~LoggerBase();

    virtual LoggerBase& operator<< (uint8_t val);
    virtual LoggerBase& operator<< (int8_t val);
    virtual LoggerBase& operator<< (uint16_t val);
    virtual LoggerBase& operator<< (int16_t val);
    virtual LoggerBase& operator<< (int8_t *string);
    virtual LoggerBase& operator<< (const char *string);

    void Enable(bool enable=true);

    static LoggerBase &GetLogger(uint8_t idx=0);
protected:
//!< \cond DOXY_IMPL
    virtual int16_t Write(uint8_t *buf, int16_t size);
    static LoggerBase *m_dflt_logger;
    static LoggerBase &m_default_ref;
    static LoggerBase m_default;
    int8_t m_buffer[129];
    int16_t m_size = 0;
    bool m_enable = false;
//!< \endcond
};

}

#endif
