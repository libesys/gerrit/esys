/*!
 * \file esys/object.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_OBJECT_H__
#define __ESYS_OBJECT_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/objectname.h"
#include "esys/treeitem_if.h"
#include "esys/runtimelevel.h"
#include "esys/assert.h"

namespace esys
{

/*! \class Object esys/object.h "esys/object.h"
 *  \brief Base class of all classes needing controlled static and runtime initialization
 *
 * The initialization process is done in 2 phases:
 * - the static initialization is the first action made in the "main" function of the SW, before anything else.
 *   This enhances the C++ initialization of static variables by providing a deterministic initialization order
 *   independent of the compiler passes, independent of the translation units order chosen by the compiler.
 *   Effectively this solves the "static initialization order fiasco" as explained here
 *   https://isocpp.org/wiki/faq/ctors#static-init-order. Note that since done in the scope of the "main" function,
 *   the RTOS or OS scheduler is not started yet, so no RTOS or OS functions can be called by the static initialization
 *   code.
 * - the runtime initialization is performed by the SystemTask in multi tasking environment, if ESys is implemented
 *   on top of RTOS or OS, but also in the case of a bare metal or super loop implementation of ESys. The runtime
 *   initialization goes through a set of levels to allow a hierarchy defining inter-dependencies between objects
 *   to initialize. In effect, the system insure that all runtime initialization code of a runtime level N is completed
 *   before any code of runtime level N+1 is executed. When the runtime initialization takes place, the RTOS or OS is
 *   fully functional and any of its function can be called.
 */
class ESYS_API Object: public virtual TreeItem_if<Object>
{
public:
    //! Constructor
    /*!
     *  \param[in] name The name of the Object
     */
    Object(const ObjectName &name);

    //! Destructor
    virtual ~Object();

    //! Static initialization code run before starting the initialization of its children
    /*!
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t StaticInitBeforeChildren();

    //! Static initialization code run after all its children have been initialized
    /*!
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t StaticInitAfterChildren();

    //! Static release code run before starting the release of its children
    /*!
    *  \return 0 if successful, <0 otherwise
    */
    virtual int32_t StaticReleaseBeforeChildren();

    //! Static code run after all its children have been released
    /*!
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t StaticReleaseAfterChildren();

    //! Code run during the runtime initialization before starting the initialization of its children
    /*!
     *  \param[in] runtime_lvl the current Level of the runtime initialization
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeInitBeforeChildren(RuntimeLevel runtime_lvl);

    //! Code run during the runtime initialization after all its children have been initialized
    /*!
     *  \param[in] runtime_lvl the current Level of the runtime initialization
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeInitAfterChildren(RuntimeLevel runtime_lvl);

    //! Code run during the runtime release before starting the release of its children
    /*!
     *  \param[in] runtime_lvl the current Level of the runtime release
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeReleaseBeforeChildren(RuntimeLevel runtime_lvl);

    //! Code run during the runtime release after all its children have been released
    /*!
     *  \param[in] runtime_lvl the current Level of the runtime initialization
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeReleaseAfterChildren(RuntimeLevel runtime_lvl);

//!< \cond DOXY_IMPL
    //! Static initialization code run before or after the initialization of its children depending of the parameter
    /*! Used internally to ease the implementation and it will call StaticInitBeforeChildren and
     *  StaticInitAfterChildren when required.
     *
     *  \param[in] order specify if the initialization is before or after the children
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t StaticInit(Order order);

    //! Static release code run before or after the release of its children depending of the parameter
    /*! Used internally to ease the implementation and it will call StaticReleaseBeforeChildren and
     *  StaticReleaseAfterChildren when required.
     *
     *  \param[in] order specify if the release is before or after the children
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t StaticRelease(Order order);

    //! Runtime initialization code run before or after the initialization of its children depending of the parameter
    /*! Used internally to ease the implementation and it will call RuntimeInitBeforeChildren and
     *  RuntimeInitAfterChildren when required.
     *
     *  \param[in] runtime_lvl the level of runtime where the code will be executed
     *  \param[in] order specify if the initialization is before or after the children
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeInit(RuntimeLevel runtime_lvl, Order order);

    //! Runtime release code run before or after the release of its children depending of the parameter
    /*! Used internally to ease the implementation and it will call RuntimeReleaseBeforeChildren and
     *  RuntimeReleaseAfterChildren when required.
     *
     *  \param[in] runtime_lvl the level of runtime where the code will be executed
     *  \param[in] order specify if the release is before or after the children
     *  \return 0 if successful, <0 otherwise
     */
    virtual int32_t RuntimeRelease(RuntimeLevel runtime_lvl, Order order);
//!< \endcond

#ifdef ESYS_OBJECT_META
    //! Returns the name of the object
    /*!
     *  \return the object name
     */
    const char *GetName() const;

    //! Returns the unique id of the object
    /*! The id is simply the number of Object, which have had they constructor called already
     *  when the constructor of this Object is called.
     *
     *  \return the id of the object
     */
    int32_t GetId() const;
#endif
protected:
//!< \cond DOXY_IMPL

#ifdef ESYS_OBJECT_META
    static int32_t g_count;     //!< The number of objects
    const char *m_name;         //!< The name of the object
    int32_t m_id;               //!< The unique id of the object
#endif
//!< \endcond

};

}

#endif
