/*!
 * \file esys/i2cdevicebase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_I2CDEVICEBASE_H__
#define __ESYS_I2CDEVICEBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

namespace esys
{

class ESYS_API I2CBusBase;

class ESYS_API I2CDeviceBase
{
public:
    I2CDeviceBase(int16_t addr=-1);

    int16_t GetAddr();
    void SetAddr(int16_t addr);
    uint8_t GetSubAddr();
    void SetSubAddr(uint8_t sub_addr);

    I2CBusBase *GetBus();
    void SetBus(I2CBusBase *bus);

    // API corresponding to I2CHW_if
    int16_t Start();
    int16_t Stop();
    int16_t Read(uint8_t &value);
    int16_t Write(uint8_t value);
    int16_t Read(uint8_t *buf, uint16_t buf_len, bool repeat=false);
    int16_t Write(uint8_t *buf, uint16_t buf_len, bool repeat=false);
    int16_t Transfer(uint8_t *tx_buf, uint16_t tx_buf_len, uint8_t *rx_buf, uint16_t rx_buf_len, bool repeat=false);
protected:
    int16_t m_addr;
    uint8_t m_sub_addr;
    I2CBusBase *m_bus;
};

}

#endif

