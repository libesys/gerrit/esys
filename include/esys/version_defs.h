/*!
 * \file esys/version_defs.h
 * \brief Version info for esys
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYS_MAJOR_VERSION    0
#define ESYS_MINOR_VERSION    1
#define ESYS_RELEASE_NUMBER   0
#define ESYS_VERSION_STRING   "ESys 0.1.0"

// Must be updated manually as well each time the version above changes
#define ESYS_VERSION_NUM_DOT_STRING   "0.1.0"
#define ESYS_VERSION_NUM_STRING       "0100"

// nothing should be updated below this line when updating the version

#define ESYS_VERSION_NUMBER (ESYS_MAJOR_VERSION * 1000) + (ESYS_MINOR_VERSION * 100) + ESYS_RELEASE_NUMBER
#define ESYS_BETA_NUMBER      1
#define ESYS_VERSION_FLOAT ESYS_MAJOR_VERSION + (ESYS_MINOR_VERSION/10.0) + (ESYS_RELEASE_NUMBER/100.0) + (ESYS_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYS_CHECK_VERSION(major,minor,release) \
    (ESYS_MAJOR_VERSION > (major) || \
    (ESYS_MAJOR_VERSION == (major) && ESYS_MINOR_VERSION > (minor)) || \
    (ESYS_MAJOR_VERSION == (major) && ESYS_MINOR_VERSION == (minor) && ESYS_RELEASE_NUMBER >= (release)))

