/*!
 * \file esys/win32/usbexternalhubinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WIN32_USBEXTERNALHUBINFO_H__
#define __ESYS_WIN32_USBEXTERNALHUBINFO_H__

#include "esys/esys_defs.h"
#include "esys/win32/data.h"
#include "esys/win32/usbroothubinfo.h"

namespace esys
{

class ESYS_API USBExternalHUBInfo: public USBRootHUBInfo
{
public:
    USBExternalHUBInfo();
    ~USBExternalHUBInfo();

    //USBDeviceInfoType                       m_device_info_type;
    //PUSB_NODE_INFORMATION                   m_hub_info;
    //PUSB_HUB_INFORMATION_EX                 m_hub_info_ex;
    //std::wstring                            m_hub_name;
    PUSB_NODE_CONNECTION_INFORMATION_EX     m_connection_info;
    //PUSB_PORT_CONNECTOR_PROPERTIES          m_port_connector_props;
    PUSB_DESCRIPTOR_REQUEST                 m_config_desc;
    PUSB_DESCRIPTOR_REQUEST                 m_bos_desc;
    StringDescriptorNode                    *m_string_descs;
    PUSB_NODE_CONNECTION_INFORMATION_EX_V2  m_connection_info_v2; // NULL if root HUB
    //USBDevicePNPStrings                     *m_usb_device_properties;
    //DeviceInfoNode                          *m_device_info_node; // Never used??
    //DevInfo                                 *m_dev_info;
    //PUSB_HUB_CAPABILITIES_EX                m_hub_capability_ex;
};


}

#endif


