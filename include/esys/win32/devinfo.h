/*!
 * \file esys/win32/devinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

#include <setupapi.h>

#include <vector>
#include <map>
#include <string>

namespace esys
{

class ESYS_API DevInfo
{
public:
    DevInfo();
    virtual ~DevInfo();

    SP_DEVINFO_DATA &GetData();
    SP_DEVICE_INTERFACE_DATA &GetInterfaceData();
    void SetDescName(const std::string &desc_name);
    std::string &GetDescName();
    void SetDriverName(const std::string &driver_name);
    std::string &GetDriverName();

    void SetDetailDataSize(uint32_t size);
    PSP_DEVICE_INTERFACE_DETAIL_DATA GetDetailData();

    void SetFriendlyName(const std::string &friendly_name);
    std::string &GetFriendlyName();

    std::string &GetName();

    DEVICE_POWER_STATE &GetLatestPowerState();

protected:
    SP_DEVINFO_DATA m_device_info_data;
    SP_DEVICE_INTERFACE_DATA m_device_interface_data;
    PSP_DEVICE_INTERFACE_DETAIL_DATA m_detail_data;
    DEVICE_POWER_STATE m_latest_power_state;
    std::string m_desc_name;
    std::string m_driver_name;
    std::string m_friendly_name;
};

} // namespace esys
