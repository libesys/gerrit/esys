/*!
 * \file esys/win32/devnode.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/win32/data.h"

#include <cfgmgr32.h>

#include <vector>
#include <map>

namespace esys
{

BOOL DriverNameToDeviceInst(const std::string &DriverName,
                            //_In_reads_bytes_(cbDriverName) PCHAR DriverName,
                            // _In_ size_t cbDriverName,
                            _Out_ HDEVINFO *pDevInfo,
                            _Out_writes_bytes_(sizeof(SP_DEVINFO_DATA)) PSP_DEVINFO_DATA pDevInfoData);

/*USBDevicePNPStrings *DriverNameToDeviceProperties(
    _In_reads_bytes_(cbDriverName) PCHAR  DriverName,
    _In_ size_t cbDriverName
    ); */

void ESYS_API FreeDeviceProperties(USBDevicePNPStrings **ppDevProps);

class ESYS_API DevNode
{
public:
    enum class RefreshDir
    {
        LOCAL,
        UP,
        DOWN
    };

    DevNode();
    virtual ~DevNode();

    const std::string &GetInstanceId();
    void SetInstanceId(const std::string &instance_id);

    void SetParent(DevNode *parent);
    DevNode *GetParent();
    int32_t GetNbrChildren();
    DevNode *GetChild(int32_t idx);
    virtual void AddChild(DevNode *child, bool do_register = true);

    void SetProperty(const std::string &name, const std::string &value);
    int32_t GetProperty(const std::string &name, std::string &value);

    virtual int32_t Refresh(RefreshDir refresh_dir = RefreshDir::LOCAL);
    void SetDevInst(DEVINST dev_inst);
    DEVINST GetDevInst();

    static int32_t IsValid(DEVINST dev_inst);

protected:
    static int32_t win32GetParent(DEVINST dev_inst, DEVINST &parent_dev_inst);
    static int32_t win32GetIntanceId(DEVINST dev_inst, std::string &instance_id);

    DEVINST m_dev_inst = 0;
    DevNode *m_parent = nullptr;
    std::vector<DevNode *> m_children;
    std::string m_instance_id;
    std::map<std::string, std::string> m_properties;
};

} // namespace esys
