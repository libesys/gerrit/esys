/*!
 * \file esys/win32/setup.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WIN32_SETUP_H__
#define __ESYS_WIN32_SETUP_H__

#define ESYS_MULTIOS	1
#define ESYS_USE_WX			1
#define ESYS_USE_BOOST		1

#endif