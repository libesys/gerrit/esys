/*!
 * \file esys/win32/serialportenum.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/serialportenumbase.h"

#include <windows.h>
#include <setupapi.h>

#include <string>
#include <vector>

namespace esys
{

// typedef DWORD(WINAPI *CM_Open_DevNode_Key) (DWORD, DWORD, DWORD, DWORD, ::PHKEY, DWORD);

class ESYS_API DevNode;

class ESYS_API SerialPortEnum : public SerialPortEnumBase
{
public:
    SerialPortEnum();
    virtual ~SerialPortEnum();

    virtual int32_t Refresh();

    uint32_t GetCount();
    uint32_t GetCount(DevNode *dev_node);
    DevNode *GetPort(uint32_t idx);
    virtual std::string GetPortName(uint32_t idx) override;

    DevNode *Find(DevNode *dev_node, uint32_t idx = 0);
    int32_t Find(DevNode *dev_node, std::string &com_port, uint32_t idx = 0);

    static SerialPortEnum &Get();

protected:
    void Cleanup();

    static SerialPortEnum s_port_enum;

    bool m_debug;
    HDEVINFO m_dev_info;
    std::vector<DevNode *> m_ports;
    // HINSTANCE m_cfg_mgr;
    // CM_Open_DevNode_Key m_dev_node;
};

} // namespace esys
