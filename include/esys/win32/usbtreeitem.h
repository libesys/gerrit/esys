/*!
 * \file esys/win32/usbtreeitem.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/usbtreeitembase.h"
#include "esys/win32/data.h"
#include "esys/win32/usbroothubinfo.h"
#include "esys/win32/usbexternalhubinfo.h"
#include "esys/win32/usbdeviceinfo.h"

namespace esys
{

class ESYS_API USBTreeItem : public USBTreeItemBase
{
public:
    USBTreeItem();
    virtual ~USBTreeItem();

    void SetInfo(USBHostControllerInfo *hc_info);
    void SetInfo(USBRootHUBInfo *root_hub_info);
    void SetInfo(USBExternalHUBInfo *external_hub_info);
    void SetInfo(USBDeviceInfo *device_info);

    USBTreeItem *AddRootHUB(USBRootHUBInfo *root_hub);
    USBTreeItem *AddExternalHUB(USBExternalHUBInfo *external_hub);
    USBTreeItem *AddDevice(USBDeviceInfo *device, const std::string &name = "");

protected:
    USBHostControllerInfo *m_hc_info = nullptr;
    USBRootHUBInfo *m_root_hub_info = nullptr;
    USBExternalHUBInfo *m_external_hub_info = nullptr;
    USBDeviceInfo *m_device_info = nullptr;
};

} // namespace esys
