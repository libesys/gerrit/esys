/*!
 * \file esys/win32/devinfolist.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

#include <setupapi.h>

#include <vector>
#include <map>

namespace esys
{

class ESYS_API DevInfo;

class ESYS_API DevInfoList
{
public:
    DevInfoList();
    virtual ~DevInfoList();

    void Add(DevInfo *dev_info);
    void SetDeviceInfo(HDEVINFO device_info);
    HDEVINFO GetDeviceInfo();
    DevInfo *FindByDriverName(const std::string &driver_name);

protected:
    std::vector<DevInfo *> m_dev_infos;
    HDEVINFO m_device_info;
    std::map<std::string, DevInfo *> m_dev_infos_driver_name_map;
};

} // namespace esys
