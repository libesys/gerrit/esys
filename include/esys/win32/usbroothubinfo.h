/*!
 * \file esys/win32/usbroothubinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/win32/data.h"

namespace esys
{

class ESYS_API USBRootHUBInfo
{
public:
    USBRootHUBInfo();
    ~USBRootHUBInfo();

    void SetDeviceInfoType(USBDeviceInfoType type);
    USBDeviceInfoType GetDeviceInfoType();

    USB_NODE_INFORMATION &GetHUBInfo();
    bool GetHUBInfoValid();
    void SetHUBInfoValid(bool value);

    USB_HUB_INFORMATION_EX &GetHUBInfoEx();
    bool GetHUBInfoExValid();
    void SetHUBInfoExValid(bool value);

    USB_HUB_CAPABILITIES_EX &GetHUBCapabilitiesEx();
    bool GetHUBCapabilitiesExValid();
    void SetHUBCapabilitiesExValid(bool value);

    void SetName(const std::string &name);
    const std::string &GetName() const;

    USBDeviceInfoType m_device_info_type;
    USB_NODE_INFORMATION m_hub_info;
    bool m_hub_info_valid = true;
    USB_HUB_INFORMATION_EX m_hub_info_ex;
    bool m_hub_info_ex_valid = true;
    std::string m_hub_name;
    PUSB_PORT_CONNECTOR_PROPERTIES m_port_connector_props;
    USBDevicePNPStrings *m_usb_device_properties;
    // DeviceInfoNode                      *m_device_info_node;  // Never used??
    DevInfo *m_dev_info;
    USB_HUB_CAPABILITIES_EX m_hub_capabilities_ex;
    bool m_hub_capabilities_ex_valid = true;
};

} // namespace esys
