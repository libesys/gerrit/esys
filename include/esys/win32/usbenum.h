/*!
 * \file esys/win32/usbenum.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/multios/usbenumbase.h"
#include "esys/usbdevinfo.h"
//#include "esys/win32/data.h"
#include "esys/usbtreeitem.h"
#include "esys/usbtree.h"
#include "esys/win32/devinfolist.h"

#include <vector>
#include <map>

/*#include <usbioctl.h>
#include <usbiodef.h>
#include <usb.h>
#include <usbuser.h>
#include <setupapi.h> */

namespace esys
{

class ESYS_API USBHostCtrl;

class ESYS_API USBEnum : public multios::USBEnumBase
{
public:
    static const UCHAR NUM_STRING_DESC_TO_GET = 4;

    USBEnum();
    virtual ~USBEnum();

    virtual int Refresh() override;

    void SetDebug(bool debug = true);

    static USBEnum &Get();
    USBTree &GetTree();

    static void SetEnumerateHostControllers(bool enumerate_host_controllers = true);
    static bool GetEnumerateHostControllers();

protected:
    void EnumerateHostControllers();
    void EnumerateHostController(USBHostCtrl *hc);
    void EnumerateAllDevices();
    void EnumerateHubPorts(USBTreeItem *parent, HANDLE hHubDevice, ULONG NumPorts);
    void EnumerateHub(
        // HTREEITEM                                       hTreeParent,
        USBTreeItem *parent,
        //_In_reads_(cbHubName) PCHAR                     HubName,
        //_In_ size_t                                     cbHubName,
        const std::string &HubName, _In_opt_ PUSB_NODE_CONNECTION_INFORMATION_EX ConnectionInfo,
        _In_opt_ PUSB_NODE_CONNECTION_INFORMATION_EX_V2 ConnectionInfoV2,
        _In_opt_ PUSB_PORT_CONNECTOR_PROPERTIES PortConnectorProps, _In_opt_ PUSB_DESCRIPTOR_REQUEST ConfigDesc,
        _In_opt_ PUSB_DESCRIPTOR_REQUEST BosDesc, _In_opt_ StringDescriptorNode *StringDescs,
        _In_opt_ USBDevicePNPStrings *DevProps);
    // DeviceInfoNode *FindMatchingDeviceNodeForDriverName(const std::string &DriverKeyName, BOOLEAN IsHub);
    DevInfo *FindDevInfoByDriverName(const std::string &DriverKeyName, bool IsHub);
    StringDescriptorNode *GetAllStringDescriptors(HANDLE hHubDevice, ULONG ConnectionIndex,
                                                  PUSB_DEVICE_DESCRIPTOR DeviceDesc,
                                                  PUSB_CONFIGURATION_DESCRIPTOR ConfigDesc);
    USBTreeItem *AddHostController(USBHostControllerInfo *hcInfo, const std::string &leaf_name);

    void EnumerateAllDevicesWithGuid(DevInfoList &dev_info_list, LPGUID guid);

    int32_t GetHWIds(const std::string &buf, uint16_t &vid, uint16_t &pid, uint16_t &rev, bool &is_rev_set,
                     uint16_t &mi, bool &is_mi_set);
    void ResolveParentChildRelations();

    static USBEnum s_usb_enum;
    static bool s_enumerate_host_controllers;
    bool m_debug;
    std::vector<USBHostCtrl *> m_hc_ctrls;
    std::map<std::string, USBHostCtrl *> m_map_hc_ctrls;
    int m_total_devices_connected;
    int m_total_hubs;
    // DeviceGUIDList m_hub_list;
    // DeviceGUIDList m_device_list;
    LIST_ENTRY m_enumerated_hc_list_head;
    BOOL m_do_config_desc;
    BOOL m_do_annotation;
    BOOL m_log_debug;

    struct HCInfo
    {
        std::string m_name;
        USBHostControllerInfo *m_info;
    };
    std::vector<HCInfo> m_hc_list;
    USBTree m_root_tree;
    DevInfoList m_usb_dev_info;
    DevInfoList m_hub_dev_info;
    DevInfoList m_hc_dev_info;
};

} // namespace esys

