/*!
 * \file esys/win32/usbdeviceinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WIN32_USBDEVICEINFO_H__
#define __ESYS_WIN32_USBDEVICEINFO_H__

#include "esys/esys_defs.h"
#include "esys/win32/data.h"

#include <vector>

namespace esys
{

class ESYS_API USBDeviceInfoStringDesc
{
public:
    USBDeviceInfoStringDesc(unsigned char descriptor_index, unsigned short m_language_id, const std::wstring &m_value);
protected:
    unsigned char m_descriptor_index;
    unsigned short m_language_id;
    std::wstring m_value;
};

// HubInfo, HubName may be in USBDEVICEINFOTYPE, so they can be removed
class ESYS_API USBDeviceInfo
{
public:
    USBDeviceInfo();
    ~USBDeviceInfo();

    void SetStringDescs(StringDescriptorNode *string_descs);
    void SetUSBDevicePNPStrings(USBDevicePNPStrings *props);

    USBDeviceInfoType                       m_device_info_type;
    PUSB_NODE_INFORMATION                   m_hub_info;          // NULL if not a HUB
    PUSB_HUB_INFORMATION_EX                 m_hub_info_ex;        // NULL if not a HUB
    PCHAR                                   m_hub_name;          // NULL if not a HUB
    PUSB_NODE_CONNECTION_INFORMATION_EX     m_connection_info;   // NULL if root HUB
    PUSB_PORT_CONNECTOR_PROPERTIES          m_port_connector_props;
    PUSB_DESCRIPTOR_REQUEST                 m_config_desc;       // NULL if root HUB
    PUSB_DESCRIPTOR_REQUEST                 m_bos_desc;          // NULL if root HUB
    StringDescriptorNode                    *m_string_descs;
    PUSB_NODE_CONNECTION_INFORMATION_EX_V2  m_connection_info_v2; // NULL if root HUB
    USBDevicePNPStrings                     *m_usb_device_properties = nullptr;
    //DeviceInfoNode                          *m_device_info_node;
    DevInfo                                 *m_dev_info;
    PUSB_HUB_CAPABILITIES_EX                m_hub_capability_ex;  // NULL if not a HUB
protected:
    std::vector<USBDeviceInfoStringDesc> m_string_desc_vec;
};

}

#endif
