/*!
 * \file esys/win32/data.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/usbtreeitembase.h"
#include "esys/win32/usbdeviceinfotype.h"

#include <usbioctl.h>
//#include <usbiodef.h>
#include <usb.h>
#include <usbuser.h>
#include <setupapi.h>

#include <string>
#include <vector>

#ifdef DEBUG
#undef DBG
#define DBG 1
#endif

#if DBG
#define OOPS() Oops(__FILE__, __LINE__)
#else
#define OOPS()
#endif

#if DBG

#define ALLOC(dwBytes) MyAlloc(__FILE__, __LINE__, (dwBytes))

#define REALLOC(hMem, dwBytes) MyReAlloc((hMem), (dwBytes))

#define FREE(hMem) MyFree((hMem))

#define CHECKFORLEAKS() MyCheckForLeaks()

#else

#define ALLOC(dwBytes) GlobalAlloc(GPTR, (dwBytes))

#define REALLOC(hMem, dwBytes) GlobalReAlloc((hMem), (dwBytes), (GMEM_MOVEABLE | GMEM_ZEROINIT))

#define FREE(hMem) GlobalFree((hMem))

#define CHECKFORLEAKS()

#endif

namespace esys
{

class ESYS_API DevInfo;

class ESYS_API StringDescriptorNode
{
public:
    StringDescriptorNode *m_next;
    UCHAR m_descriptor_index;
    USHORT m_language_id;
    USB_STRING_DESCRIPTOR m_string_descriptor[1];
};

//
// A collection of device properties. The device can be hub, host controller or usb device
//
class ESYS_API USBDevicePNPStrings
{
public:
    USBDevicePNPStrings();
    ~USBDevicePNPStrings();

    void SetDeviceId(const std::string &device_id);
    std::string &GetDeviceId();
    void SetDeviceDesc(const std::string &device_desc);
    std::string &GetDeviceDesc();
    void SetHWId(const std::string &hw_id);
    std::string &GetHWId();
    void SetService(const std::string &device_id);
    std::string &GetService();
    void SetDeviceClass(const std::string &device_class);
    std::string &GetDeviceClass();
    void SetPowerState(const std::string &device_class);
    std::string &GetPowerState();

    static USBDevicePNPStrings *New();
    static void Release();

protected:
    static std::vector<USBDevicePNPStrings *> m_vec;

    std::string m_device_id;
    std::string m_device_desc;
    std::string m_hw_id;
    std::string m_service;
    std::string m_device_class;
    std::string m_power_state;
};

//
// Structures associated with TreeView items through the lParam.  When an item
// is selected, the lParam is retrieved and the structure it which it points
// is used to display information in the edit control.
//

class ESYS_API USBHostControllerInfo
{
public:
    USBHostControllerInfo();
    ~USBHostControllerInfo();

    USBDeviceInfoType m_device_info_type;
    LIST_ENTRY m_list_entry;
    // PCHAR                               m_driver_key;
    // PWCHAR                              m_driver_key;
    std::string m_driver_key;
    ULONG m_vendor_id;
    ULONG m_device_id;
    ULONG m_sub_sys_id;
    ULONG m_revision;
    USB_POWER_INFO m_usb_power_info[6];
    BOOL m_bus_device_function_valid;
    ULONG m_bus_number;
    USHORT m_bus_device;
    USHORT m_bus_function;
    PUSB_CONTROLLER_INFO_0 m_controller_info;
    USBDevicePNPStrings *m_usb_device_properties = nullptr;
};

class ESYS_API StringList
{
public:
#ifdef H264_SUPPORT
    ULONGLONG ulFlag;
#else
    ULONG ulFlag;
#endif
    PCHAR m_psz_string;
    PCHAR m_psz_modifier;
};

} // namespace esys
