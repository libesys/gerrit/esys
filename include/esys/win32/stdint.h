/*!
 * \file esys/win32/sdtint.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WIN32_STDINT_H__
#define __ESYS_WIN32_STDINT_H__

#include "esys/inttypes.h"

#endif
