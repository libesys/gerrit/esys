/*!
 * \file esys/win32/devnodemngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/win32/devnode.h"

#include <map>

namespace esys
{

class ESYS_API DevNodeMngr : public DevNode
{
public:
    friend class ESYS_API DevNode;

    DevNodeMngr();
    virtual ~DevNodeMngr();

    DevNode *NewNode();

    void SetRoot(DevNode *root);
    DevNode *GetRoot();

    DevNode *Find(const std::string &instance_id);
    DevNode *Find(DEVINST dev_inst);
    void Register(DevNode *dev_node);
    DevNode *Register(DEVINST dev_inst);

    int32_t Refresh();

    uint32_t GetCount();

    static DevNodeMngr &Get();

protected:
    static DevNodeMngr s_mngr;

    std::map<std::string, DevNode *> m_map;
    std::vector<DevNode *> m_vec;
    uint32_t m_refresh_id;
    uint32_t m_count;
    DevNode *m_root = {nullptr};
};

} // namespace esys
