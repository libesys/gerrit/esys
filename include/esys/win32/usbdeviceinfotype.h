/*!
 * \file esys/win32/usbdeviceinfotype.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_WIN32_USBDEVICEINFOTYPE_H__
#define __ESYS_WIN32_USBDEVICEINFOTYPE_H__

namespace esys
{

typedef enum USBDeviceInfoType
{
    HostControllerInfo,
    RootHubInfo,
    ExternalHubInfo,
    DeviceInfo
} USBDeviceInfoType, *PUSBDeviceInfoType;

}

#endif
