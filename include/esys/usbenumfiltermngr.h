/*!
 * \file esys/usbenumfiltermngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/usbenumfilter.h"

namespace esys
{

class ESYS_API USBEnumFilterMngr
{
public:
    USBEnumFilterMngr();
    virtual ~USBEnumFilterMngr();

    void Filter(USBDevInfo *dev);

    virtual std::size_t GetNbrFilter() = 0;
    virtual USBEnumFilter *GetFilter(std::size_t idx) = 0;

    virtual std::size_t GetNbrFiltered();
    virtual USBDevInfo *GetFiltered(std::size_t idx);

    void ClearFiltered();

protected:
};

} // namespace esys
