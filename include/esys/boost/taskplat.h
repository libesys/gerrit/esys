/*!
 * \file esys/boost/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_TASKPLAT_H__
#define __ESYS_BOOST_TASKPLAT_H__

#include "esys/esys_defs.h"
#include "esys/taskplatif.h"

namespace esys
{

namespace boost
{

class ESYS_API Task;
class ESYS_API TaskPlatImpl;
class ESYS_API TaskMngrPlat;
class ESYS_API TaskMngrPlatImpl;

class ESYS_API TaskPlat;

/*! \class TaskPlat esys/boost/taskplat.h "esys/boost/taskplat.h"
 *  \brief
 */
class ESYS_API TaskPlat: public virtual esys::TaskPlatIf
{
public:
    TaskPlat(TaskBase *task_base);
    virtual ~TaskPlat();
    // Platform functions
    virtual int32_t Create() override;
    virtual int32_t Start() override;
    virtual int32_t Stop() override;
    virtual int32_t Kill() override;
    virtual int32_t Destroy() override;
    //virtual int32_t Done() override;

    virtual millis_t Millis() override;
    virtual void Sleep(millis_t ms) override;
    virtual void SleepU(micros_t us) override;

    virtual void SetTaskBase(TaskBase *task_base) override;

    //virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base)=0;
    //virtual void NotifyExit()=0;
    TaskPlatImpl *GetImpl();
    void SetTaskMngrBase(TaskMngrBase *taskmngr_base);

    void SetTask(Task *task);
    Task *GetTask();

    static TaskPlat &GetCurrent();
protected:
    Task *m_task;
    TaskBase *m_task_base;
    TaskPlatImpl *m_impl;
    TaskMngrPlat *m_mngr;
    TaskMngrPlatImpl *m_mngr_impl;
};

}

}

#endif


