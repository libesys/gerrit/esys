/*!
 * \file esys/boost/mutex.h
 * \brief Declaration of the Boost Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_MUTEX_H__
#define __ESYS_BOOST_MUTEX_H__

#include "esys/esys_defs.h"
#include "esys/mutexbase.h"

namespace esys
{

namespace boost
{

class ESYS_API MutexImpl;

/*! \class Mutex esys/boost/mutex.h "esys/boost/mutex.h"
 *  \brief Boost Mutex class
 */
class ESYS_API Mutex: public esys::MutexBase
{
public:
    Mutex(const esys::ObjectName &name, Type type = DEFAULT);
    virtual ~Mutex();

    virtual int32_t Lock(bool from_isr=false) override;
    virtual int32_t UnLock(bool from_isr = false) override;
    virtual int32_t TryLock(bool from_isr = false) override;

protected:
    MutexImpl *m_impl;      //!< The PIMPL object
};

}

}

#endif


