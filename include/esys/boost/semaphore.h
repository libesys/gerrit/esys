/*!
 * \file esys/boost/semaphore.h
 * \brief Declaration of the Boost Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_SEMAPHORE_H__
#define __ESYS_BOOST_SEMAPHORE_H__

#include "esys/esys_defs.h"
#include "esys/semaphorebase.h"

namespace esys
{

namespace boost
{

class ESYS_API SemaphoreImpl;

/*! \class Semaphore esys/boost/semaphore.h "esys/boost/semaphore.h"
 *  \brief Boost Semaphore class
 */
class ESYS_API Semaphore: public SemaphoreBase
{
public:
    Semaphore(const ObjectName &name, uint32_t count);
    virtual ~Semaphore();

    virtual int32_t Post(bool from_isr = false) override;
    virtual int32_t Wait(bool from_isr = false) override;
    virtual int32_t TryWait(bool from_isr = false) override;
    virtual uint32_t Count() override;

    virtual int32_t WaitFor(uint32_t ms, bool from_isr = false);

protected:
    SemaphoreImpl *m_impl;
};

}

}

#endif


