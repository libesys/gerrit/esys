/*!
 * \file esys/boost/serialport.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/serialportbase.h"
#include "esys/ringbuffer.h"

#include <string>
#include <memory>

namespace esys
{

namespace boost
{
class ESYS_API SerialPortImpl;

/*! \class SerialPort esys/boost/serialport.h "esys/boost/serialport.h"
 *  \brief
 */
class ESYS_API SerialPort: public SerialPortBase
{
public:
    friend class ESYS_API SerialPortImpl;

    SerialPort();               //!< Default constructor
    virtual ~SerialPort();

    virtual int Open(const std::string& devname) override;
    virtual int Open(const std::wstring& devname) override;
    virtual bool IsOpen() const override;
    virtual int Close() override;

    virtual void UseCTSRTS(bool fc) override;
    virtual void SetBaudrate(uint32_t baud) override;

    virtual int Write(uint8_t *buf, uint16_t size) override;
    virtual int Write(const std::string &data) override;
    virtual int Read(std::string &data, int timeout_ms = -1) override;

    virtual void SetCallback(SerialPortCallbackBase *call_back) override;
protected:
    void read_callback(const uint8_t *buf, size_t size);

    std::unique_ptr<SerialPortImpl> m_impl;
    esys::RingBufferStatic<uint8_t, 1024> m_buffer;

};

}

#ifdef ESYS_SERIAL_PORT_BOOST
using namespace boost;
#endif

}



