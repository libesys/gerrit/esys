/*!
 * \file esys/boost/esys.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_ESYS_H__
#define __ESYS_BOOST_ESYS_H__

#include "esys/esys_defs.h"
#include "esys/mp/esysbase.h"

namespace esys
{

namespace boost
{

class ESYS_API ESys: public mp::ESysBase
{
public:
    ESys();
    virtual ~ESys();

    static ESys &Get();
    static void Select();

    virtual SemaphoreBase *NewSemaphoreBase(const ObjectName &name, uint32_t count) override;
    virtual MutexBase *NewMutexBase(const ObjectName &name, MutexBase::Type type = MutexBase::DEFAULT) override;
    virtual TaskPlatIf *NewTaskPlat(TaskBase *task_base) override;
    virtual TaskMngrPlatIf *NewTaskMngrPlat(TaskMngrBase *taskmngr_base) override;
    virtual SystemPlatIf *NewSystemPlat(SystemBase *system_base) override;
    virtual TimerBase *NewTimerBase(const ObjectName &name) override;

    virtual uint32_t implMillis(TimeBase::Source source = TimeBase::DEFAULT_CLK) override;
    virtual uint64_t implMicros(TimeBase::Source source = TimeBase::DEFAULT_CLK) override;
    virtual uint32_t implMicrosLow() override;

    virtual int32_t SetTime(TimeBase::TimeStruct &time) override;
    virtual int32_t GetTime(TimeBase::TimeStruct &time) override;
    virtual int32_t SetDate(TimeBase::DateStruct &date) override;
    virtual int32_t GetDate(TimeBase::DateStruct &date) override;
    virtual int32_t SetDateTime(TimeBase::DateTimeStruct &date_time) override;
    virtual int32_t GetDateTime(TimeBase::DateTimeStruct &date_time) override;

    virtual int32_t StartTimestamp() override;
    virtual int32_t StopTimestamp() override;

    virtual TaskPlatIf *GetCurTaskPlatIf() override;
protected:
    static ESys s_esys;
};

}

}

#endif

