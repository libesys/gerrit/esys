/*!
 * \file esys/boost/taskmngr.h
 * \brief Declaration of the Boost TaskMngr class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/taskmngrbase.h"
#include "esys/boost/taskmngrplat.h"
#include "esys/boost/timermngr.h"

namespace esys
{

namespace boost
{

/*! \class TaskMngr esys/boost/taskmngr.h "esys/boost/taskmngr.h"
 *  \brief Boost TaskMngr class
 */
class ESYS_API TaskMngr: public TaskMngrBase, public virtual TaskMngrPlat
{
public:
    TaskMngr(const ObjectName &name);
    virtual ~TaskMngr();

    virtual int32_t StaticInitBeforeChildren() override;

    virtual void AllAppTasksDone() override;
    //virtual void ExitScheduler() override;

    TimerMngr &GetTimerMngr();
protected:
    TimerMngr m_timer_mngr;
};

}

#ifdef ESYS_USING_PLAT_NS
using namespace boost;
#endif

}







