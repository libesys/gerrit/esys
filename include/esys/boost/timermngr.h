/*!
 * \file esys/boost/timermngr.h
 * \brief The Boost implementation of the Timer Manager
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/timermngrbase.h"

#include <vector>

namespace esys
{

namespace boost
{

class ESYS_API TaskMngr;
class ESYS_API TimerMngrImpl;

/*! \class TimerMngr esys/boost/timermngr.h "esys/boost/timermngr.h"
 *  \brief Boost Timer Manager implementation
 */
class ESYS_API TimerMngr: public TimerMngrBase
{
public:
    TimerMngr(const ObjectName &name, TaskMngr *mngr);
    virtual ~TimerMngr();

    //! Returns the number of Timers handled by the Manager
    /*! \return the number of Timers
    */
    virtual uint16_t GetCount() override;

    virtual int32_t StopAll() override;

    virtual void Start(TimerBase *timer) override;
    virtual void Stop(TimerBase *timer) override;
    virtual bool IsRunning(TimerBase *timer) override;

    static TimerMngr &GetCurrent();
protected:
    virtual void AddTimer(TimerBase *timer) override;

    TimerMngrImpl *m_impl;
    std::vector<TimerBase *> m_timers;
};

}

}

