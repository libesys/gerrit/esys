/*!
 * \file esys/boost/taskmngrplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_TASKMNGRPLATIF_H__
#define __ESYS_BOOST_TASKMNGRPLATIF_H__

#include "esys/esys_defs.h"
#include "esys/taskmngrplatif.h"

namespace esys
{

namespace boost
{

class ESYS_API TaskMngrPlatImpl;
class ESYS_API TaskMngr;

/*! \class TaskMngrPlat esys/boost/taskmngrplat.h "esys/boost/taskmngrplat.h"
 *  \brief Defines the platform dependent Boost TaskMngr API when there is a scheduler
 */
class ESYS_API TaskMngrPlat: public virtual TaskMngrPlatIf
{
public:
    TaskMngrPlat(TaskMngrBase *taskmngr_base);
    virtual ~TaskMngrPlat();

    virtual int32_t StartScheduler() override;
    virtual void Done(TaskBase *task) override;
    virtual void ExitScheduler() override;

    TaskMngrPlatImpl *GetImpl();
protected:
    int32_t RegisterPlat();

    TaskMngrPlatImpl *m_impl;
};

}

}

#endif




