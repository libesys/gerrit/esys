/*!
 * \file esys/boost/dynlibrary.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2019-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/dynlibrarybase.h"

#include <memory>

namespace esys
{

namespace boost
{

class ESYS_API DynLibraryImpl;

class ESYS_API DynLibrary: public DynLibraryBase
{
public:
    DynLibrary();
    virtual ~DynLibrary();

    virtual int Load(const std::string &filename) override;
    virtual int UnLoad() override;
    virtual bool IsLoaded() override;
    virtual bool HasSymbol(const std::string& name) override;
    virtual void *GetSymbol(const std::string& name) override;
protected:
    std::unique_ptr<DynLibraryImpl> m_impl;
};

}

#ifdef ESYS_DYNLIB_USE_BOOST
using namespace boost;
#endif

}
