/*!
 * \file esys/boost/timermngrimpl.h
 * \brief The Boost PIMPL implementation of the Timer Manager
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/timermngrbase.h"
#include "esys/boost/task.h"
#include "esys/boost/mutex.h"

#include <deque>

namespace esys
{

namespace boost
{

class ESYS_API TaskMngr;
class ESYS_API TimerMngr;

/*! \class TimerMngrImpl esys/boost/timermngrimpl.h "esys/boost/timermngrimpl.h"
 *  \brief Boost Timer Manager PIMPL implementation
 */
class ESYS_API TimerMngrImpl : public Task
{
public:
    friend class ESYS_API TimerMngr;

    TimerMngrImpl(const ObjectName &name, TaskMngr *mngr);
    virtual ~TimerMngrImpl();

    virtual int32_t Entry() override;

    virtual void Start(TimerBase *timer);
    virtual void Stop(TimerBase *timer);

    bool IsRunning(TimerBase *timer);

    virtual int32_t StopAll();

    void SetTimerMngr(TimerMngr *timer_mngr);

    void SortEvents();
protected:
    virtual void AddTimer(TimerBase *timer);

    TimerMngr *m_timer_mngr;
    struct Helper
    {
        uint32_t m_next_time;
        TimerBase *m_timer;
    };
    std::deque<Helper> m_events;
    Mutex m_mutex;

    friend bool s_SortEvents(const TimerMngrImpl::Helper &left, const TimerMngrImpl::Helper &right);
};

}

}





