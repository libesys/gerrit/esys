/*!
 * \file esys/boost/systemplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/systemplatif.h"
#include "esys/systembase.h"

namespace esys
{

namespace boost
{

class ESYS_API SystemPlatImpl;

/*! \class SystemPlat esys/boost/systemplat.h "esys/boost/systemplat.h"
 *  \brief Defines the Boost SystemPlat when there is a scheduler
 */
class ESYS_API SystemPlat: public virtual SystemPlatIf
{
public:
    SystemPlat(SystemBase *system_base);
    virtual ~SystemPlat();

    virtual int32_t PlatInit() override;
    virtual int32_t PlatRelease() override;
    SystemBase *GetSystemBase();
    void SetSystemBase(SystemBase *system_base);

    SystemPlatImpl *GetImpl();
protected:
    SystemPlatImpl *m_impl = nullptr;
    SystemBase *m_system_base = nullptr;
};

}

}







