/*!
 * \file esys/boost/uuid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_UUID_H__
#define __ESYS_BOOST_UUID_H__

#include <esys/esys_defs.h>
#include <esys/inttypes.h>

#include <string>

namespace esys
{

namespace boost
{

class ESYS_API Uuid
{
public:
    typedef uint32_t Kind;
    static const Kind HEX	=1;
    static const Kind STRING=2;

    Uuid();
    virtual ~Uuid();

    std::string &GetString(Kind kind = HEX, bool new_if_null = false);
 
    bool IsNull();
    void Null();
    void New();
    uint8_t *Get();
    uint8_t operator[](int idx);
    void Set(uint8_t *data);
    void Set(std::string val);

protected:
    uint8_t m_uuid[16];
    std::string m_str;
};

}

#ifdef ESYS_VHW
using namespace boost;
#endif

}

#endif

