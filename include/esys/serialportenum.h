/*!
 * \file esys/serialportenum.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef WIN32
#include "esys/win32/serialportenum.h"
#else
#include "esys/unix/serialportenum.h"
#endif
