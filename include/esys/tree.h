/*!
 * \file esys/tree.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TREE_H__
#define __ESYS_TREE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/treeitem.h"

namespace esys
{

template<typename T>
class Tree
{
public:
    Tree();
    ~Tree();

    void SetRoot(T *first);
    T *GetRoot();

protected:
    T *m_root;
};

template<typename T>
Tree<T>::Tree(): m_root(nullptr)
{
}

template<typename T>
Tree<T>::~Tree()
{
}

template<typename T>
void Tree<T>::SetRoot(T *root)
{
    m_root = root;
}

template<typename T>
T *Tree<T>::GetRoot()
{
    return m_root;
}

}

#endif


