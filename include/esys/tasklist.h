/*!
 * \file esys/tasklist.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASKLIST_H__
#define __ESYS_TASKLIST_H__

#include "esys/esys_defs.h"

#ifndef ESYS_MULTI_PLAT

#ifdef ESYS_USE_SYSC
//#include "esys/sysc/mutex.h"
#elif defined ESYS_USE_WX
//#include "esys/wx/mutex.h"
#elif defined ESYS_USE_BOOST
//#include "esys/boost/mutex.h"
#else
#include "esys/em/tasklist.h"
#endif

#else

#include "esys/mp/tasklist.h"

#endif

#endif


