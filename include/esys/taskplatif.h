/*!
 * \file esys/taskplatif.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_TASKPLATIF_H__
#define __ESYS_TASKPLATIF_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/types.h"

namespace esys
{

class ESYS_API TaskBase;
class ESYS_API TaskMngrBase;

/*! \class TaskPlatIf esys/taskplatif.h "esys/taskplatif.h"
 *  \brief Defines the platform dependent Task API when there is a scheduler
 */
class ESYS_API TaskPlatIf
{
public:
    virtual ~TaskPlatIf();
    // Platform functions for both Task
    virtual int32_t Create() = 0;
    virtual int32_t Start() = 0;
    virtual int32_t Stop() = 0;
    virtual int32_t Kill() = 0;
    virtual int32_t Destroy() = 0;

    virtual millis_t Millis() = 0;          //!< Returns the number of milli second since the start
    virtual void Sleep(millis_t ms) = 0;    //!< Sleeps for a given amount of milli-seconds
    virtual void SleepU(micros_t us) = 0;   //!< Sleeps for a given amount of micro-seconds

    virtual void SetTaskBase(TaskBase *task_base) = 0;
    //virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base)=0;
    //virtual void NotifyExit()=0;
};

}

#endif

