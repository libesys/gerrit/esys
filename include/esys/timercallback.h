/*!
 * \file esys/timercallback.h
 * \brief The class of Timer Callback
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/timercallbackbase.h"

namespace esys
{

class ESYS_API TimerBase;

/*! \class TimerCallback esys/timercallback.h "esys/timercallback.h"
 *  \brief The class for Timer Callback
 */
template<typename T>
class TimerCallback: public TimerCallbackBase
{
public:
    typedef void (T::*CallbackType)(TimerBase *timer);

    TimerCallback(T *callee, CallbackType callback);
    virtual ~TimerCallback();

    virtual void Callback() override;
protected:
//!< \cond DOXY_IMPL
    T *m_callee;
    CallbackType m_callback;
//!< \endcond
};

template<typename T>
TimerCallback<T>::TimerCallback(T *callee, CallbackType callback): m_callee(callee), m_callback(callback)
{
}

template<typename T>
TimerCallback<T>::~TimerCallback()
{
}

template<typename T>
void TimerCallback<T>::Callback()
{
    (m_callee->*m_callback)(m_timer);
}

}






