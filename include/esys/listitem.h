/*!
 * \file esys/listitem.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_LISTITEM_H__
#define __ESYS_LISTITEM_H__

#include "esys/esys_defs.h"
#include "esys/listitemdata.h"

namespace esys
{

/*! \class ListItem esys/listitem.h "esys/listitem.h"
 *  \brief The base class of all mutex implementation
 *  \tparam T the type of item in the list
 *
 */
template<typename T>
class ListItem
{
public:
    //! Constructor
    ListItem();

    //! Destructor
    ~ListItem();

    //! Set the next item following this item
    /*!
     *  \param[in] next the item to be set after the current one
     */
    void SetNext(T *next);

    //! Get the next item after this item
    /*!
     *  \return the item following this item
     */
    T *GetNext();

    //! Set the item preceding this item
    /*!
     *  \param[in] prev the previous item
     */
    void SetPrev(T *prev);

    //! Get the item preceding this item
    /*!
     *  \return the item preceding this item
     */
    T *GetPrev();

//!< \cond DOXY_IMPL
    ListItemData<T> &GetData();
protected:
    ListItemData<T> m_data;
//!< \endcond
};

template<typename T>
ListItem<T>::ListItem() : m_data()
{
}

template<typename T>
ListItem<T>::~ListItem()
{
}

template<typename T>
void ListItem<T>::SetNext(T *next)
{
    m_data.SetNext(next);
}

template<typename T>
T *ListItem<T>::GetNext()
{
    return m_data.GetNext();
}

template<typename T>
void ListItem<T>::SetPrev(T *prev)
{
    m_data.SetPrev(prev);
}

template<typename T>
T *ListItem<T>::GetPrev()
{
    return m_data.GetPrev();
}

template<typename T>
ListItemData<T> &ListItem<T>::GetData()
{
    return m_data;
}

}

#endif

