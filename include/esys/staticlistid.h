/*!
 * \file esys/staticlistid.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_STATICLISTID_H__
#define __ESYS_STATICLISTID_H__

#include "esys/esys_defs.h"
#include "esys/staticlist.h"

#include <stddef.h>     //Needed for NULL under GCC

#ifdef MULTIOS
#include <map>
#endif

namespace esys
{

template<typename T, typename ID, int N=0>
class StaticListId: public StaticList<T, N>
{
public:
    typedef StaticListId<T, ID, N> ItemId;

    StaticListId(const ID &id, T *element=NULL);
    ~StaticListId();

    static T *GetById(ID id);
    const ID &GetId();
protected:
#ifdef MULTIOS
    static void Populate();
#endif

    ID m_id;
#ifdef MULTIOS
    static std::map<ID, T *> m_map;
#endif
};

template<typename T, typename ID, int N>
StaticListId<T,ID,N>::StaticListId(const ID &id, T *element): StaticList<T, N>(element), m_id(id)
{
}

template<typename T, typename ID, int N>
StaticListId<T,ID,N>::~StaticListId()
{
}

template<typename T, typename ID, int N>
T *StaticListId<T,ID,N>::GetById(ID id)
{
#ifdef MULTIOS
    if (m_map.size()!=StaticList<T, N>::m_count)
        Populate();

    typename std::map<ID, T *>::iterator it;

    it=m_map.find(id);
    if (it==m_map.end())
        return NULL;
    return it->second;
#else
    T *p= StaticList<T, N>::GetLast();

    while (p!=NULL)
    {
        if (p->GetId()==id)
            return p;
        p=p->m_prev;
    }
    return nullptr;
#endif
}

template<typename T, typename ID, int N>
const ID &StaticListId<T,ID,N>::GetId()
{
    return m_id;
}

#ifdef MULTIOS
template<typename T, typename ID, int N>
void StaticListId<T,ID,N>::Populate()
{
    ItemId *p=static_cast<ItemId *>(StaticList<T, N>::m_last);
    typename std::map<ID, T *>::iterator it;

    m_map.clear();

    while (p!=NULL)
    {
        it=m_map.find(p->GetId());
        if (it!=m_map.end())
        {
            //Should have way to report the error
        }
        else
        {
            m_map[p->GetId()]=p->GetElement();
        }
        p=static_cast<ItemId *>(p->m_prev);
    }
}
#endif

#ifdef MULTIOS
template<typename T, typename ID, int N> std::map<ID, T *> StaticListId<T,ID,N>::m_map;
#endif

}

#endif
