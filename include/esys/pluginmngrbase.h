/*!
 * \file esys/pluginmngrbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/pluginbase.h"

#include <vector>
#include <string>
#include <map>
#include <memory>

namespace esys
{

class ESYS_API PluginMngrBase
{
public:
    PluginMngrBase();
    virtual ~PluginMngrBase();

    virtual void SetDir(const std::string &dir); //!< Set the directory where to find plugins
    const std::string &GetDir();
    virtual int Load() = 0;                                         //!< Load the plugins
    virtual int Load(const std::string &dir) = 0;                   //!< Attempt to load a plugin
    virtual int Release() = 0;                                      //!< Release the plugins
    virtual std::size_t GetNbr() = 0;                               //!< Return the number of loaded plugins
    virtual PluginBase *GetBase(std::size_t index) = 0;             //!< Return the plugin with a given index
    virtual PluginBase *GetBase(const std::string &short_name) = 0; //!< Return the plugin with a given short name

    void SetVerboseLevel(uint32_t verbose_level);
    uint32_t GetVerboseLevel();

    const std::string &GetEntryFctName();
    virtual PluginBase *GetPluginFromEntryFct(void *entry_fct) = 0;

    static void SetBaseFolder(const std::string &base_folder);
    static const std::string &GetBaseFolder();
    static void SetAppExe(const std::string &app_exe);
    static const std::string &GetAppExe();

protected:
    static std::string m_base_folder;
    static std::string m_app_exe;

    void SetEntryFctName(const std::string &entry_fct_name);
    static void SetPluginFileName(PluginBase *plugin, const std::string &filename);

    uint32_t m_verbose_level = 0;
    std::string m_entry_fct_name;
    std::string m_dir;
};

} // namespace esys
