/*!
 * \file esys/bufferbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BUFFERBASE_H__
#define __ESYS_BUFFERBASE_H__

#include "esys/esys_defs.h"
#include "esys/assert.h"
#include "esys/inttypes.h"

namespace esys
{

class ESYS_API BufferBase
{
public:
    BufferBase(uint16_t max_size=0, uint16_t size=0);
    virtual ~BufferBase();

    void SetSize(uint16_t size);
    uint16_t GetSize();
    void SetMaxSize(uint16_t size);
    uint16_t GetMaxSize();
    virtual void Clear();

protected:
    uint16_t m_size;
    uint16_t m_max_size;
    bool m_valid;
};

}

#endif
