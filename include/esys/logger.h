/*!
 * \file esys/logger.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/logchannel.h"
#include "esys/mutex.h"
#include "esys/objectnode.h"
#include "esys/assert.h"

#include <cfloat>

namespace esys
{

class ESYS_API TaskMngrBase;
class ESYS_API SystemBase;

/*! \class Logger esys/logger.h "esys/logger.h"
 *  \brief Base class of the generic Logger
 */
class ESYS_API Logger: public ObjectNode
{
public:
//!< \cond DOXY_IMPL
    friend class ESYS_API TaskMngrBase;
    friend class ESYS_API SystemBase;

    friend ESYS_API Logger &begin(Logger &log);
    friend ESYS_API Logger &end(Logger &log);
    friend ESYS_API Logger& operator<< (Logger& log, float value);
    friend ESYS_API void set_precision(Logger &log, esys::uint32_t precision);
    friend ESYS_API Logger &fixed(Logger &log);
    friend ESYS_API Logger &scientific(Logger &log);
    friend ESYS_API void set_channel(Logger &log, esys::int32_t channel);
//!< \endcond

    static const int8_t ALL_CH = -1;

    enum Level : uint8_t
    {
        LOG_DEBUG = 0,
        LOG_INFO,
        LOG_WARN,
        LOG_CRITICAL,
        LOG_FATAL
    };

    Logger(const ObjectName &name = "");
    virtual ~Logger();

    //! Set hexadecimal output of integer values
    /*! \param hex true to set the hexadecimal output, false for decimal output
    */
    void SetHex(bool hex);

    //! Tells if the format used to log integers is Hexadecimal
    /*! \return true if the output format is hexadecimal, false for decimal output
    */
    bool GetHex();

    //! Logs functions without parameters, i.e. end or endl
    /*!
     * \param log the Logger to use
     * \return the Logger used
     */
    Logger& operator<< (Logger & (*op)(Logger& log));

    //! Flush all the logging data from all Channels
    /*!
     */
    virtual void Flush() = 0;
    virtual void PutStr(const char *buf, int8_t ch = -1) = 0;
    virtual void PutStrN(char *buf, uint16_t size, int8_t ch = -1) = 0;
    virtual void Put(uint8_t *buf, uint16_t size, int8_t ch = -1) = 0;

    //! Adds a Logging Channel to the Logger
    /*!
     * \param channel the Logging Channel to add
     */
    virtual void AddChannel(LogChannel *channel) = 0;

    //! Returns the number of Channels handled by the Logger
    /*! \return the number of Channels
     */
    uint8_t GetCount();

    int8_t GetChannel();

    virtual LogChannel &GetChannel(uint8_t idx) = 0;
    void SetActive(bool active = true, int8_t idx = -1);
    void SetEnable(bool enable = true, int8_t idx = -1);

    virtual int32_t StaticInitBeforeChildren() override;
    virtual int32_t StaticReleaseAfterChildren() override;

    virtual bool DoLog();
    void SetCurLevel(Level level);
    Level GetCurLevel();
    void SetThresholdLevel(Level level);
    Level GetThresholdLevel();

    void SaveLevel();
    void LoadLevel();

    //! Returns the default Logger
    /*! \return the default Logger
    */
    static Logger &Get();
    static void Set(Logger *logger);
protected:
    void SetChannel(int8_t ch);
    void TransLock();
    void TransUnlock();
    void PushTransactionChannel(int8_t ch);
    void PopTransactionChannel();
//!< \cond DOXY_IMPL
    static Logger *g_logger;
    Mutex m_mutex{ "LoggerMutex" };
    Mutex m_trans_mutex{ "TranMutex" };
    bool m_hex{ false };
    uint8_t m_count{ 0 };
    int8_t m_ch{ 0 };
    int8_t m_saved_ch{ 0 };
    bool m_init{ false };
    char m_float_format[10];
    esys::uint8_t m_float_precision{ 3 };
    bool m_float_scientific{ false };
    Level m_threshold_level{ LOG_INFO };
    Level m_cur_level{ LOG_INFO };
    Level m_saved_level{ LOG_INFO };
//!< \endcond
};

template <uint8_t N>
class Logger_t : public Logger
{
public:
    Logger_t();
    virtual ~Logger_t();

    virtual void Flush() override;
    virtual void AddChannel(LogChannel *channel) override;
    virtual LogChannel &GetChannel(uint8_t idx) override;
    virtual void PutStr(const char *buf, int8_t ch = -1) override;
    virtual void PutStrN(char *buf, uint16_t size, int8_t ch = -1) override;
    virtual void Put(uint8_t *buf, uint16_t size, int8_t ch = -1) override;
protected:
//!< \cond DOXY_IMPL
    LogChannel *m_channels[N];
//!< \endcond
};

template <uint8_t N>
Logger_t<N>::Logger_t() : Logger()
{
    uint8_t idx;

    for (idx = 0; idx < N; ++idx)
        m_channels[idx] = nullptr;
}

template <uint8_t N>
Logger_t<N>::~Logger_t()
{
}

template <uint8_t N>
void Logger_t<N>::Flush()
{
    uint8_t idx;

    if (this->m_init == false)
        return;

    m_mutex.Lock();

    for (idx = 0; idx < N; ++idx)
    {
        if ((m_channels[idx] != nullptr) && m_channels[idx]->active())
            m_channels[idx]->flush();
    }

    m_mutex.UnLock();
}

template <uint8_t N>
void Logger_t<N>::AddChannel(LogChannel *channel)
{
    assert(m_count < N);

    m_channels[m_count++] = channel;
}

template <uint8_t N>
LogChannel &Logger_t<N>::GetChannel(uint8_t idx)
{
    assert(idx < m_count);
    assert(m_channels[idx] != nullptr);

    return *m_channels[idx];
}

template <uint8_t N>
void Logger_t<N>::PutStr(const char *buf, int8_t ch)
{
    uint8_t idx;

    assert(ch < GetCount());

    if (this->m_init == false)
        return;

    if (DoLog() == false)
        return;

    m_mutex.Lock();

    if (ch != -1)
    {
        assert(ch >= 0);
        assert(ch < N);
        assert(m_channels[ch] != nullptr);

        m_channels[ch]->PutStr(buf);
    }
    else
    {
        for (idx = 0; idx < N; ++idx)
        {
            if ((m_channels[idx] != nullptr) && m_channels[idx]->GetActive() && m_channels[idx]->GetEnable())
                m_channels[idx]->PutStr(buf);
        }
    }
    m_mutex.UnLock();

}

template <uint8_t N>
void Logger_t<N>::PutStrN(char *buf, uint16_t size, int8_t ch)
{
    uint8_t idx;

    if (this->m_init == false)
        return;

    if (DoLog() == false)
        return;

    m_mutex.Lock();

    if (ch != -1)
    {
        assert(ch >= 0);
        assert(ch < N);

        m_channels[ch]->PutStr(buf, size);
    }
    else
    {
        for (idx = 0; idx < N; ++idx)
        {
            if ((m_channels[idx] != nullptr) && m_channels[idx]->GetActive() && m_channels[idx]->GetEnable())
                m_channels[idx]->PutStr(buf, size);
        }
    }
    m_mutex.UnLock();
}

template <uint8_t N>
void Logger_t<N>::Put(uint8_t *buf, uint16_t size, int8_t ch)
{
    uint8_t idx;

    if (this->m_init == false)
        return;

    if (DoLog() == false)
        return;

    m_mutex.Lock();

    if (ch != -1)
    {
        assert(ch >= 0);
        assert(ch < N);

        m_channels[ch]->Put(buf, size);
    }
    else
    {
        for (idx = 0; idx < N; ++idx)
        {
            if ((m_channels[idx] != nullptr) && m_channels[idx]->GetActive() && m_channels[idx]->GetEnable())
                m_channels[idx]->Put(buf, size);
        }
    }
    m_mutex.UnLock();
}

class ESYS_API Depth
{
public:
    Depth(int16_t depth);
    ~Depth();

    void Print(Logger &log) const;
protected:
//!< \cond DOXY_IMPL
    int16_t m_depth;
//!< \endcond
};

class ESYS_API Str
{
public:
    Str(char *buf, uint16_t size);
    ~Str();

    void Print(Logger &log) const;
protected:
    //!< \cond DOXY_IMPL
    char *m_buf;
    uint16_t m_size;
    //!< \endcond
};

/*! \brief Mark the beginning of a log transaction
 * A log transaction is atomic, meaning that it will be logged in one block, even if multiple threads use the same
 * logger.
 */
ESYS_API Logger &begin(Logger &log);

/*! \brief Mark the end of a log transaction
 * A log transaction is atomic, meaning that it will be logged in one block, even if multiple threads use the same
 * logger.
 */
ESYS_API Logger &end(Logger &log);

ESYS_API Logger &endl(Logger &log);

ESYS_API Logger &hex(Logger &log);
ESYS_API Logger &dec(Logger &log);

ESYS_API Logger& operator<< (Logger& log, uint8_t value);
ESYS_API Logger& operator<< (Logger& log, uint16_t value);
ESYS_API Logger& operator<< (Logger& log, uint32_t value);
//ESYS_API Logger& operator<< (Logger& log, int8_t value);   //conflict with char
ESYS_API Logger& operator<< (Logger& log, int16_t value);
ESYS_API Logger& operator<< (Logger& log, int32_t value);

ESYS_API Logger& operator<< (Logger& log, char letter);
ESYS_API Logger& operator<< (Logger& log, const char *str);

ESYS_API Logger& operator<< (Logger& log, const Depth &depth);
ESYS_API Logger& operator<< (Logger& log, const Str &str);

ESYS_API Logger& operator<< (Logger& log, float value);

ESYS_API Logger &fixed(Logger &log);
ESYS_API Logger &scientific(Logger &log);

template<typename T>
class Manip
{
public:
    typedef void(*Func)(Logger&, T);

    Manip(Func func, T arg) : m_func(func), m_arg(arg)
    {
    }

    Func m_func;
    T m_arg;
};

template<typename T>
Logger& operator<< (Logger& log, const Manip<T>& manip)
{
    if (log.DoLog() == false)
        return log;
    (*manip.m_func)(log, manip.m_arg);
    return log;
}

ESYS_API Manip<esys::uint32_t> setprecision(esys::uint32_t precision);
ESYS_API Manip<esys::int32_t> set_channel(esys::int32_t channel);


ESYS_API Logger &debug(Logger &log);
ESYS_API Logger &info(Logger &log);
ESYS_API Logger &warn(Logger &log);
ESYS_API Logger &critical(Logger &log);
ESYS_API Logger &fatal(Logger &log);

ESYS_API Logger &save_level(Logger &log);
ESYS_API Logger &load_level(Logger &log);

}




