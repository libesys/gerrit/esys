/*!
 * \if DOXY_IMPL
 * \file esys/mp/modulelist.h
 * \brief
 * \else
 * \file esys/modulelist.h
 * \brief
 * \endif

 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_MODULELIST_H__
#define __ESYS_MP_MODULELIST_H__

#include "esys/esys_defs.h"
#include "esys/modulelistbase.h"

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class ModuleList esys/mp/modulelist.h "esys/mp/modulelist.h"
 * \brief Multi Platform Module list
 * \else
 * \class ModuleList esys/modulelist.h "esys/modulelist.h"
 * \brief The Module list
 * \endif
 *
 */
class ESYS_API ModuleList : public ModuleListBase
{
public:
    //! Constructor
    ModuleList();

    //! Destructor
    ~ModuleList();

    static ModuleList &GetCurrent();
protected:
//!< \cond DOXY_IMPL
    static ModuleList *m_dft_list_mod;
//!< \endcond
};

#ifndef DOXY_PUB

}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif

#endif

}

#endif



