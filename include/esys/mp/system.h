/*!
 * \if DOXY_IMPL
 * \file esys/mp/system.h
 * \brief
 * \else
 * \file esys/system.h
 * \brief
 * \endif
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_SYSTEM_H__
#define __ESYS_MP_SYSTEM_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/systembase.h"
#include "esys/systemplatif.h"

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class System esys/mp/system.h "esys/mp/system.h"
 * \brief Defines the Multi Platform System when there is a scheduler
 * \else
 * \class System esys/system.h "esys/system.h"
 * \brief Defines the System when there is a scheduler
 * \endif
 *
 */
class ESYS_API System: public SystemBase
{
public:
    //! Constructor
    /*!
     *  \param[in] name The name of the Object
     */
    System(const ObjectName &name);

    //! Destructor
    virtual ~System();

    virtual int32_t PlatInit() override;
    virtual int32_t PlatRelease() override;

    //! Set the system platform interface object
    /*!
     *  \param[in] plat_if the system platform interface object
     */
    void SetPlatIf(SystemPlatIf *plat_if);

    //! Get the system platform interface object
    /*!
     *  \return the system platform interface object
     */
    SystemPlatIf *GetPlatIf();
protected:
//!< \cond DOXY_IMPL
    SystemPlatIf *m_plat_if = nullptr;
//!< \endcond
};

#ifndef DOXY_PUB

}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif

#endif

}

#endif




