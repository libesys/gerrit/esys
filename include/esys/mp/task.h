/*!
 * \if DOXY_IMPL
 * \file esys/mp/task.h
 * \brief Declaration of the Multi Platform Task class
 * \else
 * \file esys/task.h
 * \brief Declaration of the Task class
 * \endif
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_TASK_H__
#define __ESYS_MP_TASK_H__

#include "esys/esys_defs.h"
#include "esys/taskbase.h"

#include <map>

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class Task esys/mp/task.h "esys/mp/task.h"
 * \brief Multi Platform Task class
 * \else
 * \class Task esys/task.h "esys/task.h"
 * \brief The Task class
 * \endif
 *
 */
class ESYS_API Task: public TaskBase
{
public:
    //! Constructor
    /*!
     *  \param[in] name The name of the Object
     *  \param[in] type the type of the Task
     */
    Task(const ObjectName &name, TaskType type = TaskType::APPLICATION);

    //! Destructor
    virtual ~Task();

    virtual int32_t Create() override;
    virtual int32_t Start() override;
    virtual int32_t Stop() override;
    virtual int32_t Kill() override;
    virtual int32_t Destroy() override;

    virtual millis_t Millis() override;
    virtual void Sleep(millis_t ms) override;
    virtual void SleepU(micros_t us) override;
    virtual void SetTaskBase(TaskBase *task_base) override;

    static Task &GetCurrent();
protected:
    typedef std::map<TaskPlatIf *, Task *> TaskMap;
    static TaskMap s_task_map;

    TaskPlatIf *m_task_plat_if;

};

#ifndef DOXY_PUB

}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif

#endif

}

#endif




