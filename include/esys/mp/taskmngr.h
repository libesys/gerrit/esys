/*!
 * \if DOXY_IMPL
 * \file esys/mp/taskmngr.h
 * \brief Declaration of the Multi Platform TaskMngr class
 * \else
 * \file esys/taskmngr.h
 * \brief Declaration of the TaskMngr class
 * \brief
 * \endif
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_TASKMNGR_H__
#define __ESYS_MP_TASKMNGR_H__

#include "esys/esys_defs.h"
#include "esys/taskmngrbase.h"

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class TaskMngr esys/mp/taskmngr.h "esys/mp/taskmngr.h"
 * \brief Multi Platform TaskMngr class
 * \else
 * \class TaskMngr esys/taskmngr.h "esys/taskmngr.h"
 * \brief TaskMngr class
 * \endif
 *
 */
class ESYS_API TaskMngr: public TaskMngrBase
{
public:
    //! Constructor
    /*!
     *  \param[in] name The name of the TaskMgr
     */
    TaskMngr(const ObjectName &name);

    //! Destructor
    virtual ~TaskMngr();

    virtual int32_t StartScheduler() override;
    virtual void Done(TaskBase *task_base) override;
    virtual void ExitScheduler() override;
    //virtual void AllAppTasksDone() override;

    static TaskMngr *GetCurrent();

protected:
//!< \cond DOXY_IMPL
    TaskMngrPlatIf *m_taskmngr_plat_if;
//!< \endcond
};

#ifndef DOXY_PUB
}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif

#endif

}

#endif





