/*!
 * \file esys/mp/plugin_t.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/mp/plugin.h"

namespace esys
{

namespace mp
{

/*! \class Plugin_t esys/mp/plugin_t.h "esys/mp/plugin_t.h"
 * \brief Plugin class
 *
 */
template<typename T>
class Plugin_t : public Plugin
{
public:
    Plugin_t();
    virtual ~Plugin_t();

protected:

};

template<typename T>
Plugin_t<T>::Plugin_t(): Plugin()
{
    bool debug;

#ifdef _MSC_VER
#ifdef _DEBUG
    debug = true;
#elif NDEBUG
    debug = false;
#else
#error _DEBUG or NDEBUG must be defined
#endif
#else
    debug = false;
#endif
    this->SetDebug(debug);
}

template<typename T>
Plugin_t<T>::~Plugin_t()
{
}

}

}





