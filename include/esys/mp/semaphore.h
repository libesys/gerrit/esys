/*!
 * \if DOXY_IMPL
 * \file esys/mp/semaphore.h
 * \brief Declaration of the Multi Platform Semaphore class
 * \else
 * \file esys/semaphore.h
 * \brief Declaration of the Semaphore class
 * \endif
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/semaphorebase.h"
#include "esys/mp/objectinitrelay.h"

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class Semaphore esys/mp/semaphore.h "esys/mp/semaphore.h"
 * \brief Multi Platform Semaphore class
 * \else
 * \class Semaphore esys/semaphore.h "esys/semaphore.h"
 * \brief The Semaphore class
 * \endif
 *
 */
class ESYS_API Semaphore :
#ifndef DOXY_PUB
    public ObjectInitRelay<SemaphoreBase>
#else
    public SemaphoreBase
#endif
{
public:
    //! Constructor
    /*!
     *  \param[in] name The name of the Semaphore
     *  \param[in] count the initial count of the Semaphore
     */
    Semaphore(const ObjectName &name, uint32_t count);

    //! Destructor
    virtual ~Semaphore();

    virtual int32_t Post(bool from_isr = false) override;
    virtual int32_t Wait(bool from_isr = false) override;
    virtual int32_t WaitFor(uint32_t ms, bool from_isr = false) override;
    virtual int32_t TryWait(bool from_isr = false) override;
    virtual uint32_t Count() override;
protected:
};

#ifndef DOXY_PUB
}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif

#endif

}





