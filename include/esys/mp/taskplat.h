/*!
 * \file esys/mp/taskplat.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_TASKPLAT_H__
#define __ESYS_MP_TASKPLAT_H__

#include "esys/esys_defs.h"
#include "esys/taskplatif.h"
#include "esys/types.h"

namespace esys
{

namespace mp
{

/*! \class TaskPlatIf esys/mp/taskplat.h "esys/mp/taskplat.h"
 *  \brief Defines the platform dependent Task API
 */
class ESYS_API TaskPlat: public virtual TaskPlatIf
{
public:
    TaskPlat(TaskBase *task_base);
    virtual ~TaskPlat();

    virtual int32_t Create() override;
    virtual int32_t Start() override;
    virtual int32_t Stop() override;
    virtual int32_t Kill() override;
    virtual int32_t Destroy() override;

    virtual millis_t Millis();
    virtual void Sleep(millis_t ms);
    virtual void SleepU(micros_t us);

    virtual void SetTaskBase(TaskBase *task_base);
};

}

}

#endif


