/*!
 * \file esys/mp/mutex.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_MUTEX_H__
#define __ESYS_MP_MUTEX_H__

#include "esys/esys_defs.h"
#include "esys/mutexbase.h"
#include "esys/mp/objectinitrelay.h"

namespace esys
{

namespace mp
{

class ESYS_API Mutex: public ObjectInitRelay<MutexBase>
{
public:
    Mutex(const ObjectName &name, Type type=DEFAULT);
    virtual ~Mutex();

    virtual int32_t Lock(bool from_isr = false) override;
    virtual int32_t UnLock(bool from_isr = false) override;
    virtual int32_t TryLock(bool from_isr = false) override;

    /*virtual int32_t StaticInit(Order order) override;
    virtual int32_t StaticRelease(Order order) override;
    virtual int32_t RuntimeInit(RuntimeLevel runtime_lvl, Order order) override;
    virtual int32_t RuntimeRelease(RuntimeLevel runtime_lvl, Order order) override; */
protected:
    //esys::MutexBase *m_mutex;
};

}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif


}

#endif


