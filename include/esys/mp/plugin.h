/*!
 * \file esys/mp/plugin.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/pluginbase.h"

#define DECLARE_ESYS_PLUGIN(exp) \
    extern "C" exp esys::mp::Plugin *get_esys_plugin();

#define DEFINE_ESYS_PLUGIN(exp, class_) \
    static class_ g_plugin; \
    extern "C" exp esys::mp::Plugin *get_esys_plugin() \
    { \
        return &g_plugin;\
    }

namespace esys
{

namespace mp
{

/*! \class Plugin esys/mp/plugin.h "esys/mp/plugin.h"
 * \brief Plugin class
 *
 */
class ESYS_API Plugin : public PluginBase
{
public:
    //! Constructor
    Plugin();

    //! Destructor
    virtual ~Plugin();


protected:

};

typedef Plugin *(*PluginEntryFunction)();

}

}

