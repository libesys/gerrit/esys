/*!
 * \file esys/mp/esysbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/semaphorebase.h"
#include "esys/mutexbase.h"
#include "esys/timerbase.h"
#include "esys/timebase.h"
#include "esys/taskplatif.h"
#include "esys/taskmngrplatif.h"
#include "esys/systemplatif.h"
#include "esys/systembase.h"
#include "esys/platform/id.h"
#include "esys/staticlistid.h"
#include "esys/esysbase.h"

namespace esys
{

namespace mp
{

class ESYS_API ESysBase: public StaticListId<ESysBase, int32_t>, public esys::ESysBase
{
public:
    typedef StaticListId<ESysBase, int32_t> BaseType;

    virtual SemaphoreBase *NewSemaphoreBase(const ObjectName &name, uint32_t count) = 0;
    virtual MutexBase *NewMutexBase(const ObjectName &name, MutexBase::Type type = MutexBase::DEFAULT) = 0;
    virtual TaskPlatIf *NewTaskPlat(TaskBase *task_base) = 0;
    virtual TaskMngrPlatIf *NewTaskMngrPlat(TaskMngrBase *taskmngr_base) = 0;
    virtual SystemPlatIf *NewSystemPlat(SystemBase *system_base) = 0;
    virtual TimerBase *NewTimerBase(const ObjectName &name) = 0;

    virtual uint32_t implMillis(TimeBase::Source source = TimeBase::DEFAULT_CLK);
    virtual uint64_t implMicros(TimeBase::Source source = TimeBase::DEFAULT_CLK);
    virtual uint32_t implMicrosLow();

    virtual int32_t SetTime(TimeBase::TimeStruct &time);
    virtual int32_t GetTime(TimeBase::TimeStruct &time);
    virtual int32_t SetDate(TimeBase::DateStruct &date);
    virtual int32_t GetDate(TimeBase::DateStruct &date);
    virtual int32_t SetDateTime(TimeBase::DateTimeStruct &date_time);
    virtual int32_t GetDateTime(TimeBase::DateTimeStruct &date_time);

    virtual int32_t StartTimestamp();
    virtual int32_t StopTimestamp();

    virtual TaskPlatIf *GetCurTaskPlatIf() = 0;

    platform::Id *GetPlatformId();

    const std::string &GetName();
protected:
    ESysBase(int32_t platform_id, const std::string &name = "");
    virtual ~ESysBase();

    void SetName(const std::string &name);
private:
    platform::Id *m_platform;
    std::string m_name;
};

}

}
