/*!
 * \if DOXY_IMPL
 * \file esys/mp/taskmngrplat.h
 * \brief
 * \else
 * \file esys/taskmngrplat.h
 * \brief
 * \endif
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_TASKMNGRPLATIF_H__
#define __ESYS_MP_TASKMNGRPLATIF_H__

#include "esys/esys_defs.h"
#include "esys/taskmngrplatif.h"

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class TaskMngrPlat esys/mp/taskmngrplat.h "esys/mp/taskmngrplat.h"
 * \brief Defines the platform dependent Multi Platform TaskMngr API when there is a scheduler
 * \else
 * \class TaskMngrPlat esys/taskmngrplat.h "esys/taskmngrplat.h"
 * \brief Defines the platform dependent TaskMngr API when there is a scheduler
 * \endif
 *
 */
class ESYS_API TaskMngrPlat: public virtual TaskMngrPlatIf
{
public:
    //!< Destructor
    virtual ~TaskMngrPlat();

    //! Start the RTOS scheduler
    /*!
     *  \return <0 if an error occured, 0 otherwise
     */
    virtual int32_t StartScheduler();
};

#ifndef DOXY_PUB
}
#endif

}

#endif



