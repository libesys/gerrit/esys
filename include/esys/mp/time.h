/*!
 * \if DOXY_IMPL
 * \file esys/mp/time.h
 * \brief
 * \else
 * \file esys/time.h
 * \brief
 * \endif
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/timebase.h"

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class Time esys/mp/time.h "esys/mp/time.h"
 * \brief
 * \else
 * \class Time esys/time.h "esys/time.h"
 * \brief
 * \endif
 */

class ESYS_API Time: public TimeBase
{
public:
    Time();

    static uint32_t Millis(Source source = DEFAULT_CLK);
    static uint64_t Micros(Source source = DEFAULT_CLK);
    static uint32_t MicrosLow();
    static void Sleep(uint32_t ms);
    static void USleep(uint32_t us);

    static int32_t SetTime(TimeStruct &time);
    static int32_t GetTime(TimeStruct &time);
    static int32_t SetDate(DateStruct &date);
    static int32_t GetDate(DateStruct &date);
    static int32_t SetDateTime(DateTimeStruct &date_time);
    static int32_t GetDateTime(DateTimeStruct &date_time);

    static int32_t StartTimestamp();
    static int32_t StopTimestamp();
};

#ifndef DOXY_PUB
}
#endif

#ifdef ESYS_USE_MP
using namespace mp;
#endif

}



