/*!
 * \file esys/mp/objectinitrelay.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_OBJECTINITRELAY_H__
#define __ESYS_MP_OBJECTINITRELAY_H__

#include "esys/esys_defs.h"
#include "esys/assert.h"
#include "esys/object.h"
#include "esys/objectname.h"

namespace esys
{

namespace mp
{

template<typename T>
class ObjectInitRelay: public T
{
public:
    ObjectInitRelay(const ObjectName &name);
    virtual ~ObjectInitRelay();

    void SetObjRelay(T *obj_relay);
    T *GetObjRelay();

    virtual int32_t StaticInit(Object::Order order) override;
    virtual int32_t StaticRelease(Object::Order order) override;
    virtual int32_t RuntimeInit(RuntimeLevel runtime_lvl, Object::Order order) override;
    virtual int32_t RuntimeRelease(RuntimeLevel runtime_lvl, Object::Order order) override;

protected:
    T *m_obj_relay;
};

template<typename T>
ObjectInitRelay<T>::ObjectInitRelay(const ObjectName &name): T(name), m_obj_relay(nullptr)
{
}

template<typename T>
ObjectInitRelay<T>::~ObjectInitRelay()
{
    assert(m_obj_relay != nullptr);

    delete m_obj_relay;
}

template<typename T>
void ObjectInitRelay<T>::SetObjRelay(T *obj_relay)
{
    m_obj_relay = obj_relay;
}

template<typename T>
T *ObjectInitRelay<T>::GetObjRelay()
{
    return m_obj_relay;
}

template<typename T>
int32_t ObjectInitRelay<T>::StaticInit(Object::Order order)
{
    assert(m_obj_relay != nullptr);

    return m_obj_relay->StaticInit(order);
}

template<typename T>
int32_t ObjectInitRelay<T>::StaticRelease(Object::Order order)
{
    assert(m_obj_relay != nullptr);

    return m_obj_relay->StaticRelease(order);
}

template<typename T>
int32_t ObjectInitRelay<T>::RuntimeInit(RuntimeLevel runtime_lvl, Object::Order order)
{
    assert(m_obj_relay != nullptr);

    return m_obj_relay->RuntimeInit(runtime_lvl, order);
}

template<typename T>
int32_t ObjectInitRelay<T>::RuntimeRelease(RuntimeLevel runtime_lvl, Object::Order order)
{
    assert(m_obj_relay != nullptr);

    return m_obj_relay->RuntimeRelease(runtime_lvl, order);
}

}

}

#endif


