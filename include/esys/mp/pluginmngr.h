/*!
 * \file esys/mp/pluginmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/pluginmngrcore_t.h"
#include "esys/mp/plugin.h"

namespace esys
{

namespace mp
{

/*! \class PluginMngr esys/mp/pluginmngr.h "esys/mp/pluginmngr.h"
 * \brief Plugin class
 *
 */
class ESYS_API PluginMngr : public esys::PluginMngrCore_t<Plugin>
{
public:
    //! Constructor
    PluginMngr();

    //! Destructor
    virtual ~PluginMngr();

    virtual PluginBase *GetPluginFromEntryFct(void *entry_fct) override;
};

}

}

