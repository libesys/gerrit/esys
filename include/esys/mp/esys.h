/*!
 * \if DOXY_IMPL
 * \file esys/mp/esys.h
 * \brief
 * \else
 * \file esys/esys.h
 * \brief
 * \endif
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MP_ESYS_H__
#define __ESYS_MP_ESYS_H__

#include "esys/esys_defs.h"
#include "esys/mp/esysbase.h"

#include "esys/mp/pluginmngr.h"

namespace esys
{

namespace evtloop
{

namespace mp
{

class ESYS_API ESys;

}

}


#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class ESys esys/mp/esys.h "esys/mp/esys.h"
 * \brief Multi Platform ESys class
 * \else
 * \class ESys esys/esys.h "esys/esys.h"
 * \brief ESys class
 * \endif
 *
 */
class ESYS_API ESys: public ESysBase
{
public:
    friend class ESYS_API esys::evtloop::mp::ESys;
    //! Constructor
    ESys();

    //! Destructor
    virtual ~ESys();

    //! Create a log to collect logging information
    /*!
     *  \return 0 if successful, <0 otherwise
     */
    int32_t CreateLog();

    //! Sleep for a given amount of milli seconds
    /*!
     *  \param[in] msec the number of milliseconds to sleep
     */
    static void Sleep(uint16_t msec);

    //! Returns the amount of milli seconds since boot
    /*! Depending of the platform and HW, this count will eventually warp around.
     *
     *  \return the number of milliseconds since boot
     */
    static millis_t Millis();

    static std::size_t GetCount();
    static std::size_t GetPluginsCount();
    static Plugin *GetPlugin(std::size_t idx);

    PluginMngr &GetPluginMngr();
    bool GetPluginsLoaded();

    static int Release();

//!< \cond DOXY_IMPL
    //! Returns the ESys instance
    /*!
     *  \return the ESys instance
     */
    static ESys &Get();
    static ESysBase &GetBase();
    static ESysBase *GetBase(std::size_t idx);
    static void SetPlatform(int platform_id);
    static void SetPlatform(const platform::Id &platform_id);

    virtual SemaphoreBase *NewSemaphoreBase(const ObjectName &name, uint32_t count) override;
    virtual MutexBase *NewMutexBase(const ObjectName &name, MutexBase::Type type = MutexBase::DEFAULT) override;
    virtual TaskPlatIf *NewTaskPlat(TaskBase *task_base) override;
    virtual TaskMngrPlatIf *NewTaskMngrPlat(TaskMngrBase *taskmngr_base) override;
    virtual SystemPlatIf *NewSystemPlat(SystemBase *system_base) override;
    virtual TimerBase *NewTimerBase(const ObjectName &name) override;

    virtual uint32_t implMillis(TimeBase::Source source = TimeBase::DEFAULT_CLK) override;
    virtual uint64_t implMicros(TimeBase::Source source = TimeBase::DEFAULT_CLK) override;

    virtual TaskPlatIf *GetCurTaskPlatIf() override;
protected:
    void SetPluginsLoaded(bool plugins_loaded = true);
    int LoadPlugins();
    int ReleasePlugins();

    static ESys s_esys;
    static int32_t s_platform_id;
    static const platform::Id *s_platform;
    static const platform::Id *s_dft_platform;

    static ESysBase *s_esys_base;

    bool m_plugins_loaded = false;
    PluginMngr m_plugin_mngr;

//!< \endcond
};

#ifndef DOXY_PUB

}

#ifdef ESYS_MULTI_PLAT
using namespace mp;
#endif

#endif

}

#endif
