/*!
 * \if DOXY_IMPL
 * \file esys/mp/timer.h
 * \brief The Multi Platform declaration of the Timer
 * \else
 * \file esys/timer.h
 * \brief The declaration of the Timer
 * \endif
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/timerbase.h"

namespace esys
{

#ifndef DOXY_PUB
namespace mp
{
#endif

/*! \if DOXY_IMPL
 * \class Timer esys/mp/timer.h "esys/mp/timer.h"
 * \brief The Multi Platform Timer
 * \else
 * \class Timer esys/timer.h "esys/timer.h"
 * \brief The Timer class
 * \endif
 */
class ESYS_API Timer : public TimerBase
{
public:
    Timer(const ObjectName &name);
    virtual ~Timer();

    //! Start the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Start(uint32_t ms, bool one_shot = false, bool from_isr = false) override;

    //! Stop the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Stop(bool from_isr = false) override;

    //! Return the Timer period in ms
    /*! \return the period in ms
    */
    virtual int32_t GetPeriod() override;

    //! Return the state of the Timer
    /*! \return True if the Timer is running, false otherwise
    */
    virtual bool IsRunning() override;
    virtual bool IsOneShot() override;

    virtual void SetCallback(TimerCallbackBase *callback) override;
    virtual TimerCallbackBase *GetCallback() override;
protected:
//!< \cond DOXY_IMPL
    TimerBase *m_timer = nullptr;
//!< \endcond
};

#ifndef DOXY_PUB
}
#endif

#ifdef ESYS_USE_MP
using namespace mp;
#endif

}







