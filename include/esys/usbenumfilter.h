/*!
 * \file esys/usbenumfilter.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_USBENUMFILTER_H__
#define __ESYS_USBENUMFILTER_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/usbdevinfo.h"

#include <vector>

namespace esys
{

class ESYS_API USBEnumFilter
{
public:
    USBEnumFilter();
    virtual ~USBEnumFilter();

    virtual bool IsKnownDev(USBDevInfo *dev)=0;

    void Filter(USBDevInfo *dev);

    std::size_t GetNbrFiltered();
    USBDevInfo *GetFiltered(std::size_t idx);
    void ClearFiltered();
protected:
    virtual void AddDev(USBDevInfo *dev);

    std::vector<USBDevInfo *> m_devices;
};

}

#endif
