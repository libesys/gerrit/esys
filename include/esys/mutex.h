/*!
 * \file esys/mutex.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MUTEX_H__
#define __ESYS_MUTEX_H__

#include "esys/esys_defs.h"
#include "esys/mutexbase.h"

#ifndef ESYS_MULTI_PLAT

#ifdef ESYS_USE_SYSC
#include "esys/sysc/mutex.h"
#elif defined ESYS_USE_WX
#include "esys/wx/mutex.h"
#elif defined ESYS_USE_BOOST
#include "esys/boost/mutex.h"
#elif defined ESYS_USE_FREERTOS
#include "esys/freertos/mutex.h"
#endif

#else

#include "esys/mp/mutex.h"

#endif

#endif
