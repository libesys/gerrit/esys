/*!
 * \file esys/i2chw_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_I2CHW_IF_H__
#define __ESYS_I2CHW_IF_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/i2cdevicebase.h"

namespace esys
{

class ESYS_API I2CHW_if
{
public:
    virtual int16_t Start()=0;
    virtual int16_t Stop()=0;
    virtual int16_t Read(I2CDeviceBase *dev, uint8_t &value)=0;
    virtual int16_t Write(I2CDeviceBase *dev, uint8_t value)=0;
    virtual int16_t Read(I2CDeviceBase *dev, uint8_t *buf, uint16_t buf_len, bool repeat=false)=0;
    virtual int16_t Write(I2CDeviceBase *dev, uint8_t *buf, uint16_t buf_len, bool repeat=false)=0;
    virtual int16_t Transfer(I2CDeviceBase *dev, uint8_t *tx_buf, uint16_t tx_buf_len, uint8_t *rx_buf, uint16_t rx_buf_len, bool repeat=false)=0;
};

}

#endif
