/*!
 * \file esys/usbtree.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_USBTREE_H__
#define __ESYS_USBTREE_H__

#include "esys/usbtreeitem.h"

namespace esys
{

class ESYS_API USBTree: public USBTreeItem
{
public:
    USBTree();
    virtual ~USBTree();

};

}


#endif



