/*!
 * \file esys/mutexbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_MUTEXBASE_H__
#define __ESYS_MUTEXBASE_H__

#include "esys/esys_defs.h"
#include "esys/objectleaf.h"

#ifdef DEFAULT
#undef DEFAULT
#endif

namespace esys
{
/*! \class MutexBase esys/mutexbase.h "esys/mutexbase.h"
 *  \brief The base class of all mutex implementation
 *
 * Even if the Mutex API support being used from an HW interrupt context, it must be
 * noted that it's usually not supported by the underlying RTOS and/or HW.
 */
class ESYS_API MutexBase : public ObjectLeaf
{
public:
    /*! \enum Type
     *  \brief The type of the Mutex
     */
    enum Type
    {
        DEFAULT,    //!< Standard Mutex
        RECURSIVE   //!< Recursive Mutex
    };

    //! Constructor
    /*!
     *  \param[in] name the name of the Mutex
     *  \param[in] type the type of the Mutex
     */
    MutexBase(const ObjectName &name, Type type=DEFAULT);

    //! Destructor
    virtual ~MutexBase();

    //! Return the type of the Mutex
    /*!
     *  \return the Mutex type
     */
    Type GetType();

    //! Lock the Mutex
    /*! Even if the Mutex API support locking it from an HW interrupt context, it must be
     *  noted that it's usually not supported by the underlying RTOS and/or HW.
     *
     *  \param[in] from_isr if true the call is made from an HW interrupt context
     *  \return 0 if success, otherwise <0
     */
    virtual int32_t Lock(bool from_isr = false) = 0;

    //! Unlock the Mutex
    /*! Even if the Mutex API support unlocking it from an HW interrupt context, it must be
     *  noted that it's usually not supported by the underlying RTOS and/or HW.
     *
     *  \param[in] from_isr if true the call is made from an HW interrupt context
     *  \return 0 if success, otherwise <0
     */
    virtual int32_t UnLock(bool from_isr = false) = 0;

    //! Try locking the Mutex
    /*! Even if the Mutex API support trying to lock it from an HW interrupt context, it must be
     *  noted that it's usually not supported by the underlying RTOS and/or HW.
     *
     *  \param[in] from_isr if true the call is made from an HW interrupt context
     *  \return 0 if success, otherwise <0
     */
    virtual int32_t TryLock(bool from_isr = false) = 0;

protected:
    Type m_type;    //!< Type of the Mutex
};


}

#endif
