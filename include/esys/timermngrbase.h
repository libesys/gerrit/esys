/*!
 * \file esys/timermngrbase.h
 * \brief The base class of all Timer Manager implementations
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/objectnode.h"
#include "esys/objectname.h"

namespace esys
{

class ESYS_API TimerBase;

/*! \class TimerMngrBase esys/timermngrbase.h "esys/timermngrbase.h"
 *  \brief Base class of all Timer Manager implementations
 */
class ESYS_API TimerMngrBase: public ObjectNode
{
public:
    TimerMngrBase(const ObjectName &name);
    virtual ~TimerMngrBase();

    //! Returns the number of Timers handled by the Manager
    /*! \return the number of Timers
    */
    virtual uint16_t GetCount();

    virtual int32_t StopAll();

    virtual void Start(TimerBase *timer);
    virtual void Stop(TimerBase *timer);
    virtual bool IsRunning(TimerBase *timer);
protected:
//!< \cond DOXY_IMPL
    virtual void AddTimer(TimerBase *timer);

    TimerBase *m_first = nullptr;
    TimerBase *m_last = nullptr;
    uint16_t m_count = 0;
//!< \endcond
};

}





