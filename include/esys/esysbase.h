/*!
 * \file esys/esysbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_ESYSBASE_H__
#define __ESYS_ESYSBASE_H__

#include "esys/esys_defs.h"
#include "esys/list.h"
#include "esys/module.h"

namespace esys
{

class ESYS_API SystemBase;
class ESYS_API SystemTask;
class ESYS_API TaskMngrBase;

class ESYS_API ESysBase
{
public:
    void SetSystemBase(SystemBase *system_base);
    SystemBase *GetSystemBase();
    void SetSystemTask(SystemTask *system_task);
    SystemTask *GetSystemTask();
    void SetTaskMngrBase(TaskMngrBase *taskmngr_base);
    TaskMngrBase *GetTaskMngrBase();
protected:
    ESysBase();
    ~ESysBase();
private:
    SystemBase *m_system_base;
    SystemTask *m_system_task;
    TaskMngrBase *m_taskmngr_base;
    List<Module> m_list_modules;
};

}

#endif

