/*!
 * \file esys/busmngrbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BUSMNGRBASE_H__
#define __ESYS_BUSMNGRBASE_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/busbase_t.h"

namespace esys
{

class ESYS_API BusMngrBase
{
public:
    BusMngrBase();
    virtual ~BusMngrBase();

    virtual uint8_t GetNbrBus();
protected:
    uint8_t m_nbr_bus;
};

template<class T>
class BusMngrBase_t: public BusMngrBase
{
public:
    typedef BusBase_t<T> BusBaseType;

    BusMngrBase_t();
    virtual ~BusMngrBase_t();

    void AddBus(BusBaseType *bus);
    BusBaseType *GetBus(uint8_t idx);
private:
    BusBaseType *m_first;
    BusBaseType *m_last;
};

template<class T>
BusMngrBase_t<T>::BusMngrBase_t(): m_first(NULL), m_last(NULL)
{
}

template<class T>
BusMngrBase_t<T>::~BusMngrBase_t()
{
}

template<class T>
void BusMngrBase_t<T>::AddBus(BusBaseType *bus)
{
    m_nbr_bus++;

    if (m_first==NULL)
    {
        m_first=bus;
        return;
    }
    if (m_last==NULL)
    {
        m_last=bus;
        m_first->SetNext(m_last);
    }
    else
    {
        m_last->SetNext(bus);
        m_last=bus;
    }
}

template<class T>
typename BusMngrBase_t<T>::BusBaseType *BusMngrBase_t<T>::GetBus(uint8_t idx)
{
    BusBaseType *bus=m_first;
    uint8_t count=0;

    if ((idx<0) || (idx>=GetNbrBus()))
        return NULL;

    while ((idx!=count) && (bus!=NULL))
    {
        bus=bus->GetNext();
        count++;
    }
    return bus;
}

}

#endif

