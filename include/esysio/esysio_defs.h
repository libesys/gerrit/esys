/*!
 * \file esys/esysio_defs.h
 * \brief Definitions needed for esysio
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYSIO_DEFS_H__
#define __ESYSIO_DEFS_H__

#ifdef ESYSIO_EXPORTS
#define ESYSIO_API __declspec(dllexport)
#elif ESYSIO_USE
#define ESYSIO_API __declspec(dllimport)
#else
#define ESYSIO_API
#endif

#endif

