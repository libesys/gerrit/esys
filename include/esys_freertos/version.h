/*!
 * \file esys/version.h
 * \brief Version info for esys_freertos
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_VERSION_H__
#define __ESYS_FREERTOS_VERSION_H__

// Bump-up with each new version
#define ESYS_FREERTOS_MAJOR_VERSION    0
#define ESYS_FREERTOS_MINOR_VERSION    0
#define ESYS_FREERTOS_RELEASE_NUMBER   1
#define ESYS_FREERTOS_VERSION_STRING   L"ESys FreeRTOS 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYS_FREERTOS_VERSION_NUM_DOT_STRING   "0.0.1"
#define ESYS_FREERTOS_VERSION_NUM_DOT_WSTRING  L"0.0.1"
#define ESYS_FREERTOS_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define ESYS_FREERTOS_VERSION_NUMBER (ESYS_FREERTOS_MAJOR_VERSION * 1000) + (ESYS_FREERTOS_MINOR_VERSION * 100) + ESYS_FREERTOS_RELEASE_NUMBER
#define ESYS_FREERTOS_BETA_NUMBER      1
#define ESYS_FREERTOS_VERSION_FLOAT ESYS_FREERTOS_MAJOR_VERSION + (ESYS_FREERTOS_MINOR_VERSION/10.0) + (ESYS_FREERTOS_RELEASE_NUMBER/100.0) + (ESYS_FREERTOS_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYS_FREERTOS_CHECK_VERSION(major,minor,release) \
    (ESYS_FREERTOS_MAJOR_VERSION > (major) || \
    (ESYS_FREERTOS_MAJOR_VERSION == (major) && ESYS_FREERTOS_MINOR_VERSION > (minor)) || \
    (ESYS_FREERTOS_MAJOR_VERSION == (major) && ESYS_FREERTOS_MINOR_VERSION == (minor) && ESYS_FREERTOS_RELEASE_NUMBER >= (release)))

#endif

