/*!
 * \file esys/esys_freertos_defs.h
 * \brief Definitions needed for esys_freertos
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_DEFS_H__
#define __ESYS_FREERTOS_DEFS_H__

#ifdef ESYS_FREERTOS_LOCAL

#include "esys/esys_defs.h"
#define ESYS_FREERTOS_API ESYS_API

#else

#ifdef ESYS_FREERTOS_EXPORTS
#define ESYS_FREERTOS_API __declspec(dllexport)
#elif ESYS_FREERTOS_USE
#define ESYS_FREERTOS_API __declspec(dllimport)
#else
#define ESYS_FREERTOS_API
#endif

#endif

#endif

