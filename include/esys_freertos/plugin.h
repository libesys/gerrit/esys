/*!
 * \file esys_freertos/plugin.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_FREERTOS_PLUGIN_H__
#define __ESYS_FREERTOS_PLUGIN_H__

#include "esys_freertos/esys_freertos_defs.h"
#include <esys/mp/plugin_t.h>

namespace esys_freertos
{

class ESYS_FREERTOS_API Plugin: public esys::mp::Plugin_t<Plugin>
{
public:
    typedef esys::mp::Plugin_t<Plugin> BaseType;

    Plugin();
    virtual ~Plugin();

    virtual int Init();
    virtual int Release();

protected:

};

}

DECLARE_ESYS_PLUGIN(ESYS_FREERTOS_API);

#endif



