/*!
 * \file esys/version.h
 * \brief Version info for esysio_t
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __ESYSIO_T_VERSION_H__
#define __ESYSIO_T_VERSION_H__

// Bump-up with each new version
#define ESYSIO_T_MAJOR_VERSION    0
#define ESYSIO_T_MINOR_VERSION    0
#define ESYSIO_T_RELEASE_NUMBER   1
#define ESYSIO_T_VERSION_STRING   _T("esysio_t 0.0.1")

// Must be updated manually as well each time the version above changes
#define ESYSIO_T_VERSION_NUM_DOT_STRING   "0.0.1"
#define ESYSIO_T_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define ESYSIO_T_VERSION_NUMBER (ESYSIO_T_MAJOR_VERSION * 1000) + (ESYSIO_T_MINOR_VERSION * 100) + ESYSIO_T_RELEASE_NUMBER
#define ESYSIO_T_BETA_NUMBER      1
#define ESYSIO_T_VERSION_FLOAT ESYSIO_T_MAJOR_VERSION + (ESYSIO_T_MINOR_VERSION/10.0) + (ESYSIO_T_RELEASE_NUMBER/100.0) + (ESYSIO_T_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define ESYSIO_T_CHECK_VERSION(major,minor,release) \
    (ESYSIO_T_MAJOR_VERSION > (major) || \
    (ESYSIO_T_MAJOR_VERSION == (major) && ESYSIO_T_MINOR_VERSION > (minor)) || \
    (ESYSIO_T_MAJOR_VERSION == (major) && ESYSIO_T_MINOR_VERSION == (minor) && ESYSIO_T_RELEASE_NUMBER >= (release)))

#endif

