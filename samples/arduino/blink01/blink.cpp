/*!
 * \file arduino/blink01/blink.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "Arduino.h"

#define LED0 13

void setup()
{
    pinMode(LED0, OUTPUT);
}


void loop()
{
    digitalWrite(LED0, HIGH);
    delay(2000);
    digitalWrite(LED0, LOW);
    delay(500);
    digitalWrite(LED0, HIGH);
    delay(250);
    digitalWrite(LED0, LOW);
    delay(1000);
}

