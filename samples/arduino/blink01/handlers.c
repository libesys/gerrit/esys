/*!
 * \file arduino/blink01/hanlders.c
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

void Ard_SysTick_Handler(void);

void SysTick_Handler(void)
{
    Ard_SysTick_Handler();
}

