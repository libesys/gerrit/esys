/*!
 * \file esys/esysio_t_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// esysio_t.cpp : source file that includes just the standard includes
// esysio_t.pch will be the pre-compiled header
// esysio_t.obj will contain the pre-compiled type information

#include "esysio_t/esysio_t_prec.h"

// TODO: reference any additional headers you need in esysio_t_prec.h
// and not in this file

