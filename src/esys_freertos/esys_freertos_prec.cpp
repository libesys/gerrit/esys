/*!
 * \file esys_freertos/esys_freertos_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// esys_freertos.cpp : source file that includes just the standard includes
// esys_freertos.pch will be the pre-compiled header
// esys_freertos.obj will contain the pre-compiled type information

#include "esys_freertos/esys_freertos_prec.h"

// TODO: reference any additional headers you need in esys_freertos_prec.h
// and not in this file

