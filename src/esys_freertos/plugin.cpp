/*!
 * \file esys_freertos/plugin.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys_freertos/plugin.h"
#include "esys_freertos/version.h"

#include <iomanip>
#include <iostream>

DEFINE_ESYS_PLUGIN(ESYS_FREERTOS_API, esys_freertos::Plugin);

namespace esys_freertos
{

Plugin::Plugin(): BaseType()
{
    SetName(L"FreeRTOS");
    SetShortName(L"FreeRTOS");
    SetVersion(ESYS_FREERTOS_VERSION_NUM_DOT_WSTRING);
}

Plugin::~Plugin()
{
}

int Plugin::Init()
{
    return 0;
}

int Plugin::Release()
{
    return 0;
}

}



