/*!
 * \file esys/esysio_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// esysio.cpp : source file that includes just the standard includes
// esysio.pch will be the pre-compiled header
// esysio.obj will contain the pre-compiled type information

#include "esysio/esysio_prec.h"

// TODO: reference any additional headers you need in esysio_prec.h
// and not in this file

