/*!
 * \file esys/esys_t_fix.cpp
 * \brief Entry point for a DLL under windows
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys_t_fix/esys_t_fix_prec.h"
#include "esys_t_fix/esys_t_fix_defs.h"

#ifdef WIN32
ESYS_T_FIX_API BOOL APIENTRY DllMain( HANDLE hModule,
                                      DWORD  ul_reason_for_call,
                                      LPVOID lpReserved
                                    )
{
    switch (ul_reason_for_call)
    {
        case DLL_PROCESS_ATTACH:
        case DLL_THREAD_ATTACH:
        case DLL_THREAD_DETACH:
        case DLL_PROCESS_DETACH:
            break;
    }
    return TRUE;
}
#endif

