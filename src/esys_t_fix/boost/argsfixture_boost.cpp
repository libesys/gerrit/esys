/*!
 * \file esys_t_fix/boost/argsfixture_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t_fix/esys_t_fix_prec.h"
#include "esys_t_fix/boost/argsfixture.h"

#include <esys/boost/esys.h>

#ifdef WIN32
#include <vld.h>
#endif

#include <iostream>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/version.hpp>

namespace po = boost::program_options;

namespace esys_t_fix
{

namespace boost
{

ArgsFixture::ArgsFixture(): ArgsFixtureBase()
    //m_argc(::boost::unit_test::framework::master_test_suite().argc)
    //, m_argv(::boost::unit_test::framework::master_test_suite().argv)
    //, m_desc("Allowed options")
    //, m_is_parsed(false)
    //, m_verbose(0)
    //, m_log_trace(false)
{
    SetArgs(::boost::unit_test::framework::master_test_suite().argc, ::boost::unit_test::framework::master_test_suite().argv);
}

ArgsFixture::~ArgsFixture()
{
}

/*int32_t ArgsFixture::FindFolders()
{
    std::string dft_test_file_path;
    ::boost::filesystem::path cur_path = ::boost::filesystem::absolute("");
    ::boost::filesystem::path test_path = cur_path.branch_path().branch_path();

    m_abs_temp_path = cur_path;
    m_abs_temp_path /= "temp";

    if (!::boost::filesystem::exists(m_abs_temp_path))
    {
        ::boost::filesystem::create_directory(m_abs_temp_path);
    }

    test_path /= "res";
    test_path /= m_test_files_folder;
    //test_path /= "ral_test";
    test_path /= "test_files";

    if (::boost::filesystem::exists(test_path))
        dft_test_file_path = test_path.string();
    else
    {
        test_path = cur_path.branch_path().branch_path().branch_path().branch_path();
        test_path /= "res";
        test_path /= "ral_test";
        test_path /= "test_files";

        //std::cout << test_path.string() << std::endl;

        if (::boost::filesystem::exists(test_path))
            m_dft_test_file_path = test_path.string();
        else
            m_dft_test_file_path = "test_files";
    }
    return 0;
} */

/*void ArgsFixture::AddDefaultOptions()
{
    m_desc.add_options()
    ("help-all", "produce help message")
#ifdef ESYS_USE_VLD
    ("vld-off", "turn off vld")
#endif

#ifdef ESYS_MULTI_PLAT
#ifdef WIN32
    ("freertos", "use FreeRTOS ESys")
#endif
    ("boost", "use Boost ESys")
    ("sysc", "use SystemC ESys")
#endif
    ("log_trace", po::value<std::string>(&m_log_trace_path)->implicit_value(""), "log calling traces")
    ("verbose,v", po::value<int>()->default_value(0), "set verbosity level: 0 is off")
    ("test_file_path", po::value<std::string>(&m_test_file_path)->default_value(m_dft_test_file_path), "set the path for test files");
} */

/*void ArgsFixture::SetTestFilesFolder(const std::string &test_files_folder)
{
    m_test_files_folder = test_files_folder;
}

void ArgsFixture::AddOptions(::boost::program_options::options_description *desc)
{
}

int32_t ArgsFixture::Parse()
{
    if (m_is_parsed == true)
        return -1;

    AddOptions(&m_desc);

    po::store(po::parse_command_line(m_argc, m_argv, m_desc), m_vm);
    po::notify(m_vm);

    if (m_vm.count("help-all"))
    {
        std::cout << m_desc << "\n";
#if BOOST_VERSION < 106400
        throw ::boost::unit_test::framework::nothing_to_test();
#else
        throw ::boost::unit_test::framework::nothing_to_test(0);
#endif
    }
    else
    {
        if (m_vm.count("log_trace"))
        {
            m_log_trace = true;
        }
        if (m_vm.count("verbose"))
        {
            m_verbose = m_vm["verbose"].as<int>();
        }
        if (m_vm.count("vld-off"))
        {
#ifdef WIN32
            VLDDisable();
#endif
        }
        else
        {
        }
#ifdef ESYS_MULTI_PLAT
        if (m_vm.count("boost"))
        {
            std::cout << "Boost platform is used" << std::endl;
            esys::boost::ESys::Select();
        }
        else if (m_vm.count("sysc"))
        {
            std::cout << "SystemC platform is used" << std::endl;
            //esys::sysc::ESys::Select();
        }
        else
        {
            std::cout << "Default platform is used" << std::endl;
        }
#endif
    }

    m_abs_test_file_path = ::boost::filesystem::absolute(m_test_file_path);
    m_is_parsed = true;
    return 0;
}

bool ArgsFixture::IsParsed()
{
    return m_is_parsed;
} */

}

}


