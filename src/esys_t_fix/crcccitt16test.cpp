/*!
 * \file esys_t_fix/crcccitt16test.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t_fix/esys_t_fix_prec.h"
#include "esys_t_fix/crcccitt16test.h"

#include <esys/assert.h>

#include <string.h>

namespace esys_t_fix
{

CRCCCITT16Test::CRCCCITT16Test(): m_crc(nullptr)
{
}

CRCCCITT16Test::~CRCCCITT16Test()
{
}

esys::uint32_t CRCCCITT16Test::GetChecksumCount()
{
    return 2;
}

esys::uint16_t CRCCCITT16Test::GetChecksum(esys::uint32_t idx)
{
    assert(idx < 2);

    return m_checksums[idx];
}

void CRCCCITT16Test::Test()
{
    esys::uint8_t bin[] = { 0x12, 0x34, 0x56, 0x78, 0x90};

    assert(m_crc != nullptr);

    m_crc->Reset();

    BOOST_REQUIRE_EQUAL((esys::uint16_t)0xFFFF, m_crc->GetChecksum());

    char s[] = "123456789";
    m_crc->AddData((esys::uint8_t *)s, strlen(s));

    BOOST_REQUIRE_EQUAL((esys::uint16_t)0x29B1, m_crc->GetChecksum());

    m_crc->Reset();

    BOOST_REQUIRE_EQUAL((esys::uint16_t)0xFFFF, m_crc->GetChecksum());

    m_crc->AddData(bin, sizeof(bin));

    BOOST_REQUIRE_EQUAL((esys::uint16_t)0x59EA, m_crc->GetChecksum());
}

void CRCCCITT16Test::SetCRC(esys::CRCBase *crc)
{
    m_crc = crc;
}

}

