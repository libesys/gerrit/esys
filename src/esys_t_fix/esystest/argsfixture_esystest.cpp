/*!
 * \file esys_t_fix/esystest/argsfixture_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t_fix/esys_t_fix_prec.h"
#include "esys_t_fix/esystest/argsfixture.h"

#include <esys/platform/id.h>
#include <esys/boost/esys.h>
#include <esys/mp/esys.h>

#ifdef WIN32
#include <vld.h>
#endif

#include <iostream>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

namespace po = boost::program_options;

namespace esys_t_fix
{

namespace esystest
{

ArgsFixture::ArgsFixture():  ::esystest::TestCaseCtrl(), m_desc("General ESys options")
{
}

ArgsFixture::~ArgsFixture()
{
}

/*void ArgsFixture::FindFolders()
>>>>>>> Stashed changes
{
    std::string dft_test_file_path;
    ::boost::filesystem::path cur_path = ::boost::filesystem::absolute("");
    ::boost::filesystem::path test_path = cur_path.branch_path().branch_path();

    m_abs_temp_path = cur_path;
    m_abs_temp_path /= "temp";

    if (!::boost::filesystem::exists(m_abs_temp_path))
    {
        ::boost::filesystem::create_directory(m_abs_temp_path);
    }

    test_path /= "res";
    test_path /= m_test_files_folder;
    //test_path /= "ral_test";
    test_path /= "test_files";

    if (::boost::filesystem::exists(test_path))
        dft_test_file_path = test_path.string();
    else
    {
        test_path = cur_path.branch_path().branch_path().branch_path().branch_path();
        test_path /= "res";
        test_path /= "ral_test";
        test_path /= "test_files";

        //std::cout << test_path.string() << std::endl;

        if (::boost::filesystem::exists(test_path))
            m_dft_test_file_path = test_path.string();
        else
            m_dft_test_file_path = "test_files";
    }
    return 0;
} */

void ArgsFixture::AddOptions(::boost::program_options::options_description &desc)
{
    m_desc.add_options()
#ifdef WIN32
    ("vld-off", "turn off vld")
#endif

#ifdef ESYS_MULTI_PLAT
#ifdef WIN32
    ("freertos", "use FreeRTOS ESys")
#endif
    ("boost", "use Boost ESys")
    ("sysc", "use SystemC ESys")
#endif
    ("log_trace", po::value<std::string>(&m_log_trace_path)->implicit_value(""), "log calling traces")
    ("verbose,v", po::value<int>()->default_value(0), "set verbosity level: 0 is off")
    ("test_file_path", po::value<std::string>(&m_test_file_path)->default_value(m_dft_test_file_path), "set the path for test files");

    desc.add(m_desc);
}

void ArgsFixture::SetTestFilesFolder(const std::string &test_files_folder)
{
    m_test_files_folder = test_files_folder;
}

int32_t ArgsFixture::HandleSwitches()
{
    if (m_vm.count("log_trace"))
    {
        m_log_trace = true;
    }
    if (m_vm.count("verbose"))
    {
        m_verbose = m_vm["verbose"].as<int>();
    }
    if (m_vm.count("vld-off"))
    {
#ifdef WIN32
        VLDDisable();
#endif
    }
    else
    {
    }
#ifdef ESYS_MULTI_PLAT
    if (m_vm.count("boost"))
    {
        std::cout << "Boost platform is used" << std::endl;
        esys::boost::ESys::Select();
    }
    else if (m_vm.count("sysc"))
    {
        std::cout << "SystemC platform is used" << std::endl;
        esys::mp::ESys::SetPlatform(esys::platform::SYSC);
    }
    else if (m_vm.count("freertos"))
    {
        esys::platform::Id *plat_id;

        std::cout << "FreeRTOS platform is used" << std::endl;

        plat_id = esys::platform::Id::Find(L"FreeRTOS");
        if (plat_id == nullptr)
        {
            std::cout << "ERROR: FreeRTOS platform couldn't be found" << std::endl;
            return -1;
        }
        else
            esys::mp::ESys::SetPlatform(*plat_id);
    }
    else
    {
        std::cout << "Default platform is used" << std::endl;
    }
#endif

    m_abs_test_file_path = ::boost::filesystem::absolute(m_test_file_path);
    return 0;
}

}

}


