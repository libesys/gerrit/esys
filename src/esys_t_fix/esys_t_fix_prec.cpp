/*!
 * \file esys/esys_t_fix_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

// esys_t_fix.cpp : source file that includes just the standard includes
// esys_t_fix.pch will be the pre-compiled header
// esys_t_fix.obj will contain the pre-compiled type information

#include "esys_t_fix/esys_t_fix_prec.h"

// TODO: reference any additional headers you need in esys_t_fix_prec.h
// and not in this file

