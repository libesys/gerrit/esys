/*!
 * \file esys/evtloop/sysc/task_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/evtloop/sysc/task.h"

namespace esys
{

namespace evtloop
{

namespace sysc
{

Task::Task(const char *name): TaskBase(name), TaskPlat(name)
{
    SetTaskBase(this);
    SetTaskPlat(this);
}

Task::~Task()
{
}

void Task::Init()
{
}

void Task::Setup()
{
}

TaskPlatIf *Task::GetTaskPlat()
{
    return this;
}

void Task::InternalCoreLoop()
{
}

}

}

}


