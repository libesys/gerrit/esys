/*!
 * \file esys/evtloop/sysc/esys_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/evtloop/sysc/esys.h"
#include "esys/evtloop/sysc/taskplat.h"
#include "esys/evtloop/sysc/taskmngr.h"
#include "esys/evtloop/sysc/taskmngrimpl.h"
#include "esys/evtloop/taskbase.h"

#include "esys/platform/id.h"

#ifdef WIN32
#include <Windows.h> //for the sleep
#endif

#include <boost/filesystem/path.hpp>
#ifdef DBG_LOG
#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>
#endif
#include <fstream>

#if 0 //#ifdef SYSTEMC_201
#include <systemc/kernel/sc_simcontext.h>
#include <systemc/kernel/sc_time.h>
#include <systemc/kernel/sc_wait.h>
#include <systemcclass.h>
#else
#include <systemc.h>
#include <sysc/sc_simulation.h>
#endif

namespace esys
{

namespace evtloop
{

namespace sysc
{

class ESYS_SYSC_API ESysImpl
{
public:
    ESysImpl();
    virtual ~ESysImpl();

    void SetName(const char *name);
    const char *GetName();

    int32_t CreateLog();

private:
    std::ofstream *m_ofs;
    const char *m_name;

#ifdef DBG_LOG
    logmod::logger *m_log;
#endif
};

ESysImpl::ESysImpl()
    : m_ofs(NULL)
    , m_name(NULL)
#ifdef DBG_LOG
    , m_log(NULL)
#endif
{

#if defined _MSC_VER && defined _DEBUG
    //_CrtSetBreakAlloc(531);

#endif
}

ESysImpl::~ESysImpl()
{
#ifdef DBG_LOG
    if (m_log != NULL)
    {
        dbg_log::dbg_class::SetDefaultLogger(NULL);
        m_log->SetOfstream(NULL);
        delete m_log;
        m_log = NULL;
    }
#endif
    if (m_ofs != NULL)
    {
        delete m_ofs;
        m_ofs = NULL;
    }
}

void ESysImpl::SetName(const char *name)
{
    m_name = name;
}

const char *ESysImpl::GetName()
{
    return m_name;
}

int32_t ESysImpl::CreateLog()
{
    std::string s;

    if (GetName() != NULL)
        s.append(GetName());
    else
        s.append("esys");
    s.append(".log");
    m_ofs = new std::ofstream(s.c_str());
#ifdef DBG_LOG
    m_log = new logmod::logger();

    m_log->SetOfstream(m_ofs);
    dbg_log::dbg_class::SetDefaultLogger(m_log);

    dbg_log::dbg_class::Enable();
#endif
    return 0;
}

uint64_t g_Millis()
{
    double millis = sc_time_stamp().to_seconds() * 1000;

    return (uint64_t)millis;
}

uint64_t ESys::m_started_time = 0; // Start time is obviously always 0, since SystemC has a simulated time
ESys ESys::g_esys;

static ESysImpl g_ESysImpl;

ESys::ESys()
    : esys::evtloop::mp::ESysBase(ESYS_PLAT_SYSC, "SystemC")
    , m_impl(g_ESysImpl)
{
    esys::evtloop::mp::ESysBase::SetElement(this);
}

ESys::~ESys()
{
}

ESys &ESys::Get()
{
    return g_esys;
}

void ESys::SetAppName(const char *app_name)
{
    m_impl.SetName(app_name);
}

int32_t ESys::CreateLog()
{
    return m_impl.CreateLog();
}

int32_t ESys::GetExeDir(std::string &exe_dir)
{
#ifdef WIN32
    char ownPth[MAX_PATH];

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule != NULL)
    {
        // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileName(hModule, ownPth, (sizeof(ownPth)));

        // Use above module handle to get the path using GetModuleFileName()
        // std::wcout << ownPth << std::endl ;
        ::boost::filesystem::path exe_path = std::string(ownPth);
        // std::wcout << exe_path.branch_path().wstring() << std::endl;
        exe_dir = exe_path.branch_path().string();
        return 0;
    }
    else
    {
        // std::wcout << "Module handle is NUL" << std::endl ;
        exe_dir = "";
        return -1;
    }
#else
#endif
}

void ESys::Sleep(millis_t msec)
{
    wait(sc_time((double)msec, SC_MS));
}

millis_t ESys::Millis()
{
    return g_Millis();
}

void ESys::Init()
{
    sc_simulation::init();
}

void ESys::Release()
{
    // SystemC::CleanUp();
}

#ifdef ESYS_MULTI_PLAT
void ESys::impSleep(millis_t msec)
{
    wait(sc_time((double)msec, SC_MS));
}

millis_t ESys::impMillis()
{
    return g_Millis();
}

ESys &ESys::impGet()
{
    return g_esys;
}

void ESys::impInit()
{
    Init();
}

void ESys::impRelease()
{
    Release();
}

TaskPlatIf *ESys::impNewTaskPlat(const char *name)
{
    return new TaskPlat(name);
}

TaskMngrBase *ESys::impNewTaskMngrBase()
{
    return new TaskMngr();
}

MutexBase *ESys::impNewMutexBase(MutexBase::Type type)
{
    return NULL;
}

#endif

} // namespace sysc

} // namespace evtloop

} // namespace esys
