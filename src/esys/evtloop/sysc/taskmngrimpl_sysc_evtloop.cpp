/*!
 * \file esys/evtloop/sysc/taskmngrimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/evtloop/sysc/taskmngrimpl.h"
#include "esys/evtloop/sysc/taskmngr.h"
#include "esys/evtloop/sysc/taskplat.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace sysc
{

TaskMngrImpl::TaskMngrImpl(sc_module_name name)
    : sc_module(name), m_task_mngr(NULL), m_task_mngr_base(NULL), m_boot_time(50, SC_MS)
    , m_init_setup_done("init_setup_done"), m_task_done("task_done")
{
    SC_THREAD(Main);
}

TaskMngrImpl::~TaskMngrImpl()
{
}

void TaskMngrImpl::SetTaskMngr(TaskMngrBase *task_mngr)
{
    m_task_mngr_base=task_mngr;
}

void TaskMngrImpl::SetTaskMngr(TaskMngr *task)
{
    m_task_mngr=task;
}

TaskMngr *TaskMngrImpl::GetTaskMngr()
{
    if (m_task_mngr==NULL)
    {
        assert(m_task_mngr_base!=NULL);

        m_task_mngr=dynamic_cast<TaskMngr *>(m_task_mngr_base);
    }
    return m_task_mngr;
}

void TaskMngrImpl::Main()
{
    bool done=false;

    /*assert(m_task_mngr!=NULL);

    m_task_mngr->Run(); */

    m_init_setup_done.notify(m_boot_time);

    while (!done)
    {
        wait(m_task_done);

        done=GetTaskMngr()->AllTasksDone();
    }
}

void TaskMngrImpl::WaitInitSetupDone()
{
    wait(m_init_setup_done);
}

void TaskMngrImpl::SignalTaskDone()
{
    m_task_done.notify();
}

}

}

}
