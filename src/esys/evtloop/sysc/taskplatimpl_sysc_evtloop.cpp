/*!
 * \file esys/evtloop/sysc/taskimpl_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/evtloop/sysc/taskplatimpl.h"
#include "esys/evtloop/sysc/taskplat.h"
#include "esys/evtloop/sysc/taskmngr.h"
#include "esys/evtloop/sysc/taskmngrimpl.h"

#ifdef SYSTEMC_201
#include <systemc/kernel/sc_simcontext.h>
#include <systemc/kernel/sc_time.h>
#include <systemc/kernel/sc_wait.h>
#else
#include <systemc.h>
#endif

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace sysc
{

TaskPlatImpl *TaskPlatImpl::GetCurrent()
{
    TaskPlatImpl *cur;


    /*    self=wxThread::This();
        if (self==NULL)
            return NULL;
        try
        {
            cur=dynamic_cast<TaskPlatImpl*>(self);
        }
        catch (std::bad_cast &)
        {
            return NULL;
        } */
    return cur;
}

TaskPlatImpl::TaskPlatImpl(const sc_module_name &name)
    : sc_module(name), m_task_plat(NULL), m_task_base(NULL), m_started(false)
{
    SC_THREAD(Main);
}

TaskPlatImpl::~TaskPlatImpl()
{
}

void TaskPlatImpl::NotifyExit()
{
    TaskMngr *task_mngr;
    assert(m_task_plat!=NULL);

    task_mngr=dynamic_cast<TaskMngr *>(m_task_plat->GetTaskMngrBase());
    assert(task_mngr!=NULL);
    assert(task_mngr->GetImpl()!=NULL);

    task_mngr->GetImpl()->SignalTaskDone();
}

millis_t TaskPlatImpl::Millis()
{
    double millis=sc_time_stamp().to_seconds()*1000;

    return (millis_t)millis;
}

void TaskPlatImpl::Main()
{
    TaskMngr *task_mngr;
    assert(m_task_plat!=NULL);

    task_mngr=dynamic_cast<TaskMngr *>(m_task_plat->GetTaskMngrBase());
    assert(task_mngr!=NULL);
    assert(task_mngr->GetImpl()!=NULL);

    task_mngr->GetImpl()->WaitInitSetupDone();
    assert(m_task_base!=NULL);
    m_task_base->Do();
}

void TaskPlatImpl::Wait()
{
    //m_sem.Wait();
}

void TaskPlatImpl::Signal()
{
    //m_sem.Post();
}

/*wxThread::ExitCode TaskPlatImpl::Entry()
{
    assert(m_task!=NULL);

    m_task->Do();

    return (wxThread::ExitCode)m_task->GetExit();
} */

void TaskPlatImpl::Sleep(millis_t msec)
{
    wait(sc_time((double)msec, SC_MS));
}

void TaskPlatImpl::SleepU(micros_t us)
{
    wait(sc_time((double)us, SC_US));
}

void TaskPlatImpl::SetTaskBase(TaskBase *task_base)
{
    m_task_base=task_base;
}

TaskBase *TaskPlatImpl::GetTaskBase()
{
    return m_task_base;
}

void TaskPlatImpl::SetTaskPlat(TaskPlat *task_plat)
{
    m_task_plat=task_plat;
}

TaskPlat *TaskPlatImpl::GetTaskPlat()
{
    return m_task_plat;
}

void TaskPlatImpl::DoLoopIterationEnds()
{
    /*    if (!m_started)
            Sleep(10);
        else
            Yield(); */
}

void TaskPlatImpl::StartTask()
{
    m_start_task.notify();
}

void TaskPlatImpl::WaitTaskDone()
{
    wait(m_task_done);
}

}

}

}

