/*!
 * \file esys/evtloop/sysc/taskmngr_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/evtloop/sysc/taskmngr.h"

#include <systemc.h>

namespace esys
{

namespace evtloop
{

namespace sysc
{

TaskMngr::TaskMngr(): BaseType("sysc::TaskMngr")
{
}

TaskMngr::~TaskMngr()
{
}

void TaskMngr::Run()
{
    BaseType::Run();
#ifdef SYSTEMC_201
    sc_start(-1);
#else
    sc_start();
#endif
}

}

}

}


