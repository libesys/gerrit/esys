/*!
 * \file esys/evtloop/sysc/taskplat_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/esys_defs.h"

#include "esys/evtloop/sysc/taskplat.h"
#include "esys/evtloop/sysc/taskplatimpl.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace sysc
{

TaskPlat *TaskPlat::Current()
{
    TaskPlatImpl *cur=TaskPlatImpl::GetCurrent();
    if (cur!=NULL)
        return cur->GetTaskPlat();
    return NULL;
}

TaskBase *TaskPlat::CurrentBase()
{
    TaskPlatImpl *cur=TaskPlatImpl::GetCurrent();

    if (cur!=NULL)
        return cur->GetTaskBase();
    return NULL;
}

TaskPlat::TaskPlat(const char *name): m_task_mngr(NULL)
{
    m_impl=new TaskPlatImpl(sc_module_name(name));
    m_impl->SetTaskPlat(this);
    TaskBase::AddCurrentFct(&TaskPlat::CurrentBase);
}

TaskPlat::~TaskPlat()
{
    if (m_impl!=NULL)
    {
        delete m_impl;
        m_impl=NULL;
    }
}

millis_t TaskPlat::Millis()
{
    assert(m_impl!=NULL);

    return m_impl->Millis();
}

void TaskPlat::Sleep(millis_t ms)
{
    assert(m_impl!=NULL);

    m_impl->Sleep(ms);
}

void TaskPlat::SleepU(micros_t us)
{
    assert(m_impl!=NULL);

    m_impl->Sleep(us);
}

void TaskPlat::Wait()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void TaskPlat::Signal()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void TaskPlat::DoLoopIterationEnds()
{
    assert(m_impl!=NULL);

    m_impl->DoLoopIterationEnds();
}

void TaskPlat::SetTaskBase(TaskBase *task_base)
{
    assert(m_impl!=NULL);

    m_impl->SetTaskBase(task_base);
}

void TaskPlat::NotifyExit()
{
    assert(m_impl!=NULL);

    m_impl->NotifyExit();
}

void TaskPlat::SetTaskMngrBase(TaskMngrBase *task_mngr)
{
    m_task_mngr=task_mngr;
}

/*void TaskPlat::SetTaskMngr(TaskMngr *task_mngr)
{
    m_task_mngr=task_mngr;
} */

TaskMngrBase *TaskPlat::GetTaskMngrBase()
{
    return m_task_mngr;
}

void TaskPlat::StartTask()
{
    assert(m_impl!=NULL);

    m_impl->StartTask();
}

void TaskPlat::WaitTaskDone()
{
    assert(m_impl!=NULL);

    m_impl->WaitTaskDone();
}

}

}

}

