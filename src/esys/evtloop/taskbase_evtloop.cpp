/*!
 * \file esys/evtloop/taskbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/taskbase.h"
#include "esys/evtloop/tasklet.h"
#include "esys/evtloop/taskplatif.h"

#include "esys/assert.h"

#include <stddef.h>     //Needed for NULL under GCC

#if defined WIN32 || defined MULTIOS
#include <vector>
#endif

namespace esys
{

namespace evtloop
{

TaskBase::TaskBase(const char *name)
    : m_info(NULL), m_task_mngr(NULL), m_task_plat(NULL), m_nbr_info(0)
    , m_max_info(0), m_exit(false), m_prev_task(NULL), m_running(false)
{
}

TaskBase::~TaskBase()
{
}

// TaskBaseIf

/*void TaskBase::Init()
{
}

void TaskBase::Setup()
{
}*/

void TaskBase::Exit(int16_t ret)
{
    m_ret=ret;
    m_exit=true;
}

bool TaskBase::IsExit()
{
    return m_exit;
}

int16_t TaskBase::GetExit()
{
    return m_ret;
}

void TaskBase::Done()
{
    assert(m_done_count>0);

    m_done_count--;
}

void TaskBase::DoLoopIteration(bool all)
{
    InternalCoreLoop();
    DoTasklets(all);
    if (all)
        Loop();
    m_task_plat->DoLoopIterationEnds();
    if ((!m_exit)&&(m_done_count==0))
    {
        m_task_plat->Wait();
        m_done_count=m_nbr_info+1;
    }
}

/*void TaskBase::DoLoopIterationEnds()
{
} */

void TaskBase::Inits()
{
    uint8_t idx;

    InternalInit();
    Init();
    //m_task_plat=GetTaskPlat();

    for (idx=0; idx<m_nbr_info; ++idx)
    {
        assert(m_info[idx].m_tasklet!=NULL);

        m_info[idx].m_tasklet->Init();
    }

    //assert(m_task_plat!=NULL);
    //assert(m_task_mngr!=NULL);

    //m_task_plat->SetTaskMngrBase(m_task_mngr);  //To late!
}

void TaskBase::Setups()
{
    uint8_t idx;

    Setup();

    for (idx=0; idx<m_nbr_info; ++idx)
    {
        assert(m_info[idx].m_tasklet!=NULL);

        m_info[idx].m_tasklet->Setup();
    }
}

void TaskBase::SetInfoArray(TaskletInfo *info, uint8_t size)
{
    uint8_t idx;

    m_nbr_info=0;
    m_info=info;
    m_max_info=size;

    for (idx=0; idx<size; ++idx)
    {
        m_info[idx].m_tasklet=NULL;
        m_info[idx].m_is_core=false;
    }
}

void TaskBase::AddTasklet(Tasklet *tasklet, bool core)
{
    assert(m_nbr_info<m_max_info);
    assert(m_running==false);

    m_info[m_nbr_info].m_tasklet=tasklet;
    m_info[m_nbr_info++].m_is_core=core;
    tasklet->SetTask(this);
}

void TaskBase::InternalInit()
{
}

// TaskBase specific
void TaskBase::Do()
{
    m_done_count=m_nbr_info+1;
    m_running=true;

    while (!m_exit)
    {
        DoLoopIteration(true);
    }
}

void TaskBase::SetTaskPlat(TaskPlatIf *task_plat)
{
    m_task_plat=task_plat;
}

TaskPlatIf *TaskBase::GetTaskPlat()
{
    return m_task_plat;
}

void TaskBase::SetTaskMngr(TaskMngrBase *task_mngr)
{
    m_task_mngr=task_mngr;
}

TaskMngrBase *TaskBase::GetTaskMngr()
{
    return m_task_mngr;
}

void TaskBase::DoTasklets(bool all)
{
    uint8_t idx;

    InternalCoreLoop();

    if (m_info==NULL)
        return;

    for (idx=0; (idx<m_nbr_info) && (m_exit==false) ; ++idx)
    {
        if (all||m_info[idx].m_is_core)
        {
            assert(m_info[idx].m_tasklet!=NULL);

            m_info[idx].m_tasklet->Loop();
        }
    }
}


/*void TaskBase::InternalCoreLoop()
{
}*/

/*void TaskBase::Wait()
{
}

void TaskBase::Signal()
{
} */

void TaskBase::SetPrevTask(TaskBase *task)
{
    m_prev_task=task;
}

TaskBase *TaskBase::GetPrevTask()
{
    return m_prev_task;
}

#if defined WIN32 || defined MULTIOS

static std::vector<TaskBase::CurrentFct> g_current_fcts;

TaskBase *TaskBase::Current()
{
    uint32_t idx;
    TaskBase *current;

    for (idx=0; idx<g_current_fcts.size(); ++idx)
    {
        current=g_current_fcts[idx]();
        if (current!=NULL)
            return current;
    }
    return NULL;
}

void TaskBase::AddCurrentFct(CurrentFct fct)
{
    g_current_fcts.push_back(fct);
}

#else
static TaskBase::CurrentFct g_current_fct;

TaskBase *TaskBase::Current()
{
    assert(g_current_fct!=NULL);

    return g_current_fct();
}

void TaskBase::AddCurrentFct(TaskBase::CurrentFct fct)
{
    g_current_fct=fct;
}

#endif


}

}
