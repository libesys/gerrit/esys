/*!
 * \file esys/evtloop/task_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/task.h"


#ifdef ESYS_MULTI_PLAT

#include "esys/evtloop/mp/esys.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

Task::Task(const char *name): TaskBase(name), /*m_task_plat(NULL),*/ m_name(name)
{
}

Task::~Task()
{
    if ((m_task_plat != NULL) && (m_task_plat != this))
    {
        delete m_task_plat;
        m_task_plat = NULL;
    }
}

// Application Task and Tasklet specific
void Task::Init()
{
}

void Task::Setup()
{
}

//virtual void Loop()=0;

// Platform and Application independent present both in Tasks and Tasklets
/*void Task::Exit(int16_t ret)
{
}

bool Task::IsExit()
{
}

int16_t Task::GetExit()
{
}

void Task::Done()
{
} */

// Platform functions for both Task and Tasklet
millis_t Task::Millis()
{
    assert(m_task_plat!=NULL);

    return m_task_plat->Millis();
}

void Task::Sleep(millis_t ms)
{
    assert(m_task_plat!=NULL);

    m_task_plat->Sleep(ms);
}

void Task::SleepU(micros_t us)
{
    assert(m_task_plat!=NULL);

    m_task_plat->SleepU(us);
}

// Platform and Application independent present in Tasks only
//void DoLoopIteration(bool all=false)=0;
//void DoLoopIterationEnds()=0;
//void Inits()=0;

//void SetInfoArray(TaskletInfo *info, uint8_t size)=0;
//void AddTasklet(Tasklet *tasklet, bool core=false)=0;

// Platform and Application independent present in Tasklets only
//bool WaitCond()=0;

// Platform functions for Task only
void Task::DoLoopIterationEnds()
{
    assert(m_task_plat!=NULL);

    if (m_task_plat != this)
        m_task_plat->DoLoopIterationEnds();
}

void Task::Wait()
{
    assert(m_task_plat!=NULL);

    m_task_plat->Wait();
}

void Task::Signal()
{
    assert(m_task_plat!=NULL);

    m_task_plat->Signal();
}

void Task::InternalInit()
{
    GetTaskPlat();

    assert(m_task_plat!=NULL);
}

void Task::InternalCoreLoop()
{
}

TaskPlatIf *Task::GetTaskPlat()
{
    if (m_task_plat!=NULL)
        return m_task_plat;
    m_task_plat=esys::evtloop::ESys::Get().NewTaskPlat(m_name);    //ESysBase::NewTaskPlat(m_name);
    m_task_plat->SetTaskBase(this);
    return m_task_plat;
}

void Task::SetTaskBase(TaskBase *task_base)
{
}

void Task::SetTaskMngrBase(TaskMngrBase *task_mngr_base)
{
}

void Task::NotifyExit()
{
}

}

}

#endif
