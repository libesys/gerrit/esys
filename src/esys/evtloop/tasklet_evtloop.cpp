/*!
 * \file esys/evtloop/tasklet.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/tasklet.h"
#include "esys/evtloop/taskbase.h"
#include "esys/evtloop/taskplatif.h"

#include "esys/assert.h"

namespace esys
{

namespace evtloop
{

Tasklet::Tasklet()
{
}

Tasklet::~Tasklet()
{
}


void Tasklet::Init()
{
}

void Tasklet::Setup()
{
}

void Tasklet::Loop()
{
}


void Tasklet::Exit(int16_t ret)
{
    m_exit_val=ret;
    m_exit=true;

    assert(m_task != nullptr);

    m_task->Exit(ret);
}

bool Tasklet::IsExit()
{
    return m_exit;
}

int16_t Tasklet::GetExit()
{
    return m_exit_val;
}

void Tasklet::TaskletDone()
{
}

millis_t Tasklet::Millis()
{
    assert(m_task != nullptr);

    return GetTaskPlat()->Millis();
}

void Tasklet::Sleep(millis_t ms)
{
    assert(m_task != nullptr);

    GetTaskPlat()->Sleep(ms);
}

void Tasklet::SleepU(micros_t us)
{
    GetTaskPlat()->SleepU(us);
}


bool Tasklet::WaitCond()
{
    return false;
}

int16_t Tasklet::Wait(StateType state, TimeOutType time_out_ms)
{
    bool done = false;
    int16_t result = SUCCESS;

    assert(GetTask() != nullptr);

    if (time_out_ms == WAIT_FOREVER)
    {
        while (!done)
        {
            GetTask()->DoLoopIteration();

            if (m_prev_state == state)
                done = true;
        }
        return SUCCESS;
    }

    millis_t start = Millis();
    millis_t end = start + time_out_ms;
    millis_t cur;

    while (!done)
    {
        GetTask()->DoLoopIteration();

        if (m_prev_state == state)
            done = true;
        cur = Millis();

        if ((end > start) && (cur > start) && (cur > end))
        {
            // Case where Millis didn't warp around
            done = true;
            result = TIMED_OUT;
        }
        if ((end < start) && (cur < start) && (cur > end))
        {
            // Case where Millis did wrap around
            done = true;
            result = TIMED_OUT;
        }
    }
    return result;
}

void Tasklet::SetTask(TaskBase *task)
{
    m_task = task;
}

TaskBase *Tasklet::GetTask()
{
    return m_task;
}

TaskPlatIf *Tasklet::GetTaskPlat()
{
    if (m_task_plat != nullptr)
        return m_task_plat;

    assert(m_task != nullptr);

    m_task_plat=m_task;//->GetTaskPlat();
    return m_task_plat;
}

void Tasklet::SetState(StateType state)
{
    m_state = state;
}

Tasklet::StateType Tasklet::GetState()
{
    return m_state;
}

void Tasklet::SetPrevState(StateType prev_state)
{
    m_prev_state = prev_state;
}

Tasklet::StateType Tasklet::GetPrevState()
{
    return m_prev_state;
}

}

}


