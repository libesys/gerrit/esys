/*!
 * \file esys/evtloop/wx/taskmngrimpl_wx.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_wx/esys_wx_prec.h"
#include "esys/evtloop/wx/taskmngrimpl.h"
#include "esys/evtloop/wx/taskmngr.h"
#include "esys/evtloop/wx/taskplat.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace wx
{

TaskMngrImpl::TaskMngrImpl(const char *name): m_task_mngr(NULL)
{
}

TaskMngrImpl::~TaskMngrImpl()
{
}

void TaskMngrImpl::SetTaskMngr(TaskMngrBase *task_mngr)
{
    m_task_mngr=dynamic_cast<TaskMngr *>(task_mngr);
}

void TaskMngrImpl::SetTaskMngr(TaskMngr *task)
{
    m_task_mngr=task;
}

TaskMngr *TaskMngrImpl::GetTaskMngr()
{
    return m_task_mngr;
}

void TaskMngrImpl::Main()
{
}

void TaskMngrImpl::WaitInitSetupDone()
{
}

void TaskMngrImpl::SignalTaskDone()
{
}

}

}

}

