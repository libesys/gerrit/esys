/*!
 * \file esys/evtloop/wx/taskplatimpl_wx.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_wx/esys_wx_prec.h"
#include "esys/evtloop/wx/taskplatimpl.h"
#include "esys/evtloop/wx/taskplat.h"

#include <wx/timer.h>
#include <wx/utils.h>

namespace esys
{

namespace evtloop
{

namespace wx
{

TaskPlatImpl *TaskPlatImpl::GetCurrent()
{
    TaskPlatImpl *cur;
    wxThread *self;

    self=wxThread::This();
    if (self==NULL)
        return NULL;
    try
    {
        cur=dynamic_cast<TaskPlatImpl*>(self);
    }
    catch (std::bad_cast &)
    {
        return NULL;
    }
    return cur;
}

TaskPlatImpl::TaskPlatImpl(wxThreadKind kind)
    : wxThread(kind), m_task_plat(NULL), m_task_base(NULL), m_sem(), m_started(false)
{
    m_started_time = wxGetLocalTimeMillis();
}

TaskPlatImpl::~TaskPlatImpl()
{
}

millis_t TaskPlatImpl::Millis()
{
    wxLongLong diff = wxGetLocalTimeMillis()-m_started_time;

    return diff.GetValue();
}

void TaskPlatImpl::Wait()
{
    m_sem.Wait();
}

void TaskPlatImpl::Signal()
{
    m_sem.Post();
}

wxThread::ExitCode TaskPlatImpl::Entry()
{
    assert(m_task_base!=NULL);

    m_task_base->Do();

    return (wxThread::ExitCode)m_task_base->GetExit();
}

void TaskPlatImpl::Sleep(millis_t ms)
{
    wxMilliSleep(ms);
}

void TaskPlatImpl::SleepU(micros_t us)
{
    wxMicroSleep(us);
}

void TaskPlatImpl::SetTaskBase(TaskBase *task_base)
{
    m_task_base=task_base;
}

TaskBase *TaskPlatImpl::GetTaskBase()
{
    return m_task_base;
}

void TaskPlatImpl::SetTaskPlat(TaskPlat *task_plat)
{
    m_task_plat=task_plat;
}

TaskPlat *TaskPlatImpl::GetTaskPlat()
{
    return m_task_plat;
}

void TaskPlatImpl::DoLoopIterationEnds()
{
    if (!m_started)
        Sleep(2);
    else
        Yield();
}

}

}

}

