/*!
 * \file esys/evtloop/wx/taskmngr_wx.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_wx/esys_wx_prec.h"
#include "esys/evtloop/taskbase.h"
#include "esys/evtloop/wx/taskmngr.h"
#include "esys/evtloop/wx/taskmngrimpl.h"
#include "esys/evtloop/wx/taskplat.h"

namespace esys
{

namespace evtloop
{

namespace wx
{

TaskMngr::TaskMngr(): BaseType("wx::TaskMngr")
{
}

TaskMngr::~TaskMngr()
{
}

void TaskMngr::Run()
{
    BaseType::Run();

    /*! \todo start all tasks */
}

}

}

}

