/*!
 * \file esys/evtloop/wx/esys.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_wx/esys_wx_prec.h"
#include "esys/evtloop/wx/esys.h"
#include "esys/evtloop/taskbase.h"
#include "esys/evtloop/wx/taskplat.h"
#include "esys/evtloop/wx/taskmngr.h"
#include "esys/evtloop/wx/taskmngrimpl.h"
//#include "esys/evtloop/wx/mutex.h"

#include "esys/platform/id.h"

//#include <Windows.h>    //for the sleep

#include <wx/timer.h>

#include <boost/filesystem/path.hpp>

#ifdef DBG_LOG
#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>
#endif

#include <fstream>

namespace esys
{

namespace evtloop
{

namespace wx
{

class ESYS_WX_API ESysImpl
{
public:
    ESysImpl();
    virtual ~ESysImpl();

    void SetName(const char *name);
    const char *GetName();

    int32_t CreateLog();

private:
    std::ofstream *m_ofs;
    const char *m_name;
#ifdef DBG_LOG
    logmod::logger *m_log;
#endif

};

ESysImpl::ESysImpl()
    : m_ofs(NULL), m_name(NULL)
#ifdef DBG_LOG
    , m_log(NULL)
#endif
{
}

ESysImpl::~ESysImpl()
{
#ifdef DBG_LOG
    if (m_log!=NULL)
    {
        dbg_log::dbg_class::SetDefaultLogger(NULL);
        m_log->SetOfstream(NULL);
        delete m_log;
        m_log=NULL;
    }
    if (m_ofs!=NULL)
    {
        delete m_ofs;
        m_ofs=NULL;
    }
#endif
}

void ESysImpl::SetName(const char *name)
{
    m_name=name;
}

const char *ESysImpl::GetName()
{
    return m_name;
}

int32_t ESysImpl::CreateLog()
{
    std::string s;

    if (GetName()!=NULL)
        s.append(GetName());
    else
        s.append("esys");
    s.append(".log");

#ifdef DBG_LOG
    m_ofs=new std::ofstream(s.c_str());
    m_log=new logmod::logger();

    m_log->SetOfstream(m_ofs);
    dbg_log::dbg_class::SetDefaultLogger(m_log);

    dbg_log::dbg_class::Enable();
    return 0;
#else
    return -1;
#endif
}

uint64_t g_Millis()
{
    return wxGetLocalTimeMillis().GetValue();
}


uint64_t ESys::m_started_time=g_Millis();
ESys ESys::g_esys;

static ESysImpl g_ESysImpl;

ESys::ESys(): mp::ESysBase(ESYS_PLAT_MULTIOS_WX, "wxWidgets"), m_impl(g_ESysImpl)
{
    mp::ESysBase::SetElement(this);
}

ESys::~ESys()
{
}

ESys &ESys::Get()
{
    return g_esys;
}

void ESys::SetAppName(const char *app_name)
{
    m_impl.SetName(app_name);
}

int32_t ESys::CreateLog()
{
    return m_impl.CreateLog();
}

int32_t ESys::GetExeDir(std::string &exe_dir)
{
#ifdef WIN32
    wchar_t ownPth[MAX_PATH];

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(NULL);
    if (hModule != NULL)
    {
        // When passing NULL to GetModuleHandle, it returns handle of exe itself
        GetModuleFileName(hModule,ownPth, (sizeof(ownPth)));

        // Use above module handle to get the path using GetModuleFileName()
        //std::wcout << ownPth << std::endl ;
        ::boost::filesystem::path exe_path=std::wstring(ownPth);
        //std::wcout << exe_path.branch_path().wstring() << std::endl;
        exe_dir=exe_path.branch_path().string();
        return 0;
    }
    else
    {
        //std::wcout << "Module handle is NUL" << std::endl ;
        exe_dir="";
        return -1;
    }
#else
#endif
}

void ESys::Sleep(millis_t msec)
{
    TaskBase *task;

    task=TaskBase::Current();
    if (task!=NULL)
    {
        //task->Sleep(msec);
        return;
    }

    wxMilliSleep(msec);
}

millis_t ESys::Millis()
{
    TaskBase *task;

    task=TaskBase::Current();
    /*if (task!=NULL)
        return task->Millis(); */
    return g_Millis()-m_started_time;
}

void ESys::Init()
{
}

void ESys::Release()
{
}

#ifdef ESYS_MULTI_PLAT
void ESys::impSleep(millis_t msec)
{
    ESys::Sleep(msec);
}

millis_t ESys::impMillis()
{
    return ESys::Millis();
}

ESys &ESys::impGet()
{
    return ESys::Get();
}

void ESys::impInit()
{
    Init();
}

void ESys::impRelease()
{
    Release();
}

TaskPlatIf *ESys::impNewTaskPlat(const char *name)
{
    return new TaskPlat(name);
}

TaskMngrBase *ESys::impNewTaskMngrBase()
{
    return new TaskMngr();
}

MutexBase *ESys::impNewMutexBase(MutexBase::Type type)
{
//    return new Mutex(type);
    return nullptr;
}

#endif

}

}

}

