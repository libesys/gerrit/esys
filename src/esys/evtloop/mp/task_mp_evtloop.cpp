/*!
 * \file esys/evtloop/mp/task_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/mp/task.h"
#include "esys/evtloop/mp/esys.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace mp
{

Task::Task(const char *name)
    : TaskBase(name), TaskPlat(name), /*m_task_plat_if(NULL),*/ m_name(name)
{
    SetTaskBase(this);
    SetTaskPlat(this);
}

Task::~Task()
{
    if (m_task_plat_if!=NULL)
    {
        delete m_task_plat_if;
        m_task_plat_if=NULL;
    }
}

void Task::Init()
{
}

void Task::Setup()
{
}

void Task::InternalInit()
{
    GetTaskPlat();

    assert(m_task_plat_if!=NULL);
}

void Task::InternalCoreLoop()
{
}

TaskPlatIf *Task::GetTaskPlat()
{
    if (m_task_plat_if!=NULL)
        return m_task_plat_if;
    m_task_plat_if=ESys::NewTaskPlat(m_name);
    m_task_plat_if->SetTaskBase(this);
    SetTaskPlatIf(m_task_plat_if);

    return m_task_plat_if;
}

}

}

}

