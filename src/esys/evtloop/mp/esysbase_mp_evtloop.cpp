/*!
 * \file esys/evtloop/esysbase_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/time.h"
#include "esys/evtloop/mp/esysbase.h"

#ifndef ESYS_MULTI_PLAT

namespace esys
{

namespace evtloop
{

namespace mp
{

ESys::ESys(int_t platform_id, ESys *element)
{
}

void ESys::SetElement(ESys *element)
{
}

}

}

}

#else

#include "esys/platform/id.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace mp
{
/*int_t ESysBase::m_platform_id=-1;
ESysBase *ESysBase::m_esys=NULL;
platform::Id *ESysBase::m_platform=NULL;

void ESysBase::Sleep(millis_t msec)
{
    Get().impSleep(msec);
}

millis_t ESysBase::Millis()
{
    return Get().impMillis();
}

void ESysBase::Init()
{
    Get().impInit();
}

void ESysBase::Release()
{
    Get().impRelease();

    GetById(ESYS_PLAT_SYSC)->impRelease();
} */

ESysBase::ESysBase(int_t platform_id, const std::string &name): BaseType(platform_id), m_name(name)
{
}

ESysBase::~ESysBase()
{
}

const std::string &ESysBase::GetName()
{
    return m_name;
}

/*ESysBase &ESysBase::Get()
{
    if (m_esys==NULL)
        SetPlatform(ESYS_PLAT_MULTIOS_WX);

    assert(m_esys!=NULL);

    return *m_esys;
}

void ESysBase::SetAppName(const char *app_name)
{
}

int32_t ESysBase::CreateLog()
{
    return -1;
}

void ESysBase::FlushLog()
{
}

int32_t ESysBase::GetExeDir(std::string &exe_dir)
{
    return -1;
}

void ESysBase::SetPlatform(int platform_id)
{
    m_platform_id=platform_id;
    m_platform=platform::Id::Get(platform_id);
    m_esys=GetById(platform_id);
}

void ESysBase::SetPlatform(platform::Id &platform_id)
{
    m_platform=&platform_id;
    m_platform_id=platform_id.GetId();
    m_esys=GetById(platform_id);
} */

platform::Id *ESysBase::GetPlatformId()
{
    platform::Id *p_id;
    int_t id=GetId();

    p_id=platform::Id::Get(id);
    return p_id;
}

/*TaskPlatIf *ESysBase::NewTaskPlat(const char *name)
{
    return Get().impNewTaskPlat(name);
}

TaskMngrBase *ESysBase::NewTaskMngrBase()
{
    return Get().impNewTaskMngrBase();
}

MutexBase *ESysBase::NewMutexBase(MutexBase::Type type)
{
    return Get().impNewMutexBase(type);
} */

void ESysBase::impSleep(millis_t msec)
{
    esys::Time::Sleep(msec);
}

millis_t ESysBase::impMillis()
{
    return Time::Millis();
}

}

}

}

#endif


