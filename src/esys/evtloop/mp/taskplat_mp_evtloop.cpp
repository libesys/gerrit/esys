/*!
 * \file esys/evtloop/mp/taskplat_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/mp/taskplat.h"
#include "esys/evtloop/esys.h"
//#include "esys/sysc/taskplatimpl.h"

#include <stddef.h>     //Needed for NULL under GCC
#include <cassert>

namespace esys
{

namespace evtloop
{

namespace mp
{

/*TaskPlat *TaskPlat::Current()
{
    TaskPlatImpl *cur=TaskPlatImpl::GetCurrent();
    if (cur!=NULL)
        return cur->GetTaskPlat();
    return NULL;
}

TaskBase *TaskPlat::CurrentBase()
{
    TaskPlatImpl *cur=TaskPlatImpl::GetCurrent();

    if (cur!=NULL)
        return cur->GetTaskBase();
    return NULL;
} */

TaskPlat::TaskPlat(const char *name): m_task_mngr(NULL), m_task_plat_if(NULL)
{
    m_task_plat_if = ESys::Get().NewTaskPlat(name);
    //m_impl=new TaskPlatImpl(name);
    //m_impl->SetTaskPlat(this);
    //TaskBase::AddCurrentFct(&TaskPlat::CurrentBase);
}

TaskPlat::~TaskPlat()
{
    if (m_task_plat_if != nullptr)
    {
        delete m_task_plat_if;
        m_task_plat_if = nullptr;
    }
    /*if (m_impl!=NULL)
    {
        delete m_impl;
        m_impl=NULL;
    }*/
}

millis_t TaskPlat::Millis()
{
    assert(m_task_plat_if!=NULL);

    return m_task_plat_if->Millis();
}

void TaskPlat::Sleep(millis_t ms)
{
    assert(m_task_plat_if!=NULL);

    m_task_plat_if->Sleep(ms);
}

void TaskPlat::SleepU(micros_t us)
{
    assert(m_task_plat_if!=NULL);

    m_task_plat_if->Sleep(us);
}

void TaskPlat::Wait()
{
    assert(m_task_plat_if!=NULL);

    m_task_plat_if->Wait();
}

void TaskPlat::Signal()
{
    assert(m_task_plat_if!=NULL);

    m_task_plat_if->Wait();
}

void TaskPlat::DoLoopIterationEnds()
{
    assert(m_task_plat_if!=NULL);

    m_task_plat_if->DoLoopIterationEnds();
}

void TaskPlat::SetTaskBase(TaskBase *task_base)
{
    assert(m_task_plat_if!=NULL);

    m_task_plat_if->SetTaskBase(task_base);
}

void TaskPlat::NotifyExit()
{
    assert(m_task_plat_if!=NULL);

    m_task_plat_if->NotifyExit();
}

void TaskPlat::SetTaskMngrBase(TaskMngrBase *task_mngr)
{
    m_task_mngr=task_mngr;
}

/*void TaskPlat::SetTaskMngr(TaskMngr *task_mngr)
{
    m_task_mngr=task_mngr;
} */

TaskMngrBase *TaskPlat::GetTaskMngrBase()
{
    return m_task_mngr;
}

void TaskPlat::SetTaskPlatIf(TaskPlatIf *task_plat_if)
{
    m_task_plat_if=task_plat_if;
}

void TaskPlat::StartTask()
{
    assert(m_task_plat_if!=NULL);

    //m_task_plat_if->StartTask();
}

void TaskPlat::WaitTaskDone()
{
    assert(m_task_plat_if!=NULL);

    //m_task_plat_if->WaitTaskDone();
}

}

}

}

