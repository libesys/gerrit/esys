/*!
 * \file esys/evtloop/mp/taskmngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/mp/taskmngr.h"
#include "esys/evtloop/mp/esys.h"

namespace esys
{

namespace evtloop
{

namespace mp
{

TaskMngr::TaskMngr(): TaskMngrBase(), m_task_mngr(NULL)
{
}

TaskMngr::~TaskMngr()
{
    if (m_task_mngr!=NULL)
    {
        delete m_task_mngr;
        m_task_mngr=NULL;
    }
}

void TaskMngr::Init()
{
    Get()->Init();
}

void TaskMngr::Setup()
{
    Get()->Setup();
}

void TaskMngr::Run()
{
    Get()->Run();
}

void TaskMngr::AddTask(TaskBase *task)
{
    Get()->AddTask(task);
}

esys::evtloop::TaskMngrBase *TaskMngr::Get()
{
    if (m_task_mngr!=NULL)
        return m_task_mngr;

    m_task_mngr=ESys::NewTaskMngrBase();
    return m_task_mngr;
}

}

}

}

