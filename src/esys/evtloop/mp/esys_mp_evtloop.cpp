/*!
 * \file esys/evtloop/mp/esys.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/mp/esys.h"
#include "esys/evtloop/mp/taskplat.h"
#include "esys/evtloop/mp/taskmngr.h"
#include "esys/mp/esys.h"
#include "esys/platform/id.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace mp
{

int_t ESys::m_platform_id = -1;
ESysBase *ESys::m_esys = nullptr;
platform::Id *ESys::m_platform = nullptr;

ESys ESys::g_esys;

void ESys::Sleep(millis_t msec)
{
    Get().impSleep(msec);
}

millis_t ESys::Millis()
{
    return Get().impMillis();
}

void ESys::Init()
{
    Get().impInit();
}

void ESys::Release()
{
    Get().impRelease();

    // GetById(ESYS_PLAT_SYSC)->impRelease();
}

ESys::ESys()
    : esys::evtloop::ESysBase(ESYS_PLAT_MULTI_PLAT, "MultiPlatform")
{
    mp::ESysBase::SetElement(this);
}

ESys::~ESys()
{
}

ESys &ESys::Get()
{
    esys::mp::ESys::Get().LoadPlugins();

    return g_esys;
}

ESysBase &ESys::GetESysBase()
{
    if (m_esys == nullptr) SetPlatform(ESYS_PLAT_BOOST);

    assert(m_esys != nullptr);

    return *m_esys;
}

ESysBase *ESys::GetBase(std::size_t idx)
{
    esys::mp::ESys::Get().LoadPlugins();

    if (idx >= GetCount()) return nullptr;
    return g_esys.GetByIdx(idx);
}

void ESys::SetAppName(const char *app_name)
{
}

int32_t ESys::CreateLog()
{
    return -1;
}

void ESys::FlushLog()
{
}

int32_t ESys::GetExeDir(std::string &exe_dir)
{
    return -1;
}

void ESys::SetPlatform(int platform_id)
{
    m_platform_id = platform_id;
    m_platform = platform::Id::Get(platform_id);
    m_esys = GetById(platform_id);
}

void ESys::SetPlatform(platform::Id &platform_id)
{
    if (platform_id != esys::platform::MULTI_PLAT)
    {
        m_platform = &platform_id;
        m_platform_id = platform_id.GetId();
        m_esys = GetById(platform_id);
    }
    else
    {
        m_platform = nullptr;
        m_platform_id = -1;
        m_esys = nullptr;
    }
}

platform::Id *ESys::GetPlatformId()
{
    platform::Id *p_id;
    int_t id = GetId();

    p_id = platform::Id::Get(id);
    return p_id;
}

TaskPlatIf *ESys::NewTaskPlat(const char *name)
{
    return Get().impNewTaskPlat(name);
}

esys::evtloop::TaskMngrBase *ESys::NewTaskMngrBase()
{
    return Get().impNewTaskMngrBase();
}

MutexBase *ESys::NewMutexBase(MutexBase::Type type)
{
    return Get().impNewMutexBase(type);
}

void ESys::impSleep(millis_t msec)
{
    // wait(sc_time((double)msec, SC_MS));
    GetESysBase().impSleep(msec);
}

millis_t ESys::impMillis()
{
    return GetESysBase().impMillis();
}

ESys &ESys::impGet()
{
    return g_esys;
}

void ESys::impInit()
{
    GetESysBase().impInit();
}

void ESys::impRelease()
{
    GetESysBase().impRelease();
}

TaskPlatIf *ESys::impNewTaskPlat(const char *name)
{
    // return new TaskPlat(name);
    return GetESysBase().impNewTaskPlat(name);
}

esys::evtloop::TaskMngrBase *ESys::impNewTaskMngrBase()
{
    // return new TaskMngr();
    return GetESysBase().impNewTaskMngrBase();
}

MutexBase *ESys::impNewMutexBase(MutexBase::Type type)
{
    return nullptr;
}

} // namespace mp

} // namespace evtloop

} // namespace esys
