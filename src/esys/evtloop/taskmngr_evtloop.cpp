/*!
 * \file esys/evtloop/taskmngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/taskmngr.h"
#include "esys/evtloop/esys.h"

#if !defined(ESYS_MULTI_PLAT) && defined(ESYS_USE_SYSC)

#elif defined(ESYS_MULTI_PLAT)

#else

namespace esys
{

namespace evtloop
{

TaskMngr::TaskMngr(): TaskMngrBase()
{
}

TaskMngr::~TaskMngr()
{
}

}

}

#endif

