/*!
 * \file esys/evtloop/taskmngrbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/taskmngrbase.h"
#include "esys/evtloop/taskbase.h"

#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

namespace evtloop
{

TaskMngrBase::TaskMngrBase(): m_last_task(NULL), m_task_count(0)
{
}

TaskMngrBase::~TaskMngrBase()
{
}

void TaskMngrBase::Init()
{
}

void TaskMngrBase::Release()
{
}

void TaskMngrBase::Setup()
{
}

void TaskMngrBase::Run()
{
    TaskBase *task;

    Init();

    task=m_last_task;

    while (task!=NULL)
    {
        task->Inits();
        task=task->GetPrevTask();
    }

    Setup();

    task=m_last_task;

    while (task!=NULL)
    {
        task->Setups();
        task=task->GetPrevTask();
    }
}

void TaskMngrBase::AddTask(TaskBase *task)
{
    task->SetPrevTask(m_last_task);
    m_last_task=task;
    m_task_count++;
}

}

}

