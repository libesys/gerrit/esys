/*!
 * \file esys/evtloop/ard/task_ard_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/evtloop/ard/task.h"

//Arduino header
#include <Arduino.h>
//#include "wiring.h"//
//#include "USBAPI.h"

namespace esys
{

namespace evtloop
{

namespace ard
{

Task::Task(const char *name): TaskBase(name), TaskPlat(name)
{
    SetTaskPlat(this);
}

Task::~Task()
{
}

millis_t Task::Millis()
{
    return ::millis();
}

void Task::Init()
{
    init();

#if defined(USBCON)
    USBDevice.attach();
#endif

}

void Task::SerialEventRun()
{
}

void Task::InternalCoreLoop()
{
    SerialEventRun();
}

}

}

}
