/*!
 * \file esys/evtloop/ard/esys_ard.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"
#include "esys/evtloop/ard/esys.h"

#include "esys/platform/id.h"
#include "esys/evtloop/ard/task.h"
#include "esys/evtloop/ard/esys.h"

#include <Arduino.h>

//#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

namespace evtloop
{

namespace ard
{

class ESYS_API ESysImpl
{
public:
    ESysImpl();
    virtual ~ESysImpl();

    void SetName(const char *name);
    const char *GetName();

    int32_t CreateLog();

private:
    //std::ofstream *m_ofs;
    //logmod::logger *m_log;
    const char *m_name;
};

ESysImpl::ESysImpl()
    : /*m_log(NULL),*/ m_name(NULL)
{
}

ESysImpl::~ESysImpl()
{
    /*if (m_log!=NULL)
    {
        dbg_log::dbg_class::SetDefaultLogger(NULL);
        m_log->SetOfstream(NULL);
        delete m_log;
        m_log=NULL;
    }
    if (m_ofs!=NULL)
    {
        delete m_ofs;
        m_ofs=NULL;
    } */
}

void ESysImpl::SetName(const char *name)
{
    m_name=name;
}

const char *ESysImpl::GetName()
{
    return m_name;
}

int32_t ESysImpl::CreateLog()
{
    /*    std::string s;

        if (GetName()!=NULL)
            s.append(GetName());
        else
            s.append("esys");
        s.append(".log");
        m_ofs=new std::ofstream(s.c_str());
        m_log=new logmod::logger();

        m_log->SetOfstream(m_ofs);
        dbg_log::dbg_class::SetDefaultLogger(m_log);

        dbg_log::dbg_class::Enable(); */
    return 0;
}

uint64_t g_Millis()
{
    return ::millis();
}


ESys::ESys(): esys::ESys(ESYS_PLAT_ARDUINO)//, m_impl(g_ESysImpl)
{
    esys::ESys::SetElement(this);
}

ESys::~ESys()
{
}

/*ESys &ESys::Get()
{
    return g_esys;
} */

/*void ESys::SetAppName(const char *app_name)
{
    m_impl.SetName(app_name);
}

int32_t ESys::CreateLog()
{
    return m_impl.CreateLog();
} */

/*int32_t ESys::GetExeDir(std::wstring &exe_dir)
{
} */

void ESys::Sleep(uint16_t msec)
{
    ::delay(msec);
}

millis_t ESys::Millis()
{
    return ::millis();
}

/*void ESys::Init()
{
}

void ESys::Release()
{
} */

}

}


