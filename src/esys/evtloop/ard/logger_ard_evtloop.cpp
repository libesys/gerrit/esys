/*!
 * \file esys/evtloop/ard/logger_ard_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/evtloop/ard/logger.h"

namespace esys
{

Logger::Logger():LoggerBase(),m_stream(0)
{
}

Logger::~Logger()
{

}

void Logger::SetStream(::Stream *stream)
{
    m_stream=stream;
}

LoggerBase& Logger::operator<< (uint8_t val)
{
    if ((m_stream!=0) && (m_enable==true))
        m_stream->print(val);
    return *this;
}

LoggerBase& Logger::operator<< (int8_t val)
{
    if ((m_stream!=0) && (m_enable==true))
        m_stream->print(val);
    return *this;
}

LoggerBase& Logger::operator<< (uint16_t val)
{
    if ((m_stream!=0) && (m_enable==true))
        m_stream->print(val);
    return *this;
}

LoggerBase& Logger::operator<< (int16_t val)
{
    if ((m_stream!=0) && (m_enable==true))
        m_stream->print(val);
    return *this;
}

LoggerBase& Logger::operator<< (int8_t *string)
{
    if ((m_stream!=0) && (m_enable==true))
        m_stream->write((char *)string);
    return *this;
}

LoggerBase& Logger::operator<< (const char *string)
{
    if ((m_stream!=0) && (m_enable==true))
        m_stream->write(string);
    return *this;
}

int16_t Logger::Write(uint8_t *buf, int16_t size)
{
    if ((m_stream==0) || (m_enable==false))
        return -1;
    return m_stream->write(buf,size);
}

}

