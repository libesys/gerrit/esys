/*!
 * \file esys/evtloop/boost/taskmngr_boost_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/taskbase.h"
#include "esys/evtloop/boost/taskmngr.h"
#include "esys/evtloop/boost/taskmngrimpl.h"
#include "esys/evtloop/boost/taskplat.h"

namespace esys
{

namespace evtloop
{

namespace boost
{

TaskMngr::TaskMngr(): BaseType("wx::TaskMngr")
{
}

TaskMngr::~TaskMngr()
{
}

void TaskMngr::Run()
{
    BaseType::Run();

    /*! \todo start all tasks */
}

}

}

}

