/*!
 * \file esys/evtloop/boost/taskplat_boost_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/evtloop/boost/taskplat.h"
#include "esys/evtloop/boost/taskplatimpl.h"

namespace esys
{

namespace evtloop
{

namespace boost
{

TaskPlat *TaskPlat::Current()
{
    TaskPlatImpl *cur=TaskPlatImpl::GetCurrent();
    if (cur != nullptr)
        return cur->GetTaskPlat();
    return nullptr;
}

TaskBase *TaskPlat::CurrentBase()
{
    TaskPlatImpl *cur=TaskPlatImpl::GetCurrent();

    if (cur != nullptr)
        return cur->GetTaskBase();
    return nullptr;
}

TaskPlat::TaskPlat(const char *name)
{
    m_impl=new TaskPlatImpl();
    m_impl->SetTaskPlat(this);

    /*! \todo calling TaskBase::AddCurrentFct shoudn't be done here, but
     *  once when the platform is initialized.
     */
    TaskBase::AddCurrentFct(&TaskPlat::CurrentBase);
}

TaskPlat::~TaskPlat()
{
    delete m_impl;
}

millis_t TaskPlat::Millis()
{
    return m_impl->Millis();
}

void TaskPlat::Sleep(millis_t val)
{
    m_impl->Sleep(val);
}

void TaskPlat::SleepU(micros_t val)
{
    m_impl->SleepU(val);
}

void TaskPlat::Wait()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void TaskPlat::Signal()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void TaskPlat::SetTaskBase(TaskBase *task_base)
{
    assert(m_impl!=NULL);

    m_impl->SetTaskBase(task_base);
}

void TaskPlat::SetTaskMngrBase(TaskMngrBase *task_mngr_base)
{

}

void TaskPlat::DoLoopIterationEnds()
{
    assert(m_impl!=NULL);

    m_impl->DoLoopIterationEnds();
}

void TaskPlat::NotifyExit()
{
}

}

}

}

