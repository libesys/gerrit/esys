/*!
 * \file esys/evtloop/boost/esys_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/boost/esys.h"
#include "esys/evtloop/boost/taskplat.h"
#include "esys/evtloop/boost/taskmngr.h"
#include "esys/evtloop/boost/taskmngrimpl.h"

#include "esys/platform/id.h"
#include "esys/evtloop/task.h"

#include <Windows.h> //for the sleep

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem/path.hpp>

#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>

#include <fstream>

namespace esys
{

namespace evtloop
{

namespace boost
{

class ESYS_API ESysImpl
{
public:
    ESysImpl();
    virtual ~ESysImpl();

    void SetName(const char *name);
    const char *GetName();

    int32_t CreateLog();

private:
    std::ofstream *m_ofs;
    logmod::logger *m_log;
    const char *m_name;
};

ESysImpl::ESysImpl()
    : m_ofs(nullptr)
    , m_log(nullptr)
    , m_name(nullptr)
{
}

ESysImpl::~ESysImpl()
{
    if (m_log != nullptr)
    {
        dbg_log::dbg_class::SetDefaultLogger(nullptr);
        m_log->SetOfstream(nullptr);
        delete m_log;
        m_log = nullptr;
    }
    if (m_ofs != nullptr)
    {
        delete m_ofs;
        m_ofs = nullptr;
    }
}

void ESysImpl::SetName(const char *name)
{
    m_name = name;
}

const char *ESysImpl::GetName()
{
    return m_name;
}

int32_t ESysImpl::CreateLog()
{
    std::string s;

    if (GetName() != nullptr)
        s.append(GetName());
    else
        s.append("esys");
    s.append(".log");
    m_ofs = new std::ofstream(s.c_str());
    m_log = new logmod::logger();

    m_log->SetOfstream(m_ofs);
    dbg_log::dbg_class::SetDefaultLogger(m_log);

    dbg_log::dbg_class::Enable();
    return 0;
}

::boost::posix_time::ptime g_boost_started_time = ::boost::posix_time::microsec_clock::local_time();

uint64_t g_Millis()
{
    ::boost::posix_time::ptime now = ::boost::posix_time::microsec_clock::local_time();

    ::boost::posix_time::time_duration diff = now - g_boost_started_time;
    // std::cout << "g_Millis = " << diff.total_milliseconds() << " ms" << std::endl;
    return diff.total_milliseconds();
    // return 0;
}

uint64_t ESys::m_started_time = g_Millis();
ESys ESys::g_esys;

static ESysImpl g_ESysImpl;

ESys::ESys()
    : mp::ESysBase(ESYS_PLAT_BOOST, "Boost")
    , m_impl(g_ESysImpl)
{
    esys::evtloop::mp::ESysBase::SetElement(this);
}

ESys::~ESys()
{
}

ESys &ESys::Get()
{
    return g_esys;
}

void ESys::SetAppName(const char *app_name)
{
    m_impl.SetName(app_name);
}

int32_t ESys::CreateLog()
{
    return m_impl.CreateLog();
}

int32_t ESys::GetExeDir(std::string &exe_dir)
{
#ifdef WIN32
    char ownPth[MAX_PATH];

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(nullptr);
    if (hModule != nullptr)
    {
        // When passing nullptr to GetModuleHandle, it returns handle of exe itself
        GetModuleFileName(hModule, ownPth, (sizeof(ownPth)));

        // Use above module handle to get the path using GetModuleFileName()
        // std::wcout << ownPth << std::endl ;
        ::boost::filesystem::path exe_path = std::string(ownPth);
        // std::wcout << exe_path.branch_path().wstring() << std::endl;
        exe_dir = exe_path.branch_path().string();
        return 0;
    }
    else
    {
        // std::wcout << L"Module handle is nullptr" << std::endl ;
        exe_dir = "";
        return -1;
    }
#else
#endif
}

void ESys::Sleep(uint16_t msec)
{
    TaskBase *task;

    task = TaskBase::Current();
    if (task != nullptr)
    {
        task->Sleep(msec);
        return;
    }

    ::boost::this_thread::sleep(::boost::posix_time::millisec(msec));
}

millis_t ESys::Millis()
{
    TaskBase *task;

    task = TaskBase::Current();
    if (task != nullptr) return task->Millis();
    return g_Millis() - m_started_time;
}

void ESys::Init()
{
}

void ESys::Release()
{
}

#ifdef ESYS_MULTI_PLAT
void ESys::impSleep(millis_t msec)
{
    ESys::Sleep(msec);
}

millis_t ESys::impMillis()
{
    return ESys::Millis();
}

ESys &ESys::impGet()
{
    return ESys::Get();
}

void ESys::impInit()
{
    Init();
}

void ESys::impRelease()
{
    Release();
}

MutexBase *ESys::impNewMutexBase(MutexBase::Type type)
{
    return nullptr;
}

esys::evtloop::TaskPlatIf *ESys::impNewTaskPlat(const char *name)
{
    return new TaskPlat(name);
}

esys::evtloop::TaskMngrBase *ESys::impNewTaskMngrBase()
{
    return new TaskMngr();
}

#endif

} // namespace boost

} // namespace evtloop

} // namespace esys
