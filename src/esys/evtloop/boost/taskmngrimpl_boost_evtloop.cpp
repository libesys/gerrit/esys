/*!
 * \file esys/evtloop/boost/taskmngrimpl_boost_evtlopp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/boost/taskmngrimpl.h"
#include "esys/evtloop/boost/taskmngr.h"
#include "esys/evtloop/boost/taskplat.h"

#include <cassert>

namespace esys
{

namespace evtloop
{

namespace boost
{

TaskMngrImpl::TaskMngrImpl(const char *name): m_task_mngr(nullptr)
{
}

TaskMngrImpl::~TaskMngrImpl()
{
}

void TaskMngrImpl::SetTaskMngr(TaskMngrBase *task_mngr)
{
    m_task_mngr = dynamic_cast<TaskMngr *>(task_mngr);
}

void TaskMngrImpl::SetTaskMngr(TaskMngr *task)
{
    m_task_mngr = task;
}

TaskMngr *TaskMngrImpl::GetTaskMngr()
{
    return m_task_mngr;
}

void TaskMngrImpl::Main()
{
}

void TaskMngrImpl::WaitInitSetupDone()
{
}

void TaskMngrImpl::SignalTaskDone()
{
}

}

}

}

