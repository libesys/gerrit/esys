/*!
 * \file esys/evtloop/boost/taskmngrimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include "esys/evtloop/taskbase.h"

namespace esys
{

namespace evtloop
{

namespace boost
{

class ESYS_API TaskMngr;

class ESYS_API TaskMngrImpl
{
public:
    friend class ESYS_API TaskMngr;

    TaskMngrImpl(const char *name);
    virtual ~TaskMngrImpl();

    void SetTaskMngr(TaskMngrBase *task);
    void SetTaskMngr(TaskMngr *task);
    TaskMngr *GetTaskMngr();

    void WaitInitSetupDone();
    void SignalTaskDone();

    void Main();

protected:
    TaskMngr *m_task_mngr;
    bool m_started;
};

}

}

}

