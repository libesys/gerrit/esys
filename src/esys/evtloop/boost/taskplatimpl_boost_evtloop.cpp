/*!
 * \file esys/evtloop/boost/taskplatimpl_boost_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/boost/taskplatimpl.h"
#include "esys/evtloop/boost/taskplat.h"
#include "esys/time.h"

#include <typeinfo>

namespace esys
{

namespace evtloop
{

namespace boost
{

TaskPlatImpl *TaskPlatImpl::GetCurrent()
{
    TaskPlatImpl *cur = nullptr;
    /*wxThread *self;

    self = wxThread::This();
    if (self == nullptr)
        return nullptr;
    try
    {
        cur = dynamic_cast<TaskPlatImpl*>(self);
    }
    catch (std::bad_cast &)
    {
        return nullptr;
    } */
    return cur;
}

TaskPlatImpl::TaskPlatImpl()
    : m_task_plat(nullptr), m_task_base(nullptr), m_sem("Sem", 0), m_started(false)
{
}

TaskPlatImpl::~TaskPlatImpl()
{
}

millis_t TaskPlatImpl::Millis()
{
    return Time::Millis();
}

void TaskPlatImpl::Wait()
{
    m_sem.Wait();
}

void TaskPlatImpl::Signal()
{
    m_sem.Post();
}

/*wxThread::ExitCode TaskPlatImpl::Entry()
{
    assert(m_task_base != nullptr);

    m_task_base->Do();

    return (wxThread::ExitCode)m_task_base->GetExit();
} */

void TaskPlatImpl::Sleep(millis_t ms)
{
    Time::Sleep(ms);
}

void TaskPlatImpl::SleepU(micros_t us)
{
    Time::USleep(us);
}

void TaskPlatImpl::SetTaskBase(TaskBase *task_base)
{
    m_task_base = task_base;
}

TaskBase *TaskPlatImpl::GetTaskBase()
{
    return m_task_base;
}

void TaskPlatImpl::SetTaskPlat(TaskPlat *task_plat)
{
    m_task_plat = task_plat;
}

TaskPlat *TaskPlatImpl::GetTaskPlat()
{
    return m_task_plat;
}

void TaskPlatImpl::DoLoopIterationEnds()
{
    if (!m_started)
        Sleep(2);
    else
        Yield();
}

}

}

}

