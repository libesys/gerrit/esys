/*!
 * \file esys/evtloop/stm32/task.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/evtloop/stm32/task.h"

namespace esys
{

namespace evtloop
{

namespace stm32
{

Task::Task(const char *name): TaskBase(name), TaskPlat(name)
{
    SetTaskPlat(this);
}

Task::~Task()
{
}


void Task::Init()
{

}

void Task::SerialEventRun()
{
}

void Task::InternalCoreLoop()
{

}

}

}

}
