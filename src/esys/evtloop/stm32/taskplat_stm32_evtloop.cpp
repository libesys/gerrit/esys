/*!
 * \file esys/evtloop/stm32/taskplat_stm32_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/evtloop/stm32/taskplat.h"

#include "esys/time.h"

namespace esys
{

namespace evtloop
{

namespace stm32
{

TaskPlat *TaskPlat::Current()
{
    return nullptr;
}

TaskBase *TaskPlat::CurrentBase()
{
    return nullptr;
}

TaskPlat::TaskPlat(const char *name) : TaskPlatIf()
{
}

TaskPlat::~TaskPlat()
{
}


millis_t TaskPlat::Millis()
{
    return esys::Time::Millis();
}

void TaskPlat::Sleep(millis_t ms)
{
    esys::Time::Sleep(ms);
}

void TaskPlat::SleepU(micros_t us)
{
    esys::Time::USleep(us);
}

void TaskPlat::DoLoopIterationEnds()
{
}

void TaskPlat::Wait()
{
}

void TaskPlat::Signal()
{
}

void TaskPlat::SetTaskBase(TaskBase *task_base)
{
}

void TaskPlat::SetTaskMngrBase(TaskMngrBase *task_mngr_base)
{
}

void TaskPlat::NotifyExit()
{
}


TaskMngrBase *TaskPlat::GetTaskMngrBase()
{
    return m_task_mngr;
}


void TaskPlat::StartTask()
{
}

void TaskPlat::WaitTaskDone()
{
}

}

}

}


