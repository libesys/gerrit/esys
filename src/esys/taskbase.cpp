/*!
 * \file esys/taskbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/taskbase.h"
#include "esys/tasklist.h"
#include "esys/taskmngrbase.h"

#include "esys/assert.h"

namespace esys
{

TaskList TaskBase::s_task_list;

TaskBase::TaskBase(const ObjectName &name, TaskType type)
    : ObjectNode(name), m_task_mngr_base(nullptr), m_exit_value(-1), m_runtime_lvl(RuntimeLevel::ESYS)
    , m_sem("TaskBaseSem", 0), m_type(type), m_next_task(nullptr), m_state(State::INSTANCIATED)
    , m_stack_size(DEFAULT_STACK_SIZE), m_priority(LOWEST2)
{
    TaskList::GetCurrent().Add(this);
    if (type == TaskType::APPLICATION)
        SetRuntimeLevel(RuntimeLevel::APP);
}

TaskBase::~TaskBase()
{
}

void TaskBase::SetType(TaskType typ)
{
    m_type = typ;
}

TaskType TaskBase::GetType()
{
    return m_type;
}

void TaskBase::SetState(TaskBase::State state)
{
    m_state = state;
    //if (state == State::STOPPING)
    //    m_sem.Post();
}

TaskBase::State TaskBase::GetState()
{
    return m_state;
}

void TaskBase::SetNextTask(TaskBase *next_task)
{
    m_next_task = next_task;
}

TaskBase *TaskBase::GetNextTask()
{
    return m_next_task;
}

int32_t TaskBase::StaticInit(Order order)
{
    /*if (order == BEFORE_CHILDREN)
        TaskList::GetCurrent().Add(this); */
    return 0;
}

int32_t TaskBase::RuntimeInit(RuntimeLevel runtime_lvl, Order order)
{
    if ((order == AFTER_CHILDREN) && (GetRuntimeLevel() == runtime_lvl))
        m_sem.Post();
    return 0;
}

void TaskBase::WaitRuntimeInitDone()
{
    assert(GetRuntimeLevel() != RuntimeLevel::NOT_SET);

    // If the runtime level is 0, the Task shouldn't wait and just start
    // execution since part of the core of the initialization. Typically, only one
    // Task should have the runtime level 0, and it should be the Task running the
    // actual runtime initialization phase.
    if (GetRuntimeLevel() > RuntimeLevel::ROOT)
        m_sem.Wait();
}

void TaskBase::SetExitValue(int32_t exit_value)
{
    m_exit_value = exit_value;
}

int32_t TaskBase::GetExitValue()
{
    return m_exit_value;
}

void TaskBase::SetMngrBase(TaskMngrBase *task_mngr_base)
{
    m_task_mngr_base = task_mngr_base;
}

TaskMngrBase *TaskBase::GetMngrBase()
{
    return m_task_mngr_base;
}

void TaskBase::SetRuntimeLevel(RuntimeLevel runtime_lvl)
{
    m_runtime_lvl = runtime_lvl;
}

RuntimeLevel TaskBase::GetRuntimeLevel()
{
    return m_runtime_lvl;
}

int32_t TaskBase::Started()
{
    assert(m_task_mngr_base != nullptr);

    m_task_mngr_base->TaskStarted(this);
    return 0;
}

int32_t TaskBase::Done()
{
    if (GetType() == TaskType::INTERNAL)
        return 0;
    assert(m_task_mngr_base != nullptr);

    m_task_mngr_base->TaskDone(this);
    return 0;
}

int32_t TaskBase::StopTask()
{
    SetState(State::STOPPING);
    m_sem.Post();
    return Stop();
}

uint32_t TaskBase::GetStackSize()
{
    return m_stack_size;
}

void TaskBase::SetStackSize(uint32_t stack_size)
{
    m_stack_size = stack_size;
}

TaskBase::Priority TaskBase::GetPriority()
{
    return m_priority;
}

void TaskBase::SetPriority(Priority priority)
{
    m_priority = priority;
}

}





