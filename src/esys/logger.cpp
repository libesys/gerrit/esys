/*!
 * \file esys/logger.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/logger.h"

#include <stdio.h>
#include <cstring>

namespace esys
{

Logger *Logger::g_logger = nullptr;

Logger &Logger::Get()
{
    assert(g_logger != nullptr);

    return *g_logger;
}

void Logger::Set(Logger *logger)
{
    g_logger = logger;
}

Logger::Logger(const ObjectName &name)
    : ObjectNode(name)
{
    strcpy(m_float_format, "%.3F");
}

Logger::~Logger()
{
}

void Logger::TransLock()
{
    m_trans_mutex.Lock();
}

void Logger::TransUnlock()
{
    m_trans_mutex.UnLock();
}

void Logger::PushTransactionChannel(int8_t ch)
{
    assert(ch < m_count);

    m_saved_ch = m_ch;
    m_ch = ch;
}

void Logger::PopTransactionChannel()
{
    m_ch = m_saved_ch;
}

bool Logger::DoLog()
{
    return (m_cur_level >= m_threshold_level);
}

void Logger::SetCurLevel(Level cur_level)
{
    m_cur_level = cur_level;
}

Logger::Level Logger::GetCurLevel()
{
    return m_cur_level;
}

void Logger::SetThresholdLevel(Level threshold_level)
{
    m_threshold_level = threshold_level;
}

Logger::Level Logger::GetThresholdLevel()
{
    return m_threshold_level;
}

void Logger::SaveLevel()
{
    m_saved_level = m_cur_level;
}

void Logger::LoadLevel()
{
    m_cur_level = m_saved_level;
}

void Logger::SetHex(bool hex)
{
    m_hex = hex;
}

bool Logger::GetHex()
{
    return m_hex;
}

uint8_t Logger::GetCount()
{
    return m_count;
}

int8_t Logger::GetChannel()
{
    return m_ch;
}

void Logger::SetChannel(int8_t ch)
{
    m_ch = ch;
}

void Logger::SetActive(bool active, int8_t idx)
{
    uint8_t ch_idx;

    if (idx == -1)
    {
        for (ch_idx = 0; ch_idx < m_count; ++ch_idx)
        {
            GetChannel(ch_idx).SetActive(active);
        }
    }
    else
    {
        GetChannel(idx).SetActive(active);
    }
}

void Logger::SetEnable(bool enable, int8_t idx)
{
    uint8_t ch_idx;

    if (idx == -1)
    {
        for (ch_idx = 0; ch_idx < m_count; ++ch_idx)
        {
            GetChannel(ch_idx).SetEnable(enable);
        }
    }
    else
    {
        GetChannel(idx).SetEnable(enable);
    }
}

Logger& Logger::operator<< (Logger& (*op)(Logger&))
{
    return (*op)(*this);
}

int32_t Logger::StaticInitBeforeChildren()
{
    int32_t result = ObjectNode::StaticInitBeforeChildren();

    if (result < 0)
        return result;
    // Add this point all children of this ObjectInit, i.e. the Mutex, should be initialized,
    // making it safe to use the Mutex
    m_init = true;
    return 0;
}

int32_t Logger::StaticReleaseAfterChildren()
{
    // Disable the usage of the Mutex before to delete the Mutex, so we are certain that
    // the Mutex will not be called while deleting it or after it was deleted
    m_init = false;

    // Now , it's safe to release the Mutex, since it won't be used anymore
    return ObjectNode::StaticReleaseAfterChildren();
}

Depth::Depth(int16_t depth) : m_depth(depth)
{
}

Depth::~Depth()
{
}

void Depth::Print(Logger &log) const
{
    int16_t count;

    if (log.DoLog() == false)
        return;

    for (count = 0; count < m_depth; ++count)
        log << "  ";
}

Str::Str(char *buf, uint16_t size): m_buf(buf), m_size(size)
{
}

Str::~Str()
{
}

void Str::Print(Logger &log) const
{
    log.PutStrN(m_buf, m_size, log.GetChannel());
}


ESYS_API Logger &begin(Logger &log)
{
    log.TransLock();
    return log;
}

ESYS_API Logger &end(Logger &log)
{
    log.PopTransactionChannel();
    log.TransUnlock();
    return log;
}

ESYS_API Logger &endl(Logger &log)
{
    log.PutStr("\n", log.GetChannel());
    return log;
}

ESYS_API Logger &hex(Logger &log)
{
    if (log.DoLog() == false)
        return log;
    log.SetHex(true);
    return log;
}

ESYS_API Logger &dec(Logger &log)
{
    if (log.DoLog() == false)
        return log;
    log.SetHex(false);
    return log;
}

ESYS_API Logger& operator<< (Logger& log, uint8_t value)
{
    char buf[4];
    unsigned int val = value;

    if (log.DoLog() == false)
        return log;

    if (log.GetHex() == true)
        snprintf(buf, sizeof(buf), "%02X", val);
    else
        snprintf(buf, sizeof(buf), "%u", val);
    log.PutStr(buf, log.GetChannel());

    return log;
}

ESYS_API Logger& operator<< (Logger& log, uint16_t value)
{
    char buf[6];
    unsigned int val = value;

    if (log.DoLog() == false)
        return log;

    if (log.GetHex() == true)
        snprintf(buf, sizeof(buf), "%04X", val);
    else
        snprintf(buf, sizeof(buf), "%u", val);
    log.PutStr(buf, log.GetChannel());
    return log;
}

ESYS_API Logger& operator<< (Logger& log, uint32_t value)
{
    char buf[11];
    unsigned int val = value;

    if (log.DoLog() == false)
        return log;

    if (log.GetHex() == true)
        snprintf(buf, sizeof(buf), "%08X", val);
    else
        snprintf(buf, sizeof(buf), "%u", val);
    log.PutStr(buf, log.GetChannel());
    return log;
}

/*Logger& operator<< (Logger& log, int8_t value)
{
    char buf[5];
    int val = value;

    if (log.hex() == true)
        snprintf(buf, sizeof(buf), "%02X", val);
    else
        snprintf(buf, sizeof(buf), "%d", val);
    log.put_str(buf, log.channel());
    return log;
}*/

ESYS_API Logger& operator<< (Logger& log, int16_t value)
{
    char buf[7];
    int val = value;

    if (log.DoLog() == false)
        return log;

    if (log.GetHex() == true)
        snprintf(buf, sizeof(buf), "%04X", val);
    else
        snprintf(buf, sizeof(buf), "%d", val);
    log.PutStr(buf, log.GetChannel());
    return log;
}

ESYS_API Logger& operator<< (Logger& log, int32_t value)
{
    char buf[12];
    int val = value;

    if (log.DoLog() == false)
        return log;

    if (log.GetHex() == true)
        snprintf(buf, sizeof(buf), "%08X", val);
    else
        snprintf(buf, sizeof(buf), "%d", val);
    log.PutStr(buf, log.GetChannel());
    return log;
}

ESYS_API Logger& operator<< (Logger& log, char letter)
{
    log.PutStrN(&letter, 1, log.GetChannel());
    return log;
}

ESYS_API Logger& operator<< (Logger& log, const char *str)
{
    log.PutStr(str, log.GetChannel());
    return log;
}

ESYS_API Logger& operator<< (Logger& log, const Depth &depth)
{
    if (log.DoLog() == false)
        return log;

    depth.Print(log);
    return log;
}

ESYS_API Logger& operator<< (Logger& log, const Str &str)
{
    if (log.DoLog() == false)
        return log;

    str.Print(log);
    return log;
}

ESYS_API Logger& operator<< (Logger& log, float value)
{
    if (log.DoLog() == false)
        return log;

    char value_[20];

    snprintf(value_, 20, log.m_float_format, value);
    log << value_;
    return log;
}


ESYS_API Logger &fixed(Logger &log)
{
    if (log.DoLog() == false)
        return log;

    log.m_float_scientific = false;

    snprintf(log.m_float_format, 10, "%%.%iF", log.m_float_precision);
    return log;
}

ESYS_API Logger &scientific(Logger &log)
{
    if (log.DoLog() == false)
        return log;

    log.m_float_scientific = true;

    snprintf(log.m_float_format, 10, "%%.%iE", log.m_float_precision);
    return log;
}

ESYS_API void set_precision(Logger &log, esys::uint32_t precision)
{
    if (log.DoLog() == false)
        return;

    log.m_float_precision = precision;

    if (log.m_float_scientific==true)
        snprintf(log.m_float_format, 10, "%%.%iE", precision);
    else
        snprintf(log.m_float_format, 10, "%%.%iF", precision);
}


ESYS_API Manip<esys::uint32_t> setprecision(esys::uint32_t precision)
{
    Manip<esys::uint32_t> result(set_precision, precision);

    return result;
}

ESYS_API void set_channel(Logger &log, esys::int32_t channel)
{

    log.PushTransactionChannel(channel);
}

ESYS_API Manip<esys::int32_t> set_channel(esys::int32_t channel)
{
    Manip<esys::int32_t> result(set_channel, channel);

    return result;
}

/*ESYS_API Logger &setprecision(esys::uint32_t precision)
{
    snprintf(log.m_float_format, 10, "%%.%iE", precision);
}*/

ESYS_API Logger &debug(Logger &log)
{
    log.SetCurLevel(Logger::LOG_DEBUG);
    return log;
}

ESYS_API Logger &info(Logger &log)
{
    log.SetCurLevel(Logger::LOG_INFO);
    return log;
}

ESYS_API Logger &warn(Logger &log)
{
    log.SetCurLevel(Logger::LOG_WARN);
    return log;
}

ESYS_API Logger &critical(Logger &log)
{
    log.SetCurLevel(Logger::LOG_CRITICAL);
    return log;
}

ESYS_API Logger &fatal(Logger &log)
{
    log.SetCurLevel(Logger::LOG_FATAL);
    return log;
}

ESYS_API Logger &save_level(Logger &log)
{
    log.SaveLevel();
    return log;
}

ESYS_API Logger &load_level(Logger &log)
{
    log.LoadLevel();
    return log;
}

}


