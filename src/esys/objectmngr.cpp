/*!
 * \file esys/objectmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/objectmngr.h"
#include "esys/assert.h"

#include "objectmngrstaticfunctor.h"
#include "objectmngrruntimefunctor.h"

namespace esys
{

ObjectMngr *ObjectMngr::g_cur_mngr = nullptr;

void ObjectMngr::SetCurrent(ObjectMngr *cur_mngr)
{
    g_cur_mngr = cur_mngr;
}

ObjectMngr *ObjectMngr::GetCurrent()
{
    return g_cur_mngr;
}


ObjectMngr::ObjectMngr(const ObjectName &name, bool top)
    : ObjectNode(name), m_top(top)
{
    //if (GetCurrent() == nullptr)
    SetCurrent(this);
}

ObjectMngr::~ObjectMngr()
{
    SetCurrent(nullptr);
}

int32_t ObjectMngr::StaticInit(Order order)
{
    int32_t result = 0;

    if (order == BEFORE_CHILDREN)
        return StaticInitBeforeChildren();
    if (order == AFTER_CHILDREN)
        return StaticInitAfterChildren();
    if (order == EVERYTHING)
    {
        result = StaticInitBeforeChildren();
        if (result < 0)
            return result;
    }

    Object *first_child = GetFirstChild();
    StaticFunctor fct(&Object::StaticInit);

    result = WalkTreeDepthFirst(first_child, fct);
    if (result < 0)
        return result;

    if (order == EVERYTHING)
    {
        return StaticInitAfterChildren();
    }

    return result;
}

int32_t ObjectMngr::StaticRelease(Order order)
{
    int32_t result = 0;

    if (order == BEFORE_CHILDREN)
        return StaticReleaseBeforeChildren();
    if (order == AFTER_CHILDREN)
        return StaticReleaseAfterChildren();
    if (order == EVERYTHING)
    {
        result = StaticReleaseBeforeChildren();
        if (result < 0)
            return result;
    }

    Object *first_child = GetLastChild();
    StaticFunctor fct(&Object::StaticRelease);

    result = WalkTreeDepthFirst(first_child, fct, true);
    if (result < 0)
        return result;

    if (order == EVERYTHING)
    {
        return StaticReleaseAfterChildren();
    }

    return result;
}

int32_t ObjectMngr::RuntimeInit(RuntimeLevel runtime_lvl, Order order)
{
    if (order == BEFORE_CHILDREN)
        return RuntimeInitBeforeChildren(runtime_lvl);
    if (order == AFTER_CHILDREN)
        return RuntimeInitAfterChildren(runtime_lvl);

    if (runtime_lvl != RuntimeLevel::ROOT)
        return 0;

    Object *first_child = GetFirstChild();
    RuntimeFunctor fct(&Object::RuntimeInit);
    int32_t result = 0;

    if (order == EVERYTHING)
    {
        result = RuntimeInitBeforeChildren(runtime_lvl);
        /*if (result < 0)
            return result; */
    }

    for (runtime_lvl = RuntimeLevel::ROOT + 1; runtime_lvl < (int32_t)RuntimeLevel::GetCount(); ++runtime_lvl)
    {
        fct.SetRuntimeLevel(runtime_lvl);
        result = WalkTreeDepthFirst(first_child, fct);
    }

    if (order == EVERYTHING)
    {
        return RuntimeInitAfterChildren(runtime_lvl);
    }

    return result;
}

int32_t ObjectMngr::RuntimeRelease(RuntimeLevel runtime_lvl, Order order)
{
    if (order == BEFORE_CHILDREN)
        return RuntimeReleaseBeforeChildren(runtime_lvl);
    if (order == AFTER_CHILDREN)
        return RuntimeReleaseAfterChildren(runtime_lvl);

    if (runtime_lvl != RuntimeLevel::ROOT)
        return 0;

    int32_t result = 0;

    if (order == EVERYTHING)
        result = RuntimeReleaseBeforeChildren(runtime_lvl);

    Object *first_child = GetLastChild();
    RuntimeFunctor fct(&Object::RuntimeRelease);

    for (runtime_lvl = RuntimeLevel::GetCount()-1; runtime_lvl >= RuntimeLevel::ROOT+1; --runtime_lvl)
    {
        fct.SetRuntimeLevel(runtime_lvl);
        result = WalkTreeDepthFirst(first_child, fct, true);
    }

    if (order == EVERYTHING)
        return RuntimeReleaseAfterChildren(runtime_lvl);

    return result;
}

void ObjectMngr::SetTop(bool top)
{
    m_top = top;
}

bool ObjectMngr::GetTop()
{
    return m_top;
}

}


