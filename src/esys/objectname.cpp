/*!
 * \file esys/objectname.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/objectname.h"
#include "esys/objectmngr.h"
#include "esys/assert.h"

namespace esys
{

ObjectName *ObjectName::g_current = nullptr;

void ObjectName::SetCurrent(ObjectName *current)
{
    g_current = current;
}

ObjectName *ObjectName::GetCurrent()
{
    return g_current;
}

ObjectName::ObjectName(const char *name)
    : ListItem<ObjectName>(), m_name(name), m_object(nullptr)
{
    /*ObjectMngr *mngr = ObjectMngr::GetCurrent();

    assert(mngr != nullptr); */

    if (GetCurrent() == nullptr)
    {
        SetCurrent(this);
    }
    else
    {
        SetPrev(GetCurrent());
        GetCurrent()->SetNext(this);
        SetCurrent(this);
    }
}

ObjectName::ObjectName(const ObjectName &name) : ListItem<ObjectName>(), m_name(name.GetName())
{
}

ObjectName::~ObjectName()
{
    ObjectName *cur;
    ObjectName *prev;
    Object *cur_obj;
    Object *prev_obj;

    cur = GetCurrent();
    if (cur != nullptr)
    {
        prev = cur->GetPrev();
        if (prev != nullptr)
        {
            cur_obj = cur->GetObject();
            assert(cur_obj != nullptr);

            prev_obj = prev->GetObject();
            assert(prev_obj != nullptr);

            prev_obj->AddChild(cur_obj);
            cur_obj->SetParent(prev_obj);
            prev->SetNext(nullptr);
        }
        SetCurrent(prev);
    }

}

const char *ObjectName::GetName() const
{
    return m_name;
}

ObjectName::operator const char *() const
{
    return m_name;
}

void ObjectName::SetObject(Object *object)
{
    m_object = object;
}

Object *ObjectName::GetObject() const
{
    return m_object;
}

/*ObjectName *ObjectName::GetT()
{
    return this;
}*/

}


