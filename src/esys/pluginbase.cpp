/*!
 * \file esys/pluginbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/pluginbase.h"

namespace esys
{

PluginBase::PluginBase()
{
}

PluginBase::~PluginBase()
{
}

std::string &PluginBase::GetName()
{
    return m_name;
}

std::string &PluginBase::GetShortName()
{
    return m_short_name;
}

std::string &PluginBase::GetVersion()
{
    return m_version;
}

bool PluginBase::IsDebug()
{
    return m_is_debug;
}

int PluginBase::Run(const std::vector<std::string> &cmd_line, const po::variables_map &vm)
{
    return 0;
}

std::size_t PluginBase::GetNbrOptionsDesc()
{
    return m_options_desc.size();
}

po::options_description *PluginBase::GetOptionsDesc(std::size_t idx)
{
    if (idx < m_options_desc.size())
        return m_options_desc[idx];
    return nullptr;
}

void PluginBase::AddOptionsDesc(po::options_description *options_desc)
{
    m_options_desc.push_back(options_desc);
}

void PluginBase::SetDebug(bool debug)
{
    m_is_debug = debug;
}

void PluginBase::SetName(const std::string &name)
{
    m_name = name;
}

void PluginBase::SetShortName(const std::string &short_name)
{
    m_short_name = short_name;
}

void PluginBase::SetVersion(const std::string &version)
{
    m_version = version;
}

const ::boost::filesystem::path &PluginBase::GetPath()
{
    return m_path;
}

void PluginBase::SetPath(const ::boost::filesystem::path &path)
{
    m_path = path;
}

std::string &PluginBase::GetFileName()
{
    return m_filename;
}

void PluginBase::SetFileName(const std::string &filename)
{
    m_filename = filename;
}

}


