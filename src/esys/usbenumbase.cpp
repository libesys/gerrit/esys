/*!
 * \file esys/usbenumbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/usbenumbase.h"

#include <cassert>

//#include <Arduino.h>

namespace esys
{

USBEnumBase::USBEnumBase(): m_filter_mngr(NULL), m_verbose(false)
{
}

USBEnumBase::~USBEnumBase()
{
}

void USBEnumBase::SetFilterMngr(USBEnumFilterMngr *filter_mngr)
{
    m_filter_mngr=filter_mngr;
}

USBEnumFilterMngr *USBEnumBase::GetFilterMngr()
{
    return m_filter_mngr;
}

std::size_t USBEnumBase::GetNbrFiltered()
{
    assert(m_filter_mngr!=NULL);

    return m_filter_mngr->GetNbrFiltered();
}

USBDevInfo *USBEnumBase::GetFiltered(std::size_t idx)
{
    assert(m_filter_mngr!=NULL);

    return m_filter_mngr->GetFiltered(idx);
}

void USBEnumBase::ClearFiltered()
{
    assert(m_filter_mngr!=NULL);

    m_filter_mngr->ClearFiltered();
}

void USBEnumBase::SetVerbose(bool verbose)
{
    m_verbose=verbose;
}

}
