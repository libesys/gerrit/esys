/*!
 * \file esys/win32/devinfo.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/devinfo.h"

namespace esys
{

DevInfo::DevInfo() :m_detail_data(nullptr)
{
    m_device_interface_data.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
    m_device_info_data.cbSize = sizeof(SP_DEVINFO_DATA);
}

DevInfo::~DevInfo()
{
    if (m_detail_data != nullptr)
        delete[] (uint8_t *)m_detail_data;
}

SP_DEVINFO_DATA &DevInfo::GetData()
{
    return m_device_info_data;
}

SP_DEVICE_INTERFACE_DATA &DevInfo::GetInterfaceData()
{
    return m_device_interface_data;
}

void DevInfo::SetDescName(const std::string &desc_name)
{
    m_desc_name = desc_name;
}

std::string &DevInfo::GetDescName()
{
    return m_desc_name;
}

void DevInfo::SetDriverName(const std::string &driver_name)
{
    m_driver_name = driver_name;
}

std::string &DevInfo::GetDriverName()
{
    return m_driver_name;
}

void DevInfo::SetDetailDataSize(uint32_t size)
{
    uint8_t *data;
    if (m_detail_data != nullptr)
        delete[] (uint8_t *)m_detail_data;

    data = new uint8_t[size];
    m_detail_data = (PSP_DEVICE_INTERFACE_DETAIL_DATA)data;
    m_detail_data->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
    //m_detail_data->cbSize = size; // Shouldn't it be this one??
}

PSP_DEVICE_INTERFACE_DETAIL_DATA DevInfo::GetDetailData()
{
    return m_detail_data;
}

void DevInfo::SetFriendlyName(const std::string &friendly_name)
{
    m_friendly_name = friendly_name;
}

std::string &DevInfo::GetFriendlyName()
{
    return m_friendly_name;
}

std::string &DevInfo::GetName()
{
    if (!m_friendly_name.empty())
        return m_friendly_name;
    return m_desc_name;
}

DEVICE_POWER_STATE &DevInfo::GetLatestPowerState()
{
    return m_latest_power_state;
}

}


