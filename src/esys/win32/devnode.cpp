/*!
 * \file esys/win32/devnode.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include <windows.h>
#include "esys/win32/devnode.h"
#include "esys/win32/devnodemngr.h"

#include <assert.h>

namespace esys
{

extern bool GetDeviceProperty(HDEVINFO DeviceInfoSet, PSP_DEVINFO_DATA DeviceInfoData, DWORD Property,
                              LPTSTR *ppBuffer);
/*
BOOL DriverNameToDeviceInst(
    _In_reads_bytes_(cbDriverName) PCHAR DriverName,
    _In_ size_t cbDriverName,
    _Out_ HDEVINFO *pDevInfo,
    _Out_writes_bytes_(sizeof(SP_DEVINFO_DATA)) PSP_DEVINFO_DATA pDevInfoData
    )
{
    HDEVINFO         deviceInfo = INVALID_HANDLE_VALUE;
    BOOL             status = TRUE;
    ULONG            deviceIndex;
    SP_DEVINFO_DATA  deviceInfoData;
    BOOL             bResult = FALSE;
    PCHAR            pDriverName = NULL;
    PSTR             buf = NULL;
    BOOL             done = FALSE;

    if (pDevInfo == NULL)
    {
        return FALSE;
    }

    if (pDevInfoData == NULL)
    {
        return FALSE;
    }

    memset(pDevInfoData, 0, sizeof(SP_DEVINFO_DATA));

    *pDevInfo = INVALID_HANDLE_VALUE;

    // Use local string to guarantee zero termination
    pDriverName = (PCHAR)ALLOC((DWORD)cbDriverName + 1);
    if (NULL == pDriverName)
    {
        status = FALSE;
        goto Done;
    }
    StringCbCopyN(pDriverName, cbDriverName + 1, DriverName, cbDriverName);

    //
    // We cannot walk the device tree with CM_Get_Sibling etc. unless we assume
    // the device tree will stabilize. Any devnode removal (even outside of USB)
    // would force us to retry. Instead we use Setup API to snapshot all
    // devices.
    //

    // Examine all present devices to see if any match the given DriverName
    //
    deviceInfo = SetupDiGetClassDevs(NULL,
        NULL,
        NULL,
        DIGCF_ALLCLASSES | DIGCF_PRESENT);

    if (deviceInfo == INVALID_HANDLE_VALUE)
    {
        status = FALSE;
        goto Done;
    }

    deviceIndex = 0;
    deviceInfoData.cbSize = sizeof(deviceInfoData);

    while (done == FALSE)
    {
        //
        // Get devinst of the next device
        //

        status = SetupDiEnumDeviceInfo(deviceInfo,
            deviceIndex,
            &deviceInfoData);

        deviceIndex++;

        if (!status)
        {
            //
            // This could be an error, or indication that all devices have been
            // processed. Either way the desired device was not found.
            //

            done = TRUE;
            break;
        }

        //
        // Get the DriverName value
        //

        bResult = GetDeviceProperty(deviceInfo,
            &deviceInfoData,
            SPDRP_DRIVER,
            &buf);

        // If the DriverName value matches, return the DeviceInstance
        //
        if (bResult == TRUE && buf != NULL && _stricmp(pDriverName, buf) == 0)
        {
            done = TRUE;
            *pDevInfo = deviceInfo;
            CopyMemory(pDevInfoData, &deviceInfoData, sizeof(deviceInfoData));
            FREE(buf);
            break;
        }

        if (buf != NULL)
        {
            FREE(buf);
            buf = NULL;
        }
    }

Done:

    if (bResult == FALSE)
    {
        if (deviceInfo != INVALID_HANDLE_VALUE)
        {
            SetupDiDestroyDeviceInfoList(deviceInfo);
        }
    }

    if (pDriverName != NULL)
    {
        FREE(pDriverName);
    }

    return status;
}
 */

/*****************************************************************************

DriverNameToDeviceProperties()

Returns the Device properties of the DevNode with the matching DriverName.
Returns NULL if the matching DevNode is not found.

The caller should free the returned structure using FREE() macro

*****************************************************************************/
/*
PUSB_DEVICE_PNP_STRINGS
DriverNameToDeviceProperties(
    _In_reads_bytes_(cbDriverName) PCHAR  DriverName,
    _In_ size_t cbDriverName
    )
{
    HDEVINFO        deviceInfo = INVALID_HANDLE_VALUE;
    SP_DEVINFO_DATA deviceInfoData = { 0 };
    ULONG           len;
    BOOL            status;
    PUSB_DEVICE_PNP_STRINGS DevProps = NULL;
    DWORD           lastError;

    // Allocate device propeties structure
    DevProps = (PUSB_DEVICE_PNP_STRINGS)ALLOC(sizeof(USB_DEVICE_PNP_STRINGS));

    if (NULL == DevProps)
    {
        status = FALSE;
        goto Done;
    }

    // Get device instance
    status = DriverNameToDeviceInst(DriverName, cbDriverName, &deviceInfo, &deviceInfoData);
    if (status == FALSE)
    {
        goto Done;
    }

    len = 0;
    status = SetupDiGetDeviceInstanceId(deviceInfo,
        &deviceInfoData,
        NULL,
        0,
        &len);
    lastError = GetLastError();


    if (status != FALSE && lastError != ERROR_INSUFFICIENT_BUFFER)
    {
        status = FALSE;
        goto Done;
    }

    //
    // An extra byte is required for the terminating character
    //

    len++;
    DevProps->DeviceId = ALLOC(len);

    if (DevProps->DeviceId == NULL)
    {
        status = FALSE;
        goto Done;
    }

    status = SetupDiGetDeviceInstanceId(deviceInfo,
        &deviceInfoData,
        DevProps->DeviceId,
        len,
        &len);
    if (status == FALSE)
    {
        goto Done;
    }

    status = GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_DEVICEDESC,
        &DevProps->DeviceDesc);

    if (status == FALSE)
    {
        goto Done;
    }


    //
    // We don't fail if the following registry query fails as these fields are additional information only
    //

    GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_HARDWAREID,
        &DevProps->HwId);

    GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_SERVICE,
        &DevProps->Service);

    GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_CLASS,
        &DevProps->DeviceClass);
Done:

    if (deviceInfo != INVALID_HANDLE_VALUE)
    {
        SetupDiDestroyDeviceInfoList(deviceInfo);
    }

    if (status == FALSE)
    {
        if (DevProps != NULL)
        {
            FreeDeviceProperties(&DevProps);
        }
    }
    return DevProps;
}
 */

/*****************************************************************************

DriverNameToDeviceInst()

Finds the Device instance of the DevNode with the matching DriverName.
Returns FALSE if the matching DevNode is not found and TRUE if found

*****************************************************************************/
BOOL DriverNameToDeviceInst(const std::string &DriverName,
                            //    _In_ size_t cbDriverName,
                            _Out_ HDEVINFO *pDevInfo,
                            _Out_writes_bytes_(sizeof(SP_DEVINFO_DATA)) PSP_DEVINFO_DATA pDevInfoData)
{
    HDEVINFO deviceInfo = INVALID_HANDLE_VALUE;
    BOOL status = TRUE;
    ULONG deviceIndex;
    SP_DEVINFO_DATA deviceInfoData;
    BOOL bResult = FALSE;
    // PCHAR            pDriverName = NULL;
    std::string pDriverName;
    PSTR buf = NULL;
    BOOL done = FALSE;

    if (pDevInfo == NULL)
    {
        return FALSE;
    }

    if (pDevInfoData == NULL)
    {
        return FALSE;
    }

    memset(pDevInfoData, 0, sizeof(SP_DEVINFO_DATA));

    *pDevInfo = INVALID_HANDLE_VALUE;

    // Use local string to guarantee zero termination
    // pDriverName = (PCHAR)ALLOC((DWORD)cbDriverName + 1);
    /*if (NULL == pDriverName)
    {
        status = FALSE;
        goto Done;
    }*/
    // StringCbCopyN(pDriverName, cbDriverName + 1, DriverName, cbDriverName);
    pDriverName = DriverName;

    //
    // We cannot walk the device tree with CM_Get_Sibling etc. unless we assume
    // the device tree will stabilize. Any devnode removal (even outside of USB)
    // would force us to retry. Instead we use Setup API to snapshot all
    // devices.
    //

    // Examine all present devices to see if any match the given DriverName
    //
    deviceInfo = SetupDiGetClassDevs(NULL, NULL, NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT);

    if (deviceInfo == INVALID_HANDLE_VALUE)
    {
        status = FALSE;
        goto Done;
    }

    deviceIndex = 0;
    deviceInfoData.cbSize = sizeof(deviceInfoData);

    while (done == FALSE)
    {
        //
        // Get devinst of the next device
        //

        status = SetupDiEnumDeviceInfo(deviceInfo, deviceIndex, &deviceInfoData);

        deviceIndex++;

        if (!status)
        {
            //
            // This could be an error, or indication that all devices have been
            // processed. Either way the desired device was not found.
            //

            done = TRUE;
            break;
        }

        //
        // Get the DriverName value
        //

        bResult = GetDeviceProperty(deviceInfo, &deviceInfoData, SPDRP_DRIVER, &buf);

        // If the DriverName value matches, return the DeviceInstance
        //
        if (bResult == TRUE && buf != NULL && stricmp(pDriverName.c_str(), buf) == 0)
        {
            done = TRUE;
            *pDevInfo = deviceInfo;
            CopyMemory(pDevInfoData, &deviceInfoData, sizeof(deviceInfoData));
            FREE(buf);
            break;
        }

        if (buf != NULL)
        {
            FREE(buf);
            buf = NULL;
        }
    }

Done:

    if (bResult == FALSE)
    {
        if (deviceInfo != INVALID_HANDLE_VALUE)
        {
            SetupDiDestroyDeviceInfoList(deviceInfo);
        }
    }

    /*if (pDriverName != NULL)
    {
        FREE(pDriverName);
    }*/

    return status;
}

/*****************************************************************************

DriverNameToDeviceProperties()

Returns the Device properties of the DevNode with the matching DriverName.
Returns NULL if the matching DevNode is not found.

The caller should free the returned structure using FREE() macro

*****************************************************************************/
/*USBDevicePNPStrings *DriverNameToDeviceProperties(
    //_In_reads_bytes_(cbDriverName) PCHAR  DriverName,
    //_In_ size_t cbDriverName
    const std::string DriverName
    )
{
    HDEVINFO        deviceInfo = INVALID_HANDLE_VALUE;
    SP_DEVINFO_DATA deviceInfoData = { 0 };
    ULONG           len;
    BOOL            status;
    USBDevicePNPStrings *DevProps = NULL;
    DWORD           lastError;

    // Allocate device propeties structure
    DevProps = (USBDevicePNPStrings *)ALLOC(sizeof(USBDevicePNPStrings));

    if (NULL == DevProps)
    {
        status = FALSE;
        goto Done;
    }

    // Get device instance
    status = DriverNameToDeviceInst(DriverName, &deviceInfo, &deviceInfoData);
    if (status == FALSE)
    {
        goto Done;
    }

    len = 0;
    status = SetupDiGetDeviceInstanceId(deviceInfo,
        &deviceInfoData,
        NULL,
        0,
        &len);
    lastError = GetLastError();


    if (status != FALSE && lastError != ERROR_INSUFFICIENT_BUFFER)
    {
        status = FALSE;
        goto Done;
    }

    //
    // An extra byte is required for the terminating character
    //

    len++;
    DevProps->m_device_id = (PWCHAR)ALLOC(len*sizeof(WCHAR));

    if (DevProps->m_device_id == NULL)
    {
        status = FALSE;
        goto Done;
    }

    status = SetupDiGetDeviceInstanceId(deviceInfo,
        &deviceInfoData,
        DevProps->m_device_id,
        len,
        &len);
    if (status == FALSE)
    {
        goto Done;
    }

    status = GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_DEVICEDESC,
        &DevProps->m_device_desc);

    if (status == FALSE)
    {
        goto Done;
    }


    //
    // We don't fail if the following registry query fails as these fields are additional information only
    //

    GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_HARDWAREID,
        &DevProps->m_hw_id);

    GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_SERVICE,
        &DevProps->m_service);

    GetDeviceProperty(deviceInfo,
        &deviceInfoData,
        SPDRP_CLASS,
        &DevProps->m_device_class);
Done:

    if (deviceInfo != INVALID_HANDLE_VALUE)
    {
        SetupDiDestroyDeviceInfoList(deviceInfo);
    }

    if (status == FALSE)
    {
        if (DevProps != NULL)
        {
            FreeDeviceProperties(&DevProps);
        }
    }
    return DevProps;
} */

DevNode::DevNode()
    : m_parent(nullptr)
    , m_dev_inst(-1)
{
}

DevNode::~DevNode()
{
    /*std::vector<DevNode *>::iterator it;

    for (it = m_children.begin(); it != m_children.end(); ++it)
    {
        delete *it;
    }
    m_children.clear(); */
}

const std::string &DevNode::GetInstanceId()
{
    if (m_instance_id.empty()) win32GetIntanceId(m_dev_inst, m_instance_id);
    return m_instance_id;
}

void DevNode::SetInstanceId(const std::string &instance_id)
{
    m_instance_id = instance_id;
}

void DevNode::SetParent(DevNode *parent)
{
    m_parent = parent;
}

DevNode *DevNode::GetParent()
{
    return m_parent;
}

int32_t DevNode::GetNbrChildren()
{
    return m_children.size();
}

DevNode *DevNode::GetChild(int32_t idx)
{
    if (idx >= m_children.size()) return nullptr;
    return m_children[idx];
}

void DevNode::AddChild(DevNode *child, bool do_register)
{
    assert(child != nullptr);

    m_children.push_back(child);
    child->SetParent(this);

    if (do_register == true) DevNodeMngr::Get().Register(child);
}

void DevNode::SetProperty(const std::string &name, const std::string &value)
{
    m_properties[name] = value;
}

int32_t DevNode::GetProperty(const std::string &name, std::string &value)
{
    std::map<std::string, std::string>::iterator it;

    it = m_properties.find(name);
    if (it == m_properties.end()) return -1;
    value = it->second;
    return 0;
}

int32_t DevNode::Refresh(RefreshDir refresh_dir)
{
    int32_t result;
    DEVINST parent_dev_inst;
    std::string parent_instance_id;
    DevNode *parent;

    result = win32GetIntanceId(m_dev_inst, m_instance_id);
    if (result < 0) return result;

    if (refresh_dir == RefreshDir::UP)
    {
        result = win32GetParent(m_dev_inst, parent_dev_inst);
        if (result < 0)
        {
            // Does it mean this is the root?
            return result;
        }
        if (result > 0)
        {
            // This DevNode doesn't have any parent
            DevNodeMngr::Get().SetRoot(this);
            return 0;
        }

        result = win32GetIntanceId(parent_dev_inst, parent_instance_id);
        if (result < 0) return result;
        parent = DevNodeMngr::Get().Find(parent_instance_id);
        if (parent == nullptr)
        {
            parent = DevNodeMngr::Get().NewNode();
            parent->AddChild(this, false);
            parent->SetDevInst(parent_dev_inst);
            parent->SetInstanceId(parent_instance_id);
            DevNodeMngr::Get().Register(parent);
            // We look for parents, only if the parent was not registered
            return parent->Refresh(RefreshDir::UP);
        }
        parent->AddChild(this, false);
        parent->SetDevInst(parent_dev_inst);
        parent->SetInstanceId(parent_instance_id);
    }
    return 0;
}

void DevNode::SetDevInst(DEVINST dev_inst)
{
    m_dev_inst = dev_inst;
}

DEVINST DevNode::GetDevInst()
{
    return m_dev_inst;
}

int32_t DevNode::win32GetParent(DEVINST dev_inst, DEVINST &parent_dev_inst)
{
    ULONG pul_status;
    ULONG pul_problem_number;

    assert(dev_inst != -1);

    if (CM_Get_DevNode_Status(&pul_status, &pul_problem_number, dev_inst, 0) != CR_SUCCESS) return -1;

    if (CM_Get_Parent(&parent_dev_inst, dev_inst, 0) != CR_SUCCESS) return 1;
    return 0;
}

int32_t DevNode::win32GetIntanceId(DEVINST dev_inst, std::string &instance_id)
{
    int32_t result;
    ULONG pul_len;

    assert(dev_inst != -1);

    if (CM_Get_Device_ID_Size(&pul_len, dev_inst, 0) != CR_SUCCESS) return -1;

    PSTR buffer = new char[pul_len + 10];
    auto result_cfg = CM_Get_Device_ID(dev_inst, buffer, pul_len + 9, 0);

    if (result_cfg == CR_SUCCESS)
    {
        buffer[pul_len] = 0;
        instance_id = buffer;
        result = 0;
    }
    else
        result = -2;
    delete[] buffer;

    return result;
}

int32_t DevNode::IsValid(DEVINST dev_inst)
{
    ULONG pul_status;
    ULONG pul_problem_number;

    assert(dev_inst != -1);

    if (CM_Get_DevNode_Status(&pul_status, &pul_problem_number, dev_inst, 0) != CR_SUCCESS) return -1;
    return 0;
}

} // namespace esys
