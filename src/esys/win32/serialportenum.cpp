/*!
 * \file esys/win32/serialportenum.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/serialportenum.h"
#include "esys/win32/devnode.h"
#include "esys/win32/devnodemngr.h"

#include <boost/locale.hpp>

#include <cfgmgr32.h>
#include <assert.h>

namespace esys
{

SerialPortEnum SerialPortEnum::s_port_enum;

SerialPortEnum &SerialPortEnum::Get()
{
    return s_port_enum;
}

SerialPortEnum::SerialPortEnum(): m_debug(false), m_dev_info(INVALID_HANDLE_VALUE)
{
}

SerialPortEnum::~SerialPortEnum()
{
    m_ports.clear();
}

uint32_t SerialPortEnum::GetCount()
{
    return m_ports.size();
}

DevNode *SerialPortEnum::GetPort(uint32_t idx)
{
    if (idx >= m_ports.size())
        return nullptr;

    return m_ports[idx];
}

std::string SerialPortEnum::GetPortName(uint32_t idx)
{
    std::string value;
    int32_t result;
    DevNode *dev_node = GetPort(idx);

    assert(dev_node != nullptr);

    result = dev_node->GetProperty("comportname", value);
    assert(result == 0);

    return value;
}

uint32_t SerialPortEnum::GetCount(DevNode *dev_node)
{
    uint32_t idx;
    uint32_t result = 0;
    DevNode *child;
    std::string value;

    assert(dev_node != nullptr);

    for (idx = 0; idx < dev_node->GetNbrChildren(); ++idx)
    {
        child = dev_node->GetChild(idx);
        assert(child != nullptr);

        if (child->GetProperty("comportname", value) == 0)
            result++;
    }
    return result;
}

DevNode *SerialPortEnum::Find(DevNode *dev_node, uint32_t idx)
{
    uint32_t idx_;
    uint32_t count = 0;
    DevNode *child;
    std::string value;

    for (idx_ = 0; idx_ < dev_node->GetNbrChildren(); ++idx_)
    {
        child = dev_node->GetChild(idx_);
        assert(child != nullptr);

        if (child->GetProperty("comportname", value) == 0)
        {
            if (idx == count)
                return child;
            count++;
        }
    }
    return nullptr;
}

int32_t SerialPortEnum::Find(DevNode *dev_node, std::string &com_port, uint32_t idx)
{
    int32_t result;
    std::string value;
    DevNode *com_dev_node = Find(dev_node, idx);

    if (com_dev_node == nullptr)
        return -1;
    result = com_dev_node->GetProperty("comportname", value);
    if (result < 0)
        return result;
    com_port = value;
    return 0;
}

int32_t SerialPortEnum::Refresh()
{
    DWORD size = 0;
    int rc;
    char dev_name[256];
    SP_DEVINFO_DATA dev_data;
    HKEY dev_key;
    DWORD len;
    int dev_idx;
    ULONG  pulStatus;
    ULONG  pulProblemNumber;

    uint32_t idx;
    esys::DevNode *dev_node;

    m_ports.clear();

    if (m_dev_info != INVALID_HANDLE_VALUE)
        Cleanup();

    SetupDiClassGuidsFromNameA("Ports", 0, 0, &size);
    if (size < 1)
        return -1;

    //GUID guids[size];
    GUID *guids;
    guids = new GUID[size];

    if (!SetupDiClassGuidsFromNameA("Ports", guids, size * sizeof(GUID), &size))
    {
        delete[] guids;
        return -2;
    }

    m_dev_info = SetupDiGetClassDevs(guids, NULL, NULL, DIGCF_PRESENT);
    if (m_dev_info == INVALID_HANDLE_VALUE)
    {
        delete[] guids;
        return -3;
    }

    dev_idx = 0;

    while (1)
    {
        dev_data.cbSize = sizeof(SP_DEVINFO_DATA);
        if (!SetupDiEnumDeviceInfo(m_dev_info, dev_idx, &dev_data))
        {
            delete[] guids;
            return 0;
        }

        rc = CM_Open_DevNode_Key(dev_data.DevInst,
                                 KEY_QUERY_VALUE,
                                 0,
                                 1,
                                 &dev_key,
                                 0);

        if (rc != ERROR_SUCCESS)
        {
            delete[] guids;
            return -5;
        }

        len = sizeof(dev_name);
        rc = RegQueryValueEx(dev_key,
                             "portname",
                             NULL,
                             NULL,
                             (BYTE *)dev_name,
                             &len);

        RegCloseKey(dev_key);
        if (rc != ERROR_SUCCESS)
        {
            delete[] guids;
            return -8;
        }

        if (DevNode::IsValid(dev_data.DevInst) == 0)
        {
            DevNode *dev_node = DevNodeMngr::Get().NewNode();
            dev_node->SetDevInst(dev_data.DevInst);
            dev_node->SetProperty("comportname", dev_name);
            dev_node->Refresh(DevNode::RefreshDir::UP);
            DevNodeMngr::Get().Register(dev_node);
            m_ports.push_back(dev_node);
        }
        /*if (CM_Get_DevNode_Status(&pulStatus, &pulProblemNumber, dev_data.DevInst, 0) == CR_SUCCESS)
        {
            DEVINST parent_dev_inst;
            if (CM_Get_Parent(&parent_dev_inst, dev_data.DevInst, 0) == CR_SUCCESS)
            {
                ULONG pulLen;

                if (CM_Get_Device_ID_Size(&pulLen, parent_dev_inst, 0) == CR_SUCCESS)
                {
                    PWSTR buffer = new WCHAR[pulLen + 1];
                    if (CM_Get_Device_ID(parent_dev_inst, buffer, pulLen, 0) == CR_SUCCESS)
                    {

                    }
                    delete[] buffer;
                }
            }
        } */
        dev_idx++;

        /*if (strncmp("COM", (char*)devName, 3) == 0)
            break; */
    }
    delete[] guids;
}

void SerialPortEnum::Cleanup()
{
    //_devNum = 0;
    //_devNode = NULL;

    if (m_dev_info != INVALID_HANDLE_VALUE)
    {
        SetupDiDestroyDeviceInfoList(m_dev_info);
        m_dev_info = INVALID_HANDLE_VALUE;
    }
}

}

