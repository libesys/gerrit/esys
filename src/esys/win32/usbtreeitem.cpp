/*!
 * \file esys/win32/usbtreeitem.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/usbtreeitem.h"

namespace esys
{

USBTreeItem::USBTreeItem()
    : USBTreeItemBase()
    , m_hc_info(nullptr)
    , m_root_hub_info(nullptr)
    , m_external_hub_info(nullptr)
    , m_device_info(nullptr)
{
}

USBTreeItem::~USBTreeItem()
{
    if (m_hc_info != nullptr) delete m_hc_info;
    if (m_root_hub_info != nullptr) delete m_root_hub_info;
    if (m_device_info != nullptr) delete m_device_info;
    if (m_external_hub_info != nullptr) delete m_external_hub_info;
}

void USBTreeItem::SetInfo(USBHostControllerInfo *hc_info)
{
    m_hc_info = hc_info;
}

void USBTreeItem::SetInfo(USBRootHUBInfo *root_hub_info)
{
    m_root_hub_info = root_hub_info;
}

void USBTreeItem::SetInfo(USBExternalHUBInfo *external_hub_info)
{
    m_external_hub_info = external_hub_info;
}

void USBTreeItem::SetInfo(USBDeviceInfo *device_info)
{
    m_device_info = device_info;
}

USBTreeItem *USBTreeItem::AddRootHUB(USBRootHUBInfo *root_hub)
{
    USBTreeItem *child = new USBTreeItem();

    child->SetInfo(root_hub);
    if (root_hub->m_port_connector_props != nullptr)
        child->SetHubPort(root_hub->m_port_connector_props->ConnectionIndex);
    AddChild(child);
    return child;
}

USBTreeItem *USBTreeItem::AddExternalHUB(USBExternalHUBInfo *external_hub)
{
    USBTreeItem *child = new USBTreeItem();

    child->SetInfo(external_hub);
    if (external_hub->m_port_connector_props != nullptr)
        child->SetHubPort(external_hub->m_port_connector_props->ConnectionIndex);
    AddChild(child);
    return child;
}

USBTreeItem *USBTreeItem::AddDevice(USBDeviceInfo *device, const std::string &name)
{
    USBTreeItem *child = new USBTreeItem();

    child->SetName(name);
    child->SetInfo(device);
    if (device->m_port_connector_props != nullptr) child->SetHubPort(device->m_port_connector_props->ConnectionIndex);
    AddChild(child);
    return child;
}

} // namespace esys
