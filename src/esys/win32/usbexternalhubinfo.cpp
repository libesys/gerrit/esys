/*!
 * \file esys/win32/usbexternalhubinfo.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/usbexternalhubinfo.h"

namespace esys
{

USBExternalHUBInfo::USBExternalHUBInfo()
{
}

USBExternalHUBInfo::~USBExternalHUBInfo()
{
}

}



