/*!
 * \file esys/win32/usbenum.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include <initguid.h>
#include "esys/esys_defs.h"

#include "esys/win32/usbenum.h"
#include "esys/win32/devnode.h"
#include "esys/win32/devnodemngr.h"
#include "esys/win32/usbdesc.h"
#include "esys/win32/serialportenum.h"
#include "esys/win32/devinfo.h"
#include "esys/win32/usbdeviceinfo.h"
#include "esys/win32/usbroothubinfo.h"
#include "esys/win32/usbexternalhubinfo.h"

//#include "esys/usbdevserialportinfo.h"

#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>

#include <boost/locale.hpp>

#include <Setupapi.h>
#include <devioctl.h>
#include <Usbioctl.h>
#include <Usbiodef.h>
#include <Cfgmgr32.h>
#include <strsafe.h>

#include <iostream>
#include <sstream>

#include <tchar.h>
#include <assert.h>

//#define INITGUID 1  //needed to have the stuff to compile

//#include <Devpropdef.h>
#include <Devpkey.h>

#define MAX_DEVICE_PROP 200
#define MAX_DRIVER_KEY_NAME 256

namespace esys
{

std::string g_ConnectionStatuses[] = {
    "",                   // 0  - NoDeviceConnected
    "",                   // 1  - DeviceConnected
    "FailedEnumeration",  // 2  - DeviceFailedEnumeration
    "GeneralFailure",     // 3  - DeviceGeneralFailure
    "Overcurrent",        // 4  - DeviceCausedOvercurrent
    "NotEnoughPower",     // 5  - DeviceNotEnoughPower
    "NotEnoughBandwidth", // 6  - DeviceNotEnoughBandwidth
    "HubNestedTooDeeply", // 7  - DeviceHubNestedTooDeeply
    "InLegacyHub",        // 8  - DeviceInLegacyHub
    "Enumerating",        // 9  - DeviceEnumerating
    "Reset"               // 10 - DeviceReset
};

FORCEINLINE
VOID InitializeListHead(_Out_ PLIST_ENTRY ListHead)
{
    ListHead->Flink = ListHead->Blink = ListHead;
}

//*****************************************************************************
//
// GetExternalHubName()
//
//*****************************************************************************

std::string GetExternalHubName(HANDLE Hub, ULONG ConnectionIndex)
{
    BOOL success = 0;
    ULONG nBytes = 0;
    USB_NODE_CONNECTION_NAME extHubName;
    PUSB_NODE_CONNECTION_NAME extHubNameW = NULL;
    PCHAR extHubNameA = NULL;
    std::string name;
    std::wstring name_w;
    // Get the length of the name of the external hub attached to the
    // specified port.
    //
    extHubName.ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub, IOCTL_USB_GET_NODE_CONNECTION_NAME, &extHubName, sizeof(extHubName), &extHubName,
                              sizeof(extHubName), &nBytes, NULL);

    if (!success)
    {
        OOPS();
        goto GetExternalHubNameError;
    }

    // Allocate space to hold the external hub name
    //
    nBytes = extHubName.ActualLength;

    if (nBytes <= sizeof(extHubName))
    {
        OOPS();
        goto GetExternalHubNameError;
    }

    extHubNameW = (PUSB_NODE_CONNECTION_NAME)ALLOC(nBytes);

    if (extHubNameW == NULL)
    {
        OOPS();
        goto GetExternalHubNameError;
    }

    // Get the name of the external hub attached to the specified port
    //
    extHubNameW->ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub, IOCTL_USB_GET_NODE_CONNECTION_NAME, extHubNameW, nBytes, extHubNameW, nBytes,
                              &nBytes, NULL);

    if (!success)
    {
        OOPS();
        goto GetExternalHubNameError;
    }

    // Convert the External Hub name
    //
    // extHubNameA = WideStrToMultiStr(extHubNameW->NodeName, nBytes - sizeof(USB_NODE_CONNECTION_NAME) +
    // sizeof(WCHAR));
    name_w = extHubNameW->NodeName;

    name = boost::locale::conv::utf_to_utf<char>(name_w);

    // All done, free the uncoverted external hub name and return the
    // converted external hub name
    //
    FREE(extHubNameW);

    return name;

GetExternalHubNameError:
    // There was an error, free anything that was allocated
    //
    if (extHubNameW != NULL)
    {
        FREE(extHubNameW);
        extHubNameW = NULL;
    }

    return "";
}

StringDescriptorNode *GetStringDescriptor(HANDLE hHubDevice, ULONG ConnectionIndex, UCHAR DescriptorIndex,
                                          USHORT LanguageID)
{
    BOOL success = 0;
    ULONG nBytes = 0;
    ULONG nBytesReturned = 0;

    UCHAR stringDescReqBuf[sizeof(USB_DESCRIPTOR_REQUEST) + MAXIMUM_USB_STRING_LENGTH];

    PUSB_DESCRIPTOR_REQUEST stringDescReq = NULL;
    PUSB_STRING_DESCRIPTOR stringDesc = NULL;
    StringDescriptorNode *stringDescNode = NULL;

    nBytes = sizeof(stringDescReqBuf);

    stringDescReq = (PUSB_DESCRIPTOR_REQUEST)stringDescReqBuf;
    stringDesc = (PUSB_STRING_DESCRIPTOR)(stringDescReq + 1);

    // Zero fill the entire request structure
    //
    memset(stringDescReq, 0, nBytes);

    // Indicate the port from which the descriptor will be requested
    //
    stringDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    stringDescReq->SetupPacket.wValue = (USB_STRING_DESCRIPTOR_TYPE << 8) | DescriptorIndex;

    stringDescReq->SetupPacket.wIndex = LanguageID;

    stringDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //
    success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, stringDescReq, nBytes,
                              stringDescReq, nBytes, &nBytesReturned, NULL);

    //
    // Do some sanity checks on the return from the get descriptor request.
    //

    if (!success)
    {
        OOPS();
        return NULL;
    }

    if (nBytesReturned < 2)
    {
        OOPS();
        return NULL;
    }

    if (stringDesc->bDescriptorType != USB_STRING_DESCRIPTOR_TYPE)
    {
        OOPS();
        return NULL;
    }

    if (stringDesc->bLength != nBytesReturned - sizeof(USB_DESCRIPTOR_REQUEST))
    {
        OOPS();
        return NULL;
    }

    if (stringDesc->bLength % 2 != 0)
    {
        OOPS();
        return NULL;
    }

    //
    // Looks good, allocate some (zero filled) space for the string descriptor
    // node and copy the string descriptor to it.
    //

    stringDescNode = (StringDescriptorNode *)ALLOC(sizeof(StringDescriptorNode) + stringDesc->bLength);

    if (stringDescNode == NULL)
    {
        OOPS();
        return NULL;
    }

    stringDescNode->m_descriptor_index = DescriptorIndex;
    stringDescNode->m_language_id = LanguageID;

    memcpy(stringDescNode->m_string_descriptor, stringDesc, stringDesc->bLength);

    return stringDescNode;
}

//*****************************************************************************
//
// GetStringDescriptors()
//
// hHubDevice - Handle of the hub device containing the port from which the
// String Descriptor will be requested.
//
// ConnectionIndex - Identifies the port on the hub to which a device is
// attached from which the String Descriptor will be requested.
//
// DescriptorIndex - String Descriptor index.
//
// NumLanguageIDs -  Number of languages in which the string should be
// requested.
//
// LanguageIDs - Languages in which the string should be requested.
//
// StringDescNodeHead - First node in linked list of device's string descriptors
//
// Return Value: HRESULT indicating whether the string is on the list
//
//*****************************************************************************

HRESULT
GetStringDescriptors(_In_ HANDLE hHubDevice, _In_ ULONG ConnectionIndex, _In_ UCHAR DescriptorIndex,
                     _In_ ULONG NumLanguageIDs, _In_reads_(NumLanguageIDs) WCHAR *LanguageIDs,
                     _In_ StringDescriptorNode *StringDescNodeHead)
{
    StringDescriptorNode *tail = NULL;
    StringDescriptorNode *trailing = NULL;
    ULONG i = 0;

    //
    // Go to the end of the linked list, searching for the requested index to
    // see if we've already retrieved it
    //
    for (tail = StringDescNodeHead; tail != NULL; tail = tail->m_next)
    {
        if (tail->m_descriptor_index == DescriptorIndex)
        {
            return S_OK;
        }

        trailing = tail;
    }

    tail = trailing;

    //
    // Get the next String Descriptor. If this is NULL, then we're done (return)
    // Otherwise, loop through all Language IDs
    //
    for (i = 0; (tail != NULL) && (i < NumLanguageIDs); i++)
    {
        tail->m_next = GetStringDescriptor(hHubDevice, ConnectionIndex, DescriptorIndex, LanguageIDs[i]);

        tail = tail->m_next;
    }

    if (tail == NULL)
    {
        return E_FAIL;
    }
    else
    {
        return S_OK;
    }
}

BOOL AreThereStringDescriptors(PUSB_DEVICE_DESCRIPTOR DeviceDesc, PUSB_CONFIGURATION_DESCRIPTOR ConfigDesc)
{
    PUCHAR descEnd = NULL;
    PUSB_COMMON_DESCRIPTOR commonDesc = NULL;

    //
    // Check Device Descriptor strings
    //

    if (DeviceDesc->iManufacturer || DeviceDesc->iProduct || DeviceDesc->iSerialNumber)
    {
        return TRUE;
    }

    //
    // Check the Configuration and Interface Descriptor strings
    //

    descEnd = (PUCHAR)ConfigDesc + ConfigDesc->wTotalLength;

    commonDesc = (PUSB_COMMON_DESCRIPTOR)ConfigDesc;

    while ((PUCHAR)commonDesc + sizeof(USB_COMMON_DESCRIPTOR) < descEnd
           && (PUCHAR)commonDesc + commonDesc->bLength <= descEnd)
    {
        switch (commonDesc->bDescriptorType)
        {
            case USB_CONFIGURATION_DESCRIPTOR_TYPE:
            case USB_OTHER_SPEED_CONFIGURATION_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_CONFIGURATION_DESCRIPTOR))
                {
                    OOPS();
                    break;
                }
                if (((PUSB_CONFIGURATION_DESCRIPTOR)commonDesc)->iConfiguration)
                {
                    return TRUE;
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)((PUCHAR)commonDesc + commonDesc->bLength);
                continue;

            case USB_INTERFACE_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR))
                //&&
                // commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR2))
                {
                    OOPS();
                    break;
                }
                if (((PUSB_INTERFACE_DESCRIPTOR)commonDesc)->iInterface)
                {
                    return TRUE;
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)((PUCHAR)commonDesc + commonDesc->bLength);
                continue;

            default: commonDesc = (PUSB_COMMON_DESCRIPTOR)((PUCHAR)commonDesc + commonDesc->bLength); continue;
        }
        break;
    }

    return FALSE;
}

//*****************************************************************************
//
// GetAllStringDescriptors()
//
// hHubDevice - Handle of the hub device containing the port from which the
// String Descriptors will be requested.
//
// ConnectionIndex - Identifies the port on the hub to which a device is
// attached from which the String Descriptors will be requested.
//
// DeviceDesc - Device Descriptor for which String Descriptors should be
// requested.
//
// ConfigDesc - Configuration Descriptor (also containing Interface Descriptor)
// for which String Descriptors should be requested.
//
//*****************************************************************************

StringDescriptorNode *USBEnum::GetAllStringDescriptors(HANDLE hHubDevice, ULONG ConnectionIndex,
                                                       PUSB_DEVICE_DESCRIPTOR DeviceDesc,
                                                       PUSB_CONFIGURATION_DESCRIPTOR ConfigDesc)
{
    StringDescriptorNode *supportedLanguagesString = NULL;
    ULONG numLanguageIDs = 0;
    // USHORT                  *languageIDs = NULL;
    WCHAR *languageIDs = NULL;

    PUCHAR descEnd = NULL;
    PUSB_COMMON_DESCRIPTOR commonDesc = NULL;
    UCHAR uIndex = 1;
    UCHAR bInterfaceClass = 0;
    BOOL getMoreStrings = FALSE;
    HRESULT hr = S_OK;

    //
    // Get the array of supported Language IDs, which is returned
    // in String Descriptor 0
    //
    supportedLanguagesString = GetStringDescriptor(hHubDevice, ConnectionIndex, 0, 0);

    if (supportedLanguagesString == NULL)
    {
        return NULL;
    }

    numLanguageIDs = (supportedLanguagesString->m_string_descriptor->bLength - 2) / 2;

    languageIDs = &supportedLanguagesString->m_string_descriptor->bString[0];

    //
    // Get the Device Descriptor strings
    //

    if (DeviceDesc->iManufacturer)
    {
        GetStringDescriptors(hHubDevice, ConnectionIndex, DeviceDesc->iManufacturer, numLanguageIDs, languageIDs,
                             supportedLanguagesString);
    }

    if (DeviceDesc->iProduct)
    {
        GetStringDescriptors(hHubDevice, ConnectionIndex, DeviceDesc->iProduct, numLanguageIDs, languageIDs,
                             supportedLanguagesString);
    }

    if (DeviceDesc->iSerialNumber)
    {
        GetStringDescriptors(hHubDevice, ConnectionIndex, DeviceDesc->iSerialNumber, numLanguageIDs, languageIDs,
                             supportedLanguagesString);
    }

    //
    // Get the Configuration and Interface Descriptor strings
    //

    descEnd = (PUCHAR)ConfigDesc + ConfigDesc->wTotalLength;

    commonDesc = (PUSB_COMMON_DESCRIPTOR)ConfigDesc;

    while ((PUCHAR)commonDesc + sizeof(USB_COMMON_DESCRIPTOR) < descEnd
           && (PUCHAR)commonDesc + commonDesc->bLength <= descEnd)
    {
        switch (commonDesc->bDescriptorType)
        {
            case USB_CONFIGURATION_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_CONFIGURATION_DESCRIPTOR))
                {
                    OOPS();
                    break;
                }
                if (((PUSB_CONFIGURATION_DESCRIPTOR)commonDesc)->iConfiguration)
                {
                    GetStringDescriptors(hHubDevice, ConnectionIndex,
                                         ((PUSB_CONFIGURATION_DESCRIPTOR)commonDesc)->iConfiguration, numLanguageIDs,
                                         languageIDs, supportedLanguagesString);
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)((PUCHAR)commonDesc + commonDesc->bLength);
                continue;

            case USB_IAD_DESCRIPTOR_TYPE:
                if (commonDesc->bLength < sizeof(USB_IAD_DESCRIPTOR))
                {
                    OOPS();
                    break;
                }
                if (((PUSB_IAD_DESCRIPTOR)commonDesc)->iFunction)
                {
                    GetStringDescriptors(hHubDevice, ConnectionIndex, ((PUSB_IAD_DESCRIPTOR)commonDesc)->iFunction,
                                         numLanguageIDs, languageIDs, supportedLanguagesString);
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)((PUCHAR)commonDesc + commonDesc->bLength);
                continue;

            case USB_INTERFACE_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR)
                    && commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR2))
                {
                    OOPS();
                    break;
                }
                if (((PUSB_INTERFACE_DESCRIPTOR)commonDesc)->iInterface)
                {
                    GetStringDescriptors(hHubDevice, ConnectionIndex,
                                         ((PUSB_INTERFACE_DESCRIPTOR)commonDesc)->iInterface, numLanguageIDs,
                                         languageIDs, supportedLanguagesString);
                }

                //
                // We need to display more string descriptors for the following
                // interface classes
                //
                bInterfaceClass = ((PUSB_INTERFACE_DESCRIPTOR)commonDesc)->bInterfaceClass;
                if (bInterfaceClass == USB_DEVICE_CLASS_VIDEO)
                {
                    getMoreStrings = TRUE;
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)((PUCHAR)commonDesc + commonDesc->bLength);
                continue;

            default: commonDesc = (PUSB_COMMON_DESCRIPTOR)((PUCHAR)commonDesc + commonDesc->bLength); continue;
        }
        break;
    }

    // if (getMoreStrings)
    {
        //
        // We might need to display strings later that are referenced only in
        // class-specific descriptors. Get String Descriptors 1 through 32 (an
        // arbitrary upper limit for Strings needed due to "bad devices"
        // returning an infinite repeat of Strings 0 through 4) until one is not
        // found.
        //
        // There are also "bad devices" that have issues even querying 1-32, but
        // historically USBView made this query, so the query should be safe for
        // video devices.
        //
        for (uIndex = 6; SUCCEEDED(hr) && (uIndex < 6 + NUM_STRING_DESC_TO_GET); uIndex++)
        {
            hr = GetStringDescriptors(hHubDevice, ConnectionIndex, uIndex, numLanguageIDs, languageIDs,
                                      supportedLanguagesString);
        }
    }

    return supportedLanguagesString;
}

PUSB_DESCRIPTOR_REQUEST GetConfigDescriptor(HANDLE hHubDevice, ULONG ConnectionIndex, UCHAR DescriptorIndex)
{
    BOOL success = 0;
    ULONG nBytes = 0;
    ULONG nBytesReturned = 0;

    UCHAR configDescReqBuf[sizeof(USB_DESCRIPTOR_REQUEST) + sizeof(USB_CONFIGURATION_DESCRIPTOR)];

    PUSB_DESCRIPTOR_REQUEST configDescReq = NULL;
    PUSB_CONFIGURATION_DESCRIPTOR configDesc = NULL;

    // Request the Configuration Descriptor the first time using our
    // local buffer, which is just big enough for the Cofiguration
    // Descriptor itself.
    //
    nBytes = sizeof(configDescReqBuf);

    configDescReq = (PUSB_DESCRIPTOR_REQUEST)configDescReqBuf;
    configDesc = (PUSB_CONFIGURATION_DESCRIPTOR)(configDescReq + 1);

    // Zero fill the entire request structure
    //
    memset(configDescReq, 0, nBytes);

    // Indicate the port from which the descriptor will be requested
    //
    configDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    configDescReq->SetupPacket.wValue = (USB_CONFIGURATION_DESCRIPTOR_TYPE << 8) | DescriptorIndex;

    configDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //
    success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, configDescReq, nBytes,
                              configDescReq, nBytes, &nBytesReturned, NULL);

    if (!success)
    {
        OOPS();
        return NULL;
    }

    if (nBytes != nBytesReturned)
    {
        OOPS();
        return NULL;
    }

    if (configDesc->wTotalLength < sizeof(USB_CONFIGURATION_DESCRIPTOR))
    {
        OOPS();
        return NULL;
    }

    // Now request the entire Configuration Descriptor using a dynamically
    // allocated buffer which is sized big enough to hold the entire descriptor
    //
    nBytes = sizeof(USB_DESCRIPTOR_REQUEST) + configDesc->wTotalLength;

    configDescReq = (PUSB_DESCRIPTOR_REQUEST)ALLOC(nBytes);

    if (configDescReq == NULL)
    {
        OOPS();
        return NULL;
    }

    configDesc = (PUSB_CONFIGURATION_DESCRIPTOR)(configDescReq + 1);

    // Indicate the port from which the descriptor will be requested
    //
    configDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    configDescReq->SetupPacket.wValue = (USB_CONFIGURATION_DESCRIPTOR_TYPE << 8) | DescriptorIndex;

    configDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //

    success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, configDescReq, nBytes,
                              configDescReq, nBytes, &nBytesReturned, NULL);

    if (!success)
    {
        OOPS();
        FREE(configDescReq);
        return NULL;
    }

    if (nBytes != nBytesReturned)
    {
        OOPS();
        FREE(configDescReq);
        return NULL;
    }

    if (configDesc->wTotalLength != (nBytes - sizeof(USB_DESCRIPTOR_REQUEST)))
    {
        OOPS();
        FREE(configDescReq);
        return NULL;
    }

    return configDescReq;
}

bool GetDeviceProperty(HDEVINFO DeviceInfoSet, PSP_DEVINFO_DATA DeviceInfoData, DWORD Property, LPTSTR *ppBuffer)
{
    BOOL bResult;
    DWORD requiredLength = 0;
    DWORD lastError;

    if (ppBuffer == NULL)
    {
        return false;
    }

    *ppBuffer = NULL;

    bResult = SetupDiGetDeviceRegistryProperty(DeviceInfoSet, DeviceInfoData, Property, NULL, NULL, 0, &requiredLength);
    lastError = GetLastError();

    if ((requiredLength == 0) || (bResult != FALSE && lastError != ERROR_INSUFFICIENT_BUFFER))
    {
        return false;
    }

    //*ppBuffer = (LPTSTR) new uint8_t[requiredLength];
    *ppBuffer = (LPTSTR)ALLOC(requiredLength);

    if (*ppBuffer == NULL)
    {
        return false;
    }

    bResult = SetupDiGetDeviceRegistryProperty(DeviceInfoSet, DeviceInfoData, Property, NULL, (PBYTE)*ppBuffer,
                                               requiredLength, &requiredLength);
    if (bResult == FALSE)
    {
        FREE(*ppBuffer);
        *ppBuffer = NULL;
        return false;
    }

    return true;
}

bool GetDeviceProperty(HDEVINFO DeviceInfoSet, PSP_DEVINFO_DATA DeviceInfoData, DWORD Property, std::string &value)
{
    BOOL bResult;
    DWORD requiredLength = 0;
    DWORD lastError;
    // LPTSTR buffer;
    std::vector<uint8_t> buffer;

    bResult = SetupDiGetDeviceRegistryProperty(DeviceInfoSet, DeviceInfoData, Property, NULL, NULL, 0, &requiredLength);
    lastError = GetLastError();

    if ((requiredLength == 0) || (bResult != FALSE && lastError != ERROR_INSUFFICIENT_BUFFER))
    {
        return false;
    }

    buffer.reserve(requiredLength * 2);
    buffer.resize(requiredLength);

    // buffer = (LPTSTR) new uint8_t[requiredLength];
    //*ppBuffer = (LPTSTR)ALLOC(requiredLength);

    /*if (buffer == nullptr)
    {
        return false;
    }*/

    bResult = SetupDiGetDeviceRegistryProperty(DeviceInfoSet, DeviceInfoData, Property, NULL, (PBYTE)buffer.data(),
                                               requiredLength, &requiredLength);

    if (bResult == FALSE)
    {
        // delete buffer;
        value = "";
        return false;
    }

    value = (char *)buffer.data();
    return true;
}

PUSB_DESCRIPTOR_REQUEST GetBOSDescriptor(HANDLE hHubDevice, ULONG ConnectionIndex)
{
    BOOL success = 0;
    ULONG nBytes = 0;
    ULONG nBytesReturned = 0;

    UCHAR bosDescReqBuf[sizeof(USB_DESCRIPTOR_REQUEST) + sizeof(USB_BOS_DESCRIPTOR)];

    PUSB_DESCRIPTOR_REQUEST bosDescReq = NULL;
    PUSB_BOS_DESCRIPTOR bosDesc = NULL;

    // Request the BOS Descriptor the first time using our
    // local buffer, which is just big enough for the BOS
    // Descriptor itself.
    //
    nBytes = sizeof(bosDescReqBuf);

    bosDescReq = (PUSB_DESCRIPTOR_REQUEST)bosDescReqBuf;
    bosDesc = (PUSB_BOS_DESCRIPTOR)(bosDescReq + 1);

    // Zero fill the entire request structure
    //
    memset(bosDescReq, 0, nBytes);

    // Indicate the port from which the descriptor will be requested
    //
    bosDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    bosDescReq->SetupPacket.wValue = (USB_BOS_DESCRIPTOR_TYPE << 8);

    bosDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //
    success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, bosDescReq, nBytes, bosDescReq,
                              nBytes, &nBytesReturned, NULL);

    if (!success)
    {
        OOPS();
        return NULL;
    }

    if (nBytes != nBytesReturned)
    {
        OOPS();
        return NULL;
    }

    if (bosDesc->wTotalLength < sizeof(USB_BOS_DESCRIPTOR))
    {
        OOPS();
        return NULL;
    }

    // Now request the entire BOS Descriptor using a dynamically
    // allocated buffer which is sized big enough to hold the entire descriptor
    //
    nBytes = sizeof(USB_DESCRIPTOR_REQUEST) + bosDesc->wTotalLength;

    bosDescReq = (PUSB_DESCRIPTOR_REQUEST)ALLOC(nBytes);

    if (bosDescReq == NULL)
    {
        OOPS();
        return NULL;
    }

    bosDesc = (PUSB_BOS_DESCRIPTOR)(bosDescReq + 1);

    // Indicate the port from which the descriptor will be requested
    //
    bosDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    bosDescReq->SetupPacket.wValue = (USB_BOS_DESCRIPTOR_TYPE << 8);

    bosDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //

    success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, bosDescReq, nBytes, bosDescReq,
                              nBytes, &nBytesReturned, NULL);

    if (!success)
    {
        OOPS();
        FREE(bosDescReq);
        return NULL;
    }

    if (nBytes != nBytesReturned)
    {
        OOPS();
        FREE(bosDescReq);
        return NULL;
    }

    if (bosDesc->wTotalLength != (nBytes - sizeof(USB_DESCRIPTOR_REQUEST)))
    {
        OOPS();
        FREE(bosDescReq);
        return NULL;
    }

    return bosDescReq;
}

std::string GetDriverKeyName(HANDLE Hub, ULONG ConnectionIndex)
{
    BOOL success = 0;
    ULONG nBytes = 0;
    USB_NODE_CONNECTION_DRIVERKEY_NAME driverKeyName;
    PUSB_NODE_CONNECTION_DRIVERKEY_NAME driverKeyNameW = NULL;
    PCHAR driverKeyNameA = NULL;
    std::string name;
    std::wstring name_w;

    // Get the length of the name of the driver key of the device attached to
    // the specified port.
    //
    driverKeyName.ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub, IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME, &driverKeyName, sizeof(driverKeyName),
                              &driverKeyName, sizeof(driverKeyName), &nBytes, NULL);

    if (!success)
    {
        OOPS();
        goto GetDriverKeyNameError;
    }

    // Allocate space to hold the driver key name
    //
    nBytes = driverKeyName.ActualLength;

    if (nBytes <= sizeof(driverKeyName))
    {
        OOPS();
        goto GetDriverKeyNameError;
    }

    driverKeyNameW = (PUSB_NODE_CONNECTION_DRIVERKEY_NAME)ALLOC(nBytes);
    if (driverKeyNameW == NULL)
    {
        OOPS();
        goto GetDriverKeyNameError;
    }

    // Get the name of the driver key of the device attached to
    // the specified port.
    //
    driverKeyNameW->ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub, IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME, driverKeyNameW, nBytes, driverKeyNameW,
                              nBytes, &nBytes, NULL);

    if (!success)
    {
        OOPS();
        goto GetDriverKeyNameError;
    }

    // Convert the driver key name
    //
    // driverKeyNameA = WideStrToMultiStr(driverKeyNameW->DriverKeyName, nBytes -
    // sizeof(USB_NODE_CONNECTION_DRIVERKEY_NAME) + sizeof(WCHAR));
    name_w = driverKeyNameW->DriverKeyName;
    name = boost::locale::conv::utf_to_utf<char>(name_w);
    // All done, free the uncoverted driver key name and return the
    // converted driver key name
    //
    FREE(driverKeyNameW);

    // return driverKeyNameA;
    return name;

GetDriverKeyNameError:
    // There was an error, free anything that was allocated
    //
    if (driverKeyNameW != NULL)
    {
        FREE(driverKeyNameW);
        driverKeyNameW = NULL;
    }

    return "";
}

bool DriverNameToDeviceInst(std::string DriverName, size_t cbDriverName, HDEVINFO *pDevInfo,
                            PSP_DEVINFO_DATA pDevInfoData)
{
    HDEVINFO deviceInfo = INVALID_HANDLE_VALUE;
    BOOL status = TRUE;
    ULONG deviceIndex;
    SP_DEVINFO_DATA deviceInfoData;
    BOOL bResult = FALSE;
    PCHAR pDriverName = NULL;
    LPTSTR buf = NULL;
    BOOL done = FALSE;

    if (pDevInfo == NULL)
    {
        return false;
    }

    if (pDevInfoData == NULL)
    {
        return false;
    }

    memset(pDevInfoData, 0, sizeof(SP_DEVINFO_DATA));

    *pDevInfo = INVALID_HANDLE_VALUE;

    // Use local string to guarantee zero termination
    /*pDriverName = (PCHAR) new uint8_t[(DWORD) cbDriverName + 1];
    if (NULL == pDriverName)
    {
    status = FALSE;
    goto Done;
    }*/

    // StringCbCopyN(pDriverName, cbDriverName + 1, DriverName, cbDriverName);

    //
    // We cannot walk the device tree with CM_Get_Sibling etc. unless we assume
    // the device tree will stabilize. Any devnode removal (even outside of USB)
    // would force us to retry. Instead we use Setup API to snapshot all
    // devices.
    //

    // Examine all present devices to see if any match the given DriverName
    //
    deviceInfo = SetupDiGetClassDevs(NULL, NULL, NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT);

    if (deviceInfo == INVALID_HANDLE_VALUE)
    {
        status = FALSE;
        goto Done;
    }

    deviceIndex = 0;
    deviceInfoData.cbSize = sizeof(deviceInfoData);

    while (done == FALSE)
    {
        //
        // Get devinst of the next device
        //

        status = SetupDiEnumDeviceInfo(deviceInfo, deviceIndex, &deviceInfoData);

        deviceIndex++;

        if (!status)
        {
            //
            // This could be an error, or indication that all devices have been
            // processed. Either way the desired device was not found.
            //

            done = TRUE;
            break;
        }

        //
        // Get the DriverName value
        //

        bResult = GetDeviceProperty(deviceInfo, &deviceInfoData, SPDRP_DRIVER, &buf);

        // If the DriverName value matches, return the DeviceInstance
        //
        if (bResult == TRUE && buf != NULL && (DriverName == buf))
        {
            done = TRUE;
            *pDevInfo = deviceInfo;
            CopyMemory(pDevInfoData, &deviceInfoData, sizeof(deviceInfoData));
            FREE(buf);
            break;
        }

        if (buf != NULL)
        {
            FREE(buf);
            buf = NULL;
        }
    }

Done:

    if (bResult == FALSE)
    {
        if (deviceInfo != INVALID_HANDLE_VALUE)
        {
            SetupDiDestroyDeviceInfoList(deviceInfo);
        }
    }

    /*if (pDriverName != NULL)
    {
    FREE(pDriverName);
    }*/

    return (status == TRUE);
}

USBDevicePNPStrings *DriverNameToDeviceProperties(const std::string &DriverName)
{
    HDEVINFO deviceInfo = INVALID_HANDLE_VALUE;
    SP_DEVINFO_DATA deviceInfoData = {0};
    ULONG len;
    BOOL status;
    USBDevicePNPStrings *dev_props = nullptr;
    DWORD lastError;
    size_t cbDriverName = DriverName.size();
    std::vector<char> device_id;

    // Allocate device properties structure
    /*dev_props = new USBDevicePNPStrings();

    if (nullptr == dev_props)
    {
        status = FALSE;
        goto Done;
    } */

    // Get device instance
    status = DriverNameToDeviceInst(DriverName, cbDriverName, &deviceInfo, &deviceInfoData);
    if (status == FALSE)
    {
        goto Done;
    }

    len = 0;
    status = SetupDiGetDeviceInstanceId(deviceInfo, &deviceInfoData, NULL, 0, &len);
    lastError = GetLastError();

    if (status != FALSE && lastError != ERROR_INSUFFICIENT_BUFFER)
    {
        status = FALSE;
        goto Done;
    }

    //
    // An extra byte is required for the terminating character
    //

    len++;
    device_id.reserve(len);
    device_id.resize(len);
    // PWCHAR device_id = new WCHAR[len];

    /*if (device_id == nullptr)
    {
        status = FALSE;
        goto Done;
    } */

    status = SetupDiGetDeviceInstanceId(deviceInfo, &deviceInfoData, device_id.data(), len, &len);
    if (status == FALSE)
    {
        goto Done;
    }

    dev_props = USBDevicePNPStrings::New();

    dev_props->SetDeviceId(device_id.data());

    status = GetDeviceProperty(deviceInfo, &deviceInfoData, SPDRP_DEVICEDESC, dev_props->GetDeviceDesc());

    if (status == FALSE)
    {
        goto Done;
    }

    //
    // We don't fail if the following registry query fails as these fields are additional information only
    //

    GetDeviceProperty(deviceInfo, &deviceInfoData, SPDRP_HARDWAREID, dev_props->GetHWId());

    GetDeviceProperty(deviceInfo, &deviceInfoData, SPDRP_SERVICE, dev_props->GetService());

    GetDeviceProperty(deviceInfo, &deviceInfoData, SPDRP_CLASS, dev_props->GetDeviceClass());
Done:

    if (deviceInfo != INVALID_HANDLE_VALUE)
    {
        SetupDiDestroyDeviceInfoList(deviceInfo);
    }

    if (status == FALSE)
    {
        if (dev_props != nullptr)
        {
            delete dev_props;
            return nullptr;
        }
    }
    return dev_props;
}

std::string GetRootHubName(HANDLE HostController)
{
    BOOL success = 0;
    ULONG nBytes = 0;
    USB_ROOT_HUB_NAME rootHubName;
    PUSB_ROOT_HUB_NAME rootHubNameW = NULL;
    PCHAR rootHubNameA = NULL;
    std::string name;
    std::wstring name_w;

    // Get the length of the name of the Root Hub attached to the
    // Host Controller
    //
    success = DeviceIoControl(HostController, IOCTL_USB_GET_ROOT_HUB_NAME, 0, 0, &rootHubName, sizeof(rootHubName),
                              &nBytes, NULL);

    if (!success)
    {
        OOPS();
        goto GetRootHubNameError;
    }

    // Allocate space to hold the Root Hub name
    //
    nBytes = rootHubName.ActualLength;

    rootHubNameW = (PUSB_ROOT_HUB_NAME)ALLOC(nBytes);
    if (rootHubNameW == NULL)
    {
        OOPS();
        goto GetRootHubNameError;
    }

    // Get the name of the Root Hub attached to the Host Controller
    //
    success =
        DeviceIoControl(HostController, IOCTL_USB_GET_ROOT_HUB_NAME, NULL, 0, rootHubNameW, nBytes, &nBytes, NULL);
    if (!success)
    {
        OOPS();
        goto GetRootHubNameError;
    }

    // Convert the Root Hub name
    //
    // rootHubNameA = WideStrToMultiStr(rootHubNameW->RootHubName, nBytes - sizeof(USB_ROOT_HUB_NAME) + sizeof(WCHAR));
    name_w = rootHubNameW->RootHubName;
    name = boost::locale::conv::utf_to_utf<char>(name_w);

    // All done, free the uncoverted Root Hub name and return the
    // converted Root Hub name
    //
    FREE(rootHubNameW);

    return name;

GetRootHubNameError:
    // There was an error, free anything that was allocated
    //
    if (rootHubNameW != NULL)
    {
        FREE(rootHubNameW);
        rootHubNameW = NULL;
    }
    return "";
}

DevInfo *USBEnum::FindDevInfoByDriverName(const std::string &DriverKeyName, bool IsHub)
{
    DevInfoList *dev_info_list;
    DevInfo *result;

    if (IsHub == true)
        dev_info_list = &m_hub_dev_info;
    else
        dev_info_list = &m_usb_dev_info;

    result = dev_info_list->FindByDriverName(DriverKeyName);
    return result;
}

/*DeviceInfoNode *USBEnum::FindMatchingDeviceNodeForDriverName(const std::string &DriverKeyName, BOOLEAN IsHub)
{
    DeviceInfoNode *pNode = nullptr;
    DeviceGUIDList *pList = nullptr;
    PLIST_ENTRY       pEntry = NULL;

    pList = IsHub ? &m_hub_list : &m_device_list;

    pEntry = pList->m_list_head.Flink;

    while (pEntry != &pList->m_list_head)
    {
        pNode = CONTAINING_RECORD(pEntry,
            DeviceInfoNode,
            m_list_entry);
        if (_wcsicmp(DriverKeyName.c_str(), pNode->m_device_driver_name) == 0)
        {
            return pNode;
        }

        pEntry = pEntry->Flink;
    }

    return NULL;
} */

void EnumerateHub(
    // HTREEITEM                                       hTreeParent,
    USBTreeItem *parent,
    //_In_reads_(cbHubName) PCHAR                     HubName,
    //_In_ size_t                                     cbHubName,
    const std::string &HubName, _In_opt_ PUSB_NODE_CONNECTION_INFORMATION_EX ConnectionInfo,
    _In_opt_ PUSB_NODE_CONNECTION_INFORMATION_EX_V2 ConnectionInfoV2,
    _In_opt_ PUSB_PORT_CONNECTOR_PROPERTIES PortConnectorProps, _In_opt_ PUSB_DESCRIPTOR_REQUEST ConfigDesc,
    _In_opt_ PUSB_DESCRIPTOR_REQUEST BosDesc, _In_opt_ StringDescriptorNode *StringDescs,
    _In_opt_ USBDevicePNPStrings *DevProps);

void USBEnum::EnumerateHubPorts(USBTreeItem *parent, HANDLE hHubDevice, ULONG NumPorts)
{
    ULONG index = 0;
    BOOL success = 0;
    HRESULT hr = S_OK;
    // PCHAR       driverKeyName = NULL;
    std::string driverKeyName;
    USBDevicePNPStrings *DevProps = nullptr;
    DWORD dwSizeOfLeafName = 0;
    char leafName[512];
    int icon = 0;

    PUSB_NODE_CONNECTION_INFORMATION_EX connectionInfoEx;
    PUSB_PORT_CONNECTOR_PROPERTIES pPortConnectorProps;
    USB_PORT_CONNECTOR_PROPERTIES portConnectorProps;
    PUSB_DESCRIPTOR_REQUEST configDesc;
    PUSB_DESCRIPTOR_REQUEST bosDesc;
    StringDescriptorNode *stringDescs = nullptr;
    USBDeviceInfo *info = nullptr;
    PUSB_NODE_CONNECTION_INFORMATION_EX_V2 connectionInfoExV2;
    // DeviceInfoNode                          *pNode;
    DevInfo *dev_info;
    // Loop over all ports of the hub.
    //
    // Port indices are 1 based, not 0 based.
    //
    for (index = 1; index <= NumPorts; index++)
    {
        ULONG nBytesEx;
        ULONG nBytes = 0;

        connectionInfoEx = NULL;
        pPortConnectorProps = NULL;
        ZeroMemory(&portConnectorProps, sizeof(portConnectorProps));
        configDesc = NULL;
        bosDesc = NULL;
        stringDescs = NULL;
        info = NULL;
        connectionInfoExV2 = NULL;
        // pNode = NULL;
        dev_info = nullptr;
        DevProps = NULL;
        ZeroMemory(leafName, sizeof(leafName));

        //
        // Allocate space to hold the connection info for this port.
        // For now, allocate it big enough to hold info for 30 pipes.
        //
        // Endpoint numbers are 0-15.  Endpoint number 0 is the standard
        // control endpoint which is not explicitly listed in the Configuration
        // Descriptor.  There can be an IN endpoint and an OUT endpoint at
        // endpoint numbers 1-15 so there can be a maximum of 30 endpoints
        // per device configuration.
        //
        // Should probably size this dynamically at some point.
        //

        nBytesEx = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) + (sizeof(USB_PIPE_INFO) * 30);

        connectionInfoEx = (PUSB_NODE_CONNECTION_INFORMATION_EX)ALLOC(nBytesEx);

        if (connectionInfoEx == NULL)
        {
            OOPS();
            break;
        }

        connectionInfoExV2 =
            (PUSB_NODE_CONNECTION_INFORMATION_EX_V2)ALLOC(sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2));

        if (connectionInfoExV2 == NULL)
        {
            OOPS();
            FREE(connectionInfoEx);
            break;
        }

        //
        // Now query USBHUB for the structures
        // for this port.  This will tell us if a device is attached to this
        // port, among other things.
        // The fault tolerate code is executed first.
        //

        portConnectorProps.ConnectionIndex = index;

        success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES, &portConnectorProps,
                                  sizeof(USB_PORT_CONNECTOR_PROPERTIES), &portConnectorProps,
                                  sizeof(USB_PORT_CONNECTOR_PROPERTIES), &nBytes, NULL);

        if (success && nBytes == sizeof(USB_PORT_CONNECTOR_PROPERTIES))
        {
            pPortConnectorProps = (PUSB_PORT_CONNECTOR_PROPERTIES)ALLOC(portConnectorProps.ActualLength);

            if (pPortConnectorProps != NULL)
            {
                pPortConnectorProps->ConnectionIndex = index;

                success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_PORT_CONNECTOR_PROPERTIES, pPortConnectorProps,
                                          portConnectorProps.ActualLength, pPortConnectorProps,
                                          portConnectorProps.ActualLength, &nBytes, NULL);

                if (!success || nBytes < portConnectorProps.ActualLength)
                {
                    FREE(pPortConnectorProps);
                    pPortConnectorProps = NULL;
                }
            }
        }

        connectionInfoExV2->ConnectionIndex = index;
        connectionInfoExV2->Length = sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2);
        connectionInfoExV2->SupportedUsbProtocols.Usb300 = 1;

        success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX_V2, connectionInfoExV2,
                                  sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2), connectionInfoExV2,
                                  sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2), &nBytes, NULL);

        if (!success || nBytes < sizeof(USB_NODE_CONNECTION_INFORMATION_EX_V2))
        {
            FREE(connectionInfoExV2);
            connectionInfoExV2 = NULL;
        }

        connectionInfoEx->ConnectionIndex = index;

        success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX, connectionInfoEx, nBytesEx,
                                  connectionInfoEx, nBytesEx, &nBytesEx, NULL);

        if (success)
        {
            //
            // Since the USB_NODE_CONNECTION_INFORMATION_EX is used to display
            // the device speed, but the hub driver doesn't support indication
            // of superspeed, we overwrite the value if the super speed
            // data structures are available and indicate the device is operating
            // at SuperSpeed.
            //

            if (connectionInfoEx->Speed == UsbHighSpeed && connectionInfoExV2 != NULL
                && (connectionInfoExV2->Flags.DeviceIsOperatingAtSuperSpeedOrHigher))
            /*||
                connectionInfoExV2->Flags.DeviceIsOperatingAtSuperSpeedPluOrHigher)) */
            {
                connectionInfoEx->Speed = UsbSuperSpeed;
            }
        }
        else
        {
            PUSB_NODE_CONNECTION_INFORMATION connectionInfo = NULL;

            // Try using IOCTL_USB_GET_NODE_CONNECTION_INFORMATION
            // instead of IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX
            //

            nBytes = sizeof(USB_NODE_CONNECTION_INFORMATION) + sizeof(USB_PIPE_INFO) * 30;

            connectionInfo = (PUSB_NODE_CONNECTION_INFORMATION)ALLOC(nBytes);

            if (connectionInfo == NULL)
            {
                OOPS();

                FREE(connectionInfoEx);
                if (pPortConnectorProps != NULL)
                {
                    FREE(pPortConnectorProps);
                }
                if (connectionInfoExV2 != NULL)
                {
                    FREE(connectionInfoExV2);
                }
                continue;
            }

            connectionInfo->ConnectionIndex = index;

            success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_NODE_CONNECTION_INFORMATION, connectionInfo, nBytes,
                                      connectionInfo, nBytes, &nBytes, NULL);

            if (!success)
            {
                OOPS();

                FREE(connectionInfo);
                FREE(connectionInfoEx);
                if (pPortConnectorProps != NULL)
                {
                    FREE(pPortConnectorProps);
                }
                if (connectionInfoExV2 != NULL)
                {
                    FREE(connectionInfoExV2);
                }
                continue;
            }

            // Copy IOCTL_USB_GET_NODE_CONNECTION_INFORMATION into
            // IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX structure.
            //
            connectionInfoEx->ConnectionIndex = connectionInfo->ConnectionIndex;
            connectionInfoEx->DeviceDescriptor = connectionInfo->DeviceDescriptor;
            connectionInfoEx->CurrentConfigurationValue = connectionInfo->CurrentConfigurationValue;
            connectionInfoEx->Speed = connectionInfo->LowSpeed ? UsbLowSpeed : UsbFullSpeed;
            connectionInfoEx->DeviceIsHub = connectionInfo->DeviceIsHub;
            connectionInfoEx->DeviceAddress = connectionInfo->DeviceAddress;
            connectionInfoEx->NumberOfOpenPipes = connectionInfo->NumberOfOpenPipes;
            connectionInfoEx->ConnectionStatus = connectionInfo->ConnectionStatus;

            memcpy(&connectionInfoEx->PipeList[0], &connectionInfo->PipeList[0], sizeof(USB_PIPE_INFO) * 30);

            FREE(connectionInfo);
        }

        // Update the count of connected devices
        //
        if (connectionInfoEx->ConnectionStatus == DeviceConnected)
        {
            m_total_devices_connected++;
        }

        if (connectionInfoEx->DeviceIsHub)
        {
            m_total_hubs++;
        }

        // If there is a device connected, get the Device Description
        //
        if (connectionInfoEx->ConnectionStatus != NoDeviceConnected)
        {
            driverKeyName = GetDriverKeyName(hHubDevice, index);

            if (!driverKeyName.empty())
            {
                size_t cbDriverName = 0;

                hr = StringCbLength(driverKeyName.c_str(), MAX_DRIVER_KEY_NAME, &cbDriverName);
                if (SUCCEEDED(hr))
                {
                    DevProps = DriverNameToDeviceProperties(driverKeyName);
                    dev_info = FindDevInfoByDriverName(driverKeyName, connectionInfoEx->DeviceIsHub);
                }
                // FREE(driverKeyName);
            }
        }

        // If there is a device connected to the port, try to retrieve the
        // Configuration Descriptor from the device.
        //
        if (m_do_config_desc && (connectionInfoEx->ConnectionStatus == DeviceConnected))
        {
            configDesc = GetConfigDescriptor(hHubDevice, index, 0);
        }
        else
        {
            configDesc = NULL;
        }

        if (configDesc != NULL && connectionInfoEx->DeviceDescriptor.bcdUSB > 0x0200)
        {
            bosDesc = GetBOSDescriptor(hHubDevice, index);
        }
        else
        {
            bosDesc = NULL;
        }

        if (!connectionInfoEx->DeviceIsHub)
        {
            info = new USBDeviceInfo();
            info->SetUSBDevicePNPStrings(DevProps);
        }

        if (configDesc != NULL
            && AreThereStringDescriptors(&connectionInfoEx->DeviceDescriptor,
                                         (PUSB_CONFIGURATION_DESCRIPTOR)(configDesc + 1)))
        {
            stringDescs = GetAllStringDescriptors(hHubDevice, index, &connectionInfoEx->DeviceDescriptor,
                                                  (PUSB_CONFIGURATION_DESCRIPTOR)(configDesc + 1));
        }
        else
        {
            stringDescs = NULL;
        }

        // If the device connected to the port is an external hub, get the
        // name of the external hub and recursively enumerate it.
        //
        if (connectionInfoEx->DeviceIsHub)
        {
            // PCHAR extHubName;
            std::string extHubName;
            size_t cbHubName = 0;

            extHubName = GetExternalHubName(hHubDevice, index);
            if (!extHubName.empty())
            {
                hr = StringCbLength(extHubName.c_str(), MAX_DRIVER_KEY_NAME, &cbHubName);
                if (SUCCEEDED(hr))
                {
                    EnumerateHub(parent, // hPortItem,
                                 extHubName,
                                 // cbHubName,
                                 connectionInfoEx, connectionInfoExV2, pPortConnectorProps, configDesc, bosDesc,
                                 stringDescs, DevProps);
                }
            }
        }
        else
        {
            // Allocate some space for a USBDEVICEINFO structure to hold the
            // hub info, hub name, and connection info pointers.  GPTR zero
            // initializes the structure for us.
            //
            // info = (USBDeviceInfo *)ALLOC(sizeof(USBDeviceInfo));
            assert(info != nullptr);
            if (info == nullptr) info = new USBDeviceInfo();

            if (info == NULL)
            {
                OOPS();
                if (configDesc != NULL)
                {
                    FREE(configDesc);
                }
                if (bosDesc != NULL)
                {
                    FREE(bosDesc);
                }
                FREE(connectionInfoEx);

                if (pPortConnectorProps != NULL)
                {
                    FREE(pPortConnectorProps);
                }
                if (connectionInfoExV2 != NULL)
                {
                    FREE(connectionInfoExV2);
                }
                break;
            }

            info->m_device_info_type = DeviceInfo;
            info->m_connection_info = connectionInfoEx;
            info->m_port_connector_props = pPortConnectorProps;
            info->m_config_desc = configDesc;
            // info->SetUSBDevicePNPStrings(DevProps);
            // info->m_usb_device_properties = DevProps;
            info->SetStringDescs(stringDescs);
            // info->m_string_descs = stringDescs;
            info->m_bos_desc = bosDesc;
            info->m_connection_info_v2 = connectionInfoExV2;

            // info->m_device_info_node = pNode;

            info->m_dev_info = dev_info;

            StringCchPrintf(leafName, sizeof(leafName), "[Port%d] ", index);

            // Add error description if ConnectionStatus is other than NoDeviceConnected / DeviceConnected
            StringCchCat(leafName, sizeof(leafName), g_ConnectionStatuses[connectionInfoEx->ConnectionStatus].c_str());

            if (DevProps)
            {
                size_t cchDeviceDesc = 0;

                hr = StringCbLength(DevProps->GetDeviceDesc().c_str(), MAX_DEVICE_PROP, &cchDeviceDesc);
                if (FAILED(hr))
                {
                    OOPS();
                }
                dwSizeOfLeafName = sizeof(leafName);
                StringCchCatN(leafName, dwSizeOfLeafName - 1, " :  ", sizeof(" :  "));
                StringCchCatN(leafName, dwSizeOfLeafName - 1, DevProps->GetDeviceDesc().c_str(), cchDeviceDesc);
            }

            if (connectionInfoEx->ConnectionStatus == NoDeviceConnected)
            {
                if (connectionInfoExV2 != NULL && connectionInfoExV2->SupportedUsbProtocols.Usb300 == 1)
                {
                    // icon = NoSsDeviceIcon;
                }
                else
                {
                    // icon = NoDeviceIcon;
                }
            }
            else if (connectionInfoEx->CurrentConfigurationValue)
            {
                if (connectionInfoEx->Speed == UsbSuperSpeed)
                {
                    // icon = GoodSsDeviceIcon;
                }
                else
                {
                    // icon = GoodDeviceIcon;
                }
            }
            else
            {
                // icon = BadDeviceIcon;
            }

            /*AddLeaf(hTreeParent, //hPortItem,
                (LPARAM)info,
                leafName,
                icon); */
            parent->AddDevice(info, leafName);
        }
    } // for
}

void USBEnum::EnumerateHub(
    // HTREEITEM                                       hTreeParent,
    USBTreeItem *parent,
    //_In_reads_(cbHubName) PCHAR                     HubName,
    //_In_ size_t                                     cbHubName,
    const std::string &HubName, _In_opt_ PUSB_NODE_CONNECTION_INFORMATION_EX ConnectionInfo,
    _In_opt_ PUSB_NODE_CONNECTION_INFORMATION_EX_V2 ConnectionInfoV2,
    _In_opt_ PUSB_PORT_CONNECTOR_PROPERTIES PortConnectorProps, _In_opt_ PUSB_DESCRIPTOR_REQUEST ConfigDesc,
    _In_opt_ PUSB_DESCRIPTOR_REQUEST BosDesc, _In_opt_ StringDescriptorNode *StringDescs,
    _In_opt_ USBDevicePNPStrings *DevProps)
{
    // Initialize locals to not allocated state so the error cleanup routine
    // only tries to cleanup things that were successfully allocated.
    //
    // PUSB_NODE_INFORMATION    hubInfo = NULL;
    // PUSB_HUB_INFORMATION_EX  hubInfoEx = NULL;
    // PUSB_HUB_CAPABILITIES_EX hubCapabilityEx = NULL;
    HANDLE hHubDevice = INVALID_HANDLE_VALUE;
    HTREEITEM hItem = NULL;
    USBExternalHUBInfo *info = nullptr;
    PCHAR deviceName = NULL;
    ULONG nBytes = 0;
    BOOL success = 0;
    DWORD dwSizeOfLeafName = 0;
    // CHAR                    leafName[512] = { 0 };
    std::string leafName(512, 0);
    HRESULT hr = S_OK;
    size_t cchHeader = 0;
    size_t cchFullHubName = 0;

    info = new USBExternalHUBInfo();
    if (info == NULL)
    {
        OOPS();
        goto EnumerateHubError;
    }

    // Allocate some space for a USB_NODE_INFORMATION structure for this Hub
    //
    /*hubInfo = (PUSB_NODE_INFORMATION)ALLOC(sizeof(USB_NODE_INFORMATION));
    if (hubInfo == NULL)
    {
        OOPS();
        goto EnumerateHubError;
    } */

    /*hubInfoEx = (PUSB_HUB_INFORMATION_EX)ALLOC(sizeof(USB_HUB_INFORMATION_EX));
    if (hubInfoEx == NULL)
    {
        OOPS();
        goto EnumerateHubError;
    } */

    /*hubCapabilityEx = (PUSB_HUB_CAPABILITIES_EX)ALLOC(sizeof(USB_HUB_CAPABILITIES_EX));
    if (hubCapabilityEx == NULL)
    {
        OOPS();
        goto EnumerateHubError;
    } */

    // Keep copies of the Hub Name, Connection Info, and Configuration
    // Descriptor pointers
    //
    //((USBRootHUBInfo *)info)->m_hub_info = hubInfo;
    //((USBRootHUBInfo *)info)->m_hub_name = HubName;
    info->SetName(HubName);

    if (ConnectionInfo != NULL)
    {
        info->SetDeviceInfoType(ExternalHubInfo);
        //((USBExternalHUBInfo *)info)->m_device_info_type = ExternalHubInfo;
        ((USBExternalHUBInfo *)info)->m_connection_info = ConnectionInfo;
        ((USBExternalHUBInfo *)info)->m_config_desc = ConfigDesc;
        ((USBExternalHUBInfo *)info)->m_string_descs = StringDescs;
        ((USBExternalHUBInfo *)info)->m_port_connector_props = PortConnectorProps;
        //((USBExternalHUBInfo *)info)->m_hub_info_ex = hubInfoEx;
        info->SetHUBInfoExValid(true);
        //((USBExternalHUBInfo *)info)->m_hub_capability_ex = hubCapabilityEx;
        info->SetHUBCapabilitiesExValid(true);
        ((USBExternalHUBInfo *)info)->m_bos_desc = BosDesc;
        ((USBExternalHUBInfo *)info)->m_connection_info_v2 = ConnectionInfoV2;
        ((USBExternalHUBInfo *)info)->m_usb_device_properties = DevProps;
    }
    else
    {
        info->SetDeviceInfoType(RootHubInfo);
        //((USBRootHUBInfo *)info)->m_device_info_type = RootHubInfo;
        //((USBRootHUBInfo *)info)->m_hub_info_ex = hubInfoEx;
        info->SetHUBInfoExValid(true);
        //((USBRootHUBInfo *)info)->m_hub_capability_ex = hubCapabilityEx;
        info->SetHUBCapabilitiesExValid(true);
        ((USBRootHUBInfo *)info)->m_port_connector_props = PortConnectorProps;
        ((USBRootHUBInfo *)info)->m_usb_device_properties = DevProps;
    }

    // Allocate a temp buffer for the full hub device name.
    //
    hr = StringCbLength("\\\\.\\", MAX_DEVICE_PROP, &cchHeader);
    if (FAILED(hr))
    {
        goto EnumerateHubError;
    }
    cchFullHubName = cchHeader + HubName.length() + 1;
    deviceName = (PCHAR)ALLOC((DWORD)cchFullHubName * sizeof(CHAR));
    if (deviceName == NULL)
    {
        OOPS();
        goto EnumerateHubError;
    }

    // Create the full hub device name
    //
    hr = StringCchCopyN(deviceName, cchFullHubName, "\\\\.\\", cchHeader);
    if (FAILED(hr))
    {
        goto EnumerateHubError;
    }
    hr = StringCchCatN(deviceName, cchFullHubName, HubName.c_str(), HubName.length());
    if (FAILED(hr))
    {
        goto EnumerateHubError;
    }

    // Try to hub the open device
    //
    hHubDevice = CreateFile(deviceName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

    // Done with temp buffer for full hub device name
    //
    FREE(deviceName);

    if (hHubDevice == INVALID_HANDLE_VALUE)
    {
        OOPS();
        goto EnumerateHubError;
    }

    //
    // Now query USBHUB for the USB_NODE_INFORMATION structure for this hub.
    // This will tell us the number of downstream ports to enumerate, among
    // other things.
    //
    success =
        DeviceIoControl(hHubDevice, IOCTL_USB_GET_NODE_INFORMATION, &info->GetHUBInfo(), sizeof(USB_NODE_INFORMATION),
                        &info->GetHUBInfo(), sizeof(USB_NODE_INFORMATION), &nBytes, NULL);

    if (!success)
    {
        OOPS();
        goto EnumerateHubError;
    }

    success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_HUB_INFORMATION_EX, &info->GetHUBInfoEx(),
                              sizeof(USB_HUB_INFORMATION_EX), &info->GetHUBInfoEx(), sizeof(USB_HUB_INFORMATION_EX),
                              &nBytes, NULL);

    //
    // Fail gracefully for downlevel OS's from Win8
    //
    if ((!success) || (nBytes < sizeof(USB_HUB_INFORMATION_EX)))
    {
        // FREE(hubInfoEx);
        // hubInfoEx = NULL;
        if (ConnectionInfo != NULL)
        {
            //((USBExternalHUBInfo *)info)->m_hub_info_ex = NULL;
            info->SetHUBInfoExValid(false);
        }
        else
        {
            //((USBRootHUBInfo *)info)->m_hub_info_ex = NULL;
            info->SetHUBInfoExValid(false);
        }
    }

    //
    // Obtain Hub Capabilities
    //
    success = DeviceIoControl(hHubDevice, IOCTL_USB_GET_HUB_CAPABILITIES_EX, &info->GetHUBCapabilitiesEx(),
                              sizeof(USB_HUB_CAPABILITIES_EX), &info->GetHUBCapabilitiesEx(),
                              sizeof(USB_HUB_CAPABILITIES_EX), &nBytes, NULL);

    //
    // Fail gracefully
    //
    if ((!success) || (nBytes < sizeof(USB_HUB_CAPABILITIES_EX)))
    {
        // FREE(hubCapabilityEx);
        // hubCapabilityEx = NULL;
        if (ConnectionInfo != NULL)
        {
            //((USBExternalHUBInfo *)info)->m_hub_capability_ex = NULL;
            info->SetHUBCapabilitiesExValid(false);
        }
        else
        {
            //((USBRootHUBInfo *)info)->m_hub_capability_ex = NULL;
            info->SetHUBCapabilitiesExValid(false);
        }
    }

    // Build the leaf name from the port number and the device description
    //
    dwSizeOfLeafName = (DWORD)leafName.capacity(); // sizeof(leafName);
    if (ConnectionInfo)
    {
        StringCchPrintf((char *)leafName.c_str(), dwSizeOfLeafName, "[Port%d] ", ConnectionInfo->ConnectionIndex);
        StringCchCat((char *)leafName.c_str(), dwSizeOfLeafName,
                     g_ConnectionStatuses[ConnectionInfo->ConnectionStatus].c_str());
        StringCchCatN((char *)leafName.c_str(), dwSizeOfLeafName, " :  ", sizeof(" :  "));
    }

    if (DevProps)
    {
        size_t cbDeviceDesc = 0;
        hr = StringCbLength(DevProps->GetDeviceDesc().c_str(), MAX_DRIVER_KEY_NAME, &cbDeviceDesc);
        if (SUCCEEDED(hr))
        {
            StringCchCatN((char *)leafName.c_str(), dwSizeOfLeafName, DevProps->GetDeviceDesc().c_str(), cbDeviceDesc);
        }
    }
    else
    {
        if (ConnectionInfo != NULL)
        {
            // External hub
            StringCchCatN((char *)leafName.c_str(), dwSizeOfLeafName, HubName.c_str(), HubName.length());
        }
        else
        {
            // Root hub
            StringCchCatN((char *)leafName.c_str(), dwSizeOfLeafName, "RootHub", sizeof("RootHub"));
        }
    }

    // Now add an item to the TreeView with the PUSBDEVICEINFO pointer info
    // as the LPARAM reference value containing everything we know about the
    // hub.
    //
    /*hItem = AddLeaf(hTreeParent,
        (LPARAM)info,
        leafName,
        HubIcon);

    if (hItem == NULL)
    {
        OOPS();
        goto EnumerateHubError;
    } */
    USBTreeItem *child;
    if (ConnectionInfo != nullptr)
        child = parent->AddExternalHUB((USBExternalHUBInfo *)info);
    else
        child = parent->AddRootHUB((USBRootHUBInfo *)info);
    // Now recursively enumerate the ports of this hub.
    //
    EnumerateHubPorts(child, hHubDevice, info->GetHUBInfo().u.HubInformation.HubDescriptor.bNumberOfPorts);

    CloseHandle(hHubDevice);
    return;

EnumerateHubError:
    //
    // Clean up any stuff that got allocated
    //

    if (hHubDevice != INVALID_HANDLE_VALUE)
    {
        CloseHandle(hHubDevice);
        hHubDevice = INVALID_HANDLE_VALUE;
    }

    /*if (hubInfo)
    {
        FREE(hubInfo);
    }*/

    /*if (hubInfoEx)
    {
        FREE(hubInfoEx);
    }*/

    if (info)
    {
        delete info;
    }

    /*if (HubName)
    {
        FREE(HubName);
    } */

    if (ConnectionInfo)
    {
        FREE(ConnectionInfo);
    }

    if (ConfigDesc)
    {
        FREE(ConfigDesc);
    }

    if (BosDesc)
    {
        FREE(BosDesc);
    }

    if (StringDescs != NULL)
    {
        StringDescriptorNode *Next;

        do
        {

            Next = StringDescs->m_next;
            FREE(StringDescs);
            StringDescs = Next;

        } while (StringDescs != NULL);
    }
}

class ESYS_API USBHostCtrl
{
public:
    USBHostCtrl();
    virtual ~USBHostCtrl();

    // private:
    HANDLE m_hHCDev;
    // PCHAR m_leafName;
    std::string m_leafName;
    HANDLE m_deviceInfo;
    PSP_DEVINFO_DATA m_deviceInfoData;

    USBDeviceInfoType m_DeviceInfoType;
    LIST_ENTRY m_ListEntry;
    PTSTR m_DriverKey;
    ULONG m_VendorID;
    ULONG m_DeviceID;
    ULONG m_SubSysID;
    ULONG m_Revision;
};

USBHostCtrl::USBHostCtrl()
{
}

USBHostCtrl::~USBHostCtrl()
{
}

USBEnum USBEnum::s_usb_enum;
bool USBEnum::s_enumerate_host_controllers = false;

USBEnum &USBEnum::Get()
{
    return s_usb_enum;
}

void USBEnum::SetEnumerateHostControllers(bool enumerate_host_controllers)
{
    s_enumerate_host_controllers = enumerate_host_controllers;
}

bool USBEnum::GetEnumerateHostControllers()
{
    return s_enumerate_host_controllers;
}

USBEnum::USBEnum()
    : multios::USBEnumBase()
    , m_total_devices_connected(0)
    , m_total_hubs(0)
    , m_do_config_desc(TRUE)
    , m_do_annotation(FALSE)
    , m_log_debug(FALSE)
{
    m_enumerated_hc_list_head = {&m_enumerated_hc_list_head, &m_enumerated_hc_list_head};
}

USBEnum::~USBEnum()
{
    USBHostCtrl *hc;
    uint32_t i;

    for (i = 0; i < m_hc_ctrls.size(); ++i)
    {
        hc = m_hc_ctrls.at(i);
        if (hc != NULL) delete hc;
    }

    USBDevicePNPStrings::Release();
    // Delete by USBTreeItemBase
    /*for (i = 0; i < m_hc_list.size(); ++i)
    {
        //FREE((HGLOBAL)m_hc_list[i].m_info);
        delete m_hc_list[i].m_info;
    }*/
}

void USBEnum::SetDebug(bool debug)
{
    m_debug = debug;
}

// DEFINE_GUID(GUID_DEVINTERFACE_SERENUM_BUS_ENUMERATOR, 0x4D36E978L, 0xE325, 0x11CE, 0xBF, 0xC1, 0x08, 0x00, 0x2B,
// 0xE1, 0x03, 0x18);

int32_t USBEnum::GetHWIds(const std::string &buf, uint16_t &vid, uint16_t &pid, uint16_t &rev, bool &is_rev_set,
                          uint16_t &mi, bool &is_mi_set)
{
    std::string s, s1;
    std::stringstream ss;

    std::size_t i, pos;

    is_rev_set = false;
    is_mi_set = false;

    s = buf;
    i = s.find("VID_");
    pos = 4;
    if (i == std::string::npos)
    {
        i = s.find("VID");
        pos = 3;
        if (i == std::string::npos) return -1;
    }
    s = s.substr(i + pos);
    ss.str(s.substr(0, 4));
    ss.seekg(0);
    ss >> std::hex >> vid;

    i = s.find("PID_");
    pos = 4;
    if (i == std::string::npos)
    {
        i = s.find("PID");
        pos = 3;
        if (i == std::string::npos) return -2;
    }
    s = s.substr(i + pos);
    s1 = s.substr(0, 4);
    ss.str(s1);
    ss.seekg(0);
    ss >> std::hex >> pid;

    i = s.find("REV_");
    pos = 4;
    if (i == std::string::npos)
    {
        i = s.find("REV");
        pos = 3;
    }
    if (i != std::string::npos)
    {
        s = s.substr(i + pos);
        ss.str(s.substr(0, 4));
        ss.seekg(0);
        ss >> std::dec >> rev;
        is_rev_set = true;
    }

    i = s.find("MI_");
    pos = 3;
    if (i == std::string::npos)
    {
        i = s.find("MI");
        pos = 2;
    }
    if (i != std::string::npos)
    {
        s = s.substr(i + pos);
        ss.str(s.substr(0, 2));
        ss.seekg(0);
        ss >> std::dec >> mi;
        is_mi_set = true;
    }

    return 0;
}

int USBEnum::Refresh()
{
    DBG_CALLMEMBER_RET("esys::USBEnum::Refresh", false, int16_t, result);
    DBG_CALLMEMBER_END;
    HDEVINFO dev_info;
    SP_DEVINFO_DATA dev_info_data;
    SP_DEVICE_INTERFACE_DATA dev_interface_data;
    DWORD flags;
    int dev_idx = 0;
    DWORD reg_type;
    DWORD length;
    DWORD last_error;
    char buf[3 * MAX_PATH + 1];
    wchar_t buf_w[3 * MAX_PATH + 1];
    USBDevInfo *dev_info_obj;
    std::string class_;
    std::string loc_path;
    std::string dev_id;
    std::string phy_dev_obj_name;
    std::string unique_id;
    uint16_t vid = 0;
    uint16_t pid = 0;
    uint16_t rev = 0;
    uint16_t mi = 0;
    bool is_rev_set = false;
    bool is_mi_set = false;
    bool is_com_port = false;
    std::map<std::string, USBDevInfo *> found_devs;
    SerialPortEnum &port_enum = SerialPortEnum::Get();
    USBDevInfo *info;
    std::vector<std::string> location_paths;

    port_enum.Refresh();

    if (GetFilterMngr() != nullptr) GetFilterMngr()->ClearFiltered();

    if (m_debug)
    {
        uint32_t idx;

        std::cout << "[COMPortEnum] number of ports = " << port_enum.GetCount() << std::endl;
        for (idx = 0; idx < port_enum.GetCount(); ++idx)
        {
            std::cout << "[" << idx << "] " << port_enum.GetPortName(idx) << std::endl;
        }
        std::cout << std::endl;
    }

    if (GetEnumerateHostControllers() == true) EnumerateHostControllers();

    flags = DIGCF_ALLCLASSES | DIGCF_PRESENT;

    dev_info = SetupDiGetClassDevs(NULL, _T("USB"), NULL, flags);
    // dev_info = SetupDiGetClassDevs(&GUID_DEVINTERFACE_SERENUM_BUS_ENUMERATOR, _T("USB"), NULL, flags);
    // Serial port class {4d36e978-e325-11ce-bfc1-08002be10318}
    // GUID_DEVINTERFACE_SERENUM_BUS_ENUMERATOR
    if (dev_info == INVALID_HANDLE_VALUE)
    {
        result = -1;
        return result;
    }

    ZeroMemory(&dev_info_data, sizeof(SP_DEVINFO_DATA));
    dev_info_data.cbSize = sizeof(SP_DEVINFO_DATA);

    ZeroMemory(&dev_interface_data, sizeof(SP_DEVICE_INTERFACE_DATA));
    dev_interface_data.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

    DevNode *dev_node;

    while (SetupDiEnumDeviceInfo(dev_info, dev_idx, &dev_info_data))
    {
        dev_node = DevNodeMngr::Get().Register(dev_info_data.DevInst);
        is_rev_set = false;
        is_com_port = false;
        info = nullptr;
        loc_path = "";
        phy_dev_obj_name = "";
        length = 0;

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_HARDWAREID, &reg_type, (BYTE *)buf,
                                             3 * MAX_PATH, &length))
        {
            if (m_debug) std::cout << _T("USB Device ") << dev_idx << _T(" = ") << buf << std::endl;

            if (GetHWIds(buf, vid, pid, rev, is_rev_set, mi, is_mi_set) < 0)
            {
                dev_idx++;
                continue;
            }
        }

        DWORD MemberIndex = 0;

        while (SetupDiEnumDeviceInterfaces(dev_info, &dev_info_data,
                                           NULL, //_In_      const GUID *InterfaceClassGuid,
                                           MemberIndex, &dev_interface_data))
        {
            // dev_interface_data.
        }

        location_paths.clear();

        BOOL result_setupapi;
        result_setupapi = SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_LOCATION_PATHS, &reg_type,
                                                           (BYTE *)buf, 3 * MAX_PATH, &length);
        if (result_setupapi == FALSE)
        {
            last_error = GetLastError();
        }
        if (result_setupapi)
        {
            loc_path = buf;
            unique_id = loc_path;
            if (m_debug) std::cout << "    Location Paths= " << std::endl;
            char *p;

            p = buf;
            while (*p != 0)
            {
                if (m_debug) std::cout << "                    " << p << std::endl;
                location_paths.push_back(p);
                p += strlen(p);
            }
        }
        else
        {
            dev_idx++;
            continue;
        }

        dev_idx++;
        bool add_it = false;

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_PHYSICAL_DEVICE_OBJECT_NAME, &reg_type,
                                             (BYTE *)buf, MAX_PATH, &length))
        {
            if (m_debug) std::cout << "    Phy Dev Obj Name = " << buf << std::endl;
            phy_dev_obj_name = buf;
            if (loc_path.empty() == true) unique_id = phy_dev_obj_name;
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_CLASS, &reg_type, (BYTE *)buf, MAX_PATH,
                                             &length))
        {
            // std::map<std::string, USBDevInfo *>::iterator it;
            USBDevInfo *usb_dev_info;
            class_ = buf;

            // usb_dev_info=Find(loc_path+_T("__")+dev_id);
            usb_dev_info = Find(unique_id);
            if (usb_dev_info == NULL)
            {
                // No bus dev info is known for this location path
                add_it = true;
            }
            else
            {
                bool same = true;

                if (pid != usb_dev_info->GetUSBAddr().GetPID()) same = false;
                if (vid != usb_dev_info->GetUSBAddr().GetVID()) same = false;
                if ((is_rev_set == true) && (rev != usb_dev_info->GetUSBAddr().GetREV())) same = false;
                if ((is_mi_set == true) && (mi != usb_dev_info->GetUSBAddr().GetMI())) same = false;
                if (same == false)
                {
                    Remove(usb_dev_info);
                    // delete it->second;
                    // m_map_dev_infos.erase(it);    ???
                    add_it = true;
                }
                else
                    // found_devs[loc_path + _T("__") + dev_id] = usb_dev_info;    // it->second;	//Add to the list of
                    // found devices
                    found_devs[unique_id] = usb_dev_info;
            }

            if (add_it == true)
            {
                /*if (COMPortEnum::Get().GetCount(dev_node) > 0)
                {
                    std::string com_port;

                    COMPortEnum::Get().Find(dev_node, com_port);

                    info = new USBDevInfo(USBDevInfo::SERIALPORT);
                    //AddSerial(info);
                    info->SetPortName(com_port);
                    is_com_port = true;
                    dev_info_obj = info;
                }
                else */
                if (strcmp(buf, "Ports") == 0)
                {
                    // USBDevSerialPortInfo *info;
                    // USBDevInfo *info;
                    HKEY key;

                    // info=new USBDevSerialPortInfo();
                    info = new USBDevInfo(USBDevInfo::SERIALPORT);
                    is_com_port = true;
                    // AddSerial(info);
                    // m_dev_serial_port_infos.push_back(info);

                    dev_info_obj = info;

                    key = SetupDiOpenDevRegKey(dev_info, &dev_info_data, DICS_FLAG_GLOBAL, 0, DIREG_DEV /*DIREG_DRV,*/,
                                               KEY_READ);
                    if (key != INVALID_HANDLE_VALUE)
                    {
                        char name[256];
                        DWORD name_size = sizeof(name) - 1;
                        char symb_name[256];
                        DWORD symb_name_size = sizeof(symb_name) - 1;
                        DWORD type;

                        LONG result;

                        result = RegQueryValueEx(key, _T("PortName"), NULL, &type, (BYTE *)name, &name_size);
                        if (result == ERROR_SUCCESS)
                        {
                            // char _name[256];
                            // wcstombs(_name,name,255);
                            // std::cout << "    Port Name = " << name << std::endl;
                            // info->SetPortName(_name);
                            info->SetPortName(name);
                        }
                        result =
                            RegQueryValueEx(key, _T("SymbolicName"), NULL, &type, (BYTE *)symb_name, &symb_name_size);
                        if (result == ERROR_SUCCESS)
                        {
                            // \??\USB#VID_2341&PID_0010#95232343833351502172#{a5dcbf10-6530-11d2-901f-00c04fb951ed}
                            std::string _symb_name = (char *)symb_name;
                            std::string::size_type i;

                            // std::cout << "Symbolic name = " << symb_name << std::endl;
                            i = _symb_name.find("#");
                            if (i != std::string::npos)
                            {
                                _symb_name = _symb_name.substr(i + 1);
                                i = _symb_name.find("#");
                                if (i != std::string::npos)
                                {
                                    _symb_name = _symb_name.substr(i + 1);
                                    i = _symb_name.find("#");
                                    if (i != std::string::npos)
                                    {
                                        _symb_name = _symb_name.substr(0, i);
                                        // std::cout << "Serial number = " << _symb_name << std::endl;
                                    }
                                }
                            }
                        }
                        RegCloseKey(key);
                    }
                }
                else
                {
                    dev_info_obj = new USBDevInfo();
                }
                dev_info_obj->GetUSBAddr().SetVID(vid);
                dev_info_obj->GetUSBAddr().SetPID(pid);
                dev_info_obj->SetDeviceInstance(dev_info_data.DevInst);
                dev_info_obj->SetDevNode(dev_node);
                dev_info_obj->GetUSBAddr().SetREV(rev, is_rev_set);
                dev_info_obj->GetUSBAddr().SetMI(mi, is_mi_set);
                dev_info_obj->SetLocationPath(loc_path);
                dev_info_obj->SetLocationPaths(location_paths);
                // dev_info_obj->SetUID(loc_path+_T("__")+dev_id);
                dev_info_obj->SetUID(unique_id);
                dev_info_obj->SetValid();
                Add(dev_info_obj);
                if (is_com_port == true) AddSerial(info);
                // m_dev_infos.push_back(dev_info_obj);
                // m_map_dev_infos[loc_path+_T("__")+dev_id]=dev_info_obj;
                // found_devs[loc_path+_T("__")+dev_id]=dev_info_obj;
                found_devs[unique_id] = dev_info_obj;
            }
            else
            {
                if (usb_dev_info->GetUSBAddr().IsMISet() == false)
                {
                    if (GetFilterMngr() != nullptr) GetFilterMngr()->Filter(usb_dev_info);
                }
            }
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_DEVICEDESC, &reg_type, (BYTE *)buf,
                                             MAX_PATH, &length))
        {
            if (m_debug) std::cout << _T("    Description   = ") << buf << std::endl;
            if (add_it == true) dev_info_obj->SetDescription(buf);
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_MFG, &reg_type, (BYTE *)buf, MAX_PATH,
                                             &length))
        {
            if (m_debug) std::cout << _T("    Manufacturer  = ") << buf << std::endl;
            if (add_it == true) dev_info_obj->SetManufacturer(buf);
        }

        DEVPROPTYPE ulPropertyType;

        if (SetupDiGetDevicePropertyW(dev_info, &dev_info_data, &DEVPKEY_Device_Manufacturer, &ulPropertyType,
                                      (BYTE *)buf_w, MAX_PATH, &length, 0))
        {
            std::string buf_s = boost::locale::conv::utf_to_utf<char>(buf_w);
            if (m_debug) std::cout << _T("    Device Manufacturer  = ") << buf_s << std::endl;

            /*if (add_it==true)
                dev_info_obj->SetManufacturer(buf); */
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_BUSNUMBER, &reg_type, (BYTE *)buf,
                                             MAX_PATH, &length))
        {
            if (m_debug) std::cout << _T("    Bus Number   = ") << buf << std::endl;
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_CLASSGUID, &reg_type, (BYTE *)buf,
                                             MAX_PATH, &length))
        {
            if (m_debug) std::cout << _T("    Class GUID   = ") << buf << std::endl;
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_COMPATIBLEIDS, &reg_type, (BYTE *)buf,
                                             MAX_PATH, &length))
        {
            if (m_debug)
            {
                std::cout << _T("    Compatible IDs= ") << std::endl;
                char *p;

                p = buf;
                while (*p != 0)
                {
                    std::cout << _T("                    ") << p << std::endl;
                    p += strlen(p);
                }
            }
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_BUSTYPEGUID, &reg_type, (BYTE *)buf,
                                             MAX_PATH, &length))
        {
            /*if (m_debug)
                std::cout << _T("    Bus Type GUID = ") << buf << std::endl; */
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_FRIENDLYNAME, &reg_type, (BYTE *)buf,
                                             MAX_PATH, &length))
        {
            if (m_debug) std::cout << _T("    Friendly Name = ") << buf << std::endl;
            if (add_it == true) dev_info_obj->SetFriendlyName(buf);
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_UI_NUMBER_DESC_FORMAT, &reg_type,
                                             (BYTE *)buf, MAX_PATH, &length))
        {
            if (m_debug) std::cout << _T("    UI Number Desc= ") << buf << std::endl;
            if (add_it == true) dev_info_obj->SetFriendlyName(buf);
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_LOCATION_INFORMATION, &reg_type,
                                             (BYTE *)buf, MAX_PATH, &length))
        {
            if (m_debug) std::cout << _T("    Location      = ") << buf << std::endl;
        }

        /*if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_PHYSICAL_DEVICE_OBJECT_NAME  , &reg_type,
        (BYTE *)buf, MAX_PATH, &length))
        {
            if (m_debug)
                std::cout << ("    Phy Dev Obj Name = ") << buf << std::endl;
            if (add_it==true)
                dev_info_obj->SetPhyDevObjName(buf);
        }*/
        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_DEVTYPE, &reg_type, (BYTE *)buf,
                                             sizeof(DWORD), &length))
        {
            if (m_debug) std::cout << _T("    Device Type   = ") << *(DWORD *)buf << std::endl;
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_ADDRESS, &reg_type, (BYTE *)buf, MAX_PATH,
                                             &length))
        {
            if (m_debug) std::cout << _T("    Address       = ") << *(DWORD *)buf << std::endl;
            if (add_it == true) dev_info_obj->SetDeviceAddress(*(DWORD *)buf);
        }

        if (SetupDiGetDeviceRegistryProperty(dev_info, &dev_info_data, SPDRP_DRIVER, &reg_type, (BYTE *)buf, MAX_PATH,
                                             &length))
        {
            if (m_debug) std::cout << _T("    Driver       = ") << buf << std::endl;
            // dev_info_obj->SetBusNumber(buf);
        }

        /*DEVINST child_instance;
        DEVINST sibling_instance;
        if (CR_SUCCESS == CM_Get_Child(&child_instance, dev_info_data.DevInst, 0))
        {
            ULONG pulLen;
            ULONG ulRegDataType;
            std::string name;

            if (CR_SUCCESS == CM_Get_Device_ID_Size(&pulLen, child_instance, 0))
            {
                WCHAR *buf=new WCHAR[pulLen+1];

                if (CR_SUCCESS == CM_Get_Device_ID(child_instance, buf, pulLen, 0))
                {
                    buf[pulLen]=0;
                    name=buf;
                }
                delete buf;
            }

            length=MAX_PATH;
            if (CR_SUCCESS == CM_Get_DevNode_Registry_Property(child_instance, CM_DRP_CLASS, &ulRegDataType, (BYTE
        *)buf, &length, 0))
            {
                if (wcscmp(buf,"Ports")==0)
                {
                    HKEY key;

                    if (CR_SUCCESS == CM_Open_DevNode_Key(child_instance, KEY_READ, DICS_FLAG_GLOBAL,
        RegDisposition_OpenExisting, &key, CM_REGISTRY_HARDWARE))
                    {
                        wchar_t name[256];
                        DWORD name_size=sizeof(name)-1;
                        DWORD type;
                        LONG result;

                        result=RegQueryValueEx(key,_T("PortName"),NULL,&type,(BYTE *)name,&name_size);
                        if (result==ERROR_SUCCESS)
                        {
                            dev_info_obj->SetPortName(name);
                            //AddSerial(dev_info_obj);
                            //m_dev_serial_port_infos.push_back(dev_info_obj);
                        }

                        RegCloseKey(key);

                    }
                }
            }

            length=MAX_PATH;
            if (CR_SUCCESS == CM_Get_DevNode_Registry_Property(child_instance, CM_DRP_CLASSGUID, &ulRegDataType, (BYTE
        *)buf, &length, 0))
            {

            }


            while (CR_SUCCESS == CM_Get_Sibling(&sibling_instance, child_instance, 0))
            {

                child_instance=sibling_instance;
            }
        } */

        // dev_idx++;
    }
    SetupDiDestroyDeviceInfoList(dev_info);

    FindParentsOfPendingChildren();

    // ResolveParentChildRelations();

    RefreshFound(found_devs);
    result = 0;
    return result;
}

void USBEnum::ResolveParentChildRelations()
{
    uint32_t idx;
    USBDevInfo *dev_info;
    DevNode *dev_node;
    DevNode *dev_node_parent;
    std::map<DevNode *, USBDevInfo *> map_dev_node;
    std::map<DevNode *, USBDevInfo *>::iterator it;

    for (idx = 0; idx < m_dev_infos.size(); ++idx)
    {
        dev_info = m_dev_infos[idx];
        assert(dev_info != nullptr);
        assert(dev_info->GetDevNode());
        map_dev_node[dev_info->GetDevNode()] = dev_info;
    }

    for (idx = 0; idx < m_dev_infos.size(); ++idx)
    {
        dev_info = m_dev_infos[idx];
        dev_node = dev_info->GetDevNode();
        dev_node_parent = dev_node->GetParent();
        assert(dev_node_parent != nullptr);
        it = map_dev_node.find(dev_node_parent);
        if (it != map_dev_node.end())
        {
            assert(it->second != nullptr);

            dev_info->SetParent(it->second);
            it->second->AddChild(dev_info);
        }
    }
}

/*VOID FreeDeviceInfoNode(DeviceInfoNode **ppNode)
{
    if (ppNode == NULL)
    {
        return;
    }

    if (*ppNode == NULL)
    {
        return;
    }

    if ((*ppNode)->m_device_detail_data != NULL)
    {
        FREE((*ppNode)->m_device_detail_data);
    }

    if ((*ppNode)->m_device_desc_name != NULL)
    {
        FREE((*ppNode)->m_device_desc_name);
    }

    if ((*ppNode)->m_device_driver_name != NULL)
    {
        FREE((*ppNode)->m_device_driver_name);
    }

    FREE(*ppNode);
    *ppNode = NULL;
} */

#define IsListEmpty(ListHead) ((ListHead)->Flink == (ListHead))

#define RemoveHeadList(ListHead)           \
    (ListHead)->Flink;                     \
    {                                      \
        RemoveEntryList((ListHead)->Flink) \
    }

#define RemoveEntryList(Entry)        \
    {                                 \
        PLIST_ENTRY _EX_Blink;        \
        PLIST_ENTRY _EX_Flink;        \
        _EX_Flink = (Entry)->Flink;   \
        _EX_Blink = (Entry)->Blink;   \
        _EX_Blink->Flink = _EX_Flink; \
        _EX_Flink->Blink = _EX_Blink; \
    }

#define InsertTailList(ListHead, Entry)  \
    {                                    \
        PLIST_ENTRY _EX_Blink;           \
        PLIST_ENTRY _EX_ListHead;        \
        _EX_ListHead = (ListHead);       \
        _EX_Blink = _EX_ListHead->Blink; \
        (Entry)->Flink = _EX_ListHead;   \
        (Entry)->Blink = _EX_Blink;      \
        _EX_Blink->Flink = (Entry);      \
        _EX_ListHead->Blink = (Entry);   \
    }

/*void ClearDeviceList(DeviceGUIDList *device_list)
{
    if (device_list->m_device_info != INVALID_HANDLE_VALUE)
    {
        SetupDiDestroyDeviceInfoList(device_list->m_device_info);
        device_list->m_device_info = INVALID_HANDLE_VALUE;
    }

    while (!IsListEmpty(&device_list->m_list_head))
    {
        DeviceInfoNode *pNode = NULL;
        PLIST_ENTRY pEntry;

        pEntry = RemoveHeadList(&device_list->m_list_head);

        pNode = CONTAINING_RECORD(pEntry,
            DeviceInfoNode,
            m_list_entry);

        FreeDeviceInfoNode(&pNode);
    }
} */

void USBEnum::EnumerateAllDevicesWithGuid(DevInfoList &dev_info_list, LPGUID guid)
{
    DevInfo *dev_info;
    HDEVINFO device_info;

    device_info = SetupDiGetClassDevs(guid, NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

    dev_info_list.SetDeviceInfo(device_info);

    if (device_info == INVALID_HANDLE_VALUE) return;

    ULONG index;
    DWORD error;

    error = 0;
    index = 0;

    while (error != ERROR_NO_MORE_ITEMS)
    {
        BOOL success;

        dev_info = new DevInfo();

        success = SetupDiEnumDeviceInfo(dev_info_list.GetDeviceInfo(), index, &dev_info->GetData());

        index++;

        if (success == FALSE)
        {
            error = GetLastError();

            if (error != ERROR_NO_MORE_ITEMS)
            {
                OOPS();
            }
            delete dev_info;
            continue;
        }

        BOOL bResult;
        ULONG requiredLength;
        PSTR device_desc_name;

        bResult =
            GetDeviceProperty(dev_info_list.GetDeviceInfo(), &dev_info->GetData(), SPDRP_DEVICEDESC, &device_desc_name);

        if (bResult == FALSE)
        {
            delete dev_info;
            OOPS();
            break;
        }

        dev_info->SetDescName(device_desc_name);

        PSTR device_driver_name;
        bResult =
            GetDeviceProperty(dev_info_list.GetDeviceInfo(), &dev_info->GetData(), SPDRP_DRIVER, &device_driver_name);

        if (bResult == FALSE)
        {
            delete dev_info;
            OOPS();
            break;
        }

        dev_info->SetDriverName(device_driver_name);

        PSTR friendly_name;
        bResult =
            GetDeviceProperty(dev_info_list.GetDeviceInfo(), &dev_info->GetData(), SPDRP_FRIENDLYNAME, &friendly_name);

        if (bResult == TRUE) dev_info->SetFriendlyName(friendly_name);

        success = SetupDiEnumDeviceInterfaces(dev_info_list.GetDeviceInfo(), 0, guid, index - 1,
                                              &dev_info->GetInterfaceData());

        if (!success)
        {
            delete dev_info;
            OOPS();
            break;
        }

        success = SetupDiGetDeviceInterfaceDetail(dev_info_list.GetDeviceInfo(), &dev_info->GetInterfaceData(), NULL, 0,
                                                  &requiredLength, NULL);

        error = GetLastError();

        if (!success && error != ERROR_INSUFFICIENT_BUFFER)
        {
            delete dev_info;
            OOPS();
            break;
        }

        dev_info->SetDetailDataSize(requiredLength);

        success = SetupDiGetDeviceInterfaceDetail(dev_info_list.GetDeviceInfo(), &dev_info->GetInterfaceData(),
                                                  dev_info->GetDetailData(), requiredLength, &requiredLength, NULL);

        if (!success)
        {
            delete dev_info;
            OOPS();
            break;
        }

        dev_info_list.Add(dev_info);
    }
}

void USBEnum::EnumerateAllDevices()
{
    EnumerateAllDevicesWithGuid(m_usb_dev_info, (LPGUID)&GUID_DEVINTERFACE_USB_DEVICE);

    EnumerateAllDevicesWithGuid(m_hub_dev_info, (LPGUID)&GUID_DEVINTERFACE_USB_HUB);
}

DWORD GetHostControllerInfo(HANDLE hHCDev, USBHostControllerInfo *hcInfo)
{
    USBUSER_CONTROLLER_INFO_0 UsbControllerInfo;
    DWORD dwError = 0;
    DWORD dwBytes = 0;
    BOOL bSuccess = FALSE;

    memset(&UsbControllerInfo, 0, sizeof(UsbControllerInfo));

    // set the header and request sizes
    UsbControllerInfo.Header.UsbUserRequest = USBUSER_GET_CONTROLLER_INFO_0;
    UsbControllerInfo.Header.RequestBufferLength = sizeof(UsbControllerInfo);

    //
    // Query for the USB_CONTROLLER_INFO_0 structure
    //
    bSuccess = DeviceIoControl(hHCDev, IOCTL_USB_USER_REQUEST, &UsbControllerInfo, sizeof(UsbControllerInfo),
                               &UsbControllerInfo, sizeof(UsbControllerInfo), &dwBytes, NULL);

    if (!bSuccess)
    {
        dwError = GetLastError();
        OOPS();
    }
    else
    {
        hcInfo->m_controller_info = (PUSB_CONTROLLER_INFO_0)ALLOC(sizeof(USB_CONTROLLER_INFO_0));
        if (NULL == hcInfo->m_controller_info)
        {
            dwError = GetLastError();
            OOPS();
        }
        else
        {
            // copy the data into our USB Host Controller's info structure
            memcpy(hcInfo->m_controller_info, &UsbControllerInfo.Info0, sizeof(USB_CONTROLLER_INFO_0));
        }
    }
    return dwError;
}

void USBEnum::EnumerateHostControllers()
{
    HANDLE hHCDev = NULL;
    HDEVINFO deviceInfo = NULL;
    SP_DEVINFO_DATA deviceInfoData;
    SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA dev_detail_data = NULL;
    ULONG index = 0;
    ULONG requiredLength = 0;
    BOOL success;

    m_total_devices_connected = 0;
    m_total_hubs = 0;

    EnumerateAllDevices(); // ?? why needed

    // Iterate over host controllers using the new GUID based interface
    //
    deviceInfo = SetupDiGetClassDevs((LPGUID)&GUID_CLASS_USB_HOST_CONTROLLER, NULL, NULL,
                                     (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

    deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

    for (index = 0; SetupDiEnumDeviceInfo(deviceInfo, index, &deviceInfoData); index++)
    {
        deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

        success = SetupDiEnumDeviceInterfaces(deviceInfo, 0, (LPGUID)&GUID_CLASS_USB_HOST_CONTROLLER, index,
                                              &deviceInterfaceData);

        if (!success)
        {
            break;
        }

        success = SetupDiGetDeviceInterfaceDetail(deviceInfo, &deviceInterfaceData, NULL, 0, &requiredLength, NULL);

        if (!success && GetLastError() != ERROR_INSUFFICIENT_BUFFER)
        {
            break;
        }

        dev_detail_data = (PSP_DEVICE_INTERFACE_DETAIL_DATA) new uint8_t[requiredLength];
        if (dev_detail_data == NULL)
        {
            break;
        }

        dev_detail_data->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

        success = SetupDiGetDeviceInterfaceDetail(deviceInfo, &deviceInterfaceData, dev_detail_data, requiredLength,
                                                  &requiredLength, NULL);

        if (!success)
        {
            break;
        }

        hHCDev = CreateFile(dev_detail_data->DevicePath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

        // If the handle is valid, then we've successfully opened a Host
        // Controller.  Display some info about the Host Controller itself,
        // then enumerate the Root Hub attached to the Host Controller.
        //
        if (hHCDev != INVALID_HANDLE_VALUE)
        {
            USBHostCtrl *hc = new USBHostCtrl();

            hc->m_hHCDev = hHCDev;
            hc->m_leafName = dev_detail_data->DevicePath;
            hc->m_deviceInfo = deviceInfo;
            hc->m_deviceInfoData = &deviceInfoData;

            m_hc_ctrls.push_back(hc);

            EnumerateHostController(hc);
            /*EnumerateHostController(hHCDev,
                                    dev_detail_data->DevicePath,
                                    deviceInfo,
                                    &deviceInfoData); */

            CloseHandle(hHCDev);
        }

        delete dev_detail_data;
    }

    SetupDiDestroyDeviceInfoList(deviceInfo);

    //*DevicesConnected = TotalDevicesConnected;

    // return;
}

std::string GetHCDDriverKeyName(HANDLE HCD)
{
    BOOL success = 0;
    ULONG nBytes = 0;
    USB_HCD_DRIVERKEY_NAME driverKeyName = {0};
    PUSB_HCD_DRIVERKEY_NAME driverKeyNameW = NULL;
    PCHAR driverKeyNameA = NULL;
    std::string name;
    std::wstring name_w;

    ZeroMemory(&driverKeyName, sizeof(driverKeyName));

    // Get the length of the name of the driver key of the HCD
    //
    success = DeviceIoControl(HCD, IOCTL_GET_HCD_DRIVERKEY_NAME, &driverKeyName, sizeof(driverKeyName), &driverKeyName,
                              sizeof(driverKeyName), &nBytes, NULL);

    if (!success)
    {
        // OOPS();
        goto GetHCDDriverKeyNameError;
    }

    // Allocate space to hold the driver key name
    //
    nBytes = driverKeyName.ActualLength;
    if (nBytes <= sizeof(driverKeyName))
    {
        // OOPS();
        goto GetHCDDriverKeyNameError;
    }

    // Allocate size of name plus 1 for terminal zero
    driverKeyNameW = (PUSB_HCD_DRIVERKEY_NAME) new WCHAR[sizeof(USB_HCD_DRIVERKEY_NAME) + nBytes + 1];
    if (driverKeyNameW == NULL)
    {
        // OOPS();
        goto GetHCDDriverKeyNameError;
    }

    // Get the name of the driver key of the device attached to
    // the specified port.
    //

    success = DeviceIoControl(HCD, IOCTL_GET_HCD_DRIVERKEY_NAME, driverKeyNameW, nBytes, driverKeyNameW, nBytes,
                              &nBytes, NULL);
    if (!success)
    {
        // OOPS();
        goto GetHCDDriverKeyNameError;
    }

    // Convert the driver key name
    //
    // driverKeyNameA = WideStrToMultiStr(driverKeyNameW->DriverKeyName, nBytes);

    // All done, free the uncoverted driver key name and return the
    // converted driver key name
    //
    name_w = driverKeyNameW->DriverKeyName;
    name = boost::locale::conv::utf_to_utf<char>(name_w);
    delete driverKeyNameW;

    return name;

GetHCDDriverKeyNameError:
    // There was an error, free anything that was allocated
    //
    if (driverKeyNameW != NULL)
    {
        delete driverKeyNameW;
        driverKeyNameW = NULL;
    }

    return NULL;
}

/*
typedef struct _USB_DEVICE_PNP_STRINGS
{
    PCHAR DeviceId;
    PCHAR DeviceDesc;
    PCHAR HwId;
    PCHAR Service;
    PCHAR DeviceClass;
    PCHAR PowerState;
} USB_DEVICE_PNP_STRINGS, *PUSB_DEVICE_PNP_STRINGS; */

DWORD GetHostControllerPowerMap(HANDLE hHCDev, USBHostControllerInfo *hcInfo)
{
    USBUSER_POWER_INFO_REQUEST UsbPowerInfoRequest;
    PUSB_POWER_INFO pUPI = &UsbPowerInfoRequest.PowerInformation;
    DWORD dwError = 0;
    DWORD dwBytes = 0;
    BOOL bSuccess = FALSE;
    int nIndex = 0;
    int nPowerState = WdmUsbPowerSystemWorking;

    for (; nPowerState <= WdmUsbPowerSystemShutdown; nIndex++, nPowerState++)
    {
        // zero initialize our request
        memset(&UsbPowerInfoRequest, 0, sizeof(UsbPowerInfoRequest));

        // set the header and request sizes
        UsbPowerInfoRequest.Header.UsbUserRequest = USBUSER_GET_POWER_STATE_MAP;
        UsbPowerInfoRequest.Header.RequestBufferLength = sizeof(UsbPowerInfoRequest);
        UsbPowerInfoRequest.PowerInformation.SystemState = (WDMUSB_POWER_STATE)nPowerState;

        //
        // Now query USBHUB for the USB_POWER_INFO structure for this hub.
        // For Selective Suspend support
        //
        bSuccess = DeviceIoControl(hHCDev, IOCTL_USB_USER_REQUEST, &UsbPowerInfoRequest, sizeof(UsbPowerInfoRequest),
                                   &UsbPowerInfoRequest, sizeof(UsbPowerInfoRequest), &dwBytes, NULL);

        if (!bSuccess)
        {
            dwError = GetLastError();
            OOPS();
        }
        else
        {
            // copy the data into our USB Host Controller's info structure
            memcpy(&(hcInfo->m_usb_power_info[nIndex]), pUPI, sizeof(USB_POWER_INFO));
        }
    }

    return dwError;
}

void USBEnum::EnumerateHostController(USBHostCtrl *hc)
{
    std::string driverKeyName;
    HTREEITEM hHCItem = NULL;
    std::string rootHubName;
    PLIST_ENTRY listEntry = NULL;
    USBHostControllerInfo *hcInfo = NULL;
    USBHostControllerInfo *hcInfoInList = NULL;
    DWORD dwSuccess;
    BOOL success = FALSE;
    ULONG deviceAndFunction = 0;
    USBDevicePNPStrings *DevProps = NULL;

    std::map<std::string, USBHostCtrl *>::iterator it;

    // Allocate a structure to hold information about this host controller.
    //
    // hcInfo = (USBHostControllerInfo *)ALLOC(sizeof(USBHostControllerInfo));
    // hcInfo = (PUSBHOSTCONTROLLERINFO)new uint8_t[sizeof(USBHOSTCONTROLLERINFO)];
    hcInfo = new USBHostControllerInfo();

    //// just return if could not alloc memory
    if (NULL == hcInfo) return;

    hcInfo->m_device_info_type = HostControllerInfo;

    // Obtain the driver key name for this host controller.
    //
    driverKeyName = GetHCDDriverKeyName(hc->m_hHCDev);

    if (driverKeyName.empty())
    {
        // Failure obtaining driver key name.
        // OOPS();
        // FREE(hcInfo);
        delete hcInfo;
        return;
    }

    // Don't enumerate this host controller again if it already
    // on the list of enumerated host controllers.
    //
    listEntry = m_enumerated_hc_list_head.Flink;

    while (listEntry != &m_enumerated_hc_list_head)
    {
        hcInfoInList = CONTAINING_RECORD(listEntry, USBHostControllerInfo, m_list_entry);

        // if (strcmp(driverKeyName.c_str(), hcInfoInList->m_driver_key) == 0)
        if (strcmp(driverKeyName.c_str(), hcInfoInList->m_driver_key.c_str()) == 0)
        {
            // Already on the list, exit
            //
            // FREE(driverKeyName);
            // FREE(hcInfo);
            delete hcInfo;
            return;
        }

        listEntry = listEntry->Flink;
    }
    it = m_map_hc_ctrls.find(driverKeyName);
    if (it != m_map_hc_ctrls.end())
    {
        // FREE(hcInfo);
        delete hcInfo;
        return;
    }
    // Obtain host controller device properties
    {
        size_t cbDriverName = 0;
        HRESULT hr = S_OK;

        // hr = StringCbLengthW(driverKeyName, MAX_DRIVER_KEY_NAME, &cbDriverName);
        // if (SUCCEEDED(hr))
        //{
        DevProps = DriverNameToDeviceProperties(driverKeyName);
        //}
    }

    hcInfo->m_driver_key = driverKeyName;

    if (DevProps)
    {
        ULONG ven, dev, subsys, rev;
        ven = dev = subsys = rev = 0;

        if (sscanf_s(DevProps->GetDeviceId().c_str(), "PCI\\VEN_%x&DEV_%x&SUBSYS_%x&REV_%x", &ven, &dev, &subsys, &rev)
            != 4)
        {
            OOPS();
        }

        hcInfo->m_vendor_id = ven;
        hcInfo->m_device_id = dev;
        hcInfo->m_sub_sys_id = subsys;
        hcInfo->m_revision = rev;
        hcInfo->m_usb_device_properties = DevProps;
    }
    else
    {
        OOPS();
    }

    if ((DevProps != nullptr) && (DevProps->GetDeviceDesc().empty() == false))
    {
        hc->m_leafName = DevProps->GetDeviceDesc();
    }
    else
    {
        OOPS();
    }

    // Get the USB Host Controller power map
    dwSuccess = GetHostControllerPowerMap(hc->m_hHCDev, hcInfo);

    if (ERROR_SUCCESS != dwSuccess)
    {
        OOPS();
    }

    // Get bus, device, and function
    //
    hcInfo->m_bus_device_function_valid = FALSE;

    success = SetupDiGetDeviceRegistryProperty(hc->m_deviceInfo, hc->m_deviceInfoData, SPDRP_BUSNUMBER, NULL,
                                               (PBYTE)&hcInfo->m_bus_number, sizeof(hcInfo->m_bus_number), NULL);

    if (success)
    {
        success = SetupDiGetDeviceRegistryProperty(hc->m_deviceInfo, hc->m_deviceInfoData, SPDRP_ADDRESS, NULL,
                                                   (PBYTE)&deviceAndFunction, sizeof(deviceAndFunction), NULL);
    }

    if (success)
    {
        hcInfo->m_bus_device = deviceAndFunction >> 16;
        hcInfo->m_bus_function = deviceAndFunction & 0xffff;
        hcInfo->m_bus_device_function_valid = TRUE;
    }

    // Get the USB Host Controller info
    dwSuccess = GetHostControllerInfo(hc->m_hHCDev, hcInfo);

    if (ERROR_SUCCESS != dwSuccess)
    {
        OOPS();
    }

    // Add this host controller to the USB device tree view.
    //
    /*hHCItem = AddLeaf(hTreeParent,
                      (LPARAM)hcInfo,
                      leafName,
                      hcInfo->Revision == UsbSuperSpeed ? GoodSsDeviceIcon : GoodDeviceIcon);

    if (NULL == hHCItem)
    {
        // Failure adding host controller to USB device tree
        // view.

        OOPS();
        //FREE(driverKeyName);
        FREE(hcInfo);
        return;
    } */
    USBTreeItem *tree_item = AddHostController(hcInfo, hc->m_leafName);

    // Add this host controller to the list of enumerated
    // host controllers.
    //
    InsertTailList(&m_enumerated_hc_list_head, &hcInfo->m_list_entry);

    // Get the name of the root hub for this host
    // controller and then enumerate the root hub.
    //
    rootHubName = GetRootHubName(hc->m_hHCDev);

    if (!rootHubName.empty())
    {
        size_t cbHubName = 0;
        HRESULT hr = S_OK;

        // hr = StringCbLength(rootHubName, MAX_DRIVER_KEY_NAME, &cbHubName);
        // if (SUCCEEDED(hr))
        //{
        EnumerateHub(tree_item, rootHubName,
                     // cbHubName,
                     NULL,  // ConnectionInfo
                     NULL,  // ConnectionInfoV2
                     NULL,  // PortConnectorProps
                     NULL,  // ConfigDesc
                     NULL,  // BosDesc
                     NULL,  // StringDescs
                     NULL); // We do not pass DevProps for RootHub
        //}
    }
    else
    {
        // Failure obtaining root hub name.

        OOPS();
    }

    return;
}

USBTreeItem *USBEnum::AddHostController(USBHostControllerInfo *hc_info, const std::string &leaf_name)
{
    HCInfo info;
    USBTreeItem *hc;

    info.m_name = leaf_name;
    info.m_info = hc_info;

    m_hc_list.push_back(info);

    hc = new USBTreeItem();
    hc->SetName(leaf_name);
    hc->SetInfo(hc_info);

    GetTree().AddChild(hc);
    return hc;
}

USBTree &USBEnum::GetTree()
{
    return m_root_tree;
}

} // namespace esys
