/*!
 * \file esys/win32/devinfolist.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/devinfolist.h"
#include "esys/win32/devinfo.h"

#include <assert.h>

namespace esys
{

DevInfoList::DevInfoList(): m_device_info(INVALID_HANDLE_VALUE)
{
}

DevInfoList::~DevInfoList()
{
    DevInfo *dev_info;
    uint32_t idx;

    if (m_device_info != INVALID_HANDLE_VALUE)
    {
        SetupDiDestroyDeviceInfoList(m_device_info);
        m_device_info = INVALID_HANDLE_VALUE;
    }

    for (idx = 0; idx < m_dev_infos.size(); ++idx)
    {
        dev_info = m_dev_infos[idx];
        if (dev_info != nullptr)
            delete dev_info;
    }
}

void DevInfoList::SetDeviceInfo(HDEVINFO device_info)
{
    if (m_device_info != INVALID_HANDLE_VALUE)
    {
        SetupDiDestroyDeviceInfoList(m_device_info);
    }
    m_device_info = device_info;
}

HDEVINFO DevInfoList::GetDeviceInfo()
{
    return m_device_info;
}

void DevInfoList::Add(DevInfo *dev_info)
{
    assert(dev_info != nullptr);

    m_dev_infos.push_back(dev_info);
    if (dev_info->GetDriverName().empty() == false)
        m_dev_infos_driver_name_map[dev_info->GetDriverName()] = dev_info;
}

DevInfo *DevInfoList::FindByDriverName(const std::string &driver_name)
{
    std::map<std::string, DevInfo *>::iterator it;

    it = m_dev_infos_driver_name_map.find(driver_name);
    if (it == m_dev_infos_driver_name_map.end())
        return nullptr;
    return it->second;
}

}



