/*!
 * \file esys/win32/usbroothubinfo.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/usbroothubinfo.h"

namespace esys
{

USBRootHUBInfo::USBRootHUBInfo()
{
}

USBRootHUBInfo::~USBRootHUBInfo()
{
}

void USBRootHUBInfo::SetDeviceInfoType(USBDeviceInfoType device_info_type)
{
    m_device_info_type = device_info_type;
}

USBDeviceInfoType USBRootHUBInfo::GetDeviceInfoType()
{
    return m_device_info_type;
}

USB_NODE_INFORMATION &USBRootHUBInfo::GetHUBInfo()
{
    return m_hub_info;
}

bool USBRootHUBInfo::GetHUBInfoValid()
{
    return m_hub_info_valid;
}

void USBRootHUBInfo::SetHUBInfoValid(bool value)
{
    m_hub_info_valid = value;
}

USB_HUB_INFORMATION_EX &USBRootHUBInfo::GetHUBInfoEx()
{
    return m_hub_info_ex;
}

bool USBRootHUBInfo::GetHUBInfoExValid()
{
    return m_hub_info_ex_valid;
}

void USBRootHUBInfo::SetHUBInfoExValid(bool value)
{
    m_hub_info_ex_valid = value;
}

USB_HUB_CAPABILITIES_EX &USBRootHUBInfo::GetHUBCapabilitiesEx()
{
    return m_hub_capabilities_ex;
}

bool USBRootHUBInfo::GetHUBCapabilitiesExValid()
{
    return m_hub_capabilities_ex_valid;
}

void USBRootHUBInfo::SetHUBCapabilitiesExValid(bool value)
{
    m_hub_capabilities_ex_valid = value;
}

void USBRootHUBInfo::SetName(const std::string &name)
{
    m_hub_name = name;
}

const std::string &USBRootHUBInfo::GetName() const
{
    return m_hub_name;
}

}



