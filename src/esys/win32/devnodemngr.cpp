/*!
 * \file esys/win32/devnodemngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/devnodemngr.h"

#include <assert.h>

namespace esys
{

DevNodeMngr DevNodeMngr::s_mngr;

DevNodeMngr &DevNodeMngr::Get()
{
    return s_mngr;
}

DevNodeMngr::DevNodeMngr() : DevNode(), m_count(0)
{
}

DevNodeMngr::~DevNodeMngr()
{
    std::vector<DevNode *>::iterator it;

    for (it = m_vec.begin(); it != m_vec.end(); ++it)
    {
        delete *it;
    }
    m_vec.clear();
}

DevNode *DevNodeMngr::NewNode()
{
    DevNode *result = new DevNode();
    m_vec.push_back(result);
    return result;
}

void DevNodeMngr::SetRoot(DevNode *root)
{
    m_root = root;
}

DevNode *DevNodeMngr::GetRoot()
{
    return m_root;
}

int32_t DevNodeMngr::Refresh()
{
    return 0;
}

DevNode *DevNodeMngr::Find(const std::string &instance_id)
{
    std::map<std::string, DevNode *>::iterator it;

    it = m_map.find(instance_id);
    if (it == m_map.end())
        return nullptr;
    return it->second;
}

DevNode *DevNodeMngr::Find(DEVINST dev_inst)
{
    std::string instance_id;
    int32_t result;

    result = win32GetIntanceId(dev_inst, instance_id);
    if (result < 0)
        return nullptr;
    return Find(instance_id);
}

void DevNodeMngr::Register(DevNode *dev_node)
{
    std::map<std::string, DevNode *>::iterator it;

    assert(dev_node != nullptr);

    assert(dev_node->GetInstanceId().empty() == false);

    it = m_map.find(dev_node->GetInstanceId());
    if (it == m_map.end())
    {
        m_map[dev_node->GetInstanceId()] = dev_node;
        m_count++;
    }
}

DevNode *DevNodeMngr::Register(DEVINST dev_inst)
{
    int32_t result;
    DevNode *dev_node = Find(dev_inst);

    if (dev_node != nullptr)
        return dev_node;
    result = IsValid(dev_inst);
    if (result < 0)
        return nullptr;
    dev_node = NewNode();
    dev_node->SetDevInst(dev_inst);
    dev_node->Refresh(RefreshDir::UP);
    Register(dev_node);
    return dev_node;
}

uint32_t DevNodeMngr::GetCount()
{
    return m_count;
}

}



