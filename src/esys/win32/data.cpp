/*!
 * \file esys/win32/data.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/data.h"

namespace esys
{

std::vector<USBDevicePNPStrings *> USBDevicePNPStrings::m_vec;

USBDevicePNPStrings *USBDevicePNPStrings::New()
{
    USBDevicePNPStrings *result = new USBDevicePNPStrings();
    m_vec.push_back(result);

    return result;
}

void USBDevicePNPStrings::Release()
{
    std::vector<USBDevicePNPStrings *>::iterator it;

    for (it = m_vec.begin(); it != m_vec.end(); ++it)
    {
        delete *it;
    }
    m_vec.clear();
}

USBDevicePNPStrings::USBDevicePNPStrings()
{
}

USBDevicePNPStrings::~USBDevicePNPStrings()
{
}

void USBDevicePNPStrings::SetDeviceId(const std::string &device_id)
{
    m_device_id = device_id;
}

std::string &USBDevicePNPStrings::GetDeviceId()
{
    return m_device_id;
}

void USBDevicePNPStrings::SetDeviceDesc(const std::string &device_desc)
{
    m_device_desc = device_desc;
}

std::string &USBDevicePNPStrings::GetDeviceDesc()
{
    return m_device_desc;
}

void USBDevicePNPStrings::SetHWId(const std::string &hw_id)
{
    m_hw_id = hw_id;
}

std::string &USBDevicePNPStrings::GetHWId()
{
    return m_hw_id;
}

void USBDevicePNPStrings::SetService(const std::string &service)
{
    m_service = service;
}

std::string &USBDevicePNPStrings::GetService()
{
    return m_service;
}

void USBDevicePNPStrings::SetDeviceClass(const std::string &device_class)
{
    m_device_class = device_class;
}

std::string &USBDevicePNPStrings::GetDeviceClass()
{
    return m_device_class;
}

void USBDevicePNPStrings::SetPowerState(const std::string &power_state)
{
    m_power_state = power_state;
}

std::string &USBDevicePNPStrings::GetPowerState()
{
    return m_power_state;
}

USBHostControllerInfo::USBHostControllerInfo()
{
}

USBHostControllerInfo::~USBHostControllerInfo()
{
    /*if (m_usb_device_properties != nullptr)
        delete m_usb_device_properties; */
}

}

