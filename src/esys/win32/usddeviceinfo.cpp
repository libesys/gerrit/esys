/*!
 * \file esys/win32/usbdeviceinfo.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (C) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/win32/usbdeviceinfo.h"

namespace esys
{

USBDeviceInfoStringDesc::USBDeviceInfoStringDesc(unsigned char descriptor_index, unsigned short language_id, const std::wstring &value)
    : m_descriptor_index(descriptor_index), m_language_id(language_id), m_value(value)
{
}

USBDeviceInfo::USBDeviceInfo()
{
}

USBDeviceInfo::~USBDeviceInfo()
{
    /*if (m_usb_device_properties != nullptr)
        delete m_usb_device_properties; */
}

void USBDeviceInfo::SetStringDescs(StringDescriptorNode *string_descs)
{

    m_string_descs = string_descs;

    if (m_string_descs == nullptr)
        return;

    while (string_descs != nullptr)
    {
        std::wstring s(string_descs->m_string_descriptor[0].bString, string_descs->m_string_descriptor[0].bLength);

        USBDeviceInfoStringDesc desc(string_descs->m_descriptor_index,
                                     string_descs->m_language_id, s);
        m_string_desc_vec.push_back(desc);
        string_descs = string_descs->m_next;
    }

}

void USBDeviceInfo::SetUSBDevicePNPStrings(USBDevicePNPStrings *props)
{
    m_usb_device_properties = props;
}

}

