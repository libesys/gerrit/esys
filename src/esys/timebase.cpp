/*!
 * \file esys/timebase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/timebase.h"

namespace esys
{

TimeBase::Source TimeBase::m_dft_time_source = MCU_CLK_BASED;

void TimeBase::SetDftTimeSrc(Source dft_time_source)
{
    m_dft_time_source = dft_time_source;
}

TimeBase::Source TimeBase::GetDftTimeSrc()
{
    return m_dft_time_source;
}

// civil_from_days inspired by http://howardhinnant.github.io/date_algorithms.html#civil_from_days
void TimeBase::CivilFromDays(esys::uint64_t z, esys::int32_t *py, esys::uint32_t *pm, esys::uint32_t *pd)
{
//    static_assert(std::numeric_limits<unsigned>::digits >= 18,
    //            "This algorithm has not been ported to a 16 bit unsigned integer");
    //  static_assert(std::numeric_limits<Int>::digits >= 20,
    //         "This algorithm has not been ported to a 16 bit signed integer");
    z += 719468;
    const esys::int32_t era = (z >= 0 ? z : z - 146096) / 146097;
    const esys::uint32_t doe = static_cast<esys::uint32_t>(z - era * 146097);     // [0, 146096]
    const esys::uint32_t yoe = (doe - doe/1460 + doe/36524 - doe/146096) / 365;  // [0, 399]
    const esys::int32_t y = static_cast<esys::int32_t>(yoe) + era * 400;
    const esys::uint32_t doy = doe - (365*yoe + yoe/4 - yoe/100);                // [0, 365]
    const esys::uint32_t mp = (5*doy + 2)/153;                                   // [0, 11]
    const esys::uint32_t d = doy - (153*mp+2)/5 + 1;                             // [1, 31]
    const esys::uint32_t m = mp + (mp < 10 ? 3 : -9);                            // [1, 12]
    *py = y + (m <= 2);
    *pm=m;
    *pd=d;
}

TimeBase::DateTimeStruct::DateTimeStruct(esys::uint64_t ms)
{
    SetEpochMS(ms);
}

void TimeBase::DateTimeStruct::SetEpochMS(esys::uint64_t ms)
{
    esys::uint64_t secs = ms/1000;
    esys::uint64_t mins = secs / 60;
    esys::uint64_t hours = mins/ 60;
    esys::uint64_t days = hours/24;
    esys::uint32_t cyear;
    esys::uint32_t cmonth;
    esys::uint32_t cday;
    CivilFromDays( (esys::uint64_t)days, (esys::int32_t *)&cyear, (esys::uint32_t *)&cmonth, (esys::uint32_t*)&cday);

    hours = hours-24*days;
    mins = mins - 60*(24*days + hours);
    secs = secs - 60*(60*(24*days + hours) + mins);

    m_date.m_year = cyear;
    m_date.m_month = cmonth;
    m_date.m_day = cday;

    m_time.m_hour = hours;
    m_time.m_minutes = mins;
    m_time.m_seconds = secs;
}

}

