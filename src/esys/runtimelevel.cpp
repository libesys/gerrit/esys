/*!
 * \file esys/runtimelevel.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/runtimelevel.h"

namespace esys
{

uint32_t RuntimeLevel::g_count = RuntimeLevel::LAST;
uint32_t RuntimeLevel::GetCount()
{
    return g_count;
}

RuntimeLevel::RuntimeLevel(): m_id(g_count)
{
    ++g_count;
}

RuntimeLevel::RuntimeLevel(const int32_t &id): m_id(id)
{
}

RuntimeLevel::operator int32_t() const
{
    return m_id;
}

RuntimeLevel &RuntimeLevel::operator=(const RuntimeLevel &lvl)
{
    if (this != &lvl)
    {
        m_id = lvl;
    }
    return *this;
}

RuntimeLevel &RuntimeLevel::operator++()
{
    ++m_id;
    return *this;
}

RuntimeLevel RuntimeLevel::operator++(int increment)
{
    m_id += increment;
    return *this;
}

RuntimeLevel &RuntimeLevel::operator--()
{
    --m_id;
    return *this;
}

RuntimeLevel RuntimeLevel::operator--(int decrement)
{
    m_id -= decrement;
    return *this;
}

//const RuntimeLevel RuntimeLevel::NOT_SET(-1);
//const RuntimeLevel RuntimeLevel::ALL(-2);
//const RuntimeLevel RuntimeLevel::ROOT;
//const RuntimeLevel RuntimeLevel::ESYS;
//const RuntimeLevel RuntimeLevel::ESYSIO;
//const RuntimeLevel RuntimeLevel::APP;

}


