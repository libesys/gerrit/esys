/*!
 * \file esys/usbdevserialportinfo.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/usbdevserialportinfo.h"

#include <exception>
#include <string.h>
//#include <Arduino.h>

namespace esys
{

/*USBDevSerialPortInfo::USBDevSerialPortInfo():USBDevSerialPortInfoBase(),USBDevInfo(USBDevInfo::SERIALPORT)
{
}

USBDevSerialPortInfo::~USBDevSerialPortInfo()
{
}

//void USBDevSerialPortInfo::GetPortName(char *name,uint8_t &size)
//{
//    strncpy(name,m_port_name.c_str(),size);
//    //wcstombs
//}
//
//void USBDevSerialPortInfo::SetPortName(const char *name)
//{
//    m_port_name=name;
//}
//
//const std::string &USBDevSerialPortInfo::GetPortName() const
//{
//    return m_port_name;
//}
//
//bool USBDevSerialPortInfo::operator==(const USBDevInfoBase& obj) const
//{
//    return USBDevInfo::operator==(obj);
//}
 */
}
