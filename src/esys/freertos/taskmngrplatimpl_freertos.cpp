/*!
 * \file esys/freetos/taskmngrplatimpl_freertos.cpp
 * \brief Definition of the PIMPL class of the FreeRTOS TaskMngr
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/taskmngrplatimpl.h"

#ifdef WIN32
#include <FreeRTOS/win32/app.h>
#endif

#include <FreeRTOS/task.h>

namespace esys
{

namespace freertos
{

void TaskMngrPlatImpl::ApplicationIdleCallback()
{
}

void TaskMngrPlatImpl::PostSleepProcessing(unsigned long xExpectedIdleTime)
{
}

void TaskMngrPlatImpl::ApplicationMallocFailedHook()
{
}

void TaskMngrPlatImpl::ApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName)
{
}

void TaskMngrPlatImpl::ApplicationTickHook()
{
}

void TaskMngrPlatImpl::AssertCalled()
{
}

TaskMngrPlatImpl::TaskMngrPlatImpl(TaskMngrBase *task_mngr_base)
#ifndef ESYS_VHW
    : TaskMngrPlatIf(), m_taskmngr_base(task_mngr_base)
#else
    : m_taskmngr_base(task_mngr_base)
#endif
{
}

TaskMngrPlatImpl::~TaskMngrPlatImpl()
{
}

int32_t TaskMngrPlatImpl::PlatInit()
{

#if defined(_WIN32) || defined(_WIN64)
    vSetApplicationIdleCallback(TaskMngrPlatImpl::ApplicationIdleCallback);
#endif

#ifdef configKERNEL_END_SCHEDULER
    xTaskInit();
    xTimerInit();
#endif

    return 0;
}

void TaskMngrPlatImpl::Done(TaskBase *task_base)
{

}

int32_t TaskMngrPlatImpl::StartScheduler()
{
    vTaskStartScheduler();
    return 0;
}

void TaskMngrPlatImpl::ExitScheduler()
{
    vTaskEndScheduler();
}

}

}

#ifndef ESYS_VHW

extern "C"
{

    void vApplicationIdleHook(void)
    {
    }

    uint32_t vSystemIdleHook(uint32_t expected_idle_ticks)
    {
        return expected_idle_ticks;
    }

    void vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName)
    {
    }

    void vApplicationMallocFailedHook(void)
    {
    }

    void vApplicationTickHook(void)
    {
    }
}

#endif








