/*!
 * \file esys/freertos/mutex_boost.cpp
 * \brief Declaration of the FreeRTOS Mutex class
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/mutex.h"
#include "esys/freertos/muteximpl.h"
#include "esys/assert.h"

namespace esys
{

namespace freertos
{

Mutex::Mutex(const esys::ObjectName &name, Type type) : esys::MutexBase(name, type)
{
    m_impl = new MutexImpl();
}

Mutex::~Mutex()
{
    delete m_impl;
}

int32_t Mutex::Lock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Lock(from_isr);
}

int32_t Mutex::UnLock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->UnLock(from_isr);
}

int32_t Mutex::TryLock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->TryLock(from_isr);
}

int32_t Mutex::StaticInit(Order order)
{
    assert(m_impl != nullptr);

    return m_impl->StaticInit(order);
}


int32_t Mutex::StaticRelease(Order order)
{
    assert(m_impl != nullptr);

    return m_impl->StaticRelease(order);
}

}

}


