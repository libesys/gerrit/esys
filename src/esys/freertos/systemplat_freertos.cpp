/*!
 * \file esys/freertos/systemplat_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/systemplat.h"

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>

#ifdef WIN32
#include <FreeRTOS/win32/app.h>
#endif

namespace esys
{

namespace freertos
{


SystemPlat::SystemPlat(SystemBase *system_base) : m_system_base(system_base), m_heap5_initialized(false)
{
#ifdef ESYS_FREERTOS_HEAP_5
    /* This demo uses heap_5.c, so start by defining some heap regions.  This
    is only done to provide an example as this demo could easily create one
    large heap region instead of multiple smaller heap regions - in which case
    heap_4.c would be the more appropriate choice.  No initialization is
    required when heap_4.c is used. */
    if (m_heap5_initialized == false)
    {
        prvInitialiseHeap();
        m_heap5_initialized = true;
    }
#endif

#ifdef WIN32
    /* Initialize the trace recorder */
    //vTraceInitTraceData();
#endif
}

SystemPlat::~SystemPlat()
{
}

int32_t SystemPlat::PlatInit()
{
    return 0;
}

int32_t SystemPlat::PlatRelease()
{
    return 0;
}

SystemBase *SystemPlat::GetSystemBase()
{
    return m_system_base;
}

void SystemPlat::SetSystemBase(SystemBase *system_base)
{
    m_system_base = system_base;
}

}

}






