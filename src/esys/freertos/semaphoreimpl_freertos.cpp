/*!
 * \file esys/freertos/semaphoreimpl_freertos.cpp
 * \brief Declaration of the FreeRTOS Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/semaphoreimpl.h"

#include <FreeRTOS/task.h> //???

namespace esys
{

namespace freertos
{

#ifndef ESYS_VHW
SemaphoreImpl::SemaphoreImpl(const ObjectName &name, uint32_t count)
    : SemaphoreBase(name), m_sem(nullptr), m_count(count)
#else
SemaphoreImpl::SemaphoreImpl(uint32_t count)
    : m_sem(nullptr), m_count(count)
#endif
{
}

SemaphoreImpl::~SemaphoreImpl()
{
}

int32_t SemaphoreImpl::Post(bool from_isr)
{
    assert(m_sem != nullptr);

    if (from_isr == false)
    {
        if (xSemaphoreGive(m_sem) != pdTRUE)
            return -1;
    }
    else
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreGiveFromISR(m_sem, &xHigherPriorityTaskWoken) != pdTRUE)
            return -1;

        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    ++m_count;
    return 0;
}

int32_t SemaphoreImpl::Wait(bool from_isr)
{
    assert(m_sem != nullptr);

    if (m_count > 0)
        --m_count;

    if (from_isr == false)
    {
        if (xSemaphoreTake(m_sem, portMAX_DELAY) != pdTRUE)
            return -1;
    }
    else
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreTakeFromISR(m_sem, &xHigherPriorityTaskWoken) != pdTRUE)
            return -1;
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return 0;
}

int32_t SemaphoreImpl::WaitFor(uint32_t ms, bool from_isr)
{
    assert(m_sem != nullptr);

    const TickType_t xDelay = ms / portTICK_PERIOD_MS;

    if (m_count > 0)
        --m_count;

    if (from_isr == false)
    {
        if (xSemaphoreTake(m_sem, xDelay) != pdTRUE)
            return -1;
    }
    else
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreTakeFromISR(m_sem, &xHigherPriorityTaskWoken) != pdTRUE)
            return -1;
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return 0;
}



int32_t SemaphoreImpl::TryWait(bool from_isr)
{
    assert(m_sem != nullptr);
    assert(from_isr == false);

    if (m_count > 0)
        --m_count;

    if (xSemaphoreTake(m_sem, 0) != pdTRUE)
        return -1;
    return 0;
}

uint32_t SemaphoreImpl::Count()
{
    assert(m_sem != nullptr);

    return m_count;
}

int32_t SemaphoreImpl::StaticInit(SemaphoreBase::Order order)
{
    uint32_t max_count = m_count;
    int32_t result = 0;

#ifndef ESYS_VHW
    result = SemaphoreBase::StaticInit(order);
    if (result < 0)
        return result;
#endif
    if (order == SemaphoreBase::AFTER_CHILDREN)
    {
        assert(m_sem == nullptr);
        if (max_count == 0)
            max_count = 1;
        m_sem = xSemaphoreCreateCounting(max_count, m_count);
        if (m_sem == nullptr)
            result = -1;
    }
    return result;
}

int32_t SemaphoreImpl::StaticRelease(SemaphoreBase::Order order)
{
    int32_t result = 0;

#ifndef ESYS_VHW
    result = SemaphoreBase::StaticRelease(order);
    if (result < 0)
        return result;
#endif
    if (order == SemaphoreBase::BEFORE_CHILDREN)
    {
        assert(m_sem != nullptr);
        vSemaphoreDelete(m_sem);
    }

    return result;
}

}

}


