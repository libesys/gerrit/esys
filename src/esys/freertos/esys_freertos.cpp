/*!
 * \file esys/freertos/esys_freeros.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"

#include "esys/freertos/esys.h"
#include "esys/freertos/semaphore.h"
#include "esys/freertos/mutex.h"
#include "esys/freertos/taskplat.h"
#include "esys/freertos/taskmngrplat.h"
#include "esys/freertos/systemplat.h"
#include "esys/freertos/timer.h"
#include "esys/freertos/time.h"
#include "esys/platform/id.h"

#ifdef ESYS_MULTI_PLAT
#include "esys/mp/esys.h"
#endif

namespace esys
{

namespace freertos
{

ESys ESys::s_esys;

ESys::ESys()
#ifdef ESYS_MULTI_PLAT
    : BaseType(ESYS_PLAT_FREERTOS, L"FreeRTOS")
#else
    : BaseType()
#endif
{
}

ESys::~ESys()
{
}

ESys &ESys::Get()
{
    return s_esys;
}

#ifdef ESYS_MULTI_PLAT

void ESys::Select()
{
    esys::mp::ESys::SetPlatform(platform::FREERTOS);
}

SemaphoreBase *ESys::NewSemaphoreBase(const ObjectName &name, uint32_t count)
{
    return new Semaphore(name, count);
}

MutexBase *ESys::NewMutexBase(const ObjectName &name, MutexBase::Type type)
{
    return new Mutex(name, type);
}

TaskPlatIf *ESys::NewTaskPlat(TaskBase *task_base)
{
    return new TaskPlat(task_base);
}

TaskMngrPlatIf *ESys::NewTaskMngrPlat(TaskMngrBase *taskmngr_base)
{
    return new TaskMngrPlat(taskmngr_base);
}

SystemPlatIf *ESys::NewSystemPlat(SystemBase *system_base)
{
    return new SystemPlat(system_base);
}

TimerBase *ESys::NewTimerBase(const ObjectName &name)
{
    return new Timer(name);
}

TaskPlatIf *ESys::GetCurTaskPlatIf()
{
    assert(false); // Not implemented

    return nullptr;
}

uint32_t ESys::implMillis(TimeBase::Source source)
{
    return Time::Millis(source);
}

uint64_t ESys::implMicros(TimeBase::Source source)
{
    return Time::Micros(source);
}

#endif



}

}


