/*!
 * \file esys/freertos/taskmngr.cpp
 * \brief Declaration of the Boost TaskMngr class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/taskmngr.h"

namespace esys
{

namespace freertos
{

TaskMngr::TaskMngr(const ObjectName &name) : TaskMngrBase(name), TaskMngrPlat(this)
{
}

TaskMngr::~TaskMngr()
{
}

}

}


