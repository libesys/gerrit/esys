/*!
 * \file esys/freertos/time_freertos.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/time.h"
#include "esys/assert.h"

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>

namespace esys
{

namespace freertos
{

Time::Time()
{
}

uint32_t Time::Millis(Source source)
{
    if (source == DEFAULT_CLK)
        source = GetDftTimeSrc();

    if (source == MCU_CLK_BASED)
    {
        return xTaskGetTickCount() / portTICK_PERIOD_MS;
    }
    else
    {
        return xTaskGetTickCount() / portTICK_PERIOD_MS;
    }
}

uint64_t Time::Micros(Source source)
{
    if (source == DEFAULT_CLK)
        source = GetDftTimeSrc();

    if (source == MCU_CLK_BASED)
    {
        uint64_t result = xTaskGetTickCount() * 1000;
        result = result / portTICK_PERIOD_MS;
        return result;
    }
    else
    {
        uint64_t result = xTaskGetTickCount() * 1000;
        result = result / portTICK_PERIOD_MS;
        return result;
    }
}

uint32_t Time::MicrosLow()
{
    uint64_t us = Micros();
    us = (us << 32) >> 32;
    return (uint32_t)us;
}

void Time::Sleep(uint32_t ms)
{
    const TickType_t xDelay = ms / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

void Time::USleep(uint32_t us)
{
    /*! \todo find a way to directly wait us and not converting to ms
     */
    const TickType_t xDelay = (us / 1000) / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

int32_t Time::SetTime(TimeStruct &time)
{
    return -1;
}

int32_t Time::GetTime(TimeStruct &time)
{
    return -1;
}

int32_t Time::SetDate(DateStruct &date)
{
    return -1;
}

int32_t Time::GetDate(DateStruct &date)
{
    return -1;
}

int32_t Time::SetDateTime(DateTimeStruct &date_time)
{
    int32_t result;

    result = SetDate(date_time.m_date);
    if (result < 0)
        return -1;
    result = SetTime(date_time.m_time);
    if (result < 0)
        return -2;
    return 0;
}

int32_t Time::GetDateTime(DateTimeStruct &date_time)
{
    int32_t result;

    result = GetTime(date_time.m_time);
    if (result < 0)
        return result;
    result = GetDate(date_time.m_date);
    return result;
}

int32_t Time::StartTimestamp()
{
    return -1;
}

int32_t Time::StopTimestamp()
{
    return -1;
}

}

}

