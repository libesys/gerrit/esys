/*!
 * \file esys/freertos/timerimpl.h
 * \brief The FreeRTOS PIMPL implementation of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/timerimpl.h"
#include "esys/assert.h"

#include <FreeRTOS.h>
#include <FreeRTOS/timers.h>

namespace esys
{

namespace freertos
{

void TimerImpl::Callback(TimerHandle_t timer_handle)
{
    TimerBase *timer;

    timer = (TimerImpl *)pvTimerGetTimerID(timer_handle);
    timer->Notify();
}

TimerImpl::TimerImpl(const char *name)
    : TimerBase(name), m_timer_handle(nullptr), m_name(name), m_period_ms(0), m_one_shot(false)
{
}

TimerImpl::~TimerImpl()
{
    if (m_timer_handle != nullptr)
    {
        xTimerDelete(m_timer_handle, 100);  //!< \TODO find what value it should be
        m_timer_handle = nullptr;
    }
}

int32_t TimerImpl::Start(uint32_t period_ms, bool one_shot, bool from_isr)
{
    portTickType timer_period = period_ms / portTICK_PERIOD_MS;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if (m_timer_handle != nullptr)
    {
        if (one_shot != m_one_shot)
        {
            // One shot parameters have changed
            xTimerDelete(m_timer_handle, 100);  //!< \TODO find what value it should be
            m_timer_handle = nullptr;
        }
        else if (period_ms != m_period_ms)
        {
            if (xTimerChangePeriod(m_timer_handle, timer_period, 100) != pdPASS)
                return -1;
            m_period_ms = period_ms;
        }
    }

    m_one_shot = one_shot;

    if (m_timer_handle == nullptr)
    {
        UBaseType_t auto_reload = pdFALSE;

        // It's not allowed to create a new timer within an interrupt handle
        assert(from_isr == false);

        if (one_shot == false)
            auto_reload = pdTRUE;
        m_timer_handle = xTimerCreate(m_name, timer_period, auto_reload, (TimerImpl *)this, &TimerImpl::Callback);
        assert(m_timer_handle != nullptr);
        if (m_timer_handle == nullptr)
            return -1;
        if (from_isr == false)
        {
            if (xTimerStart(m_timer_handle, 0) != pdPASS)
                return -1;
        }
        else
        {
            if (xTimerStartFromISR(m_timer_handle, &xHigherPriorityTaskWoken) != pdPASS)
                return -1;
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
        m_period_ms = period_ms;
    }
    else
    {
        if (from_isr == false)
        {
            if (xTimerReset(m_timer_handle, 100) != pdPASS) //!< TODO change the 100 to a constant defined somewhere
                return -1;
        }
        else
        {
            if (xTimerResetFromISR(m_timer_handle, &xHigherPriorityTaskWoken) != pdPASS)
                return -1;
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
    }
    return 0;
}

int32_t TimerImpl::Stop(bool from_isr)
{
    BaseType_t result;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    assert(m_timer_handle != nullptr);

    if (m_timer_handle == nullptr)
        return -1;

    if (from_isr == false)
    {
        result = xTimerStop(m_timer_handle, 100);    //!< TODO change the 100 to a constant defined somewhere
        if (result == pdFALSE)
            return -1;
    }
    else
    {
        if (xTimerStopFromISR(m_timer_handle, &xHigherPriorityTaskWoken) != pdPASS)
            return -1;
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return 0;
}

int32_t TimerImpl::GetPeriod()
{
    return m_period_ms;
}

bool TimerImpl::IsRunning()
{
    BaseType_t result;

    if (!m_timer_handle)
        return false;

    result = xTimerIsTimerActive(m_timer_handle);
    if (result == pdFALSE)
        return false;
    return true;
}

bool TimerImpl::IsOneShot()
{
    return m_one_shot;
}

}

}






