/*!
 * \file esys/freertos/timer.h
 * \brief The FreeRTOS declaration of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/timer.h"
#include "esys/freertos/timerimpl.h"
#include "esys/assert.h"

#ifdef ESYS_FREERTOS_PIMPL

namespace esys
{

namespace freertos
{

Timer::Timer(const ObjectName &name) : TimerBase(name), m_one_shot(false)
{
    m_impl = new TimerImpl(name);
}

Timer::~Timer()
{
    delete m_impl;
}

int32_t Timer::Start(uint32_t ms, bool one_shot, bool from_isr)
{
    assert(m_impl != nullptr);

    m_one_shot = one_shot;

    return m_impl->start(ms, one_shot, from_isr);
}

int32_t Timer::Stop(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->stop(from_isr);
}

int32_t Timer::GetPeriod()
{
    assert(m_impl != nullptr);

    return m_impl->period();
}

bool Timer::IsRunning()
{
    assert(m_impl != nullptr);

    return m_impl->is_running();
}

bool Timer::IsOneShot()
{
    return m_one_shot;
}

}

}

#endif



