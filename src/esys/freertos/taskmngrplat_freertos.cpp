/*!
 * \file esys/freertos/taskmngrplat.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/taskmngrplat.h"
#include "esys/freertos/taskmngrplatimpl.h"

#include "esys/assert.h"

namespace esys
{

namespace freertos
{

TaskMngrPlat::TaskMngrPlat(TaskMngrBase *taskmngr_base)//: TaskMngrPlatIf
{
    m_impl = new TaskMngrPlatImpl(taskmngr_base);
}

TaskMngrPlat::~TaskMngrPlat()
{
    delete m_impl;
}

TaskMngrPlatImpl *TaskMngrPlat::GetImpl()
{
    return m_impl;
}

int32_t TaskMngrPlat::StartScheduler()
{
    assert(m_impl != nullptr);

    return m_impl->StartScheduler();
}

void TaskMngrPlat::ExitScheduler()
{
    assert(m_impl != nullptr);

    return m_impl->ExitScheduler();
}

void TaskMngrPlat::Done(TaskBase *task)
{
    assert(m_impl != nullptr);

    m_impl->Done(task);
}



}

}






