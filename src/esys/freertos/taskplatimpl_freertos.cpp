/*!
 * \file esys/freertos/taskplatimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/assert.h"

#include "esys/freertos/taskplatimpl.h"

#ifdef ESYS_VHW
#include <dbg_log/dbg_class.h>
#endif

namespace esys
{

namespace freertos
{

TaskPlatImpl::TaskPlatImpl()
{
}

TaskPlatImpl::TaskPlatImpl(TaskBase *task_base, const char *name)
#ifndef ESYS_VHW
    : esys::TaskPlatIf(), m_task_base(task_base), m_name(name)
#else
    : m_task_base(task_base), m_name(name)
#endif
{
}

TaskPlatImpl::~TaskPlatImpl()
{
}

int32_t TaskPlatImpl::Create()
{
    return 0;
}

int32_t TaskPlatImpl::Start()
{
    assert(m_task_base != nullptr);
    assert(m_task == nullptr);

    xTaskCreate(TaskPlatImpl::sCallEntry, m_name, m_task_base->GetStackSize(), m_task_base, tskIDLE_PRIORITY + (m_task_base->GetPriority() / 2), &m_task);

    return 0;
}

int32_t TaskPlatImpl::Stop()
{

    return 0;
}

int32_t TaskPlatImpl::Kill()
{
    return -1;
}

int32_t TaskPlatImpl::Destroy()
{
    return 0;
}

millis_t TaskPlatImpl::Millis()
{
    return 0;
}

void TaskPlatImpl::Sleep(millis_t ms)
{
    const TickType_t xDelay = ms / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

void TaskPlatImpl::SleepU(micros_t us)
{
    const TickType_t xDelay = (us / 1000) / portTICK_PERIOD_MS;

    vTaskDelay(xDelay);
}

void TaskPlatImpl::sCallEntry(void *param)
{
    TaskBase *task_base = static_cast<TaskBase *>(param);

    assert(task_base != nullptr);

    task_base->WaitRuntimeInitDone();

    task_base->Started();
    task_base->Entry();
    task_base->Done();

    vTaskDelete(nullptr);
}


void TaskPlatImpl::SetTaskBase(TaskBase *task_base)
{
    m_task_base = task_base;
}

}

}





