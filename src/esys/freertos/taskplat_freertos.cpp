/*!
 * \file esys/freertos/taskplat.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/taskplat.h"
#include "esys/freertos/taskplatimpl.h"
#include "esys/freertos/taskmngr.h"
#include "esys/taskbase.h"

#include "esys/assert.h"

namespace esys
{

namespace freertos
{

/*TaskPlat &TaskPlat::GetCurrent()
{
    TaskHandle_t task_handle = xTaskGetCurrentTaskHandle();

} */

TaskPlat::TaskPlat(TaskBase *task_base)
    : esys::TaskPlatIf(), m_task(nullptr), m_task_base(task_base), m_impl(nullptr), m_mngr_impl(nullptr)
{
    if (task_base!=nullptr)
        m_impl = new TaskPlatImpl(task_base, task_base->GetName());
    else
        m_impl = new TaskPlatImpl(task_base, nullptr);
}

TaskPlat::~TaskPlat()
{
    if (m_impl != nullptr)
        delete m_impl;
}

void TaskPlat::SetTaskMngrBase(TaskMngrBase *taskmngr_base)
{
    m_mngr = dynamic_cast<TaskMngr *>(taskmngr_base);
    m_mngr_impl = m_mngr->GetImpl();
}

int32_t TaskPlat::Create()
{
    assert(m_impl != nullptr);

    return m_impl->Create();
}

int32_t TaskPlat::Start()
{
    assert(m_impl != nullptr);

    return m_impl->Start();
}

int32_t TaskPlat::Stop()
{
    assert(m_impl != nullptr);

    return m_impl->Stop();
}

int32_t TaskPlat::Kill()
{
    assert(m_impl != nullptr);

    return m_impl->Kill();
}

int32_t TaskPlat::Destroy()
{
    assert(m_impl != nullptr);

    return m_impl->Destroy();
}

/*int32_t TaskPlat::Done()
{

} */

millis_t TaskPlat::Millis()
{
    assert(m_impl != nullptr);

    return m_impl->Millis();
}

void TaskPlat::Sleep(millis_t ms)
{
    assert(m_impl != nullptr);

    m_impl->Sleep(ms);
}

void TaskPlat::SleepU(micros_t us)
{
    assert(m_impl != nullptr);

    m_impl->SleepU(us);
}


void TaskPlat::SetTaskBase(TaskBase *task_base)
{
    assert(m_impl != nullptr);

    m_impl->SetTaskBase(task_base);
}

TaskPlatImpl *TaskPlat::GetImpl()
{
    return m_impl;
}

}

}




