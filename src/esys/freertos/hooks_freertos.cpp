/*!
 * \file esys/freertos/hooks_freeros.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"

#include <FreeRTOS.h>
#include <FreeRTOS/task.h>

#include <esys/assert.h>

extern "C"
{


#if (configUSE_IDLE_HOOK == 1)
    void __attribute__ ((weak)) vApplicationIdleHook( void )
    {
        /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
        to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
        task.  It is essential that code added to this hook function never attempts
        to block in any way (for example, call xQueueReceive() with a block time
        specified, or call vTaskDelay()).  If the application makes use of the
        vTaskDelete() API function (as this demo application does) then it is also
        important that vApplicationIdleHook() is permitted to return to its calling
        function, because it is the responsibility of the idle task to clean up
        memory allocated by the kernel to any task that has since been deleted. */
        //__asm volatile( "NOP" );
        __asm volatile ("wfi");
        //for(;;);
    }
#endif

    void __attribute__((weak)) vPreSleepProcessing(unsigned long xExpectedIdleTime)
    {

    }

    void __attribute__((weak)) vPostSleepProcessing(unsigned long xExpectedIdleTime)
    {
        /* FreeRTOS will call this function when it is ready to sleep for a longer
        * time (specified in the argument). SysTick has been already configured
        * in port.c to wake us up if no other interrupt will do it earlier.
        */

        __asm volatile("dsb");
        __asm volatile("wfi");
        __asm volatile("isb");
    }

#if( configUSE_MALLOC_FAILED_HOOK == 1 )
    void __attribute__((weak)) vApplicationMallocFailedHook(void)
    {
        /* vApplicationMallocFailedHook() will only be called if
        configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
        function that will get called if a call to pvPortMalloc() fails.
        pvPortMalloc() is called internally by the kernel whenever a task, queue,
        timer or semaphore is created.  It is also called by various parts of the
        demo application.  If heap_1.c or heap_2.c are used, then the size of the
        heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
        FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
        to query the size of free heap space that remains (although it does not
        provide information on how the remaining heap might be fragmented). */
        taskDISABLE_INTERRUPTS();
        for (;; );
    }
#endif

#if(  configCHECK_FOR_STACK_OVERFLOW > 0 )
    void __attribute__((weak)) vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName)
    {
        (void)pcTaskName;
        (void)pxTask;

        /* Run time stack overflow checking is performed if
        configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
        function is called if a stack overflow is detected. */
        taskDISABLE_INTERRUPTS();
        for (;; );
    }
#endif

#if( configUSE_TICK_HOOK > 0 )
    void __attribute__((weak)) vApplicationTickHook(void)
    {
        /* This function will be called by each tick interrupt if
        configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
        added here, but the tick hook is called from an interrupt context, so
        code must not attempt to block, and only the interrupt safe FreeRTOS API
        functions can be used (those that end in FromISR()). */
    }
#endif

    void __attribute__((weak)) vAssertCalled(void)
    {
        volatile unsigned long ul = 0;

        taskENTER_CRITICAL();
        {
            /* Set ul to a non-zero value using the debugger to step out of this
            function. */
            while (ul == 0)
            {
                __asm volatile("NOP");
            }
        }
        taskEXIT_CRITICAL();
    }

}

