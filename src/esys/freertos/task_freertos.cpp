/*!
 * \file esys/freertos/task_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/task.h"

namespace esys
{

namespace freertos
{

Task::Task(const esys::ObjectName &name, TaskType type)
    : esys::TaskBase(name, type), TaskPlat(this)
{
}

Task::~Task()
{
}

void Task::SetMngrBase(TaskMngrBase *taskmngr_base)
{
#ifdef ESYS_VHW
    TaskPlat::SetTaskMngrBase(taskmngr_base);
#else
#endif
    esys::TaskBase::SetMngrBase(taskmngr_base);
}

}

}





