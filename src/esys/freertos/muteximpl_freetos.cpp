/*!
 * \file esys/freertos/muteximpl.cpp
 * \brief Implementation of the FreeRTOS Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_freertos/esys_freertos_prec.h"
#include "esys/freertos/muteximpl.h"

namespace esys
{

namespace freertos
{

#ifndef ESYS_VHW
MutexImpl::MutexImpl(const esys::ObjectName &name, Type type)
    : esys::MutexBase(name, type), m_mutex(nullptr), m_type(type)
#else
MutexImpl::MutexImpl(MutexBase::Type type)
    : m_mutex(nullptr), m_type(type)
#endif
{
}

MutexImpl::~MutexImpl()
{
}

int32_t MutexImpl::Lock(bool from_isr)
{
    int32_t result = 0;

    assert(m_mutex != nullptr);

    if (from_isr == false)
    {
        if (m_type == MutexBase::DEFAULT)
        {
            if (xSemaphoreTake(m_mutex, portMAX_DELAY) != pdTRUE)
                result = -1;
        }
        else
        {
            if (xSemaphoreTakeRecursive(m_mutex, portMAX_DELAY) != pdTRUE)
                result = -1;
        }
    }
    else
    {
        assert(m_type == MutexBase::DEFAULT);

        BaseType_t xTaskWoken = pdFALSE;
        if (xSemaphoreTakeFromISR(m_mutex, &xTaskWoken) != pdTRUE)
            result=-1;
        else
            portYIELD_FROM_ISR(xTaskWoken);
    }
    return result;
}

int32_t MutexImpl::UnLock(bool from_isr)
{
    int32_t result = 0;

    assert(m_mutex != nullptr);

    if (from_isr == false)
    {
        if (m_type == MutexBase::DEFAULT)
        {
            if (xSemaphoreGive(m_mutex) != pdTRUE)
                result = -1;
        }
        else
        {
            if (xSemaphoreGiveRecursive(m_mutex) != pdTRUE)
                result = -1;
        }
    }
    else
    {
        assert(m_type == MutexBase::DEFAULT);

        BaseType_t xHigherPriorityTaskWoken = pdFALSE;

        if (xSemaphoreGiveFromISR(m_mutex, &xHigherPriorityTaskWoken) != pdTRUE)
            result = -1;
        else
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    return result;
}

int32_t MutexImpl::TryLock(bool from_isr)
{
    assert(m_mutex != nullptr);
    assert(from_isr != true);

    if (m_type == MutexBase::DEFAULT)
    {
        if (xSemaphoreTake(m_mutex, 0) != pdTRUE)
            return -1;
    }
    else
    {
        if (xSemaphoreTakeRecursive(m_mutex, 0) != pdTRUE)
            return -1;
    }
    return 0;
}

int32_t MutexImpl::StaticInit(MutexBase::Order order)
{
    int32_t result = 0;

#ifndef ESYS_VHW
    result = MutexBase::StaticInit(order);
    if (result < 0)
        return result;
#endif

    if (order == MutexBase::AFTER_CHILDREN)
    {
        assert(m_mutex == nullptr);

        if (m_type == MutexBase::DEFAULT)
            m_mutex = xSemaphoreCreateMutex();
        else
            m_mutex = xSemaphoreCreateRecursiveMutex();
        if (m_mutex == nullptr)
            result = -1;
    }
    return result;
}

int32_t MutexImpl::StaticRelease(MutexBase::Order order)
{
    int32_t result = 0;

#ifndef ESYS_VHW
    result = MutexBase::StaticRelease(order);
    if (result < 0)
        return result;
#endif

    if (order == MutexBase::BEFORE_CHILDREN)
    {
        assert(m_mutex != nullptr);

        vSemaphoreDelete(m_mutex);
    }
    return 0;
}

}

}



