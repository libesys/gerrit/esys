/*!
 * \file esys/boost/semaphoreimpl.cpp
 * \brief Declaration of the Boost Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/semaphoreimpl.h"

#include <boost/chrono.hpp>

namespace esys
{

namespace boost
{

SemaphoreImpl::SemaphoreImpl(uint32_t count) : m_count(count), m_mutex(), m_condition()
{
}

SemaphoreImpl::~SemaphoreImpl()
{
}

int32_t SemaphoreImpl::Post()
{
    ::boost::unique_lock< ::boost::mutex> lock(m_mutex);

    ++m_count;

    m_condition.notify_one();
    return 0;
}

int32_t SemaphoreImpl::Wait()
{
    ::boost::unique_lock< ::boost::mutex> lock(m_mutex);

    while (m_count == 0)
    {
        m_condition.wait(lock);
    }
    --m_count;
    return 0;
}

int32_t SemaphoreImpl::WaitFor(uint32_t ms)
{
    ::boost::unique_lock< ::boost::mutex> lock(m_mutex);
    ::boost::chrono::milliseconds period(ms);
    ::boost::cv_status status;

    status = m_condition.wait_for(lock, period);

    if (status == ::boost::cv_status::no_timeout)
    {
        --m_count;
        return 0;
    }
    return -1;
}

int32_t SemaphoreImpl::TryWait()
{
    //int32_t result=0;
    ::boost::unique_lock< ::boost::mutex> lock(m_mutex);

    if (m_count == 0)
        return -1;
    --m_count;
    return 0;
}

uint32_t SemaphoreImpl::Count()
{
    return m_count;
}

}

}

