/*!
 * \file esys/boost/systemplatimplinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"

#include <boost/thread/thread.hpp>

#include <map>

namespace esys
{

namespace boost
{

class ESYS_API SystemPlatImpl;
class ESYS_API SystemPlat;

/*! \class SystemPlatImplInfo esys/boost/systemplatimplinfo.h "esys/boost/systemplatimplinfo.h"
 *  \brief Defines the Boost SystemPlatImplInfo
 */
class ESYS_API SystemPlatImplInfo
{
public:
    friend class ESYS_API SystemPlatImpl;

    SystemPlatImplInfo();
    virtual ~SystemPlatImplInfo();

    void SetSystemPlatImpl(SystemPlatImpl *system_plat_impl);
    SystemPlatImpl *GetSystemPlatImpl();

    void SetSystemPlat(SystemPlat *system_plat);
    SystemPlat *GetSystemPlat();

    static SystemPlatImplInfo *Find(::boost::thread::id thread_id);
protected:
    static void Register(::boost::thread::id thread_id, SystemPlatImplInfo &info);

    typedef std::map< ::boost::thread::id, SystemPlatImplInfo> ThreadIdMap;

    static ThreadIdMap &GetThreadIdMap();

    SystemPlatImpl *m_system_plat_impl = nullptr;
    SystemPlat *m_system_plat = nullptr;
};

}

}




