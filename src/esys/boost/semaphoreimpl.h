/*!
 * \file esys/boost/semaphoreimpl.h
 * \brief Declaration of the Boost Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_SEMAPHOREIMPL_H__
#define __ESYS_BOOST_SEMAPHOREIMPL_H__

#include "esys/esys_defs.h"
#include "esys/semaphorebase.h"

#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/lock_types.hpp>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace esys
{

namespace boost
{

/*! \class Semaphore esys/boost/semaphore.h "esys/boost/semaphore.h"
 *  \brief Boost Semaphore PIMPL class
 */
class ESYS_API SemaphoreImpl
{
public:
    SemaphoreImpl(uint32_t count=0);
    ~SemaphoreImpl();

    int32_t Post();
    int32_t Wait();
    int32_t WaitFor(uint32_t ms);
    int32_t TryWait();
    uint32_t Count();

protected:
    uint32_t m_count;
    ::boost::mutex m_mutex;
    ::boost::condition_variable m_condition;
};

}

}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif


