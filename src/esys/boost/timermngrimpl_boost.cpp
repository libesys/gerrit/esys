/*!
 * \file esys/boost/timermngrimpl.h
 * \brief The Boost PIMPL implementation of the Timer Manager
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/timermngrimpl.h"
#include "esys/boost/time.h"
#include "esys/boost/taskmngr.h"
#include "esys/timerbase.h"
//#include "esys/log.h"
#include "esys/assert.h"

#include <algorithm>

namespace esys
{

namespace boost
{

TimerMngrImpl::TimerMngrImpl(const ObjectName &name, TaskMngr *mngr)
    : Task(name), m_timer_mngr(nullptr), m_mutex("State")
{

//    mngr->Add(this);
}

TimerMngrImpl::~TimerMngrImpl()
{
}

int32_t TimerMngrImpl::Entry()
{
    uint32_t now;

    while (GetState() != State::STOPPING)
    {
        Sleep(1);   // Sleep 1 ms and check if a timer expired or not
        if (m_events.size() == 0)
            // No Timer is running
            continue;

        now = Time::Millis();

        m_mutex.Lock();

        if (m_events[0].m_next_time <= now)
        {
            assert(m_events[0].m_timer != nullptr);

            // A Timer expired
            TimerBase *timer = m_events[0].m_timer;

            if (!m_events[0].m_timer->IsOneShot())
            {
                // Reset the event on the Timer for next timeout
                m_events[0].m_next_time = now + m_events[0].m_timer->GetPeriod();
                // Make sure that all events are sorted by time of arrival
                SortEvents();
            }
            else
            {
                // If the timer expired and is one_short, remove it from future events.
                m_events.pop_front();
            }

            // It is possible to stop a timer in timer callback
            // So let's call notify here
            m_mutex.UnLock();
            timer->Notify();
            m_mutex.Lock();
        }
        m_mutex.UnLock();
    }
    SetState(State::STOPPED);

    return 0;
}

void TimerMngrImpl::Start(TimerBase *timer)
{
    m_mutex.Lock();
    std::deque<Helper>::iterator it;
    Helper helper;

    for (it = m_events.begin(); it != m_events.end(); ++it)
    {
        assert(it->m_timer != timer);
    }

    helper.m_timer = timer;
    helper.m_next_time = Time::Millis() + timer->GetPeriod();

    m_events.push_back(helper);
    SortEvents();
    m_mutex.UnLock();
}

void TimerMngrImpl::Stop(TimerBase *timer)
{
    std::deque<Helper>::iterator it;
    bool found = false;

    m_mutex.UnLock();

    for (it = m_events.begin(); it != m_events.end(); ++it)
    {
        if (it->m_timer == timer)
        {
            found = true;
            m_events.erase(it);
            break;
        }
    }

    assert(found);

    m_mutex.UnLock();
}

bool TimerMngrImpl::IsRunning(TimerBase *timer)
{
    std::deque<Helper>::iterator it;
    bool found = false;

    m_mutex.Lock();

    for (it = m_events.begin(); it != m_events.end(); ++it)
    {
        if (it->m_timer == timer)
        {
            found = true;
            break;
        }
    }
    m_mutex.UnLock();
    return found;
}

int32_t TimerMngrImpl::StopAll()
{
    if (m_events.size() == 0)
        return -1;

    m_mutex.Lock();
    m_events.clear();
    m_mutex.UnLock();

    return 0;
}

void TimerMngrImpl::SetTimerMngr(TimerMngr *timer_mngr)
{
    m_timer_mngr = timer_mngr;
}


void TimerMngrImpl::AddTimer(TimerBase *timer)
{

}

bool s_SortEvents(const TimerMngrImpl::Helper &left, const TimerMngrImpl::Helper &right)
{
    return (left.m_next_time < right.m_next_time);
}

void TimerMngrImpl::SortEvents()
{
    std::sort(m_events.begin(), m_events.end(), &s_SortEvents);
}

}

}





