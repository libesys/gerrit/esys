/*!
 * \file esys/boost/taskmngrimpl.h
 * \brief Declaration of the PIMPL class of the Boost TaskMngr
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/taskmngrbase.h"
#include "esys/semaphore.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>

namespace esys
{

namespace boost
{

class ESYS_API TaskMngr;
class ESYS_API TaskMngrPlat;

/*! \class TaskMngrPlatImpl esys/boost/taskmngrplatimpl.h "esys/boost/taskmngrplatimpl.h"
 *  \brief Boost TaskMngr class
 */
class ESYS_API TaskMngrPlatImpl
{
public:
    TaskMngrPlatImpl(TaskMngrPlat *taskmngr_plat, TaskMngrBase *taskmngr_base);
    virtual ~TaskMngrPlatImpl();

    int32_t StartScheduler();
    void ExitScheduler();

    uint32_t Millis();
    uint64_t Micros();
    uint32_t MicrosLow();

    static TaskMngrPlatImpl &GetCurrent();
    int32_t RegisterPlat();
protected:
    TaskMngrPlat *m_taskmngr_plat = nullptr;
    TaskMngrBase *m_taskmngr_base = nullptr;
    Semaphore m_wait_all_done;
    uint32_t m_task_done_count = 0;
    uint32_t m_app_task_count = 0;
    ::boost::posix_time::ptime m_boost_started_time;
};

}

}







