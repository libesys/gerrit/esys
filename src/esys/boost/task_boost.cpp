/*!
 * \file esys/boost/task_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/task.h"

namespace esys
{

namespace boost
{

Task &Task::GetCurrent()
{
    TaskPlat &cur = TaskPlat::GetCurrent();

    assert(cur.GetTask() != nullptr);

    return *cur.GetTask();
}

Task::Task(const esys::ObjectName &name, TaskType type)
    : esys::TaskBase(name, type), TaskPlat(this)
{
    SetTask(this);
}

Task::~Task()
{
}

void Task::SetMngrBase(TaskMngrBase *taskmngr_base)
{
    TaskPlat::SetTaskMngrBase(taskmngr_base);
}

}

}





