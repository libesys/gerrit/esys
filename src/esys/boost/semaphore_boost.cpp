/*!
 * \file esys/boost/semaphore_boost.cpp
 * \brief Definition of the Boost Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/semaphore.h"
#include "esys/boost/semaphoreimpl.h"
#include "esys/assert.h"

namespace esys
{

namespace boost
{

Semaphore::Semaphore(const ObjectName &name, uint32_t count): SemaphoreBase(name)
{
    m_impl=new SemaphoreImpl(count);
}

Semaphore::~Semaphore()
{
    delete m_impl;
}

int32_t Semaphore::Post(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Post();
}

int32_t Semaphore::Wait(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Wait();
}

int32_t Semaphore::TryWait(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->TryWait();
}

uint32_t Semaphore::Count()
{
    assert(m_impl != nullptr);

    return m_impl->Count();
}

int32_t Semaphore::WaitFor(uint32_t ms, bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->WaitFor(ms);
}

}

}

