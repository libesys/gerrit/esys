/*!
 * \file esys/boost/taskmngrplat.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/taskmngrplat.h"
#include "esys/boost/taskmngrplatimpl.h"

#include "esys/assert.h"

namespace esys
{

namespace boost
{

TaskMngrPlat::TaskMngrPlat(TaskMngrBase *taskmngr)//: TaskMngrPlatIf
{
    m_impl = new TaskMngrPlatImpl(this, taskmngr);
}

TaskMngrPlat::~TaskMngrPlat()
{
    delete m_impl;
}

TaskMngrPlatImpl *TaskMngrPlat::GetImpl()
{
    return m_impl;
}

int32_t TaskMngrPlat::StartScheduler()
{
    assert(m_impl != nullptr);

    return m_impl->StartScheduler();
}

void TaskMngrPlat::Done(TaskBase *task)
{
}

void TaskMngrPlat::ExitScheduler()
{
    assert(m_impl != nullptr);

    m_impl->ExitScheduler();
}

int32_t TaskMngrPlat::RegisterPlat()
{
    assert(m_impl != nullptr);

    return m_impl->RegisterPlat();
}

}

}






