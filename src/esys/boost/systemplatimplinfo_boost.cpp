/*!
 * \file esys/boost/systemplatimplinfo_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/systemplatimplinfo.h"

namespace esys
{

namespace boost
{

SystemPlatImplInfo::ThreadIdMap &SystemPlatImplInfo::GetThreadIdMap()
{
    static ThreadIdMap s_map;

    return s_map;
}

void SystemPlatImplInfo::Register(::boost::thread::id thread_id, SystemPlatImplInfo &info)
{
    GetThreadIdMap()[thread_id] = info;
}

SystemPlatImplInfo *SystemPlatImplInfo::Find(::boost::thread::id thread_id)
{
    ThreadIdMap::iterator it;

    it = GetThreadIdMap().find(thread_id);
    if (it == GetThreadIdMap().end())
        return nullptr;
    return &it->second;
}

SystemPlatImplInfo::SystemPlatImplInfo()
{
}

SystemPlatImplInfo::~SystemPlatImplInfo()
{
}

void SystemPlatImplInfo::SetSystemPlatImpl(SystemPlatImpl *system_plat_impl)
{
    m_system_plat_impl = system_plat_impl;
}

SystemPlatImpl *SystemPlatImplInfo::GetSystemPlatImpl()
{
    return m_system_plat_impl;
}

void SystemPlatImplInfo::SetSystemPlat(SystemPlat *system_plat)
{
    m_system_plat = system_plat;
}

SystemPlat *SystemPlatImplInfo::GetSystemPlat()
{
    return m_system_plat;
}

}

}




