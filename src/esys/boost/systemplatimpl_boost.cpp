/*!
 * \file esys/boost/systemplatimpl_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/systemplatimpl.h"
#include "esys/assert.h"

namespace esys
{

namespace boost
{

void SystemPlatImpl::Register(::boost::thread::id thread_id, SystemPlatImplInfo &info)
{
    SystemPlatImplInfo::Register(thread_id, info);
}

SystemPlatImpl &SystemPlatImpl::GetCurrent()
{
    ::boost::thread::id thread_id = ::boost::this_thread::get_id();

    SystemPlatImplInfo *info = SystemPlatImplInfo::Find(thread_id);

    assert(info != nullptr);
    assert(info->GetSystemPlatImpl());

    return *info->GetSystemPlatImpl();
}

SystemPlatImpl::SystemPlatImpl(SystemPlat *system_plat) : m_system_plat(system_plat)
{
    m_boost_started_time = ::boost::posix_time::microsec_clock::local_time();
}

SystemPlatImpl::~SystemPlatImpl()
{
}

int32_t SystemPlatImpl::PlatInit()
{
    ::boost::thread::id m_thread_id = ::boost::this_thread::get_id();
    SystemPlatImplInfo info;

    info.SetSystemPlatImpl(this);
    info.SetSystemPlat(m_system_plat);

    Register(m_thread_id, info);

    return 0;
}

int32_t SystemPlatImpl::PlatRelease()
{
    return 0;
}

SystemBase *SystemPlatImpl::GetSystemBase()
{
    return m_system_base;
}

void SystemPlatImpl::SetSystemBase(SystemBase *system_base)
{
    m_system_base = system_base;
}

void SystemPlatImpl::SetSystemPlat(SystemPlat *system_plat)
{
    m_system_plat = system_plat;
}

SystemPlat *SystemPlatImpl::GetSystemPlat()
{
    return m_system_plat;
}

void SystemPlatImpl::SetTaskMngrPlatImpl(TaskMngrPlatImpl *taskmngr_plat_impl)
{
    m_taskmngr_plat_impl = taskmngr_plat_impl;
}

TaskMngrPlatImpl *SystemPlatImpl::GetTaskMngrPlatImpl()
{
    return m_taskmngr_plat_impl;
}

void SystemPlatImpl::SetTaskMngrPlat(TaskMngrPlat *taskmngr_plat)
{
    m_taskmngr_plat = taskmngr_plat;
}

TaskMngrPlat *SystemPlatImpl::GetTaskMngrPlat()
{
    return m_taskmngr_plat;
}

void SystemPlatImpl::SetTaskMngr(TaskMngr *taskmngr)
{
    m_taskmngr = taskmngr;
}

TaskMngr *SystemPlatImpl::GetTaskMngr()
{
    return m_taskmngr;
}

void SystemPlatImpl::SetTaskMngrBase(TaskMngrBase *taskmngr_base)
{
    m_taskmngr_base = taskmngr_base;
}

TaskMngrBase *SystemPlatImpl::GetTaskMngrBase()
{
    return m_taskmngr_base;
}

::boost::posix_time::ptime SystemPlatImpl::GetStartedTime()
{
    return m_boost_started_time;
}

::boost::thread::id SystemPlatImpl::GetThreadId()
{
    return m_thread_id;
}

uint32_t SystemPlatImpl::Millis()
{
    ::boost::posix_time::ptime now = ::boost::posix_time::microsec_clock::local_time();

    ::boost::posix_time::time_duration diff = now - m_boost_started_time;

    return (uint32_t)diff.total_milliseconds();
}

uint64_t SystemPlatImpl::Micros()
{
    ::boost::posix_time::ptime now = ::boost::posix_time::microsec_clock::local_time();

    ::boost::posix_time::time_duration diff = now - m_boost_started_time;

    return (uint64_t)diff.total_microseconds();
}

uint32_t SystemPlatImpl::MicrosLow()
{
    uint64_t micros;
    ::boost::posix_time::ptime now = ::boost::posix_time::microsec_clock::local_time();

    ::boost::posix_time::time_duration diff = now - m_boost_started_time;

    micros = (uint64_t)diff.total_microseconds();
    micros = (micros << 32) >> 32;
    return (uint32_t)micros;
}

}

}







