/*!
 * \file esys/boost/dynlibraryimpl_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2019-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/dynlibraryimpl.h"

namespace esys
{

namespace boost
{

DynLibraryImpl::DynLibraryImpl(DynLibrary *self) : m_self(self)
{
}

DynLibraryImpl::~DynLibraryImpl()
{
}

int DynLibraryImpl::Load(const std::string &filename)
{
    try
    {
        m_lib.load(filename);
    }
    catch (::boost::system::system_error &)
    {
        return -1;
    }

    if (m_lib.is_loaded())
        return 0;

    return -1;
}

int DynLibraryImpl::UnLoad()
{
    if (!m_lib.is_loaded())
        return -1;
    m_lib.unload();
    return 0;
}

bool DynLibraryImpl::IsLoaded()
{
    return m_lib.is_loaded();
}

bool DynLibraryImpl::HasSymbol(const std::string& name)
{
    return m_lib.has(name);
}

void *DynLibraryImpl::GetSymbol(const std::string& name)
{

    if (!HasSymbol(name))
        return nullptr;

    return (void *)m_lib.get<void()>(name);
}

}

}

