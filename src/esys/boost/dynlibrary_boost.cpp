/*!
 * \file esys/boost/dynlibrary_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/dynlibrary.h"
#include "esys/boost/dynlibraryimpl.h"
#include "esys/assert.h"

namespace esys
{

namespace boost
{

DynLibrary::DynLibrary() : DynLibraryBase()
{
    m_impl = std::make_unique<DynLibraryImpl>(this);
}

DynLibrary::~DynLibrary()
{
}

int DynLibrary::Load(const std::string &filename)
{
    assert(m_impl != nullptr);

    return m_impl->Load(filename);
}

int DynLibrary::UnLoad()
{
    assert(m_impl != nullptr);

    return m_impl->UnLoad();
}

bool DynLibrary::IsLoaded()
{
    assert(m_impl != nullptr);

    return m_impl->IsLoaded();
}

bool DynLibrary::HasSymbol(const std::string& name)
{
    assert(m_impl != nullptr);

    return m_impl->HasSymbol(name);
}

void *DynLibrary::GetSymbol(const std::string& name)
{
    assert(m_impl != nullptr);

    return m_impl->GetSymbol(name);
}

}

}
