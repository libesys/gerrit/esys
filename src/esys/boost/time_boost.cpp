/*!
 * \file esys/boost/time_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/time.h"
#include "esys/boost/taskmngrplatimpl.h"
#include "esys/boost/systemplatimpl.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/chrono/chrono.hpp>
#include <boost/thread.hpp>

namespace esys
{

namespace boost
{

Time::Time()
{
}

uint32_t Time::Millis(Source source)
{
    return SystemPlatImpl::GetCurrent().Millis();
}

uint64_t Time::Micros(Source source)
{
    return SystemPlatImpl::GetCurrent().Micros();
}

uint32_t Time::MicrosLow()
{
    return SystemPlatImpl::GetCurrent().MicrosLow();
}

void Time::Sleep(uint32_t ms)
{
    ::boost::this_thread::sleep_for(::boost::chrono::milliseconds(ms));
}

void Time::USleep(uint32_t us)
{
    ::boost::this_thread::sleep_for(::boost::chrono::microseconds(us));
}

static ::boost::posix_time::ptime g_ptime_when_set_done;
static ::boost::posix_time::ptime g_ptime_set;
static bool g_date_or_time_set = false;

int32_t Time::SetTime(TimeStruct &time)
{
    g_ptime_when_set_done = ::boost::posix_time::second_clock::universal_time();
    g_ptime_set = ::boost::posix_time::ptime(g_ptime_when_set_done.date(), ::boost::posix_time::time_duration(time.m_hour, time.m_minutes, time.m_seconds));
    g_date_or_time_set = true;

    return 0;
}

int32_t Time::GetTime(TimeStruct &time)
{
    if (g_date_or_time_set == false)
    {
        ::boost::posix_time::ptime date_time = ::boost::posix_time::second_clock::universal_time();
        ::boost::posix_time::time_duration duration;

        duration = date_time.time_of_day();
        time.m_hour = duration.hours();
        time.m_minutes = duration.minutes();
        time.m_seconds = duration.seconds();
        return 0;
    }

    ::boost::posix_time::ptime date_time_now = ::boost::posix_time::second_clock::universal_time();
    ::boost::posix_time::ptime simulated_time = g_ptime_set + (date_time_now - g_ptime_when_set_done);
    ::boost::posix_time::time_duration duration;

    duration = simulated_time.time_of_day();
    time.m_hour = duration.hours();
    time.m_minutes = duration.minutes();
    time.m_seconds = duration.seconds();
    return 0;
}

int32_t Time::SetDate(DateStruct &date)
{
    g_ptime_when_set_done = ::boost::posix_time::second_clock::universal_time();
    g_ptime_set = ::boost::posix_time::ptime(::boost::gregorian::date(date.m_year, date.m_month, date.m_day), g_ptime_when_set_done.time_of_day());
    g_date_or_time_set = true;

    return 0;
}

int32_t Time::GetDate(DateStruct &date)
{
    if (g_date_or_time_set == false)
    {
        ::boost::posix_time::ptime date_time = ::boost::posix_time::second_clock::universal_time();

        date.m_year = date_time.date().year();
        date.m_month = (esys::uint8_t)date_time.date().month();
        date.m_day = (esys::uint8_t)date_time.date().day();
        return 0;
    }

    ::boost::posix_time::ptime date_time_now = ::boost::posix_time::second_clock::universal_time();
    ::boost::posix_time::ptime simulated_time = g_ptime_set + (date_time_now - g_ptime_when_set_done);
    ::boost::posix_time::time_duration duration;

    date.m_year = simulated_time.date().year();
    date.m_month = (esys::uint8_t)simulated_time.date().month();
    date.m_day = (esys::uint8_t)simulated_time.date().day();
    return 0;
}

int32_t Time::SetDateTime(DateTimeStruct &date_time)
{
    g_ptime_when_set_done = ::boost::posix_time::second_clock::universal_time();
    g_ptime_set = ::boost::posix_time::ptime(
                      ::boost::gregorian::date(date_time.m_date.m_year, date_time.m_date.m_month, date_time.m_date.m_day),
                      ::boost::posix_time::time_duration(date_time.m_time.m_hour, date_time.m_time.m_minutes, date_time.m_time.m_seconds));
    g_date_or_time_set = true;

    return 0;
}

int32_t Time::GetDateTime(DateTimeStruct &date_time)
{
    int32_t result = GetDate(date_time.m_date);
    if (result < 0)
        return result;

    return GetTime(date_time.m_time);
}

int32_t Time::StartTimestamp()
{
    return 0;   // Since always enabled
}

int32_t Time::StopTimestamp()
{
    return 0;   // Since always enabled
}

}

}

