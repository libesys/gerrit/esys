/*!
 * \file esys/boost/taskplatimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/assert.h"

//#include "esys/boost/scheduler/taskplatimpl.h"
#include "esys/boost/taskplatimpl.h"
#include "esys/assert.h"

#include <dbg_log/dbg_class.h>

#include <boost/thread/thread.hpp>

namespace esys
{

namespace boost
{

TaskPlatImpl::ThreadIdMap TaskPlatImpl::s_thread_id_map;

void TaskPlatImpl::Register(::boost::thread::id thread_id, TaskPlatImplInfo &helper)
{
    s_thread_id_map[thread_id] = helper;
}

TaskPlatImplInfo *TaskPlatImpl::TryGetCurImplInfo()
{
    ::boost::thread::id thread_id = ::boost::this_thread::get_id();
    ThreadIdMap::iterator it;

    it = s_thread_id_map.find(thread_id);

    if (it == s_thread_id_map.end())
        return nullptr;

    return &it->second;
}

TaskPlatImplInfo &TaskPlatImpl::GetCurImplInfo()
{
    ::boost::thread::id thread_id = ::boost::this_thread::get_id();
    ThreadIdMap::iterator it;

    it = s_thread_id_map.find(thread_id);

    assert(it != s_thread_id_map.end());

    return it->second;
}

TaskPlatImpl::TaskPlatImpl(TaskPlat *task_plat, TaskBase *task_base)
    : m_task(nullptr), m_task_plat(task_plat), m_task_base(task_base), m_sem_registered("SemRegistered", 0)
{
}

TaskPlatImpl::~TaskPlatImpl()
{
}

int32_t TaskPlatImpl::Create()
{
    return 0;
}

int32_t TaskPlatImpl::Start()
{
    TaskPlatImplInfo helper;

    assert(m_task_base != nullptr);

    helper.SetTask(m_task);    //If created by mp::Taksk, it would be always nullptr
    helper.SetTaskBase(m_task_base);
    helper.SetTaskPlat(m_task_plat);
    if (m_task != nullptr)
        helper.SetTaskPlatImpl(m_task->GetImpl());
    helper.SetTaskMngr(nullptr);
    helper.SetTaskMngrPlatImpl(nullptr);

    m_thread = ::boost::thread(::boost::bind(&TaskPlatImpl::CallEntry, this));

    Register(m_thread.get_id(), helper);

    m_sem_registered.Post();

    return 0;
}

int32_t TaskPlatImpl::Stop()
{
    if (m_thread.joinable())
        m_thread.join();
    return 0;
}

int32_t TaskPlatImpl::Kill()
{
    return -1;
}

int32_t TaskPlatImpl::Destroy()
{
    return 0;
}

millis_t TaskPlatImpl::Millis()
{
    return 0;
}

void TaskPlatImpl::Sleep(millis_t ms)
{
    ::boost::this_thread::sleep_for(::boost::chrono::milliseconds(ms));
}

void TaskPlatImpl::SleepU(micros_t us)
{
    ::boost::this_thread::sleep_for(::boost::chrono::microseconds(us));
}

void TaskPlatImpl::CallEntry()
{
    assert(m_task_base != nullptr);

    SetThreadName(m_thread.get_id(), m_task_base->GetName());

    //m_task->started();

    m_sem_registered.Wait();

#ifdef ESYS_OBJECT_META
    DBG_SET_THREAD_NAME(m_task_base->GetName());
#endif
    DBG_CALLMEMBER("boost::TaskPlatImpl::call_entry", false);
    DBG_CALLMEMBER_END;

    m_task_base->WaitRuntimeInitDone();

    m_task_base->Started();
    m_task_base->Entry();
    m_task_base->Done();
}

void TaskPlatImpl::SetTaskBase(TaskBase *task_base)
{
    m_task_base = task_base;

    TaskPlatImplInfo *info = TryGetCurImplInfo();
    if (info != nullptr)
        info->SetTaskBase(task_base);
}

void TaskPlatImpl::SetTask(Task *task)
{
    m_task = task;

    TaskPlatImplInfo *info = TryGetCurImplInfo();
    if (info != nullptr)
        info->SetTask(task);
}

Task *TaskPlatImpl::GetTask()
{
    return m_task;
}

#ifdef WIN32

const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push, 8)
typedef struct THREADNAME_INFO
{
    DWORD dwType; // Must be 0x1000.
    LPCSTR szName; // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)

void _SetThreadName(DWORD threadId, const char* threadName)
{
    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = threadName;
    info.dwThreadID = threadId;
    info.dwFlags = 0;
    __try
    {
        RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
    }
}
#else
#endif

void TaskPlatImpl::SetThreadName(::boost::thread::id threadId, const char* thread_name)
{
#ifdef WIN32
    const char* cchar = thread_name;//  .c_str();
    // convert HEX string to DWORD
    unsigned int dwThreadId;
    std::stringstream ss;
    ss << std::hex << threadId;
    ss >> dwThreadId;
    // set thread name
    _SetThreadName((DWORD)dwThreadId, cchar);
#else
#endif
}


}

}





