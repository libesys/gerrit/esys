/*!
 * \file esys/boost/muteximpl.cpp
 * \brief Implementation of the Boost Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/muteximpl.h"

namespace esys
{

namespace boost
{

MutexImpl::MutexImpl(): m_mutex()
{
}

MutexImpl::~MutexImpl()
{
}

int32_t MutexImpl::Lock(bool from_isr)
{
    m_mutex.lock();
    return 0;
}

int32_t MutexImpl::UnLock(bool from_isr)
{
    m_mutex.unlock();
    return 0;
}

int32_t MutexImpl::TryLock(bool from_isr)
{
    bool result;

    result=m_mutex.try_lock();

    if (result)
        return 0;
    return -1;
}

}

}



