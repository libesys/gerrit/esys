/*!
 * \file esys/boost/timer_boost.cpp
 * \brief The Boost declaration of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/timer.h"
#include "esys/boost/timerimpl.h"
#include "esys/timermngrbase.h"
#include "esys/assert.h"

namespace esys
{

namespace boost
{

Timer::Timer(const ObjectName &name) : TimerBase(name)
{
    m_impl = new TimerImpl(name);
}

Timer::~Timer()
{
    delete m_impl;
}

int32_t Timer::Start(uint32_t ms, bool one_shot, bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Start(ms, one_shot, from_isr);
}

int32_t Timer::Stop(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Stop(from_isr);
}

int32_t Timer::GetPeriod()
{
    assert(m_impl != nullptr);

    return m_impl->GetPeriod();
}

bool Timer::IsRunning()
{
    assert(m_impl != nullptr);

    return m_impl->IsRunning();
}

bool Timer::IsOneShot()
{
    assert(m_impl != nullptr);

    return m_impl->IsOneShot();
}

void Timer::SetCallback(TimerCallbackBase *callback)
{
    assert(m_impl != nullptr);

    m_impl->SetCallback(callback);
}

TimerCallbackBase *Timer::GetCallback()
{
    assert(m_impl != nullptr);

    return m_impl->GetCallback();
}

}

}




