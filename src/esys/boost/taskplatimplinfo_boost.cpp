/*!
 * \file esys/boost/taskplatimplinfo_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/taskplatimplinfo.h"

namespace esys
{

namespace boost
{

TaskPlatImplInfo::TaskPlatImplInfo()
{
}

void TaskPlatImplInfo::SetTaskMngrPlatImpl(TaskMngrPlatImpl *task_mngr_plat_impl)
{
    m_task_mngr_plat_impl = task_mngr_plat_impl;
}

TaskMngrPlatImpl *TaskPlatImplInfo::GetTaskMngrPlatImpl()
{
    return m_task_mngr_plat_impl;
}

void TaskPlatImplInfo::SetTaskPlatImpl(TaskPlatImpl *task_plat_impl)
{
    m_task_plat_impl = task_plat_impl;
}

TaskPlatImpl *TaskPlatImplInfo::GetTaskPlatImpl()
{
    return m_task_plat_impl;
}

void TaskPlatImplInfo::SetTaskMngr(TaskMngr *task_mngr)
{
    m_task_mngr = task_mngr;
}

TaskMngr *TaskPlatImplInfo::GetTaskMngr()
{
    return m_task_mngr;
}

void TaskPlatImplInfo::SetTaskPlat(TaskPlat *task_plat)
{
    m_task_plat = task_plat;
}

TaskPlat *TaskPlatImplInfo::GetTaskPlat()
{
    return m_task_plat;
}

void TaskPlatImplInfo::SetTask(Task *task)
{
    m_task = task;
}

Task *TaskPlatImplInfo::GetTask()
{
    return m_task;
}

void TaskPlatImplInfo::SetTaskBase(TaskBase *task_base)
{
    m_task_base = task_base;
}

TaskBase *TaskPlatImplInfo::GetTaskBase()
{
    return m_task_base;
}

void TaskPlatImplInfo::SetSystemPlat(SystemPlat *system_plat)
{
    m_system_plat = system_plat;
}

SystemPlat *TaskPlatImplInfo::GetSystemPlat()
{
    return m_system_plat;
}

}

}





