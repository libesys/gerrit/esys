/*!
 * \file esys/boost/serialportimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

#include <esys/ringbuffer.h>
#include <esys/serialportcallbackbase.h>

#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/utility.hpp>
#include <boost/function.hpp>
#include <boost/shared_array.hpp>
#include <boost/locale.hpp>

#include <condition_variable>

namespace esys
{

namespace boost
{

class ESYS_API SerialPort;

class ESYS_API SerialPortImpl
{
public:
    SerialPortImpl(SerialPort *self);   //!< Default constructor
    virtual ~SerialPortImpl();

    int Open(const std::wstring& devname);
    bool IsOpen() const;
    bool ErrorStatus() const;
    int Close();

    void SetErrorStatus(bool e);
    void DoRead();
    void ReadEnd(const ::boost::system::error_code& error, size_t bytes_transferred);
    void DoClose();

    void Write(uint8_t *data, size_t size);
    void Write(const std::string &data);
    int Read(std::string &data, int timeout_ms = -1);

    void DoWrite();
    void WriteEnd(const ::boost::system::error_code& error);

    void SetReadCallback(const ::boost::function<void (const uint8_t*, size_t)>& callback);
    void ClearReadCallback();

    void UseCTSRTS(bool fc);
    void SetBaudrate(uint32_t baud);

    void SetCallback(SerialPortCallbackBase *call_back);
    SerialPortCallbackBase *GetCallback();

    esys::RingBuffer<uint8_t> *GetRXBuf();
    void SetRXBuf(esys::RingBuffer<uint8_t> *rx_buf);
    static const int m_read_buffer_size=512;

    SerialPort *m_self = nullptr;

    ::boost::asio::io_service m_io;         //!< Io service object
    ::boost::asio::serial_port m_port;      //!< Serial port object
    ::boost::thread m_background_thread;    //!< Thread that runs read/write operations
    bool m_open;                            //!< True if port open
    bool m_error;                           //!< Error flag
    mutable ::boost::mutex m_error_mutex;   //!< Mutex for access to error

    /// Data are queued here before they go in writeBuffer
    std::vector<uint8_t> m_write_queue;
    ::boost::shared_array<uint8_t> m_write_buffer;      //!< Data being written
    size_t m_write_buffer_size;                         //!< Size of writeBuffer
    ::boost::mutex m_write_queue_mutex;                 //!< Mutex for access to writeQueue
    uint8_t m_read_buffer[m_read_buffer_size];          //!< data being read

    /// Read complete callback
    ::boost::function<void (const uint8_t *, size_t)> m_callback;

    unsigned int m_baud_rate = 115200;
    ::boost::asio::serial_port_base::parity m_opt_parity;
    ::boost::asio::serial_port_base::character_size m_opt_csize;
    ::boost::asio::serial_port_base::flow_control m_opt_flow;
    ::boost::asio::serial_port_base::stop_bits m_opt_stop;
    size_t m_read_size;
    std::condition_variable m_cond_var;
    std::mutex m_cond_var_mutex;
    esys::RingBuffer<uint8_t> *m_rx_buf = nullptr;
    SerialPortCallbackBase *m_call_back = nullptr;
};

}

}



