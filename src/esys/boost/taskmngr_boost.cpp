/*!
 * \file esys/boost/taskmngr.cpp
 * \brief Declaration of the Boost TaskMngr class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/taskmngr.h"

namespace esys
{

namespace boost
{

TaskMngr::TaskMngr(const ObjectName &name)
    : TaskMngrBase(name), TaskMngrPlat(this), m_timer_mngr("TimerMngr", this)
{
}

TaskMngr::~TaskMngr()
{
}

int32_t TaskMngr::StaticInitBeforeChildren()
{
    return RegisterPlat();
}

void TaskMngr::AllAppTasksDone()
{
}

TimerMngr &TaskMngr::GetTimerMngr()
{
    return m_timer_mngr;
}


}

}


