/*!
 * \file esys/boost/dynlibraryimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/boost/dynlibrary.h"

#include <boost/dll.hpp>

namespace esys
{

namespace boost
{

class ESYS_API DynLibraryImpl
{
public:
    DynLibraryImpl(DynLibrary *self);
    virtual ~DynLibraryImpl();

    int Load(const std::string &filename);
    int UnLoad();
    bool IsLoaded();
    bool HasSymbol(const std::string& name);
    void *GetSymbol(const std::string& name);
protected:
    DynLibrary *m_self = nullptr;
    ::boost::dll::shared_library m_lib;
};

}

}

