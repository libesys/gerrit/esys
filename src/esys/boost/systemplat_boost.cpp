/*!
 * \file esys/boost/systemplat.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/systemplat.h"
#include "esys/boost/systemplatimpl.h"
#include "esys/assert.h"

namespace esys
{

namespace boost
{

SystemPlat::SystemPlat(SystemBase *system_base) : m_system_base(system_base)
{
    m_impl = new SystemPlatImpl(this);
}

SystemPlat::~SystemPlat()
{
    if (m_impl != nullptr)
        delete m_impl;
}

int32_t SystemPlat::PlatInit()
{
    assert(m_impl != nullptr);

    return m_impl->PlatInit();
}

int32_t SystemPlat::PlatRelease()
{
    assert(m_impl != nullptr);

    return m_impl->PlatRelease();
}

SystemBase *SystemPlat::GetSystemBase()
{
    return m_system_base;
}

void SystemPlat::SetSystemBase(SystemBase *system_base)
{
    assert(m_impl != nullptr);

    m_impl->SetSystemBase(system_base);

    m_system_base = system_base;
}

SystemPlatImpl *SystemPlat::GetImpl()
{
    return m_impl;
}

}

}







