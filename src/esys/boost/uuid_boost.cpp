/*!
 * \file esys/uuid_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/boost/uuid.h"

#include <memory.h>

#include <sstream>
#include <iomanip>

#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>
#include <boost/locale.hpp>

namespace esys
{

namespace boost
{

Uuid::Uuid()
{
    Null();
}


Uuid::~Uuid()
{
}

std::string &Uuid::GetString(Kind kind, bool new_if_null)
{
    std::ostringstream ss(m_str);
    ::boost::uuids::uuid u;
    uint32_t idx;

    m_str.clear();

    if (IsNull())
    {
        if (new_if_null == false)
            return m_str;
        New();
    }

    switch (kind)
    {
        case HEX:
            for (idx=0; idx<16; idx++)
                ss << std::setw(2) << std::setfill('0') << std::hex << (int)m_uuid[idx];
            break;
        case STRING:
            memcpy(&u, m_uuid, 16);
            ss << u;
            break;
    }
    m_str=ss.str();
    return m_str;
}

bool Uuid::IsNull()
{
    uint32_t i;

    for (i=0; i<16; i++)
        if (m_uuid[i]!=0x00)
            return false;
    return true;
}

void Uuid::Null()
{
    uint32_t i;

    for (i=0; i<16; i++)
        m_uuid[i]=0x00;
}

void Uuid::New()
{
    uint32_t idx;
    ::boost::uuids::uuid u; //( boost::uuids::random_generator()() );

    u=::boost::uuids::random_generator()();

    for (idx=0; idx<16; idx++)
        m_uuid[idx]=u.begin()[idx];
}

uint8_t *Uuid::Get()
{
    return m_uuid;
}

uint8_t Uuid::operator[](int idx)
{
    if ((idx<0)||(idx>=16))
        return 0;
    return m_uuid[idx];
}

void Uuid::Set(uint8_t *data)
{
    memcpy(m_uuid,data,16);
}

void Uuid::Set(std::string val)
{
    std::string r;
    std::string val8s;
    std::istringstream iss;
    std::string::iterator it;
    uint32_t idx;
    int vali;

    int state=0;
    idx=0;

    for (it=val.begin(); (it<val.end())&&(idx<16); ++it)
    {
        if (isalnum(*it))
        {
            r.push_back(*it);
            if (state==0)
            {
                val8s.push_back(*it);
                state=1;
            }
            else
            {
                val8s.push_back(*it);
                iss.str(val8s);
                iss.seekg(0);
                iss >> std::hex >> vali;
                m_uuid[idx++]=vali;
                state=0;
                val8s.clear();
            }
        }
    }
}

}

}
