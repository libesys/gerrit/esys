/*!
 * \file esys/boost/timerimpl.h
 * \brief The Boost PIMPL implementation of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/timerimpl.h"
#include "esys/boost/timermngr.h"

#include "esys/assert.h"

namespace esys
{

namespace boost
{

TimerImpl::TimerImpl(const char *name)
    : TimerBase(name), m_name(name), m_period_ms(0), m_one_shot(false)
{
}

TimerImpl::~TimerImpl()
{
}

int32_t TimerImpl::Start(uint32_t period_ms, bool one_shot, bool from_isr)
{
    m_period_ms = period_ms;
    m_one_shot = one_shot;

    if (m_mngr == nullptr)
    {
        m_mngr = &TimerMngr::GetCurrent();
    }

    assert(m_mngr != nullptr);

    m_mngr->Start(this);
    return 0;
}

int32_t TimerImpl::Stop(bool from_isr)
{
    assert(m_mngr != nullptr);

    m_mngr->Stop(this);
    return 0;
}

int32_t TimerImpl::GetPeriod()
{
    return m_period_ms;
}

bool TimerImpl::IsRunning()
{
    if (m_mngr == nullptr)
        return false;

    return m_mngr->IsRunning(this);
}

bool TimerImpl::IsOneShot()
{
    return m_one_shot;
}

}

}






