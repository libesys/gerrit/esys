/*!
 * \file esys/boost/pluginmngrcore_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/pluginbase.h"
#include "esys/boost/pluginmngrcore.h"

#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys
{

namespace boost
{

PluginMngrImplHelper::PluginMngrImplHelper()
{
}

PluginMngrImplHelper::~PluginMngrImplHelper()
{
}

void PluginMngrImplHelper::SetDynLib(std::shared_ptr<DynLibrary> &dyn_lib)
{
    m_dyn_lib = dyn_lib;
}

DynLibrary *PluginMngrImplHelper::GetDynLib()
{
    return m_dyn_lib.get();
}

void PluginMngrImplHelper::SetEntryPoint(void *entry)
{
    m_entry_fct = entry;
}

void *PluginMngrImplHelper::GetEntryPoint()
{
    return m_entry_fct;
}

void PluginMngrImplHelper::SetPlugin(PluginBase *plugin)
{
    m_plugin = plugin;
}

PluginBase *PluginMngrImplHelper::GetPlugin()
{
    return m_plugin;
}

PluginMngrCore::PluginMngrCore(): PluginMngrBase()
{
}

PluginMngrCore::~PluginMngrCore()
{
}

int PluginMngrCore::Load(const std::string &dir)
{
    std::shared_ptr<PluginMngrImplHelper> helper;
    PluginBase *plugin;
    std::shared_ptr<DynLibrary> plugin_lib = std::make_shared<DynLibrary>();

    if (plugin_lib->Load(dir) != 0)
        return -1;
    if (plugin_lib->HasSymbol(GetEntryFctName()) == false)
        return -2;

    void *plugin_entry_function = plugin_lib->GetSymbol(GetEntryFctName());
    if (plugin_entry_function == nullptr)
        return -3;

    plugin = GetPluginFromEntryFct(plugin_entry_function);

#ifdef _MSC_VER
#ifdef _DEBUG
    if (plugin->IsDebug() == false)
        return -4;
#elif NDEBUG
    if (plugin->IsDebug() == true)
        return -5;
#else
#error _DEBUG or NDEBUG must be defined
#endif
#endif
    helper = std::make_shared<PluginMngrImplHelper>();

    helper->SetDynLib(plugin_lib);
    helper->SetEntryPoint(plugin_entry_function);
    helper->SetPlugin(plugin);
    SetPluginFileName(plugin, dir);

    m_plugins.push_back(helper);
    return 0;
}

int PluginMngrCore::Load()
{
    DBG_CALLMEMBER_RET("esys::wx::PluginMngrCore::Load", false, int, result);
    DBG_CALLMEMBER_END;

    ::boost::filesystem::path abs_plugin_dir;
    ::boost::filesystem::path plugin_dir;
    ::boost::filesystem::path filename;
    ::boost::filesystem::directory_iterator file_end_it;
    DynLibrary *plugin_lib = nullptr;

    if (!GetBaseFolder().empty())
    {
        filename = GetBaseFolder();
        filename /= GetDir();
    }
    else
        filename = GetDir();
    filename = ::boost::filesystem::absolute(filename);

    abs_plugin_dir = filename;

    if (GetVerboseLevel() > 0)
    {
        std::cout << "Plugin folder = " << abs_plugin_dir.c_str() << std::endl;
    }
#ifdef WIN32
    SetDllDirectory(abs_plugin_dir.string().c_str());
#endif

#ifdef WIN32
    std::string mask = "*.dll";
#else
    std::string mask = "*.so";
#endif
    ::boost::filesystem::path search_path = abs_plugin_dir / mask;

    for (::boost::filesystem::directory_iterator it(abs_plugin_dir); it != file_end_it; ++it)
    {
        // If it's not a directory, list it. If you want to list directories too, just remove this check.
        if (!::boost::filesystem::is_regular_file(it->path()))
            continue;

        // assign current file name to current_file and echo it out to the console.
        std::string current_file = it->path().string();
        result = Load(current_file);
    }

    return result;
}

int PluginMngrCore::Release()
{
    uint16_t idx;

    for (idx = 0; idx < m_plugins.size(); ++idx)
    {
        assert(m_plugins[idx]->GetPlugin() != nullptr);

        m_plugins[idx]->GetPlugin()->Release();
        m_plugins[idx].reset();
    }
    m_plugins.clear();

    return 0;
}

std::size_t PluginMngrCore::GetNbr()
{
    return m_plugins.size();
}

PluginBase *PluginMngrCore::GetBase(std::size_t index)
{
    if (index >= m_plugins.size())
        return nullptr;
    return m_plugins[index]->GetPlugin();
}

}

}
