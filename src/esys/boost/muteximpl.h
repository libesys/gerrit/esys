/*!
 * \file esys/boost/muteximpl.h
 * \brief Declaration of the Boost Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_BOOST_MUTEXIMPL_H__
#define __ESYS_BOOST_MUTEXIMPL_H__

#include "esys/esys_defs.h"
#include "esys/semaphorebase.h"

#include <boost/thread.hpp>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

namespace esys
{

namespace boost
{

/*! \class Mutex esys/boost/muteximpl.h "esys/boost/muteximpl.h"
 *  \brief Boost Mutex class
 */
class ESYS_API MutexImpl
{
public:
    MutexImpl();
    ~MutexImpl();

    int32_t Lock(bool from_isr = false);
    int32_t UnLock(bool from_isr = false);
    int32_t TryLock(bool from_isr = false);

protected:
    ::boost::mutex m_mutex;
};

}

}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif


