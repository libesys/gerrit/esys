/*!
 * \file esys/boost/serialportimpl_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

//#include "esys/esys_prec.h"
#define _WIN32_WINNT 0x0601

#include "esys/boost/serialportimpl.h"
#include "esys/boost/serialport.h"

#include <chrono>

using namespace std::chrono_literals;

namespace esys
{

namespace boost
{

SerialPortImpl::SerialPortImpl(SerialPort *self)
    : m_self(self), m_io(), m_port(m_io), m_background_thread(), m_open(false)
    , m_error(false), m_opt_parity(), m_opt_csize(), m_opt_flow(), m_opt_stop()
{
    /*SetReadCallback(boost::bind(&BinChannelAsio::read_callback
        , self
        , m_read_buffer
        ); */
}

SerialPortImpl::~SerialPortImpl()
{
    if(IsOpen())
    {
        try
        {
            Close();
        }
        catch(...)
        {
            //Don't throw from a destructor
        }
    }
}

esys::RingBuffer<uint8_t> *SerialPortImpl::GetRXBuf()
{
    return m_rx_buf;
}

void SerialPortImpl::SetRXBuf(esys::RingBuffer<uint8_t> *rx_buf)
{
    m_rx_buf = rx_buf;
}

void SerialPortImpl::UseCTSRTS(bool fc)
{
    if (fc)
        m_opt_flow=::boost::asio::serial_port_base::flow_control(::boost::asio::serial_port_base::flow_control::hardware);
    else
        m_opt_flow=::boost::asio::serial_port_base::flow_control(::boost::asio::serial_port_base::flow_control::none);
}

void SerialPortImpl::SetBaudrate(uint32_t baud)
{
    m_baud_rate=baud;
}

int SerialPortImpl::Open(const std::wstring& devname)
{
    int result;
    ::boost::system::error_code ec;

    if (IsOpen())
    {
        result=Close();
    }

    SetErrorStatus(true);
    m_port.open(::boost::locale::conv::utf_to_utf<char>(devname), ec);
    if (ec)
    {
        return -1;
    }
    m_port.set_option(::boost::asio::serial_port_base::baud_rate(m_baud_rate));
    m_port.set_option(m_opt_parity);
    m_port.set_option(m_opt_csize);
    m_port.set_option(m_opt_flow);
    m_port.set_option(m_opt_stop);

    //This gives some work to the io_service before it is started
    m_io.post(::boost::bind(&SerialPortImpl::DoRead, this));

    ::boost::thread t(::boost::bind(&::boost::asio::io_service::run, &m_io));
    m_background_thread.swap(t);
    SetErrorStatus(false);      //If we get here, no error
    m_open=true;                //Port is now open

    return 0;
}

bool SerialPortImpl::IsOpen() const
{
    return m_open;
}

bool SerialPortImpl::ErrorStatus() const
{
    ::boost::lock_guard<::boost::mutex> l(m_error_mutex);
    return m_error;
}

int SerialPortImpl::Close()
{
    if(!IsOpen())
        return -1;

    m_open=false;
    m_io.post(::boost::bind(&SerialPortImpl::DoClose, this));
    m_background_thread.join();
    m_io.reset();

    if(ErrorStatus())
    {
        throw(::boost::system::system_error(::boost::system::error_code(),
                                            "Error while closing the device"));
    }
    return 0;
}

void SerialPortImpl::SetErrorStatus(bool e)
{
    ::boost::lock_guard<::boost::mutex> l(m_error_mutex);
    m_error=e;
}

void SerialPortImpl::DoRead()
{
    m_port.async_read_some(::boost::asio::buffer(m_read_buffer,m_read_buffer_size),
                           ::boost::bind(&SerialPortImpl::ReadEnd,
                                         this,
                                         ::boost::asio::placeholders::error,
                                         ::boost::asio::placeholders::bytes_transferred));
}

void SerialPortImpl::ReadEnd(const ::boost::system::error_code& error,
                             size_t bytes_transferred)
{
    m_read_size = bytes_transferred;

    if(error)
    {
#ifdef __APPLE__
        if(error.value()==45)
        {
            //Bug on OS X, it might be necessary to repeat the setup
            //http://osdir.com/ml/lib.boost.asio.user/2008-08/msg00004.html
            doRead();
            return;
        }
#endif //__APPLE__
        //error can be true even because the serial port was closed.
        //In this case it is not a real error, so ignore
        if(IsOpen())
        {
            DoClose();
            SetErrorStatus(true);
        }
    }
    else
    {
        if(m_callback)
            m_callback(m_read_buffer, bytes_transferred);
        //m_self->read_callback(m_read_buffer, m_read_size);

        if (m_read_size != 0)
        {
            if (GetRXBuf() != nullptr)
            {
                std::unique_lock< std::mutex > lock(m_cond_var_mutex);

                GetRXBuf()->Push(m_read_buffer, m_read_size);
            }
            m_cond_var.notify_one();
            if ((GetCallback() != nullptr) && (GetRXBuf() != nullptr))
                GetCallback()->Reveived(*GetRXBuf());
        }

        DoRead();
    }
}

void SerialPortImpl::DoClose()
{
    ::boost::system::error_code ec;

    m_port.cancel(ec);
    if (ec)
        SetErrorStatus(true);

    m_port.close(ec);
    if(ec)
        SetErrorStatus(true);
}

void SerialPortImpl::Write(uint8_t *data, size_t size)
{
    {
        ::boost::lock_guard<::boost::mutex> l(m_write_queue_mutex);
        m_write_queue.insert(m_write_queue.end(), data, data+size);
    }
    m_io.post(::boost::bind(&SerialPortImpl::DoWrite, this));
}

void SerialPortImpl::Write(const std::string &data)
{
    {
        ::boost::lock_guard<::boost::mutex> l(m_write_queue_mutex);
        m_write_queue.insert(m_write_queue.end(), data.begin(), data.end());
    }
    m_io.post(::boost::bind(&SerialPortImpl::DoWrite, this));
}

int SerialPortImpl::Read(std::string &data, int timeout_ms)
{
    std::unique_lock<std::mutex> lock(m_cond_var_mutex);

    assert(GetRXBuf() != nullptr);

    // *INDENT-OFF*
    if (timeout_ms == -1)
    {
        m_cond_var.wait(lock, [this] { return (this->GetRXBuf()->GetSize() != 0); });
        data.resize(GetRXBuf()->GetSize());
        esys::memcpy((uint8_t *)data.data(), *GetRXBuf(), GetRXBuf()->GetSize());

        GetRXBuf()->Pop(GetRXBuf()->GetSize());
    }
    else
    {
        if (m_cond_var.wait_for(lock, timeout_ms * 1ms, [this] { return (this->GetRXBuf()->GetSize() != 0); }))
        {
            data.resize(GetRXBuf()->GetSize());
            esys::memcpy((uint8_t *)data.data(), *GetRXBuf(), GetRXBuf()->GetSize());

            GetRXBuf()->Pop(GetRXBuf()->GetSize());
            return 0;
        }
        else
            return -1;  // TimeOut
    }
    // *INDENT-ON*
    return 0;
}

void SerialPortImpl::DoWrite()
{
    //If a write operation is already in progress, do nothing
    if(m_write_buffer==0)
    {
        ::boost::lock_guard<::boost::mutex> l(m_write_queue_mutex);

        m_write_buffer_size=m_write_queue.size();
        m_write_buffer.reset(new uint8_t[m_write_queue.size()]);

        std::copy(m_write_queue.begin(),m_write_queue.end(), m_write_buffer.get());

        m_write_queue.clear();

        ::boost::asio::async_write(m_port, ::boost::asio::buffer(m_write_buffer.get(),
                                   m_write_buffer_size),
                                   ::boost::bind(&SerialPortImpl::WriteEnd, this, ::boost::asio::placeholders::error));
    }
}

void SerialPortImpl::WriteEnd(const ::boost::system::error_code& error)
{
    if (!error)
    {
        ::boost::lock_guard<::boost::mutex> l(m_write_queue_mutex);

        if(m_write_queue.empty())
        {
            m_write_buffer.reset();
            m_write_buffer_size=0;

            return;
        }
        m_write_buffer_size=m_write_queue.size();
        m_write_buffer.reset(new uint8_t[m_write_queue.size()]);
        std::copy(m_write_queue.begin(), m_write_queue.end(), m_write_buffer.get());
        m_write_queue.clear();

        ::boost::asio::async_write(m_port, ::boost::asio::buffer(m_write_buffer.get(),
                                   m_write_buffer_size),
                                   ::boost::bind(&SerialPortImpl::WriteEnd, this, ::boost::asio::placeholders::error));
    }
    else
    {
        SetErrorStatus(true);
        DoClose();
    }
}

void SerialPortImpl::SetReadCallback(const ::boost::function<void (const uint8_t*, size_t)>& callback)
{
    m_callback = callback;
}

void SerialPortImpl::ClearReadCallback()
{
    m_callback.clear();
}

void SerialPortImpl::SetCallback(SerialPortCallbackBase *call_back)
{
    m_call_back = call_back;
}

SerialPortCallbackBase *SerialPortImpl::GetCallback()
{
    return m_call_back;
}

}

}



