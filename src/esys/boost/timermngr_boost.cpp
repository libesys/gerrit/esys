/*!
 * \file esys/boost/timermngr_boost.h
 * \brief The Boost implementation of the Timer Manager
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/timerbase.h"
#include "esys/boost/taskmngr.h"
#include "esys/boost/timermngr.h"
#include "esys/boost/timermngrimpl.h"
#include "esys/boost/taskplatimpl.h"
#include "esys/boost/taskplatimplinfo.h"
#include "esys/assert.h"

namespace esys
{

namespace boost
{

TimerMngr &TimerMngr::GetCurrent()
{
    TaskPlatImplInfo &info = TaskPlatImpl::GetCurImplInfo();

    assert(info.GetTaskMngr() != nullptr);

    return info.GetTaskMngr()->GetTimerMngr();
}

TimerMngr::TimerMngr(const ObjectName &name, TaskMngr *mngr) : TimerMngrBase(name)
{
    m_impl = new TimerMngrImpl("TimerMngrImpl", mngr);
}

TimerMngr::~TimerMngr()
{
    delete m_impl;
}

uint16_t TimerMngr::GetCount()
{
    return (uint16_t)m_timers.size();
}

void TimerMngr::Start(TimerBase *timer)
{
    assert(m_impl != nullptr);

    m_impl->Start(timer);
}

void TimerMngr::Stop(TimerBase *timer)
{
    assert(m_impl != nullptr);

    m_impl->Stop(timer);
}

bool TimerMngr::IsRunning(TimerBase *timer)
{
    if (m_impl == nullptr)
        return false;

    return m_impl->IsRunning(timer);
}

int32_t TimerMngr::StopAll()
{
    std::vector<TimerBase *>::iterator it;
    int32_t t_result = 0;
    int32_t result = 0;

    for (it = m_timers.begin(); it < m_timers.end(); ++it)
    {
        result=(*it)->Stop();
        if (result < 0)
            --t_result;
    }
    return t_result;
}

void TimerMngr::AddTimer(TimerBase *timer)
{
    TimerMngrBase::AddTimer(timer);

    m_timers.push_back(timer);

    assert(m_impl != nullptr);

    m_impl->AddTimer(timer);
}

}

}


