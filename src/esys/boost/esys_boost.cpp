/*!
 * \file esys/boost/esys_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/boost/esys.h"
#include "esys/boost/semaphore.h"
#include "esys/boost/mutex.h"
#include "esys/boost/taskplat.h"
#include "esys/boost/taskmngrplat.h"
#include "esys/boost/systemplat.h"
#include "esys/boost/time.h"
#include "esys/boost/timer.h"
#include "esys/mp/esys.h"
#include "esys/platform/id.h"

namespace esys
{

namespace boost
{

ESys ESys::s_esys;

ESys::ESys(): mp::ESysBase(ESYS_PLAT_BOOST, "Boost")
{
}

ESys::~ESys()
{
}

ESys &ESys::Get()
{
    return s_esys;
}

void ESys::Select()
{
    esys::mp::ESys::SetPlatform(platform::BOOST);
}

SemaphoreBase *ESys::NewSemaphoreBase(const ObjectName &name, uint32_t count)
{
    return new Semaphore(name, count);
}

MutexBase *ESys::NewMutexBase(const ObjectName &name, MutexBase::Type type)
{
    return new Mutex(name, type);
}

TaskPlatIf *ESys::NewTaskPlat(TaskBase *task_base)
{
    return new TaskPlat(task_base);
}

TaskMngrPlatIf *ESys::NewTaskMngrPlat(TaskMngrBase *taskmngr_base)
{
    return new TaskMngrPlat(taskmngr_base);
}

SystemPlatIf *ESys::NewSystemPlat(SystemBase *system_base)
{
    return new SystemPlat(system_base);
}

TimerBase *ESys::NewTimerBase(const ObjectName &name)
{
    return new Timer(name);
}

uint32_t ESys::implMillis(TimeBase::Source source)
{
    return Time::Millis(source);
}

uint64_t ESys::implMicros(TimeBase::Source source)
{
    return Time::Micros(source);
}

uint32_t ESys::implMicrosLow()
{
    return Time::MicrosLow();
}

int32_t ESys::SetTime(TimeBase::TimeStruct &time)
{
    return Time::SetTime(time);
}

int32_t ESys::GetTime(TimeBase::TimeStruct &time)
{
    return Time::GetTime(time);
}

int32_t ESys::SetDate(TimeBase::DateStruct &date)
{
    return Time::SetDate(date);
}

int32_t ESys::GetDate(TimeBase::DateStruct &date)
{
    return Time::GetDate(date);
}

int32_t ESys::SetDateTime(TimeBase::DateTimeStruct &date_time)
{
    return Time::SetDateTime(date_time);
}

int32_t ESys::GetDateTime(TimeBase::DateTimeStruct &date_time)
{
    return Time::GetDateTime(date_time);
}

int32_t ESys::StartTimestamp()
{
    return Time::StartTimestamp();
}

int32_t ESys::StopTimestamp()
{
    return Time::StopTimestamp();
}


TaskPlatIf *ESys::GetCurTaskPlatIf()
{
    return &TaskPlat::GetCurrent();
}

}

}


