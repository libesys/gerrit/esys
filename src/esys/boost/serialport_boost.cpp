/*!
 * \file esys/boost/serialport_boost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#define _WIN32_WINNT 0x0601

#include "esys/boost/serialport.h"
#include "esys/boost/serialportimpl.h"

#include <boost/locale.hpp>

namespace esys
{

namespace boost
{

SerialPort::SerialPort(): SerialPortBase()
{
#ifdef _MSC_VER
    m_impl = std::make_unique<SerialPortImpl>(this);
#else
    m_impl = std::unique_ptr<SerialPortImpl>(new SerialPortImpl(this));
#endif

    m_impl->SetRXBuf(&m_buffer);
}

SerialPort::~SerialPort()
{
}

int SerialPort::Open(const std::string& devname)
{
    return Open(::boost::locale::conv::utf_to_utf<wchar_t>(devname));
}

int SerialPort::Open(const std::wstring& devname)
{
    int result;

    assert(m_impl != NULL);
    m_name = devname;

    result = m_impl->Open(devname);
    /*if (result == 0)
        Connected(true);
    else
        Connected(false); */
    return result;
}

bool SerialPort::IsOpen() const
{
    assert(m_impl != NULL);

    return m_impl->IsOpen();
}

int SerialPort::Close()
{
    assert(m_impl != NULL);

    return m_impl->Close();
}

void SerialPort::UseCTSRTS(bool fc)
{
    assert(m_impl != NULL);

    m_impl->UseCTSRTS(fc);
}

void SerialPort::SetBaudrate(uint32_t baud)
{
    assert(m_impl != NULL);

    m_impl->SetBaudrate(baud);
}

int SerialPort::Write(uint8_t *buf, uint16_t size)
{
    assert(m_impl != NULL);

    m_impl->Write(buf, size);
    return 0;
}

int SerialPort::Write(const std::string &data)
{
    assert(m_impl != NULL);

    m_impl->Write(data);
    return 0;
}

int SerialPort::Read(std::string &data, int timeout_ms)
{
    assert(m_impl != NULL);

    return m_impl->Read(data, timeout_ms);
}

void SerialPort::read_callback(const uint8_t *buf, size_t size)
{
    m_buffer.Push((uint8_t *)buf, size);
}

void SerialPort::SetCallback(SerialPortCallbackBase *call_back)
{
    SerialPortBase::SetCallback(call_back);

    if (call_back != nullptr)
        call_back->SetSerialPortBase(this);

    assert(m_impl != nullptr);

    m_impl->SetCallback(call_back);
}

}

}
