/*!
 * \file esys/boost/taskmngrplatimpl.cpp
 * \brief Definition of the PIMPL class of the Boost TaskMngr
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/systemtask.h"
#include "esys/boost/taskmngrplatimpl.h"
#include "esys/boost/taskplatimpl.h"
#include "esys/boost/systemplatimpl.h"

namespace esys
{

namespace boost
{

TaskMngrPlatImpl &TaskMngrPlatImpl::GetCurrent()
{
    TaskPlatImplInfo &info = TaskPlatImpl::GetCurImplInfo();

    assert(info.GetTaskMngrPlatImpl() != nullptr);
    return *info.GetTaskMngrPlatImpl();
}

TaskMngrPlatImpl::TaskMngrPlatImpl(TaskMngrPlat *task_mngr_plat, TaskMngrBase *taskmngr_base)
    : m_taskmngr_plat(task_mngr_plat), m_taskmngr_base(taskmngr_base), m_wait_all_done("WaitAllDone", 0)
    , m_task_done_count(0), m_app_task_count(0)
{
}

TaskMngrPlatImpl::~TaskMngrPlatImpl()
{
}

int32_t TaskMngrPlatImpl::StartScheduler()
{
    m_wait_all_done.Wait();

    // Makes sure the thread of the SystemTask is done.
    return SystemTask::GetCurrent().Stop();
}

void TaskMngrPlatImpl::ExitScheduler()
{
    m_wait_all_done.Post();
}

uint32_t TaskMngrPlatImpl::Millis()
{
    ::boost::posix_time::ptime now = ::boost::posix_time::microsec_clock::local_time();

    ::boost::posix_time::time_duration diff = now - m_boost_started_time;
    //std::cout << "g_Millis = " << diff.total_milliseconds() << " ms" << std::endl;
    return (uint32_t)diff.total_milliseconds();
}

uint64_t TaskMngrPlatImpl::Micros()
{
    ::boost::posix_time::ptime now = ::boost::posix_time::microsec_clock::local_time();

    ::boost::posix_time::time_duration diff = now - m_boost_started_time;
    return (uint64_t)diff.total_microseconds();
}

uint32_t TaskMngrPlatImpl::MicrosLow()
{
    uint64_t micros;
    ::boost::posix_time::ptime now = ::boost::posix_time::microsec_clock::local_time();

    ::boost::posix_time::time_duration diff = now - m_boost_started_time;

    micros = (uint64_t)diff.total_microseconds();
    micros = (micros << 32) >> 32;
    return (uint32_t)micros;
}

int32_t TaskMngrPlatImpl::RegisterPlat()
{
    // Must register TaskMngr
    SystemPlatImpl &sys_plat = SystemPlatImpl::GetCurrent();

    sys_plat.SetTaskMngrPlatImpl(this);
    sys_plat.SetTaskMngrPlat(m_taskmngr_plat);
    sys_plat.SetTaskMngrBase(m_taskmngr_base);

    return 0;
}

}

}








