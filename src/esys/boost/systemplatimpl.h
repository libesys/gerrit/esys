/*!
 * \file esys/boost/systemplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/systemplatif.h"
#include "esys/systembase.h"
#include "esys/boost/systemplatimplinfo.h"

#include <boost/date_time/posix_time/posix_time.hpp>

namespace esys
{

class ESYS_API TaskMngrBase;

namespace boost
{

class ESYS_API SystemPlat;
class ESYS_API TaskMngrPlatImpl;
class ESYS_API TaskMngrPlat;
class ESYS_API TaskMngr;

/*! \class SystemPlatImpl esys/boost/systemplatimpl.h "esys/boost/systemplatimpl.h"
 *  \brief Defines the Boost SystemPlatImpl when there is a scheduler
 */
class ESYS_API SystemPlatImpl
{
public:
    SystemPlatImpl(SystemPlat *system_plat);
    virtual ~SystemPlatImpl();

    int32_t PlatInit();
    int32_t PlatRelease();
    SystemBase *GetSystemBase();
    void SetSystemBase(SystemBase *system_base);
    ::boost::posix_time::ptime GetStartedTime();
    ::boost::thread::id GetThreadId();

    uint32_t Millis();
    uint64_t Micros();
    uint32_t MicrosLow();

    void SetSystemPlat(SystemPlat *system_plat);
    SystemPlat *GetSystemPlat();

    void SetTaskMngrPlatImpl(TaskMngrPlatImpl *taskmngr_plat_impl);
    TaskMngrPlatImpl *GetTaskMngrPlatImpl();

    void SetTaskMngrPlat(TaskMngrPlat *taskmngr_plat);
    TaskMngrPlat *GetTaskMngrPlat();

    void SetTaskMngr(TaskMngr *taskmngr);
    TaskMngr *GetTaskMngr();

    void SetTaskMngrBase(TaskMngrBase *taskmngr_base);
    TaskMngrBase *GetTaskMngrBase();

    static SystemPlatImpl &GetCurrent();
protected:
    static void Register(::boost::thread::id thread_id, SystemPlatImplInfo &info);

    SystemBase *m_system_base = nullptr;
    SystemPlat *m_system_plat = nullptr;
    ::boost::posix_time::ptime m_boost_started_time;
    ::boost::thread::id m_thread_id;
    TaskMngrPlatImpl *m_taskmngr_plat_impl = nullptr;
    TaskMngrPlat *m_taskmngr_plat = nullptr;
    TaskMngr *m_taskmngr = nullptr;
    TaskMngrBase *m_taskmngr_base = nullptr;
};

}

}







