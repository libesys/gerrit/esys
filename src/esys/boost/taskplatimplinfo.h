/*!
 * \file esys/boost/taskplatimplinfo.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"

namespace esys
{

class ESYS_API TaskBase;

namespace boost
{

class ESYS_API TaskMngr;
class ESYS_API TaskMngrPlatImpl;
class ESYS_API Task;
class ESYS_API TaskPlat;
class ESYS_API TaskPlatImpl;
class ESYS_API SystemPlat;

class ESYS_API TaskPlatImplInfo
{
public:
    TaskPlatImplInfo();

    void SetTaskMngrPlatImpl(TaskMngrPlatImpl *task_mngr_plat_impl);
    TaskMngrPlatImpl *GetTaskMngrPlatImpl();
    void SetTaskPlatImpl(TaskPlatImpl *task_plat_impl);
    TaskPlatImpl *GetTaskPlatImpl();
    void SetTaskMngr(TaskMngr *task_mngr);
    TaskMngr *GetTaskMngr();
    void SetTaskPlat(TaskPlat *task_plat);
    TaskPlat *GetTaskPlat();
    void SetTask(Task *task);
    Task *GetTask();
    void SetTaskBase(TaskBase *task_base);
    TaskBase *GetTaskBase();
    void SetSystemPlat(SystemPlat *system_plat);
    SystemPlat *GetSystemPlat();
protected:
    TaskMngrPlatImpl *m_task_mngr_plat_impl = nullptr;
    TaskPlatImpl *m_task_plat_impl = nullptr;

    TaskMngr *m_task_mngr = nullptr;
    TaskPlat *m_task_plat = nullptr;
    Task *m_task = nullptr;
    TaskBase *m_task_base = nullptr;
    SystemPlat *m_system_plat = nullptr;
};

}

}






