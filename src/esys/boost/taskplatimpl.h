/*!
 * \file esys/boost/taskplatimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/esys_defs.h"
#include "esys/taskplatif.h"
#include "esys/taskbase.h"
#include "esys/boost/task.h"
#include "esys/boost/semaphore.h"
#include "esys/boost/taskplatimplinfo.h"

#include <boost/thread.hpp>
#include <map>

namespace esys
{

namespace boost
{

//class ESYS_API TaskMngr;
//class ESYS_API TaskMngrPlatImpl;
class ESYS_API TaskPlat;

/*! \class TaskPlat esys/boost/scheduler/taskplatimpl.h "esys/boost/scheduler/taskplatimpl.h"
 *  \brief
 */
class ESYS_API TaskPlatImpl
{
public:
    TaskPlatImpl(TaskPlat *task_plat, TaskBase *task_base);
    virtual ~TaskPlatImpl();

    int32_t Create();
    int32_t Start();
    int32_t Stop();
    int32_t Kill();
    int32_t Destroy();
    int32_t Done();

    millis_t Millis();
    void Sleep(millis_t ms);
    void SleepU(micros_t us);

    void CallEntry();

    void SetTaskBase(TaskBase *task_base);

    void SetTask(Task *task);
    Task *GetTask();
    //virtual void SetTaskMngrBase(TaskMngrBase *task_mngr_base)=0;
    //virtual void NotifyExit()=0;
    static TaskPlatImplInfo *TryGetCurImplInfo();
    static TaskPlatImplInfo &GetCurImplInfo();
protected:
    static void Register(::boost::thread::id thread_id, TaskPlatImplInfo &helper);
    static void SetThreadName(::boost::thread::id threadId, const char* threadName);

    typedef std::map< ::boost::thread::id, TaskPlatImplInfo> ThreadIdMap;
    static ThreadIdMap s_thread_id_map;

    esys::boost::Task *m_task;
    esys::boost::TaskPlat *m_task_plat;
    TaskBase *m_task_base;
    ::boost::thread m_thread;       //!< Boost thread used
    Semaphore m_sem_registered;
};

}

}





