/*!
 * \file esys/pluginmngrbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/pluginmngrbase.h"
//#include "esys/pluginmngrbaseimpl.h"
#include "esys/pluginbase.h"

#include <boost/filesystem.hpp>
#include <boost/locale.hpp>

#include <memory>

namespace esys
{

std::string PluginMngrBase::m_base_folder;
std::string PluginMngrBase::m_app_exe;

void PluginMngrBase::SetBaseFolder(const std::string &base_folder)
{
    m_base_folder = base_folder;
}

const std::string &PluginMngrBase::GetBaseFolder()
{
    return m_base_folder;
}

void PluginMngrBase::SetAppExe(const std::string &app_exe)
{
    m_app_exe = app_exe;

    boost::filesystem::path exe_path = app_exe;
    exe_path.remove_filename();
    if (!exe_path.is_absolute())
        exe_path = boost::filesystem::absolute(exe_path);
    exe_path.normalize();
    SetBaseFolder(exe_path.generic_string());
}

const std::string &PluginMngrBase::GetAppExe()
{
    return m_app_exe;
}

void PluginMngrBase::SetPluginFileName(PluginBase *plugin, const std::string &filename)
{
    plugin->SetFileName(filename);
}

PluginMngrBase::PluginMngrBase()
{
    /*#ifdef _MSC_VER
        m_impl = std::make_unique<PluginMngrBaseImpl>(this);
    #else
        m_impl = std::unique_ptr<PluginMngrBaseImpl>(new PluginMngrBaseImpl(this));
    #endif*/
}

PluginMngrBase::~PluginMngrBase()
{
}

void PluginMngrBase::SetDir(const std::string &dir)
{
    m_dir = dir;
}

const std::string &PluginMngrBase::GetDir()
{
    return m_dir;
}

void PluginMngrBase::SetVerboseLevel(uint32_t verbose_level)
{
    m_verbose_level = verbose_level;
}

uint32_t PluginMngrBase::GetVerboseLevel()
{
    return m_verbose_level;
}

const std::string &PluginMngrBase::GetEntryFctName()
{
    return m_entry_fct_name;
}

void PluginMngrBase::SetEntryFctName(const std::string &entry_fct_name)
{
    m_entry_fct_name = entry_fct_name;
}

}





