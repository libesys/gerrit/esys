/*!
 * \file esys/esys.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/esys.h"

#ifndef ESYS_MULTI_PLAT

namespace esys
{

ESys::ESys(int_t platform_id, ESys *element)
{
}

void ESys::SetElement(ESys *element)
{
}

}
#else

#include "esys/platform/id.h"

#include <cassert>

namespace esys
{

int_t ESys::m_platform_id=-1;
ESys *ESys::m_esys=NULL;
platform::Id *ESys::m_platform=NULL;

void ESys::Sleep(millis_t msec)
{
    Get().impSleep(msec);
}

millis_t ESys::Millis()
{
    return Get().impMillis();
}

void ESys::Init()
{
    Get().impInit();
}

void ESys::Release()
{
    Get().impRelease();

    GetById(ESYS_PLAT_SYSC)->impRelease();
}

ESys::ESys(int_t platform_id): BaseType(platform_id)
{
}

ESys::~ESys()
{
}

ESys &ESys::Get()
{
    if (m_esys==NULL)
        SetPlatform(ESYS_PLAT_MULTIOS_WX);

    assert(m_esys!=NULL);

    return *m_esys;
}

void ESys::SetAppName(const char *app_name)
{
}

int32_t ESys::CreateLog()
{
    return -1;
}

void ESys::FlushLog()
{
}

int32_t ESys::GetExeDir(std::wstring &exe_dir)
{
    return -1;
}

void ESys::SetPlatform(int platform_id)
{
    m_platform_id=platform_id;
    m_platform=platform::Id::Get(platform_id);
    m_esys=GetById(platform_id);
}

void ESys::SetPlatform(platform::Id &platform_id)
{
    m_platform=&platform_id;
    m_platform_id=platform_id.GetId();
    m_esys=GetById(platform_id);
}

platform::Id *ESys::GetPlatformId()
{
    platform::Id *p_id;
    int_t id=GetId();

    p_id=platform::Id::Get(id);
    return p_id;
}

TaskPlatIf *ESys::NewTaskPlat(const char *name)
{
    return Get().impNewTaskPlat(name);
}

TaskMngrBase *ESys::NewTaskMngrBase()
{
    return Get().impNewTaskMngrBase();
}

MutexBase *ESys::NewMutexBase(MutexBase::Type type)
{
    return Get().impNewMutexBase(type);
}

}

#endif


