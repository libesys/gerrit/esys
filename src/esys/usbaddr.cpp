/*!
 * \file esys/usbaddr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/usbaddr.h"

#include <sstream>
#include <iomanip>

namespace esys
{

USBAddr::USBAddr()
    : m_vid(0), m_pid(0), m_is_rev_set(false), m_is_mi_set(false)
{
}

USBAddr::USBAddr(uint16_t vid, uint16_t pid)
    : m_vid(vid), m_pid(pid), m_is_rev_set(false), m_is_mi_set(false)
{
}

USBAddr::USBAddr(uint16_t vid, uint16_t pid, uint16_t rev)
    : m_vid(vid), m_pid(pid), m_rev(rev), m_is_rev_set(true), m_is_mi_set(false)
{
}

USBAddr::USBAddr(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi)
    : m_vid(vid), m_pid(pid), m_rev(rev), m_is_rev_set(true), m_mi(mi), m_is_mi_set(true)
{
}

USBAddr::~USBAddr()
{
}

void USBAddr::Set(uint16_t vid, uint16_t pid)
{
    m_vid=vid;
    m_pid=pid;
}

void USBAddr::Set(uint16_t vid, uint16_t pid, uint16_t rev)
{
    m_vid=vid;
    m_pid=pid;
    m_rev=rev;
    m_is_rev_set=true;
}

void USBAddr::Set(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi)
{
    m_vid=vid;
    m_pid=pid;
    m_rev=rev;
    m_is_rev_set=true;
    m_mi=mi;
    m_is_mi_set=true;
}

void USBAddr::SetVID(uint16_t vid)
{
    m_vid=vid;
}

void USBAddr::SetPID(uint16_t pid)
{
    m_pid=pid;
}

void USBAddr::SetREV(uint16_t rev,bool present)
{
    m_rev=rev;
    m_is_rev_set=present;
}

void USBAddr::SetMI(uint16_t mi,bool present)
{
    m_mi=mi;
    m_is_mi_set=present;
}

uint16_t USBAddr::GetVID() const
{
    return m_vid;
}

uint16_t USBAddr::GetPID() const
{
    return m_pid;
}

uint16_t USBAddr::GetREV() const
{
    return m_rev;
}

bool USBAddr::IsREVSet()
{
    return m_is_rev_set;
}

uint16_t USBAddr::GetMI() const
{
    return m_mi;
}

bool USBAddr::IsMISet()
{
    return m_is_mi_set;
}

const std::string &USBAddr::GetAddr(bool full)
{
    std::string s;
    std::ostringstream oss;

    oss << std::setw(4) << std::setfill('0') << std::hex << m_vid << "-" << std::setw(4) << std::setfill('0') << m_pid;
    if (full)
    {
        if (m_is_rev_set)
            oss << "-" << std::setw(4) << std::setfill('0') << std::hex << m_rev;
        if (m_is_mi_set)
            oss << "-" << std::setw(2) << std::setfill('0') << std::hex << m_mi;
    }
    m_addr=oss.str();
    return m_addr;
}

bool USBAddr::operator==(const USBAddr &addr) const
{
    if ((m_is_rev_set==false)&&(m_is_mi_set==false)&&(addr.m_is_rev_set==false)&&(addr.m_is_mi_set==false))
    {
        return ( (m_vid==addr.m_vid)&&(m_vid==addr.m_vid) );
    }
    else if ((m_is_rev_set==true)&&(m_is_mi_set==true)&&(addr.m_is_rev_set==true)&&(addr.m_is_mi_set==true))
    {
        return ( (m_vid==addr.m_vid)&&(m_vid==addr.m_vid)&&(m_rev==addr.m_rev)&&(m_mi==addr.m_mi) );
    }
    else if ((m_is_rev_set==true)&&(m_is_mi_set==false)&&(addr.m_is_rev_set==true)&&(addr.m_is_mi_set==false))
    {
        return ( (m_vid==addr.m_vid)&&(m_vid==addr.m_vid)&&(m_rev==addr.m_rev) );
    }
    return false;
}

}
