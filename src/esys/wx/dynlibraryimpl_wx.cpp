/*!
 * \file esys/wx/dynlibraryimpl_wx.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/wx/dynlibraryimpl.h"

namespace esys
{

namespace wx
{

DynLibraryImpl::DynLibraryImpl(DynLibrary *self) : m_self(self)
{
}

DynLibraryImpl::~DynLibraryImpl()
{
}

int DynLibraryImpl::Load(const std::wstring &filename)
{
    bool result = m_dyn_lib.Load(filename);

    if (result == true)
        return 0;
    return -1;
}

int DynLibraryImpl::UnLoad()
{
    m_dyn_lib.Unload();
    return 0;
}

bool DynLibraryImpl::IsLoaded()
{
    return m_dyn_lib.IsLoaded();
}

bool DynLibraryImpl::HasSymbol(const std::wstring& name)
{
    return m_dyn_lib.HasSymbol(name);
}

void *DynLibraryImpl::GetSymbol(const std::wstring& name)
{
    return m_dyn_lib.GetSymbol(name);
}

}

}

