/*!
 * \file esys/wx/pluginmngrbaseimpl_wx.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/pluginbase.h"
#include "esys/pluginmngrbase.h"
#include "esys/wx/pluginmngrbaseimpl.h"

#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>

#include <iostream>

namespace esys
{

PluginMngrImplHelper::PluginMngrImplHelper()
{
}

PluginMngrImplHelper::~PluginMngrImplHelper()
{
    if (m_dyn_lib != nullptr)
        delete m_dyn_lib;
}

void PluginMngrImplHelper::SetDynLib(wxDynamicLibrary *dyn_lib)
{
    m_dyn_lib = dyn_lib;
}

wxDynamicLibrary *PluginMngrImplHelper::GetDynLib()
{
    return m_dyn_lib;
}

void PluginMngrImplHelper::SetEntryPoint(void *entry)
{
    m_entry_fct = entry;
}

void *PluginMngrImplHelper::GetEntryPoint()
{
    return m_entry_fct;
}

void PluginMngrImplHelper::SetPlugin(PluginBase *plugin)
{
    m_plugin = plugin;
}

PluginBase *PluginMngrImplHelper::GetPlugin()
{
    return m_plugin;
}

PluginMngrBaseImpl::PluginMngrBaseImpl(PluginMngrBase *self): m_self(self)
{
}

PluginMngrBaseImpl::~PluginMngrBaseImpl()
{
}

uint32_t PluginMngrBaseImpl::GetVerboseLevel()
{
    assert(m_self != nullptr);

    return m_self->GetVerboseLevel();
}

void PluginMngrBaseImpl::SetSelf(PluginMngrBase *self)
{
    m_self = self;
}

void PluginMngrBaseImpl::SetDir(const std::wstring &dir)
{
    m_dir = dir;
}

int PluginMngrBaseImpl::Load()
{
    DBG_CALLMEMBER_RET("esys::wx::PluginMngrBaseImpl::Load", false, int, result);
    DBG_CALLMEMBER_END;

    wxString abs_plugin_dir;
    wxString plugin_dir;
    wxPathList path;
    wxDir dir;
    void *plugin_entry_function;
    PluginBase *plugin;
    wxFileName wxfilename;
    wxFileName filepath;
    wxDynamicLibrary *plugin_lib = nullptr;
    PluginMngrImplHelper *helper;

    assert(m_self != nullptr);

    wxfilename.AssignDir(m_dir);
    wxfilename.MakeAbsolute();
    abs_plugin_dir = wxfilename.GetPath();
    //abs_plugin_dir=path.FindAbsoluteValidPath(m_plugin_dir);

    if (GetVerboseLevel() > 0)
    {
        std::cout << "Plugin folder = " << abs_plugin_dir.c_str() << std::endl;
    }
#ifdef WIN32
    SetDllDirectory(abs_plugin_dir.ToStdWstring().c_str());
#endif

    if (dir.Open(abs_plugin_dir) == false)
    {
        //ERROR
        result = -1;
        return result;
    }
#ifdef WIN32
    wxString mask = wxT("*.dll");
#else
    wxString mask = wxT("*.so");
#endif
    if (dir.GetFirst(&plugin_dir, mask/*wxEmptyString*/, wxDIR_FILES) == false)
    {
        result = -2;
        return result;
    }

    do
    {
        if (plugin_lib == nullptr)
            plugin_lib = new wxDynamicLibrary;
        else
            plugin_lib->Unload();
        filepath = wxfilename;
        filepath.SetFullName(plugin_dir);

        if (plugin_lib->Load(filepath.GetFullPath()) == false)
        {
            //ERROR failed to load the library
            continue;
        }
        if (plugin_lib->HasSymbol(m_self->GetEntryFctName()) == false)
        {
            //ERROR failed find the symbol get_up_user_soft_lib
            continue;
        }
        /*helper=__new_helper();
        helper->m_dyn_lib=plugin_lib;
        plugin_lib=NULL; */

        plugin_entry_function = plugin_lib->GetSymbol(m_self->GetEntryFctName());
        //plugin_entry_function = (PluginEntryFunction)plugin_lib->GetSymbol(wxT("get_esysprog_plugin"));

        plugin = m_self->GetPluginFromEntryFct(plugin_entry_function);

        //    (*plugin_entry_function)();
#ifdef _MSC_VER
#ifdef _DEBUG
        if (plugin->IsDebug() == false)
            continue;
#elif NDEBUG
        if (plugin->IsDebug() == true)
            continue;
#else
#error _DEBUG or NDEBUG must be defined
#endif
#endif
        /*if (up_sock_mngr_lib->IsSameSystemCVersion()==false)
        continue;
        if (up_sock_mngr_lib->IsSameType()==false)
        continue; */
        helper = new PluginMngrImplHelper();
        m_plugins.push_back(helper);
        helper->SetDynLib(plugin_lib);
        plugin_lib = nullptr;
        helper->SetEntryPoint(plugin_entry_function);
        helper->SetPlugin(plugin);
        //m_self->SetPluginFileName(plugin, plugin_dir.ToStdWstring());
        //BoardInfoLibMngr::GetDefault().Add(plugin->GetBoardInfoLib());

    }
    while (dir.GetNext(&plugin_dir));
    result = 0;
    if (plugin_lib != nullptr)
        delete plugin_lib;
    return result;
}

int PluginMngrBaseImpl::Release()
{
    uint16_t idx;

    for (idx = 0; idx<m_plugins.size(); ++idx)
    {
        assert(m_plugins[idx]->GetPlugin() != nullptr);

        m_plugins[idx]->GetPlugin()->Release();
        delete m_plugins[idx];
    }
    m_plugins.clear();

    return 0;
}

std::size_t PluginMngrBaseImpl::GetNbr()
{
    return m_plugins.size();
}

PluginBase *PluginMngrBaseImpl::Get(std::size_t index)
{
    if (index >= m_plugins.size())
        return nullptr;
    return m_plugins[index]->GetPlugin();
}

}
