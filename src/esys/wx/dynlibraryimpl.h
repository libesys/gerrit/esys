/*!
 * \file esys/wx/dynlibraryimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/wx/dynlibrary.h"

#include <wx/dynlib.h>

namespace esys
{

namespace wx
{

class ESYS_API DynLibraryImpl
{
public:
    DynLibraryImpl(DynLibrary *self);
    virtual ~DynLibraryImpl();

    int Load(const std::wstring &filename);
    int UnLoad();
    bool IsLoaded();
    bool HasSymbol(const std::wstring& name);
    void *GetSymbol(const std::wstring& name);
protected:
    DynLibrary *m_self = nullptr;
    wxDynamicLibrary m_dyn_lib;
};

}

}

