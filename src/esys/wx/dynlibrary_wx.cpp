/*!
 * \file esys/wx/dynlibrary_wx.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/wx/dynlibrary.h"
#include "esys/wx/dynlibraryimpl.h"
#include "esys/assert.h"

namespace esys
{

namespace wx
{

DynLibrary::DynLibrary() : DynLibraryBase()
{
    m_impl = new DynLibraryImpl(this);
}

DynLibrary::~DynLibrary()
{
    if (m_impl != nullptr)
        delete m_impl;
}

int DynLibrary::Load(const std::wstring &filename)
{
    assert(m_impl != nullptr);

    return m_impl->Load(filename);
}

int DynLibrary::UnLoad()
{
    assert(m_impl != nullptr);

    return m_impl->UnLoad();
}

bool DynLibrary::IsLoaded()
{
    assert(m_impl != nullptr);

    return m_impl->IsLoaded();
}

bool DynLibrary::HasSymbol(const std::wstring& name)
{
    assert(m_impl != nullptr);

    return m_impl->HasSymbol(name);
}

void *DynLibrary::GetSymbol(const std::wstring& name)
{
    assert(m_impl != nullptr);

    return m_impl->GetSymbol(name);
}

}

}
