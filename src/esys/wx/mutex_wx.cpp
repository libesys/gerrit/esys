/*!
 * \file esys/wx/mutex.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/wx/mutex.h"

#include <wx/thread.h>

namespace esys
{

namespace wx
{

Mutex::Mutex(const ObjectName &name, Type type): esys::MutexBase(name, type), m_mutex(NULL)
{
    wxMutexType type_;

    if (type==DEFAULT)
        type_=wxMUTEX_DEFAULT;
    else if (type==RECURSIVE)
        type_=wxMUTEX_RECURSIVE;
    else
        return;
    m_mutex=new wxMutex(type_);
}

Mutex::~Mutex()
{
    if (m_mutex!=NULL)
        delete m_mutex;
}

int16_t Mutex::Lock()
{
    wxMutexError e;

    if (m_mutex==NULL)
        return -1;
    e=m_mutex->Lock();
    if (e==wxMUTEX_NO_ERROR)
        return 0;
    return -2;

}

int16_t Mutex::UnLock()
{
    wxMutexError e;

    if (m_mutex==NULL)
        return -1;

    e=m_mutex->Unlock();
    if (e==wxMUTEX_NO_ERROR)
        return 0;
    return -2;
}

int16_t Mutex::TryLock()
{
    wxMutexError e;

    if (m_mutex==NULL)
        return -1;

    e=m_mutex->TryLock();
    if (e==wxMUTEX_NO_ERROR)
        return 0;
    return -2;
}


}

}


