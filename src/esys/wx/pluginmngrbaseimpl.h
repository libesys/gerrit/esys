/*!
 * \file esys/wx/pluginmngrbaseimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/pluginmngrbaseimpl.h"
#include "esys/pluginbase.h"

#include <wx/dynlib.h>
#include <wx/dir.h>
#include <wx/filefn.h>
#include <wx/filename.h>

namespace esys
{

class ESYS_API PluginBase;
class ESYS_API PluginMngrBase;

class PluginMngrImplHelper
{
public:
    PluginMngrImplHelper();
    virtual ~PluginMngrImplHelper();

    void SetDynLib(wxDynamicLibrary *dyn_lib);
    wxDynamicLibrary *GetDynLib();

    void SetEntryPoint(void *entry);
    void *GetEntryPoint();

    void SetPlugin(PluginBase *plugin);
    PluginBase *GetPlugin();
protected:
    wxDynamicLibrary *m_dyn_lib = nullptr;      //!< Pointer to the object implementing the dynamic loading
    void *m_entry_fct = nullptr;                //!< The entry point giving access to the plugin
    PluginBase *m_plugin = nullptr;             //!< Pointer to the plugin
};

class ESYS_API PluginMngrBaseImpl
{
public:
    PluginMngrBaseImpl(PluginMngrBase *self = nullptr);
    virtual ~PluginMngrBaseImpl();

    void SetSelf(PluginMngrBase *self);

    void SetDir(const std::wstring &dir);       //!< Set the directory where to find plugins
    int Load();                                 //!< Load the plugins
    int Release();                              //!< Release the plugins
    std::size_t GetNbr();                       //!< Return the number of loaded plugins
    PluginBase *Get(std::size_t index);         //!< Return the plugin with a given index

    uint32_t GetVerboseLevel();
protected:
    PluginMngrBase *m_self = nullptr;
    std::vector<PluginMngrImplHelper *> m_plugins;
    std::wstring m_dir;
};

}



