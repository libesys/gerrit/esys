/*!
 * \file esys/object.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/object.h"
#include "esys/objectmngr.h"
#include "esys/assert.h"

namespace esys
{

#ifdef ESYS_OBJECT_META
int32_t Object::g_count = 0;
#endif

Object::Object(const ObjectName &name)
    : TreeItem_if<Object>()
#ifdef ESYS_OBJECT_META
    , m_name(name.GetName())
#endif
{
#ifdef ESYS_OBJECT_META
    m_id = g_count;
    ++g_count;
#endif

    ObjectName *cur = ObjectName::GetCurrent();
    ObjectName *prev;

    assert(cur != nullptr);

    if (cur->GetObject() == nullptr)
    {
        cur->SetObject(this);
        prev = cur->GetPrev();
        if (prev != nullptr)
        {
            assert(prev->GetObject() != nullptr);
            /* Note that the 2 following line are commented out and left there to have the chance to explain why.
               Since we are in constructor, calling SetParent will fail because the virtual table hasn't been
               initialized yet and SetParent must be delayed till the point where the constructor has returned,
               which is in the destructor of ObjectName.

               The reason for not being able to do the AddChild is basically the same, but more complex to understand.
               AddChild is defined in TreeItem_if and implemented in TreeLeaf. It's thus safe to call AddChild when the
               constructor of TreeLeaf is done. But hold on a second, when the destructor of ObjectName is called,
               the constructor of the previous Object hasn't completed yet, so we can't call AddChild
               on it. Actually, we can because if one looks at the definition of ObjectNode, the constructor of
               TreeItem is called BEFORE the constructor of Object; so the when the constructor of Object starts,
               the entry in the virtual table for AddChild was already configured.

            */
            //prev->GetObject()->AddChild(this);
            //SetParent(prev->GetObject());
        }
    }
    //assert(ObjectMngr::GetCurrent())
}

Object::~Object()
{
}

/*TreeItem<Object> *Object::GetTreeItemT()
{
    return this;
}*/

/*Object *Object::GetT()
{
    return this;
}*/

int32_t Object::StaticInitBeforeChildren()
{
    return 0;
}

int32_t Object::StaticInitAfterChildren()
{
    return 0;
}

int32_t Object::StaticReleaseBeforeChildren()
{
    return 0;
}

int32_t Object::StaticReleaseAfterChildren()
{
    return 0;
}

int32_t Object::RuntimeInitBeforeChildren(RuntimeLevel runtime_lvl)
{
    return 0;
}

int32_t Object::RuntimeInitAfterChildren(RuntimeLevel runtime_lvl)
{
    return 0;
}

int32_t Object::RuntimeReleaseBeforeChildren(RuntimeLevel runtime_lvl)
{
    return 0;
}

int32_t Object::RuntimeReleaseAfterChildren(RuntimeLevel runtime_lvl)
{
    return 0;
}

int32_t Object::StaticInit(Order order)
{
    if (order == BEFORE_CHILDREN)
        return StaticInitBeforeChildren();
    if (order == AFTER_CHILDREN)
        return StaticInitAfterChildren();

    assert(false);
    return -1;
}

int32_t Object::StaticRelease(Order order)
{
    if (order == BEFORE_CHILDREN)
        return StaticReleaseBeforeChildren();
    if (order == AFTER_CHILDREN)
        return StaticReleaseAfterChildren();

    assert(false);
    return -1;
}

int32_t Object::RuntimeInit(RuntimeLevel runtime_lvl, Order order)
{
    if (order == BEFORE_CHILDREN)
        return RuntimeInitBeforeChildren(runtime_lvl);
    if (order == AFTER_CHILDREN)
        return RuntimeInitAfterChildren(runtime_lvl);

    assert(false);
    return -1;
}

int32_t Object::RuntimeRelease(RuntimeLevel runtime_lvl, Order order)
{
    if (order == BEFORE_CHILDREN)
        return RuntimeReleaseBeforeChildren(runtime_lvl);
    if (order == AFTER_CHILDREN)
        return RuntimeReleaseAfterChildren(runtime_lvl);

    assert(false);
    return -1;
}

#ifdef ESYS_OBJECT_META
const char *Object::GetName() const
{
    return m_name;
}

int32_t Object::GetId() const
{
    return m_id;
}
#endif

}


