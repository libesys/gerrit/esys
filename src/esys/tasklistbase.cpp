/*!
 * \file esys/tasklistbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/tasklistbase.h"
#include "esys/taskbase.h"

#include "esys/assert.h"

namespace esys
{

TaskListBase::TaskListBase()
{
}

TaskListBase::~TaskListBase()
{
}

uint32_t TaskListBase::GetNbrTask(TaskType typ)
{
    assert((typ == TaskType::INTERNAL) || (typ == TaskType::APPLICATION));

    return m_list_info[typ].m_count;
}

uint32_t TaskListBase::GetActiveCount(TaskType typ)
{
    assert((typ == TaskType::INTERNAL) || (typ == TaskType::APPLICATION));

    return m_list_info[typ].m_active_count;
}

void TaskListBase::IncActiveCount(TaskType typ)
{
    assert((typ == TaskType::INTERNAL) || (typ == TaskType::APPLICATION));

    m_list_info[typ].m_active_count++;
}

void TaskListBase::DecActiveCount(TaskType typ)
{
    assert((typ == TaskType::INTERNAL) || (typ == TaskType::APPLICATION));
    assert(m_list_info[typ].m_active_count > 0);

    m_list_info[typ].m_active_count--;
}

TaskBase *TaskListBase::GetFirst(TaskType typ)
{
    assert((typ >= 0) && (typ < 2));

    return m_list_info[typ].m_first;
}

TaskBase *TaskListBase::GetLast(TaskType typ)
{
    assert((typ >= 0) && (typ < 2));

    return m_list_info[typ].m_last;
}

void TaskListBase::Reset()
{
    uint32_t idx;

    for (idx = 0; idx < 2; ++idx)
    {
        m_list_info[idx].m_count = 0;
        m_list_info[idx].m_first = nullptr;
        m_list_info[idx].m_last = nullptr;
    }
}

void TaskListBase::Add(TaskBase *task_base)
{
    TaskType typ;

    assert(task_base != nullptr);

    typ = task_base->GetType();

    assert((typ >= 0) && (typ < 2));

    if (m_list_info[typ].m_first == nullptr)
    {
        m_list_info[typ].m_first = task_base;
        m_list_info[typ].m_last = task_base;
        m_list_info[typ].m_count = 1;
    }
    else
    {
        m_list_info[typ].m_last->SetNextTask(task_base);
        m_list_info[typ].m_last = task_base;
        m_list_info[typ].m_count += 1;
    }
}

}



