/*!
 * \file esys/module.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/module.h"
#include "esys/systembase.h"
#include "esys/modulelist.h"

namespace esys
{

ModuleList g_list_module;

ModuleList &Module::s_list_module = g_list_module;

Module::Module(const ObjectName &name, int32_t order_id)
    : ListItem<Module>(), ObjectMngr(name), m_order_id(order_id), m_system_base(nullptr)
{
    ModuleList::GetCurrent().Add(this);
}

Module::~Module()
{
}

void Module::SetNext(Module *next)
{
    ListItem<Module>::SetNext(next);
}

void Module::SetPrev(Module *prev)
{
    ListItem<Module>::SetPrev(prev);
}

Module *Module::GetNext()
{
    return ListItem<Module>::GetNext();
}

Module *Module::GetPrev()
{
    return ListItem<Module>::GetPrev();
}

int32_t Module::GetOrderId()
{
    return m_order_id;
}

void Module::SetOrderId(int32_t order_id)
{
    m_order_id = order_id;
}

SystemBase *Module::GetSystemBase()
{
    return m_system_base;
}

void Module::SetSystemBase(SystemBase *system_base)
{
    m_system_base = system_base;
}

}


