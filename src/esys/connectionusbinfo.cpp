/*!
 * \file esys/connectionusbinfo.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/connectionusbinfo.h"

#include <string>

namespace esys
{

uint32_t ConnectionUSBInfo::Baudrate::m_next=1;

const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_9600("9600", 9600);
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_19200("19200", 19200);//		=2;
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_38400("38400", 38400);//		=3;
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_57600("57600", 57600);//		=4;
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_115200("115200", 115200);//	=5;
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_230400("230400", 230400);
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_460800("460800", 460800);
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_921600("921600", 921600);
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_1M5("1500000", 1500000);
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_1M8432("1M8432", 1843200);
const ConnectionUSBInfo::Baudrate ConnectionUSBInfo::BPS_3M("3M", 3000000);

ConnectionUSBInfo::ConnectionUSBInfo()
    : ConnectionInfo(ConnectionInfo::USB), m_usb_addr(), m_baudrate(NULL), m_use_ctsrts(false), m_stop_bits(1)
{
}

ConnectionUSBInfo::ConnectionUSBInfo(uint16_t vid, uint16_t pid,const Baudrate *b)
    : ConnectionInfo(ConnectionInfo::USB), m_usb_addr(vid,pid), m_baudrate(b), m_use_ctsrts(false), m_stop_bits(1)
{
}

ConnectionUSBInfo::ConnectionUSBInfo(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi, const Baudrate *b)
    : ConnectionInfo(ConnectionInfo::USB), m_usb_addr(vid,pid,rev,mi), m_baudrate(b), m_use_ctsrts(false), m_stop_bits(1)
{
}

ConnectionUSBInfo::~ConnectionUSBInfo()
{
}

bool ConnectionUSBInfo::UseCTSRTS() const
{
    return m_use_ctsrts;
}

void ConnectionUSBInfo::SetUseCTSRTS(bool use)
{
    m_use_ctsrts=use;
}

uint16_t ConnectionUSBInfo::GetStopBits() const
{
    return m_stop_bits;
}

void ConnectionUSBInfo::SetStopBits(uint16_t stop_bits)
{
    m_stop_bits=stop_bits;
}

void ConnectionUSBInfo::SetUSBAddr(uint16_t vid, uint16_t pid)
{
    m_usb_addr.Set(vid, pid);
}

void ConnectionUSBInfo::SetUSBAddr(uint16_t vid, uint16_t pid, uint16_t rev)
{
    m_usb_addr.Set(vid, pid, rev);
}

void ConnectionUSBInfo::SetUSBAddr(uint16_t vid, uint16_t pid, uint16_t rev, uint16_t mi)
{
    m_usb_addr.Set(vid, pid, rev, mi);
}

const USBAddr &ConnectionUSBInfo::GetUSBAddr() const
{
    return m_usb_addr;
}

USBAddr &ConnectionUSBInfo::GetUSBAddr()
{
    return m_usb_addr;
}

void ConnectionUSBInfo::SetBaudrate(const Baudrate *b)
{
    m_baudrate=b;
}

const ConnectionUSBInfo::Baudrate *ConnectionUSBInfo::GetBaudrate() const
{
    return m_baudrate;
}

const std::string &ConnectionUSBInfo::GetAddr()
{
    return m_usb_addr.GetAddr();
}

}

ESYS_API std::ostream& operator<< (std::ostream& stream, const esys::ConnectionUSBInfo::Baudrate& b)
{
    stream << std::string(b.GetName()) << std::endl;
    return stream;
}
