/*!
 * \file esys/i2cbusbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/i2cbusbase.h"

namespace esys
{

I2CBusBase::I2CBusBase(): BusBase_t<I2CBusBase>()
{
}

I2CBusBase::~I2CBusBase()
{
}

}


