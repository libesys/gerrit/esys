/*!
 * \file esys/semaphorebase.cpp
 * \brief The Semaphore base class
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/semaphorebase.h"

namespace esys
{

SemaphoreBase::SemaphoreBase(const ObjectName &name) : ObjectLeaf(name)
{
}

SemaphoreBase::~SemaphoreBase()
{
}

}


