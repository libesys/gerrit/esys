/*!
 * \file esys/timerbase.cpp
 * \brief The base class of all Timer implementations
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/timerbase.h"
#include "esys/timercallbackbase.h"

#include  "esys/assert.h"

namespace esys
{

TimerBase::TimerBase(const ObjectName &name)
    : ObjectLeaf(name), m_callback(nullptr), m_next(nullptr), m_mngr(nullptr)
{
}

TimerBase::~TimerBase()
{
}


void TimerBase::SetCallback(TimerCallbackBase *callback)
{
    m_callback = callback;
}

TimerCallbackBase *TimerBase::GetCallback()
{
    return m_callback;
}

void TimerBase::Notify()
{
    if (m_callback != nullptr)
    {
        m_callback->SetTimer(this);
        m_callback->Callback();
    }
}

void TimerBase::SetNext(TimerBase *next)
{
    m_next = next;
}

TimerBase *TimerBase::GetNext()
{
    return m_next;
}

void TimerBase::SetMngr(TimerMngrBase *mngr)
{
    m_mngr = mngr;
}

TimerMngrBase &TimerBase::GetMngr()
{
    assert(m_mngr != nullptr);

    return *m_mngr;
}

}




