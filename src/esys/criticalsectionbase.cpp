/*!
 * \file esys/criticalsectionbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/criticalsectionbase.h"

namespace esys
{

CriticalSectionBase::CriticalSectionBase()
{
}

CriticalSectionBase::~CriticalSectionBase()
{
}

}

