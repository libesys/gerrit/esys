/*!
 * \file esys/usbtreeitembase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/usbtreeitembase.h"

namespace esys
{

USBTreeItemBase::USBTreeItemBase(): m_parent(nullptr)
{
}

USBTreeItemBase::~USBTreeItemBase()
{
    uint32_t idx;
    USBTreeItemBase *child;

    for (idx = 0; idx < NbrChildren(); ++idx)
    {
        child = GetChild(idx);
        if (child != nullptr)
            delete child;
    }
    m_children.clear();
}

const std::string &USBTreeItemBase::GetName()
{
    return m_name;
}

void USBTreeItemBase::SetName(const std::string &name)
{
    m_name = name;
}

void USBTreeItemBase::SetParent(USBTreeItemBase *parent)
{
    m_parent = parent;
}

USBTreeItemBase *USBTreeItemBase::GetParent()
{
    return m_parent;
}

int32_t USBTreeItemBase::NbrChildren()
{
    return m_children.size();
}

USBTreeItemBase *USBTreeItemBase::GetChild(int32_t idx)
{
    if (idx >= m_children.size())
        return nullptr;
    return m_children[idx];
}

void USBTreeItemBase::AddChild(USBTreeItemBase *child)
{
    child->SetParent(this);
    m_children.push_back(child);
}

void USBTreeItemBase::SetHubPort(int32_t hub_port)
{
    m_hub_port = hub_port;
}

int32_t USBTreeItemBase::GetHubPort()
{
    return m_hub_port;
}

void USBTreeItemBase::SetUSBDevInfo(USBDevInfo *usb_dev_info)
{
    m_usb_dev_info = usb_dev_info;
}

USBDevInfo *USBTreeItemBase::GetUSBDevInfo()
{
    return m_usb_dev_info;
}

}



