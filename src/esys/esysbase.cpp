/*!
 * \file esys/esysbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esysbase.h"

namespace esys
{

void ESysBase::SetSystemBase(SystemBase *system_base)
{
    m_system_base = system_base;
}

SystemBase *ESysBase::GetSystemBase()
{
    return m_system_base;
}

void ESysBase::SetSystemTask(SystemTask *system_task)
{
    m_system_task = system_task;
}

SystemTask *ESysBase::GetSystemTask()
{
    return m_system_task;
}

void ESysBase::SetTaskMngrBase(TaskMngrBase *taskmngr_base)
{
    m_taskmngr_base = taskmngr_base;
}

TaskMngrBase *ESysBase::GetTaskMngrBase()
{
    return m_taskmngr_base;
}


ESysBase::ESysBase()
    : m_system_base(nullptr), m_system_task(nullptr), m_taskmngr_base(nullptr)
    , m_list_modules()
{
}

ESysBase::~ESysBase()
{
}

}





