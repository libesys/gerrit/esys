/*!
 * \file esys/hwuid.cpp
 * \brief Source file of the UID class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/hwuid.h"
#include "esys/assert.h"

namespace esys
{

HWUID::HWUID()
{
}

HWUID::~HWUID()
{
}

HWUID::Type HWUID::GetType()
{
    return GetRawData()[0];
}

void HWUID::SetType(Type type)
{
    GetRawData()[0] = type;
}


void HWUID::SetData(esys::uint8_t *data, esys::uint8_t size)
{
    assert(size == GetSize());

    memcpy(GetRawData() + 2, data, size);
}

}
