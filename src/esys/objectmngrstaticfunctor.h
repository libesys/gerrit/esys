/*!
 * \file esys/objectmngrstaticfunctor.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_OBJECTMNGRSTATICFUNCTOR_H__
#define __ESYS_OBJECTMNGRSTATICFUNCTOR_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/objectmngr.h"

namespace esys
{

class StaticFunctor : public ObjectMngr::FunctorType
{
public:
    typedef int32_t(Object::*Function)(Object::Order order);

    StaticFunctor(Function fct) : m_fct(fct)
    {
    }

    virtual int32_t CallFct(Object *obj, Object::Order order) override
    {
        assert(obj != nullptr);
        assert(m_fct != nullptr);

        return (obj->*m_fct)(order);
    }
protected:
    Function m_fct;
};

}

#endif
