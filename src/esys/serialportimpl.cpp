/*!
 * \file esys/serialportimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/serialportimpl.h"
#include "esys/usbdevserialportinfo.h"

#include <cassert>

namespace esys
{

//SerialPortImpl::SerialPortImpl(USBDevSerialPortInfo *com_info)
SerialPortImpl::SerialPortImpl(USBDevInfo *com_info)
    : m_com_info(com_info), m_is_open(false)
{
    assert(com_info!=NULL);

    com_info->SetSerialPortImpl(this);
}

SerialPortImpl::~SerialPortImpl()
{
}

void SerialPortImpl::Removed()
{
    m_is_open=false;
    m_com_info=NULL;
}

}

