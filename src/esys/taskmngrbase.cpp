/*!
 * \file esys/taskmngrbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/taskmngrbase.h"
#include "esys/tasklist.h"
#include "esys/mutexlocker.h"
#include "esys/esys.h"
#include "esys/systemtask.h"

namespace esys
{

TaskMngrBase::TaskMngrBase(const ObjectName &name)
    : ObjectMngr(name), m_task_list(nullptr)
    , m_functor_create_all(&TaskBase::Create, true)
    , m_functor_start_all(&TaskBase::Start)
    , m_functor_stop_all(&TaskBase::StopTask)
    , m_mutex("Mutex")
{
}

TaskMngrBase::~TaskMngrBase()
{
}

int32_t TaskMngrBase::Run()
{
    int32_t result;

    result = CreateAll();
    if (result < 0)
        return result;
    result = StartAll();
    if (result < 0)
        return result;
    /* At this point, there are 2 main cases:
        - there is already a Task scheduler running, i.e. in VHW case
        - there are no scheduler running i.e. in HW case
       In the HW case, this is executed in the context of the main function. And since the main is not
       a scheduled Task, StartScheduler will returns only when the scheduler exits back to the
       main context.
    */
    result = StartScheduler();
    if (result < 0)
        return result;
    // Makes sure all Task, except the System Task are stopped
    /*result = StopAll(Tasks::ALL_NOT_SYSTEM);
    if (result < 0)
        return result;
    // When the only remaining task running is the System Task, it is finally shutdown
    result = StopAll(Tasks::ONLY_SYSTEM);
    if (result < 0)
        return result; */
    return result;
}

void TaskMngrBase::TaskDone(TaskBase *task)
{
    MutexLocker lock(m_mutex);

    GetTaskList().DecActiveCount(task->GetType());
    //task->SetState(TaskBase::State::STOPPING);
    Done(task);

    if ((GetNbrActiveTasks(TaskType::APPLICATION) == 0) && (task->GetType() == TaskType::APPLICATION))
        //ESys::Get().GetSystemTask()->AllAppTasksDone();
        AllAppTasksDone();
}

void TaskMngrBase::TaskStarted(TaskBase *task)
{
    MutexLocker lock(m_mutex);

    GetTaskList().IncActiveCount(task->GetType());
}

uint16_t TaskMngrBase::GetNbrTasks(TaskType type)
{
    return GetTaskList().GetNbrTask(type);
}

uint16_t TaskMngrBase::GetNbrActiveTasks(TaskType type)
{
    return GetTaskList().GetActiveCount(type);
}

TaskBase *TaskMngrBase::GetTask(uint16_t idx, TaskType type)
{
    assert(m_task_list != nullptr);

    //return m_task_list->
    return nullptr;
}

TaskBase *TaskMngrBase::GetFirstTask(TaskType type)
{
    return GetTaskList().GetFirst(type);
}

void TaskMngrBase::Started(TaskBase *task)
{
}

int32_t TaskMngrBase::Stopping(TaskBase *task)
{
    return -1;
}

void TaskMngrBase::AllAppTasksDone()
{
    SystemTask::GetCurrent().AllAppTasksDone();
}

int32_t TaskMngrBase::Init()
{
    int32_t result;

    result = PlatInit();
    assert(result == 0);

    result = StaticInit();
    assert(result == 0);

    //m_task_list = &TaskList::GetCurrent();
    GetTaskList();
    assert(m_task_list != nullptr);
    return 0;
}

int32_t TaskMngrBase::Release()
{
    return StaticRelease();
}

/*int32_t TaskMngrBase::StartScheduler()
{
    return 0;
}n*/

int32_t TaskMngrBase::CreateAll()
{
    return DoOnAll(m_functor_create_all);
}

int32_t TaskMngrBase::StartAll()
{
    return DoOnAll(m_functor_start_all);
}

int32_t TaskMngrBase::StopAll(Tasks tasks)
{
    return DoOnAll(m_functor_stop_all, tasks);
}

int32_t TaskMngrBase::DoOnAll(FunctorBase &task_fctor, TaskType type, Tasks tasks)
{
    TaskBase *task;
    int32_t result = 0;
    int32_t l_result;
    bool call_it;

    task = GetFirstTask(type);
    while (task != nullptr)
    {
        call_it = false;

        if (tasks == Tasks::ALL)
            call_it = true;
        else
        {
            switch (tasks)
            {
                case Tasks::ALL_NOT_SYSTEM:
                    if (task->GetRuntimeLevel() != RuntimeLevel::ROOT)
                        call_it = true;
                    break;
                case Tasks::ONLY_SYSTEM:
                    if (task->GetRuntimeLevel() == RuntimeLevel::ROOT)
                        call_it = true;
                    break;
                default:
                    call_it = true;
            }
        }
        if (call_it == true)
        {
            if (task_fctor.GetDoSetTaskMngr() == true)
                task->SetMngrBase(this);
            task_fctor.SetTask(task);
            l_result = task_fctor.CallFct(this, tasks);
            if ((result == 0) && (l_result < 0))
                result = l_result;
        }

        task = task->GetNextTask();
    }
    return result;
}

int32_t TaskMngrBase::DoOnAll(FunctorBase &task_fctor, Tasks tasks)
{
    int32_t result = 0;
    int32_t l_result;

    result = DoOnAll(task_fctor, TaskType::INTERNAL, tasks);
    l_result = DoOnAll(task_fctor, TaskType::APPLICATION, tasks);
    if ((result == 0) && (l_result < 0))
        result = l_result;
    return result;
}

TaskList &TaskMngrBase::GetTaskList()
{
    if (m_task_list != nullptr)
        return *m_task_list;
    m_task_list = &TaskList::GetCurrent();
    return *m_task_list;
}

void TaskMngrBase::SetSystemTask(SystemTask *system_task)
{
    m_system_task = system_task;
}

SystemTask *TaskMngrBase::GetSystemTask()
{
    return m_system_task;
}

}



