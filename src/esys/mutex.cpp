/*!
 * \file esys/mutex.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mutex.h"
#include "esys/esys.h"

#ifdef ESYS_MULTI_PLAT

namespace esys
{

Mutex::Mutex(Type type): esys::MutexBase(type), m_mutex(NULL)
{
    m_mutex=ESys::Get().NewMutexBase(type); //ESysBase::NewMutexBase(type);
}

Mutex::~Mutex()
{
}

int16_t Mutex::Lock()
{
    if (m_mutex==NULL)
        return -1;
    return m_mutex->Lock();
}

int16_t Mutex::UnLock()
{
    if (m_mutex==NULL)
        return -1;
    return m_mutex->UnLock();
}

int16_t Mutex::TryLock()
{
    if (m_mutex==NULL)
        return -1;
    return m_mutex->TryLock();
}

}

#endif
