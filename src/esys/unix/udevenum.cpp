/*!
 * \file esys/unix/udevenum.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/unix/udevenum.h"
#include "esys/usbdevinfo.h"

#include <boost/locale.hpp>

#include <stdlib.h>

#include <iostream>
#include <sstream>
#include <iomanip>

#include <libudev.h>

namespace esys
{

UDevEnum UDevEnum::s_udev_enum;

UDevEnum &UDevEnum::Get()
{
    return s_udev_enum;
}

UDevEnum::UDevEnum():m_verbose(false)
{
}

UDevEnum::~UDevEnum()
{
}

void UDevEnum::SetVerbose(bool verbose)
{
    m_verbose=verbose;
}

int UDevEnum::FindCOMPort()
{
    struct udev *udev;
    struct udev_enumerate *enumerate;
    struct udev_list_entry *devices, *dev_list_entry;
    struct udev_device *orig_dev;
    struct udev_device *dev;
    int32_t result;

    m_com_port_vec.clear();

    /* Create the udev object */
    udev = udev_new();
    if (!udev)
    {
        return -1;
    }

    result = -2;
    /* Create a list of the devices in the 'hidraw' subsystem. */
    enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(enumerate, "tty");
    udev_enumerate_scan_devices(enumerate);
    devices = udev_enumerate_get_list_entry(enumerate);
    /* For each item enumerated, print out its information.
    udev_list_entry_foreach is a macro which expands to
    a loop. The loop will be executed for each member in
    devices, setting dev_list_entry to a list entry
    which contains the device's path in /sys. */
    udev_list_entry_foreach(dev_list_entry, devices)
    {
        const char *path;

        /* Get the filename of the /sys entry for the device
        and create a udev_device object (dev) representing it */
        path = udev_list_entry_get_name(dev_list_entry);
        orig_dev = udev_device_new_from_syspath(udev, path);

        /* usb_device_get_devnode() returns the path to the device node
        itself in /dev. */
        if (m_verbose)
            std::cout << "Device Node Path: " << udev_device_get_devnode(orig_dev) << std::endl;

        /* The device pointed to by dev contains information about
        the hidraw device. In order to get information about the
        USB device, get the parent device with the
        subsystem/devtype pair of "usb"/"usb_device". This will
        be several levels up the tree, but the function will find
        it.*/
        dev = udev_device_get_parent_with_subsystem_devtype(
                  orig_dev,
                  "usb",
                  "usb_device");
        if (!dev)
        {
            if (m_verbose)
                std::cout << "Unable to find parent usb device." << std::endl;
            continue;
        }

        m_com_port_vec.push_back(boost::locale::conv::utf_to_utf<wchar_t>(udev_device_get_devnode(orig_dev)));

        /* From here, we can call get_sysattr_value() for each file
        in the device's /sys entry. The strings passed into these
        functions (idProduct, idVendor, serial, etc.) correspond
        directly to the files in the directory which represents
        the USB device. Note that USB strings are Unicode, UCS2
        encoded, but the strings returned from
        udev_device_get_sysattr_value() are UTF-8 encoded. */
        if (m_verbose)
        {
            std::cout << "  VID/PID: ";
            std::cout << std::hex << std::setw(4) << std::setfill('0') << udev_device_get_sysattr_value(dev, "idVendor") << ":";
            std::cout << std::hex << std::setw(4) << std::setfill('0') << udev_device_get_sysattr_value(dev, "idProduct") << std::endl;
            std::cout << "  " << udev_device_get_sysattr_value(dev, "manufacturer") << std::endl;
            std::cout << "  " << udev_device_get_sysattr_value(dev, "manufacturer") << std::endl;
            std::cout << "  " << udev_device_get_sysattr_value(dev, "product") << std::endl;
            std::cout << "  serial: " << udev_device_get_sysattr_value(dev, "serial") << std::endl;
            std::cout << "  busnum: " << udev_device_get_sysattr_value(dev, "busnum") << std::endl;
            std::cout << "  devpath: " << udev_device_get_sysattr_value(dev, "devpath") << std::endl;
        }

        uint8_t busnum;
        uint16_t idVendor;
        uint16_t idProduct;
        std::wstring devpath;
        std::istringstream iss;
        std::wostringstream woss;

        busnum = atoi(udev_device_get_sysattr_value(dev, "busnum"));
        iss.str(udev_device_get_sysattr_value(dev, "idVendor"));
        iss >> std::hex >> idVendor;

        iss.seekg(0);
        iss.str(udev_device_get_sysattr_value(dev, "idProduct"));
        iss >> std::hex >> idProduct;

        woss << udev_device_get_sysattr_value(dev, "busnum") << L"-" << udev_device_get_sysattr_value(dev, "devpath");
        devpath = woss.str();

        if (m_verbose)
            std::cout << "idProduct = " << std::dec << idProduct << " " << std::hex << idProduct << std::endl;
        /*bool found = (dev_info->GetBusNumber() == busnum) && (dev_info->GetIdProduct() == idProduct) &&
            (dev_info->GetIdVendor() == idVendor) && (dev_info->GetLocationPath() == devpath); */

        if (m_verbose)
        {
            /*std::cout << "dev_info->GetBusNumber() = " << (int)dev_info->GetBusNumber() << std::endl;

            if (dev_info->GetBusNumber() == busnum)
                std::cout << "BusNum OK" << std::endl;
            if (dev_info->GetIdProduct() == idProduct)
                std::cout << "idProduct OK" << std::endl;
            if (dev_info->GetIdVendor() == idVendor)
                std::cout << "idVendor OK" << std::endl;

            std::wcout << "Location = " << dev_info->GetLocationPath() << std::endl; */
            std::wcout << "devpath  = " << devpath << std::endl;
        }

        udev_device_unref(dev);
    }
    /* Free the enumerator object */
    udev_enumerate_unref(enumerate);

    udev_unref(udev);
    return m_com_port_vec.size();
}

unsigned int UDevEnum::GetCOMPortCount()
{
    return m_com_port_vec.size();
}

std::wstring UDevEnum::GetCOMPort(unsigned int idx)
{
    if ((idx < 0) || (idx >= m_com_port_vec.size()))
        return L"";
    return m_com_port_vec[idx];
}

int32_t UDevEnum::FindDevName(USBDevInfo *dev_info, const std::wstring &port_name)
{
    struct udev *udev;
    struct udev_enumerate *enumerate;
    struct udev_list_entry *devices, *dev_list_entry;
    struct udev_device *orig_dev;
    struct udev_device *dev;
    int32_t result;

    /* Create the udev object */
    udev = udev_new();
    if (!udev)
    {
        return -1;
    }

    result=-2;
    /* Create a list of the devices in the 'hidraw' subsystem. */
    enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(enumerate, "tty");
    udev_enumerate_scan_devices(enumerate);
    devices = udev_enumerate_get_list_entry(enumerate);
    /* For each item enumerated, print out its information.
       udev_list_entry_foreach is a macro which expands to
       a loop. The loop will be executed for each member in
       devices, setting dev_list_entry to a list entry
       which contains the device's path in /sys. */
    udev_list_entry_foreach(dev_list_entry, devices)
    {
        const char *path;

        /* Get the filename of the /sys entry for the device
           and create a udev_device object (dev) representing it */
        path = udev_list_entry_get_name(dev_list_entry);
        orig_dev = udev_device_new_from_syspath(udev, path);

        /* usb_device_get_devnode() returns the path to the device node
           itself in /dev. */
        if (m_verbose)
            std::cout << "Device Node Path: " << udev_device_get_devnode(orig_dev) << std::endl;

        /* The device pointed to by dev contains information about
           the hidraw device. In order to get information about the
           USB device, get the parent device with the
           subsystem/devtype pair of "usb"/"usb_device". This will
           be several levels up the tree, but the function will find
           it.*/
        dev = udev_device_get_parent_with_subsystem_devtype(
                  orig_dev,
                  "usb",
                  "usb_device");
        if (!dev)
        {
            if (m_verbose)
                std::cout << "Unable to find parent usb device." << std::endl;
            continue;
        }

        /* From here, we can call get_sysattr_value() for each file
           in the device's /sys entry. The strings passed into these
           functions (idProduct, idVendor, serial, etc.) correspond
           directly to the files in the directory which represents
           the USB device. Note that USB strings are Unicode, UCS2
           encoded, but the strings returned from
           udev_device_get_sysattr_value() are UTF-8 encoded. */
        if (m_verbose)
        {
            std::cout << "  VID/PID: ";
            std::cout << std::hex << std::setw(4) << std::setfill('0') << udev_device_get_sysattr_value(dev,"idVendor") << ":";
            std::cout << std::hex << std::setw(4) << std::setfill('0') << udev_device_get_sysattr_value(dev, "idProduct") << std::endl;
            std::cout << "  " << udev_device_get_sysattr_value(dev,"manufacturer") << std::endl;
            std::cout << "  " << udev_device_get_sysattr_value(dev,"manufacturer") << std::endl;
            std::cout << "  " << udev_device_get_sysattr_value(dev,"product") << std::endl;
            std::cout << "  serial: " << udev_device_get_sysattr_value(dev, "serial") << std::endl;
            std::cout << "  busnum: " << udev_device_get_sysattr_value(dev, "busnum") << std::endl;
            std::cout << "  devpath: " << udev_device_get_sysattr_value(dev, "devpath") << std::endl;
        }

        uint8_t busnum;
        uint16_t idVendor;
        uint16_t idProduct;
        std::wstring devpath;
        std::istringstream iss;
        std::wostringstream woss;

        busnum=atoi(udev_device_get_sysattr_value(dev, "busnum"));
        iss.str(udev_device_get_sysattr_value(dev,"idVendor"));
        iss >> std::hex >> idVendor;


        //idVendor=
        iss.seekg(0);
        iss.str(udev_device_get_sysattr_value(dev, "idProduct"));
        iss >> std::hex >> idProduct;

        woss << udev_device_get_sysattr_value(dev, "busnum") << L"-" << udev_device_get_sysattr_value(dev, "devpath");
        devpath=woss.str();

        if (m_verbose)
            std::cout << "idProduct = " << std::dec << idProduct << " " << std::hex << idProduct << std::endl;
        bool found=(dev_info->GetBusNumber()==busnum)&&(dev_info->GetIdProduct()==idProduct)&&
                   (dev_info->GetIdVendor()==idVendor)&&(dev_info->GetLocationPath()==devpath);

        if (m_verbose)
        {
            std::cout << "dev_info->GetBusNumber() = " << (int) dev_info->GetBusNumber() << std::endl;

            if (dev_info->GetBusNumber()==busnum)
                std::cout << "BusNum OK" << std::endl;
            if (dev_info->GetIdProduct()==idProduct)
                std::cout << "idProduct OK" << std::endl;
            if (dev_info->GetIdVendor()==idVendor)
                std::cout << "idVendor OK" << std::endl;

            std::wcout << "Location = " << dev_info->GetLocationPath() << std::endl;
            std::wcout << "devpath  = " << devpath << std::endl;
        }

        if (found)
        {
            if (port_name.empty())
            {
                if (m_verbose)
                    std::cout << "FOUND" << std::endl;
                dev_info->SetPortName(udev_device_get_devnode(orig_dev));
                result=0;
                break;
            }
            else
            {
                if (boost::locale::conv::utf_to_utf<char>(port_name.c_str())==udev_device_get_devnode(orig_dev))
                {
                    result=0;
                    break;
                }
            }
        }
        else
        {
            if (m_verbose)
                std::cout << "NOT FOUND" << std::endl;
        }
        if (m_verbose)
            std::wcout << std::endl << dev_info->GetLocationPath() << std::endl;

        udev_device_unref(dev);
    }
    /* Free the enumerator object */
    udev_enumerate_unref(enumerate);

    udev_unref(udev);
    return result;
}

}
