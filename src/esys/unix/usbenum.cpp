/*!
 * \file esys/unix/usbenum.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/unix/usbenum.h"
#include "esys/usbdevinfo.h"

#include "esys/unix/udevenum.h"

#include <iostream>
#include <sstream>
#include <iomanip>

#include <libusb-1.0/libusb.h>

namespace esys
{

class USBEnumImpl
{
public:
    USBEnumImpl();
    ~USBEnumImpl();

    int Init();
    void Exit();
    libusb_context *GetContext();
protected:
    libusb_context *m_libusb_ctx;
};

USBEnumImpl::USBEnumImpl(): m_libusb_ctx(NULL)
{
}

USBEnumImpl::~USBEnumImpl()
{
    if (m_libusb_ctx!=NULL)
        libusb_exit(m_libusb_ctx);
}

int USBEnumImpl::Init()
{
    int r;

    if (m_libusb_ctx==NULL)
    {
        r = libusb_init(&m_libusb_ctx); //initialize a library session
        if(r < 0)
        {
            return r;
        }
        libusb_set_debug(m_libusb_ctx, 3);
        return 0;
    }
    return 0;
}

void USBEnumImpl::Exit()
{
    if (m_libusb_ctx==NULL)
        return;

    libusb_exit(m_libusb_ctx);
    m_libusb_ctx=NULL;
}

libusb_context *USBEnumImpl::GetContext()
{
    return m_libusb_ctx;
}

USBEnum USBEnum::s_usb_enum;

USBEnum &USBEnum::Get()
{
    return s_usb_enum;
}

USBEnum::USBEnum(): USBEnumBase(), m_impl(NULL), m_init(0), m_root_tree()
{
    m_impl=new USBEnumImpl();
    m_init=m_impl->Init();
}

USBEnum::~USBEnum()
{
    USBDevInfoBase *info;
    uint32_t i;

    for (i=0; i<m_dev_infos.size(); i++)
    {
        info=m_dev_infos.at(i);
        if (info!=NULL)
            delete info;
    }

    if (m_impl!=NULL)
    {
        m_impl->Exit();
        delete m_impl;
    }
}

USBTree &USBEnum::GetTree()
{
    return m_root_tree;
}

void USBEnum::SetDebug(bool debug)
{
    m_debug=debug;
}

void USBEnum::Released(USBDevInfoBase *obj)
{
    //TODO
}

void USBEnum::Remove(USBDevInfo *obj)
{
    std::vector<USBDevInfo *>::iterator it;
    std::vector<USBDevInfo *>::iterator s_it;
    bool done=false;

    for (it=m_dev_infos.begin(); (it!=m_dev_infos.end())&&(done==false); ++it)
    {
        if ((*it)->GetId()==obj->GetId())
        {
            m_dev_infos.erase(it);
            done=true;
        }
    }

    done=false;
    for (s_it=m_dev_serial_port_infos.begin(); (s_it!=m_dev_serial_port_infos.end())&&(done==false); ++it)
    {
        if ((*s_it)->GetId()==obj->GetId())
        {
            m_dev_serial_port_infos.erase(s_it);
            done=true;
        }
    }
}

int16_t USBEnum::Refresh()
{
    UDevEnum &udev_enum=UDevEnum::Get();
    std::map<std::wstring, USBDevInfo *> m_found_devs;
    //libusb_context *context;
    libusb_device **devs;
    libusb_device *dev;
    int i = 0, j = 0;
    int result;
    uint8_t path[8];

    if (m_init!=0)
    {
        return -1;
    }

    result = libusb_get_device_list(m_impl->GetContext(), &devs);
    if (result < 0)
    {
        //libusb_exit(NULL);
        return -2;
    }

    while ((dev = devs[i++]) != NULL)
    {
        struct libusb_device_descriptor desc;
        std::string usb_path;
        std::ostringstream s_usb_path;

        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0)
        {
            std::cerr << "failed to get device descriptor" << std::endl;
            libusb_free_device_list(devs, 1);
            //libusb_exit(NULL);
            return -3;
        }

        if (m_verbose)
        {
            std::cout << "0x" << std::hex << std::setw(4) << std::setfill('0') << desc.idVendor << ": 0x" << std::hex << std::setw(4) << std::setfill('0') << desc.idProduct;
            std::cout << std::dec << "(bus " << (int)libusb_get_bus_number(dev) << ", device " << (int)libusb_get_device_address(dev) << ")" << std::endl;
        }

        s_usb_path << libusb_get_bus_number(dev) << L"-";
        r = libusb_get_port_numbers(dev, path, sizeof(path));
        if (r > 0)
        {
            if (m_verbose)
                std::cout << "path: ";

            for (j = 0; j < r; j++)
            {
                if (j!=0)
                {
                    if (m_verbose)
                        std::cout << ".";
                    s_usb_path << L".";
                }
                if (m_verbose)
                    std::cout << (int)path[j];
                s_usb_path << (int)path[j];
            }
        }
        if (m_verbose)
        {
            std::cout << std::endl;
            std::wcout << s_usb_path.str() << std::endl;
        }

        struct libusb_device_descriptor dev_desc;

        r =libusb_get_device_descriptor(dev,&dev_desc);
        if (r==0)
        {
            if (m_verbose)
            {
                std::cout << "Dev. Descriptor" << std::endl;
                std::cout << "    bLength          = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bLength << std::endl;
                std::cout << "    bDescriptorType  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDescriptorType << std::endl;
                std::cout << "    bcdUSB           = " << std::setw(4) << std::setfill('0') << dev_desc.bcdUSB << std::endl;
                std::cout << "    bDeviceClass     = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDeviceClass << std::endl;
                std::cout << "    bDeviceSubClass  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDeviceSubClass << std::endl;
                std::cout << "    bDeviceProtocol  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDeviceProtocol << std::endl;
                std::cout << "    bMaxPacketSize0  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bMaxPacketSize0 << std::endl;
                std::cout << "    idVendor         = 0x" << std::hex << std::setw(4) << std::setfill('0') << (int)dev_desc.idVendor << std::endl;
                std::cout << "    idProduct        = 0x" << std::hex << std::setw(4) << std::setfill('0') << (int)dev_desc.idProduct << std::endl;
                std::cout << "    bcdDevice        = " << std::setw(4) << std::setfill('0') << dev_desc.bcdDevice << std::endl;
                std::cout << "    iManufacturer    = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.iManufacturer << std::endl;
                std::cout << "    iProduct         = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.iProduct << std::endl;
                std::cout << "    iSerialNumber    = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.iSerialNumber << std::endl;
                std::cout << "    bNumConfigurations = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bNumConfigurations << std::endl;
            }

            USBDevInfo *dev_info;
            USBDevInfo *serial_dev_info=NULL;

            if (dev_desc.bDeviceClass!=0x02)
            {
                dev_info=new USBDevInfo();
            }
            else
            {
                serial_dev_info=new USBDevInfo();
                dev_info=serial_dev_info;
                m_dev_serial_port_infos.push_back(serial_dev_info);


                //TBD find the linux device to access the serial port
            }
            m_dev_infos.push_back(dev_info);
            dev_info->SetClass(dev_desc.bDeviceClass);
            dev_info->SetSubClass(dev_desc.bDeviceSubClass);
            dev_info->SetIdVendor(dev_desc.idVendor);
            dev_info->SetIdProduct(dev_desc.idProduct);

            dev_info->GetUSBAddr().SetVID(dev_desc.idVendor);
            dev_info->GetUSBAddr().SetPID(dev_desc.idProduct);
            //dev_info->GetUSBAddr().SetREV(rev,is_rev_set);
            //dev_info->GetUSBAddr().SetMI(mi,is_mi_set);

            uint8_t busnum=libusb_get_bus_number(dev);
            dev_info->SetBusNumber(busnum);
            dev_info->SetLocationPath(s_usb_path.str());
            std::wostringstream ss;
            ss.clear();

            udev_enum.SetVerbose(m_verbose);
            if (serial_dev_info!=NULL)
                udev_enum.FindDevName(serial_dev_info);
        }
        if (m_verbose)
            std::cout << std::endl;
    }
    libusb_free_device_list(devs, 1);
    //libusb_exit(NULL);

    std::map<std::wstring, USBDevInfo *>::iterator it;
    std::map<std::wstring, USBDevInfo *>::iterator it_found;

    for (it=m_map_dev_infos.begin(); it!=m_map_dev_infos.end(); ++it)
    {
        it_found=m_found_devs.find(it->first);
        if (it_found==m_found_devs.end())
        {
            Remove(it->second);
            delete it->second;
            m_map_dev_infos.erase(it);
        }
    }
    return 0;
}

uint16_t USBEnum::GetNbrDev()
{
    return m_dev_infos.size();
}

USBDevInfo *USBEnum::GetDev(uint16_t idx)
{
    if (idx>=m_dev_infos.size())

        return NULL;
    return m_dev_infos.at(idx);
}

uint16_t USBEnum::GetNbrSerialPort()
{
    return m_dev_serial_port_infos.size();
}

USBDevInfo *USBEnum::GetSerialPort(uint16_t idx)
{
    if (idx>=m_dev_serial_port_infos.size())
        return NULL;
    return m_dev_serial_port_infos.at(idx);
}

libusb_device_handle *USBEnum::GetLibUSBDevHandle(USBDevInfo *dev_info)
{
    UDevEnum &udev_enum=UDevEnum::Get();
    std::map<std::wstring, USBDevInfo *> m_found_devs;
    //libusb_context *context;
    libusb_device **devs;
    libusb_device *dev;
    int i = 0, j = 0;
    int result;
    uint8_t path[8];
    libusb_device_handle *handle;

    if (m_init!=0)
    {
        return NULL;
    }

    result = libusb_get_device_list(m_impl->GetContext(), &devs);
    if (result < 0)
    {
        return NULL;
    }

    while ((dev = devs[i++]) != NULL)
    {
        struct libusb_device_descriptor desc;
        std::wstring usb_path;
        std::wostringstream s_usb_path;

        int r = libusb_get_device_descriptor(dev, &desc);
        if (r < 0)
            continue;

        if (m_verbose)
        {
            //std::cout << "0x" << std::hex << std::setw(4) << std::setfill('0') << desc.idVendor << ": 0x" << std::hex << std::setw(4) << std::setfill('0') << desc.idProduct;
            //std::cout << std::dec << "(bus " << (int)libusb_get_bus_number(dev) << ", device " << (int)libusb_get_device_address(dev) << ")" << std::endl;
        }

        s_usb_path << libusb_get_bus_number(dev) << L"-";
        r = libusb_get_port_numbers(dev, path, sizeof(path));
        if (r > 0)
        {
            /*if (m_verbose)
                std::cout << "path: "; */

            for (j = 0; j < r; j++)
            {
                if (j!=0)
                {
                    if (m_verbose)
                        std::cout << ".";
                    s_usb_path << L".";
                }
                /*if (m_verbose)
                    std::cout << (int)path[j]; */
                s_usb_path << (int)path[j];
            }
        }
        /*if (m_verbose)
        {
            std::cout << std::endl;
            std::wcout << s_usb_path.str() << std::endl;
        } */

        struct libusb_device_descriptor dev_desc;

        r =libusb_get_device_descriptor(dev,&dev_desc);
        if (r==0)
        {
            /*if (m_verbose)
            {
                std::cout << "Dev. Descriptor" << std::endl;
                std::cout << "    bLength          = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bLength << std::endl;
                std::cout << "    bDescriptorType  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDescriptorType << std::endl;
                std::cout << "    bcdUSB           = " << std::setw(4) << std::setfill('0') << dev_desc.bcdUSB << std::endl;
                std::cout << "    bDeviceClass     = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDeviceClass << std::endl;
                std::cout << "    bDeviceSubClass  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDeviceSubClass << std::endl;
                std::cout << "    bDeviceProtocol  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bDeviceProtocol << std::endl;
                std::cout << "    bMaxPacketSize0  = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bMaxPacketSize0 << std::endl;
                std::cout << "    idVendor         = 0x" << std::hex << std::setw(4) << std::setfill('0') << (int)dev_desc.idVendor << std::endl;
                std::cout << "    idProduct        = 0x" << std::hex << std::setw(4) << std::setfill('0') << (int)dev_desc.idProduct << std::endl;
                std::cout << "    bcdDevice        = " << std::setw(4) << std::setfill('0') << dev_desc.bcdDevice << std::endl;
                std::cout << "    iManufacturer    = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.iManufacturer << std::endl;
                std::cout << "    iProduct         = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.iProduct << std::endl;
                std::cout << "    iSerialNumber    = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.iSerialNumber << std::endl;
                std::cout << "    bNumConfigurations = 0x" << std::hex << std::setw(2) << std::setfill('0') << (int)dev_desc.bNumConfigurations << std::endl;
            } */

            /*USBDevInfo *dev_info;
            USBDevInfo *serial_dev_info=NULL;

            if (dev_desc.bDeviceClass!=0x02)
            {
                dev_info=new USBDevInfo();
            }
            else
            {
                serial_dev_info=new USBDevInfo();
                dev_info=serial_dev_info;
                m_dev_serial_port_infos.push_back(serial_dev_info);


                //TBD find the linux device to access the serial port
            }
            m_dev_infos.push_back(dev_info);
            dev_info->SetClass(dev_desc.bDeviceClass);
            dev_info->SetSubClass(dev_desc.bDeviceSubClass);
            dev_info->SetIdVendor(dev_desc.idVendor);
            dev_info->SetIdProduct(dev_desc.idProduct);

            dev_info->GetUSBAddr().SetVID(dev_desc.idVendor);
            dev_info->GetUSBAddr().SetPID(dev_desc.idProduct);
            //dev_info->GetUSBAddr().SetREV(rev,is_rev_set);
            //dev_info->GetUSBAddr().SetMI(mi,is_mi_set); */

            uint8_t busnum=libusb_get_bus_number(dev);
            //dev_info->SetBusNumber(busnum);
            //dev_info->SetLocationPath(s_usb_path.str());
            if ( (dev_info->GetBusNumber()==busnum) &&
                    (dev_info->GetLocationPath()==s_usb_path.str()) )
            {
                if (udev_enum.FindDevName(dev_info, dev_info->GetPortName())==0)
                {
                    // Gets the device handle
                    int r=libusb_open(dev, &handle);
                    if (r==0)
                        return handle;
                }
            }
        }
    }
    libusb_free_device_list(devs, 1);
    //libusb_exit(NULL);
    return NULL;
}

}


// dmesg | grep ttyUSBx
// -> gets the bus path 2-1.3.1 of a given ttyUSB= bus 2, port 1, port 3, port 1

// cat /proc/tty/drivers
// give the list of active driver tty driver

// ls /sys/class/tty/ttyUSB0
