/*!
 * \file esys/unix/serialportenum.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/unix/serialportenum.h"
#include "esys/unix/udevenum.h"

namespace esys
{

SerialPortEnum SerialPortEnum::s_com_port_enum;

SerialPortEnum &SerialPortEnum::Get()
{
    return s_com_port_enum;
}

SerialPortEnum::SerialPortEnum()
{
}

SerialPortEnum::~SerialPortEnum()
{
}

int32_t SerialPortEnum::Refresh()
{
    esys::UDevEnum & udev_enum = esys::UDevEnum::Get();

    return udev_enum.FindCOMPort();
}

uint32_t SerialPortEnum::GetCount()
{
    esys::UDevEnum &udev_enum = esys::UDevEnum::Get();

    return udev_enum.GetCOMPortCount();
}

std::wstring SerialPortEnum::GetPortName(uint32_t idx)
{
    esys::UDevEnum &udev_enum = esys::UDevEnum::Get();

    return udev_enum.GetCOMPort(idx);
}

}


