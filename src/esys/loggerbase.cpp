/*!
 * \file esys/loggerbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/loggerbase.h"

//#include <Arduino.h>

namespace esys
{

LoggerBase *LoggerBase::m_dflt_logger=0;
LoggerBase &LoggerBase::m_default_ref=m_default;
LoggerBase LoggerBase::m_default=LoggerBase();

LoggerBase::LoggerBase()
{
    m_dflt_logger=this;
}

LoggerBase::~LoggerBase()
{

}

LoggerBase& LoggerBase::operator<< (uint8_t val)
{
    return *this;
}

LoggerBase& LoggerBase::operator<< (int8_t val)
{
    return *this;
}

LoggerBase& LoggerBase::operator<< (uint16_t val)
{
    return *this;
}

LoggerBase& LoggerBase::operator<< (int16_t val)
{
    return *this;
}

LoggerBase& LoggerBase::operator<< (int8_t *string)
{
    return *this;
}

LoggerBase& LoggerBase::operator<< (const char *string)
{
    return *this;
}

void LoggerBase::Enable(bool enable)
{
    m_enable=enable;
}

LoggerBase &LoggerBase::GetLogger(uint8_t idx)
{
    if ((idx!=0)||(m_dflt_logger==0))
    {
        //Serial.println("GetLogger default\n");
        return m_default_ref;
    }

    return *m_dflt_logger;
}

int16_t LoggerBase::Write(uint8_t *buf, int16_t size)
{
    return 0;
}

}
