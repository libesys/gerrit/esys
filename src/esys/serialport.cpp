/*!
 * \file esys/serialport.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/serialport.h"
#include "esys/serialportimpl.h"

#include <stdlib.h>

namespace esys
{

SerialPortBase::SerialPortBase()
{
}

SerialPortBase::~SerialPortBase()
{
}

SerialPort SerialPort::g_NULL(false);
SerialPort SerialPort::g_Port0;

SerialPort::SerialPort(bool active):SerialPortBase(), m_active(active), m_impl(NULL)
{
}

SerialPort::~SerialPort()
{
}

int16_t SerialPort::Open()
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;

    return m_impl->Open();
}

int16_t SerialPort::Close()
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;
    return m_impl->Close();
}

int16_t SerialPort::Write(uint8_t val)
{
    return -1;
}

int16_t SerialPort::Write(uint8_t *buf,uint16_t size)
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;
    return m_impl->Write(buf,size);
}

int16_t SerialPort::Write(const char *cmd,bool endln)
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;
    return m_impl->Write(cmd,endln);
}

int16_t SerialPort::Read(uint8_t *buf,uint16_t size)
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;
    return m_impl->Read(buf,size);
}

int16_t SerialPort::Read(uint8_t &val)
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;
    return m_impl->Read(val);
}

int16_t SerialPort::Available()
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;
    return m_impl->Available();
}

int16_t SerialPort::ClearRXBuffer()
{
    if ((m_active==false)||(m_impl==NULL))
        return -1;
    return m_impl->ClearRXBuffer();
}

}