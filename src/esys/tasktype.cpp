/*!
 * \file esys/tasktype.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/tasktype.h"

namespace esys
{

const int32_t TaskType::NOT_SET;
const int32_t TaskType::INTERNAL;
const int32_t TaskType::APPLICATION;

TaskType::TaskType() : m_value(NOT_SET)
{
}

TaskType::TaskType(const int32_t &value): m_value(value)
{
}

TaskType::operator int32_t()
{
    return m_value;
}

int32_t TaskType::GetValue() const
{
    return m_value;
}

}

