/*!
 * \file esys/usbenumfiltermngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/usbenumfiltermngr.h"

#include <cassert>

namespace esys
{

USBEnumFilterMngr::USBEnumFilterMngr()
{
}

USBEnumFilterMngr::~USBEnumFilterMngr()
{
}

void USBEnumFilterMngr::Filter(USBDevInfo *dev)
{
    std::size_t count, idx;
    USBEnumFilter *filter;

    count=GetNbrFilter();

    for (idx=0; idx<count; ++idx)
    {
        filter=GetFilter(idx);

        assert(filter!=NULL);

        filter->Filter(dev);
    }
}

std::size_t USBEnumFilterMngr::GetNbrFiltered()
{
    std::size_t count, idx, result=0;
    USBEnumFilter *filter;

    count=GetNbrFilter();

    for (idx=0; idx<count; ++idx)
    {
        filter=GetFilter(idx);

        assert(filter!=NULL);

        result+=filter->GetNbrFiltered();
    }
    return result;
}

USBDevInfo *USBEnumFilterMngr::GetFiltered(std::size_t idx)
{
    std::size_t count, filter_count, filter_idx, cur_idx=0, result=0;
    USBEnumFilter *filter;

    count=GetNbrFilter();

    for (filter_idx=0; filter_idx<count; ++filter_idx)
    {
        filter=GetFilter(filter_idx);

        assert(filter!=NULL);
        filter_count=filter->GetNbrFiltered();
        if ( (idx>=cur_idx) && (idx<(cur_idx+filter_count)) )
        {
            return filter->GetFiltered(idx-cur_idx);
        }
        cur_idx+=filter_count;
    }
    return NULL;
}

void USBEnumFilterMngr::ClearFiltered()
{
    std::size_t count, idx;
    USBEnumFilter *filter;

    count=GetNbrFilter();

    for (idx=0; idx<count; ++idx)
    {
        filter=GetFilter(idx);

        assert(filter!=NULL);

        filter->ClearFiltered();
    }
}

}
