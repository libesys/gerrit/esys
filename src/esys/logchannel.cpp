/*!
 * \file esys/logchannel.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/logchannel.h"


namespace esys
{


LogChannel::LogChannel() : m_active(false), m_enable(false)
{
}

LogChannel::~LogChannel()
{
}

void LogChannel::SetActive(bool active)
{
    m_active = active;
}

bool LogChannel::GetActive()
{
    return m_active;
}

void LogChannel::SetEnable(bool enable)
{
    m_enable = enable;
}

bool LogChannel::GetEnable()
{
    return m_enable;
}

}


