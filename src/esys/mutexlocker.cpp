/*!
 * \file esys/mutexlocker.cpp
 * \brief
 *
 * \cond
 *__legal_b__
 *
 * Copyright (c) 2015-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 *__legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mutexlocker.h"

namespace esys
{

MutexLocker::MutexLocker(Mutex& mutex, bool from_isr): m_is_ok(false), m_mutex(mutex), m_from_isr(from_isr)
{
    m_is_ok = (m_mutex.Lock(m_from_isr) == 0);
}

// unlock the mutex in dtor
MutexLocker::~MutexLocker()
{
    if (IsOk())
        m_mutex.UnLock(m_from_isr);
}

bool MutexLocker::IsOk() const
{
    return m_is_ok;
}

}


