/*!
 * \file esys/serialportbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/serialportbase.h"

#include <boost/locale.hpp>

namespace esys
{

SerialPortBase::SerialPortBase()
{
}

SerialPortBase::~SerialPortBase()
{
}

const std::wstring &SerialPortBase::GetName()
{
    return m_name;
}

const std::string SerialPortBase::GetName_s()
{
    return boost::locale::conv::utf_to_utf<char>(GetName());
}

void SerialPortBase::SetCallback(SerialPortCallbackBase *call_back)
{
    m_call_back = call_back;
}

SerialPortCallbackBase *SerialPortBase::GetCallback()
{
    return m_call_back;
}

}



