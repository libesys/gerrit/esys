/*!
 * \file esys/esys.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"

#include "esys/esys.h"
#include "esys/task.h"

#ifdef WIN32
#include <Windows.h> //for the sleep
#elif defined ARDUINO
#include <Arduino.h>
#endif

#ifdef ESYS_USE_WX
#include <wx/timer.h>
#endif

#ifdef ESYS_USE_BOOST
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>
#endif

#if defined WIN32 || defined MULTIOS

#include <boost/filesystem/path.hpp>

#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>

#include <fstream>
#endif

namespace esys
{

namespace multios
{

#if defined WIN32 || defined MULTIOS
class ESYS_API ESysImpl
{
public:
    ESysImpl();
    virtual ~ESysImpl();

    void SetName(const char *name);
    const char *GetName();

    int32_t CreateLog();

private:
    std::ofstream *m_ofs;
    logmod::logger *m_log;
    const char *m_name;
};

ESysImpl::ESysImpl()
    : m_ofs(nullptr)
    , m_log(nullptr)
    , m_name(nullptr)
{

#if defined _MSC_VER && defined _DEBUG
    //_CrtSetBreakAlloc(531);

#endif
}

ESysImpl::~ESysImpl()
{
    if (m_log != nullptr)
    {
        dbg_log::dbg_class::SetDefaultLogger(nullptr);
        m_log->SetOfstream(nullptr);
        delete m_log;
        m_log = nullptr;
    }
    if (m_ofs != nullptr)
    {
        delete m_ofs;
        m_ofs = nullptr;
    }
}

void ESysImpl::SetName(const char *name)
{
    m_name = name;
}

const char *ESysImpl::GetName()
{
    return m_name;
}

int32_t ESysImpl::CreateLog()
{
    std::string s;

    if (GetName() != nullptr)
        s.append(GetName());
    else
        s.append("esys");
    s.append(".log");
    m_ofs = new std::ofstream(s.c_str());
    m_log = new logmod::logger();

    m_log->SetOfstream(m_ofs);
    dbg_log::dbg_class::SetDefaultLogger(m_log);

    dbg_log::dbg_class::Enable();
    return 0;
}

#endif

#ifdef ESYS_USE_BOOST
::boost::posix_time::ptime g_boost_started_time = ::boost::posix_time::microsec_clock::local_time();
#endif

#if defined WIN32 || defined ESYS_MULTIOS

uint64_t g_Millis()
{
#ifdef ESYS_USE_WX
    return wxGetLocalTimeMillis().GetValue();
#elif ESYS_USE_BOOST
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();

    boost::posix_time::time_duration diff = now - g_boost_started_time;
    // std::cout << "g_Millis = " << diff.total_milliseconds() << " ms" << std::endl;
    return diff.total_milliseconds();
#else
    error("Must define either ESYS_WX or ESYS_BOOST")
#endif
}

uint64_t ESys::m_started_time = g_Millis();
ESys ESys::g_esys;

static ESysImpl g_ESysImpl;

ESys::ESys()
    : m_impl(g_ESysImpl)
#else
ESys::ESys()
#endif
{
}

ESys::~ESys()
{
}

#if defined WIN32 || defined MULTIOS

ESys &ESys::Get()
{
    return g_esys;
}

void ESys::SetAppName(const char *app_name)
{
    m_impl.SetName(app_name);
}

int32_t ESys::CreateLog()
{
    return m_impl.CreateLog();
}

int32_t ESys::GetExeDir(std::string &exe_dir)
{
#ifdef WIN32
    char ownPth[MAX_PATH];

    // Will contain exe path
    HMODULE hModule = GetModuleHandle(nullptr);
    if (hModule != nullptr)
    {
        // When passing nullptr to GetModuleHandle, it returns handle of exe itself
        GetModuleFileName(hModule, ownPth, (sizeof(ownPth)));

        // Use above module handle to get the path using GetModuleFileName()
        // std::wcout << ownPth << std::endl ;
        ::boost::filesystem::path exe_path = std::string(ownPth);
        // std::wcout << exe_path.branch_path().wstring() << std::endl;
        exe_dir = exe_path.branch_path().string();
        return 0;
    }
    else
    {
        // std::wcout << L"Module handle is nullptr" << std::endl ;
        exe_dir = "";
        return -1;
    }
#else
#endif
}

#endif

void ESys::Sleep(uint16_t msec)
{
#if defined WIN32 || defined ESYS_MULTIOS
    TaskBase *task;

    task = TaskBase::Current();
    if (task != nullptr)
    {
        task->Sleep(msec);
        return;
    }
#ifdef ESYS_USE_WX
    wxMilliSleep(msec);
#elif defined ESYS_USE_BOOST
    boost::this_thread::sleep(boost::posix_time::millisec(msec));
#endif

#elif defined ARDUINO
    ::delay(msec);
#elif defined ESYS_USE_BOOST && (defined __GUNC__ || defined __GNUG__)
    boost::this_thread::sleep(boost::posix_time::millisec(msec));
#endif
}

millis_t ESys::Millis()
{
#if defined WIN32 || defined ESYS_MULTIOS
    TaskBase *task;

    task = TaskBase::Current();
    if (task != nullptr) return task->Millis();
    return g_Millis() - m_started_time;
#elif defined ESYS_USE_BOOST
    return g_Millis();

#elif defined ARDUINO
    return ::millis();
#endif
}

} // namespace multios

} // namespace esys
