/*!
 * \file esys/multios/serialportimplboost.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/multios/serialportimplboost.h"
#include "esys/usbdevserialportinfo.h"

#ifdef ESYS_BOOST

namespace esys
{

SerialPortImplBoost::SerialPortImplBoost(USBDevSerialPortInfo *com_info)
    : SerialPortImpl(com_info)
{
}

SerialPortImplBoost::~SerialPortImplBoost()
{
}

int16_t SerialPortImplBoost::Open()
{
    return -1;
}

int16_t SerialPortImplBoost::Close()
{
    return -1;
}

int16_t SerialPortImplBoost::Write(uint8_t val)
{
    return -1;
}

int16_t SerialPortImplBoost::Write(uint8_t *buf,uint16_t size)
{
    return -1;
}

int16_t SerialPortImplBoost::Write(const char *cmd,bool enldn)
{
    return -1;
}

int16_t SerialPortImplBoost::Read(uint8_t *buf,uint16_t size)
{
    return -1;
}

int16_t SerialPortImplBoost::Read(uint8_t &val)
{
    return -1;
}

int16_t SerialPortImplBoost::Available()
{
    return -1;
}

int16_t SerialPortImplBoost::ClearRXBuffer()
{
    return -1;
}

}

#endif

