/*!
 * \file esys/multios/taskwximpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/multios/taskwximpl.h"
#include "esys/multios/taskwx.h"

#include <wx/timer.h>

namespace esys
{

namespace wx
{

TaskImpl *TaskImpl::GetCurrent()
{
    TaskImpl *cur;
    wxThread *self;

    self=wxThread::This();
    if (self==NULL)
        return NULL;
    try
    {
        cur=dynamic_cast<TaskImpl*>(self);
    }
    catch (std::bad_cast &)
    {
        return NULL;
    }
    return cur;
}

TaskImpl::TaskImpl(wxThreadKind kind): wxThread(kind), m_task(NULL), m_sem(), m_started(false)
{
    m_started_time = wxGetLocalTimeMillis();
}

TaskImpl::~TaskImpl()
{
}

millis_t TaskImpl::Millis()
{
    wxLongLong diff = wxGetLocalTimeMillis()-m_started_time;

    return diff.GetValue();
}

void TaskImpl::Wait()
{
    m_sem.Wait();
}

void TaskImpl::Signal()
{
    m_sem.Post();
}

wxThread::ExitCode TaskImpl::Entry()
{
    assert(m_task!=NULL);

    m_task->Do();

    return (wxThread::ExitCode)m_task->GetExit();
}

void TaskImpl::Sleep(uint64_t val)
{
    Sleep(val);
}

void TaskImpl::SetTask(Task *task)
{
    m_task=task;
}

Task *TaskImpl::GetTask()
{
    return m_task;
}

void TaskImpl::DoLoopIterationEnds()
{
    if (!m_started)
        Sleep(10);
    else
        Yield();
}

}

}


