/*!
 * \file esys/multios/serialportimplmngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/multios/serialportimplmngr.h"
#include "esys/serialportimpl.h"
#include "esys/usbenum.h"
#include "esys/usbdevserialportinfo.h"

#include "esys/multios/serialportimplwxctb.h"
#include "esys/multios/serialportimplboost.h"

#include <cassert>

namespace esys
{

SerialPortImplMngr SerialPortImplMngr::s_mngr;

SerialPortImplMngr &SerialPortImplMngr::Get()
{
    return s_mngr;
}

SerialPortImplMngr::SerialPortImplMngr()
{
}

SerialPortImplMngr::~SerialPortImplMngr()
{
    uint32_t i;

    for (i=0; i<m_impls.size(); ++i)
        delete m_impls[i];
    for (i=0; i<m_ports.size(); ++i)
        delete m_ports[i];
}

//SerialPort::Handle SerialPortImplMngr::Get(USBDevSerialPortInfo *com_info,int type)
SerialPort::Handle SerialPortImplMngr::Get(USBDevInfo *com_info,int type)
{
    SerialPortImpl *impl;
    SerialPort *port;

    switch (type)
    {
#ifdef ESYS_USE_WX
        case SerialPort::IMPL_WXCTB:
            impl=new SerialPortImplWxCTB(com_info);
            break;
#endif
#ifdef ESYS_BOOST
        case SerialPort::IMPL_BOOST_ASIO:
            impl=new SerialPortImplBoost(com_info);
            break;
#endif
        default:
            assert(false);
    }

    m_impls.push_back(impl);
    port=new SerialPort(true);
    m_ports.push_back(port);
    port->m_impl=impl;
    return *port;
}



}

