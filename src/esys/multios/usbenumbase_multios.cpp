/*!
 * \file esys/multios/usbenum.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/multios/usbenumbase.h"
//#include "esys/usbdevserialportinfo.h"

#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>

#include <iostream>
#include <sstream>
#include <cassert>

namespace esys
{

namespace multios
{

USBEnumBase::USBEnumBase()
    : esys::USBEnumBase()
{
}

USBEnumBase::~USBEnumBase()
{
    USBDevInfoBase *info;
    uint32_t i;

    for (i = 0; i < m_all_dev_infos.size(); i++)
    {
        info = m_all_dev_infos.at(i);
        if (info != nullptr) delete info;
    }
}

void USBEnumBase::Released(USBDevInfoBase *obj)
{
    std::vector<USBDevInfo *>::iterator it;

    assert(obj != nullptr);
    assert(obj->GetRefCount() == 0);

    if (obj->IsValid())
        // If the info is valid, it shouldn't be removed from the list
        // of valid infos.
        return;

    // Since the info is not valid anymore and its reference count is 0,
    // it can be removed and deleted
    it = m_dev_infos.begin();
    while (it != m_dev_infos.end())
    {
        if ((*it)->GetId() == obj->GetId())
        {
            m_dev_infos.erase(it);
            delete obj;
            return;
        }
    }
    // If a USBDevInfoBase is release, it must be in the list
    assert(false);
}

void USBEnumBase::Remove(USBDevInfo *obj, std::vector<USBDevInfo *> &list)
{
    std::vector<USBDevInfo *>::iterator it;
    bool done = false;

    it = list.begin();
    while (it != list.end())
    {
        if ((*it)->GetId() == obj->GetId())
        {
            list.erase(it);
            return;
        }
        else
            it++;
    }
}

void USBEnumBase::Remove(USBDevInfo *obj, std::map<std::string, USBDevInfo *> &map_dev)
{
    std::map<std::string, USBDevInfo *>::iterator it;

    assert(obj != nullptr);

    it = map_dev.find(obj->GetUID());
    if (it != map_dev.end())
    {
        map_dev.erase(it);
    }
}

void USBEnumBase::Remove(USBDevInfo *obj)
{
    Remove(obj, m_dev_infos);
    Remove(obj, m_all_dev_infos);
    Remove(obj, m_dev_serial_port_infos);
    Remove(obj, m_map_dev_infos);
    delete obj;
}

void USBEnumBase::RefreshFound(std::map<std::string, USBDevInfo *> &found)
{
    std::map<std::string, USBDevInfo *>::iterator it;
    std::map<std::string, USBDevInfo *>::iterator next;
    std::map<std::string, USBDevInfo *>::iterator it_found;

    // Go through all known USBDevInfos from the previous Refresh.
    it = m_map_dev_infos.begin();
    while (it != m_map_dev_infos.end())
    {
        next = it;
        ++next; // To make sure that it's safe to remove it from the m_map_dev_infos

        it_found = found.find(it->first);
        if (it_found == found.end())
        {
            // If the object found from the previous refresh is not found
            // anymore on the latest refresh, it's an invalid USBDevInfo
            assert(it->second != nullptr);
            it->second->SetValid(false);

            // Check if the invalid object is still in use or not
            if (it->second->GetRefCount() == 0)
                Remove(it->second); // No in use, removed totally
            else
                Remove(it->second, m_dev_infos); // In use, removed only from valid list
        }
        it = next;
    }
}

USBDevInfo *USBEnumBase::FindParent(USBDevInfo *child)
{
    // Location Paths will have a #USBMI(x)
    if (child->GetLocationPaths().size() == 0) return nullptr;
    std::string child_loc_path = child->GetLocationPaths()[0];
    std::size_t idx = child_loc_path.find("#USBMI");
    if (idx == std::string::npos) return nullptr;

    std::string parent_loc_path = child_loc_path.substr(0, idx);
    std::map<std::string, USBDevInfo *>::iterator it;

    it = m_map_loc_path_dev_infos.find(parent_loc_path);
    if (it == m_map_loc_path_dev_infos.end())
    {
        return nullptr;
    }
    return it->second;
}

void USBEnumBase::FindParentsOfPendingChildren()
{
    std::vector<USBDevInfo *>::iterator it;
    USBDevInfo *parent;

    for (it = m_child_dev_infos.begin(); it != m_child_dev_infos.end(); ++it)
    {
        parent = FindParent(*it);
        if (parent == nullptr)
        {
            delete *it;
            *it = nullptr;
        }
        else
        {
            parent->AddChild(*it);
        }
    }
    m_child_dev_infos.clear();
}

void USBEnumBase::Add(USBDevInfo *obj)
{
    assert(obj != nullptr);
    assert(obj->GetUID().empty() == false);

    obj->SetUSBEnum(this);

    if (obj->GetUSBAddr().IsMISet() == true)
    {
        // This is a child device of a parent USB device
        USBDevInfo *parent = FindParent(obj);
        if (parent != nullptr)
            parent->AddChild(obj);
        else
            // Store it so its parent can be searched later
            m_child_dev_infos.push_back(obj);
    }
    else
    {
        if (obj->GetLocationPaths().size() != 0)
        {

            m_map_loc_path_dev_infos[obj->GetLocationPaths()[0]] = obj;
        }

        m_dev_infos.push_back(obj);
        m_all_dev_infos.push_back(obj);
        m_map_dev_infos[obj->GetUID()] = obj;

        if (GetFilterMngr() != nullptr) GetFilterMngr()->Filter(obj);
    }
}

void USBEnumBase::AddSerial(USBDevInfo *obj)
{
    m_dev_serial_port_infos.push_back(obj);
    // m_all_dev_infos.push_back(obj);
    /*if (GetFilterMngr() != nullptr)
        GetFilterMngr()->Filter(obj); */
}

std::size_t USBEnumBase::GetNbrDev()
{
    return m_dev_infos.size();
}

USBDevInfo *USBEnumBase::GetDev(std::size_t idx)
{
    if (idx >= m_dev_infos.size()) return nullptr;
    return m_dev_infos.at(idx);
}

std::size_t USBEnumBase::GetNbrSerialPort()
{
    return m_dev_serial_port_infos.size();
}

USBDevInfo *USBEnumBase::GetSerialPort(std::size_t idx)
{
    if (idx >= m_dev_serial_port_infos.size()) return nullptr;
    return m_dev_serial_port_infos.at(idx);
}

USBDevInfo *USBEnumBase::Find(const std::string &location, std::map<std::string, USBDevInfo *>::iterator *it_)
{
    std::map<std::string, USBDevInfo *>::iterator it;

    it = m_map_dev_infos.find(location);
    if (it == m_map_dev_infos.end()) return nullptr;

    if (it_ != nullptr) *it_ = it;

    assert(it->second != nullptr);

    return it->second;
}

} // namespace multios

} // namespace esys
