/*!
 * \file esys/multios/serialportmultios.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/serialportimpl.h"
#include "esys/multios/serialportimplmngr.h"
#include "esys/usbenum.h"

namespace esys
{

uint8_t SerialPort::GetNbr()
{
    USBEnum &usb_enum=USBEnum::Get();

    return (uint8_t)usb_enum.GetNbrSerialPort();
}

SerialPort::Handle SerialPort::Get(uint8_t idx,int type)
{
    //USBDevSerialPortInfo *com_info;
    USBDevInfo *com_info;
    USBEnum &usb_enum=USBEnum::Get();
    int16_t nbr=usb_enum.GetNbrSerialPort();

    if (idx>=nbr)
        return g_NULL;
    com_info=usb_enum.GetSerialPort(idx);
    if (com_info==NULL)
        return g_NULL;

    return SerialPortImplMngr::Get().Get(com_info,type);
}

//SerialPort::Handle SerialPort::Get(USBDevSerialPortInfo *com_info,int type)
SerialPort::Handle SerialPort::Get(USBDevInfo *com_info,int type)
{
    return SerialPortImplMngr::Get().Get(com_info,type);
}

}
