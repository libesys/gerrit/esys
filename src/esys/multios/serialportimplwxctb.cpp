/*!
 * \file esys/multios/serialportimplwxctb.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/multios/SerialPortImplWxCTB.h"
#include "esys/usbdevserialportinfo.h"

#ifdef ESYS_USE_WX

#include <iostream>

namespace esys
{

//SerialPortImplWxCTB::SerialPortImplWxCTB(USBDevSerialPortInfo *com_info)
SerialPortImplWxCTB::SerialPortImplWxCTB(USBDevInfo *com_info)
    : SerialPortImpl(com_info), m_com(NULL)
{
    assert(com_info!=NULL);

    m_dft_dcs.baud=wxBAUD_115200;
    m_dft_dcs.parity=wxPARITY_NONE;
    m_dft_dcs.rtscts=false;
    m_dft_dcs.stopbits=1;
    m_dft_dcs.wordlen=8;
}

SerialPortImplWxCTB::~SerialPortImplWxCTB()
{
    if (m_com!=NULL)
        delete m_com;
}

int16_t SerialPortImplWxCTB::Open()
{
    int result;
    wxString com_name;

    if (m_com==NULL)
        m_com=new wxSerialPort();

    assert(m_com!=NULL);

    if (m_is_open)
        return -1;

    com_name=m_com_info->GetPortName();//wxString::From8BitData(m_com_info->GetPortName().c_str());

    result=m_com->Open(com_name /*wxT("COM20") */,&m_dft_dcs);

    if (result<0)
        return -1;
    m_is_open=true;
    return 0;
}

int16_t SerialPortImplWxCTB::Close()
{
    int result;

    assert(m_com!=NULL);

    if (m_is_open)
    {
        result=m_com->Close();
        if (result==0)
            return 0;
    }
    return -1;
}

int16_t SerialPortImplWxCTB::Write(uint8_t val)
{
    if (m_is_open)
        return m_com->Write((char *)&val,1);
    return -1;
}

int16_t SerialPortImplWxCTB::Write(uint8_t *buf,uint16_t size)
{
    if (m_is_open)
        return m_com->Write((char *)buf,size);
    return -1;
}

int16_t SerialPortImplWxCTB::Write(const char *cmd,bool endln)
{
    int16_t total;
    int16_t result;

    if (!m_is_open)
        return -1;

    result=m_com->Write((char *)cmd,strlen(cmd));
    if (result<0)
        return result;
    if (endln)
    {
        total=result;
        result=m_com->Write("\r",1);
        if (result<0)
            return result;
        total+=result;
        result=m_com->Write("\n",1);
        if (result<0)
            return result;
        total+=result;
        return total;
    }
    return result;
}

int16_t SerialPortImplWxCTB::Read(uint8_t *buf,uint16_t size)
{
    int size_in_buf;

    if (!m_is_open)
        return -1;

    size_in_buf=Available();

    if (size_in_buf==0)
        return 0;

    if (size>=size_in_buf)
        size=size_in_buf;

    memcpy(buf,m_rx_buf.c_str(),size);
    m_rx_buf=m_rx_buf.substr(size);
    return size;
}

int16_t SerialPortImplWxCTB::Read(uint8_t &val)
{
    int size_in_buf;

    if (!m_is_open)
        return -1;

    size_in_buf=Available();
    if (size_in_buf==0)
        return 0;
    val=m_rx_buf.c_str()[0];
    m_rx_buf=m_rx_buf.substr(1);
    return 1;
}

int16_t SerialPortImplWxCTB::Available()
{
    char buffer[1025];
    int size;

    if (!m_is_open)
        return -1;

    size=m_com->Read(buffer,1024);
    if (size<0)
        return m_rx_buf.size();
    buffer[size]=0;

    if (size>0)
    {
        m_rx_buf.append(buffer);
        std::cout << "COM buf =" << std::endl;
        std::cout << m_rx_buf << std::endl;
        std::cout << "COM END." << std::endl;
    }
    return m_rx_buf.size();
}

int16_t SerialPortImplWxCTB::ClearRXBuffer()
{
    if (!m_is_open)
        return -1;

    //m_rx_buf.clear();
    m_rx_buf="";
    return 0;
}

void SerialPortImplWxCTB::Removed()
{
    SerialPortImpl::Removed();
    if (m_com!=NULL)
    {
        delete m_com;
        m_com=NULL;
    }
}

}

#endif
