/*!
 * \file esys/multios/taskwxthread.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/multios/taskwxthread.h"
#include "esys/taskbase.h"

#include <wx/longlong.h>
#include <wx/thread.h>

#include <typeinfo>

namespace esys
{

namespace wx
{

class TaskThreadBaseImpl: public wxThread
{
public:
    TaskThreadBaseImpl(wxThreadKind kind = wxTHREAD_DETACHED);
    virtual ~TaskThreadBaseImpl();

    virtual ExitCode Entry();

    static TaskThreadBaseImpl *Current();

    void SetTask(TaskBase *task);
    TaskBase *GetTask();
    virtual void Sleep(uint64_t val);
protected:
    wxLongLong m_started_time;
    TaskBase *m_task;
};


TaskThreadBaseImpl::TaskThreadBaseImpl(wxThreadKind kind): wxThread(kind)
{
}

TaskThreadBaseImpl::~TaskThreadBaseImpl()
{
}

wxThread::ExitCode TaskThreadBaseImpl::Entry()
{
    m_task->Do();

    return (wxThread::ExitCode)m_task->GetExit();
}

TaskThreadBaseImpl *TaskThreadBaseImpl::Current()
{
    TaskThreadBaseImpl *cur;
    wxThread *self;

    self=wxThread::This();
    if (self==NULL)
        return NULL;
    try
    {
        cur=dynamic_cast<TaskThreadBaseImpl*>(self);
    }
    catch (std::bad_cast &)
    {
        return NULL;
    }
    return cur;
}

void TaskThreadBaseImpl::SetTask(TaskBase *task)
{
    m_task=task;
}

TaskBase *TaskThreadBaseImpl::GetTask()
{
    return m_task;
}

void TaskThreadBaseImpl::Sleep(uint64_t val)
{
}

TaskThreadBase::TaskThreadBase(bool detached): m_exit(false)
{
    wxThreadKind kind=wxTHREAD_DETACHED;

    if (detached==false)
        kind=wxTHREAD_JOINABLE;

    m_impl=new TaskThreadBaseImpl(kind);
}

TaskThreadBase::~TaskThreadBase()
{
    delete m_impl;
}

void TaskThreadBase::SetTask(TaskBase *task)
{
    m_impl->SetTask(task);
}

void TaskThreadBase::YieldThread()
{
    wxThread::Yield();
}


}

}


