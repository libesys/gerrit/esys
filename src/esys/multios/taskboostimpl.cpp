/*!
 * \file esys/multios/taskboostimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/multios/taskboost.h"
#include "esys/multios/taskboostimpl.h"

namespace esys
{

namespace boost
{

TaskImpl *TaskImpl::Current()
{
    return NULL;
}

TaskImpl::TaskImpl()
{
    m_started_time = ::boost::posix_time::second_clock::local_time();
}

TaskImpl::~TaskImpl()
{
}

millis_t TaskImpl::Millis()
{
    ::boost::posix_time::ptime now = ::boost::posix_time::second_clock::local_time();
    ::boost::posix_time::time_duration diff = now - m_started_time;

    return diff.total_milliseconds();
}

void TaskImpl::Wait()
{
}

void TaskImpl::Signal()
{
}

void TaskImpl::Sleep(uint64_t val)
{

}

void TaskImpl::SetTask(Task *task)
{
    m_task=task;
}

Task *TaskImpl::GetTask()
{
    return m_task;
}

}

}
