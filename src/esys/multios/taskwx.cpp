/*!
 * \file esys/multios/taskwx.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/multios/taskwx.h"
#include "esys/multios/taskwximpl.h"

#include <wx/timer.h>

namespace esys
{

namespace wx
{

Task *Task::Current()
{
    TaskImpl *cur=TaskImpl::GetCurrent();
    if (cur!=NULL)
        return cur->GetTask();
    return NULL;
}

TaskBase *Task::CurrentBase()
{
    return Current();
}

Task::Task(const char *name): TaskBase(name)
{
    m_impl=new TaskImpl();
    m_impl->SetTask(this);
    TaskBase::AddCurrentFct(&Task::CurrentBase);
}

Task::~Task()
{
    delete m_impl;
}

millis_t Task::Millis()
{
    return m_impl->Millis();
}

void Task::Sleep(uint64_t val)
{
    m_impl->Sleep(val);
}

void Task::Wait()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void Task::Signal()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void Task::DoLoopIterationEnds()
{
    assert(m_impl!=NULL);

    m_impl->DoLoopIterationEnds();
}

}

}

