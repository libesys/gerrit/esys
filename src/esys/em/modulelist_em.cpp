/*!
 * \file esys/em/modulelist.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/em/modulelist.h"
#include "esys/assert.h"

namespace esys
{

namespace em
{

ModuleList *ModuleList::m_dft_list_mod = nullptr;

ModuleList &ModuleList::GetCurrent()
{
    assert(m_dft_list_mod != nullptr);
    return *m_dft_list_mod;
}

ModuleList::ModuleList() : ModuleListBase()
{
    m_dft_list_mod = this;
}

ModuleList::~ModuleList()
{
}

}

}




