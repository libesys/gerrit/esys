/*!
 * \file esys/timercallbackbase.h
 * \brief The base class of all Timer Callbacks
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/timercallbackbase.h"

namespace esys
{

TimerCallbackBase::TimerCallbackBase() : m_timer(nullptr)
{
}

TimerCallbackBase::~TimerCallbackBase()
{
}

void TimerCallbackBase::SetTimer(TimerBase *timer)
{
    m_timer = timer;
}

TimerBase *TimerCallbackBase::GetTimer()
{
    return m_timer;
}

}




