/*!
 * \file esys/bufferbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/bufferbase.h"

namespace esys
{

BufferBase::BufferBase(uint16_t max_size, uint16_t size)
    : m_size(size), m_max_size(max_size) //, m_idx(0), m_offset(0), m_valid(true)
{
}

BufferBase::~BufferBase()
{
}

void BufferBase::SetSize(uint16_t size)
{
    m_size=size;
}

uint16_t BufferBase::GetSize()
{
    return m_size;
}

void BufferBase::SetMaxSize(uint16_t size)
{
    m_max_size=size;
}

uint16_t BufferBase::GetMaxSize()
{
    return m_max_size;
}

void BufferBase::Clear()
{
    SetSize(0);
}

}
