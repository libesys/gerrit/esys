/*!
 * \file esys/timermngrbase.h
 * \brief The base class of all Timer Manager implementations
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/timermngrbase.h"
#include "esys/timerbase.h"

namespace esys
{

TimerMngrBase::TimerMngrBase(const ObjectName &name)
    : ObjectNode(name), m_first(nullptr), m_last(nullptr), m_count(0)
{
}

TimerMngrBase::~TimerMngrBase()
{
}

uint16_t TimerMngrBase::GetCount()
{
    return m_count;
}

int32_t TimerMngrBase::StopAll()
{
    return -2;
}

void TimerMngrBase::Start(TimerBase *timer)
{
}

void TimerMngrBase::Stop(TimerBase *timer)
{
}

bool TimerMngrBase::IsRunning(TimerBase *timer)
{
    return true;
}

void TimerMngrBase::AddTimer(TimerBase *timer)
{
    ++m_count;
    if (m_first == nullptr)
    {
        m_first = timer;
        m_last = timer;
        return;
    }
    m_last->SetNext(timer);
    m_last = timer;
}

}





