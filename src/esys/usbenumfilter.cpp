/*!
 * \file esys/usbenumfilter.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/usbenumfilter.h"

namespace esys
{

USBEnumFilter::USBEnumFilter()
{
}

USBEnumFilter::~USBEnumFilter()
{
}

void USBEnumFilter::Filter(USBDevInfo *dev)
{
    if (IsKnownDev(dev)) AddDev(dev);
}

std::size_t USBEnumFilter::GetNbrFiltered()
{
    return m_devices.size();
}

USBDevInfo *USBEnumFilter::GetFiltered(std::size_t idx)
{
    if (idx >= m_devices.size()) return nullptr;
    return m_devices[idx];
}

void USBEnumFilter::ClearFiltered()
{
    m_devices.clear();
}

void USBEnumFilter::AddDev(USBDevInfo *dev)
{
    m_devices.push_back(dev);
}

} // namespace esys
