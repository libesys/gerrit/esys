/*!
* \file esys/pointer.cpp
* \brief
*
* \cond
*__legal_b__
*
*Copyright (c) 2014 Michel Gillet
*Distributed under the wxWindows Library Licence, Version 3.1.
*(See accompanying file LICENSE_3_1.txt or
*copy at http://www.wxwidgets.org/about/licence)
*
*__legal_e__
* \endcond
*
*/

#include "esys/esys_prec.h"
#include "esys/pointer.h"

namespace esys
{

PointerBase::PointerBase(uint16_t max_size, uint16_t size)
    : m_size(size), m_max_size(max_size), m_idx(0), m_offset(0), m_valid(true)
{
}

PointerBase::~PointerBase()
{
}

void PointerBase::SetSize(uint16_t size)
{
    m_size=size;
}

uint16_t PointerBase::GetSize()
{
    return m_size;
}

void PointerBase::SetMaxSize(uint16_t size)
{
    m_max_size=size;
}

uint16_t PointerBase::GetMaxSize()
{
    return m_max_size;
}

void PointerBase::SetIndex(uint16_t idx)
{
    m_idx=idx;
}

uint16_t PointerBase::GetIndex()
{
    return m_idx;
}

void PointerBase::Reset()
{
    SetIndex(0);
    SetOffset(0);
}

bool PointerBase::IsValid()
{
    return m_valid;
}

void PointerBase::SetValid(bool valid)
{
    m_valid=true;
}

void PointerBase::SetOffset(uint16_t offset)
{
    m_offset=offset;
}

uint16_t PointerBase::GetOffset()
{
    return m_offset;
}

}
