/*!
 * \file esys/taskmngrplatif.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/taskmngrplatif.h"

namespace esys
{

TaskMngrPlatIf::~TaskMngrPlatIf()
{
}

int32_t TaskMngrPlatIf::PlatInit()
{
    return 0;
}

int32_t TaskMngrPlatIf::PlatRelease()
{
    return 0;
}

void TaskMngrPlatIf::ExitScheduler()
{
}

}



