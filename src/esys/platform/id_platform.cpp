/*!
 * \file esys/platform/id_plaftorm.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/platform/id.h"

#ifdef ESYS_MULTI_PLAT

namespace esys
{

namespace platform
{

Id *Id::m_last = nullptr;
uint_t Id::m_count = 0;

std::map<uint_t, Id*> Id::m_map_ids;
std::map<std::wstring, Id*> Id::m_map_name_ids;

Id *Id::Get(uint_t id)
{
    if (m_map_ids.size() != m_count)
        Populate();

    std::map<uint_t, Id*>::iterator it;

    it = m_map_ids.find(id);
    if (it == m_map_ids.end())
        return nullptr;
    return it->second;
}

Id *Id::Find(const std::wstring &name)
{
    if (m_map_ids.size() != m_count)
        Populate();

    std::map<std::wstring, Id*>::iterator it;

    it = m_map_name_ids.find(name);
    if (it == m_map_name_ids.end())
        return nullptr;
    return it->second;
}

void Id::Populate()
{
    Id *p;

    m_map_ids.clear();

    p = m_last;
    while (p != nullptr)
    {
        m_map_ids[p->m_id] = p;
        if (p->GetName().empty() == false)
        {
            m_map_name_ids[p->GetName()] = p;
        }
        p = p->m_prev;
    }
}

Id::Id(uint_t id, const std::wstring &name): m_id(id), m_name(name)
{
    m_prev = m_last;
    m_last = this;
    m_count++;
}

Id::~Id()
{
}

Id::operator uint_t() const
{
    return m_id;
}

const std::wstring &Id::GetName()
{
    return m_name;
}

const uint_t Id::GetId() const
{
    return m_id;
}

ESYS_API const Id MULTIOS_WX(ESYS_PLAT_MULTIOS_WX, L"MultiOS wxWidgets");
ESYS_API const Id BOOST(ESYS_PLAT_BOOST, L"Boost");
ESYS_API const Id SYSC(ESYS_PLAT_SYSC, L"SystemC");
ESYS_API const Id MULTI_PLAT(ESYS_PLAT_MULTI_PLAT, L"Multi Platforms");
ESYS_API const Id FREERTOS(ESYS_PLAT_FREERTOS, L"FreeRTOS");
//static const Id ARD;

ESYS_API const MultiOSwx_t MultiOSwx(MULTIOS_WX);
ESYS_API const Boost_t Boost(BOOST);
ESYS_API const SysC_t SysC(SYSC);
ESYS_API const MultiPlat_t MultiPlat(MULTI_PLAT);
ESYS_API const FreeRTOS_t FreeRTOS(FREERTOS);

template<uint_t N>
class Test
{
public:
    static const uint_t n=N;
};

typedef Test<MultiOSwx_t::ID> TestW;

}

}

#endif

