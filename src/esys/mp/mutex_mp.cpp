/*!
 * \file esys/mp/mutex.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/mutex.h"
#include "esys/mp/esys.h"

namespace esys
{

namespace mp
{

Mutex::Mutex(const ObjectName &name, Type type): ObjectInitRelay<MutexBase>(name)
{
    MutexBase *mutex = ESys::Get().NewMutexBase(name, type);
    assert(mutex != nullptr);

    SetObjRelay(mutex);
}

Mutex::~Mutex()
{
}

int32_t Mutex::Lock(bool from_isr)
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->Lock(from_isr);
}

int32_t Mutex::UnLock(bool from_isr)
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->UnLock(from_isr);
}

int32_t Mutex::TryLock(bool from_isr)
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->TryLock(from_isr);
}

}

}


