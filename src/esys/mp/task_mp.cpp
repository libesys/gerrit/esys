/*!
 * \file esys/mp/task_mp.cpp
 * \brief Declaration of the Multi Platform Task class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/task.h"
#include "esys/esys.h"

namespace esys
{

namespace mp
{

Task::TaskMap Task::s_task_map;

Task &Task::GetCurrent()
{
    TaskMap::iterator it;
    TaskPlatIf *task_plat_if = ESys::Get().GetCurTaskPlatIf();

    assert(task_plat_if != nullptr);

    it = s_task_map.find(task_plat_if);

    assert(it != s_task_map.end());
    assert(it->second != nullptr);

    return *it->second;
}

Task::Task(const ObjectName &name, TaskType type) : TaskBase(name, type), m_task_plat_if(nullptr)
{
    m_task_plat_if = ESys::Get().NewTaskPlat(this);
    m_task_plat_if->SetTaskBase(this);

    s_task_map[m_task_plat_if] = this;
}

Task::~Task()
{
    delete m_task_plat_if;
}

int32_t Task::Create()
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Create();
}

int32_t Task::Start()
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Start();
}

int32_t Task::Stop()
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Stop();
}

int32_t Task::Kill()
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Kill();
}

int32_t Task::Destroy()
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Destroy();
}

/*int32_t Task::Done()
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Done();
}*/

millis_t Task::Millis()
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Millis();
}

void Task::Sleep(millis_t ms)
{
    assert(m_task_plat_if != nullptr);

    return m_task_plat_if->Sleep(ms);
}

void Task::SleepU(micros_t us)
{
    assert(m_task_plat_if != nullptr);

    m_task_plat_if->SleepU(us);
}

void Task::SetTaskBase(TaskBase *task_base)
{
    assert(m_task_plat_if != nullptr);

    m_task_plat_if->SetTaskBase(task_base);
}

}

}






