/*!
 * \file esys/mp/tasklist_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/tasklist.h"

#include "esys/assert.h"

namespace esys
{

namespace mp
{

TaskList *TaskList::s_task_list = nullptr;

TaskList &TaskList::GetCurrent()
{
    /* When it will be possible to run 2 firmwares in parallel, something else will need
        to be done. And if there is only 1 firmware running, we can use a simple static member variable
     */
    assert(s_task_list != nullptr);
    return *s_task_list;
}

TaskList::TaskList() : TaskListBase()
{
    if (s_task_list == nullptr)
        s_task_list = this;
}

TaskList::~TaskList()
{
}

void TaskList::Add(TaskBase *task_base)
{
    /* If in a setup where 2 MCUs communicating are tested, we would need first to find which MCUs it is,
    by using ThreadId and then get the TaskList for that MCU. But if there is only one MCU, the statically
    created TaskList by TaskBase can be used. */
    TaskListBase::Add(task_base);
}

}

}


