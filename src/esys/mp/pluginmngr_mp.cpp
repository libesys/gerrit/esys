/*!
 * \file esys/mp/pluginmngr_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/pluginmngr.h"
#include "esys/mp/plugin.h"
#include "esys/assert.h"

namespace esys
{

namespace mp
{

PluginMngr::PluginMngr(): PluginMngrCore_t<Plugin>()
{
    SetEntryFctName("get_esys_plugin");
}

PluginMngr::~PluginMngr()
{
}

PluginBase *PluginMngr::GetPluginFromEntryFct(void *entry_fct)
{
    PluginEntryFunction the_entry_fct = (PluginEntryFunction)entry_fct;
    Plugin *plugin;

    assert(entry_fct != nullptr);

    plugin = (*the_entry_fct)();

    return plugin;
}

}

}

