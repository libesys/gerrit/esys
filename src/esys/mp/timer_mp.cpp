/*!
 * \file esys/mp/timer_mp.cpp
 * \brief The Multi Platform declaration of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/timer.h"
#include "esys/mp/esys.h"
#include "esys/timerbase.h"
#include "esys/assert.h"

namespace esys
{

namespace mp
{

Timer::Timer(const ObjectName &name) : TimerBase(name)
{
    m_timer = ESys::Get().NewTimerBase(name);
}

Timer::~Timer()
{
    delete m_timer;
}

int32_t Timer::Start(uint32_t ms, bool one_shot, bool from_isr)
{
    assert(m_timer != nullptr);

    return m_timer->Start(ms, one_shot, from_isr);
}

int32_t Timer::Stop(bool from_isr)
{
    assert(m_timer != nullptr);

    return m_timer->Stop(from_isr);
}

int32_t Timer::GetPeriod()
{
    assert(m_timer != nullptr);

    return m_timer->GetPeriod();
}

bool Timer::IsRunning()
{
    assert(m_timer != nullptr);

    return m_timer->IsRunning();
}

bool Timer::IsOneShot()
{
    assert(m_timer != nullptr);

    return m_timer->IsOneShot();
}

void Timer::SetCallback(TimerCallbackBase *callback)
{
    assert(m_timer != nullptr);

    m_timer->SetCallback(callback);
}

TimerCallbackBase *Timer::GetCallback()
{
    assert(m_timer != nullptr);

    return m_timer->GetCallback();
}

}

}






