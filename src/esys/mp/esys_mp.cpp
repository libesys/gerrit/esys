/*!
 * \file esys/mp/esys.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/esys.h"
#include "esys/mp/esysbase.h"
#include "esys/assert.h"

namespace esys
{

namespace mp
{

ESys ESys::s_esys;
int32_t ESys::s_platform_id = -1;
const platform::Id *ESys::s_platform = nullptr;
const platform::Id *ESys::s_dft_platform = &platform::BOOST;
ESysBase *ESys::s_esys_base = nullptr;

std::size_t ESys::GetCount()
{
    ESys &esys_mp = Get();

    return ESysBase::GetCount();
}

std::size_t ESys::GetPluginsCount()
{
    ESys &esys_mp = Get();

    return esys_mp.GetPluginMngr().GetNbr();
}

Plugin *ESys::GetPlugin(std::size_t idx)
{
    ESys &esys_mp = Get();

    return esys_mp.GetPluginMngr().Get(idx);
}

void ESys::Sleep(uint16_t msec)
{
    Get().GetCurTaskPlatIf()->Sleep(msec);
}

millis_t ESys::Millis()
{
    return Get().GetCurTaskPlatIf()->Millis();
}

ESys &ESys::Get()
{
    s_esys.LoadPlugins();

    return s_esys;
}

ESysBase &ESys::GetBase()
{
    if (s_esys_base != nullptr)
        return *s_esys_base;

    s_esys.LoadPlugins();

    assert(s_dft_platform != nullptr);

    SetPlatform(*s_dft_platform);
    assert(s_esys_base != nullptr);
    return *s_esys_base;
}

ESysBase *ESys::GetBase(std::size_t idx)
{
    s_esys.LoadPlugins();

    if (idx >= GetCount())
        return nullptr;
    return s_esys.GetByIdx(idx);
}

void ESys::SetPlatform(int platform_id)
{
    s_esys.LoadPlugins();

    s_platform_id = platform_id;
    s_platform = platform::Id::Get(platform_id);
    s_esys_base = GetById(platform_id);
}

void ESys::SetPlatform(const platform::Id &platform_id)
{
    s_esys.LoadPlugins();

    if (platform_id != esys::platform::MULTI_PLAT)
    {
        s_platform = &platform_id;
        s_platform_id = platform_id.GetId();
        s_esys_base = GetById(platform_id);
    }
    else
    {
        s_platform = nullptr;
        s_platform_id = -1;
        s_esys_base = nullptr;
    }
}

ESys::ESys() : ESysBase(ESYS_PLAT_MULTI_PLAT, "MultiPlatform")
{
    SetElement(this);
#ifdef WIN32
    m_plugin_mngr.SetDir("plugins\\esys");
#else
    m_plugin_mngr.SetDir("../lib/plugins/esys");
#endif
}

ESys::~ESys()
{
}

int ESys::Release()
{
    return Get().ReleasePlugins();
}

int32_t ESys::CreateLog()
{
    // TBD after rtos_io branch rebased on master then merged
    return -1;
    //return GetBase().CreateLog();
}

bool ESys::GetPluginsLoaded()
{
    return m_plugins_loaded;
}

PluginMngr &ESys::GetPluginMngr()
{
    return m_plugin_mngr;
}

void ESys::SetPluginsLoaded(bool plugins_loaded)
{
    m_plugins_loaded = plugins_loaded;
}

int ESys::LoadPlugins()
{
    int result;

    if (GetPluginsLoaded() == true)
        return 0;

    result = m_plugin_mngr.Load();
    SetPluginsLoaded();

    return result;
}

int ESys::ReleasePlugins()
{
    int result;

    if (GetPluginsLoaded() == false)
        return -1;

    result = m_plugin_mngr.Release();
    SetPluginsLoaded(false);

    return result;
}

SemaphoreBase *ESys::NewSemaphoreBase(const ObjectName &name, uint32_t count)
{
    return GetBase().NewSemaphoreBase(name, count);
}

MutexBase *ESys::NewMutexBase(const ObjectName &name, MutexBase::Type type)
{
    return GetBase().NewMutexBase(name, type);
}

TaskPlatIf *ESys::NewTaskPlat(TaskBase *task_base)
{
    return GetBase().NewTaskPlat(task_base);
}

TaskMngrPlatIf *ESys::NewTaskMngrPlat(TaskMngrBase *taskmngr_base)
{
    return GetBase().NewTaskMngrPlat(taskmngr_base);
}

SystemPlatIf *ESys::NewSystemPlat(SystemBase *system_base)
{
    return GetBase().NewSystemPlat(system_base);
}

TimerBase *ESys::NewTimerBase(const ObjectName &name)
{
    return GetBase().NewTimerBase(name);
}

uint32_t ESys::implMillis(TimeBase::Source source)
{
    return GetBase().implMillis(source);
}

uint64_t ESys::implMicros(TimeBase::Source source)
{
    return GetBase().implMicros(source);
}

TaskPlatIf *ESys::GetCurTaskPlatIf()
{
    return GetBase().GetCurTaskPlatIf();
}

}

}


