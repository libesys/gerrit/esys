/*!
 * \file esys/mp/esysbase_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/esysbase.h"
#include "esys/mutexbase.h"
#include "esys/staticlistid.h"
#include "esys/time.h"

namespace esys
{

namespace mp
{

ESysBase::ESysBase(int32_t platform_id, const std::string &name)
    : BaseType(platform_id, this), esys::ESysBase(), m_platform(nullptr), m_name(name)
{
}

ESysBase::~ESysBase()
{
}

platform::Id *ESysBase::GetPlatformId()
{
    platform::Id *p_id;
    int_t id = GetId();

    p_id = platform::Id::Get(id);
    return p_id;
}

const std::string &ESysBase::GetName()
{
    return m_name;
}

void ESysBase::SetName(const std::string &name)
{
    m_name = name;
}

/*void ESysBase::SetSystemBase(SystemBase *system_base)
{
    m_system_base = system_base;
}

SystemBase *ESysBase::GetSystemBase()
{
    return m_system_base;
} */

uint32_t ESysBase::implMicrosLow()
{
    uint64_t us = implMicros();
    us = (us << 32) >> 32;
    return (uint32_t)us;
}

int32_t ESysBase::SetTime(TimeBase::TimeStruct &time)
{
    return -1;
}

int32_t ESysBase::GetTime(TimeBase::TimeStruct &time)
{
    return -1;
}

int32_t ESysBase::SetDate(TimeBase::DateStruct &date)
{
    return -1;
}

int32_t ESysBase::GetDate(TimeBase::DateStruct &date)
{
    return -1;
}

int32_t ESysBase::SetDateTime(TimeBase::DateTimeStruct &date_time)
{
    return -1;
}

int32_t ESysBase::GetDateTime(TimeBase::DateTimeStruct &date_time)
{
    return -1;
}

int32_t ESysBase::StartTimestamp()
{
    return -1;
}

int32_t ESysBase::StopTimestamp()
{
    return -1;
}

uint32_t ESysBase::implMillis(TimeBase::Source source)
{
    return Time::Millis();
}

uint64_t ESysBase::implMicros(TimeBase::Source source)
{
    return Time::Micros();
}

}

}


