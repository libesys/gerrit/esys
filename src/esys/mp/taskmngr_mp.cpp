/*!
 * \file esys/mp/taskmngr_mp.cpp
 * \brief Declaration of the Multi Platform TaskMngr class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/taskmngr.h"
#include "esys/esys.h"

#include "esys/assert.h"

namespace esys
{

namespace mp
{

TaskMngr *g_current = nullptr;

TaskMngr *TaskMngr::GetCurrent()
{
    return g_current;
}

TaskMngr::TaskMngr(const ObjectName &name) : TaskMngrBase(name)
{
    g_current = this;
    m_taskmngr_plat_if = ESys::Get().NewTaskMngrPlat(this);
}

TaskMngr::~TaskMngr()
{
    delete m_taskmngr_plat_if;
}

int32_t TaskMngr::StartScheduler()
{
    assert(m_taskmngr_plat_if != nullptr);

    return m_taskmngr_plat_if->StartScheduler();
}

void TaskMngr::Done(TaskBase *task_base)
{
    assert(m_taskmngr_plat_if != nullptr);

    m_taskmngr_plat_if->Done(task_base);
}

void TaskMngr::ExitScheduler()
{
    assert(m_taskmngr_plat_if != nullptr);

    m_taskmngr_plat_if->ExitScheduler();
}

/*void TaskMngr::AllAppTasksDone()
{
    assert(m_taskmngr_plat_if != nullptr);

    //m_taskmngr_plat_if->ExitScheduler();
} */

}

}






