/*!
 * \file esys/mp/semaphore_mp.cpp
 * \brief Declaration of the Multi Platorm Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/semaphore.h"
#include "esys/mp/esys.h"
#include "esys/assert.h"

namespace esys
{

namespace mp
{

Semaphore::Semaphore(const ObjectName &name, uint32_t count) : ObjectInitRelay<SemaphoreBase>(name)
{
    SemaphoreBase *sem_base = ESys::Get().NewSemaphoreBase(name, count);
    assert(sem_base != nullptr);

    SetObjRelay(sem_base);
}

Semaphore::~Semaphore()
{
}

int32_t Semaphore::Post(bool from_isr)
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->Post(from_isr);
}

int32_t Semaphore::Wait(bool from_isr)
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->Wait(from_isr);
}

int32_t Semaphore::WaitFor(uint32_t ms, bool from_isr)
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->WaitFor(ms, from_isr);
}

int32_t Semaphore::TryWait(bool from_isr)
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->TryWait(from_isr);
}

uint32_t Semaphore::Count()
{
    assert(GetObjRelay() != nullptr);

    return GetObjRelay()->Count();
}

/*int32_t Semaphore::StaticInit(Order order)
{
    assert(m_sem_base != nullptr);

    return m_sem_base->StaticInit(order);
}

int32_t Semaphore::StaticRelease(Order order)
{
    assert(m_sem_base != nullptr);

    return m_sem_base->StaticRelease(order);
}

int32_t Semaphore::RuntimeInit(RuntimeLevel runtime_lvl, Order order)
{
    assert(m_sem_base != nullptr);

    return m_sem_base->RuntimeInit(runtime_lvl, order);
}

int32_t Semaphore::RuntimeRelease(RuntimeLevel runtime_lvl, Order order)
{
    assert(m_sem_base != nullptr);

    return m_sem_base->RuntimeRelease(runtime_lvl, order);
} */

}

}




