/*!
 * \file esys/mp/system.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/system.h"

#include "esys/esys.h"

#include "esys/assert.h"

namespace esys
{

namespace mp
{

System::System(const ObjectName &name) : SystemBase(name)
{
    m_plat_if= ESys::Get().NewSystemPlat(this);
    assert(m_plat_if != nullptr);
}

System::~System()
{
    delete m_plat_if;
}

int32_t System::PlatInit()
{
    assert(m_plat_if != nullptr);

    return m_plat_if->PlatInit();
}

int32_t System::PlatRelease()
{
    assert(m_plat_if != nullptr);

    return m_plat_if->PlatRelease();
}

void System::SetPlatIf(SystemPlatIf *plat_if)
{
    m_plat_if = plat_if;
}

SystemPlatIf *System::GetPlatIf()
{
    return m_plat_if;
}

}

}






