/*!
 * \file esys/mp/time_mp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/mp/time.h"
#include "esys/mp/esys.h"
//#include "esys/mp/platformbase.h"
#include "esys/assert.h"

namespace esys
{

namespace mp
{

Time::Time(): TimeBase()
{
}

uint32_t Time::Millis(Source source)
{
    return ESys::Get().implMillis(source);
}

uint64_t Time::Micros(Source source)
{
    return ESys::Get().implMicros(source);
}

uint32_t Time::MicrosLow()
{
    return ESys::Get().implMicrosLow();
}

void Time::Sleep(uint32_t ms)
{
    ESys::Get().Sleep(ms);
}

void Time::USleep(uint32_t us)
{
    //ESys::Get().USleep(us); ???
}

int32_t Time::SetTime(TimeStruct &time)
{
    return ESys::Get().SetTime(time);
}

int32_t Time::GetTime(TimeStruct &time)
{
    return ESys::Get().GetTime(time);
}

int32_t Time::SetDate(DateStruct &date)
{
    return ESys::Get().SetDate(date);
}

int32_t Time::GetDate(DateStruct &date)
{
    return ESys::Get().GetDate(date);
}

int32_t Time::SetDateTime(DateTimeStruct &date_time)
{
    return ESys::Get().SetDateTime(date_time);
}

int32_t Time::GetDateTime(DateTimeStruct &date_time)
{
    return ESys::Get().GetDateTime(date_time);
}

int32_t Time::StartTimestamp()
{
    return ESys::Get().StartTimestamp();
}

int32_t Time::StopTimestamp()
{
    return ESys::Get().StopTimestamp();
}

}

}



