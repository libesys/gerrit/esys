/*!
 * \file esys/usbdevinfo.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/usbdevinfo.h"

#include <typeinfo>
#include <boost/locale.hpp>
#include <boost/lexical_cast.hpp>

#ifdef WIN32
#include <esys/win32/devnodemngr.h>
#include <esys/win32/serialportenum.h>
#endif

namespace esys
{

USBDevInfo::USBDevInfo(Kind kind)
    : USBDevInfoBase(kind)
    , m_class(0)
    , m_subclass(0)
    , m_id_vendor(0)
    , m_id_product(0)
    , m_bus_num(0)
    , m_dev_addr(0)
    , m_serial_impl(NULL)
    , m_dev_inst(0)
    , m_dev_node(nullptr)
    , m_parent(nullptr)
{
}

USBDevInfo::~USBDevInfo()
{
    std::vector<USBDevInfo *>::iterator it;

    for (it = m_children.begin(); it != m_children.end(); ++it)
    {
        delete *it;
    }
    m_children.clear();
}

uint8_t USBDevInfo::GetClass()
{
    return m_class;
}

uint8_t USBDevInfo::GetSubClass()
{
    return m_subclass;
}

uint16_t USBDevInfo::GetIdVendor()
{
    return GetUSBAddr().GetVID();
}

uint16_t USBDevInfo::GetIdProduct()
{
    return GetUSBAddr().GetPID(); // m_id_product;
}

uint8_t USBDevInfo::GetBusNumber()
{
    return m_bus_num;
}

uint8_t USBDevInfo::GetDeviceAddress()
{
    return m_dev_addr;
}

const std::string &USBDevInfo::GetDescription() const
{
    return m_description;
}

const std::string &USBDevInfo::GetManufacturer() const
{
    return m_manufacturer;
}

const std::string &USBDevInfo::GetFriendlyName() const
{
    return m_friendly_name;
}

const std::string &USBDevInfo::GetPhyDevObjName() const
{
    return m_phy_dev_obj_name;
}

const std::string &USBDevInfo::GetBusNumberWStr() const
{
    return m_bus_number;
}

const std::string &USBDevInfo::GetLocationPath() const
{
    return m_location_path;
}

const std::string &USBDevInfo::GetSerialNumber() const
{
    return m_serial_number;
}

void USBDevInfo::SetDescription(const std::string &desc)
{
    m_description = desc;
}

void USBDevInfo::SetManufacturer(const std::string &manufacturer)
{
    m_manufacturer = manufacturer;
}

void USBDevInfo::SetFriendlyName(const std::string &name)
{
    m_friendly_name = name;
}

void USBDevInfo::SetPhyDevObjName(const std::string &name)
{
    m_phy_dev_obj_name = name;
}

void USBDevInfo::SetBusNumber(const std::string &name)
{
    m_bus_number = name;
}

void USBDevInfo::SetDeviceAddress(uint8_t dev_addr)
{
    m_dev_addr = dev_addr;
}

void USBDevInfo::SetLocationPath(const std::string &name)
{
    m_location_path = name;
}

const std::string USBDevInfo::GetPortName_s() const
{
    return boost::locale::conv::utf_to_utf<char>(m_serial_port_name);
}

int32_t USBDevInfo::FindPortName()
{
    uint32_t idx;
    USBDevInfo *child;
    std::string port_name;
    int32_t result = 0;

    if (m_serial_port_name.empty() == false) return 0;
#ifdef WIN32
    esys::DevNodeMngr &dev_node_mngr = esys::DevNodeMngr::Get();
    // esys::COMPortEnum &port_enum = esys::COMPortEnum::Get();
    esys::DevNode *dev_node;
    esys::DevNode *dev_node_child;
    std::string child_instance_id;
    std::string com_port;

    dev_node = GetDevNode();
    if (dev_node != nullptr)
    {
        for (idx = 0; idx < dev_node->GetNbrChildren(); ++idx)
        {
            dev_node_child = dev_node->GetChild(idx);
            result = dev_node_child->GetProperty("comportname", com_port);
            if ((result == 0) && (port_name.empty() == true))
            {
                port_name = com_port;
            }
        }
    }
#endif
    SetPortName(port_name);
    if (result != 0) return result;
    if (port_name.empty()) return -1;
    return 0;
}

const std::string &USBDevInfo::GetPortName() const
{
    if (!m_serial_port_name.empty()) return m_serial_port_name;

    USBDevInfo *child;

    for (unsigned int idx = 0; idx < GetChildrenCount(); ++idx)
    {
        child = GetChild(idx);
        const std::string &child_port = child->GetPortName();
        if (!child_port.empty()) return child_port;
    }
    return m_serial_port_name;
}

int USBDevInfo::GetPortNameInt()
{
    int port_num;
    std::string str_port_num;

#ifdef WIN32
    if (m_serial_port_name.size() < 3) return -1;
    str_port_num = m_serial_port_name.substr(3);
#else
    // No idea what to do here
    return -1;
#endif

    try
    {
        port_num = boost::lexical_cast<int>(str_port_num);
    }
    catch (boost::bad_lexical_cast &)
    {
        return -1;
    }
    return port_num;
}

void USBDevInfo::SetPortName(const char *name)
{
    m_serial_port_name = name;
}

void USBDevInfo::SetPortName(const std::string &name)
{
    m_serial_port_name = name;
}

void USBDevInfo::SetClass(uint8_t class_)
{
    m_class = class_;
}

void USBDevInfo::SetSubClass(uint8_t subclass)
{
    m_subclass = subclass;
}

void USBDevInfo::SetIdVendor(uint16_t id_vendor)
{
    m_id_vendor = id_vendor;
}

void USBDevInfo::SetIdProduct(uint16_t id_product)
{
    m_id_product = id_product;
}

void USBDevInfo::SetBusNumber(uint8_t busnum)
{
    m_bus_num = busnum;
}

void USBDevInfo::SetPorts(uint8_t *ports)
{
    uint32_t i = 0;

    while ((i < 16) && (ports[i] != 0)) m_ports[i] = ports[i++];
}

void USBDevInfo::SetSerialNumber(const std::string &number)
{
    m_serial_number = number;
}

void USBDevInfo::SetSerialNumber(const char *number)
{
    m_serial_number = number;
}

void USBDevInfo::SetUID(const std::string &uid)
{
    m_uid = uid;
}

const std::string &USBDevInfo::GetUID()
{
    return m_uid;
}

void USBDevInfo::SetParent(USBDevInfo *parent)
{
    m_parent = parent;
}

USBDevInfo *USBDevInfo::GetParent()
{
    return m_parent;
}

uint32_t USBDevInfo::GetChildrenCount() const
{
    return m_children.size();
}

USBDevInfo *USBDevInfo::GetChild(uint32_t idx) const
{
    if (idx >= m_children.size()) return nullptr;
    return m_children[idx];
}

/*USBDevInfo *USBDevInfo::GetChild(const std::string &target)
{

} */

void USBDevInfo::AddChild(USBDevInfo *child)
{
    std::map<USBDevInfo *, bool>::iterator it;

    it = m_children_map.find(child);
    if (it != m_children_map.end()) return;
    m_children_map[child] = true;
    m_children.push_back(child);
}

bool USBDevInfo::operator==(const USBDevInfoBase &obj) const
{
    if (typeid(*this) != typeid(obj)) return false;

    const USBDevInfo *obj_;

    try
    {
        obj_ = dynamic_cast<const USBDevInfo *>(&obj);
    }
    catch (std::exception & /*e*/)
    {
        return false;
    }
    return operator==(*obj_);
}

bool USBDevInfo::operator==(const USBDevInfo &obj) const
{
    if (GetPhyDevObjName() == obj.GetPhyDevObjName()) return true;
    return false;
}

void USBDevInfo::SetSerialPortImpl(esys::SerialPortImpl *impl)
{
    m_serial_impl = impl;
}

uint32_t USBDevInfo::GetDeviceInstance()
{
    return m_dev_inst;
}

void USBDevInfo::SetDeviceInstance(uint32_t dev_inst)
{
    m_dev_inst = dev_inst;
}

DevNode *USBDevInfo::GetDevNode()
{
    return m_dev_node;
}

void USBDevInfo::SetDevNode(DevNode *dev_node)
{
    m_dev_node = dev_node;
}

void USBDevInfo::SetLocationPaths(const std::vector<std::string> &location_paths)
{
    m_location_paths = location_paths;
}

const std::vector<std::string> &USBDevInfo::GetLocationPaths()
{
    return m_location_paths;
}

void USBDevInfo::SetUSBTreeItem(USBTreeItem *usb_tree_item)
{
    m_usb_tree_item = usb_tree_item;
}

USBTreeItem *USBDevInfo::GetUSBTreeItem()
{
    return m_usb_tree_item;
}

} // namespace esys
