/*!
 * \file esys/nfc/hslcardapp.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/hslcardapp.h"

namespace esys
{

HSLCardApp::HSLCardApp(): DesFireApp_t<1>()
{
    SetID(0x00ef2011);
    AddSupportedFile(&m_hsl_card_file_id);
}

HSLCardApp::~HSLCardApp()
{
}

}

