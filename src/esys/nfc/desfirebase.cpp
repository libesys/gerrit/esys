/*!
 * \file esys/nfc/desfirebase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/desfirebase.h"
#include "esys/nfc/desfireappbase.h"
#include "esys/nfc/nfccard.h"

#include "esys/assert.h"

#include <dbg_log/dbg_class.h>

namespace esys
{

DesFireBase::DesFireBase(NFCCard *card): NFCCard(card)
{
}

DesFireBase::~DesFireBase()
{
    //Disconnect();
    //Clear();
}

void DesFireBase::SetNFCCard(NFCCard *card)
{
    Clear();
    NFCCard::SetNFCCard(card);
    //SetNFCReader(card->GetNFCReader());
}

/*
int DesFireBase::GetAppIDs()
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::GetAppIDs",false,int, result);
    DBG_CALLMEMBER_END;

    uint8_t rcv_buf[262];
    uint32_t rx_size=262;

    uint8_t GET_APP_IDS[5]= {0x90, 0x6A, 0x00, 0x00, 0x00 };

    Clear();

    result=Transmit(GET_APP_IDS,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rx_size>=2)
    {
        if (rcv_buf[rx_size-2]!=0x91)
        {
            result=-2;
            return result;
        }
        else
        {
            //Either we are done, or there are more frames following
            if (rcv_buf[rx_size-1]==OPERATION_OK)
            {
                //we are done
                AddAppIDs(rcv_buf,rx_size-2);
            }
            else if (rcv_buf[rx_size-1]==ADDITIONAL_FRAME)
            {

            }
        }
        //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
        result=GetNbrAppIDs();
        return result;
    }
    else
    {
        result=-3;
        return result;
    }
}

int DesFireBase::SelectAppID(uint32_t app_id)
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::SelectAppID",false, int, result);
    DBG_PARAM(0,"app_id", uint32_t, app_id);
    DBG_CALLMEMBER_END;

    uint8_t rcv_buf[262];
    uint32_t rx_size=262;

    uint8_t SELECT_APP[9]= {0x90, 0x5A, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00 };
    SELECT_APP[5]=app_id&0xFF;
    SELECT_APP[6]=(app_id>>8)&0xFF;
    SELECT_APP[7]=(app_id>>16)&0xFF;

    result=Transmit(SELECT_APP,9,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rcv_buf[rx_size-2]!=0x91)
    {
        result=-2;
        return result;
    }
    else
    {
        //Either we are done, or there are more frames following
        if (rcv_buf[rx_size-1]==OPERATION_OK)
        {
            result=0;
            return result;
        }
        else if (rcv_buf[rx_size-1]==ADDITIONAL_FRAME)
        {

        }
    }
    //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
    result=-10;
    return result;
}

*/

/*uint16_t DesFireBase::GetNbrAppIDs()
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::GetNbrAppIDs",false, uint16_t, result);
    DBG_CALLMEMBER_END;

    result=m_apps.size();
    return result;
} */

/*int32_t DesFireBase::GetAppID(uint16_t idx)
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::GetAppID",false, int32_t, result);
    DBG_PARAM(0,"idx", uint16_t, idx);
    DBG_CALLMEMBER_END;

    if (idx>=m_apps.size())
    {
        result=-1;
        return result;
    }
    result=m_apps[idx]->GetID();
    return result;
} */

/*DesFireApp *DesFireBase::GetAppIdx(uint16_t idx)
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::GetAppIdx",false, DesFireApp *, result);
    DBG_PARAM(0,"idx", uint16_t, idx);
    DBG_CALLMEMBER_END;

    if (idx>=m_apps.size())
    {
        result=NULL;
        return result;
    }
    result=m_apps[idx];
    return result;
}

DesFireApp *DesFireBase::GetApp(uint32_t app_id)
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::GetApp",false, DesFireApp *, app);
    DBG_PARAM(0,"app_id", uint32_t, app_id);
    DBG_CALLMEMBER_END;

    uint16_t idx;

    for (idx=0; idx<GetNbrAppIDs(); ++idx)
    {
        app=m_apps[idx];
        if (app->GetID()==app_id)
            return app;
    }
    app=NULL;
    return app;
} */

int DesFireBase::SelectApp(uint16_t idx)
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::SelectApp",false, int, result);
    DBG_PARAM(0,"idx", uint16_t, idx);
    DBG_CALLMEMBER_END;

    int32_t app_id=GetAppID(idx);

    if (app_id<0)
    {
        result=app_id;
        return result;
    }
    result=SelectAppID(app_id);
    return result;
}

void DesFireBase::AddAppIDs(uint8_t *buf, uint16_t size)
{
    DBG_CALLMEMBER("esys::DesFireBase::AddAppIDs",false);
    DBG_PARAM_ARRAY(0,"buf", uint8_t *, buf, uint16_t, size);
    DBG_CALLMEMBER_END;

    uint16_t nbr=size/3;
    uint16_t idx;
    uint8_t *p;
    uint32_t app_id;

    DesFireAppBase *app;

    p=buf;

    for (idx=0; idx<nbr; ++idx)
    {
        app_id=(uint32_t)p[0]+(((uint32_t)p[1])<<8)+(((uint32_t)p[2])<<16);

        if (GetNbrSupportedApp()==0)
        {
            AddAppID(app_id);
            /*app=new DesFireApp();
            app->SetPresent();
            m_apps.push_back(app);
            app->SetID(app_id);
            app->SetDesFire(this);*/
        }
        else
        {
            //
            if (IsSupportedApp(app_id, &app))
            {
                assert(app!=NULL);
                app->SetPresent();
            }
        }
        p+=3;
    }
}

/*void DesFireBase::Clear()
{
    DBG_CALLMEMBER("esys::DesFireBase::Clear",false);
    DBG_CALLMEMBER_END;

    uint16_t idx;

    for (idx=0; idx<GetNbrAppIDs(); ++idx)
    {
        delete m_apps[idx];
    }
    m_apps.clear();
} */

/*int32_t DesFireBase::GetNbrSupportedApp()
{
    return m_nbr_supported_app;
}*/

}

