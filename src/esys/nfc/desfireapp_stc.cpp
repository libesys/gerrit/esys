/*!
 * \file esys/nfc/desfireapp_stc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/desfire.h"
#include "esys/nfc/desfireapp_stc.h"
#include "esys/nfc/desfirefile.h"

#include <assert.h>

namespace esys
{

DesFireApp_stc::DesFireApp_stc(): DesFireAppBase()
{
}

DesFireApp_stc::~DesFireApp_stc()
{
}

/*void DesFireApp_stc::SetID(uint32_t id)
{
    m_id=id;
}

int32_t DesFireApp_stc::GetID()
{
    return m_id;
} */

/*int DesFireApp_stc::GetFiles()
{
    uint8_t rcv_buf[262];
    uint32_t rx_size=262;
    int result;

    uint8_t GET_APP_IDS[5]= {0x90, 0x6F, 0x00, 0x00, 0x00 };

    Clear();

    assert(m_desfire!=NULL);

    result=m_desfire->Transmit(GET_APP_IDS,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rcv_buf[rx_size-2]!=0x91)
    {
        return -2;
    }
    else
    {
        //Either we are done, or there are more frames following
        if (rcv_buf[rx_size-1]==DesFire::OPERATION_OK)
        {
            //we are done
            AddFiles(rcv_buf,rx_size-2);
        }
        else if (rcv_buf[rx_size-1]==DesFire::ADDITIONAL_FRAME)
        {

        }
    }
    //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
    return GetNbrFiles();
} */

/*int DesFireApp_stc::Select()
{
    return GetDesFire()->SelectAppID(m_id);
}*/

uint16_t DesFireApp_stc::GetNbrFiles()
{
    return m_files.size();
}

DesFireFileBase *DesFireApp_stc::GetFileIdx(uint16_t idx)
{
    if (idx>=GetNbrFiles())
        return NULL;
    return m_files[idx];
}

DesFireFileBase *DesFireApp_stc::GetFile(uint8_t id)
{
    uint8_t idx;
    DesFireFileBase *file=NULL;

    for (idx=0; idx<m_files.size(); ++idx)
    {
        assert(m_files[idx]!=NULL);

        if (m_files[idx]->GetID()==id)
            file=m_files[idx];
    }
    if (file!=NULL)
        return file;
    file=new DesFireFile();
    file->SetID(id);
    file->SetDesFire(GetDesFire());
    m_files.push_back(file);
    return file;
}

int DesFireApp_stc::AddSupportedFile(DesFireFileBase *file)
{
    m_supported_files.push_back(file);
    return 0;
}

int32_t DesFireApp_stc::GetNbrSupportedFiles()
{
    return m_supported_files.size();
}

int32_t DesFireApp_stc::GetMaxNbrSupportedFiles()
{
    return -1;  //Meaning no hard limit
}

bool DesFireApp_stc::IsSupportedFile(uint32_t file_id, DesFireFileBase **file)
{
    uint16_t idx;

    for (idx=0; idx<GetNbrSupportedFiles(); ++idx)
    {
        if (m_supported_files[idx]->GetID()==file_id)
        {
            if (file!=NULL)
                *file=m_supported_files[idx];
            return true;
        }
    }
    if (file!=NULL)
        *file=NULL;
    return false;
}

/*void DesFireApp_stc::SetDesFire(DesFire *desfire)
{
    m_desfire=desfire;
}

DesFire *DesFireApp_stc::GetDesFire()
{
    return m_desfire;
} */

void DesFireApp_stc::AddFile(uint8_t file_id)
{
    DesFireFile *file;

    file=new DesFireFile();
    m_files.push_back(file);
    file->SetID(file_id);
    file->SetDesFire(GetDesFire());
}

/*void DesFireApp_stc::AddFiles(uint8_t *buf, uint16_t size)
{
    uint16_t nbr=size;
    uint16_t idx;
    uint8_t *p;
    uint8_t file_id;

    DesFireFile *file;

    p=buf;

    for (idx=0; idx<nbr; ++idx)
    {
        file_id=p[0];
        if (GetNbrSupportedFile()==0)
        {
            AddFile(file_id);
        }
        else
        {
            if (IsSupportedFile(file_id, &file))
            {
                file->SetPresent();
                file->SetDesFire(GetDesFire());
            }
        }
        p+=1;
    }
} */

void DesFireApp_stc::Clear()
{
    uint16_t idx;

    for (idx=0; idx<GetNbrFiles(); ++idx)
    {
        delete m_files[idx];
    }
    m_files.clear();

    for (idx=0; idx<GetNbrSupportedFiles(); ++idx)
    {
        m_supported_files[idx]->SetPresent(false);
    }
}

}

