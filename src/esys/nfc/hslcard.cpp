/*!
 * \file esys/nfc/hslcard.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/hslcard.h"

namespace esys
{

HSLCard::HSLCard(NFCCard *card): DesFire_t<1>(card)
{
    AddSupportedApp(&m_hsl_card_app);
}

HSLCard::~HSLCard()
{
}

}

