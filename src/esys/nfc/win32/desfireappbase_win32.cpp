/*!
 * \file esys/nfc/win32/desfireappbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/desfirebase.h"
#include "esys/nfc/win32/desfireappbase.h"

#include "esys/assert.h"

namespace esys
{

namespace win32
{


DesFireAppBase::DesFireAppBase() : esys::DesFireAppBase()
{
}

DesFireAppBase::~DesFireAppBase()
{
}

int DesFireAppBase::GetFiles()
{
    uint8_t rcv_buf[262];
    uint32_t rx_size=262;
    int result;

    uint8_t GET_APP_IDS[5]= {0x90, 0x6F, 0x00, 0x00, 0x00 };

    Clear();

    assert(m_desfire!=NULL);

    result=m_desfire->Transmit(GET_APP_IDS,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rcv_buf[rx_size-2]!=0x91)
    {
        return -2;
    }
    else
    {
        //Either we are done, or there are more frames following
        if (rcv_buf[rx_size-1]==DesFireBase::OPERATION_OK)
        {
            //we are done
            AddFiles(rcv_buf,rx_size-2);
        }
        else if (rcv_buf[rx_size-1]==DesFireBase::ADDITIONAL_FRAME)
        {

        }
    }
    //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
    return GetNbrFiles();
}

}

}

