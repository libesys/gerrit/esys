/*!
 * \file esys/nfc/win32/desfirebase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/win32/desfirebase.h"

#include <dbg_log/dbg_class.h>

namespace esys
{

namespace win32
{

DesFireBase::DesFireBase(NFCCard *card): esys::DesFireBase(card)
{
}

DesFireBase::~DesFireBase()
{
}

int DesFireBase::GetAppIDs()
{
    DBG_CALLMEMBER_RET("esys::win32::DesFireBase::GetAppIDs",false,int, result);
    DBG_CALLMEMBER_END;

    uint8_t rcv_buf[262];
    uint32_t rx_size=262;

    uint8_t GET_APP_IDS[5]= {0x90, 0x6A, 0x00, 0x00, 0x00 };

    Clear();

    result=Transmit(GET_APP_IDS,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rx_size>=2)
    {
        if (rcv_buf[rx_size-2]!=0x91)
        {
            result=-2;
            return result;
        }
        else
        {
            //Either we are done, or there are more frames following
            if (rcv_buf[rx_size-1]==OPERATION_OK)
            {
                //we are done
                AddAppIDs(rcv_buf,rx_size-2);
            }
            else if (rcv_buf[rx_size-1]==ADDITIONAL_FRAME)
            {

            }
        }
        //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
        result=GetNbrAppIDs();
        return result;
    }
    else
    {
        result=-3;
        return result;
    }
}

int DesFireBase::SelectAppID(uint32_t app_id)
{
    DBG_CALLMEMBER_RET("esys::DesFireBase::SelectAppID",false, int, result);
    DBG_PARAM(0,"app_id", uint32_t, app_id);
    DBG_CALLMEMBER_END;

    uint8_t rcv_buf[262];
    uint32_t rx_size=262;

    uint8_t SELECT_APP[9]= {0x90, 0x5A, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00 };
    SELECT_APP[5]=app_id&0xFF;
    SELECT_APP[6]=(app_id>>8)&0xFF;
    SELECT_APP[7]=(app_id>>16)&0xFF;

    result=Transmit(SELECT_APP,9,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rcv_buf[rx_size-2]!=0x91)
    {
        result=-2;
        return result;
    }
    else
    {
        //Either we are done, or there are more frames following
        if (rcv_buf[rx_size-1]==OPERATION_OK)
        {
            result=0;
            return result;
        }
        else if (rcv_buf[rx_size-1]==ADDITIONAL_FRAME)
        {

        }
    }
    //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
    result=-10;
    return result;
}

}

}

