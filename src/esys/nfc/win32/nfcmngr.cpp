/*!
 * \file esys/nfc/win32/nfcmngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/win32/nfcmngr.h"
#include "esys/nfc/win32/nfcreader.h"

namespace esys
{

NFCMngr::NFCMngr()
    : m_init(false)
    , m_released(true)
{
}

NFCMngr::~NFCMngr()
{
    Clear();
}

int NFCMngr::Init()
{
    LONG retCode;
    char buffer[1025];
    DWORD size;

    Clear();

    retCode = SCardEstablishContext(SCARD_SCOPE_USER, NULL, NULL, &m_context);

    if (retCode != SCARD_S_SUCCESS)
    {
        return -1;
    }
    m_init = true;
    // List PC/SC Card Readers
    size = 1024;
    retCode = SCardListReaders(m_context, NULL, buffer, &size);

    if (retCode != SCARD_S_SUCCESS)
    {
        return -2;
    }

    if (buffer == NULL)
    {
        //?? How can thist be?
        return -3;
    }

    char *p = buffer;
    NFCReader *reader;

    while (*p)
    {
        int i;
        for (i = 0; p[i]; i++)
            ;
        i++;
        if (*p != 0)
        {
            reader = new NFCReader(p);
            reader->SetMngr(this);
            m_readers.push_back(reader);
        }
        p = &p[i];
    }
    return 0;
}

unsigned int NFCMngr::GetNbrReaders()
{
    if (m_init == false) Init();

    return m_readers.size();
}

NFCReader *NFCMngr::GetReader(unsigned int idx)
{
    if (m_init == false) Init();

    if (idx >= m_readers.size()) return NULL;
    return m_readers[idx];
}

void NFCMngr::Clear()
{
    unsigned int idx;
    NFCReader *reader;

    for (idx = 0; idx < m_readers.size(); ++idx)
    {
        reader = m_readers[idx];
        if (reader != NULL) delete reader;
    }

    m_readers.clear();
}

SCARDCONTEXT &NFCMngr::GetContext()
{
    return m_context;
}

} // namespace esys
