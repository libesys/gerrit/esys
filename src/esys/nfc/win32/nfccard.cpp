/*!
 * \file esys/win32/nfc/nfccard.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/win32/nfccard.h"

#include <dbg_log/dbg_class.h>
#include <assert.h>

namespace esys
{

NFCCard::NFCCard(const std::wstring &name): m_name(name), m_valid_handle(false), m_card(NULL)
{
}

NFCCard::NFCCard(NFCCard *card): m_valid_handle(false), m_card(NULL)
{
    /*assert(card!=NULL);

    m_name=card->GetName();
    m_valid_handle=card->GetHandle(m_handle);
    card->InvalidateHandle(); */
    if (card!=NULL)
        SetNFCCard(card);
}

NFCCard::~NFCCard()
{
    Disconnect();
}

void NFCCard::SetNFCCard(NFCCard *card)
{
    assert(card!=NULL);

    m_name=card->GetName();
    m_valid_handle=card->GetHandle(m_handle);
    card->InvalidateHandle();
}

void NFCCard::InvalidateHandle()
{
    DBG_CALLMEMBER("esys::NFCCard::InvalidateHandle",false);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);
    m_valid_handle=false;
}

void NFCCard::SetName(const std::wstring &name)
{
    m_name=name;
}

std::wstring &NFCCard::GetName()
{
    return m_name;
}

void NFCCard::SetHandle(SCARDHANDLE handle)
{
    m_handle=handle;
    m_valid_handle=true;
}

bool NFCCard::GetHandle(SCARDHANDLE &handle)
{
    handle=m_handle;
    return m_valid_handle;
}

int NFCCard::Disconnect()
{
    DBG_CALLMEMBER_RET("esys::NFCCard::Disconnect",false,int, result);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);

    LONG result_l;

    if (m_valid_handle==false)
    {
        result=-1;
        return result;
    }

    result_l=SCardDisconnect( m_handle,
                              SCARD_LEAVE_CARD);

    m_valid_handle=false;
    if (result_l!=SCARD_S_SUCCESS)
    {
        result=-2;
        return result;
    }

    result=0;
    return result;
}

int NFCCard::GetAttrib(uint32_t attrib, uint8_t *buf, uint32_t &buf_size)
{
    DBG_CALLMEMBER_RET("esys::NFCCard::GetAttrib",false,int, result);
    DBG_PARAM(0,"attrib", uint32_t, attrib);
    DBG_PARAM_ARRAY_OUT(1,"buf", uint8_t *, buf, uint32_t &, buf_size);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);

    LONG result_l;
    DWORD length=buf_size;

    result_l=SCardGetAttrib(
                 m_handle,
                 attrib,
                 buf,
                 &length);

    if (result_l!=SCARD_S_SUCCESS)
    {
        result=-1;
        return result;
    }
    buf_size=length;
    result=0;
    return result;
}

int NFCCard::Discover()
{
    DBG_CALLMEMBER_RET("esys::NFCCard::Discover",false,int, result);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);

    uint8_t buf[256];
    uint32_t buf_size=256;

    result=GetAttrib(SCARD_ATTR_ATR_STRING,buf,buf_size);
    if (result<0)
    {
        result=-1;
        return result;
    }

    result=GetAttrib(SCARD_ATTR_VENDOR_NAME,buf,buf_size);
    if (result<0)
    {
        result=-1;
        return result;
    }
    result=0;
    return result;
}

int NFCCard::GetATQA(uint16_t &atqa)
{
    DBG_CALLMEMBER_RET("esys::NFCCard::GetATQA",false,int, result);
    DBG_PARAM_OUT(0,"atqa", uint16_t &, atqa);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);

    uint8_t rcv_buf[262];
    uint32_t rx_size=262;

    //uint8_t GET_ATQA[5]={0x90, 0x60, 0x00, 0x00, 0x00 }; Works, get something in return
    //uint8_t GET_ATQA[5]={0x90, 0x26, 0x00, 0x00, 0x00 };
    uint8_t GET_ATQA[5]= {0x90, 0x6A, 0x00, 0x00, 0x00 };

    result=Transmit(GET_ATQA,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rx_size>=2)
    {
        if (rcv_buf[rx_size-2]!=0x90)
        {
            result=-2;
            return result;
        }
        atqa=(rcv_buf[3]<<8)+rcv_buf[4];
        result=0;
        return result;
    }
    else
    {
        result=-2;
        return result;
    }
}

int NFCCard::GetATS(uint8_t *buf,uint32_t &buf_size)
{
    DBG_CALLMEMBER_RET("esys::NFCCard::GetATS",false,int, result);
    DBG_PARAM_ARRAY_OUT(0,"buf", uint8_t *, buf, uint32_t &, buf_size);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);

    uint8_t rcv_buf[262];
    uint32_t rx_size=262;

    uint8_t GET_ATS[5]= {0xFF, 0xCA, 0x01, 0x00, 0x00 }; //0xFF};

    buf_size=0;

    result=Transmit(GET_ATS,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rx_size>=2)
    {
        if (rcv_buf[rx_size-2]!=0x90)
        {
            result=-2;
            return result;
        }
        memcpy(buf,rcv_buf,rx_size-2);
        buf_size=rx_size-2;
        result=0;
        return 0;
    }
    else
    {
        result=-3;
        return result;
    }
}

int NFCCard::GetSerialNumber(uint8_t *buf, uint32_t &buf_size)
{
    DBG_CALLMEMBER_RET("esys::NFCCard::GetSerialNumber",false,int, result);
    DBG_PARAM_ARRAY_OUT(0,"buf", uint8_t *, buf, uint32_t &, buf_size);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);

    uint8_t rcv_buf[262];
    uint32_t rx_size=262;

    uint8_t GET_UID[5]= {0xFF, 0xCA, 0x00, 0x00, 0x00};

    result=Transmit(GET_UID,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rx_size>=2)
    {
        if (rcv_buf[rx_size-2]!=0x90)
        {
            result=-2;
            return result;
        }
        memcpy(buf,rcv_buf,rx_size-2);
        buf_size=rx_size-2;
        result=0;
        return result;
    }
    else
    {
        result=-3;
        return result;
    }
}

int NFCCard::Transmit(uint8_t *tx_buf, uint32_t tx_size,uint8_t *rx_buf, uint32_t &rx_size)
{
    DBG_CALLMEMBER_RET("esys::NFCCard::Transmit",false,int, result);
    DBG_PARAM_ARRAY(0,"tx_buf", uint8_t *, tx_buf, uint32_t &, tx_size);
    DBG_PARAM_ARRAY_OUT(1,"rx_buf", uint8_t *, rx_buf, uint32_t &, rx_size);
    DBG_CALLMEMBER_END;

    DBG_MSG_OS(std::hex << "handle = " << m_handle);

    LONG retCode;
    DWORD rx_buf_size=rx_size;
    SCARD_IO_REQUEST pio_pci;

    /*pio.dwProtocol=SCARD_PCI_T0;
    pio.cbPciLength=sizeof(SCARD_IO_REQUEST); */

    rx_size=0;

    retCode = SCardTransmit(m_handle,
                            NULL, //SCARD_PCI_T0, //&pio_pci,
                            tx_buf,
                            tx_size,
                            NULL, //&pio_pci,
                            rx_buf,
                            &rx_buf_size);

    if( retCode != SCARD_S_SUCCESS )
    {
        result=-1;
        return result;
    }

    rx_size=rx_buf_size;

    result=0;
    return result;
}


}


