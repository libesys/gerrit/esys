/*!
 * \file esys/nfc/win32/desfirefile.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/win32/desfirefile.h"
#include "esys/nfc/desfirebase.h"

#include "esys/assert.h"

namespace esys
{

namespace win32
{

DesFireFile::DesFireFile(): DesFireFileBase()
{
}

DesFireFile::~DesFireFile()
{
}

int DesFireFile::GetData(uint32_t offset, uint8_t *buf, uint16_t &buf_size)
{
    uint8_t rcv_buf[262];
    uint32_t rx_size=262;
    int result;

    //size is 8 without frame wrapping
    //                         Cmd   P1    P2    Size
    uint8_t GET_DATA[13]= {0x90, 0xBD, 0x00, 0x00, 0x07,
                           0x00, // File Id
                           0x00, 0x00, 0x00, // Offset
                           0x00, 0x00, 0x00, // Length
                           0x00
                          }; // Len

    GET_DATA[5]=(uint8_t)m_id;
    GET_DATA[6]=offset&0xFF;
    GET_DATA[7]=(offset>>8)&0xFF;
    GET_DATA[8]=(offset>>16)&0xFF;
    //GET_DATA[9]=buf_size&0xFF;
    //GET_DATA[10]=(buf_size>>8)&0xFF;
    //GET_DATA[11]=0;
    //Clear();

    assert(m_desfire!=NULL);

    result=m_desfire->Transmit(GET_DATA,13,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rcv_buf[rx_size-2]!=0x91)
    {
        return -2;
    }
    else
    {
        //Either we are done, or there are more frames following
        if (rcv_buf[rx_size-1]==DesFireBase::OPERATION_OK)
        {
            //we are done
            //AddFiles(rcv_buf,rx_size-2);
            buf_size=rx_size-2;
            memcpy(buf,rcv_buf,buf_size);
        }
        else if (rcv_buf[rx_size-1]==DesFireBase::ADDITIONAL_FRAME)
        {

        }
    }
    //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
    return 0;
}

}

}


