/*!
 * \file esys/nfc/desfireappbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/desfirebase.h"
#include "esys/nfc/desfireappbase.h"
#include "esys/nfc/desfirefile.h"

#include <assert.h>

namespace esys
{

DesFireAppBase::DesFireAppBase(): m_desfire(NULL), m_id(-1), m_present(false)
{
}

DesFireAppBase::~DesFireAppBase()
{
}

void DesFireAppBase::SetID(uint32_t id)
{
    m_id=id;
}

int32_t DesFireAppBase::GetID()
{
    return m_id;
}

bool DesFireAppBase::IsPresent()
{
    return m_present;
}

void DesFireAppBase::SetPresent(bool present)
{
    m_present=present;
}

/*int DesFireAppBase::GetFiles()
{
    uint8_t rcv_buf[262];
    uint32_t rx_size=262;
    int result;

    uint8_t GET_APP_IDS[5]= {0x90, 0x6F, 0x00, 0x00, 0x00 };

    Clear();

    assert(m_desfire!=NULL);

    result=m_desfire->Transmit(GET_APP_IDS,5,rcv_buf,rx_size);

    if(result<0)
    {
        return result;
    }
    if (rcv_buf[rx_size-2]!=0x91)
    {
        return -2;
    }
    else
    {
        //Either we are done, or there are more frames following
        if (rcv_buf[rx_size-1]==DesFireBase::OPERATION_OK)
        {
            //we are done
            AddFiles(rcv_buf,rx_size-2);
        }
        else if (rcv_buf[rx_size-1]==DesFireBase::ADDITIONAL_FRAME)
        {

        }
    }
    //atqa=(rcv_buf[3]<<256)+rcv_buf[4];
    return GetNbrFiles();
} */

int DesFireAppBase::Select()
{
    assert(GetDesFire()!=NULL);

    return GetDesFire()->SelectAppID(m_id);
}

/*uint16_t DesFireAppBase::GetNbrFiles()
{
    return m_files.size();
} */

/*DesFireFile *DesFireAppBase::GetFileIdx(uint16_t idx)
{
    if (idx>=GetNbrFiles())
        return NULL;
    return m_files[idx];
}

DesFireFile *DesFireAppBase::GetFile(uint8_t id)
{
    uint8_t idx;
    DesFireFile *file=NULL;

    for (idx=0; idx<m_files.size(); ++idx)
    {
        assert(m_files[idx]!=NULL);

        if (m_files[idx]->GetID()==id)
            file=m_files[idx];
    }
    if (file!=NULL)
        return file;
    file=new DesFireFile();
    file->SetID(id);
    file->SetDesFire(GetDesFire());
    m_files.push_back(file);
    return file;
} */

void DesFireAppBase::SetDesFire(DesFireBase *desfire)
{
    m_desfire=desfire;
}

DesFireBase *DesFireAppBase::GetDesFire()
{
    return m_desfire;
}

void DesFireAppBase::AddFiles(uint8_t *buf, uint16_t size)
{
    uint16_t nbr=size;
    uint16_t idx;
    uint8_t *p;
    uint8_t file_id;

    DesFireFileBase *file;

    p=buf;

    for (idx=0; idx<nbr; ++idx)
    {
        file_id=p[0];
        if (GetNbrSupportedFiles()==0)
        {
            AddFile(file_id);
        }
        else
        {
            if (IsSupportedFile(file_id, &file))
            {
                file->SetPresent();
                file->SetDesFire(GetDesFire());
            }
        }
        p+=1;
    }
}

/*void DesFireAppBase::Clear()
{
    uint16_t idx;

    for (idx=0; idx<GetNbrFiles(); ++idx)
    {
        delete m_files[idx];
    }
    m_files.clear();
} */

}

