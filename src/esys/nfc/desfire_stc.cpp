/*!
 * \file esys/nfc/desfire_stc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/desfire_stc.h"
#include "esys/nfc/desfireapp.h"
#include "esys/nfc/nfccard.h"

#include "esys/assert.h"

#include <dbg_log/dbg_class.h>

namespace esys
{

DesFire_stc::DesFire_stc(NFCCard *card): DesFireBase(card)//NFCCard(card)
{
}

DesFire_stc::~DesFire_stc()
{
    //Disconnect();
    Clear();
}

uint16_t DesFire_stc::GetNbrAppIDs()
{
    DBG_CALLMEMBER_RET("esys::DesFire_stc::GetNbrAppIDs",false, uint16_t, result);
    DBG_CALLMEMBER_END;

    result=m_apps.size();
    return result;
}

int32_t DesFire_stc::GetAppID(uint16_t idx)
{
    DBG_CALLMEMBER_RET("esys::DesFire_stc::GetAppID",false, int32_t, result);
    DBG_PARAM(0,"idx", uint16_t, idx);
    DBG_CALLMEMBER_END;

    if (idx>=m_apps.size())
    {
        result=-1;
        return result;
    }
    result=m_apps[idx]->GetID();
    return result;
}

esys::DesFireAppBase *DesFire_stc::GetAppIdx(uint16_t idx)
{
    DBG_CALLMEMBER_RET("esys::DesFire_stc::GetAppIdx",false, esys::DesFireAppBase *, result);
    DBG_PARAM(0,"idx", uint16_t, idx);
    DBG_CALLMEMBER_END;

    if (idx>=m_apps.size())
    {
        result=NULL;
        return result;
    }
    result=m_apps[idx];
    return result;
}

esys::DesFireAppBase *DesFire_stc::GetApp(uint32_t app_id)
{
    DBG_CALLMEMBER_RET("esys::DesFire_stc::GetApp",false, esys::DesFireAppBase *, app);
    DBG_PARAM(0,"app_id", uint32_t, app_id);
    DBG_CALLMEMBER_END;

    uint16_t idx;

    for (idx=0; idx<GetNbrAppIDs(); ++idx)
    {
        app=m_apps[idx];
        if (app->GetID()==app_id)
            return app;
    }
    app=NULL;
    return app;
}

void DesFire_stc::AddAppID(uint32_t app_id)
{
    esys::DesFireAppBase *app;

    app=new DesFireApp();
    app->SetPresent();
    m_apps.push_back(app);
    app->SetID(app_id);
    app->SetDesFire(this);
}

void DesFire_stc::Clear()
{
    DBG_CALLMEMBER("esys::DesFire_stc::Clear",false);
    DBG_CALLMEMBER_END;

    uint16_t idx;

    for (idx=0; idx<GetNbrAppIDs(); ++idx)
    {
        delete m_apps[idx];
    }
    m_apps.clear();
}

int DesFire_stc::AddSupportedApp(esys::DesFireAppBase *app)
{
    m_supported_apps.push_back(app);
    app->SetDesFire(this);
    return 0;
}

int32_t DesFire_stc::GetNbrSupportedApp()
{
    return m_supported_apps.size();
}

int32_t DesFire_stc::GetMaxNbrSupportedApp()
{
    return -1;
}

bool DesFire_stc::IsSupportedApp(uint32_t app_id, esys::DesFireAppBase **app)
{
    for (uint32_t idx=0; idx < m_supported_apps.size(); ++idx)
    {
        if (m_supported_apps[idx]->GetID()==app_id)
        {
            if (app!=NULL)
                *app=m_supported_apps[idx];
            return true;
        }
    }
    if (app!=NULL)
        *app=NULL;
    return false;
}

}


