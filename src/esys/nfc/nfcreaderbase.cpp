/*!
 * \file esys/nfc/nfcreaderbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/nfcreaderbase.h"

namespace esys
{

NFCReaderBase::NFCReaderBase()
{
}

NFCReaderBase::~NFCReaderBase()
{
}

}


