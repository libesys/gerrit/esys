/*!
 * \file esys/nfc/arduino/desfire_win32.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/arduino/desfire.h"

namespace esys
{

namespace ard
{

DesFire::DesFire(NFCCard *card): DesFireBase(card), DesFire_stc(card)
{
}

DesFire::~DesFire()
{
}

}

}

