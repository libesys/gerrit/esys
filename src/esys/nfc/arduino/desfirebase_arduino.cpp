/*!
 * \file esys/nfc/arduino/desfirebase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/arduino/desfirebase.h"
#include "esys/nfc/arduino/nfcreader.h"

#include <PN532.h>
#include <dbg_log/dbg_class.h>

namespace esys
{

namespace ard
{

DesFireBase::DesFireBase(NFCCard *card): esys::DesFireBase(card)
{
}

DesFireBase::~DesFireBase()
{
}

void DesFireBase::SetNFCCard(NFCCard *card)
{
    esys::DesFireBase::SetNFCCard(card);
    SetNFCReader(card->GetNFCReader());
}

//uint8_t SELECT_DESFIRE[]= { 0x40, 0x01, 0x00, 0xA4, 0x04, 0x00, 0x07, 0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01, 0x00};

int DesFireBase::GetAppIDs()
{
    DBG_CALLMEMBER_RET("esys::arduino::DesFireBase::GetAppIDs",false,int, result);
    DBG_CALLMEMBER_END;

    int16_t result16;
    uint8_t GET_APP_IDS[]= { 0x40, 0x01, 0x6A, };

    uint8_t len;

    result16=GetNFCReader()->GetPN532().sendCommandCheckAck(GET_APP_IDS, 3);

    if (result16<0)
    {
        return result16;
    }

    GetNFCReader()->GetPN532().read(m_buf, 64);

    len=m_buf[3];

    if (m_buf[6] != 0x41)
    {
        return -12;
    }

    //if (m_buf[7] != 0x00)
    //	result=-3;

    AddAppIDs(&m_buf[8], len-3);
    result=0;
    return result;
}

int DesFireBase::SelectAppID(uint32_t app_id)
{
    DBG_CALLMEMBER_RET("esys::arduino::DesFireBase::SelectAppID",false, int, result);
    DBG_PARAM(0,"app_id", uint32_t, app_id);
    DBG_CALLMEMBER_END;

    /*uint8_t SELECT_APP[9]= {0x90, 0x5A, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00 };
    SELECT_APP[5]=app_id&0xFF;
    SELECT_APP[6]=(app_id>>8)&0xFF;
    SELECT_APP[7]=(app_id>>16)&0xFF;*/

    int16_t result16;
    uint8_t SELECT_APP[]= {0x40, 0x01, 0x5A, 0x00, 0x00, 0x00 };
    SELECT_APP[3]=app_id&0xFF;
    SELECT_APP[4]=(app_id>>8)&0xFF;
    SELECT_APP[5]=(app_id>>16)&0xFF;

    uint8_t len;

    result16=GetNFCReader()->GetPN532().sendCommandCheckAck(SELECT_APP, sizeof(SELECT_APP));

    if (result16<0)
    {
        return result16;
    }

    GetNFCReader()->GetPN532().read(m_buf, 64);

    len=m_buf[3];

    if (m_buf[6] != 0x41)
    {
        return -12;
    }

    //if (m_buf[7] != 0x00)
    //	result=-3;

    result=0;
    return result;
}

}

}

