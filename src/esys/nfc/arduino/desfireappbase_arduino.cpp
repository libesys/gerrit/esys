/*!
 * \file esys/nfc/arduino/desfireappbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/desfirebase.h"
#include "esys/nfc/arduino/desfireappbase.h"
#include "esys/nfc/arduino/nfcreader.h"

#include <PN532.h>

#include "esys/assert.h"

namespace esys
{

namespace ard
{


DesFireAppBase::DesFireAppBase() : esys::DesFireAppBase()
{
}

DesFireAppBase::~DesFireAppBase()
{
}

int DesFireAppBase::GetFiles()
{
    int result;
    int16_t result16;
    uint8_t GET_FILES[]= { 0x40, 0x01, 0x6F, };

    uint8_t len;

    result16=GetDesFire()->GetNFCReader()->GetPN532().sendCommandCheckAck(GET_FILES, 3);

    if (result16<0)
    {
        return result16;
    }

    GetDesFire()->GetNFCReader()->GetPN532().read(m_buf, 64);

    len=m_buf[3];

    if (m_buf[6] != 0x41)
    {
        return -12;
    }

    //if (m_buf[7] != 0x00)
    //	result=-3;

    AddFiles(&m_buf[8], len-3);
    result=0;
    return result;
}

}

}

