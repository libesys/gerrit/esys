/*!
 * \file esys/nfc/arduino/nfcmngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/arduino/nfcmngr.h"
#include "esys/nfc/arduino/nfcreader.h"

#include <stddef.h>     //Needed for NULL under GCC

namespace esys
{

NFCMngr NFCMngr::m_mngr;

NFCMngr &NFCMngr::Get()
{
    return m_mngr;
}

NFCMngr::NFCMngr(): m_init(false), m_released(true)
{
    m_reader.SetMngr(this);
}

NFCMngr::~NFCMngr()
{
    Clear();
}

int NFCMngr::Init()
{
    return 0;
}

unsigned int NFCMngr::GetNbrReaders()
{
    return 1;
}

NFCReader *NFCMngr::GetReader(unsigned int idx)
{
    return &m_reader;
}

void NFCMngr::Clear()
{
}

}


