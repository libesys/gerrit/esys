/*!
 * \file esys/nfc/arduino/desfirefile.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/nfc/arduino/desfirefile.h"
#include "esys/nfc/desfirebase.h"
#include "esys/nfc/arduino/nfcreader.h"

#include "esys/assert.h"

namespace esys
{

namespace ard
{

DesFireFile::DesFireFile(): DesFireFileBase()
{
}

DesFireFile::~DesFireFile()
{
}

int DesFireFile::GetData(uint32_t offset, uint8_t *buf, uint16_t &buf_size)
{
    int16_t result16;
    int result;
    uint8_t len;
    //size is 8 without frame wrapping
    //                         Cmd   P1    P2    Size
    /*uint8_t GET_DATA[13]= {0x90, 0xBD, 0x00, 0x00, 0x07,
                           0x00, // File Id
                           0x00, 0x00, 0x00, // Offset
                           0x00, 0x00, 0x00, // Length
                           0x00
                          }; // Len */

    /*GET_DATA[5]=(uint8_t)m_id;
    GET_DATA[6]=offset&0xFF;
    GET_DATA[7]=(offset>>8)&0xFF;
    GET_DATA[8]=(offset>>16)&0xFF; */
    //GET_DATA[9]=buf_size&0xFF;
    //GET_DATA[10]=(buf_size>>8)&0xFF;
    //GET_DATA[11]=0;
    //Clear();

    uint8_t GET_DATA[]= { 0x40, 0x01, 0xBD,
                          0x00, // File Id
                          0x00, 0x00, 0x00, // Offset
                          0x00, 0x00, 0x00, // Length
                        }; // Len */

    GET_DATA[3]=(uint8_t)m_id;
    GET_DATA[4]=offset&0xFF;
    GET_DATA[5]=(offset>>8)&0xFF;
    GET_DATA[6]=(offset>>16)&0xFF;

    if (GetDesFire()==NULL)
        return -17;
    if (GetDesFire()->GetNFCReader()==NULL)
        return -18;

    result16=GetDesFire()->GetNFCReader()->GetPN532().sendCommandCheckAck(GET_DATA, sizeof(GET_DATA));

    if (result16<0)
    {
        return result16;
    }

    GetDesFire()->GetNFCReader()->GetPN532().read(m_buf, 64);

    len=m_buf[3];

    if (m_buf[6] != 0x41)
    {
        return -12;
    }
    memcpy(buf, &m_buf[10], len-6);
    buf_size=len-6;
    return 0;
}

}

}


