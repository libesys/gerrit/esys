/*!
 * \file esys/nfc/arduino/nfcreader.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/arduino/nfcreader.h"
#include "esys/nfc/arduino/nfcmngr.h"
#include "esys/nfc/arduino/nfccard.h"

#include <stddef.h>     //Needed for NULL under GCC

//#include "assert.h"

//#include <iostream>

namespace esys
{

NFCReader::NFCReader(const char *name):
    /*m_name(name),*/ m_mngr(NULL), m_connected(false), m_nfc(PN532_CS), m_card(NULL)
{
    m_card.SetNFCReader(this);
}

NFCReader::~NFCReader()
{
    Clear();
}

int NFCReader::Init()
{
    uint32_t versiondata;
    bool result;

    m_nfc.begin();
    versiondata=m_nfc.getFirmwareVersion();
    if (!versiondata)
        return -1;
    result=m_nfc.SAMConfig();
    if (!result)
        return -2;
    return 0;
}

PN532 &NFCReader::GetPN532()
{
    return m_nfc;
}

/*void NFCReader::SetName(const std::wstring &name)
{
    m_name=name;
    m_reader_states[0].szReader = name.c_str();
}

std::wstring &NFCReader::GetName()
{
    return m_name;
} */

void NFCReader::SetMngr(NFCMngr *mngr)
{
    m_mngr=mngr;
}

NFCMngr *NFCReader::GetMngr()
{
    return m_mngr;
}

int NFCReader::Connect()
{
    /*LONG retCode;

    assert(GetMngr()!=NULL);
    assert(m_name.empty()==false);

    retCode = SCardConnect( GetMngr()->GetContext(),
                            m_name.c_str(),
                            SCARD_SHARE_SHARED,
                            SCARD_PROTOCOL_T0|SCARD_PROTOCOL_T1,
                            &m_card_hnd,
                            &m_act_protocol);

    if (retCode!=SCARD_S_SUCCESS)
    {
        return -1;
    }
    m_connected=true; */

    return 0;
}

bool NFCReader::IsConnected()
{
    return m_connected;
}

int NFCReader::Transmit(uint8_t *tx_buf, uint32_t tx_size,uint8_t *rx_buf, uint32_t &rx_size)
{
    return 0;
}

unsigned int NFCReader::GetNbrCards()
{
    return 0;
}

NFCCard *NFCReader::GetCard(unsigned int idx)
{
    if (idx>0)
        return NULL;
    return &m_card;
}

void NFCReader::Clear()
{
    /*unsigned int idx;
    NFCCard *card;

    for (idx=0; idx<GetNbrCards(); ++idx)
    {
        card=m_cards[idx];

        if (card!=NULL)
            delete card;
    }
    m_cards.clear(); */
}

int NFCReader::RefreshCards()
{
    m_card.SetId(0);
    return 0;
}

NFCCard *NFCReader::ConnectCard()
{
    uint32_t id;

    /*id = m_nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A);
    if (id==0)
    {
    	m_card.SetId(0);
    	return NULL;
    } */
    m_nfc.GetGeneralStatus();
    m_nfc.Select();
    m_nfc.GetGeneralStatus();

    m_card.SetId(id);
    return &m_card;
}

int NFCReader::WaitNotification(int32_t timeout, int32_t &size, uint8_t *atr)
{
    /*LONG result;
    //SCARD_READERSTATE reader_states[1];

    assert(m_mngr!=NULL);

    result=SCardGetStatusChange(
               m_mngr->GetContext(),
               timeout,
               m_reader_states,
               1);

    if (result != SCARD_S_SUCCESS)
        return -1;
    size=m_reader_states[0].cbAtr;
    memcpy(atr,m_reader_states[0].rgbAtr,size);

    if (HasCard())
        m_reader_states[0].dwCurrentState=SCARD_STATE_PRESENT;
    else
        m_reader_states[0].dwCurrentState=SCARD_STATE_EMPTY;
    m_reader_states[0].dwCurrentState=m_reader_states[0].dwEventState; */
    return 0;
}

bool NFCReader::HasCard()
{
    uint32_t id;

    id = m_nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A);
    if (id!=0)
        return true;
    return false;
}

}


