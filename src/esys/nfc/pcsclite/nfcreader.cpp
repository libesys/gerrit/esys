/*!
 * \file esys/nfc/pcsclite/nfcreader.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/nfc/pcsclite/nfcreader.h"
#include "esys/nfc/pcsclite/nfcmngr.h"
#include "esys/nfc/pcsclite/nfccard.h"

#include <assert.h>
#include <cstring>
#include <iostream>

namespace esys
{

NFCReader::NFCReader(const std::string &name): m_name(name), m_mngr(NULL), m_connected(false)
{
    memset(&m_reader_states[0],0,sizeof(SCARD_READERSTATE));
    if (!name.empty())
        m_reader_states[0].szReader = m_name.c_str();
    m_reader_states[0].dwCurrentState = SCARD_STATE_UNAWARE;
}

NFCReader::~NFCReader()
{
    Clear();
}

void NFCReader::SetName(const std::string &name)
{
    m_name=name;
    m_reader_states[0].szReader = name.c_str();
}

std::string &NFCReader::GetName()
{
    return m_name;
}

void NFCReader::SetMngr(NFCMngr *mngr)
{
    m_mngr=mngr;
}

NFCMngr *NFCReader::GetMngr()
{
    return m_mngr;
}

int NFCReader::Connect()
{
    LONG retCode;

    assert(GetMngr()!=NULL);
    assert(m_name.empty()==false);

    retCode = SCardConnect( GetMngr()->GetContext(),
                            m_name.c_str(),
                            SCARD_SHARE_SHARED,
                            SCARD_PROTOCOL_T0|SCARD_PROTOCOL_T1,
                            &m_card_hnd,
                            &m_act_protocol);

    if (retCode!=SCARD_S_SUCCESS)
    {
        return -1;
    }
    m_connected=true;

    return 0;
}

bool NFCReader::IsConnected()
{
    return m_connected;
}

int NFCReader::Transmit(uint8_t *tx_buf, uint32_t tx_size,uint8_t *rx_buf, uint32_t &rx_size)
{
    LONG retCode;
    DWORD rx_buf_size=rx_size;

    rx_size=0;

    retCode = SCardTransmit(m_card_hnd,
                            NULL,
                            tx_buf,
                            tx_size,
                            NULL,
                            rx_buf,
                            &rx_buf_size);

    if( retCode != SCARD_S_SUCCESS )
    {
        return -1;
    }

    rx_size=rx_buf_size;

    return 0;
}

unsigned int NFCReader::GetNbrCards()
{
    return m_cards.size();
}

NFCCard *NFCReader::GetCard(unsigned int idx)
{
    if (idx>=GetNbrCards())
        return NULL;
    return m_cards[idx];
}

void NFCReader::Clear()
{
    unsigned int idx;
    NFCCard *card;

    for (idx=0; idx<GetNbrCards(); ++idx)
    {
        card=m_cards[idx];

        if (card!=NULL)
            delete card;
    }
    m_cards.clear();
}

int NFCReader::RefreshCards()
{
    assert(m_mngr!=NULL);

    Clear();

    return 0;
}

NFCCard *NFCReader::ConnectCard()
{
    SCARDHANDLE     hCardHandle;
    LONG            lReturn;
    DWORD           dwAP;

    assert(m_mngr!=NULL);

    lReturn = SCardConnect( m_mngr->GetContext(),
                            m_name.c_str(),
                            SCARD_SHARE_SHARED,
                            SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
                            &hCardHandle,
                            &dwAP );
    if ( SCARD_S_SUCCESS != lReturn )
    {
        //printf("Failed SCardConnect\n");
        //exit(1);  // Or other appropriate action.
        return NULL;
    }

    NFCCard *card;

    Clear();

    card=new NFCCard();
    m_cards.push_back(card);
    card->SetHandle(hCardHandle);

    // Use the connection.
    // Display the active protocol.
    switch ( dwAP )
    {
        case SCARD_PROTOCOL_T0:
            //std::wcout << L"Active protocol T0" << std::endl;
            break;

        case SCARD_PROTOCOL_T1:
            //std::wcout << L"Active protocol T1" << std::endl;
            break;

        case SCARD_PROTOCOL_UNDEFINED:
        default:
            std::wcout << L"Active protocol unnegotiated or unknown" << std::endl;
            break;
    }

    char            szReader[200];
    DWORD           cch = 200;
    BYTE            bAttr[32];
    DWORD           cByte = 32;
    DWORD           dwState, dwProtocol;
    //LONG            lReturn;

    // Determine the status.
    // hCardHandle was set by an earlier call to SCardConnect.
    lReturn = SCardStatus(hCardHandle,
                          szReader,
                          &cch,
                          &dwState,
                          &dwProtocol,
                          (LPBYTE)&bAttr,
                          &cByte);


    return card;
}

int NFCReader::WaitNotification(int32_t timeout, int32_t &size, uint8_t *atr)
{
    LONG result;
    //SCARD_READERSTATE reader_states[1];

    assert(m_mngr!=NULL);

    result=SCardGetStatusChange(
               m_mngr->GetContext(),
               timeout,
               m_reader_states,
               1);

    if (result != SCARD_S_SUCCESS)
        return -1;
    size=m_reader_states[0].cbAtr;
    memcpy(atr,m_reader_states[0].rgbAtr,size);

    if (HasCard())
        m_reader_states[0].dwCurrentState=SCARD_STATE_PRESENT;
    else
        m_reader_states[0].dwCurrentState=SCARD_STATE_EMPTY;
    m_reader_states[0].dwCurrentState=m_reader_states[0].dwEventState;
    return 0;
}

bool NFCReader::HasCard()
{
    return ((m_reader_states[0].dwEventState&SCARD_STATE_PRESENT)==SCARD_STATE_PRESENT);
}

}


