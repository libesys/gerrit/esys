/*!
 * \file esys/ringbuffer.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/ringbuffer.h"

namespace esys
{

RingBufferBase::RingBufferBase(): m_nbr_el(0), m_read_idx(-1), m_write_idx(-1)
{
}

RingBufferBase::~RingBufferBase()
{
}

}
