/*!
 * \file esys/i2cbusmngrbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/i2cbusmngrbase.h"

namespace esys
{

I2CBusMngrBase::I2CBusMngrBase(): BusMngrBase_t<I2CBusBase>()
{
}

I2CBusMngrBase::~I2CBusMngrBase()
{
}

}
