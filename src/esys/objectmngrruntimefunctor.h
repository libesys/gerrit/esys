/*!
 * \file esys/objectmngrruntimefunctor.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __ESYS_OBJECTMNGRRUNTIMEFUNCTOR_H__
#define __ESYS_OBJECTMNGRRUNTIMEFUNCTOR_H__

#include "esys/esys_defs.h"
#include "esys/inttypes.h"
#include "esys/objectmngr.h"
#include "esys/runtimelevel.h"

namespace esys
{

class RuntimeFunctor : public ObjectMngr::FunctorType
{
public:
    typedef int32_t(Object::*Function)(RuntimeLevel runtime_lvl, Object::Order order);

    RuntimeFunctor(Function fct, RuntimeLevel runtime_lvl = RuntimeLevel::NOT_SET)
        : m_fct(fct), m_runtime_lvl(runtime_lvl)
    {
    }

    void SetRuntimeLevel(RuntimeLevel runtime_lvl)
    {
        m_runtime_lvl = runtime_lvl;
    }

    virtual int32_t CallFct(Object *obj, Object::Order order) override
    {
        assert(obj != nullptr);
        assert(m_fct != nullptr);

        return (obj->*m_fct)(m_runtime_lvl, order);
    }
protected:
    Function m_fct;
    RuntimeLevel m_runtime_lvl;
};

}

#endif

