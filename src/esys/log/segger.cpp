/*!
 * \file esys/log/segger.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/log/segger.h"
#include "esys/assert.h"

#ifdef ESYS_HW
extern "C"
{
#include <SEGGER_RTT_Conf.h>
#include <SEGGER_RTT.h>
}
#else
#include <iostream>
#endif

namespace esys
{

namespace log
{

#ifdef ESYS_VHW
bool Segger::m_write_buf_info = false;

void Segger::SetWriteBufInfo(bool write_buf_info)
{
    m_write_buf_info = write_buf_info;
}

#endif

Segger::Segger(unsigned buffer_index)
    : LogChannel(), m_buffer_index(buffer_index)
{
#ifdef ESYS_HW
    assert(buffer_index < SEGGER_RTT_MAX_NUM_UP_BUFFERS);
#else
#endif
}

Segger::~Segger()
{
}

void Segger::PutStr(const char *buf)
{
#ifdef ESYS_HW
    //debug_rtt(buf);
    SEGGER_RTT_WriteString(m_buffer_index, buf);
#else
    if (m_write_buf_info)
        std::cout << "[SEGGER" << m_buffer_index << "]";
    std::cout << buf;
#endif
}

void Segger::PutStr(char *buf, uint16_t size)
{
#ifdef ESYS_HW
    SEGGER_RTT_Write(m_buffer_index, buf, size);
#else
    if (m_write_buf_info)
        std::cout << "[SEGGER" << m_buffer_index << "]";
    std::cout.write(buf, size);
#endif
}

void Segger::Put(uint8_t *buf, uint16_t size)
{
#ifdef ESYS_HW
    SEGGER_RTT_Write(m_buffer_index, buf, size);
#else
    if (m_write_buf_info)
        std::cout << "[SEGGER" << m_buffer_index << "]";
    std::cout.write((char *)buf, size);
#endif
}

void Segger::Flush()
{
#ifdef ESYS_HW

#else
    std::cout.flush();
#endif
}

void Segger::SetBuffer(SeggerBufferBase *buf, int buffer_index)
{
    // Segger library already allocate the buffer for Segger channel #0
#ifdef ESYS_HW
    if (buffer_index!=-1)
        m_buffer_index=buffer_index;

    assert(m_buffer_index < SEGGER_RTT_MAX_NUM_UP_BUFFERS);
#else
#endif
    assert(m_buffer_index != 0);
    assert(buf != nullptr);

#ifdef ESYS_HW
    int result;

    result = SEGGER_RTT_ConfigUpBuffer(m_buffer_index, buf->GetName(), buf->GetUp(), buf->GetSizeUp(), SEGGER_RTT_MODE_DEFAULT);
    assert(result >= 0);
    result = SEGGER_RTT_ConfigDownBuffer(m_buffer_index, buf->GetName(), buf->GetDown(), buf->GetSizeDown(), SEGGER_RTT_MODE_DEFAULT);
    assert(result >= 0);
#else
#endif
}

}

}


