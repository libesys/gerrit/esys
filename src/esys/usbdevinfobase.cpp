/*!
 * \file esys/usbdevinfobase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/usbdevinfobase.h"
#include "esys/usbenumbase.h"

#include <cassert>

//#include <Arduino.h>

namespace esys
{

uint32_t USBDevInfoBase::m_next_id=0;

USBDevInfoBase::USBDevInfoBase(Kind kind)
    : Connection(), m_usb_addr(), m_kind(kind), m_valid(false), m_ref_count(0)
    , m_usb_enum(NULL)
{
    m_id=m_next_id++;
}

USBDevInfoBase::~USBDevInfoBase()
{
}

uint32_t USBDevInfoBase::GetId()
{
    return m_id;
}

USBDevInfoBase::Kind USBDevInfoBase::GetKind()
{
    return m_kind;
}

USBAddr &USBDevInfoBase::GetUSBAddr()
{
    return m_usb_addr;
}

bool USBDevInfoBase::IsValid()
{
    return m_valid;
}

void USBDevInfoBase::SetValid(bool valid)
{
    m_valid=valid;
}

void USBDevInfoBase::SetUSBEnum(USBEnumBase *usb_enum)
{
    m_usb_enum=usb_enum;
}

void USBDevInfoBase::Use()
{
    m_ref_count++;
}

void USBDevInfoBase::Release()
{
    if (m_ref_count>0)
        m_ref_count--;
    if (m_ref_count==0)
    {
        assert(m_usb_enum!=NULL);
        m_usb_enum->Released(this);
    }
}

int32_t USBDevInfoBase::GetRefCount()
{
    return m_ref_count;
}

}
