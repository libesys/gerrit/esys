/*!
 * \file esys/systembase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/systembase.h"
#include "esys/tasklist.h"
#include "esys/modulelist.h"

#include "esys/assert.h"

#ifdef ESYS_HW
extern "C" void __cxa_pure_virtual()
{
    while (1);
}

namespace __gnu_cxx
{
void __verbose_terminate_handler()
{
    for (;;)
        ;
}
}

#endif

namespace esys
{

SystemBase::SystemBase(const ObjectName &name) : ObjectMngr(name), m_expected_module_id(0)
{
}

SystemBase::~SystemBase()
{
}

int32_t SystemBase::Init()
{
    int32_t result;
    int32_t l_result;
    Module *module;

    result = PlatInit();

    // Initialize the whole system
    result = StaticInit();

    // Initialize the modules if any
    module = ModuleList::GetCurrent().GetFirst();
    while (module != nullptr)
    {
        l_result = module->StaticInit();
        if ((result == 0) && (l_result < 0))
            result = l_result;
        module = module->GetNext();
    }
    return result;
}

int32_t SystemBase::Release()
{
    int32_t result = 0;
    int32_t l_result;
    Module *module;

    module = ModuleList::GetCurrent().GetLast();
    while (module != nullptr)
    {
        l_result = module->StaticRelease();
        if ((result == 0) && (l_result < 0))
            result = l_result;
        module = module->GetPrev();
    }

    result = StaticRelease();

    result = PlatRelease();

    /* Clearing the list is necessary to be able to have several unit tests per flashed firmware.
        In this way, the list is empty before starting a unit test.
    */
    TaskList::GetCurrent().Reset();
    return result;
}

void SystemBase::StartInit(Module *module)
{
    assert(module->GetOrderId() == (esys::int32_t)m_expected_module_id);

    ++m_expected_module_id;
}

}




