/*!
 * \file esys/systemtask.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/systemtask.h"
#include "esys/esys.h"
#include "esys/systembase.h"
#include "esys/taskmngrbase.h"
#include "esys/modulelist.h"

#include "esys/assert.h"

namespace esys
{

#ifndef ESYS_MULTI_SYS
SystemTask *SystemTask::m_current = nullptr;

SystemTask &SystemTask::GetCurrent()
{
    assert(m_current != nullptr);

    return *m_current;
}

void SystemTask::SetCurrent(SystemTask *current)
{
    m_current = current;
}

#else
SystemTask *SystemTask::GetCurrent()
{
    return m_current;
}

void SystemTask::SetCurrent(SystemTask *current)
{
    m_current = current;
}
#endif

SystemTask::SystemTask(const ObjectName &name) : Task(name, TaskType::INTERNAL)
{
    SetCurrent(this);

    SetRuntimeLevel(RuntimeLevel::ROOT);
}

SystemTask::~SystemTask()
{
}

int32_t SystemTask::Entry()
{
    SystemBase *system;
    TaskMngrBase *taskmngr_base;
    int32_t result;
    int32_t l_result;
    Module *module;

    system = ESys::Get().GetSystemBase();
    assert(system != nullptr);

    result = system->RuntimeInit();

    module = ModuleList::GetCurrent().GetFirst();
    while (module != nullptr)
    {
        l_result = module->RuntimeInit();
        if ((result == 0) && (l_result < 0))
            result = l_result;
        module = module->GetNext();
    }

    taskmngr_base = GetMngrBase(); 		//ESys::Get().GetTaskMngrBase();
    assert(taskmngr_base != nullptr);

    result = taskmngr_base->RuntimeInit();

    while (GetState() != TaskBase::State::STOPPING)
    {
        result = ExecLoop();
        m_sem.Wait();
    }

    // Makes sure all Task, except the System Task are stopped
    result = GetMngrBase()->StopAll(TaskMngrBase::Tasks::ALL_NOT_SYSTEM);

    result = taskmngr_base->RuntimeRelease();

    module = ModuleList::GetCurrent().GetLast();
    while (module != nullptr)
    {
        l_result = module->RuntimeRelease();
        if ((result == 0) && (l_result < 0))
            result = l_result;
        module = module->GetPrev();
    }

    result = system->RuntimeRelease();

    GetMngrBase()->ExitScheduler();

    return result;
}

int32_t SystemTask::ExecLoop()
{
    return 0;
}

void SystemTask::AllAppTasksDone()
{
    SetState(State::STOPPING);
    m_sem.Post();
}

}



