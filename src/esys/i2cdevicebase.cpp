/*!
 * \file esys/i2cdevicebase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/i2cdevicebase.h"
#include "esys/i2cbusbase.h"

#include "esys/assert.h"

namespace esys
{

I2CDeviceBase::I2CDeviceBase(int16_t addr): m_addr(addr), m_sub_addr(0), m_bus(NULL)
{
}

int16_t I2CDeviceBase::GetAddr()
{
    return m_addr;
}

void I2CDeviceBase::SetAddr(int16_t addr)
{
    m_addr=addr;
}

uint8_t I2CDeviceBase::GetSubAddr()
{
    return m_sub_addr;
}

void I2CDeviceBase::SetSubAddr(uint8_t sub_addr)
{
    m_sub_addr=sub_addr;
}

I2CBusBase *I2CDeviceBase::GetBus()
{
    return m_bus;
}

void I2CDeviceBase::SetBus(I2CBusBase *bus)
{
    m_bus=bus;
}

// API corresponding to I2CHW_if
int16_t I2CDeviceBase::Start()
{
    assert(m_bus!=NULL);

    return m_bus->Start();
}

int16_t I2CDeviceBase::Stop()
{
    assert(m_bus!=NULL);

    return m_bus->Stop();
}

int16_t I2CDeviceBase::Read(uint8_t &value)
{
    assert(m_bus!=NULL);

    return m_bus->Read(this, value);
}

int16_t I2CDeviceBase::Write(uint8_t value)
{
    assert(m_bus!=NULL);

    return m_bus->Write(this, value);
}

int16_t I2CDeviceBase::Read(uint8_t *buf, uint16_t buf_len, bool repeat)
{
    assert(m_bus!=NULL);

    return m_bus->Read(this, buf, buf_len, repeat);
}

int16_t I2CDeviceBase::Write(uint8_t *buf, uint16_t buf_len, bool repeat)
{
    assert(m_bus!=NULL);

    return m_bus->Write(this, buf, buf_len, repeat);
}

int16_t I2CDeviceBase::Transfer(uint8_t *tx_buf, uint16_t tx_buf_len, uint8_t *rx_buf, uint16_t rx_buf_len, bool repeat)
{
    assert(m_bus!=NULL);

    return m_bus->Transfer(this, tx_buf, tx_buf_len, rx_buf, rx_buf_len, repeat);
}

}



