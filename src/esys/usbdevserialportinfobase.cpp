/*!
 * \file esys/usbdevserialportinfobase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */
#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/usbdevserialportinfobase.h"
#include "esys/serialportimpl.h"
//#include <Arduino.h>

namespace esys
{

USBDevSerialPortInfoBase::USBDevSerialPortInfoBase()
    : USBDevInfoBase(USBDevInfoBase::SERIALPORT), m_serial_impl(NULL)
{
}

USBDevSerialPortInfoBase::~USBDevSerialPortInfoBase()
{
}

void USBDevSerialPortInfoBase::Removed()
{
    if (m_serial_impl!=NULL)
    {
        m_serial_impl->Removed();
    }
}

void USBDevSerialPortInfoBase::SetSerialPortImpl(esys::SerialPortImpl *impl)
{
    m_serial_impl=impl;
}


}
