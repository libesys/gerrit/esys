/*!
 * \file esys/sysc/i2chw_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/sysc/i2chw.h"

namespace esys
{

namespace sysc
{

I2CHW::I2CHW(): I2CHWBase()
{
}

I2CHW::~I2CHW()
{
}

int16_t I2CHW::Start()
{
    return -1;
}

int16_t I2CHW::Stop()
{
    return -1;
}

int16_t I2CHW::Read(I2CDeviceBase *dev, uint8_t &value)
{
    return -1;
}

int16_t I2CHW::Write(I2CDeviceBase *dev, uint8_t value)
{
    return -1;
}

int16_t I2CHW::Read(I2CDeviceBase *dev, uint8_t *buf, uint16_t buf_len, bool repeat)
{
    return -1;
}

int16_t I2CHW::Write(I2CDeviceBase *dev, uint8_t *buf, uint16_t buf_len, bool repeat)
{
    return -1;
}

int16_t I2CHW::Transfer(I2CDeviceBase *dev, uint8_t *tx_buf, uint16_t tx_buf_len, uint8_t *rx_buf, uint16_t rx_buf_len, bool repeat)
{
    return -1;
}

}

}



