/*!
 * \file esys/sysc/timerimpl.h
 * \brief The SystemC declaration of the Timer
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys_sysc/esys_sysc_defs.h"
#include "esys/timerbase.h"
#include "esys/mutex.h"

#include <systemc.h>

namespace esys
{

namespace sysc
{

class ESYS_SYSC_API Timer;

/*! \class TimerImpl esys/sysc/timerimpl.h "esys/sysc/timerimpl.h"
 *  \brief SystemC PIMPL class for the Timer
 */
class ESYS_SYSC_API TimerImpl: public sc_module// : public TimerBase
{
public:
    SC_HAS_PROCESS(TimerImpl);

    TimerImpl(const char *name);
    virtual ~TimerImpl();

    //! Start the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Start(uint32_t period_ms, bool one_shot = false);

    //! Stop the Timer
    /*! \return 0 if successful, <0 otherwise
    */
    virtual int32_t Stop();

    //! Return the Timer period in ms
    /*! \return the period in ms
    */
    virtual int32_t GetPeriod();

    //! Return the state of the Timer
    /*! \return True if the Timer is running, false otherwise
    */
    virtual bool IsRunning();
    virtual bool IsOneShot();

    void SetTimer(Timer *timer);
    Timer *GetTimer();

    void Main();
protected:
    const char *m_name;
    uint32_t m_period_ms;
    Timer *m_timer;
    bool m_one_shot;
    bool m_is_running;
    sc_event m_timer_evt;
    Mutex m_mutex;
};

}

}






