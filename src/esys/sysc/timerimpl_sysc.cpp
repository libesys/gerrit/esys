/*!
 * \file esys/sysc/timerimpl_sysc.cpp
 * \brief The SystemC PIMPL implementation of the Timer
 *
 * \cond
 * __legal_b__
 *
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/timerimpl.h"
#include "esys/sysc/timer.h"
#include "esys/timercallbackbase.h"
#include "esys/mutex.h"
#include "esys/assert.h"

namespace esys
{

namespace sysc
{

TimerImpl::TimerImpl(const char *name)
    : sc_module(sc_module_name(name)), m_name(name), m_period_ms(0), m_timer(nullptr), m_one_shot(false),
      m_is_running(false), m_timer_evt("timer_evt"), m_mutex("Mutex")
{
    SC_THREAD(Main);
}

TimerImpl::~TimerImpl()
{
}

int32_t TimerImpl::Start(uint32_t period_ms, bool one_shot)
{
    if (period_ms == 0)
        return -1;

    m_mutex.Lock();

    m_period_ms = period_ms;
    m_one_shot = one_shot;

    m_timer_evt.notify((double)period_ms, SC_MS);
    m_is_running = true;

    m_mutex.UnLock();
    return 0;
}

int32_t TimerImpl::Stop()
{
    m_mutex.Lock();
    m_is_running = false;
    m_timer_evt.cancel();
    m_mutex.UnLock();
    return 0;
}

int32_t TimerImpl::GetPeriod()
{
    return m_period_ms;
}

bool TimerImpl::IsRunning()
{
    return m_is_running;
}

bool TimerImpl::IsOneShot()
{
    return m_one_shot;
}

void TimerImpl::SetTimer(Timer *timer)
{
    m_timer = timer;
}

Timer *TimerImpl::GetTimer()
{
    return m_timer;
}

void TimerImpl::Main()
{
    assert(m_timer != nullptr);
    assert(m_timer->GetCallback());

    while (true)
    {
        wait(m_timer_evt);

        if (!m_is_running)
            continue;

        // To get accurate timing notify immediately.
        // If callback is too slow, you are out of luck.
        m_mutex.Lock();
        if (m_one_shot==false && m_is_running)
            m_timer_evt.notify((double)m_period_ms, SC_MS);
        else if (m_one_shot)
            m_is_running = false;
        m_mutex.UnLock();

        if (m_timer->GetCallback() != nullptr)
        {
            m_timer->GetCallback()->SetTimer(m_timer);
            m_timer->GetCallback()->Callback();
        }
    }
}

}

}






