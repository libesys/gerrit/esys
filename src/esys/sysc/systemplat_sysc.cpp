/*!
 * \file esys/sysc/systemplat_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/systemplat.h"

#include <systemc.h>
#include <sysc/sc_simulation.h>

namespace esys
{

namespace sysc
{


SystemPlat::SystemPlat(SystemBase *system_base) : m_system_base(system_base)
{

}

SystemPlat::~SystemPlat()
{
}

int32_t SystemPlat::PlatInit()
{
    return (int32_t)sc_simulation::init();
}

int32_t SystemPlat::PlatRelease()
{
    return 0; // (int32_t)SystemC::CleanUp();
}

SystemBase *SystemPlat::GetSystemBase()
{
    return m_system_base;
}

void SystemPlat::SetSystemBase(SystemBase *system_base)
{
    m_system_base = system_base;
}

}

}






