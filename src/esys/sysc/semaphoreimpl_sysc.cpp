/*!
 * \file esys/sysc/semaphoreimpl_sysc.cpp
 * \brief Declaration of the SystemC Semaphore class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/semaphoreimpl.h"

namespace esys
{

namespace sysc
{

SemaphoreImpl::SemaphoreImpl(const char *name, uint32_t  count)
    : m_sem(sc_core::sc_gen_unique_name(name), count)
{
}

SemaphoreImpl::~SemaphoreImpl()
{
}

int32_t SemaphoreImpl::Post(bool from_isr)
{
    int result;

    result = m_sem.post();
    return result;
}

int32_t SemaphoreImpl::Wait(bool from_isr)
{
    int result;

    result = m_sem.wait();
    return result;
}

int32_t SemaphoreImpl::WaitFor(uint32_t ms, bool from_isr)
{
    int32_t result = TryWait();
    unsigned int count = ms;

    while ((result == -1) && (count > 0))
    {
        wait(1, SC_MS);
        --count;

        result = TryWait();
    }

    if (count == 0)
        return -1;
    return result;
}

int32_t SemaphoreImpl::TryWait(bool from_isr)
{
    int32_t result;

    result = m_sem.trywait();
    return result;
}

uint32_t SemaphoreImpl::Count()
{
    return m_sem.get_value();
}

}

}


