/*!
 * \file esys/sysc/taskmngrplatimpl_sysc.cpp
 * \brief Definition of the PIMPL class of the SystemC TaskMngrPlat
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/taskmngrplatimpl.h"

namespace esys
{

namespace sysc
{

TaskMngrPlatImpl::TaskMngrPlatImpl(TaskMngrBase *task_mngr_base)
    : m_taskmngr_base(task_mngr_base)
{
}

TaskMngrPlatImpl::~TaskMngrPlatImpl()
{
}

int TaskMngrPlatImpl::main()
{
    sc_start();

    return 0;
}

int32_t TaskMngrPlatImpl::Init()
{
    return 0;
}

void TaskMngrPlatImpl::Done(TaskBase *task_base)
{
}

int32_t TaskMngrPlatImpl::StartScheduler()
{
    sc_start();

    return 0;
}

void TaskMngrPlatImpl::ExitScheduler()
{
}

}

}








