/*!
 * \file esys/sysc/taskplatimpl_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/assert.h"

#include "esys/sysc/taskplatimpl.h"

#include <dbg_log/dbg_class.h>

#include <systemc.h>
#include <sysc/kernel/sc_spawn.h>
#include <functional>

namespace esys
{

namespace sysc
{

TaskPlatImpl::TaskPlatImpl(const sc_module_name &name, TaskBase *task_base)
    : sc_module(sc_module_name(sc_core::sc_gen_unique_name(name)))
    , m_task_base(task_base), m_name(name)
{
    sc_core::sc_spawn(std::bind(&TaskPlatImpl::CallEntry, this), sc_core::sc_gen_unique_name("CallEntry"));
}

TaskPlatImpl::~TaskPlatImpl()
{
}

void TaskPlatImpl::CallEntry()
{
    assert(m_task_base != nullptr);

    m_task_base->WaitRuntimeInitDone();

    m_task_base->Started();
    m_task_base->Entry();
    m_task_base->Done();
}

int32_t TaskPlatImpl::Create()
{
    return 0;
}

int32_t TaskPlatImpl::Start()
{
    return 0;
}

int32_t TaskPlatImpl::Stop()
{
    return 0;
}

int32_t TaskPlatImpl::Kill()
{
    return -1;
}

int32_t TaskPlatImpl::Destroy()
{
    return 0;
}

millis_t TaskPlatImpl::Millis()
{
    double millis = sc_time_stamp().to_seconds() * 1000.0;

    return (uint32_t)millis;
}

void TaskPlatImpl::Sleep(millis_t ms)
{
    wait(sc_time((double)ms, SC_MS));
}

void TaskPlatImpl::SleepU(micros_t us)
{
    wait(sc_time((double)us, SC_US));
}

void TaskPlatImpl::SetTaskBase(TaskBase *task_base)
{
    m_task_base = task_base;
}

}

}





