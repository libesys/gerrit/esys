/*!
 * \file esys/sysc/mutex_sysc.cpp
 * \brief Declaration of the SystemC Mutex class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/mutex.h"
#include "esys/sysc/muteximpl.h"

namespace esys
{

namespace sysc
{

Mutex::Mutex(const esys::ObjectName &name, Type type) : esys::MutexBase(name, type)
{
    m_impl = new MutexImpl(name, type);
}

Mutex::~Mutex()
{
    delete m_impl;
}


int32_t Mutex::Lock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Lock(from_isr);
}

int32_t Mutex::UnLock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->UnLock(from_isr);
}

int32_t Mutex::TryLock(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->TryLock(from_isr);
}

}

}








