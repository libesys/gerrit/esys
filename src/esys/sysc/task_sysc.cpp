/*!
 * \file esys/sysc/task_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/task.h"

namespace esys
{

namespace sysc
{

Task::Task(const esys::ObjectName &name, TaskType type)
    : esys::TaskBase(name, type), TaskPlat(this)
{
}

Task::~Task()
{
}

void Task::SetMngrBase(TaskMngrBase *taskmngr_base)
{
    TaskPlat::SetTaskMngrBase(taskmngr_base);
}

}

}





