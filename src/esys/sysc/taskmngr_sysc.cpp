/*!
 * \file esys/sysc/taskmngr_sysc.cpp
 * \brief Declaration of the SystemC TaskMngr class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/taskmngr.h"

namespace esys
{

namespace sysc
{

TaskMngr::TaskMngr(const ObjectName &name) : TaskMngrBase(name), TaskMngrPlat(this)
{
}

TaskMngr::~TaskMngr()
{
}

}

}


