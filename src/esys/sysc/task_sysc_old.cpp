/*!
 * \file esys/sysc/task_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/esys_prec.h"
#include "esys/esys_defs.h"

#include "esys/sysc/task.h"
#include "esys/sysc/taskimpl.h"

#include <cassert>

namespace esys
{

namespace sysc
{

Task *Task::Current()
{
    TaskImpl *cur=TaskImpl::GetCurrent();
    if (cur!=NULL)
        return cur->GetTask();
    return NULL;
}

TaskBase *Task::CurrentBase()
{
    return Current();
}

Task::Task(const char *name): TaskBase(name), m_task_mngr(NULL)
{
    m_impl=new TaskImpl(name);
    m_impl->SetTask(this);
    TaskBase::AddCurrentFct(&Task::CurrentBase);
}

Task::~Task()
{
    if (m_impl!=NULL)
        delete m_impl;
}

void Task::Init()
{
}

void Task::Setup()
{
}

void Task::Init()
{
}

void Task::Setup()
{
}

millis_t Task::Millis()
{
    return m_impl->Millis();
}

void Task::Sleep(uint64_t val)
{
    m_impl->Sleep(val);
}

void Task::InternalCoreLoop()
{
}

void Task::InternalCoreLoop()
{
}

void Task::Wait()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void Task::Signal()
{
    assert(m_impl!=NULL);

    m_impl->Wait();
}

void Task::DoLoopIterationEnds()
{
    assert(m_impl!=NULL);

    m_impl->DoLoopIterationEnds();
}

void Task::Exit(int16_t ret)
{
    assert(m_impl!=NULL);

    TaskBase::Exit(ret);

    m_impl->Exit();
}

void Task::SetTaskMngr(TaskMngr *task_mngr)
{
    m_task_mngr=task_mngr;
}

TaskMngr *Task::GetTaskMngr()
{
    return m_task_mngr;
}

}

}

