/*!
 * \file esys/sysc/system_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/system.h"

namespace esys
{

namespace sysc
{

System::System(const ObjectName &name) : SystemBase(name), SystemPlat(this)
{
}

System::~System()
{
}

}

}





