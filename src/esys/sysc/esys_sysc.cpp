/*!
 * \file esys/sysc/esys_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/esys.h"
#include "esys/mp/esys.h"
#include "esys/platform/id.h"

#include "esys/sysc/semaphore.h"
#include "esys/sysc/mutex.h"
#include "esys/sysc/task.h"
#include "esys/sysc/taskmngr.h"
#include "esys/sysc/systemplat.h"
#include "esys/sysc/timer.h"

namespace esys
{

namespace sysc
{

ESys ESys::s_esys;

ESys &ESys::Get()
{
    return s_esys;
}

void ESys::Select()
{
    esys::mp::ESys::SetPlatform(platform::SYSC);
}

ESys::ESys()
    : mp::ESysBase(ESYS_PLAT_SYSC, "SystemC")
{
}

ESys::~ESys()
{
}

SemaphoreBase *ESys::NewSemaphoreBase(const ObjectName &name, uint32_t count)
{
    return new Semaphore(name, count);
}

MutexBase *ESys::NewMutexBase(const ObjectName &name, MutexBase::Type type)
{
    return new Mutex(name, type);
}

TaskPlatIf *ESys::NewTaskPlat(TaskBase *task_base)
{
    return new TaskPlat(task_base);
}

TaskMngrPlatIf *ESys::NewTaskMngrPlat(TaskMngrBase *taskmngr_base)
{
    return new TaskMngrPlat(taskmngr_base);
}

SystemPlatIf *ESys::NewSystemPlat(SystemBase *system_base)
{
    return new SystemPlat(system_base);
}

TimerBase *ESys::NewTimerBase(const ObjectName &name)
{
    return new Timer(name);
}

TaskPlatIf *ESys::GetCurTaskPlatIf()
{
    assert(false); // Not implemented

    return nullptr;
}

} // namespace sysc

} // namespace esys
