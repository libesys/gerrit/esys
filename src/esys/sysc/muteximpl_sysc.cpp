/*!
 * \file esys/sysc/muteximpl_sysc.cpp
 * \brief Declaration of the SystemC Mutex PIMPL class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/muteximpl.h"

namespace esys
{

namespace sysc
{

MutexImpl::MutexImpl(const char *name, esys::MutexBase::Type type)
    : m_mutex(sc_core::sc_gen_unique_name(name))
{
}

MutexImpl::~MutexImpl()
{
}

int32_t MutexImpl::Lock(bool from_isr)
{
    int result;

    result = m_mutex.lock();
    return result;
}

int32_t MutexImpl::UnLock(bool from_isr)
{
    int result;

    result = m_mutex.unlock();
    return result;
}

int32_t MutexImpl::TryLock(bool from_isr)
{
    bool result;

    result = m_mutex.trylock();

    if (result == true)
        return 0;
    return -1;
}

}

}





