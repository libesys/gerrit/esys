/*!
 * \file esys/sysc/timer_sysc.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2018 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys/sysc/timer.h"
#include "esys/sysc/timerimpl.h"
#include "esys/assert.h"

namespace esys
{

namespace sysc
{

Timer::Timer(const ObjectName &name) : TimerBase(name)
{
    m_impl = new TimerImpl(name);
    m_impl->SetTimer(this);
}

Timer::~Timer()
{
    delete m_impl;
}

int32_t Timer::Start(uint32_t ms, bool one_shot, bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Start(ms, one_shot);
}

int32_t Timer::Stop(bool from_isr)
{
    assert(m_impl != nullptr);

    return m_impl->Stop();
}

int32_t Timer::GetPeriod()
{
    assert(m_impl != nullptr);

    return m_impl->GetPeriod();
}

bool Timer::IsRunning()
{
    assert(m_impl != nullptr);

    return m_impl->IsRunning();
}

bool Timer::IsOneShot()
{
    assert(m_impl != nullptr);

    return m_impl->IsOneShot();
}

}

}




