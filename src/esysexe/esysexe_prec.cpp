/*!
 * \file esysexe/esysexe_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// esysexe.cpp : source file that includes just the standard includes
// esysexe.pch will be the pre-compiled header
// esysexe.obj will contain the pre-compiled type information

#include "esysexe/esysexe_prec.h"

// TODO: reference any additional headers you need in esysexe_prec.h
// and not in this file

