/*!
 * \file esysexe/esysexe.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysexe/esysexe_prec.h"
#include "esysexe/esysexe.h"

#include <esys/usbenum.h>
#include <esys/usbdevinfo.h>
#include <esys/usbtree.h>
#include <esys/serialportenum.h>

#include <esys/mp/esys.h>
#include <esys/evtloop/mp/esys.h>
#include <esys/assert.h>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/locale.hpp>

#include <iostream>
#include <iomanip>

ESysExe::ESysExe()
{
}

ESysExe::~ESysExe()
{
    esys::mp::ESys::Release();
}

void ESysExe::SetOs(std::ostream &os)
{
    m_os = &os;
}

std::ostream *ESysExe::GetOs()
{
    return m_os;
}

void ESysExe::SetArgs(int argc, char **argv)
{
    int i;

    m_args.clear();

    m_argc = argc;

    for (i = 1; i < argc; ++i)
    {
#ifdef WIN32
        std::vector<std::string> temp_args = po::split_winmain(argv[i]);
        m_args.insert(m_args.end(), temp_args.begin(), temp_args.end());
#else
        std::string temp = argv[i];
        m_args.push_back(temp);
#endif
    }
    // m_argv = m_args.data();
}

int ESysExe::ParseArgs()
{
    bool parse_error = false;
    bool strict = false;
    po::positional_options_description p;
    // p.add("input", -1);

    m_desc = new po::options_description("Allowed options");
    // clang-format off
    m_desc->add_options()
        ("help,h", "produce help message")
        ("list-plugins", po::value<bool>()->default_value(false)->implicit_value(true), "list plugins")
        ("list-usb", po::value<bool>()->default_value(false)->implicit_value(true), "list USB devices")
        ("tree-usb", po::value<bool>()->default_value(false)->implicit_value(true), "show tree of USB devices")
        ("list-com", po::value<bool>()->default_value(false)->implicit_value(true), "list COM ports")
        ("verbose", po::value<int>()->default_value(0)->implicit_value(1), "set verbose leve")
        ("debug", po::value<bool>()->default_value(false)->implicit_value(true), "set debug mode, default random variable")
        ;
    // clang-format on

    try
    {
        // po::store(po::wcommand_line_parser(m_argc, m_argv).options(desc).positional(p).run(), vm);
        if (strict == false)
        {
            po::parsed_options parsed =
                po::command_line_parser(m_args).options(*m_desc)./*positional(p).*/ allow_unregistered().run();
            m_to_parse_further = po::collect_unrecognized(parsed.options, po::include_positional);
            po::store(parsed, m_vm);
        }
        else
        {
            po::parsed_options parsed = po::command_line_parser(m_args).options(*m_desc).positional(p).run();
            m_to_parse_further = po::collect_unrecognized(parsed.options, po::include_positional);
            po::store(parsed, m_vm);
        }
        po::notify(m_vm);
    }
    catch (po::error &e)
    {
        m_parse_error = true;
        SetErrorMsg(e.what());
        // std::cout << e.what() << std::endl << std::flush;
    }

    if (m_parse_error) return -1;
    return 0;
}

bool ESysExe::GetDebug()
{
    return m_vm["debug"].as<bool>();
}

int ESysExe::Do()
{
    if (m_vm.count("help"))
    {
        if (GetOs() != nullptr) PrintHelp(*GetOs());
        return 0;
    }
    if (m_vm["list-plugins"].as<bool>()) return CmdListPlugings();
    if (m_vm["list-usb"].as<bool>()) return CmdListUSB();
    if (m_vm["tree-usb"].as<bool>()) return CmdTreeUSB();
    if (m_vm["list-com"].as<bool>()) return CmdListCOM();
    return -1;
}

int ESysExe::CmdListPlugings()
{
    if (GetOs() == nullptr) return -17;

    *GetOs() << "Number of ESys Plugins : " << esys::mp::ESys::GetPluginsCount() << std::endl;

    esys::mp::Plugin *plugin;
    std::size_t idx;

    for (idx = 0; idx < esys::mp::ESys::GetPluginsCount(); ++idx)
    {
        plugin = esys::mp::ESys::GetPlugin(idx);

        assert(plugin != nullptr);

        *GetOs() << "[" << idx << "] " << plugin->GetName() << " " << plugin->GetVersion() << std::endl;
        *GetOs() << "    Filename : " << plugin->GetFileName() << std::endl;
    }
    *GetOs() << std::endl;

    *GetOs() << "Number of ESys Platforms : " << esys::mp::ESys::GetCount() << std::endl;

    esys::mp::ESysBase *plat;

    for (idx = 0; idx < esys::mp::ESys::GetCount(); ++idx)
    {
        plat = esys::mp::ESys::GetBase(idx);

        assert(plat != nullptr);

        *GetOs() << "[" << idx << "] " << plat->GetName() << std::endl;
    }
    *GetOs() << std::endl;

    *GetOs() << "Number of ESys EvtLoop Platforms : " << esys::evtloop::mp::ESys::GetCount() << std::endl;

    esys::evtloop::mp::ESysBase *plat_evtloop;

    for (idx = 0; idx < esys::evtloop::mp::ESys::GetCount(); ++idx)
    {
        plat_evtloop = esys::evtloop::mp::ESys::GetBase(idx);

        assert(plat_evtloop != nullptr);

        *GetOs() << "[" << idx << "] " << plat_evtloop->GetName() << std::endl;
    }
    *GetOs() << std::endl;
    return 0;
}

void ESysExe::Print(unsigned int idx, esys::USBDevInfo *usb_dev_info, uint32_t level)
{
    esys::USBDevInfo *child_info;
    uint32_t idx_space;
    std::string space;
    std::string value;

    for (idx_space = 0; idx_space < level; ++idx_space) space += "  ";

    *GetOs() << space << std::dec << std::setfill(' ') << std::setw(2) << idx << "  ";
    *GetOs() << "vid= 0x" << std::setw(4) << std::hex << std::setfill('0') << usb_dev_info->GetIdVendor();
    *GetOs() << " pid= 0x" << std::setw(4) << std::hex << std::setfill('0') << usb_dev_info->GetIdProduct();
    value = usb_dev_info->GetDescription();
    *GetOs() << " " << value;
    *GetOs() << std::endl;

    if (usb_dev_info->GetChildrenCount() != 0)
    {
        for (unsigned int idx = 0; idx < usb_dev_info->GetChildrenCount(); ++idx)
        {
            child_info = usb_dev_info->GetChild(idx);

            Print(idx, child_info, level + 1);
        }
    }
}

int ESysExe::CmdListUSB()
{
    // DBG_CALLMEMBER_RET("esys::Programmer::ListUSB", false, int, result);
    // DBG_CALLMEMBER_END;

    uint16_t idx;
    esys::USBDevInfo *usb_dev_info;
    esys::USBDevInfo *usb_dev_child_info;
    std::wstring value;
    esys::USBEnum &usb_enum = esys::USBEnum::Get();

    if (GetOs() == nullptr)
    {
        // SetError(-1);
        // result = -1;
        return -17;
    }

    usb_enum.SetDebug(GetDebug());
    usb_enum.SetVerbose(GetDebug());
    // usb_enum.SetFilterMngr(&BoardInfoLibMngr::GetDefault());

    usb_enum.Refresh();

    if (usb_enum.GetNbrDev() == 0)
    {
        *GetOs() << "No USB devices found!" << std::endl;
        return 0;
    }

    for (idx = 0; idx < usb_enum.GetNbrDev(); ++idx)
    {
        usb_dev_info = usb_enum.GetDev(idx);

        assert(usb_dev_info != NULL);

        Print(idx, usb_dev_info, 0);
    }
    // result = 0;
    // SetError(0);
    return 0;
}

int ESysExe::CmdTreeUSB()
{
    // DBG_CALLMEMBER_RET("esys::Programmer::TreeUSB", false, int, result);
    // DBG_CALLMEMBER_END;

    int32_t idx;
    esys::USBDevInfo *usb_dev_info;
    std::wstring value;
    esys::USBEnum &usb_enum = esys::USBEnum::Get();

#ifdef WIN32

    esys::USBEnum::SetEnumerateHostControllers(true);
#endif

    if (GetOs() == nullptr)
    {
        // SetError(-1);
        // result = -1;
        return -17;
    }

    usb_enum.SetDebug(GetDebug());
    usb_enum.SetVerbose(GetDebug());
    // usb_enum.SetFilterMngr(&BoardInfoLibMngr::GetDefault());

    usb_enum.Refresh();

    esys::USBTree &tree = usb_enum.GetTree();
    esys::USBTreeItem *tree_item;

    Print(&tree, 0);

    return 0;
}

int ESysExe::CmdListCOM()
{
    int result;
    esys::SerialPortEnum &com_enum = esys::SerialPortEnum::Get();

    if (GetOs() == nullptr) return -17;

    result = com_enum.Refresh();
    if (result < 0)
    {
        *GetOs() << "ERROR: couldn't get the list of COM port." << std::endl;
        return result;
    }

    if (com_enum.GetCount() == 0)
    {
        *GetOs() << "No COM port found" << std::endl;
        return 0;
    }

    for (unsigned int idx = 0; idx < com_enum.GetCount(); ++idx)
    {
        *GetOs() << "[" << idx << "] " << com_enum.GetPortName(idx) << std::endl;
    }

    return 0;
}

void ESysExe::Print(esys::USBTreeItemBase *tree_item, uint32_t level)
{
    esys::USBTreeItemBase *child;
    uint32_t idx;
    std::string space;

    for (idx = 0; idx < level; ++idx) space += "  ";

    *GetOs() << space << "-- " << tree_item->GetName() << std::endl;

    for (idx = 0; idx < tree_item->NbrChildren(); ++idx)
    {
        child = tree_item->GetChild(idx);
        if (child == nullptr) continue;
        Print(child, level + 1);
    }
}

void ESysExe::SetErrorMsg(const std::string &error_msg)
{
    m_error_msg = error_msg;
}

const std::string &ESysExe::GetErrorMsg()
{
    return m_error_msg;
}

void ESysExe::PrintHelp(std::ostream &os)
{
    std::ostringstream oss;

    if (m_desc == nullptr) return;
    oss << *m_desc;

    os << oss.str();
}
