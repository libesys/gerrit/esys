/*!
 * \file esysexe/main.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esysexe/esysexe_prec.h"
#include "esysexe/version.h"

#include "esysexe/esysexe.h"

#include <esys/version_defs.h>
#include <esys/mp/esys.h>
#include <esys/assert.h>
#include <esys/pluginmngrbase.h>

#include <iostream>

#ifdef WIN32
#include <io.h>
#include <fcntl.h>
#endif

int main(int argc, char *argv[])
{
    ESysExe esys_exe;

    std::cout << ESYSEXE_VERSION_STRING << std::endl << std::endl;

    std::cout << ESYS_VERSION_STRING << std::endl << std::endl;

    esys_exe.SetOs(std::cout);
    esys_exe.SetArgs(argc, argv);
    int result = esys_exe.ParseArgs();

    if (result < 0)
    {
        esys_exe.PrintHelp(std::cout);
        return -result;
    }

    esys::PluginMngrBase::SetAppExe(argv[0]);

    return esys_exe.Do();
}
