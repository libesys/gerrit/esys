/*!
 * \file esys/esys_wx_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// esys_wx.cpp : source file that includes just the standard includes
// esys_wx.pch will be the pre-compiled header
// esys_wx.obj will contain the pre-compiled type information

#include "esys_wx/esys_wx_prec.h"

// TODO: reference any additional headers you need in esys_wx_prec.h
// and not in this file

