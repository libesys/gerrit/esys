/*!
 * \file esys_wx/plugin.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_wx/esys_wx_prec.h"
#include "esys_wx/plugin.h"
#include "esys_wx/version.h"

#include <iomanip>
#include <iostream>

DEFINE_ESYS_PLUGIN(ESYS_WX_API, esys_wx::Plugin);

namespace esys_wx
{

Plugin::Plugin(): BaseType()
{
    SetName("wxWidgets");
    SetShortName("wx");
    SetVersion(ESYS_WX_VERSION_NUM_DOT_STRING);
}

Plugin::~Plugin()
{
}

int Plugin::Init()
{
    return 0;
}

int Plugin::Release()
{
    return 0;
}

}



