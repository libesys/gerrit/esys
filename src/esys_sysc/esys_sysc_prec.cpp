/*!
 * \file esys/esys_sysc_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// esys_sysc.cpp : source file that includes just the standard includes
// esys_sysc.pch will be the pre-compiled header
// esys_sysc.obj will contain the pre-compiled type information

#include "esys_sysc/esys_sysc_prec.h"

// TODO: reference any additional headers you need in esys_sysc_prec.h
// and not in this file

