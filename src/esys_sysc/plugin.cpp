/*!
 * \file esys_sysc/plugin.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_sysc/esys_sysc_prec.h"
#include "esys_sysc/plugin.h"
#include "esys_sysc/version.h"

#include <iomanip>
#include <iostream>

DEFINE_ESYS_PLUGIN(ESYS_SYSC_API, esys_sysc::Plugin);

namespace esys_sysc
{

Plugin::Plugin()
    : BaseType()
{
    SetName("SystemC");
    SetShortName("SysC");
    SetVersion(ESYS_SYSC_VERSION_NUM_DOT_STRING);
}

Plugin::~Plugin()
{
}

int Plugin::Init()
{
    return 0;
}

int Plugin::Release()
{
    return 0;
}

} // namespace esys_sysc
