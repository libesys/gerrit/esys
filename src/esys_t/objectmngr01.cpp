/*!
 * \file esys_t/objectmngr01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/esys.h>
#include <esys/objectmngr.h>
#include <esys/objectnode.h>
#include <esys/objectleaf.h>

#include <iostream>

namespace objectmngr01
{

class TheObject : public esys::ObjectNode
{
public:
    TheObject(const esys::ObjectName &name);
    virtual ~TheObject();

    virtual int32_t StaticInit(Order order) override;
    virtual int32_t StaticRelease(Order order) override;
    virtual int32_t RuntimeInit(esys::RuntimeLevel runtime_lvl, Order order) override;
    virtual int32_t RuntimeRelease(esys::RuntimeLevel runtime_lvl, Order order) override;
};

TheObject::TheObject(const esys::ObjectName &name): esys::ObjectNode(name)
{
}

TheObject::~TheObject()
{
}

int32_t TheObject::StaticInit(Order order)
{
#ifdef ESYS_OBJECT_META
    std::cout << GetId() << " " << GetName();
#endif
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " StaticInit Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " StaticInit After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

int32_t TheObject::StaticRelease(Order order)
{
    std::cout << GetId() << " " << GetName();
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " StaticRelease Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " StaticRelease After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

int32_t TheObject::RuntimeInit(esys::RuntimeLevel runtime_lvl, Order order)
{
    std::cout << GetId() << " " << GetName();
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " RuntimeInit(" << runtime_lvl << ") Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " RuntimeInit(" << runtime_lvl << ") After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

int32_t TheObject::RuntimeRelease(esys::RuntimeLevel runtime_lvl, Order order)
{
    std::cout << GetId() << " " << GetName();
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " RuntimeRelease(" << runtime_lvl << ") Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " RuntimeRelease(" << runtime_lvl << ") After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

class TheObjectLeaf : public esys::ObjectLeaf
{
public:
    TheObjectLeaf(const esys::ObjectName &name);
    virtual ~TheObjectLeaf();

    virtual int32_t StaticInit(Order order) override;
    virtual int32_t StaticRelease(Order order) override;
    virtual int32_t RuntimeInit(esys::RuntimeLevel runtime_lvl, Order order) override;
    virtual int32_t RuntimeRelease(esys::RuntimeLevel runtime_lvl, Order order) override;
};

TheObjectLeaf::TheObjectLeaf(const esys::ObjectName &name) : esys::ObjectLeaf(name)
{
}

TheObjectLeaf::~TheObjectLeaf()
{
}

int32_t TheObjectLeaf::StaticInit(Order order)
{
    std::cout << GetId() << " " << GetName();
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " StaticInit Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " StaticInit After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

int32_t TheObjectLeaf::StaticRelease(Order order)
{
    std::cout << GetId() << " " << GetName();
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " StaticRelease Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " StaticRelease After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

int32_t TheObjectLeaf::RuntimeInit(esys::RuntimeLevel runtime_lvl, Order order)
{
    std::cout << GetId() << " " << GetName();
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " RuntimeInit(" << runtime_lvl << ") Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " RuntimeInit(" << runtime_lvl << ") After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

int32_t TheObjectLeaf::RuntimeRelease(esys::RuntimeLevel runtime_lvl, Order order)
{
    std::cout << GetId() << " " << GetName();
    switch (order)
    {
        case BEFORE_CHILDREN:
            std::cout << " RuntimeRelease(" << runtime_lvl << ") Before children" << std::endl;
            break;
        case AFTER_CHILDREN:
            std::cout << " RuntimeRelease(" << runtime_lvl << ") After children" << std::endl;
            break;
        default:
            std::cout << " ERROR" << std::endl;
    }
    return 0;
}

class Object1 : public TheObject
{
public:
    Object1(const esys::ObjectName &name);
    virtual ~Object1();

protected:
    TheObject m_obj_a;
    TheObject m_obj_b;
    TheObjectLeaf m_leaf_la;
};

Object1::Object1(const esys::ObjectName &name)
    : TheObject(name), m_obj_a("obj_a"), m_obj_b("obj_b"), m_leaf_la("obj_la")
{
}

Object1::~Object1()
{
}

class Object2 : TheObject
{
public:
    Object2(const esys::ObjectName &name);
    virtual ~Object2();

protected:
    TheObject m_obj_c;
    TheObject m_obj_d;
    TheObject m_obj_e;
};

Object2::Object2(const esys::ObjectName &name)
    : TheObject(name), m_obj_c("obj_c"), m_obj_d("obj_d"), m_obj_e("obj_e")
{
}

Object2::~Object2()
{
}

class ObjectMngr : public esys::ObjectMngr
{
public:
    ObjectMngr(const esys::ObjectName &name);
    virtual ~ObjectMngr();
protected:
    Object1 m_obj1;
    Object2 m_obj2;
};

ObjectMngr::ObjectMngr(const esys::ObjectName &name)
    : esys::ObjectMngr(name), m_obj1("obj_1"), m_obj2("obj_2")
{
}

ObjectMngr::~ObjectMngr()
{
}

ESYSTEST_AUTO_TEST_CASE(ObjectMngr01)
{
    ObjectMngr m_mngr("obj_mngr");
    int32_t result;

    result = m_mngr.StaticInit();
    std::cout << std::endl;
    result = m_mngr.RuntimeInit();
    std::cout << std::endl;
    result = m_mngr.RuntimeRelease();
    std::cout << std::endl;
    result = m_mngr.StaticRelease();
}

}
