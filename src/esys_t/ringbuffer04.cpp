/*!
 * \file esys_t/ringbuffer04.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/ringbuffer.h>
#include "esys_t/bufferfixture.h"

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_FIXTURE_TEST_CASE(RingBuffer04, BufferFixture)
{
    esys::RingBufferStatic<uint8_t, 100> buf;

    RandomTest<uint8_t, esys::RingBufferStatic<uint8_t, 100> >(buf, 100, 10000);
}