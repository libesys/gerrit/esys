/*!
 * \file esys_t/gen/crcccitt1601.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2018 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"
#include <esys_t_fix/crcccitt16test.h>
#include <esys/gen/crcccitt16.h>


ESYSTEST_FIXTURE_TEST_CASE(CRCTest01, esys_t_fix::CRCCCITT16Test)
{
    esys::gen::CRCCCITT16 crc;

    SetCRC(&crc);

    Test();
}
