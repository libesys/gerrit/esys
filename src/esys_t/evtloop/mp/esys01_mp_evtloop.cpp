/*!
 * \file esys_t/evtloop/mp/esys01_mp_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#ifdef _MSC_VER
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include <esys/evtloop/mp/esys.h>

#include <esys/evtloop/mp/task.h>
#include <esys/evtloop/mp/taskmngr.h>

#include <iostream>

#ifdef _MSC_VER
#pragma warning( disable : 4250 )
#endif

namespace esys
{

namespace evtloop
{

namespace mp
{

class MyTask: public Task
{
public:
    MyTask();

    virtual void Loop();
};

MyTask::MyTask(): Task("MyTask")
{
}

void MyTask::Loop()
{
    esys::millis_t started=Millis();
    esys::millis_t epsilon=50;
    int64_t diff;
    uint16_t delay=200;

    Sleep(delay);
    diff=Millis()-started;

    bool success=( (diff>=(int64_t)delay) && (diff<(int64_t)(delay+epsilon)) );

    if (!success)
        std::cout << diff << "ms" << std::endl;

    ESYSTEST_REQUIRE_EQUAL(true, success);
    Exit(0);
}

}

}

}

ESYSTEST_AUTO_TEST_CASE(ESys01MPEvtLoop)
{
#ifdef _DEBUG
//    _crtBreakAlloc = 1363;
#endif
    //esys::sysc::MyTask *task=new esys::sysc::MyTask();
    //esys::sysc::TaskMngr *mngr=new esys::sysc::TaskMngr();

    esys::evtloop::mp::ESys::Init();

    esys::evtloop::mp::MyTask task;
    esys::evtloop::mp::TaskMngr mngr;

    //mngr->AddTask(task);

    //mngr->Run();

    mngr.AddTask(&task);

    mngr.Run();

    //delete task;
    //delete mngr;

    esys::evtloop::mp::ESys::Release();
}
