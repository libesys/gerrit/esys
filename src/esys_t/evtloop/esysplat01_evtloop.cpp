/*!
 * \file esys_t/evtloop/esysplat01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/evtloop/esys.h>
#include <esys/platform/id.h>

#include <iostream>

ESYSTEST_AUTO_TEST_CASE(ESysPlat01EvtLoop)
{
    uint16_t plat_idx;
    esys::evtloop::ESysBase *e;
    esys::platform::Id *plat_id;

    if (!esys::evtloop::ESys::MULTI_PLAT)
        return;

    ESYSTEST_REQUIRE_EQUAL(3, esys::evtloop::ESys::GetCount());

    for (plat_idx=0; plat_idx<esys::evtloop::ESys::GetCount(); ++plat_idx)
    {
        e=esys::evtloop::ESys::GetByIdx(plat_idx);
        ESYSTEST_REQUIRE_NE((esys::evtloop::ESysBase *)NULL, e);

        plat_id=e->GetPlatformId();
        ESYSTEST_REQUIRE_NE((esys::platform::Id *)NULL, plat_id);

        esys::evtloop::ESys::SetPlatform(*plat_id);
        esys::evtloop::ESys::Init();

        esys::evtloop::ESys::Release();
    }
}