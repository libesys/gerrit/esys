/*!
 * \file esys_t/evtloop/esys01_evtloop.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#ifdef _MSC_VER
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include <esys/evtloop/esys.h>

#include <esys/evtloop/task.h>
#include <esys/evtloop/taskmngr.h>
#include <esys/platform/id.h>

#include <iostream>

class MyTask: public esys::evtloop::Task
{
public:
    MyTask();
    virtual ~MyTask();

    virtual void Loop();
};

MyTask::MyTask(): esys::evtloop::Task("MyTask")
{
}

MyTask::~MyTask()
{
}

void MyTask::Loop()
{
    esys::millis_t started=Millis();
    esys::millis_t epsilon=50;
    esys::millis_t diff;
    esys::millis_t delay=200;

    Sleep(delay);
    diff=Millis()-started;
    bool success=( (diff>=(int64_t)delay) && (diff<(int64_t)(delay+epsilon)) );

    if (!success)
        std::cout << diff << "ms" << std::endl;

    ESYSTEST_REQUIRE_EQUAL(true, success);
    Exit(0);
}

ESYSTEST_AUTO_TEST_CASE(ESys01EvtLoop)
{
    MyTask task;
    esys::evtloop::TaskMngr mngr;

    esys::evtloop::ESys::SetPlatform(esys::platform::SYSC);

    esys::evtloop::ESys::Init();

    mngr.AddTask(&task);

    mngr.Run();

    esys::evtloop::ESys::Release();
}

/*do
{
	::boost::unit_test::unit_test_log.set_checkpoint(
		::boost::unit_test::const_string("e:\\project\\alamaksoft\\esys_dev\\src\\esys\\src\\esys_t\\esys01.cpp",
			sizeof("e:\\project\\alamaksoft\\esys_dev\\src\\esys\\src\\esys_t\\esys01.cpp") - 1),
			static_cast<std::size_t>(65));
		::boost::test_tools::tt_detail::check_frwd(::boost::test_tools::tt_detail::equal_impl_frwd(),
			(::boost::unit_test::lazy_ostream::instance() << ""),
			::boost::unit_test::const_string("e:\\project\\alamaksoft\\esys_dev\\src\\esys\\src\\esys_t\\esys01.cpp",
			sizeof("e:\\project\\alamaksoft\\esys_dev\\src\\esys\\src\\esys_t\\esys01.cpp") - 1),
			static_cast<std::size_t>(65),
			::boost::test_tools::tt_detail::REQUIRE,
			::boost::test_tools::tt_detail::CHECK_EQUAL, true, "true", success, "success");
}
while (::boost::test_tools::tt_detail::dummy_cond());
Exit(0); */
