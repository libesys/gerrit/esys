/*!
 * \file esys_t/buffer06.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"
#include "esys_t/bufferfixture.h"

#include <esys/bufferstatic.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_FIXTURE_TEST_CASE(Buffer06, BufferFixture)
{
    esys::BufferStatic<uint8_t, 32> buf_ps;
    esys::BufferStatic<uint8_t, 32>::Ptr p;
    uint8_t buf0[4]= { 0xaa, 0x44, 0x15, 0x45 };
    uint16_t idx;

    ESYSTEST_REQUIRE_EQUAL(0, buf_ps.GetSize());

    ESYSTEST_REQUIRE_EQUAL(0, buf_ps.Push(buf0, 4));
    ESYSTEST_REQUIRE_EQUAL(4, buf_ps.GetSize());

    for (idx=0; idx<4; ++idx)
    {
        ESYSTEST_REQUIRE_EQUAL(buf0[idx], buf_ps[idx]);
    }

    ESYSTEST_REQUIRE_EQUAL(0, buf_ps.Pop(1));
    ESYSTEST_REQUIRE_EQUAL(3, buf_ps.GetSize());

    for (idx=0; idx<3; ++idx)
    {
        ESYSTEST_REQUIRE_EQUAL(buf0[idx+1], buf_ps[idx]);
    }

    ESYSTEST_REQUIRE_EQUAL(0, buf_ps.Pop(2));
    ESYSTEST_REQUIRE_EQUAL(1, buf_ps.GetSize());

    ESYSTEST_REQUIRE_EQUAL(buf0[3], buf_ps[0]);

}