/*!
 * \file esys_t/i2cbusmngrbase01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/esys.h>
#include <esys/i2cbusmngrbase.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_AUTO_TEST_CASE(I2CBusMngrBase01)
{
    esys::I2CBusMngrBase bus_mngr;

    ESYSTEST_REQUIRE_EQUAL(0, bus_mngr.GetNbrBus());

}