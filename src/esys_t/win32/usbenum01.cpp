/*!
 * \file esys_t/usbenum01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

//#include <esys/esys.h>
#include <esys/win32/usbenum.h>

#include <iostream>

ESYSTEST_AUTO_TEST_CASE(USBEnum01)
{
    esys::USBEnum &usb=esys::USBEnum::Get();

    usb.SetDebug(true);

    ESYSTEST_REQUIRE_EQUAL(0, usb.Refresh());
    //ESYSTEST_REQUIRE_EQUAL(true, success);
}

