/*!
 * \file esys_t/ringbuffer01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/ringbuffer.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_AUTO_TEST_CASE(RingBuffer01)
{
    esys::RingBufferStatic<uint8_t, 4> buf;
    uint8_t val;

    ESYSTEST_REQUIRE_EQUAL(4, buf.GetMaxSize());
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetWriteIdx());

    buf << 0x17;
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetWriteIdx());

    val=buf[(uint16_t)0];
    ESYSTEST_REQUIRE_EQUAL(0x17, val);
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetSize());

    buf >> val;
    ESYSTEST_REQUIRE_EQUAL(0x17, val);
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetReadIdx());

    buf << 0x19;
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetWriteIdx());
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetSize());

    buf >> val;
    ESYSTEST_REQUIRE_EQUAL(0x19, val);
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetWriteIdx());

    buf << 0x21;
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(3, buf.GetWriteIdx());

    buf << 0x23;
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetWriteIdx());

    buf >> val;
    ESYSTEST_REQUIRE_EQUAL(0x21, val);
    ESYSTEST_REQUIRE_EQUAL(1, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(3, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetWriteIdx());

    buf >> val;
    ESYSTEST_REQUIRE_EQUAL(0x23, val);
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(0, buf.GetWriteIdx());

}