/*!
 * \file esys_t/hwuid01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/hwuid.h>

ESYSTEST_AUTO_TEST_CASE(HWUID01)
{
    esys::HWUID_t<4> hwuid;
    esys::uint8_t data[4] = { 0x01, 0x02, 0x03, 0x04 };

    hwuid.SetData(data, sizeof(data));

    ESYSTEST_REQUIRE_EQUAL(sizeof(data) + 2, hwuid.GetRawSize());

    ESYSTEST_REQUIRE_EQUAL(sizeof(data), hwuid.GetSize());
    ESYSTEST_REQUIRE_EQUAL(hwuid.RAW, hwuid.GetType());
    ESYSTEST_REQUIRE_EQUAL(hwuid.RAW, hwuid.GetRawData()[0]);
    ESYSTEST_REQUIRE_EQUAL(sizeof(data), hwuid.GetRawData()[1]);

    for (int idx = 0; idx < sizeof(data); ++idx)
    {
        ESYSTEST_REQUIRE_EQUAL(data[idx], hwuid.GetData()[idx]);
    }
}

