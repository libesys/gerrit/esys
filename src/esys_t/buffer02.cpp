/*!
 * \file esys_t/buffer02.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/bufferstatic.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

/*class Test
{
public:
    Test(): m_size(0)
    {
    }

    void Write(uint8_t *buf, uint16_t size)
    {
        memcpy(m_buf, buf, size);
        m_size=size;
    }

    void Write1(uint8_t *buf, uint16_t size)
    {
        for (uint16_t idx=0;idx<size;++idx)
        {
            m_buf[idx]=buf[idx]+0x10;
        }
        m_size=size;
    }

    int16_t Write2(uint8_t *buf, uint16_t size)
    {
        for (uint16_t idx=0;idx<size;++idx)
        {
            m_buf[idx]=buf[idx]+0x20;
        }
        m_size=size;
        return 0;
    }

    uint8_t m_buf[256];
    uint16_t m_size;
}; */

ESYSTEST_AUTO_TEST_CASE(Buffer02)
{
    esys::BufferStatic<uint8_t, 32> buf;
    uint8_t sbuf[16]= { 0x12, 0x45, 0x75, 0xAA };

    buf.SetSize(4);
    memcpy(buf, sbuf, 4);

    ESYSTEST_REQUIRE_EQUAL(0x12, buf[0]);
    ESYSTEST_REQUIRE_EQUAL(0x45, buf[1]);
    ESYSTEST_REQUIRE_EQUAL(0x75, buf[2]);
    ESYSTEST_REQUIRE_EQUAL(0xAA, buf[3]);

    /*Test test;

    ESYSTEST_REQUIRE_EQUAL(0, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(32, buf.GetMaxSize());

    ESYSTEST_REQUIRE_EQUAL(0, buf.GetNbrSegment());

    buf.SetSize(4);
    ESYSTEST_REQUIRE_EQUAL(4, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(32, buf.GetMaxSize());

    ESYSTEST_REQUIRE_EQUAL(1, buf.GetNbrSegment());

    buf[0]=0x10;
    ESYSTEST_REQUIRE_EQUAL(0x10, buf[0]);

    buf[0]=0x12;
    ESYSTEST_REQUIRE_EQUAL(0x12, buf[0]);

    buf[1]=0x13;
    ESYSTEST_REQUIRE_EQUAL(0x12, buf[0]);
    ESYSTEST_REQUIRE_EQUAL(0x13, buf[1]);

    buf[2]=0x14;
    ESYSTEST_REQUIRE_EQUAL(0x14, buf[2]);
    buf[3]=0x15;
    ESYSTEST_REQUIRE_EQUAL(0x15, buf[3]);

    ESYSTEST_REQUIRE_EQUAL(0, test.m_size);

    buf.DoEachSeg(&test, &Test::Write);

    ESYSTEST_REQUIRE_EQUAL(4, test.m_size);
    ESYSTEST_REQUIRE_EQUAL(0x12, test.m_buf[0]);
    ESYSTEST_REQUIRE_EQUAL(0x13, test.m_buf[1]);
    ESYSTEST_REQUIRE_EQUAL(0x14, test.m_buf[2]);
    ESYSTEST_REQUIRE_EQUAL(0x15, test.m_buf[3]);

    buf.DoEachSeg(test, &Test::Write1);

    ESYSTEST_REQUIRE_EQUAL(4, test.m_size);
    ESYSTEST_REQUIRE_EQUAL(0x22, test.m_buf[0]);
    ESYSTEST_REQUIRE_EQUAL(0x23, test.m_buf[1]);
    ESYSTEST_REQUIRE_EQUAL(0x24, test.m_buf[2]);
    ESYSTEST_REQUIRE_EQUAL(0x25, test.m_buf[3]);

    int16_t result=buf.DoEachSeg(test, &Test::Write2);

    ESYSTEST_REQUIRE_EQUAL(0, result);

    ESYSTEST_REQUIRE_EQUAL(4, test.m_size);
    ESYSTEST_REQUIRE_EQUAL(0x32, test.m_buf[0]);
    ESYSTEST_REQUIRE_EQUAL(0x33, test.m_buf[1]);
    ESYSTEST_REQUIRE_EQUAL(0x34, test.m_buf[2]);
    ESYSTEST_REQUIRE_EQUAL(0x35, test.m_buf[3]); */

}