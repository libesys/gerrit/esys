/*!
 * \file esys_t/taskmngr01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/inttypes.h>
#include <esys/task.h>
#include <esys/taskmngr.h>
#include <esys/systemtask.h>
#include <esys/esys.h>

//#include <esysboard/system.h>
#include <esys/system.h>

#include <esys/assert.h>

namespace taskmngr01
{

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4250)
#endif


class MySystem : public esys::System //esysboard::System
{
public:
    MySystem(const esys::ObjectName &name="MySystem");
    virtual ~MySystem();

    void SetTaskMngr(esys::TaskMngr *task_mngr);

protected:
    esys::SystemTask m_sys_task;
};

MySystem::MySystem(const esys::ObjectName &name) : esys::System(name)/*esysboard::System(name)*/, m_sys_task("SysTask")
{
    esys::ESys::Get().SetSystemBase(this);
}

MySystem::~MySystem()
{
}

void MySystem::SetTaskMngr(esys::TaskMngr *task_mngr)
{
    m_sys_task.SetMngrBase(task_mngr);
}

class MyTask : public esys::Task
{
public:
    MyTask(const esys::ObjectName &name);
    virtual ~MyTask();

    virtual esys::int32_t Entry() override;

    virtual esys::int32_t StaticInit(Order order) override;
    virtual esys::int32_t StaticRelease(Order order) override;
    virtual esys::int32_t RuntimeInit(esys::RuntimeLevel runtime_lvl, Order order) override;
    virtual esys::int32_t RuntimeRelease(esys::RuntimeLevel runtime_lvl, Order order) override;

    int m_static_init_counter;
    bool m_static_init_called;
    bool m_static_release_called;
    int m_runtime_init_counter;
    bool m_runtime_init_called;
    bool m_runtime_release_called;
    const char *m_name;
};

MyTask::MyTask(const esys::ObjectName &name)
    : esys::Task(name, esys::TaskType::APPLICATION), m_static_init_counter(0), m_static_init_called(false), m_static_release_called(false)
    , m_runtime_init_counter(0), m_runtime_init_called(false), m_runtime_release_called(false)
    , m_name(name)
{
}

MyTask::~MyTask()
{
}

esys::int32_t MyTask::Entry()
{
    Sleep(100);
    return -1;
}

esys::int32_t MyTask::StaticInit(Order order)
{
    int32_t result;

    result = Task::StaticInit(order);

    if (order == BEFORE_CHILDREN)
    {
        ESYSTEST_REQUIRE_EQUAL(m_static_init_counter, 0);
        m_static_init_counter++;
        m_static_init_called = true;
    }
    return result;
}

esys::int32_t MyTask::StaticRelease(Order order)
{
    if (order == AFTER_CHILDREN)
    {
        ESYSTEST_REQUIRE_EQUAL(m_static_init_counter, 1);
        m_static_init_counter--;
        m_static_release_called = true;
    }
    return Task::StaticRelease(order);
}

esys::int32_t MyTask::RuntimeInit(esys::RuntimeLevel runtime_lvl, Order order)
{
    int32_t result;

    result = Task::RuntimeInit(runtime_lvl, order);

    if ((order == BEFORE_CHILDREN) && (runtime_lvl==GetRuntimeLevel()))
    {
        ESYSTEST_MT_REQUIRE_EQUAL(m_runtime_init_counter, 0);
        m_runtime_init_counter++;
        m_runtime_init_called = true;
    }
    return result;
}

esys::int32_t MyTask::RuntimeRelease(esys::RuntimeLevel runtime_lvl, Order order)
{
    if ((order == AFTER_CHILDREN) && (runtime_lvl == GetRuntimeLevel()))
    {
        ESYSTEST_MT_REQUIRE_EQUAL(m_runtime_init_counter, 1);
        m_runtime_init_counter--;
        m_runtime_release_called = true;
    }

    return Task::RuntimeRelease(runtime_lvl, order);
}

class MyTaskMngr : public esys::TaskMngr
{
public:
    MyTaskMngr(const esys::ObjectName &name="MyTaskMngr");
    virtual ~MyTaskMngr();

    MyTask m_task1;
    MyTask m_task2;
};

MyTaskMngr::MyTaskMngr(const esys::ObjectName &name)
    : esys::TaskMngr(name)
    , m_task1("Task1")
    , m_task2("Task2")
{
    esys::ESys::Get().SetTaskMngrBase(this);
}

MyTaskMngr::~MyTaskMngr()
{
}

ESYSTEST_AUTO_TEST_CASE(TaskMngr01)
//ESYSTEST_FIXTURE_TEST_CASE(TaskMngr01, MyTaskMngr)
{
    //DBG_CALLMEMBER("TaskMngr01", false);
    //DBG_CALLMEMBER_END;

    esys::int32_t result;
    MySystem sys;

    result = sys.Init();
    //sys.print_log();

    MyTaskMngr mngr;

    sys.SetTaskMngr(&mngr); //Shouldn't be needed, something need fixing somewhere
    //mngr.print_log();

    result = mngr.Init();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    ESYSTEST_CHECK_EQUAL(mngr.m_task1.m_static_init_called, true);
    ESYSTEST_CHECK_EQUAL(mngr.m_task1.m_static_init_counter, 1);
    ESYSTEST_CHECK_EQUAL(mngr.m_task1.m_runtime_init_called, false);
    ESYSTEST_CHECK_EQUAL(mngr.m_task1.m_runtime_init_counter, 0);

    ESYSTEST_CHECK_EQUAL(mngr.m_task2.m_static_init_called, true);
    ESYSTEST_CHECK_EQUAL(mngr.m_task2.m_static_init_counter, 1);
    ESYSTEST_CHECK_EQUAL(mngr.m_task2.m_runtime_init_called, false);
    ESYSTEST_CHECK_EQUAL(mngr.m_task2.m_runtime_init_counter, 0);

    result = mngr.Run();
    ESYSTEST_CHECK_EQUAL(result, 0);

    result = mngr.Release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
    ESYSTEST_CHECK_EQUAL(mngr.m_task1.m_static_init_counter, 0);
    ESYSTEST_CHECK_EQUAL(mngr.m_task2.m_static_init_counter, 0);

    ESYSTEST_CHECK_EQUAL(mngr.m_task1.m_static_release_called, true);
    ESYSTEST_CHECK_EQUAL(mngr.m_task2.m_static_release_called, true);

    ESYSTEST_CHECK_EQUAL(mngr.m_task1.m_runtime_release_called, true);
    ESYSTEST_CHECK_EQUAL(mngr.m_task2.m_runtime_release_called, true);

    //mngr.print_log();

    sys.Release();

    //test_ended_inc();
}

}
