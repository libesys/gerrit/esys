#
# __legal_b__
#
# Copyright (c) 2017 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
#
# __legal_e__
#

project (esys_t CXX C ASM)

include_directories(
    ../../include
    ${ESYS_CFG}
    )

SET( ESYS_T_CXX_SOURCES
    buffer01.cpp
    buffer02.cpp
    buffer03.cpp
    #buffer04.cpp   #Need boost
    #buffer06.cpp
    #buffercopy01.cpp
    #buffercopysg01.cpp
    #bufferfixture.cpp
    esys_t_prec.cpp
    #list01.cpp
    #objectmngr01.cpp
    ringbuffer01.cpp
    ringbuffer02.cpp 
    taskmngr01.cpp
    unittest.cpp
    #unittestexe.cpp
)

add_executable (esys_t 
    ${ESYS_T_CXX_SOURCES}
    ${ESYS_IMPL_C_SOURCES}
    )    

if(DEFINED ESYS_VHW)    
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fexceptions -DBOOST_ALL_DYN_LINK -std=gnu++11")
else()
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
endif()
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-Map,${PROJECT_NAME}.map")

#add_dependencies( esys_t
#    esys
#    esys_t_fix
#    esystest
#    #freertos
#    ${ESYS_IMPL_LIBS}
#)

target_link_libraries( esys_t
    esys
    esys_t_fix
    esystest
    gcc
    m
    #freertos
    ${ESYS_IMPL_LIBS}
    stdc++
)

if(DEFINED ESYS_HW)
add_dependencies( esys_t
    freertos
)
target_link_libraries( esys_t
    freertos
)
endif()

ADD_CUSTOM_COMMAND(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_OBJCOPY} -O binary -R .bootloader $<TARGET_FILE:${PROJECT_NAME}> ${PROJECT_NAME}-loadable.bin)
ADD_CUSTOM_COMMAND(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_OBJCOPY} -O binary $<TARGET_FILE:${PROJECT_NAME}> ${PROJECT_NAME}.bin)
ADD_CUSTOM_COMMAND(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_OBJCOPY} -O ihex $<TARGET_FILE:${PROJECT_NAME}> ${PROJECT_NAME}.hex)
ADD_CUSTOM_COMMAND(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_SIZE} -B $<TARGET_FILE:${PROJECT_NAME}>)

ADD_CUSTOM_COMMAND(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_OBJDUMP} -C -x -w $<TARGET_FILE:${PROJECT_NAME}> >${LOGS_OUTPUT_PATH}/${PROJECT_NAME}.objdump)
ADD_CUSTOM_COMMAND(TARGET ${PROJECT_NAME} POST_BUILD COMMAND ${CMAKE_NM} --print-size --size-sort --reverse-sort --radix=d  -C $<TARGET_FILE:${PROJECT_NAME}> >${LOGS_OUTPUT_PATH}/$<TARGET_FILE_NAME:${PROJECT_NAME}>.nm)
