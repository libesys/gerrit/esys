/*!
 * \file esys_t/ringbuffer03.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/ringbuffer.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_AUTO_TEST_CASE(RingBuffer03)
{
    esys::RingBufferStatic<uint8_t, 10> buf;
    esys::RingBufferStatic<uint8_t, 32>::Ptr p;
    uint8_t sbuf[16]= { 0x12, 0x45, 0x75, 0xAA, 0x39, 0x71, 0x13, 0xBB };
    uint16_t idx;
    uint16_t val;

    ESYSTEST_REQUIRE_EQUAL(0, buf.Push(sbuf, 4));
    ESYSTEST_REQUIRE_EQUAL(4, buf.GetSize());

    ESYSTEST_REQUIRE_EQUAL(0x12, buf[(uint16_t)0]);
    ESYSTEST_REQUIRE_EQUAL(0x45, buf[(uint16_t)1]);
    ESYSTEST_REQUIRE_EQUAL(0x75, buf[(uint16_t)2]);
    ESYSTEST_REQUIRE_EQUAL(0xAA, buf[(uint16_t)3]);

    ESYSTEST_REQUIRE_EQUAL(0, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(4, buf.GetWriteIdx());

    ESYSTEST_REQUIRE_EQUAL(0, buf.Pop(2));
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetSize());

    ESYSTEST_REQUIRE_EQUAL(0x75, buf[(uint16_t)0]);
    ESYSTEST_REQUIRE_EQUAL(0xAA, buf[(uint16_t)1]);

    ESYSTEST_REQUIRE_EQUAL(2, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(4, buf.GetWriteIdx());

    ESYSTEST_REQUIRE_EQUAL(0, buf.Push(sbuf, 4));
    ESYSTEST_REQUIRE_EQUAL(6, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(2, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(8, buf.GetWriteIdx());

    ESYSTEST_REQUIRE_EQUAL(0x75, buf[(uint16_t)0]);
    ESYSTEST_REQUIRE_EQUAL(0xAA, buf[(uint16_t)1]);

    for (idx=0; idx<4; ++idx)
    {
        val=buf[(uint16_t)(idx+2)];
        ESYSTEST_REQUIRE_EQUAL(sbuf[idx], val);
    }

    ESYSTEST_REQUIRE_EQUAL(0, buf.Pop(3));
    ESYSTEST_REQUIRE_EQUAL(3, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(5, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(8, buf.GetWriteIdx());

    for (idx=0; idx<3; ++idx)
    {
        val=buf[(uint16_t)(idx)];
        ESYSTEST_REQUIRE_EQUAL(sbuf[idx+1], val);
    }

    ESYSTEST_REQUIRE_EQUAL(0, buf.Push(sbuf+2, 6));
    ESYSTEST_REQUIRE_EQUAL(9, buf.GetSize());
    ESYSTEST_REQUIRE_EQUAL(5, buf.GetReadIdx());
    ESYSTEST_REQUIRE_EQUAL(4, buf.GetWriteIdx());

    for (idx=0; idx<3; ++idx)
    {
        val=buf[(uint16_t)(idx)];
        ESYSTEST_REQUIRE_EQUAL(sbuf[idx+1], val);
    }

    for (idx=0; idx<6; ++idx)
    {
        val=buf[(uint16_t)(idx+3)];
        ESYSTEST_REQUIRE_EQUAL(sbuf[idx+2], val);
    }
}