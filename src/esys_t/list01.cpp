/*!
 * \file esystest_t/list01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"
#include <esys/inttypes.h>
#include <esys/list.h>
#include <esys/listitem.h>

#include <iostream>

namespace list01
{

class Obj: public esys::ListItem<Obj>
{
public:
    Obj(int32_t value=0);
    int32_t GetValue();
    void SetValue(int32_t value);
protected:
    int32_t m_value;
};

Obj::Obj(int32_t value) : esys::ListItem<Obj>(), m_value(value)
{
}

int32_t Obj::GetValue()
{
    return m_value;
}

void Obj::SetValue(int32_t value)
{
    m_value = value;
}

ESYSTEST_AUTO_TEST_CASE(List01)
{
    Obj objects[10] = { 12, 5, 10, 3, 7, 4, 9, 11, 6, 9 };
    int32_t result;
    esys::List<Obj> list;
    Obj *object;

    for (uint32_t idx = 0; idx < 10; ++idx)
        list.Add(&objects[idx]);

    std::cout << "Unsorted list:" << std::endl;
    object = list.GetFirst();
    while (object != nullptr)
    {
        std::cout << object->GetValue() << " ";
        object = object->GetNext();
    }
    std::cout << std::endl;

    list.Sort(&Obj::GetValue);

    std::cout << "Sorted list:" << std::endl;
    object = list.GetFirst();
    while (object != nullptr)
    {
        std::cout << object->GetValue() << " ";
        object = object->GetNext();
    }
    std::cout << std::endl;
    /*result = 0;
    ESYSTEST_REQUIRE_EQUAL(0, result);

    ESYSTEST_REQUIRE_NE(1, result);

    ESYSTEST_REQUIRE_EQUAL(1, result);

    ESYSTEST_REQUIRE_LT(-1, result); */

}

}
