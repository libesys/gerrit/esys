/*!
 * \file esys_t/pointer02.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"
#include "esys_t/bufferfixture.h"

#include <esys/bufferstatic.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_FIXTURE_TEST_CASE(Buffer04, BufferFixture)
{
    esys::BufferStatic<uint8_t, 32> buf_ps;

    BasicTest<uint8_t, esys::BufferStatic<uint8_t, 32>, 128>(buf_ps, 32);

    uint8_t buf[128];
    esys::BufferWrap<uint8_t> buf_pw(buf, 64);

    BasicTest<uint8_t, esys::BufferWrap<uint8_t>, 128>(buf_pw, 64);

}