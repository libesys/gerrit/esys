/*!
 * \file esys_t/task01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"

#include <esys/inttypes.h>
#include <esys/taskbase.h>
#include <esys/tasklet.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

class Main: public esys::TaskBase
{
public:
    Main();

    virtual void Init();
    virtual void Setup();
    virtual void Loop();

    virtual void Wait();
    virtual void Signal();

    virtual esys::millis_t Millis();
    virtual void Sleep(uint64_t val);

    uint16_t GetCount();
protected:
    virtual void InternalCoreLoop();
    esys::TaskletInfo m_infos[10];
    uint16_t m_count;
};

Main::Main(): esys::TaskBase("Main")
{
    SetInfoArray(m_infos,10);
}

void Main::Init()
{
    m_count=0;
}

void Main::Setup()
{
}

void Main::Loop()
{
    m_count++;

    if (m_count>=10)
        Exit(-1);
}

void Main::Wait()
{
}

void Main::Signal()
{
}

esys::millis_t Main::Millis()
{
    return 0;
}

void Main::Sleep(uint64_t val)
{
}

uint16_t Main::GetCount()
{
    return m_count;
}

void Main::InternalCoreLoop()
{
}

/*class Tasklet0: public esys::Tasklet
{
public:
    Tasklet0();

    virtual void Do();
};

Tasklet0::Tasklet0()
{
}

void Tasklet0::Do()
{

} */


//ESYSTEST_FIXTURE_TEST_CASE(Task01, BufferFixture)
ESYSTEST_AUTO_TEST_CASE(Task01)
{
    Main main_task;

    main_task.Init();
    main_task.Setup();
    main_task.Do();

    ESYSTEST_REQUIRE_EQUAL(10, main_task.GetCount());
}