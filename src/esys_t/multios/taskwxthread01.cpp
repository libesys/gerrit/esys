/*!
 * \file esys_t/multios/taskwxthread01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"
#include <esys/multios/taskwxthread.h>
#include <esys/taskbase.h>

/*
class Main: public esys::TaskBase
{
public:
    Main();
    virtual ~Main();

    virtual esys::millis_t Millis();

    virtual void Loop();
    virtual void Init();
    virtual void Setup();
protected:
#if defined WIN32 || defined MULTIOS
    virtual void _Sleep(uint64_t val);
#endif
};

Main::Main(): esys::TaskBase()
{
}

Main::~Main()
{
}

esys::millis_t Main::Millis()
{
    return 0;
}

void Main::Loop()
{
}

void Main::Init()
{
}

void Main::Setup()
{
}

#if defined WIN32 || defined MULTIOS
void Main::_Sleep(uint64_t val)
{
}
#endif

*/
ESYSTEST_AUTO_TEST_CASE(TaskwxThread01)
{
    //esys::wx::TaskThread<Main> main;


}

