/*!
 * \file esys_t/buffer03.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"
#include <esys/bufferstatic.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_AUTO_TEST_CASE(Buffer03)
{
    esys::BufferStatic<uint8_t, 32> buf;
    esys::BufferStatic<uint8_t, 32>::Ptr p;

    uint8_t sbuf[16]= { 0x12, 0x45, 0x75, 0xAA };

    buf.SetSize(4);
    memcpy(buf, sbuf, 4);

    ESYSTEST_REQUIRE_EQUAL(0x12, buf[0]);
    ESYSTEST_REQUIRE_EQUAL(0x45, buf[1]);
    ESYSTEST_REQUIRE_EQUAL(0x75, buf[2]);
    ESYSTEST_REQUIRE_EQUAL(0xAA, buf[3]);

    p=buf.Block();
    ESYSTEST_REQUIRE_EQUAL(0x12, *p);
    p++;
    ESYSTEST_REQUIRE_EQUAL(true, p.IsValid());
    ESYSTEST_REQUIRE_EQUAL(0x45, *p);
    p++;
    ESYSTEST_REQUIRE_EQUAL(true, p.IsValid());
    ESYSTEST_REQUIRE_EQUAL(0x75, *p);
    p++;
    ESYSTEST_REQUIRE_EQUAL(true, p.IsValid());
    ESYSTEST_REQUIRE_EQUAL(0xAA, *p);
    p++;
    ESYSTEST_REQUIRE_EQUAL(false, p.IsValid());

}
