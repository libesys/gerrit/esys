/*!
 * \file esys_t/unitestexe.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifdef ESYS_BOOST_TEST_USE

#define BOOST_TEST_MAIN

#ifdef _MSC_VER
#pragma warning (disable : 4996)
#pragma warning (disable : 4985)
#endif

#include <boost/test/unit_test.hpp>

#ifdef _MSC_VER
#pragma warning (default : 4996)
#pragma warning (default : 4985)
#endif

#else

#define ESYSTEST_TEST_MAIN

#include <esystest/unit_test.h>

#endif

#ifdef WIN32

#if ESYS_USE_VLD
#include <vld.h>
#endif

#endif

#include "esys_t/unittestexe.h"

#include <esys/assert.h>

#include <iostream>

/*namespace esys_t
{ */

UnitTestExe *UnitTestExe::s_unit_test_exe = nullptr;

UnitTestExe &UnitTestExe::Get()
{
    return *s_unit_test_exe;
}

UnitTestExe::UnitTestExe() : esys_t_fix::UnitTestExeBase()
{
    s_unit_test_exe = this;
    m_logger.Set(&std::cout);
    esystest::Logger::Set(&m_logger);
    //esystest::TestCaseCtrl::Set(&m_test_ctrl);
    esystest::TestCaseCtrl::Set(this);
}

UnitTestExe::~UnitTestExe()
{
}

ESYSTEST_GLOBAL_FIXTURE(UnitTestExe);

//}




