/*!
 * \file esys_t/buffer05.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys_t/esys_t_prec.h"
#include "esys_t/bufferfixture.h"

#include <esys/bufferstatic.h>

#ifdef ESYS_VHW
#include <iostream>
#endif

ESYSTEST_FIXTURE_TEST_CASE(Buffer05, BufferFixture)
{
    esys::BufferStatic<uint8_t, 32> buf_ps;
    esys::BufferStatic<uint8_t, 32>::Ptr p;
    uint8_t buf0[4]= { 0xaa, 0x44, 0x15, 0x45 };
    uint8_t buf1[4];
    uint16_t idx;

    buf_ps.SetSize(4);

    esys::memcpy(buf_ps, buf0, 4);

    for (idx=0; idx<4; ++idx)
    {
        ESYSTEST_REQUIRE_EQUAL(buf0[idx], buf_ps[idx]);
    }

    esys::memcpy(&buf1[0], buf_ps, 4);

    for (idx=0; idx<4; ++idx)
    {
        ESYSTEST_REQUIRE_EQUAL(buf0[idx], buf1[idx]);
    }

    p=buf_ps.Block();
    esys::memcpy(&buf1[0], p+2, 2);
    //esys::memcpy(&buf1[0], p, 2);

    for (idx=0; idx<2; ++idx)
    {
        ESYSTEST_REQUIRE_EQUAL(buf0[idx+2], buf1[idx]);
    }
}