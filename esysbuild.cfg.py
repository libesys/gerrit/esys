from esysbuild.lib import *
from esysbuild.exe import *
from esysbuild.buildfile import *

class ESysBuildFile(BuildFile):
    def __init__(self):
        super().__init__()
        self.SetBootStrapScript("config/bootstrap")
        
    def AddLibs(self, lib_mngr):   
        vhw_hw=["vhw", "hw"]
        
        esys=Lib("esys")
        esys.AddGroupName(vhw_hw)
        esys.SetLinkCfg(Lib.LINK_LIBTOOL, "src/esys/libesys.la")
        esys.SetLinkCfg(Lib.LINK_INSTALLED, "-lesys")
        esys.AddDependencies(["ctb", "mysystemc", "dbg_log", "logmod"])
        lib_mngr.Add(esys)
        
        esys_t=Exe("esys_t")
        esys_t.AddGroupName("vhw")
        esys_t.AddDependencies(["esys", "esystest", "mysystemc"])
        lib_mngr.Add(esys_t)            
        return 0
    
def GetBuildFile():
    return ESysBuildFile()



    