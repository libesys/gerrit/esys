/*! \defgroup ESys_grp ESys

Some general info.

This manual is divided in the following sections:
- \subpage intro
- \subpage advanced "Advanced usage"
- \subpage developper "Developper"
*/

//-----------------------------------------------------------

/*! \page intro Introduction
This page introduces the user to the topic.
Now you can proceed to the \ref advanced "advanced section".
*/

//-----------------------------------------------------------

/*! \page advanced Advanced Usage
This page is for advanced users.
Make sure you have first read \ref intro "the introduction".
*/

/*! \page developper Deveopper
\tableofcontents
Leading text.
\section dep Dependencies
This page contains the subsections \ref subsection1 and \ref subsection2.
For more info see page \ref page2.
\subsection esys_dep ESys Dependencies
LAlala
\subsubsection boost Boost
C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\bin
Visual Studio 2008 x64 Win64 Command Prompt
b2 link=shared threading=multi runtime-link=shared address-model=64

Visual Studio 2008 Command Prompt
b2 link=shared threading=multi runtime-link=shared

\subsubsection wxwidgets wxWidgets
Text.
\subsubsection wxctb wxCTB
Text.
\subsubsection dbg_log dbg_log
Text.

\subsection esysprog_dep ESysProg Dependencies
\subsubsection zlib zlib
The library can be downloaded from http://www.zlib.net/. The version used is 1.2.8. To build the library, simply go to the folder
contrib\\vstudio and choose the Visual C++ version you are interested in: so Visual C++ 2008 here, named as vc90. 
Here, we build the DLL without the assembly code, so you need to choose the configuration ReleaseWithoutAsm.

After the build, the debug and release libraries for win32 and x64 compiled with Visual C++ 2008 are copied respectively 
in the folder lib\\vc90_dll and lib\\vc90_x64_dll. Then an environment variable called ZLIB should be creates and it's value
shall be the folder where the zlib library is found, such that %ZLIB%\\lib\\vc90_dll is the folder where the win32 libraries 
are found.

\subsubsection libarchive libarchive
The library can be downloaded from http://www.libarchive.org/. To build the library, the build tool CMake must first installed. 
It can be found at http://www.cmake.org. At the time this document is written, the verion of libarchive used is 3.1.2 and CMake 
is 2.8.12.2.

\subsubsection poco POCO C++
http://pocoproject.org

\subsection subsection2 The second subsection
More text.This page is for developpers.
Make sure you have first read \ref intro "the introduction".

\subsection esysfont_dep ESysFont Dependencies

\subsection esyscontrib_dep ESysContrib Dependencies
Windows DDK 7.1 
cd C:\Program Files\Microsoft SDKs\Windows\v7.0\Setup
type
    WindowsSdkVer.exe
*/